# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'forums'
        db.create_table('Forum_forums', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('Description', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('Forum', ['forums'])

        # Adding M2M table for field Moderators on 'forums'
        db.create_table('Forum_forums_Moderators', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('forums', models.ForeignKey(orm['Forum.forums'], null=False)),
            ('staff', models.ForeignKey(orm['MIS.staff'], null=False))
        ))
        db.create_unique('Forum_forums_Moderators', ['forums_id', 'staff_id'])

        # Adding model 'threads'
        db.create_table('Forum_threads', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('Forum', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Forum.forums'])),
            ('CreatedBy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('Status', self.gf('django.db.models.fields.CharField')(max_length=25)),
        ))
        db.send_create_signal('Forum', ['threads'])

        # Adding model 'posts'
        db.create_table('Forum_posts', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('CreatedBy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('CreatedOn', self.gf('django.db.models.fields.DateField')(default=datetime.date(2013, 3, 11))),
            ('Thread', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['Forum.threads'])),
            ('BodyText', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal('Forum', ['posts'])


    def backwards(self, orm):
        
        # Deleting model 'forums'
        db.delete_table('Forum_forums')

        # Removing M2M table for field Moderators on 'forums'
        db.delete_table('Forum_forums_Moderators')

        # Deleting model 'threads'
        db.delete_table('Forum_threads')

        # Deleting model 'posts'
        db.delete_table('Forum_posts')


    models = {
        'Forum.forums': {
            'Description': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'forums'},
            'Moderators': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['MIS.Staff']", 'symmetrical': 'False'}),
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Forum.posts': {
            'BodyText': ('django.db.models.fields.TextField', [], {}),
            'CreatedBy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Staff']"}),
            'CreatedOn': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2013, 3, 11)'}),
            'Meta': {'object_name': 'posts'},
            'Thread': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Forum.threads']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'Forum.threads': {
            'CreatedBy': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Staff']"}),
            'Forum': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['Forum.forums']"}),
            'Meta': {'object_name': 'threads'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'Status': ('django.db.models.fields.CharField', [], {'max_length': '25'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.address': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AddressLine1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'AddressLine2': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'AddressLine3': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'AddressLine4': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'HomeSalutation': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'object_name': 'Address'},
            'Note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Phone1': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'Phone2': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'Phone3': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'PostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'PostalTitle': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.menutypes': {
            'Desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Logo': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'MenuLevel': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'ordering': "['Name']", 'object_name': 'MenuTypes'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'schoolMenu'", 'to': "orm['MIS.School']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.school': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Address']"}),
            'HeadTeacher': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'School'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'TemplateBanner': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBanner.jpg'", 'max_length': '200'}),
            'TemplateCSS': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBasic.css'", 'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.staff': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Address']", 'null': 'True', 'blank': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'DefaultMenu': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.MenuTypes']", 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'FailedLoginAttempts': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname']", 'object_name': 'Staff'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'User': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['Forum']
