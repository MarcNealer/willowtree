'''
Created By:     Marc Nealer
Creation date:  11-03-2013
Description:
    This is the model for the WillowTree forum. The forum is a very
    simple forum system allowing users to post messages onto the board
    for all users to view and reply to
    Users will have to be registered via Willowtree and can only access
    the forum fromt he willowTree menu after loggin on
    
Change Log:

'''

from django.db import models
from django.contrib.auth.models import User, Permission
import datetime
from django.contrib import admin

StatusTypes = (
('Open', 'Open'),
('Closed', 'Closed'),
('Fixed', 'Fixed'),
('Implemented', 'Implemented'),
('Rejected', 'Rejected'),
)


class forums(models.Model):
    Name = models.CharField(max_length=150)
    Description = models.TextField()
    Moderators = models.ManyToManyField('MIS.Staff')

    def __unicode__(self):
        return self.Name
    def num_posts(self):
        return sum([t.num_posts() for t in self.threads_set.all()])

    def last_post(self):
        if self.threads_set.count():
            last = None
            for t in self.threads_set.all():
                l = t.last_post()
                if l:
                    if not last: last = l
                    elif l.CreatedOn > last.CreatedOn: last = l
            return last

class threads(models.Model):
    Name = models.CharField(max_length=150)
    Forum = models.ForeignKey('forums')
    CreatedBy = models.ForeignKey('MIS.Staff')
    CreatedOn = models.DateTimeField(default=datetime.datetime.now())
    Updated = models.DateTimeField(default=datetime.datetime.now())      
    Status = models.CharField(choices=StatusTypes,max_length=25)
    
    def __unicode__(self):
        return self.Name
        
    def num_posts(self):
        return self.posts_set.count()

    def num_replies(self):
        return self.posts_set.count() - 1

    def last_post(self):
        if self.posts_set.count():
            return self.posts_set.order_by("CreatedOn").reverse()[0]
            
    def createdToday(self):
        if self.CreatedOn.date() == datetime.date.today():
            return True
        else:
            return False
    class Meta():
        ordering=['-Updated']
class posts(models.Model):
    CreatedBy = models.ForeignKey('MIS.Staff')
    CreatedOn = models.DateTimeField(default=datetime.datetime.now())
    Thread = models.ForeignKey('threads')
    BodyText = models.TextField()
    
    def UserName(self):
        return '%s %s' % (self.CreatedBy.Forename, self.CreatedBy.Surname)
        
    def UserImage(self):
        if not self.CreatedBy.Picture:
            return False
        else:
            self.CreatedBy.Picture
    def createdToday(self):
        if self.CreatedOn.date() == datetime.date.today():
            return True
        else:
            return False
   