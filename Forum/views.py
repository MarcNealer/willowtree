'''
Created By:     Marc Nealer
Creation date:  11-03-2013
Description:
    This is the views for the WillowTree forum. The forum is a very
    simple forum system allowing users to post messages onto the board
    for all users to view and reply to
    Users will have to be registered via Willowtree and can only access
    the forum fromt he willowTree menu after loggin on
    
Change Log:

'''
import datetime
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.core.urlresolvers import reverse
from django.core.context_processors import csrf
from django.http import *

from settings import MEDIA_ROOT, MEDIA_URL
from Forum.models import *
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger


def ForumProc(request):
    if 'Messages' in request.session:
        Messages=request.session['Messages']
        del request.session['Messages']
    else:
        Messages=[]
    return {'school':'Forum',
            'Logo':'Thomas_Logo.jpg',
            'UserName':request.user.username,
            'ActionTime':time.strftime("%H:%M",time.localtime()),
'ActionDate':time.strftime("%d/%m/%y",time.localtime()),
'LoggedIn':True,'Messages':Messages}


def ListForums(request):
    c={'forums':forums.objects.filter()}
    return render_to_response('forums.html',c,context_instance=RequestContext(request,processors=[ForumProc]))

def ListThreads(request,Forum):
    ThreadList=threads.objects.filter(Forum__id=Forum)
    paginator=Paginator(ThreadList,10)
    
    page = request.GET.get('page')
    if not page:
        page=1
    try:
        threadspage = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        threadspage = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        threadspage = paginator.page(paginator.num_pages)
    c={'threads':threadspage,'Forum':forums.objects.get(id=Forum)}
    return render_to_response('threads.html',c,context_instance=RequestContext(request,processors=[ForumProc]))


def ListPosts(request,Thread):
    PostList=posts.objects.filter(Thread__id=int(Thread))
    paginator=Paginator(PostList,10) 
    
    page = request.GET.get('page')
    if not page:
        page=1
    try:
        postspage = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        postspage = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        postspage = paginator.page(paginator.num_pages)
    c={'posts':postspage}    
    return render_to_response('posts.html',c,context_instance=RequestContext(request,processors=[ForumProc]))

def AddThread(request,Forum):
    if 'Title' in request.POST:
        NewThread=threads(Name=request.POST['Title'],
                         Forum=forums.objects.get(id=int(Forum)),
                         CreatedBy=request.session['StaffId'],
                         Status='Open')
        NewThread.save()
        NewPost=posts(CreatedBy=request.session['StaffId'],
                      Thread=NewThread,
                      BodyText=request.POST['BodyText'])
        NewPost.save()
    return HttpResponseRedirect('/WillowTree/forum/threads/%s/' % (Forum))

def AddPost(request,Thread):
    threadRec=threads.objects.get(id=int(Thread))
    NewPost=posts(Thread=threadRec,CreatedBy=request.session['StaffId'],
                 BodyText=request.POST['BodyText'])
    NewPost.save()
    threadRec.Updated=datetime.datetime.now()
    threadRec.save()
    return HttpResponseRedirect('/WillowTree/forum/posts/%s/' % (threadRec.id))

def SearchPosts(request):
    SearchItems=request.POST['SearchItems'].split(' ')
    postrecs=posts.objects.all()
    for items in SearchItems:
        postrecs=postrecs.filter(BodyText__icontains=items)
    ThreadList=threads.objects.filter(id__in=[x.Thread.id for x in postrecs])
    
    paginator=Paginator(ThreadList,10)
    
    page = request.GET.get('page')
    if not page:
        page=1
    try:
        threadspage = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        threadspage = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        threadspage = paginator.page(paginator.num_pages)
    c={'threads':threadspage,'SearchString':request.POST['SearchItems']}
    return render_to_response('SearchResults.html',c,context_instance=RequestContext(request,processors=[ForumProc]))
    