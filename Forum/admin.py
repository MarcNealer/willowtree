# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 13:38:58 2013

@author: dbmgr
"""

from django.contrib import admin
from Forum.models import *
        

admin.site.register(forums)
admin.site.register(threads)
admin.site.register(posts)   