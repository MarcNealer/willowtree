# Django middleware
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings
from django.conf.urls import patterns, url, include

# WillowTree imports
from MIS.modules.alerts import AlertViews
from MIS.modules.notes import NoteViews
from MIS.modules.notes import NotesExplorer
from MIS.modules.setManager import SetManagerViews
from MIS.modules.attendance import AttendanceViews
from MIS.modules.LettersLabels import LetterViews
from MIS.modules.LettersLabels import ElectronicSignatures
from MIS.modules.Newrecords import NewRecordViews
from MIS.modules.manager import ManagerViews
from MIS.modules.classlists import ClassListsViews
from MIS.modules.Favourites import FavouritesViews
from MIS.modules.gradeInput import GradeViews
from MIS.modules.reports import ReportViews
from MIS.modules.AnalysisReports import AnalysisReportViews
from MIS.modules import PupilViews
from MIS.modules.SEN import SENViews
from MIS.modules.EAL import EALViews
from MIS.modules.GiftedAndTalented import GTViews
from MIS.modules.moreAbled import moreAbledViews
from MIS import views
from MIS.ViewExtras import SchoolAuth, APIAuth
from MIS.api import *
from tastypie.api import Api
from MIS.modules.globalVariables import GlobalVariablesViews
from MIS.modules.Schools import SchoolsViews
from MIS.modules.resetPassword import resetPasswordViews
from MIS.modules.emailFromWillowTree import emailViews
from MIS.modules.PotentialGroups import PotentialGroupsViews
from MIS.modules.transfers import transferViews
from MIS.modules.ReportsToParents import ReportsToParentsViews
from MIS.modules.staff import staffViews
from MIS.modules.StaffCpd import StaffCpdViews
from MIS.modules.documentServer import documentServerViews
from MIS.modules.SpecialGroups import SpecialGroupsViews
import TimeTables.urls

import Forum.views


W_api=Api(api_name='vle')
W_api.register(GroupResource())
W_api.register(PupilResource())
W_api.register(PupilGroupResource())
W_api.register(ContactResource())
W_api.register(FamilyResource())
W_api.register(FamilyContactResource())
W_api.register(FamilyChildrenResource())
W_api.register(StaffResource())
W_api.register(StaffGroupResource())
W_api.register(UserResource())
W_api.register(StaffFavResource())
W_api.register(PupilTimeTableResource())
W_api.register(StaffTimeTableResource())
W_api.register(MenuResource())
# groupCallFive api
G_api=Api(api_name='groupcall')
G_api.register(GetDataForGrpCallResourceAmended())

admin.autodiscover()


# GradeInputField
from MIS.api_modules.GradeInputFields.GradeInputField_Urls import grade_input_field_urls
# GradeAPI
from MIS.api_modules.Grading.GradeAPI_Urls import grade_api_urls
# AnalysisStore
from MIS.api_modules.AnalysisStore.AnalysisStore_urls import AnalysisStore_urls
# StaffAttendance
from StaffAttendance import StaffAttendanceUrls
# General API calls
from MIS.api_modules.General.General_urls import General_urls


# General API calls
from MIS.api_modules.Markbook.MarkBook_urls import MarkBook_urls


urlpatterns = patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/TimeTables/', include(TimeTables.urls)),
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^media/(?P<FileName>.*)',APIAuth(views.send_file)),
    url(r'^getmedia/',SchoolAuth(views.getMediaFile)),
    url(r'^api/', include(W_api.urls)),
    url(r'^api/', include(G_api.urls)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupil/(?P<PupilNo>\d+)/$',SchoolAuth(views.pupilView)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staff/(?P<StaffNo>\d+)/$',SchoolAuth(views.staffView)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/myStaffPage/$',SchoolAuth(staffViews.myStaffPage)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/FamilyRecord/(?P<PupilNo>\d+)/$',SchoolAuth(views.familyView)),

    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Groups/(?P<PupilNo>\d+)/$',SchoolAuth(views.ListGroups)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Welcome/$',SchoolAuth(views.Welcome)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PupilSearch/$',views.PupilSearch),
    url(r'^(?P<school>[^/]+)/Authenticated/$',views.Authenticate),
    url(r'^(?P<school>[^/]+)/Authenticated1/$',views.Authenticate1),
    url(r'^(?P<school>[^/]+)/Register/$',views.RegisterUser),
    url(r'^(?P<school>[^/]+)/CreateNewUser/$',views.CreateNewUser),
    url(r'^(?P<school>[^/]+)/FireDrill/', AttendanceViews.FireDrill),
    url(R'^(?P<school>[^/]+)/Logout/$',views.Logout),
    url(R'^(?P<school>[^/]+)/login/$',views.Login),
    url(r'^login/$',views.Login),
    url(r'^$',views.Login),

)
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#
#       *************   Manager URLS  **************
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.Manage), {'ManageType':'All'}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ManageAll/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.Manage), {'ManageType':'All'}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ManageThis/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.Manage), {'ManageType':'This'}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/CopyAPupil/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.CopyAPupil)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/MoveAPupil/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.MoveAPupil)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/MoveToKindie/(?P<PupilNo>[^/]+)/$',SchoolAuth(ManagerViews.MoveCopyToKindie)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/MoveToMain/(?P<PupilNo>[^/]+)/$',SchoolAuth(ManagerViews.MoveCopyFromHolding)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/MoveToReserve/(?P<PupilNo>[^/]+)/$',SchoolAuth(ManagerViews.MoveCopyFromHolding)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/MoveAnApplicant/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.MoveAnApplicant)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/MoveFromHolding/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.MoveFromHolding)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/RemoveFromHolding/(?P<GroupName>[^/]+)/(?P<PupilNo>[^/]+)/$',SchoolAuth(ManagerViews.RemoveFromHolding)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/UpdateApplicant/(?P<GroupName>[^/]+)/(?P<PupilNo>[^/]+)/$',SchoolAuth(ManagerViews.UpdateApplicant)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/RemoveApplicant/(?P<GroupName>[^/]+)/(?P<PupilNo>[^/]+)/$',SchoolAuth(ManagerViews.RemoveApplicant)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/CSV/(?P<GroupName>[^/]+)/(?P<FileName>[^/]+)$',SchoolAuth(ManagerViews.ReturnCSV), {'ManageType':'All'}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/CSV2/(?P<GroupName>[^/]+)/(?P<FileName>[^/]+)$',SchoolAuth(ManagerViews.ReturnCSV2), {'ManageType':'All'}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/EditStaffGroup/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.EditStaffGrp)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/removePupilFromGrp/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.removePupilFromGrp)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Manage/pupilMoveToCurrent/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.moveApplicantToCurrent)),
)
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#
#       *************   Transefer URLS  **************
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Transfer/SearchPage/$',SchoolAuth(transferViews.transferSearchPage)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Transfer/Search/$', SchoolAuth(views.PupilSearch), {'transfer': True}),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Transfer/Complete/(?P<PupilNo>\d+)/$',SchoolAuth(views.pupilView), {'transfer': True}),
)
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#
#       *************   Edit Pupil record  URLS **************
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditPupilDetails/(?P<PupilNo>\d+)/$',SchoolAuth(PupilViews.EditPupilDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditPupilFormHouse/(?P<PupilNo>\d+)/$',SchoolAuth(PupilViews.EditPupilFormHouse)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/update_pupil_picture/(?P<PupilNo>\d+)/$',SchoolAuth(PupilViews.update_pupil_picture)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditPupilOtherInformation/(?P<PupilNo>\d+)/$',SchoolAuth(PupilViews.EditPupilOtherInformation)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditPupilPermissions/(?P<PupilNo>\d+)/$',SchoolAuth(PupilViews.EditPupilPermissions)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/UpdateLeavingDetails/(?P<PupilNo>\d+)/$',SchoolAuth(PupilViews.UpdateLeavingDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PupilRec_MvToHolding/(?P<PupilId>\d+)/$',SchoolAuth(ManagerViews.removeFromAllGrpsMvToHolding)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifyMedicalDetails/(?P<PupilNo>\d+)/$',SchoolAuth(PupilViews.pupilModifyMedicalDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilAddFutureSchoolsProgressDetails/(?P<PupilNo>\d+)/$',SchoolAuth(PupilViews.pupilAddFutureSchoolsProgressDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifyFutureSchoolsProgressDetails/(?P<PupilNo>\d+)/(?P<schoolId>\d+)/$',SchoolAuth(PupilViews.pupilModifyFutureSchoolsProgressDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilDeleteFutureSchoolsProgressDetails/(?P<PupilNo>\d+)/(?P<schoolId>\d+)/$',SchoolAuth(PupilViews.pupilDeleteFutureSchoolsProgressDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifyFutureSchoolsProgressInterview/(?P<PupilNo>\d+)/(?P<schoolId>\d+)/$',SchoolAuth(PupilViews.pupilModifyFutureSchoolsProgressInterview)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/move_to_group_from_pupil_record/(?P<pupil_id>\d+)/$',SchoolAuth(ManagerViews.move_to_group_from_pupil_record)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/remove_from_group_from_pupil_record/(?P<pupil_id>\d+)/$',SchoolAuth(ManagerViews.remove_from_group_from_pupil_record)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/view_current_pupil_groups_for_a_school_as_a_list/$',
        SchoolAuth(PupilViews.view_current_pupil_groups_for_a_school_as_a_list)),
)

#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#
#       *************   Admin Edit Family and add new Applicants **************
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/FamilySearchForm/$',SchoolAuth(NewRecordViews.FamilySearchForm)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/FindFamilies/$',SchoolAuth(NewRecordViews.FindFamilies)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/NewFamily/$',SchoolAuth(NewRecordViews.NewFamily)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditFamily/(?P<FamilyNo>\d+)/$',SchoolAuth(NewRecordViews.EditFamily)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/NewFamilyAddress/(?P<FamilyNo>\d+)/$',SchoolAuth(NewRecordViews.NewFamilyAddress)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/FindExistingFamilyAddress/(?P<FamilyNo>\d+)/$',SchoolAuth(NewRecordViews.FindExistingFamilyAddress)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AddExistingFamilyAddress/(?P<FamilyNo>\d+)/(?P<AddressNo>\d+)/$',SchoolAuth(NewRecordViews.AddExistingFamilyAddress)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/RemoveFamilyAddress/(?P<FamilyNo>\d+)/(?P<AddressNo>\d+)/$',SchoolAuth(NewRecordViews.RemoveFamilyAddress)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyFamilyAddress/(?P<FamilyNo>\d+)/(?P<AddressNo>\d+)/$',SchoolAuth(NewRecordViews.ModifyFamilyAddress)),

    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/NewFamilyContact/(?P<FamilyNo>\d+)/$',SchoolAuth(NewRecordViews.NewFamilyContact)),

    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/RemoveFamilyContact/(?P<FamilyNo>\d+)/(?P<ContactNo>\d+)/$',SchoolAuth(NewRecordViews.RemoveFamilyContact)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyFamilyContact/(?P<FamilyNo>\d+)/(?P<ContactNo>\d+)/$',SchoolAuth(NewRecordViews.ModifyFamilyContact)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/contact_skills_bank/(?P<contact_id>\d+)/(?P<family_id>\d+)/$',SchoolAuth(NewRecordViews.contact_skills_bank)),

    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyContactAddressOld/(?P<FamilyNo>\d+)/(?P<AddressNo>\d+)/$',SchoolAuth(NewRecordViews.ModifyContactAddressOld)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyContactAddressNew/(?P<FamilyNo>\d+)/(?P<ContactNo>\d+)/$',SchoolAuth(NewRecordViews.ModifyContactAddressNew)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyContactAddressFromFamily/(?P<FamilyNo>\d+)/(?P<ContactNo>\d+)/(?P<AddressNo>\d+)/$',SchoolAuth(NewRecordViews.ModifyContactAddressFromFamily)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyContactWorkAddressOld/(?P<FamilyNo>\d+)/(?P<AddressNo>\d+)/$',SchoolAuth(NewRecordViews.ModifyContactWorkAddressOld)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyContactWorkAddressNew/(?P<FamilyNo>\d+)/(?P<ContactNo>\d+)/$',SchoolAuth(NewRecordViews.ModifyContactWorkAddressNew)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyContactWorkAddressFromFamily/(?P<FamilyNo>\d+)/(?P<ContactNo>\d+)/(?P<AddressNo>\d+)/$',SchoolAuth(NewRecordViews.ModifyContactWorkAddressFromFamily)),

    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/NewPupilRecord/(?P<FamilyNo>\d+)/$',SchoolAuth(NewRecordViews.NewPupilRecord)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Applicant/UpdateExtraFields/(?P<PupilNo>[^/]+)/$',SchoolAuth(NewRecordViews.UpdateApplicantExtra)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/addExistingContactSearch/(?P<FamilyId>[^/]+)/$',SchoolAuth(NewRecordViews.addExistingContactSearch)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/addExistingContactAdd/(?P<familyId>[^/]+)/$',SchoolAuth(NewRecordViews.addExistingContactAdd)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/addExistingPupilSearch/(?P<FamilyId>[^/]+)/$',SchoolAuth(NewRecordViews.addExistingPupilSearch)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/addExistingPupilAdd/(?P<familyId>[^/]+)/$',SchoolAuth(NewRecordViews.addExistingPupilAdd)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/RemoveExistingPupil/(?P<familyId>[^/]+)/(?P<PupilNo>[^/]+)/$',SchoolAuth(NewRecordViews.RemoveExistingPupilFromFamily)),
)




#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#
#       *************   SetManager URLS **************
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SetManager/(?P<GroupId>[^/]+)/(?P<GroupSets>[^/]+)/$',SchoolAuth(SetManagerViews.SetManagerNew)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/UpdateSets/(?P<GroupId>[^/]+)/(?P<GroupSets>[^/]+)/$',SchoolAuth(SetManagerViews.UpdateSetsNew)),

)


#  #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#
#       *************   Attendance URLS **************
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Attendance/(?P<GroupName>[^/]+)/$',SchoolAuth(AttendanceViews.AttendanceForm)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Attendance/(?P<GroupName>[^/]+)/(?P<Am_Pm>[^/]+)/$',SchoolAuth(AttendanceViews.AttendanceForm)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AttendanceByGroup/(?P<AttDate>[^/]+)/$',SchoolAuth(AttendanceViews.AttendanceByGroupForm)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AttendanceByGroupProccess/(?P<Am_Pm>[^/]+)/(?P<AttDate>[^/]+)/$',SchoolAuth(AttendanceViews.AttendanceByGroupProccess)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AttendanceP/(?P<GroupName>[^/]+)/(?P<Am_Pm>[^/]+)/$',SchoolAuth(AttendanceViews.AttendanceP)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/DailyAttendanceReport/$',SchoolAuth(AttendanceViews.DailyAttendanceReport)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AttendanceMissingToday/$',SchoolAuth(AttendanceViews.MissingReport)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AttendanceMissingTodayCSV/$',SchoolAuth(AttendanceViews.MissingReportCSV)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AddFutureAttendances/(?P<PupilNo>[^/]+)/$',SchoolAuth(AttendanceViews.AddFutureAttendances)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AttendanceAdmin/(?P<GroupName>[^/]+)/(?P<Am_Pm>[^/]+)/(?P<AttDate>[^/]+)/$',SchoolAuth(AttendanceViews.AttendanceFormAdmin)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AttendancePAdmin/(?P<GroupName>[^/]+)/(?P<Am_Pm>[^/]+)/(?P<AttDate>[^/]+)/$',SchoolAuth(AttendanceViews.AttendancePAdmin)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AttendanceMarkGroup/(?P<GroupName>[^/]+)/(?P<Am_Pm>[^/]+)/(?P<AttDate>[^/]+)/$',SchoolAuth(AttendanceViews.AttendanceMarkGroup)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditAttendance/$',SchoolAuth(AttendanceViews.EditAttendanceMainPage)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditAttendanceResultsPage/$',SchoolAuth(AttendanceViews.EditAttendanceResultsPage)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditAttendancePopUp/$',SchoolAuth(AttendanceViews.EditAttendanceCode)),
    # school att report
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/schoolAttReport/$',SchoolAuth(AttendanceViews.SchoolAttendanceReportSelectPage)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SchoolAttendanceReportGenerate/$',SchoolAuth(AttendanceViews.SchoolAttendanceReportGenerate)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SchoolAttendanceReportGenerateForNonStandardDateRange/$',SchoolAuth(AttendanceViews.SchoolAttendanceReportGenerateForNonStandardDateRange)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/number_of_girls_and_boys_present_today/$',SchoolAuth(AttendanceViews.number_of_girls_and_boys_present_today)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/classAttendanceReport/$',SchoolAuth(AttendanceViews.classAttendanceReportGenerate)),
)

#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#
#       *************   Letter and labelsURLS **************
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #

urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/NewLetter/$',SchoolAuth(LetterViews.CreateLetter)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SaveANewLetter/$',SchoolAuth(LetterViews.SaveALetter)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SelectLetterToCopy/$',SchoolAuth(LetterViews.SelectLetterToCopy)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SelectLetterToModify/$',SchoolAuth(LetterViews.SelectLetterToModify)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyALetterForm/$',SchoolAuth(LetterViews.ModifyLetter)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/CopyALetterForm/$',SchoolAuth(LetterViews.CopyLetter)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SaveALetterModification/(?P<LetterNo>\d+)$',SchoolAuth(LetterViews.SaveALetterModification)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SelectALetter/(?P<GroupName>[^/]+)/$',SchoolAuth(LetterViews.SelectALetter)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/GenerateALetter/(?P<GroupName>[^/]+)/$',SchoolAuth(LetterViews.GenerateALetter)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/GenerateLetter/$',SchoolAuth(LetterViews.GenerateALetter)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/GenerateLabels/$',SchoolAuth(LetterViews.GenerateLetterLabels)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/generate_staff_letter_labels/$', SchoolAuth(LetterViews.generate_staff_letter_labels)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/generate_school_letter_labels/$', SchoolAuth(LetterViews.generate_school_letter_labels)),

    # electronic_signatures
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/electronic_signatures_home_page/$',
        SchoolAuth(ElectronicSignatures.electronic_signatures_home_page)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/view_electronic_signatures/$',
        SchoolAuth(ElectronicSignatures.view_electronic_signatures_for_a_school)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/add_electronic_signature/$',
        SchoolAuth(ElectronicSignatures.add_electronic_signature_for_a_school)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/remove_electronic_signature/(?P<electronic_signature_id>[^/]+)/$',
        SchoolAuth(ElectronicSignatures.remove_electronic_signature_for_a_school)),

)


#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#
#       *************   ALert URLS **************
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyAlerts/(?P<PupilNo>\d+)/$',SchoolAuth(AlertViews.ViewAlerts)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/UpdateAlerts/(?P<Alert_Group>[^/]+)/(?P<PupilNo>\d+)/$',SchoolAuth(AlertViews.UpdateAlerts)),

)



#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#
#       *************   Class Lists/Reports URLS **************
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PupilList/(?P<ListType>[^/]+)/(?P<GroupName>[^/]+)/$',SchoolAuth(ClassListsViews.PupilList)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/StaffList/(?P<ListType>[^/]+)/(?P<GroupName>[^/]+)/$',SchoolAuth(ClassListsViews.StaffList)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/GenerateClassList/(?P<GroupName>[^/]+)/$',SchoolAuth(ClassListsViews.GenerateClassList)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/MultipleClassLists/$',SchoolAuth(ClassListsViews.MultipleClassLists)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SelectClassList/$',SchoolAuth(ClassListsViews.SelectClassLists)),

    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/GenerateEmailList/(?P<GroupName>[^/]+)/$',SchoolAuth(ClassListsViews.GenerateEmailList)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AttendanceReports/(?P<GroupName>[^/]+)/$',SchoolAuth(ReportViews.AttendanceReports)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/OtherReports/(?P<GroupName>[^/]+)/$',SchoolAuth(ReportViews.OtherReports)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Report2ParentsReports/(?P<GroupName>[^/]+)/$',SchoolAuth(ReportsToParentsViews.Report2NewParents)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PupilGradeReport/(?P<PupilNo>\d+)/$',SchoolAuth(ReportViews.PupilGradeReport)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EYFSReports/(?P<GroupName>[^/]+)/$',SchoolAuth(ReportViews.EYFSMapping)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/schoolStats/$',SchoolAuth(ReportViews.schoolStats)),

    # GRADE EXTRACT
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/grade_extract_options_page/$',
        SchoolAuth(ReportViews.grade_extract_options_page)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/grade_extract_generate/$',
            SchoolAuth(ReportViews.grade_extract_generate)),
)

#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#
#       *************   Note SYSTEM URLS **************
#
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SaveANote/Manager/(?P<GroupName>[^/]+)/$',SchoolAuth(NoteViews.SaveANoteManager)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SaveANoteSinglePupil/Manager/(?P<PupilNo>[^/]+)/$',SchoolAuth(NoteViews.SaveANoteManagerSinglePupil)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SaveANoteSinglePupil_fromSEN/Manager/(?P<PupilNo>[^/]+)/$',SchoolAuth(NoteViews.SaveANoteManagerSinglePupil),{'fromSEN': True}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SaveANoteSinglePupil_fromEAL/Manager/(?P<PupilNo>[^/]+)/$',SchoolAuth(NoteViews.SaveANoteManagerSinglePupil),{'fromEAL': True}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SaveANoteSinglePupil_fromMostAble/Manager/(?P<PupilNo>[^/]+)/$',SchoolAuth(NoteViews.SaveANoteManagerSinglePupil),{'fromMostAble': True}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ViewPupilNotes/(?P<PupilNo>\d+)/$',SchoolAuth(NoteViews.ViewPupilNotes) ,{'achievementsOnly':False,'allNotesButAchievements':True}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ViewPupilAchievements/(?P<PupilNo>\d+)/$',SchoolAuth(NoteViews.ViewPupilNotes) ,{'achievementsOnly':True,'allNotesButAchievements':False}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ViewGroupNotes/(?P<GroupName>[^/]+)/$',SchoolAuth(NoteViews.ViewGroupNotes)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ListGroupNotes/(?P<GroupName>[^/]+)/$',SchoolAuth(NoteViews.ListGroupNotes)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/FilterAchievementNotes/(?P<DisplayType>[^/]+)/$',SchoolAuth(NoteViews.FilterNotes) ,{'FilterachievementNotes':True}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/FilterStandardNotes/(?P<DisplayType>[^/]+)/$',SchoolAuth(NoteViews.FilterNotes) ,{'FilterachievementNotes':False}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyPupilNote/(?P<NoteId>\d+)/(?P<PupilNo>\d+)/$',SchoolAuth(NoteViews.ModifyPupilNote)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/RemovePupilNote/(?P<PupilNo>\d+)/(?P<NoteId>\d+)/$',SchoolAuth(NoteViews.RemovePupilNote)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/RemovePupilNote2/(?P<PupilNo>\d+)/(?P<NoteId>\d+)/$',SchoolAuth(NoteViews.RemovePupilNote2)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/RemovePupilNote2_fromSEN/(?P<PupilNo>\d+)/(?P<NoteId>\d+)/$',SchoolAuth(NoteViews.RemovePupilNote2),{'fromSEN': True}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/RemovePupilNote2_fromEAL/(?P<PupilNo>\d+)/(?P<NoteId>\d+)/$',SchoolAuth(NoteViews.RemovePupilNote2),{'fromEAL': True}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/RemovePupilNote2_fromMostAble/(?P<PupilNo>\d+)/(?P<NoteId>\d+)/$',SchoolAuth(NoteViews.RemovePupilNote2),{'fromMostAble': True}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/UpdateANote/(?P<PupilNo>\d+)/(?P<NoteId>\d+)/$',SchoolAuth(NoteViews.UpdateANote)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/UpdateANote2/(?P<PupilNo>\d+)/(?P<NoteId>\d+)/$',SchoolAuth(NoteViews.UpdateANote2)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AddStaffNote/(?P<StaffNo>\d+)/$',SchoolAuth(NoteViews.NoteForStaff)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/CreateAStaffNote/$',SchoolAuth(NoteViews.CreateAStaffNote)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SaveAStaffNote/$',SchoolAuth(NoteViews.SaveAStaffNote)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ViewStaffNotes/(?P<StaffNo>\d+)/$',SchoolAuth(NoteViews.ViewStaffNotes)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyAStaffNote/(?P<NoteId>\d+)/(?P<StaffNo>\d+)/$',SchoolAuth(NoteViews.ModifyStaffNote)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/UpdateAStaffNote/$',SchoolAuth(NoteViews.UpdateAStaffNote)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ViewPupilIssues/(?P<PupilNo>\d+)/$',SchoolAuth(NoteViews.ViewPupilIssues)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/UpdateIssue/(?P<IssueNo>\d+)/(?P<PupilNo>\d+)/$',SchoolAuth(NoteViews.UpdateIssue)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/notesReportCSV/(?P<GroupName>[^/]+)/$',SchoolAuth(NoteViews.notesReportCSV)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/notesReport2/$',SchoolAuth(NoteViews.notes_report)),

    # NOTES EXPLORER
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/notes_explorer/get_all_current_pupil_groups/$',
        SchoolAuth(NotesExplorer.get_all_current_pupil_groups)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/notes_explorer/get_pupil_notes_count/$',
        SchoolAuth(NotesExplorer.get_pupil_notes_count)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/notes_explorer/get_pupil_notes_individual/$',
        SchoolAuth(NotesExplorer.get_pupil_notes_count_individual)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/notes_explorer/get_all_keywords/$',
        SchoolAuth(NotesExplorer.get_all_keywords)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/notes_explorer/home_page/$',
        SchoolAuth(NotesExplorer.home_page)),

    # NOTE NOTIFICATION/NOTE TO ISSUE APIs
    url(R'^view_note_notifications_for_a_staff_group/(?P<staff_group>[^/]+)/$',
        APIAuth(NoteViews.view_note_notifications_for_a_staff_group)),
    url(R'^add_note_notification/$',
        APIAuth(NoteViews.add_note_notification)),
    url(R'^delete_note_notification/(?P<object_id>[^/]+)/$',
        APIAuth(NoteViews.delete_note_notification)),
    url(R'^view_note_to_issues_for_a_staff_group/(?P<staff_group>[^/]+)/$',
        APIAuth(NoteViews.view_note_to_issues_for_a_staff_group)),
    url(R'^add_note_to_issue/$',
        APIAuth(NoteViews.add_note_to_issue)),
    url(R'^delete_note_to_issue/(?P<object_id>[^/]+)/$',
        APIAuth(NoteViews.delete_note_to_issue)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/note_notifications_home_page/$',
        SchoolAuth(NoteViews.note_notifications_home_page)),
)

# Manage Favourites
urlpatterns+= patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AddFavourite/(?P<GroupName>[^/]+)/$',SchoolAuth(FavouritesViews.AddFavourite)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/removeFavourites/(?P<GroupId>[^/]+)/(?P<StaffId>[^/]+)/$',SchoolAuth(FavouritesViews.RemoveFavourite)),

)

# Manage Grade Input
urlpatterns+= patterns('',
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/GradeInput/Group/(?P<GroupName>[^/]+)/(?P<GradeRecord>[^/]+)/(?P<SubjectName>[^/]+)/$',SchoolAuth(GradeViews.GroupGradeInput)),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/GradeInputIntermediate/Group/(?P<GroupName>[^/]+)/$',SchoolAuth(GradeViews.GroupGradeInputIntermediateCode)),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/GradeInput/Pupil/(?P<PupilId>\d+)/(?P<GradeRecord>[^/]+)/$',SchoolAuth(GradeViews.PupilGradeInput)),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/UpdateGrade/Pupil/(?P<PupilId>\d+)/(?P<GradeRecord>[^/]+)/(?P<SubjectName>[^/]+)/$',APIAuth(GradeViews.UpdateGrade)),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/UpdateGrade/Group/(?P<PupilId>\d+)/(?P<GradeRecord>[^/]+)/(?P<GroupName>[^/]+)/(?P<SubjectName>[^/]+)/$',APIAuth(GradeViews.UpdateGrade)),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/Grade/(?P<GroupName>[^/]+)/$',SchoolAuth(GradeViews.GroupGradeSelectPage)),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/GradeInput/Pupil/Intermediate/$',SchoolAuth(GradeViews.PupilGradeInputIntermediate)),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/GradeInput/Pupil/IntermediateFromRec/(?P<PupilNo>\d+)/$',SchoolAuth(GradeViews.PupilGradeInputIntermediateFromRec)),
)

# Manage Global Variables
urlpatterns+= patterns('',
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditSystemVariable/$',SchoolAuth(GlobalVariablesViews.GlobalVariablesManager),{'variableType':'system'}),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditYearlyVariable/$',SchoolAuth(GlobalVariablesViews.GlobalVariablesManager),{'variableType':'yearly'}),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditGlobalVariable/$',SchoolAuth(GlobalVariablesViews.EditGlobalVariablesManager)),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ManageTermDates/$',SchoolAuth(GlobalVariablesViews.ManageYearDates)),
   url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyTermDates/(?P<Term>[^/]+)/$',SchoolAuth(GlobalVariablesViews.ModifyYearDates)),

)

# Manage other Schools
urlpatterns+= patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pastSchools/$',SchoolAuth(SchoolsViews.pastSchools)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pastSchoolsEdit/(?P<SchoolId>[^/]+)/$',SchoolAuth(SchoolsViews.pastSchoolsEdit)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pastSchoolsNew/$',SchoolAuth(SchoolsViews.pastSchoolsNew)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pastSchoolsNewFromInterviewPopUp/(?P<GroupName>[^/]+)/$',SchoolAuth(SchoolsViews.pastSchoolsNew),{'fromInterviewPopUp':True}),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pastSchoolsNewFromEditPupilOtherInformationPopUp/(?P<PupilId>[^/]+)/$',SchoolAuth(SchoolsViews.pastSchoolsNew),{'EditPupilOtherInformationPopUp':True}),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pastSchoolsDelete/(?P<SchoolId>[^/]+)/$',SchoolAuth(SchoolsViews.pastSchoolsDelete)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/futureSchoolsReportCSV/$',SchoolAuth(SchoolsViews.futureSchoolsReportCSV)),
)

# Reports
urlpatterns += patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AdminApplicantsAmtInGrps/$', SchoolAuth(ReportViews.AdminApplicantsAmtInGrps)),
)

# Pupil SEN and EAL records
urlpatterns += patterns('',
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ManageSEN/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.Manage), {'ManageType':'All', 'senView':True}),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilSENRecord/(?P<PupilId>[^/]+)/$', SchoolAuth(SENViews.PupilSENRecord)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifySENSpecialPermissions/(?P<PupilId>[^/]+)/$', SchoolAuth(SENViews.pupilModifySENSpecialPermissions)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifySENAreasOfConcern/(?P<PupilId>[^/]+)/$', SchoolAuth(SENViews.pupilModifySENAreasOfConcern)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AddProvision/(?P<PupilId>[^/]+)/$', SchoolAuth(SENViews.AddProvision)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/DeleteProvision/(?P<PupilId>[^/]+)/(?P<ProvisionId>[^/]+)/$', SchoolAuth(SENViews.DeleteProvision)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/DeleteEalProvision/(?P<PupilId>[^/]+)/(?P<ProvisionId>[^/]+)/$', SchoolAuth(EALViews.DeleteEalProvision)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AddProvision/(?P<PupilId>[^/]+)/$', SchoolAuth(SENViews.AddProvision)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AddEalProvision/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.AddEalProvision)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyProvision/(?P<PupilId>[^/]+)/(?P<ProvisionId>[^/]+)/$', SchoolAuth(SENViews.ModifyProvision)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AddTarget/(?P<PupilId>[^/]+)/$', SchoolAuth(SENViews.AddTarget)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AddEalTarget/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.AddEalTarget)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyEalProvision/(?P<PupilId>[^/]+)/(?P<ProvisionId>[^/]+)/$', SchoolAuth(EALViews.ModifyEalProvision)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/DeleteTarget/(?P<PupilId>[^/]+)/(?P<ProvisionId>[^/]+)/$', SchoolAuth(SENViews.DeleteTarget)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyTarget/(?P<PupilId>[^/]+)/(?P<ProvisionId>[^/]+)/$', SchoolAuth(SENViews.ModifyTarget)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/DeleteEalTarget/(?P<PupilId>[^/]+)/(?P<ProvisionId>[^/]+)/$', SchoolAuth(EALViews.DeleteEalTarget)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyEalTarget/(?P<PupilId>[^/]+)/(?P<ProvisionId>[^/]+)/$', SchoolAuth(EALViews.ModifyEalTarget)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ViewSenProvisionMap/(?P<PupilId>[^/]+)/(?P<mode>[^/]+)/$', SchoolAuth(SENViews.ViewSenProvisionMap)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ModifyAssessmentHistory/(?P<PupilId>[^/]+)/$', SchoolAuth(SENViews.addAssessmentHistory)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifyEALSpecialPermissions/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.pupilModifyEALSpecialPermissions)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifyEALAssessmentHistory/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.pupilModifyEALAssessmentHistory)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifyEALGeneralNotes/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.pupilModifyEALGeneralNotes)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifyEalAreasOfConcern/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.pupilModifyEalAreasOfConcern)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilDeleteEalAreasOfConcern/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.pupilModifyEalAreasOfConcern), {'delete':True}),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifyEalLanguageData/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.pupilModifyEalLanguageData)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilDeleteEalLanguageData/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.pupilModifyEalLanguageData), {'delete':True}),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilModifyEalFurtherLanguageDetails/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.pupilModifyEalFurtherLanguageDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ManageEAL/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.Manage), {'ManageType':'All', 'ealView':True}),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilEALRecord/(?P<PupilId>[^/]+)/$', SchoolAuth(EALViews.PupilEALRecord)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/ViewEalProvisionMap/(?P<PupilId>[^/]+)/(?P<mode>[^/]+)/$', SchoolAuth(EALViews.ViewEalProvisionMap)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/modifyExtraOptions/(?P<PupilId>[^/]+)/$', SchoolAuth(SENViews.modifyExtraOptions)),

    # APIs
    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/view_sen_details_for_a_group/(?P<pupil_group>[^/]+)/$',
        SchoolAuth(SENViews.view_sen_details_for_a_group)),
    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/view_most_able_details_for_a_group/(?P<pupil_group>[^/]+)/$',
        SchoolAuth(SENViews.view_most_able_details_for_a_group)),
)

# Staff
urlpatterns += patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/resetStaffPassword/(?P<StaffNo>\d+)/$', SchoolAuth(views.resetStaffPassword), {'fromStaffRecord': True}),
    url(r'^(?P<school>[^/]+)/newPasswordAfterReset/(?P<StaffNo>\d+)/$', SchoolAuth(resetPasswordViews.newPasswordAfterReset)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/newStaffPage/$', SchoolAuth(staffViews.newStaffPage)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/saveStaffDetails/$', SchoolAuth(staffViews.saveStaffDetails)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/saveStaffGroups/(?P<StaffNo>\d+)/$', SchoolAuth(staffViews.saveStaffGroups)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/modifyStaffGroupsFromStaffPage/(?P<StaffNo>\d+)/$', SchoolAuth(staffViews.saveStaffGroups), {'fromStaffPage': True}),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/EditStaffDetails/(?P<StaffNo>\d+)/(?P<mode>[^/]+)/$', SchoolAuth(staffViews.EditStaffDetails)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffPageDetailsWrong/$', SchoolAuth(emailViews.emailItSupport)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffDeactivate/(?P<StaffNo>\d+)/$', SchoolAuth(staffViews.staffDeactivate)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffActivate/(?P<StaffNo>\d+)/$', SchoolAuth(staffViews.staffActivate)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffSearchPage/$', SchoolAuth(staffViews.staffSearchPage)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffSearch/$', SchoolAuth(staffViews.staffSearch)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffCpd/(?P<StaffId>\d+)/$', SchoolAuth(StaffCpdViews.viewStaffCpdRecords)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/addStaffCpd/(?P<StaffId>\d+)/$', SchoolAuth(StaffCpdViews.addStaffCpdRecord)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/modifyStaffCpd/(?P<StaffId>\d+)/$', SchoolAuth(StaffCpdViews.addStaffCpdRecord), {'modify': True}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/deleteStaffCpd/(?P<StaffId>\d+)/$', SchoolAuth(StaffCpdViews.deleteStaffCpdRecord)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffModifyMedicalDetails/(?P<StaffNo>\d+)/$', SchoolAuth(staffViews.staffModifyMedicalDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffAddContactDetails/(?P<StaffNo>\d+)/$', SchoolAuth(staffViews.staffAddContactDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffModifyContactDetails/(?P<StaffNo>\d+)/(?P<contactId>\d+)/$', SchoolAuth(staffViews.staffModifyContactDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffDeleteContactDetails/(?P<StaffNo>\d+)/(?P<contactId>\d+)/$', SchoolAuth(staffViews.staffDeleteContactDetails)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/staffCpdCsvExtractPopUp/$', SchoolAuth(staffViews.staffCpdCsvExtract)),
    url(R'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/view_staff_groups_for_a_school_as_a_list/$',
        SchoolAuth(staffViews.view_staff_groups_for_a_school_as_a_list)),
)

# Gifted And Talented giftedAndTalentedId
urlpatterns += patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilGiftedAndTalented/(?P<PupilId>[^/]+)/$', SchoolAuth(GTViews.PupilGTRecord)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/addNewGiftedAndTalented/(?P<PupilId>[^/]+)/$', SchoolAuth(GTViews.addNewGT)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/modifyGiftedAndTalented/(?P<PupilId>[^/]+)/(?P<giftedAndTalentedId>[^/]+)/$', SchoolAuth(GTViews.modifyGT)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/deleteGiftedAndTalented/(?P<PupilId>[^/]+)/(?P<giftedAndTalentedId>[^/]+)/$', SchoolAuth(GTViews.deleteGT)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/MostAbled/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.Manage), {'ManageType':'All', 'gtView':True}),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/add_most_abled_target/(?P<PupilId>[^/]+)/$', SchoolAuth(GTViews.add_target)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/delete_most_abled_target/(?P<PupilId>[^/]+)/(?P<target_id>[^/]+)/$', SchoolAuth(GTViews.close_target)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/modify_most_abled_target/(?P<PupilId>[^/]+)/(?P<target_id>[^/]+)/$', SchoolAuth(GTViews.modify_target)),
)

# more abled
urlpatterns += patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/addModifyMoreAbled/(?P<PupilId>[^/]+)/$', SchoolAuth(moreAbledViews.addOrModifyMoreAbledAlert)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/MoreAbled/(?P<GroupName>[^/]+)/$',SchoolAuth(ManagerViews.Manage), {'ManageType':'All', 'moreAbledView':True}),
)

# email
urlpatterns += patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/emailTest/$', SchoolAuth(emailViews.testEmail)),
)


# Manage group Teacher Names
urlpatterns += patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/SetTeacherName/(?P<GroupName>[^/]+)/$', SchoolAuth(ManagerViews.SetTeachers)),
)

# Add Documents to Pupils
urlpatterns += patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilAddDocument/(?P<PupilId>[^/]+)/$', SchoolAuth(documentServerViews.pupilAddDocument)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/pupilRemoveDocument/(?P<PupilId>[^/]+)/(?P<DocId>[^/]+)/$', SchoolAuth(documentServerViews.pupilRemoveDocument)),
)

# Potential Groups
urlpatterns += patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PotentialGroup/ChooseClassList/(?P<NewAcYear>[^/]+)/$', SchoolAuth(PotentialGroupsViews.ChooseClasslists)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PotentialGroup/MultiClassListGen/(?P<NewAcYear>[^/]+)/$', SchoolAuth(PotentialGroupsViews.MultiClassListsGen)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PotentialGroup/MoveClass/(?P<GroupName>[^/]+)/(?P<NewAcYear>[^/]+)/$', SchoolAuth(PotentialGroupsViews.MoveClass)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PotentialGroup/AssignHouse/(?P<GroupName>[^/]+)/(?P<NewAcYear>[^/]+)/$', SchoolAuth(PotentialGroupsViews.AssignHouse)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PotentialGroup/ModifyTeacherVars/(?P<GroupName>[^/]+)/(?P<NewAcYear>[^/]+)/$', SchoolAuth(PotentialGroupsViews.ModifyTeacherVariables)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PotentialGroup/ClassList/(?P<GroupName>[^/]+)/(?P<NewAcYear>[^/]+)/$', SchoolAuth(PotentialGroupsViews.ClassList)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PotentialGroup/csv/(?P<GroupName>[^/]+)/(?P<NewAcYear>[^/]+)/$', SchoolAuth(PotentialGroupsViews.PotentialCSV)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PotentialGroup/GenerateEPIF/(?P<GroupName>[^/]+)/(?P<NewAcYear>[^/]+)/$', SchoolAuth(ReportsToParentsViews.EPIF_Report)),
    url(R'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PotentialGroup/GenerateLabels/(?P<PotentialGroup>[^/]+)/$',SchoolAuth(LetterViews.GenerateLetterLabels)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/PotentialGroup/(?P<GroupName>[^/]+)/(?P<NewAcYear>[^/]+)/$', SchoolAuth(PotentialGroupsViews.Manage)),

)

# Analysis Report
urlpatterns += patterns('',
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AnalysisReports/(?P<GroupName>[^/]+)/$', SchoolAuth(AnalysisReportViews.AnalysisReportsView)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/APSAverage/(?P<GroupName>[^/]+)/(?P<PreviousRecord>[^/]+)/(?P<CurrentRecord>[^/]+)/(?P<SubjectName>[^/]+)/$', SchoolAuth(AnalysisReportViews.APSAverage)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/APSAverageRequest/(?P<SubjectName>[^/]+)/$', SchoolAuth(AnalysisReportViews.APSAverageRequest)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/LevelsReview/(?P<GroupName>[^/]+)/(?P<NoYears>[^/]+)/(?P<SubjectName>[^/]+)/$', SchoolAuth(AnalysisReportViews.LevelsReview)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/APSPupilProgress/(?P<GroupName>[^/]+)/(?P<SubjectName>[^/]+)/$', SchoolAuth(AnalysisReportViews.APSPupilProgress)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/AnalysisReportsList/(?P<GroupName>[^/]+)/(?P<Term>[^/]+)/$', SchoolAuth(AnalysisReportViews.AnalysisReportList)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/RawGradeExtract/(?P<GroupName>[^/]+)/$', SchoolAuth(ReportViews.return_raw_grade_data_csv)),
    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/whole_school_numbers_report/$', SchoolAuth(AnalysisReportViews.whole_school_numbers_report)),

)

# Special Groups
urlpatterns += patterns('',
    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/special_group_admin_page/$',
        SchoolAuth(SpecialGroupsViews.special_group_admin_page)),
    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/create_new_special_group/$',
        SchoolAuth(SpecialGroupsViews.create_new_special_group)),
    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/special_groups_add_pupils/(?P<group_name>[^/]+)/$',
        SchoolAuth(SpecialGroupsViews.add_pupils_to_special_group)),
    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/special_groups_remove_pupils/(?P<group_name>[^/]+)/$',
        SchoolAuth(SpecialGroupsViews.remove_pupils_to_special_group)),
    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/special_groups_deactivate/$',
        SchoolAuth(SpecialGroupsViews.deactivate_special_group)),
    url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/view_special_groups/$',
        SchoolAuth(SpecialGroupsViews.view_special_groups)),
    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/special_groups_remove_all_members/$',
        SchoolAuth(SpecialGroupsViews.deactivate_special_group)),
    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/special_groups_delete_group/$',
        SchoolAuth(SpecialGroupsViews.special_groups_delete_group)),
)

# mass upload of pupil pictures
urlpatterns += patterns('',
                        url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/mass_upload_of_pupil_pictures_home/$',
                            SchoolAuth(PupilViews.mass_upload_of_pupil_pictures_home)),
                        url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/mass_upload_of_pupil_pictures/$',
                            SchoolAuth(PupilViews.mass_upload_of_pupil_pictures)),
                        )

#
#
#
#
#
#

# Forum Views
#
#
#
#
urlpatterns+= patterns('',
   url(R'^forum/$',SchoolAuth(Forum.views.ListForums)),
   url(R'^forum/threads/(\d+)/$',SchoolAuth(Forum.views.ListThreads)),
   url(R'^forum/posts/(\d+)/$',SchoolAuth(Forum.views.ListPosts)),
   url(R'^forum/addThread/(\d+)/$',SchoolAuth(Forum.views.AddThread)),
   url(R'^forum/addPost/(\d+)/$',SchoolAuth(Forum.views.AddPost)),
   url(R'^forum/Search/$',SchoolAuth(Forum.views.SearchPosts)),
)

# Grade input fields
urlpatterns += grade_input_field_urls()

# Grade API
urlpatterns += grade_api_urls()

# AnalysisStore API
urlpatterns += AnalysisStore_urls()

# Static files system URL
urlpatterns += staticfiles_urlpatterns()

# StaffAttendance URLs
urlpatterns += StaffAttendanceUrls.staff_attendance_urls()

# General API urls

urlpatterns += General_urls()

# MarkBook API urls

urlpatterns += MarkBook_urls()