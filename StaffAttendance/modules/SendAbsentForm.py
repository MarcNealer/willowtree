# Django imports
from django.core.mail import EmailMessage

# Database imports
from MIS.models import Staff, User, SystemVarData
from StaffAttendance.models import DailyAttendanceStaff, AttendanceCodesStaff


class SendAbsentForm():

    def __get_all_absent_today(self):
        """
        Collects all staff that were marked as not here today.
        """
