# Python imports
import datetime

# Django imports
from django.core.mail import EmailMessage
from django.db.models import Q
from django.template.loader import get_template
from django.template import Context

# Database imports
from MIS.models import Staff, User, SystemVarData
from StaffAttendance.models import DailyAttendanceStaff, AttendanceCodesStaff

import logging
log = logging.getLogger(__name__)

class SaveStaffAttendance():
    """
    This is used to save all staff attendance records
    """
    def __init__(self,
                 request,
                 staff_id,
                 attendance_code,
                 am_pm,
                 date_attendance_taken=False,
                 reason_for_absence=False):
        self.request = request
        self.staff_id = staff_id
        self.attendance_code = attendance_code
        self.attendance_code_object = AttendanceCodesStaff.objects.get(attendance_code=self.attendance_code)
        self.am_pm = am_pm
        if date_attendance_taken:
            self.date_attendance_taken = date_attendance_taken
        else:
            self.date_attendance_taken = ''
        if reason_for_absence:
            self.reason_for_absence = reason_for_absence
        else:
            self.reason_for_absence = ''
        self.staff_using_class = Staff.objects.get(id=User.objects.get(id=request.session['_auth_user_id']).staff.id)

    def save_record(self):
        """
        Creates a new staff attendance record.
        """
        try:
            # check for old attendance records de-activating them if needed
            self.__check_for_old_records__()

            staff_attendance_record = DailyAttendanceStaff(staff=Staff.objects.get(id=self.staff_id),
                                                           created_by=self.staff_using_class,
                                                           attendance_code=self.attendance_code_object,
                                                           am_pm=self.am_pm)

            # testing if attendance record is for today or is a past/future attendance
            if self.date_attendance_taken:
                staff_attendance_record.date_attendance_taken = self.date_attendance_taken
            else:
                staff_attendance_record.date_attendance_taken = datetime.datetime.today().date().isoformat()

            # add reason for absence
            if self.reason_for_absence:
                staff_attendance_record.reason_for_absence = self.reason_for_absence

            # save record
            staff_attendance_record.save()

            # checks to see if an absence form is to be sent
            # self.__send_absence_form__()

            return "Staff Attendance Successfully taken for %s %s" % (Staff.objects.get(id=self.staff_id).Forename,
                                                                      Staff.objects.get(id=self.staff_id).Surname)
        except Exception as error:
            return error

    def __check_for_old_records__(self):
        """
        Checks for old records matching the date for a new record to be made. Any found are deactivated.
        """
        if self.date_attendance_taken:
            test_date = self.date_attendance_taken
        else:
            test_date = datetime.datetime.today().date().isoformat()
        if DailyAttendanceStaff.objects.filter(staff__id=self.staff_id,
                                               date_attendance_taken=test_date,
                                               am_pm=self.am_pm,
                                               active=True).exists():
            for i in DailyAttendanceStaff.objects.filter(staff__id=self.staff_id,
                                                         date_attendance_taken=test_date,
                                                         am_pm=self.am_pm,
                                                         active=True):
                temp = i
                temp.active = False
                temp.deactivated_by = self.staff_using_class
                temp.deactivated_date = datetime.datetime.today().date().isoformat()
                temp.save()

    def __render_html__(self, html):
        """
        Render html for report.
        """
        template = get_template(html)
        context = Context({'school': 'school'})
        rendered_html = template.render(context)
        return rendered_html

    def __send_absence_form__(self):
        """
        An absence form is emailed to a staff member if a staff member is recorded absent but was present the day before
        """
        try:
            if self.attendance_code != '/':
                if self.date_attendance_taken:
                    date_absent_from = self.date_attendance_taken
                    test_date = datetime.datetime.strptime(self.date_attendance_taken, "%Y-%m-%d")
                    if test_date.weekday() == 0:
                        test_date = test_date - datetime.timedelta(3)
                    else:
                        test_date = test_date - datetime.timedelta(1)
                    test_date = test_date.date().isoformat()
                else:
                    date_absent_from = datetime.datetime.now().date().isoformat()
                    test_date = datetime.datetime.today()
                    if test_date.weekday() == 0:
                        test_date = test_date - datetime.timedelta(3)
                    else:
                        test_date = test_date - datetime.timedelta(1)
                    test_date = test_date.date().isoformat()

                # check am or pm. If am check for yesterdays pm code. If pm check for today's am code
                if self.am_pm == 'AM':
                    test_am_pm = 'PM'
                    final_test_date = test_date
                else:
                    test_am_pm = 'AM'
                    final_test_date = date_absent_from

                # checks if staff member was marked as here yesterday. If so absence form is sent
                test_1 = DailyAttendanceStaff.objects.filter(attendance_code__attendance_code='/',
                                                             staff__id=self.staff_id,
                                                             date_attendance_taken=final_test_date,
                                                             am_pm=test_am_pm,
                                                             active=True).exists()

                # checks if staff member has any attendance set at all. If not absence form is sent
                test_2 = DailyAttendanceStaff.objects.filter(staff__id=self.staff_id,
                                                             date_attendance_taken=final_test_date,
                                                             am_pm=test_am_pm,
                                                             active=True).exists()

                if test_1 or not test_2:

                    message = '''
This  is a message from WillowTree.

You have been marked absent from school starting on the following date: %s. Please print and complete the attached
absence form and ensure that it is signed by the appropriate Head of School or your line manager. The form should
then be handed in to the School Office.

Thank you.
''' % date_absent_from

                    msg = EmailMessage('Absence Request Form',
                                       message,
                                       SystemVarData.objects.get(Variable__Name='WillowTree Email Address').Data,
                                       [Staff.objects.get(id=self.staff_id).EmailAddress])
                    #msg.attach('Reports/request_for_absence_form.html', htmlAttachedFile)
                    msg.attach("request_for_absence_form.html",
                               self.__render_html__("Reports/request_for_absence_form.html"),
                               "application/html")
                    msg.send()
                    return log.warn('Sending Absence Request Form to staff id: %s' % self.staff_id)
                else:
                    return
        except Exception as error:
            return log.warn('ERROR: failed to send Absence Request Form to staff id: %s due to %s' % (self.staff_id,
                                                                                                      error))