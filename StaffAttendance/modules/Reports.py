# Python imports
import datetime

# Django imports

# Database imports
from MIS.models import Staff, StaffGroup
from StaffAttendance.models import DailyAttendanceStaff


class StaffAttendanceReporting():
    def __init__(self, date_given_by_user, staff_group, ac_year):
        self.ac_year = ac_year
        self.date = date_given_by_user
        self.staff_group = staff_group
        self.date_grid = self.__get_date_grid__()
        self.staff_objects = self.__get_staff_for_a_staff_group__()
        self.title = 'Weekly Attendance Report for %s (%s to %s)' % (self.staff_group,
                                                                     self.date_grid[0].date().isoformat(),
                                                                     self.date_grid[-1].date().isoformat())

    def __get_date_grid__(self):
        """
        From a start date given this method builds a date grid with week start to friday where the will week end.
        """
        return_grid = []
        dt = datetime.datetime.strptime(self.date, "%Y-%m-%d")
        start_date = dt - datetime.timedelta(days=dt.weekday())
        return_grid.append(start_date)
        for i in range(1, 5):
            return_grid.append(start_date + datetime.timedelta(days=i))
        return return_grid

    def __get_staff_for_a_staff_group__(self):
        """
        Returns distinct staff object ids for a staff group given.
        """
        return StaffGroup.objects.filter(Group__Name__contains=self.staff_group,
                                         AcademicYear=self.ac_year).order_by('Staff__Surname').distinct('Staff__Surname',
                                                                                                        'Staff__id').values_list('Staff__id',
                                                                                                                                 flat=True)

    def plugs_staff_attendance_data_into_data_grid(self):
        """
        Plugs staff attendance data into the data grid.
        """
        return_data = []
        for staff_id in self.staff_objects:
            if Staff.objects.get(id=staff_id).Active:
                staff_attendance_data_placeholder = [Staff.objects.get(id=staff_id).FullName()]
                for date in self.date_grid:
                    # AM test
                    am_test = DailyAttendanceStaff.objects.filter(staff__id=staff_id,
                                                                  am_pm='AM',
                                                                  date_attendance_taken=date,
                                                                  active=True)
                    if am_test.exists() and len(am_test) == 1:
                        am_object = DailyAttendanceStaff.objects.get(staff__id=staff_id,
                                                                     am_pm='AM',
                                                                     date_attendance_taken=date,
                                                                     active=True)
                        staff_attendance_data_placeholder.append(am_object.attendance_code.attendance_code)
                    else:
                        staff_attendance_data_placeholder.append('-')

                    # PM test
                    pm_test = DailyAttendanceStaff.objects.filter(staff__id=staff_id,
                                                                  am_pm='PM',
                                                                  date_attendance_taken=date,
                                                                  active=True)
                    if pm_test.exists() and len(pm_test) == 1:
                        pm_object = DailyAttendanceStaff.objects.get(staff__id=staff_id,
                                                                     am_pm='PM',
                                                                     date_attendance_taken=date,
                                                                     active=True)
                        staff_attendance_data_placeholder.append(pm_object.attendance_code.attendance_code)
                    else:
                        staff_attendance_data_placeholder.append('-')
                return_data.append(staff_attendance_data_placeholder)
        return return_data