# -*- coding: utf-8 -*-
"""
Created on Fri Jan 18 19:14:56 2013

@author: dbmgr
"""
# myapp/api.py
from tastypie.resources import ModelResource, Resource
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from MIS.models import *
from StaffAttendance.models import *
from tastypie import fields
from tastypie.authentication import BasicAuthentication
from tastypie.cache import SimpleCache
from django.conf.urls import url
from tastypie.utils import trailing_slash


AcYear=GlobalVariables.Get_SystemVar('CurrentYear')

# for groupCallFive api
from MIS.modules.GroupCall.groupCallFiveScripts import GroupCallDataExtractAmended
import urlparse
from tastypie.serializers import Serializer


class Staff2Resource(ModelResource):
    attendanceRec = fields.ToManyField('StaffAttendance.StaffAttendanceApi.DailyAttendanceStaffResource', 'attendanceRec')
    class Meta:
        queryset=Staff.objects.all()
        authentication = BasicAuthentication()
        allowed_methods = ['get']

class AttendanceCodesStaffResource(ModelResource):
    class Meta:
        queryset= AttendanceCodeStaff.objects.all()
        authentication = BasicAuthentication()
        allowed_methods = ['get']


class DailyAttendanceStaffResource(ModelResource):
    Staff = fields.ToOneField(Staff2Resource, 'Staff')

    class Meta:
        queryset= DailyAttendanceStaff.objects.all()
        authentication = BasicAuthentication()
        allowed_methods = ['get']
