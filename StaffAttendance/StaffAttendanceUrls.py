# Django imports
from django.conf.urls import patterns, url

# Staff attendance imports
import views

# WillowTree imports
from MIS.ViewExtras import SchoolAuth
from MIS.ViewExtras import APIAuth


def staff_attendance_urls():
    return patterns('',
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/home_page/$',
                        SchoolAuth(views.home_page, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/home/$',
                        SchoolAuth(views.home, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/(?P<staff_group>[^/]+)/staff_attendance/take_attendance_page/$',
                        SchoolAuth(views.take_attendance_page, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/staff_attendance_codes/$',
                        SchoolAuth(views.staff_attendance_codes, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/get_attendance_data/(?P<staff_group>[^/]+)/(?P<start_date>[^/]+)/(?P<end_date>[^/]+)/$',
                        SchoolAuth(views.past_and_future_attendance_page, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/process_attendance/$',
                        SchoolAuth(views.save_new_staff_attendance_record, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/process_attendance/past_future/$',
                        SchoolAuth(views.save_new_staff_attendance_record, allow_if_admin_menu=True),
                        {'past_future': True}),
                    url(r'^(?P<ac_year>[^/]+)/staff_attendance/weekly_report/$',
                        APIAuth(views.weekly_staff_attendance_report)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/generate_weekly_staff_attendance_report/$',
                        SchoolAuth(views.generate_weekly_staff_attendance_report, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/create_staff_attendance_rule/$',
                        SchoolAuth(views.create_staff_attendance_rule, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/edit_staff_attendance_rule/$',
                        SchoolAuth(views.edit_staff_attendance_rule, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/remove_staff_attendance_rule/(?P<rule_id>[^/]+)/$',
                        SchoolAuth(views.remove_staff_attendance_rule, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/view_active_staff_attendance_rules/$',
                        SchoolAuth(views.view_active_staff_attendance_rules, allow_if_admin_menu=True)),
                    url(r'^(?P<school>[^/]+)/(?P<ac_year>[^/]+)/staff_attendance/get_staff_user_names/(?P<staff_group>[^/]+)/$',
                        SchoolAuth(views.get_staff_user_names, allow_if_admin_menu=True)),
                    )