# Django imports
from django.shortcuts import HttpResponse
from django.http import HttpResponseBadRequest
from django.core import serializers
from django.shortcuts import render_to_response
from django.template import RequestContext

# Database imports
from MIS.ViewExtras import SetMenu
from MIS.models import User, Staff, Group, StaffGroup, MenuTypes
from StaffAttendance.models import *

# StaffAttendance imports
from StaffAttendance.modules.SaveStaffAttendance import SaveStaffAttendance
from StaffAttendance.modules.Reports import StaffAttendanceReporting

# Python imports
import json
import datetime

# Logging
import logging
log = logging.getLogger(__name__)


def home_page(request, school, ac_year):
    """
    Displays home page from the staff attendance app.
    """
    c = {'school': school,
         'AcYear': ac_year}
    con_inst = RequestContext(request, processors=[SetMenu])
    return render_to_response('staff_attendance_home.html', c,
                              context_instance=con_inst)


def home(request, school, ac_year):
    """
    Displays the home screen for StaffAttendance. This will display all the staff groups for their school based upon the
    users default menu. If the user has admin access to other schools then they will also be able to view staff groups
    from these schools from this page.
    """
    try:
        staff_object = Staff.objects.get(id=User.objects.get(id=request.session['_auth_user_id']).staff.id)
        staff_default_menu = staff_object.DefaultMenu.Name.split('Admin')[0]

        # gets staff groups based upon the users default menu
        staff_groups = Group.objects.filter(Name__icontains="Staff.%s" % staff_default_menu)
        staff_groups = list(staff_groups.filter(Name__icontains="_Staff_Register").values_list("Name", flat=True))

        # gets more staff groups based upon the users permissions
        for school in ['Battersea', 'Clapham', 'Fulham', 'Kensington']:
            if staff_object.User.has_perm('MIS.%sAdmin' % school) and school not in staff_default_menu:
                other_staff_groups = Group.objects.filter(Name__icontains="Staff.%s" % school)
                other_staff_groups = list(other_staff_groups.filter(Name__icontains="_Staff_Register").values_list("Name", flat=True))
                staff_groups += other_staff_groups

        # return only staff groups needed for attendance (length of three)
        staff_groups_final = []
        for i in staff_groups:
            if len(i.split('.')) == 3:
                staff_groups_final.append(i)

        return HttpResponse(json.dumps(staff_groups_final), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def take_attendance_page(request, school, ac_year, staff_group):
    """
    Compiles data for the take_attendance_page.html
    """
    try:
        data = list()
        today = datetime.datetime.today().date().isoformat()
        staff_groups = Staff.objects.filter(staffgroup__Group__Name__icontains=staff_group)
        staff_groups = staff_groups.order_by('Surname', 'Forename').distinct('Surname', 'Forename')
        for i in staff_groups:
            today_date = datetime.datetime.today().date().isoformat()

            # get all staff ids for this school.
            school_name = MenuTypes.objects.get(Name=school).SchoolId.Name
            staff_ids = Staff.objects.filter(DefaultMenu__Name__icontains=school_name,
                                             Active=True).values_list("id", flat=True)

            # check for rules
            if i.User:
                rule_am = StaffAttendanceFutureRules.objects.filter(user_names__icontains=str(i.User.username),
                                                                    start_date__lte=today_date,
                                                                    stop_date__gte=today_date,
                                                                    days__icontains=datetime.datetime.now().strftime("%A"),
                                                                    am_pm__icontains="AM",
                                                                    created_by__id__in=staff_ids,
                                                                    active=True)
                if rule_am.exists():
                    am_rule = rule_am.order_by("-date_rule_created")[0].attendance_code
                else:
                    am_rule = False
                rule_pm = StaffAttendanceFutureRules.objects.filter(user_names__icontains=str(i.User.username),
                                                                    start_date__lte=today_date,
                                                                    stop_date__gte=today_date,
                                                                    days__icontains=datetime.datetime.now().strftime("%A"),
                                                                    am_pm__icontains="PM",
                                                                    created_by__id__in=staff_ids,
                                                                    active=True)
                if rule_pm.exists():
                    pm_rule = rule_pm.order_by("-date_rule_created")[0].attendance_code
                else:
                    pm_rule = False
            else:
                am_rule = False
                pm_rule = False

            # check if attendance taken for the day
            if DailyAttendanceStaff.objects.filter(staff__id=i.id,
                                                   date_attendance_taken=today,
                                                   am_pm="AM").exists():
                am = True
            else:
                am = False
            if DailyAttendanceStaff.objects.filter(staff__id=i.id,
                                                   date_attendance_taken=today,
                                                   am_pm="PM").exists():
                pm = True
            else:
                pm = False

            # appending data
            data.append({'staff_id': i.id,
                         'staff_name': i.FullName(),
                         'am_attendance_taken': am,
                         'pm_attendance_taken': pm,
                         'am_rule': am_rule,
                         'pm_rule': pm_rule})

        return HttpResponse(json.dumps(data), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def staff_attendance_codes(request, school, ac_year):
    """
    Gets staff attendance codes.
    """
    try:
        return_data = serializers.serialize('json', AttendanceCodesStaff.objects.all(),
                                            indent=2,
                                            use_natural_keys=True)
        return HttpResponse(return_data, 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def past_and_future_attendance_page(request, school, ac_year, staff_group, start_date, end_date):
    """
    Gets attendance data for staff members of a staff group for a given date range.
    """
    try:
        # gets staff ids for a staff group
        staff_ids = Staff.objects.filter(staffgroup__Group__Name__icontains=staff_group).order_by('Surname').distinct('Surname',
                                                                                                                      'id').values_list('id', flat=True)

        # gets past/future attendance data
        return_data = []
        for staff_id in staff_ids:
            staff_record = Staff.objects.get(id=staff_id)
            temp_data = {'staff': {'staff_id': staff_id,
                                   'staff_full_name': "%s %s" % (staff_record.Forename, staff_record.Surname)}}
            attendance_data = DailyAttendanceStaff.objects.filter(staff__id=staff_id,
                                                                  date_attendance_taken__range=(start_date, end_date),
                                                                  active=True).order_by('staff__id',
                                                                                        'date_attendance_taken',
                                                                                        'am_pm')
            attendance_data_temp = []
            for i in attendance_data:
                attendance_data_temp.append({'am_pm': i.am_pm,
                                             'attendance_code': i.attendance_code.attendance_code,
                                             'date_attendance_taken': i.date_attendance_taken.isoformat()})
            temp_data.update({'attendance': attendance_data_temp})
            return_data.append(temp_data)

        # Data to JSON then send
        return HttpResponse(json.dumps(return_data), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def save_new_staff_attendance_record(request, school, ac_year, past_future=False):
    """
    This is used to save all staff attendance records
    """
    # collect messages from save staff attendance object
    messages_from_save_staff_attendance_operations = list()
    successfull_staff_attendance_taken_count = 0

    # check how many new records are to be created
    number_of_requests = list()
    for k, v in request.POST.iteritems():
        if 'staff_id' in k:
            number_of_requests.append(k.strip('staff_id'))

    # create new attendance records
    if past_future:
        for i in number_of_requests:
            save_staff_attendance_object = SaveStaffAttendance(request,
                                                               staff_id=request.POST['staff_id%s' % i],
                                                               attendance_code=request.POST['new_code%s' % i],
                                                               am_pm=request.POST['am_pm%s' % i],
                                                               date_attendance_taken=request.POST['date%s' % i])
            message_from_object = save_staff_attendance_object.save_record()
            if "Staff Attendance Successfully" in message_from_object:
                successfull_staff_attendance_taken_count += 1
            else:
                messages_from_save_staff_attendance_operations.append(message_from_object)

    else:
        test_date = datetime.datetime.today().date().isoformat()
        for i in number_of_requests:
            # check if a past or future attendance is already set
            if not DailyAttendanceStaff.objects.filter(staff__id=request.POST['staff_id%s' % i],
                                                       date_attendance_taken=test_date,
                                                       am_pm=request.POST['am_pm%s' % i],
                                                       active=True).exists():

                # if no past or future attendance is set
                save_staff_attendance_object = SaveStaffAttendance(request,
                                                                   staff_id=request.POST['staff_id%s' % i],
                                                                   attendance_code=request.POST['new_code%s' % i],
                                                                   am_pm=request.POST['am_pm%s' % i])
                message_from_object = save_staff_attendance_object.save_record()
                if "Staff Attendance Successfully" in message_from_object:
                    successfull_staff_attendance_taken_count += 1
                else:
                    messages_from_save_staff_attendance_operations.append(message_from_object)
            else:
                staff_object = Staff.objects.get(id=request.POST['staff_id%s' % i])
                error_message_from_object = "<font color=red>Error: Attendance already taken for %s %s</font>" % (staff_object.Forename,
                                                                                                                  staff_object.Surname)
                messages_from_save_staff_attendance_operations.append(error_message_from_object)

    # send messages back to frontend
    messages_from_save_staff_attendance_operations.append("%s staff attendance records successfully saved" % str(successfull_staff_attendance_taken_count))
    return HttpResponse(json.dumps(messages_from_save_staff_attendance_operations), 'application/json')


def generate_weekly_staff_attendance_report(request, school, ac_year):
    """
    Shows the page for generating the weekly staff attendance report from the weekly_staff_attendance_report view below.
    """
    school_name = MenuTypes.objects.get(Name=school).SchoolId.Name
    c = {'school': school,
         'ac_year': ac_year,
         'staff_groups': Group.objects.filter(Name__contains='Staff.%s' % school_name),
         'start_date': datetime.datetime.now().date().isoformat()}
    return render_to_response('generate_weekly_staff_attendance_report.html',
                              c,
                              context_instance=RequestContext(request, processors=[SetMenu]))


def weekly_staff_attendance_report(request, ac_year):
    """
    Generates the weekly staff attendance report.
    """
    c = {'report': StaffAttendanceReporting(request.POST['date_given'],
                                            request.POST['staff_group'],
                                            ac_year),
         'attendance_codes': AttendanceCodesStaff.objects.all()}
    return render_to_response('Reports/weekly_staff_attendance_report.html',
                              c,
                              context_instance=RequestContext(request))


def create_staff_attendance_rule(request, school, ac_year,):
    """
    create staff attendance rule view.
    """
    try:
        staff_creating_rule = Staff.objects.get(id=User.objects.get(id=request.session['_auth_user_id']).staff.id)
        today_date = datetime.datetime.today().date().isoformat()
        user_names = request.POST['user_names']
        start_date = request.POST['start_date']
        stop_date = request.POST['stop_date']
        days = request.POST['days']
        am_pm = request.POST['am_pm']
        attendance_code = request.POST['attendance_code']
        rule = StaffAttendanceFutureRules(user_names=user_names,
                                          start_date=start_date,
                                          stop_date=stop_date,
                                          days=days,
                                          am_pm=am_pm,
                                          attendance_code=attendance_code,
                                          date_rule_created=today_date,
                                          created_by=staff_creating_rule,
                                          modified_by=staff_creating_rule)
        rule.save()
        log.warn("SUCCESS: New staff attendance rule (%s) created by (%s)" % (rule.id, staff_creating_rule.id))
        return HttpResponse(json.dumps("SUCCESS: New staff attendance rule created"), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def edit_staff_attendance_rule(request, school, ac_year,):
    """
    edit staff attendance rule view.
    """
    try:
        rule = StaffAttendanceFutureRules.objects.get(id=request.POST['rule_id'])
        staff_creating_rule = Staff.objects.get(id=User.objects.get(id=request.session['_auth_user_id']).staff.id)
        today_date = datetime.datetime.today().date().isoformat()
        if "user_names" in request.POST:
            rule.user_names = request.POST['user_names']
        if "start_date" in request.POST:
            rule.start_date = request.POST['start_date']
        if "stop_date" in request.POST:
            rule.stop_date = request.POST['stop_date']
        if "days" in request.POST:
            rule.days = request.POST['days']
        if "am_pm" in request.POST:
            rule.am_pm = request.POST['am_pm']
        if "attendance_code" in request.POST:
            rule.attendance_code = request.POST['attendance_code']
        rule.date_rule_modified = today_date
        rule.modified_by = staff_creating_rule
        rule.save()
        log.warn("SUCCESS: Staff attendance rule (%s) modified by (%s)" % (rule.id, staff_creating_rule.id))
        return HttpResponse(json.dumps("SUCCESS: Staff attendance rule amended"), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def remove_staff_attendance_rule(request, school, ac_year, rule_id):
    """
    remove_staff_attendance_rule view.
    """
    try:
        staff_creating_rule = Staff.objects.get(id=User.objects.get(id=request.session['_auth_user_id']).staff.id)
        rule = StaffAttendanceFutureRules.objects.get(id=rule_id)
        rule.active = False
        rule.save()
        log.warn("SUCCESS: Staff attendance rule (%s) de-activated by (%s)" % (rule.id, staff_creating_rule.id))
        return HttpResponse(json.dumps("SUCCESS: Staff attendance rule deactivated"), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def view_active_staff_attendance_rules(request, school, ac_year):
    """
    View all active staff attendance records. To be used in the create staff attendance rule page.
    """
    try:
        return_data = serializers.serialize('json', StaffAttendanceFutureRules.objects.filter(active=True),
                                            indent=2,
                                            use_natural_keys=True)
        return HttpResponse(return_data, 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def get_staff_user_names(request, school, ac_year, staff_group):
    """
    Gets all staff user names based upon a group. If "all" is provided as a group all staff are returned.
    """
    try:
        if staff_group.lower() == "all":
            school_name = MenuTypes.objects.get(Name=school).SchoolId.Name
            staff_objects = Staff.objects.filter(staffgroup__Group__Name__icontains="Staff.%s" % school_name,
                                                 Active=True)
            staff_objects = staff_objects.order_by("Surname")
        else:
            staff_objects = Staff.objects.filter(staffgroup__Group__Name__icontains=staff_group,
                                                 Active=True)
            staff_objects = staff_objects.order_by("Surname")
        return_data = serializers.serialize('json', staff_objects,
                                            indent=2,
                                            use_natural_keys=True)
        return HttpResponse(return_data, 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)
