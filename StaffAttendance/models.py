# Django imports
from picklefield.fields import PickledObjectField
from django.db import models
from MIS.models import Staff

# Python imports
import datetime

# Choices
am_pm_choices = (('AM', 'AM'), ('PM', 'PM'))


class AttendanceCodesStaffManager(models.Manager):
    def get_by_natural_key(self, attendance_code):
        return self.get(attendance_code=attendance_code)


class AttendanceCodesStaff(models.Model):
    """
    This holds staff attendance codes.
    """
    objects = AttendanceCodesStaffManager()
    attendance_code = models.CharField(max_length=3)
    attendance_description = models.TextField(null=True, blank=True)

    def natural_key(self):
        return self.attendance_code

    def __unicode__(self):
        return self.attendance_code


class DailyAttendanceStaff(models.Model):
    """
    This holds attendance data for staff. Every time attendance is taken for a staff member a database entry is created
    here.
    """
    staff = models.ForeignKey(Staff)
    created_by = models.ForeignKey(Staff, related_name='created_by')
    attendance_code = models.ForeignKey(AttendanceCodesStaff)
    am_pm = models.CharField(max_length=2, choices=am_pm_choices, blank=True)
    date_attendance_taken = models.DateField(default=datetime.date.today())
    reason_for_absence = models.TextField(null=True, blank=True)
    active = models.BooleanField(default=True)
    deactivated_by = models.ForeignKey(Staff, related_name='deactivated_by', null=True, blank=True)
    deactivated_date = models.DateField(null=True, blank=True)

    def __unicode__(self):
        return "%s %s, (%s, %s)" % (self.staff.Forename,
                                    self.staff.Surname,
                                    self.date_attendance_taken.isoformat(),
                                    self.am_pm)


class StaffAttendanceFutureRules(models.Model):
    """
    The Future Attendance rules will work on this basis...

    Each rule with have the following, inputted via an interface, and can of course be edited and removed via the same
    interface. Rules can only be put in and effect the current academic Year.

    - One or more usernames
    - Start Date
    - Stop date
    - Days of the week to apply
    - Am/PM (can be both)
    - Attendance code

    Attendance rules will be stored in a table in the following format

    - Usernames (JSON list)
    - AcademicYear (text)
    - StartDate (date field)
    - StopDate (Date Field)
    - Days (JSON List)
    - AMPM (JSON LIST)
    - Code (text)

    Thus a rule can be scanned for existence for a selected user where todays date is TODAY, time of day is AM_PM,
    Weekday is WDAY, current Academic year is THISYEAR, and user is USER using the following

    rulestable.objects.filter(Username__icontains=USER, StartDate__lte=TODAY, StopDate__gte=TODAY,
    Days__icontains=WDAY,AMPM__icontains=AM_PM,AcadmeicYear=THISYEAR).exists()

    and the same can be used to obtain the code for that staff member.
    A User Interface needs to be created to
    Add rules
    Edit Current rules
    Remove Rules

    A two stage interface with a form at the top and a table of rules below, that can be selected from to edit or remove
    On load of the attendance page, attendance codes should be gathered by AJAX call (I assume they are already) and the
    backend script can run through the attendance rules to return the required codes. You will also need to reload is
    the AM/PM select is changed. I will leave it up to you if you want the backend to return present codes, or you put
    them on at the front end. I would recommend the first as it will make things consistent.
    """
    user_names = models.TextField()
    academic_year = models.CharField(max_length=9)
    start_date = models.DateField(default=datetime.date.today())
    stop_date = models.DateField(default=datetime.date.today())
    days = models.TextField()
    am_pm = models.TextField()
    attendance_code = models.CharField(max_length=5)

    # extra fields
    date_rule_created = models.DateField()
    date_rule_modified = models.DateField(default=datetime.date.today())
    created_by = models.ForeignKey(Staff, related_name='record_created_by')
    modified_by = models.ForeignKey(Staff, related_name='record_modified_by')
    active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s %s (%s - %s)" % (self.attendance_code,
                                    self.am_pm,
                                    self.start_date.isoformat(),
                                    self.stop_date.isoformat())