# unit test imports
import sys
import os
sys.path.append('/home/ubuntu/sites/WillowTree/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings
import unittest
from django.test import Client

# Setting up test environment
from django.test.utils import setup_test_environment
setup_test_environment()

# Python imports
import json
import datetime

# Database imports
from MIS.models import Staff
from StaffAttendance.models import *
import operator
from django.db.models import Q
from operator import and_


class StaffAttendanceTests(unittest.TestCase):
    def setUp(self):
        # dates in iso format
        self.date_today = datetime.datetime.today().date().isoformat()
        self.date_week_from_now = datetime.datetime.today().date() + datetime.timedelta(days=7)
        self.date_week_from_now = self.date_week_from_now.isoformat()

        # client 1 exists with battersea admin access
        self.client1 = Client()
        self.client1.login(username='royroy21', password='football101')

        # client 2 exists with battersea teacher access
        self.client2 = Client()
        self.client2.login(username='BTesting', password='password')

        # client 3 dose not exist
        self.client3 = Client()
        self.client3.login(username='user1', password='password')

    def test_admin_access(self):
        """
        lets the user access staff attendance
        """
        response = self.client1.get('/BatterseaAdmin/2013-2014/staff_attendance/home/')
        self.assertEqual('"Staff.Battersea", "Staff.Battersea.Department"' in response.content, True)

    def test_staff_access(self):
        """
        does not let the user access staff attendance
        """
        response = self.client2.get('/BatterseaTeacher/2013-2014/staff_attendance/home/')
        self.assertEqual("ACCESS DENIED: Admin only area" in response.content, True)

    def test_user_doesnt_exist_login(self):
        """
        does not let the user access staff attendance
        """
        response = self.client3.get('/BatterseaAdmin/2013-2014/staff_attendance/home/')
        self.assertEqual("<b>You have been logged out</b>" in response.content, True)

    def test_take_attendance_page(self):
        """
        Compiles data for the take_attendance_page.html
        """
        response = self.client1.get('/BatterseaAdmin/2013-2014/Staff.Battersea.LowerSchool.Form.1BE/staff_attendance/take_attendance_page/')
        self.assertEqual(type(response.content) == str, True)
        self.assertEqual('"staff_name": "Roy Hanley"' in response.content, True)
        self.assertEqual('pm_attendance_taken' in response.content, True)
        self.assertEqual('pm_attendance_taken' in response.content, True)

    def test_past_future_attendance_page(self):
        staff_attendance_record = DailyAttendanceStaff(staff=Staff.objects.get(id=3099),
                                                       created_by=Staff.objects.get(id=2502),
                                                       attendance_code=AttendanceCodesStaff.objects.get(attendance_code="/"),
                                                       am_pm="PM",
                                                       date_attendance_taken="2014-05-01")
        staff_attendance_record.save()
        response = self.client1.get('/BatterseaAdmin/2013-2014/staff_attendance/get_attendance_data/Staff.Battersea/2014-05-01/2014-05-01/')
        self.assertEqual(type(response.content) == str, True)
        self.assertEqual(len(json.loads(response.content)) == 1, True)
        self.assertEqual('"date_attendance_taken": "2014-05-01"' in response.content, True)
        self.assertEqual('"attendance_code": "/"' in response.content, True)

    def test_staff_attendance_codes(self):
        """
        Gets staff attendance codes
        """
        response = self.client1.get('/BatterseaAdmin/2013-2014/staff_attendance/staff_attendance_codes/')
        self.assertEqual(type(response.content) == str, True)
        for attendance_code in AttendanceCodesStaff.objects.all():
            self.assertEqual('"attendance_code": "%s"' % attendance_code.attendance_code in response.content, True)

    def test_taking_daily_attendance(self):
        """
        Tests daily attendance
        """
        post_data = {"am_pm1": "AM",
                     "staff_id1": "3099",
                     "new_code1": "S",
                     "am_pm2": "AM",
                     "staff_id2": "3100",
                     "new_code2": "V",
                     "am_pm3": "AM",
                     "staff_id3": "2502",
                     "new_code3": "WFH"}
        response = self.client1.post('/Battersea/2013-2014/staff_attendance/process_attendance/', post_data)
        self.assertEqual('Staff Attendance Successfully taken for Roy Hanley' in response.content, True)
        self.assertEqual('Staff Attendance Successfully taken for Bill Testing' in response.content, True)
        self.assertEqual('Staff Attendance Successfully taken for Mary Testing' in response.content, True)
        new_attendance_record_3099 = DailyAttendanceStaff.objects.filter(staff_id=3099,
                                                                         am_pm='AM',
                                                                         date_attendance_taken=self.date_today,
                                                                         active=True,
                                                                         attendance_code__attendance_code="S")
        new_attendance_record_3100 = DailyAttendanceStaff.objects.filter(staff_id=3100, am_pm='AM',
                                                                         date_attendance_taken=self.date_today,
                                                                         active=True,
                                                                         attendance_code__attendance_code="V")
        new_attendance_record_2502 = DailyAttendanceStaff.objects.filter(staff_id=2502,
                                                                         am_pm='AM',
                                                                         date_attendance_taken=self.date_today,
                                                                         active=True,
                                                                         attendance_code__attendance_code="WFH")
        self.assertEqual(new_attendance_record_3099.exists() and len(new_attendance_record_3099) == 1, True)
        self.assertEqual(new_attendance_record_3100.exists() and len(new_attendance_record_3100) == 1, True)
        self.assertEqual(new_attendance_record_2502.exists() and len(new_attendance_record_2502) == 1, True)
        self.assertEqual(new_attendance_record_3099[0].created_by_id == 2502, True)

    def taking_past_or_future_attendance(self):
        """
        Used with test_taking_past_or_future_attendance_1 and test_taking_past_or_future_attendance_2 tests
        """
        post_data = {"am_pm1": "AM",
                     "staff_id1": "3099",
                     "new_code1": "S",
                     "date1": "2014-06-01",
                     "am_pm2": "AM",
                     "staff_id2": "3100",
                     "new_code2": "V",
                     "date2": "2014-06-01",
                     "am_pm3": "AM",
                     "staff_id3": "2502",
                     "new_code3": "WFH",
                     "date3": "2014-06-01"}
        response = self.client1.post('/Battersea/2013-2014/staff_attendance/process_attendance/past_future/', post_data)
        self.assertEqual('Staff Attendance Successfully taken for Roy Hanley' in response.content, True)
        self.assertEqual('Staff Attendance Successfully taken for Bill Testing' in response.content, True)
        self.assertEqual('Staff Attendance Successfully taken for Mary Testing' in response.content, True)
        new_attendance_record_3099 = DailyAttendanceStaff.objects.filter(staff_id=3099,
                                                                         am_pm='AM',
                                                                         date_attendance_taken="2014-06-01",
                                                                         active=True,
                                                                         attendance_code__attendance_code="S")
        new_attendance_record_3100 = DailyAttendanceStaff.objects.filter(staff_id=3100, am_pm='AM',
                                                                         date_attendance_taken="2014-06-01",
                                                                         active=True,
                                                                         attendance_code__attendance_code="V")
        new_attendance_record_2502 = DailyAttendanceStaff.objects.filter(staff_id=2502,
                                                                         am_pm='AM',
                                                                         date_attendance_taken="2014-06-01",
                                                                         active=True,
                                                                         attendance_code__attendance_code="WFH")
        self.assertEqual(new_attendance_record_3099.exists() and len(new_attendance_record_3099) == 1, True)
        self.assertEqual(new_attendance_record_3100.exists() and len(new_attendance_record_3100) == 1, True)
        self.assertEqual(new_attendance_record_2502.exists() and len(new_attendance_record_2502) == 1, True)
        self.assertEqual(new_attendance_record_3099[0].created_by_id == 2502, True)

    def test_taking_past_or_future_attendance_1(self):
        """
        Tests past/future attendance
        """
        self.taking_past_or_future_attendance()

    def test_taking_past_or_future_attendance_2(self):
        """
        The same tests as test_taking_past_or_future_attendance_1 only this time we are amending past attendance records
        that have been created. It system will mark the old records as active = False and create new records.
        """
        self.taking_past_or_future_attendance()
        post_data = {"am_pm1": "AM",
                     "staff_id1": "3099",
                     "new_code1": "/",
                     "date1": "2014-06-01",
                     "am_pm2": "AM",
                     "staff_id2": "3100",
                     "new_code2": "/",
                     "date2": "2014-06-01",
                     "am_pm3": "AM",
                     "staff_id3": "2502",
                     "new_code3": "/",
                     "date3": "2014-06-01"}
        response = self.client1.post('/Battersea/2013-2014/staff_attendance/process_attendance/past_future/', post_data)
        self.assertEqual('Staff Attendance Successfully taken for Roy Hanley' in response.content, True)
        self.assertEqual('Staff Attendance Successfully taken for Bill Testing' in response.content, True)
        self.assertEqual('Staff Attendance Successfully taken for Mary Testing' in response.content, True)

        # testing de-activated records
        old_attendance_record_3099 = DailyAttendanceStaff.objects.filter(staff_id=3099,
                                                                         am_pm='AM',
                                                                         date_attendance_taken="2014-06-01",
                                                                         active=False,
                                                                         attendance_code__attendance_code="S")
        old_attendance_record_3100 = DailyAttendanceStaff.objects.filter(staff_id=3100, am_pm='AM',
                                                                         date_attendance_taken="2014-06-01",
                                                                         active=False,

                                                                         attendance_code__attendance_code="V")
        old_attendance_record_2502 = DailyAttendanceStaff.objects.filter(staff_id=2502,
                                                                         am_pm='AM',
                                                                         date_attendance_taken="2014-06-01",
                                                                         active=False,
                                                                         attendance_code__attendance_code="WFH")
        self.assertEqual(old_attendance_record_3099.exists(), True)
        self.assertEqual(old_attendance_record_3100.exists(), True)
        self.assertEqual(old_attendance_record_2502.exists(), True)
        self.assertEqual(old_attendance_record_3099[0].created_by_id == 2502, True)
        self.assertEqual(old_attendance_record_3099[0].deactivated_by_id == 2502, True)
        self.assertEqual(old_attendance_record_3099[0].deactivated_date.isoformat() == self.date_today, True)

        # testing newly created records
        new_attendance_record_3099 = DailyAttendanceStaff.objects.filter(staff_id=3099,
                                                                         am_pm='AM',
                                                                         date_attendance_taken="2014-06-01",
                                                                         active=True,
                                                                         attendance_code__attendance_code="/")
        new_attendance_record_3100 = DailyAttendanceStaff.objects.filter(staff_id=3100, am_pm='AM',
                                                                         date_attendance_taken="2014-06-01",
                                                                         active=True,
                                                                         attendance_code__attendance_code="/")
        new_attendance_record_2502 = DailyAttendanceStaff.objects.filter(staff_id=2502,
                                                                         am_pm='AM',
                                                                         date_attendance_taken="2014-06-01",
                                                                         active=True,
                                                                         attendance_code__attendance_code="/")
        self.assertEqual(new_attendance_record_3099.exists() and len(new_attendance_record_3099) == 1, True)
        self.assertEqual(new_attendance_record_3100.exists() and len(new_attendance_record_3100) == 1, True)
        self.assertEqual(new_attendance_record_2502.exists() and len(new_attendance_record_2502) == 1, True)
        self.assertEqual(new_attendance_record_3099[0].created_by_id == 2502, True)

    def test_create_staff_attendance_rule(self):
        """
        Tests the creation of a staff attendance rule.
        """
        # create rule
        post_data_rule_1 = {"user_names": "['royroy21']",
                            "start_date": self.date_today,
                            "stop_date": self.date_week_from_now,
                            "days": "['Monday','Tuesday','Wednesday','Thursday','Friday']",
                            "am_pm": "['AM','PM']",
                            "attendance_code": "S"}
        response = self.client1.post('/Battersea/2014-2015/staff_attendance/create_staff_attendance_rule/',
                                     post_data_rule_1)

        ########################
        # PRINT RESPONSE
        ########################
        print "response: %s" % response.content

        self.assertEqual("SUCCESS" in response.content, True)

        # check rule is created in database
        days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
        am_pm = ['AM', 'PM']
        rule = StaffAttendanceFutureRules.objects.filter(reduce(and_, [Q(days__icontains=c) for c in days]),
                                                         reduce(and_, [Q(am_pm__icontains=c) for c in am_pm]),
                                                         user_names__icontains='royroy21',
                                                         start_date=self.date_today,
                                                         stop_date=self.date_week_from_now,
                                                         attendance_code="S")
        self.assertEqual(rule.exists(), True)
        self.assertEqual(rule.count() is 1, True)
        return

    def test_if_new_staff_attendance_rule_works(self):
        """
        Tests if new staff attendance rule created in "test_create_staff_attendance_rule" works.
        """
        # create new rule
        post_data_rule_1 = {"user_names": "['royroy21']",
                            "start_date": self.date_today,
                            "stop_date": self.date_week_from_now,
                            "days": "['Monday','Tuesday','Wednesday','Thursday','Friday']",
                            "am_pm": "['AM','PM']",
                            "attendance_code": "S"}
        self.client1.post('/Battersea/2014-2015/staff_attendance/create_staff_attendance_rule/', post_data_rule_1)

        response = self.client1.get('/BatterseaAdmin/2014-2015/Staff.Battersea.Department.IT/staff_attendance/take_attendance_page/')

        ########################
        # PRINT RESPONSE
        ########################
        print "response: %s" % response.content

        response_as_dic = json.loads(response.content)
        for i in response_as_dic:
            if i['staff_id'] == 2502:
                self.assertEqual(i['am_attendance_taken'] == "S", True)
                self.assertEqual(i['pm_attendance_taken'] == "S", True)

    def test_edit_staff_attendance_rule(self):
        """
        Tests if we can edit an existing rule and see if it still applies.
        """
        # create new rule
        post_data_rule_1 = {"user_names": "['royroy21']",
                            "start_date": self.date_today,
                            "stop_date": self.date_week_from_now,
                            "days": "['Monday','Tuesday','Wednesday','Thursday','Friday']",
                            "am_pm": "['AM','PM']",
                            "attendance_code": "S"}
        self.client1.post('/Battersea/2014-2015/staff_attendance/create_staff_attendance_rule/', post_data_rule_1)

        # get rule to edit
        days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
        am_pm = ['AM', 'PM']
        rule__id = StaffAttendanceFutureRules.objects.get(reduce(and_, [Q(days__icontains=c) for c in days]),
                                                          reduce(and_, [Q(am_pm__icontains=c) for c in am_pm]),
                                                          user_names__icontains='royroy21',
                                                          start_date=self.date_today,
                                                          stop_date=self.date_week_from_now,
                                                          attendance_code="S").id

        # change rule from 'sick' to 'maternity'
        post_data_rule_1 = {"rule_id": rule__id,
                            "user_names": "['royroy21']",
                            "start_date": self.date_today,
                            "stop_date": self.date_week_from_now,
                            "days": "['Monday','Tuesday','Wednesday','Thursday','Friday']",
                            "am_pm": "['AM','PM']",
                            "attendance_code": "M"}
        response = self.client1.post('/Battersea/2014-2015/staff_attendance/edit_staff_attendance_rule/',
                                     post_data_rule_1)

        ########################
        # PRINT RESPONSE
        ########################
        print "response: %s" % response.content

        self.assertEqual("SUCCESS" in response.content, True)

        # check database entry has changed
        days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
        am_pm = ['AM', 'PM']
        rule = StaffAttendanceFutureRules.objects.filter(reduce(and_, [Q(days__icontains=c) for c in days]),
                                                         reduce(and_, [Q(am_pm__icontains=c) for c in am_pm]),
                                                         user_names__icontains='royroy21',
                                                         start_date=self.date_today,
                                                         stop_date=self.date_week_from_now,
                                                         attendance_code="M")
        self.assertEqual(rule.exists(), True)
        self.assertEqual(rule.count() is 1, True)

        # check take_attendance_page is functioning correctly
        response = self.client1.get('/BatterseaAdmin/2014-2015/Staff.Battersea.Department.IT/staff_attendance/take_attendance_page/')
        response_as_dic = json.loads(response.content)
        for i in response_as_dic:
            if i['staff_id'] == 2502:
                self.assertEqual(i['am_attendance_taken'] == "M", True)
                self.assertEqual(i['pm_attendance_taken'] == "M", True)
        return

    def test_remove_staff_attendance_rule(self):
        """
        Tests if we can delete an existing rule and see if it still applies.
        """
        # create new rule
        post_data_rule_1 = {"user_names": "['royroy21']",
                            "start_date": self.date_today,
                            "stop_date": self.date_week_from_now,
                            "days": "['Monday','Tuesday','Wednesday','Thursday','Friday']",
                            "am_pm": "['AM','PM']",
                            "attendance_code": "S"}
        self.client1.post('/Battersea/2014-2015/staff_attendance/create_staff_attendance_rule/', post_data_rule_1)

        # get rule to remove
        days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']
        am_pm = ['AM', 'PM']
        rule_id = StaffAttendanceFutureRules.objects.get(reduce(and_, [Q(days__icontains=c) for c in days]),
                                                         reduce(and_, [Q(am_pm__icontains=c) for c in am_pm]),
                                                         user_names__icontains='royroy21',
                                                         start_date=self.date_today,
                                                         stop_date=self.date_week_from_now,
                                                         attendance_code="S").id
        response = self.client1.post('/Battersea/2014-2015/staff_attendance/remove_staff_attendance_rule/%s/' % str(rule_id))

        ########################
        # PRINT RESPONSE
        ########################
        print "response: %s" % response.content

        self.assertEqual("SUCCESS" in response.content, True)
        self.assertEqual(StaffAttendanceFutureRules.objects.filter(id=rule_id, active=True).exists() is False, True)

    def test_get_active_staff_attendance_rules(self):
        """
        Tests get_active_staff_attendance_rules.
        """
        # create new rule
        post_data_rule_1 = {"user_names": "['royroy21']",
                            "start_date": self.date_today,
                            "stop_date": self.date_week_from_now,
                            "days": "['Monday','Tuesday','Wednesday','Thursday','Friday']",
                            "am_pm": "['AM','PM']",
                            "attendance_code": "S"}
        self.client1.post('/Battersea/2014-2015/staff_attendance/create_staff_attendance_rule/', post_data_rule_1)

        response = self.client1.get('/Battersea/2014-2015/staff_attendance/view_active_staff_attendance_rules/')

        ########################
        # PRINT RESPONSE
        ########################
        print "response: %s" % response.content
        self.assertEqual(len(json.loads(response.content)) == 1, True)

    def test_get_staff_user_names(self):
        """
        Tests getting staff user names.
        """
        response_all = self.client1.get('/Battersea/2014-2015/staff_attendance/get_staff_user_names/all/')
        response_group = self.client1.get('/Battersea/2014-2015/staff_attendance/get_staff_user_names/Staff.Battersea.Department.IT/')

    def tearDown(self):
        for i in DailyAttendanceStaff.objects.filter(created_by__id=2502):
            i.delete()
        for i in StaffAttendanceFutureRules.objects.filter(created_by__id=2502):
            i.delete()
        pass


if __name__ == '__main__':
    unittest.main()