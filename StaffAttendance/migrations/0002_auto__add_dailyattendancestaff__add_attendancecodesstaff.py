# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'DailyAttendanceStaff'
        db.create_table(u'StaffAttendance_dailyattendancestaff', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('staff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('created_by', self.gf('django.db.models.fields.related.ForeignKey')(related_name='created_by', to=orm['MIS.Staff'])),
            ('attendance_code', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['StaffAttendance.AttendanceCodesStaff'])),
            ('am_pm', self.gf('django.db.models.fields.CharField')(max_length=2, blank=True)),
            ('date_attendance_taken', self.gf('django.db.models.fields.DateField')(default=datetime.date(2014, 7, 10))),
            ('reason_for_absence', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'StaffAttendance', ['DailyAttendanceStaff'])

        # Adding model 'AttendanceCodesStaff'
        db.create_table(u'StaffAttendance_attendancecodesstaff', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('attendance_code', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('attendance_description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'StaffAttendance', ['AttendanceCodesStaff'])


    def backwards(self, orm):
        
        # Deleting model 'DailyAttendanceStaff'
        db.delete_table(u'StaffAttendance_dailyattendancestaff')

        # Deleting model 'AttendanceCodesStaff'
        db.delete_table(u'StaffAttendance_attendancecodesstaff')


    models = {
        u'MIS.address': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AddressLine1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'AddressLine2': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'AddressLine3': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'AddressLine4': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'HomeSalutation': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'object_name': 'Address'},
            'Note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Phone1': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'Phone2': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'Phone3': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'PostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'PostalTitle': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menutypes': {
            'Desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Logo': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'MenuLevel': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'ordering': "['Name']", 'object_name': 'MenuTypes'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'schoolMenu'", 'to': u"orm['MIS.School']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.school': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']"}),
            'HeadTeacher': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'School'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'TemplateBanner': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBanner.jpg'", 'max_length': '200'}),
            'TemplateCSS': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBasic.css'", 'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staff': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']", 'null': 'True', 'blank': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'DefaultMenu': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.MenuTypes']", 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'FailedLoginAttempts': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname']", 'object_name': 'Staff'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ResetPassword': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'User': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'StaffAttendance.attendancecodesstaff': {
            'Meta': {'object_name': 'AttendanceCodesStaff'},
            'attendance_code': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'attendance_description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'StaffAttendance.dailyattendancestaff': {
            'Meta': {'object_name': 'DailyAttendanceStaff'},
            'am_pm': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'attendance_code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['StaffAttendance.AttendanceCodesStaff']"}),
            'created_by': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'created_by'", 'to': u"orm['MIS.Staff']"}),
            'date_attendance_taken': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 7, 10)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'reason_for_absence': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'staff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 7, 10, 13, 27, 2, 909219)'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 7, 10, 13, 27, 2, 908806)'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['StaffAttendance']
