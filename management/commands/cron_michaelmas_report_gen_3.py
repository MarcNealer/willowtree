# imports
from django.core.management.base import BaseCommand
from MIS.modules.AnalysisReports.AnalysisReportsNew import *
from MIS.modules.globalVariables import GlobalVariablesFunctions

# import log
import logging
log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    15 02 * 6,7,8 *
    """
    def handle(self, *args, **options):
        try:
            term = 'Mich'
            ac_year = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
            test = AnalysisStoreCreator(ac_year, term)
            test.createCohortAnalysisReports()
            test.createSchoolReviewReports()
            test.createAPSProgressWholeSchool()
            test.createTermReports()
            log.warn('cron_michaelmas_report_gen_3.py successfully run')
        except Exception as error:
            log.warn('ERROR: cron_michaelmas_report_gen_3.py failed (%s)' % error)
        return