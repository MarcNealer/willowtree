# imports
from django.core.management.base import BaseCommand
from MIS.modules.AnalysisReports.AnalysisReportsNew import *
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules.AnalysisReports.ReportFunctions import ExamDataExtract

# import log
import logging
log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    30 02 * 6,7,8 *
    """
    def handle(self, *args, **options):
        try:
            ac_year = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
            test = ExamDataExtract(ac_year, 'Clapham', 'Summer', 2358)
            test.start()
            log.warn('cron_summer_exams_2.py successfully run')
        except Exception as error:
            log.warn('ERROR: cron_summer_exams_2.py failed (%s)' % error)
        return