# imports
from django.core.management.base import BaseCommand
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules.ThirdParty import ClarionCall
import time
import ConfigParser

# import log
import logging
log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    20 11 * * *
    """
    def handle(self, *args, **options):

        # get clarion call school settings file - this should be in the same directory as this program
        config = False
        try:
            config = ConfigParser.RawConfigParser()
            config.read('/home/ubuntu/sites/WillowTree/MIS/management/commands/clarion.cfg')
        except Exception as error:
            log.warn('ERROR: cron_clarion_call_sync_with_willowtree.py failed (%s)' % error)

        # setting academic years - program will only run if these return true
        this_ac_year = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
        if GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('NextYear'):
            next_ac_year = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('NextYear')
        else:
            next_ac_year = "%s-%s" % (str(int(this_ac_year.split('-')[0])+1), str(int(this_ac_year.split('-')[1])+1))

        # main clarion call logic (collecting school data - sending to clarion call)
        if this_ac_year and next_ac_year:
            for school in ['Battersea', 'Clapham', 'Fulham', 'Kensington', 'Kindergarten']:

                # STAFF
                try:
                    group = config.get('%s Staff' % school, 'group')
                    msisdn = config.get('%s Staff' % school, 'msisdn')
                    uploadcode = config.get('%s Staff' % school, 'uploadcode')
                    school_name = config.get('%s Staff' % school, 'school_name')
                    uploadfile = config.get('%s Staff' % school, 'uploadfile')
                    ClarionCall.ClarionCallExtractStaff(this_ac_year,
                                                        group,
                                                        msisdn,
                                                        uploadcode,
                                                        school_name,
                                                        uploadfile)

                    # wait for 5 minutes
                    time.sleep(300)

                    # log success
                    log.warn('SUCCESS: cron_clarion_call_sync_with_willowtree.py (%s %s)' % (school, 'Staff'))
                except Exception as error:

                    # or log error
                    log.warn('ERROR: cron_clarion_call_sync_with_willowtree.py failed (%s)' % error)

                # PUPIL
                for pupil_type in ['Current', 'Applicants']:
                    try:
                        group = config.get('%s %s' % (school, pupil_type), 'group')
                        msisdn = config.get('%s %s' % (school, pupil_type), 'msisdn')
                        uploadcode = config.get('%s %s' % (school, pupil_type), 'uploadcode')
                        school_name = config.get('%s %s' % (school, pupil_type), 'school_name')
                        uploadfile = config.get('%s %s' % (school, pupil_type), 'uploadfile')

                        if 'Current' in group:
                            ac_year = this_ac_year
                        else:
                            ac_year = next_ac_year

                        ClarionCall.ClarionCallExtractPupil(ac_year,
                                                            group,
                                                            msisdn,
                                                            uploadcode,
                                                            school_name,
                                                            uploadfile)

                        # wait for 5 minutes
                        time.sleep(300)

                        # log success
                        log.warn('SUCCESS: cron_clarion_call_sync_with_willowtree.py (%s %s)' % (school, pupil_type))
                    except Exception as error:

                        # or log error
                        log.warn('ERROR: cron_clarion_call_sync_with_willowtree.py failed (%s)' % error)
        else:
            log.warn('ERROR: cron_clarion_call_sync_with_willowtree.py failed (Current/NextYear system var not set)')
        return
