# -*- coding: utf-8 -*-
"""
Created on Thu Mar 14 13:38:58 2013

@author: dbmgr
"""

from django.contrib import admin
from TimeTables.models import *
        

admin.site.register(TimeSlots)
admin.site.register(Room)
admin.site.register(EventDetails)
admin.site.register(Events)
admin.site.register(TabledEvents)
admin.site.register(GroupTimeTable)   