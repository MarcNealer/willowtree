# -*- coding: utf-8 -*-
"""
Created on Tue Jun  4 11:09:40 2013

@author: marc
"""

from TimeTables.models import *
from MIS.models import Group, School,Subject
import datetime, csv
DayNumberList=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']

def ImportSlots(School,FileName):
        ImportItems = csv.DictReader(open(FileName, 'rb'))
        for recs in ImportItems:
            print recs
            if Group.objects.filter(Name__iendswith='%s.%s' % (School, recs['Group'])).filter(Name__istartswith='Current').exists():
                basegroup=Group.objects.filter(Name__iendswith='%s.%s' % (School, recs['Group'])).filter(Name__istartswith='Current')[0]
                itemname="%s %s" % (recs['Name'],recs['Group'])
                dateperiod='All AcYear'
                timeitems=recs['StartTime'].split(':')
                starttime=datetime.time(int(timeitems[0]),int(timeitems[1]))
                duration=int(recs['Duration'])
                for days in ['Monday','Tuesday','Wednesday','Thursday','Friday']:
                    newSlot=TimeSlots(Name=itemname,
                                     Description=recs['Name'],
                                     BaseGroup=basegroup,
                                     DatePeriod=dateperiod,
                                     DayNumber=DayNumberList.index(days),
                                     StartTime=starttime,
                                     Duration=duration)
                    newSlot.save()
            else:
                print 'BaseGroup invalid'

def importBaseEvents(school,FileName):
    ImportItems = csv.DictReader(open(FileName, 'rb'))
    schoolrec=School.objects.get(Name=school)
    for recs in ImportItems:
        print recs
        classlist= recs['Classes'].split(':')
        subjectrec=Subject.objects.get(Name__iexact=recs['Subject'])
        for classes in classlist:
            if Group.objects.filter(Name__iendswith=classes).filter(Name__istartswith='Current').exists():
                grouprec=Group.objects.filter(Name__iendswith=classes).filter(Name__istartswith='Current')[0]

                for lessons in range(0,int(recs['NoEvents'])):
                    eventName='%s %s (%d)' % (classes, recs['Subject'], lessons+1)                    
                    newevent=Events(Name=eventName,
                               Description=eventName,
                               School=schoolrec,
                               Subject=subjectrec)
                    newevent.save()
                    newevent.Groups.add(grouprec)
                    newevent.save()
            else:
                print '%s not found' % classes