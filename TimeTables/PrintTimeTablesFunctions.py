# -*- coding: utf-8 -*-
"""
Created on Sat Jun 22 14:30:16 2013

@author: marc
"""

import datetime
from MIS.modules.PupilRecs import *
from MIS.modules import GeneralFunctions
from TimeTables.models import *
from django.template.loader import render_to_string
from abc import ABCMeta, abstractmethod, abstractproperty

def TTPrintDate(NextYear=False):
    ''' Quick function to get the first monday in October, Feb, or June '''
    todaysDate=datetime.datetime.today()
    if todaysDate.month > 7 or NextYear==True:
        printmonth=10
    elif todaysDate.month < 5:
        printmonth=2
    else:
        printmonth=6
    datefind=False
    printday=1
    while datefind==False:
        printDate=datetime.date(todaysDate.year,printmonth,printday)
        if printDate.isoweekday()==1:
            datefind=True
        printday+=1
    return printDate

def TableList(xGroupName,PupilAcYear, TableAcYear):
    pupillist=GeneralFunctions.Get_PupilList(PupilAcYear,GroupName=xGroupName)
    tablelist=[]
    for pupils in pupillist:
        tablelist.append(PupilTimeTable(pupils.id,PupilAcYear,TableAcYear))
    return tablelist

class ABCTimeTable(object):
    __metaclass__=ABCMeta
    TimeTableAPI='TimeTableAPI.html'
    @abstractmethod
    def __genTimeTable__(self):
        return
    def Monday(self):
        return self.__ClashCheck__(self.TTRecords.filter(EventDate=self.TTPrintDate))
    def MondayHTML(self):
        return render_to_string(self.TimeTableAPI ,{'TableRecs':self.Monday()})
    def Tuesday(self):
        return self.__ClashCheck__(self.TTRecords.filter(EventDate=self.TTPrintDate+datetime.timedelta(days=1)))
    def TuesdayHTML(self):
        return render_to_string(self.TimeTableAPI,{'TableRecs':self.Tuesday()})
    def Wednesday(self):
        return self.__ClashCheck__(self.TTRecords.filter(EventDate=self.TTPrintDate+datetime.timedelta(days=2)))
    def WednesdayHTML(self):
        return render_to_string(self.TimeTableAPI,{'TableRecs':self.Wednesday()})
    def Thursday(self):
        return self.__ClashCheck__(self.TTRecords.filter(EventDate=self.TTPrintDate+datetime.timedelta(days=3)))
    def ThursdayHTML(self):
        return render_to_string(self.TimeTableAPI,{'TableRecs':self.Thursday()})
    def Friday(self):
        return self.__ClashCheck__(self.TTRecords.filter(EventDate=self.TTPrintDate+datetime.timedelta(days=4)))
    def FridayHTML(self):
        return render_to_string(self.TimeTableAPI,{'TableRecs':self.Friday()})
    def Saturday(self):
        return self.__ClashCheck__(self.TTRecords.filter(EventDate=self.TTPrintDate+datetime.timedelta(days=5)))
    def SaturdayHTML(self):
        return render_to_string(self.TimeTableAPI,{'TableRecs':self.Saturday()})
    def Sunday(self):
        return self.__ClashCheck__(self.TTRecords.filter(EventDate=self.TTPrintDate+datetime.timedelta(days=6)))
    def SundayHTML(self):
        return render_to_string(self.TimeTableAPI,{'TableRecs':self.Sunday()})
    def __ClashCheck__(self,theseRecords):
        firstloop=True
        for records in theseRecords:
            if firstloop:
                firstloop=False
                prevRecord=records
            else:
                prevRecord.ClashTest(records)
                prevRecord.BreakTest(records)
                prevRecord=records
        return theseRecords

class PupilTimeTable(PupilObject,ABCTimeTable):
    def __init__(self,PupilId,AcYear,TableAcYear=None):
        PupilObject.__init__(self,PupilId,AcYear)
        self.TableAcYear=AcYear
        if not TableAcYear==None:
            self.TableAcYear=TableAcYear
        if not self.TableAcYear==GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear'):
            self.TTPrintDate=TTPrintDate(NextYear=True)
        else:
            self.TTPrintDate=TTPrintDate()
        self.__genTimeTable__()
        self.TimeTableAPI='TimeTableAPI.html'
    def __FullGroupList__(self):
        primarylist=[x.Group.Name for x in self.GroupList()]
        grouplist=set()
        for groupnames in primarylist:
            grouplist=grouplist.union(GeneralFunctions.SubGroups(groupnames))
            grouplist.add(groupnames)
        return list(grouplist)


    def __genTimeTable__(self):
        self.TTRecords=GroupTimeTable.objects.filter(TabledEvent__EventDetails__Event__Groups__Name__in=self.__FullGroupList__(),
                                                EventDate__gte=self.TTPrintDate,
                                                AcademicYear=self.TableAcYear)
        self.TTRecords=self.TTRecords.filter(EventDate__lt=self.TTPrintDate+datetime.timedelta(days=7)).order_by('-EventDate','EventStartTime')
        return


def StaffTimeTableList(xGroupName,xAcYear):
    Stafflist=GeneralFunctions.Get_StaffList(GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear'),GroupName=xGroupName)
    tablelist=[]
    for staff in Stafflist:
        tablelist.append(StaffTimeTable(staff.id,xAcYear))
    return tablelist

class StaffTimeTable(ABCTimeTable):
    def __init__(self,StaffId,AcYear):
        self.StaffId=StaffId
        if not AcYear==GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear'):
            self.TTPrintDate=TTPrintDate(NextYear=True)
        else:
            self.TTPrintDate=TTPrintDate()
        self.AcYear=AcYear
        self.Record=Staff.objects.get(id=int(StaffId))
        self.__genTimeTable__()
        self.TimeTableAPI='StaffTimeTableAPI.html'
    def __genTimeTable__(self):
        self.TTRecords=GroupTimeTable.objects.filter(TabledEvent__EventDetails__Staff=self.Record,
                                                     EventDate__gte=self.TTPrintDate,
                                                     AcademicYear=self.AcYear)
        self.TTRecords=self.TTRecords.filter(EventDate__lt=self.TTPrintDate+datetime.timedelta(days=7)).order_by('-EventDate','EventStartTime')
