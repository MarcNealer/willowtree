from TimeTables.models import *
from MIS.models import Group
DayNumberList=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']

class ManageSlots():
    def __init__(self,request,SlotId):
        self.request=request
        self.SlotRecord=TimeSlots.objects.get(id=int(SlotId))
    @staticmethod
    def Update(SlotId, PostData):
        ''' Function to Update a TimeSlot record from Post data. Just
        pass the SlotId and Post data'''
        TimeSlotRecord = TimeSlots.objects.get(id=int(SlotId))
        if 'Name' in PostData:
            TimeSlotRecord.Name = PostData['Name']
        if 'Description' in PostData:
            TimeSlotRecord.Description = PostData['Description']
        if 'DatePeriod' in PostData:
            TimeSlotRecord.DatePeriod = PostData['DatePeriod'] 
        if 'DayOfWeek' in PostData:
            TimeSlotRecord.DayNumber=DayNumberList.index(PostData['DayOfWeek'])
        if 'Cycle' in PostData:
            TimeSlotRecord.Cycle = PostData['Cycle']
        if 'Duration' in PostData:
            TimeSlotRecord.Duration = PostData['Duration']
        if 'StartWeek' in PostData:
            TimeSlotRecord.StartWeek = PostData['StartWeek']
        if 'StartTime' in PostData:
            TimeSlotRecord.StartTime = PostData['StartTime']            
        TimeSlotRecord.save()
        return
    def Delete(self):
        ''' Deletes a Timeslot Record'''
        self.SlotRecord.delete()
        return True 

def TimeSlotList(GroupName):
    return TimeSlot.objects.filter(BaseGroup__Name__in=BuildGroupList(GroupName)).order_by('Description','StartTime')

def BuildGroupList(GroupList):
    returnedlist=set()
    for groups in GroupList:
        returnedlist.update(BuildGroupListFn(groups.Name))
    return list(returnedlist)
    

def BuildGroupListFn(GroupName):
    GroupItems=GroupName.split('.')
    if len(GroupItems)==1:
        return [GroupName]
    else:
        return [GroupName] + BuildGroupListFn('.'.join(GroupItems[:-1]))