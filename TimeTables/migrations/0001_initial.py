# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'TimeSlots'
        db.create_table('TimeTables_timeslots', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('Description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('BaseGroup', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('DatePeriod', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('DayOfWeek', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('Cycle', self.gf('django.db.models.fields.CharField')(default='Weekly', max_length=20)),
            ('StartTime', self.gf('django.db.models.fields.TimeField')()),
            ('Duration', self.gf('django.db.models.fields.IntegerField')()),
            ('StartWeek', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal('TimeTables', ['TimeSlots'])

        # Adding model 'Room'
        db.create_table('TimeTables_room', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Location', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.School'])),
        ))
        db.send_create_signal('TimeTables', ['Room'])

        # Adding model 'Events'
        db.create_table('TimeTables_events', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('Description', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('School', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.School'])),
            ('Subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Subject'], null=True, blank=True)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal('TimeTables', ['Events'])

        # Adding M2M table for field Groups on 'Events'
        db.create_table('TimeTables_events_Groups', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('events', models.ForeignKey(orm['TimeTables.events'], null=False)),
            ('group', models.ForeignKey(orm['MIS.group'], null=False))
        ))
        db.create_unique('TimeTables_events_Groups', ['events_id', 'group_id'])

        # Adding model 'EventDetails'
        db.create_table('TimeTables_eventdetails', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Event', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['TimeTables.Events'], unique=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('Room', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['TimeTables.Room'], null=True, blank=True)),
        ))
        db.send_create_signal('TimeTables', ['EventDetails'])

        # Adding M2M table for field Staff on 'EventDetails'
        db.create_table('TimeTables_eventdetails_Staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('eventdetails', models.ForeignKey(orm['TimeTables.eventdetails'], null=False)),
            ('staff', models.ForeignKey(orm['MIS.staff'], null=False))
        ))
        db.create_unique('TimeTables_eventdetails_Staff', ['eventdetails_id', 'staff_id'])

        # Adding model 'MeetingEvent'
        db.create_table('TimeTables_meetingevent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('Event', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['TimeTables.Events'], unique=True)),
        ))
        db.send_create_signal('TimeTables', ['MeetingEvent'])

        # Adding M2M table for field Staff on 'MeetingEvent'
        db.create_table('TimeTables_meetingevent_Staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('meetingevent', models.ForeignKey(orm['TimeTables.meetingevent'], null=False)),
            ('staff', models.ForeignKey(orm['MIS.staff'], null=False))
        ))
        db.create_unique('TimeTables_meetingevent_Staff', ['meetingevent_id', 'staff_id'])

        # Adding model 'OneToOneEvent'
        db.create_table('TimeTables_onetooneevent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Event', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['TimeTables.Events'], unique=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal('TimeTables', ['OneToOneEvent'])

        # Adding M2M table for field Staff on 'OneToOneEvent'
        db.create_table('TimeTables_onetooneevent_Staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('onetooneevent', models.ForeignKey(orm['TimeTables.onetooneevent'], null=False)),
            ('staff', models.ForeignKey(orm['MIS.staff'], null=False))
        ))
        db.create_unique('TimeTables_onetooneevent_Staff', ['onetooneevent_id', 'staff_id'])

        # Adding M2M table for field Pupil on 'OneToOneEvent'
        db.create_table('TimeTables_onetooneevent_Pupil', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('onetooneevent', models.ForeignKey(orm['TimeTables.onetooneevent'], null=False)),
            ('pupil', models.ForeignKey(orm['MIS.pupil'], null=False))
        ))
        db.create_unique('TimeTables_onetooneevent_Pupil', ['onetooneevent_id', 'pupil_id'])

        # Adding model 'TabledEvents'
        db.create_table('TimeTables_tabledevents', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Event', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['TimeTables.Events'])),
            ('TimeSlot', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['TimeTables.TimeSlots'])),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('CustomSlot', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('CustomSlotDetails', self.gf('picklefield.fields.PickledObjectField')()),
        ))
        db.send_create_signal('TimeTables', ['TabledEvents'])

        # Adding model 'TimeTable'
        db.create_table('TimeTables_timetable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('TabledEvent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['TimeTables.TabledEvents'])),
            ('EventDate', self.gf('django.db.models.fields.DateField')()),
            ('EventStartTime', self.gf('django.db.models.fields.TimeField')()),
            ('EventEndTime', self.gf('django.db.models.fields.TimeField')()),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal('TimeTables', ['TimeTable'])


    def backwards(self, orm):
        
        # Deleting model 'TimeSlots'
        db.delete_table('TimeTables_timeslots')

        # Deleting model 'Room'
        db.delete_table('TimeTables_room')

        # Deleting model 'Events'
        db.delete_table('TimeTables_events')

        # Removing M2M table for field Groups on 'Events'
        db.delete_table('TimeTables_events_Groups')

        # Deleting model 'EventDetails'
        db.delete_table('TimeTables_eventdetails')

        # Removing M2M table for field Staff on 'EventDetails'
        db.delete_table('TimeTables_eventdetails_Staff')

        # Deleting model 'MeetingEvent'
        db.delete_table('TimeTables_meetingevent')

        # Removing M2M table for field Staff on 'MeetingEvent'
        db.delete_table('TimeTables_meetingevent_Staff')

        # Deleting model 'OneToOneEvent'
        db.delete_table('TimeTables_onetooneevent')

        # Removing M2M table for field Staff on 'OneToOneEvent'
        db.delete_table('TimeTables_onetooneevent_Staff')

        # Removing M2M table for field Pupil on 'OneToOneEvent'
        db.delete_table('TimeTables_onetooneevent_Pupil')

        # Deleting model 'TabledEvents'
        db.delete_table('TimeTables_tabledevents')

        # Deleting model 'TimeTable'
        db.delete_table('TimeTables_timetable')


    models = {
        'MIS.address': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AddressLine1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'AddressLine2': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'AddressLine3': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'AddressLine4': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'HomeSalutation': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'object_name': 'Address'},
            'Note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Phone1': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'Phone2': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'Phone3': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'PostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'PostalTitle': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.group': {
            'MenuName': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'ordering': "['Name']", 'object_name': 'Group'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.menutypes': {
            'Desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Logo': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'MenuLevel': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'ordering': "['Name']", 'object_name': 'MenuTypes'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'schoolMenu'", 'to': "orm['MIS.School']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.pupil': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname', 'Forename']", 'object_name': 'Pupil'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Picture': ('django.db.models.fields.files.ImageField', [], {'default': "'images/StudentPhotos/Student.png'", 'max_length': '100'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'User': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.school': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Address']"}),
            'HeadTeacher': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'School'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'TemplateBanner': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBanner.jpg'", 'max_length': '200'}),
            'TemplateCSS': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBasic.css'", 'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.staff': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Address']", 'null': 'True', 'blank': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'DefaultMenu': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.MenuTypes']", 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'FailedLoginAttempts': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname']", 'object_name': 'Staff'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'User': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.subject': {
            'Desc': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Meta': {'ordering': "['Name']", 'object_name': 'Subject'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.eventdetails': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'Event': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['TimeTables.Events']", 'unique': 'True'}),
            'Meta': {'object_name': 'EventDetails'},
            'Room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['TimeTables.Room']", 'null': 'True', 'blank': 'True'}),
            'Staff': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['MIS.Staff']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.events': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['MIS.Group']", 'symmetrical': 'False'}),
            'Meta': {'object_name': 'Events'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'School': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.School']"}),
            'Subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Subject']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.meetingevent': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'Event': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['TimeTables.Events']", 'unique': 'True'}),
            'Meta': {'object_name': 'MeetingEvent'},
            'Staff': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['MIS.Staff']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.onetooneevent': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'Event': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['TimeTables.Events']", 'unique': 'True'}),
            'Meta': {'object_name': 'OneToOneEvent'},
            'Pupil': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['MIS.Pupil']", 'null': 'True', 'blank': 'True'}),
            'Staff': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['MIS.Staff']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.room': {
            'Location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.School']"}),
            'Meta': {'object_name': 'Room'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.tabledevents': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'CustomSlot': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'CustomSlotDetails': ('picklefield.fields.PickledObjectField', [], {}),
            'Event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['TimeTables.Events']"}),
            'Meta': {'object_name': 'TabledEvents'},
            'TimeSlot': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['TimeTables.TimeSlots']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.timeslots': {
            'BaseGroup': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Group']"}),
            'Cycle': ('django.db.models.fields.CharField', [], {'default': "'Weekly'", 'max_length': '20'}),
            'DatePeriod': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'DayOfWeek': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Duration': ('django.db.models.fields.IntegerField', [], {}),
            'Meta': {'object_name': 'TimeSlots'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'StartTime': ('django.db.models.fields.TimeField', [], {}),
            'StartWeek': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.timetable': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'EventDate': ('django.db.models.fields.DateField', [], {}),
            'EventEndTime': ('django.db.models.fields.TimeField', [], {}),
            'EventStartTime': ('django.db.models.fields.TimeField', [], {}),
            'Meta': {'object_name': 'TimeTable'},
            'TabledEvent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['TimeTables.TabledEvents']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['TimeTables']
