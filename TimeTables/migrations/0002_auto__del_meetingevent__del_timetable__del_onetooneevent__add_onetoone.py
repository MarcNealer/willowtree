# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting model 'MeetingEvent'
        db.delete_table('TimeTables_meetingevent')

        # Removing M2M table for field Staff on 'MeetingEvent'
        db.delete_table('TimeTables_meetingevent_Staff')

        # Deleting model 'TimeTable'
        db.delete_table('TimeTables_timetable')

        # Deleting model 'OneToOneEvent'
        db.delete_table('TimeTables_onetooneevent')

        # Removing M2M table for field Pupil on 'OneToOneEvent'
        db.delete_table('TimeTables_onetooneevent_Pupil')

        # Removing M2M table for field Staff on 'OneToOneEvent'
        db.delete_table('TimeTables_onetooneevent_Staff')

        # Adding model 'OneToOneTimeTable'
        db.create_table('TimeTables_onetoonetimetable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('StaffEvent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['TimeTables.OneToOneEvents'])),
            ('EventStaff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('EventDate', self.gf('django.db.models.fields.DateField')()),
            ('EventStartTime', self.gf('django.db.models.fields.TimeField')()),
            ('EventEndTime', self.gf('django.db.models.fields.TimeField')()),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal('TimeTables', ['OneToOneTimeTable'])

        # Adding model 'OneToOneEvents'
        db.create_table('TimeTables_onetooneevents', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('TimeSlot', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['TimeTables.TimeSlots'], unique=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal('TimeTables', ['OneToOneEvents'])

        # Adding M2M table for field Staff on 'OneToOneEvents'
        db.create_table('TimeTables_onetooneevents_Staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('onetooneevents', models.ForeignKey(orm['TimeTables.onetooneevents'], null=False)),
            ('staff', models.ForeignKey(orm['MIS.staff'], null=False))
        ))
        db.create_unique('TimeTables_onetooneevents_Staff', ['onetooneevents_id', 'staff_id'])

        # Adding M2M table for field Pupil on 'OneToOneEvents'
        db.create_table('TimeTables_onetooneevents_Pupil', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('onetooneevents', models.ForeignKey(orm['TimeTables.onetooneevents'], null=False)),
            ('pupil', models.ForeignKey(orm['MIS.pupil'], null=False))
        ))
        db.create_unique('TimeTables_onetooneevents_Pupil', ['onetooneevents_id', 'pupil_id'])

        # Adding model 'StaffTimeTable'
        db.create_table('TimeTables_stafftimetable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('StaffEvent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['TimeTables.StaffEvents'])),
            ('EventStaff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('EventDate', self.gf('django.db.models.fields.DateField')()),
            ('EventStartTime', self.gf('django.db.models.fields.TimeField')()),
            ('EventEndTime', self.gf('django.db.models.fields.TimeField')()),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal('TimeTables', ['StaffTimeTable'])

        # Adding model 'GroupTimeTable'
        db.create_table('TimeTables_grouptimetable', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('TabledEvent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['TimeTables.TabledEvents'])),
            ('EventStaff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('EventDate', self.gf('django.db.models.fields.DateField')()),
            ('EventStartTime', self.gf('django.db.models.fields.TimeField')()),
            ('EventEndTime', self.gf('django.db.models.fields.TimeField')()),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal('TimeTables', ['GroupTimeTable'])

        # Adding M2M table for field EventGroups on 'GroupTimeTable'
        db.create_table('TimeTables_grouptimetable_EventGroups', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('grouptimetable', models.ForeignKey(orm['TimeTables.grouptimetable'], null=False)),
            ('group', models.ForeignKey(orm['MIS.group'], null=False))
        ))
        db.create_unique('TimeTables_grouptimetable_EventGroups', ['grouptimetable_id', 'group_id'])

        # Adding model 'StaffEvents'
        db.create_table('TimeTables_staffevents', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('TimeSlot', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['TimeTables.TimeSlots'], unique=True)),
        ))
        db.send_create_signal('TimeTables', ['StaffEvents'])

        # Adding M2M table for field Staff on 'StaffEvents'
        db.create_table('TimeTables_staffevents_Staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('staffevents', models.ForeignKey(orm['TimeTables.staffevents'], null=False)),
            ('staff', models.ForeignKey(orm['MIS.staff'], null=False))
        ))
        db.create_unique('TimeTables_staffevents_Staff', ['staffevents_id', 'staff_id'])

        # Deleting field 'TabledEvents.CustomSlotDetails'
        db.delete_column('TimeTables_tabledevents', 'CustomSlotDetails')

        # Deleting field 'TabledEvents.CustomSlot'
        db.delete_column('TimeTables_tabledevents', 'CustomSlot')


    def backwards(self, orm):
        
        # Adding model 'MeetingEvent'
        db.create_table('TimeTables_meetingevent', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Event', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['TimeTables.Events'], unique=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=15)),
        ))
        db.send_create_signal('TimeTables', ['MeetingEvent'])

        # Adding M2M table for field Staff on 'MeetingEvent'
        db.create_table('TimeTables_meetingevent_Staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('meetingevent', models.ForeignKey(orm['TimeTables.meetingevent'], null=False)),
            ('staff', models.ForeignKey(orm['MIS.staff'], null=False))
        ))
        db.create_unique('TimeTables_meetingevent_Staff', ['meetingevent_id', 'staff_id'])

        # Adding model 'TimeTable'
        db.create_table('TimeTables_timetable', (
            ('TabledEvent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['TimeTables.TabledEvents'])),
            ('EventStartTime', self.gf('django.db.models.fields.TimeField')()),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('EventEndTime', self.gf('django.db.models.fields.TimeField')()),
            ('EventDate', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal('TimeTables', ['TimeTable'])

        # Adding model 'OneToOneEvent'
        db.create_table('TimeTables_onetooneevent', (
            ('Event', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['TimeTables.Events'], unique=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal('TimeTables', ['OneToOneEvent'])

        # Adding M2M table for field Pupil on 'OneToOneEvent'
        db.create_table('TimeTables_onetooneevent_Pupil', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('onetooneevent', models.ForeignKey(orm['TimeTables.onetooneevent'], null=False)),
            ('pupil', models.ForeignKey(orm['MIS.pupil'], null=False))
        ))
        db.create_unique('TimeTables_onetooneevent_Pupil', ['onetooneevent_id', 'pupil_id'])

        # Adding M2M table for field Staff on 'OneToOneEvent'
        db.create_table('TimeTables_onetooneevent_Staff', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('onetooneevent', models.ForeignKey(orm['TimeTables.onetooneevent'], null=False)),
            ('staff', models.ForeignKey(orm['MIS.staff'], null=False))
        ))
        db.create_unique('TimeTables_onetooneevent_Staff', ['onetooneevent_id', 'staff_id'])

        # Deleting model 'OneToOneTimeTable'
        db.delete_table('TimeTables_onetoonetimetable')

        # Deleting model 'OneToOneEvents'
        db.delete_table('TimeTables_onetooneevents')

        # Removing M2M table for field Staff on 'OneToOneEvents'
        db.delete_table('TimeTables_onetooneevents_Staff')

        # Removing M2M table for field Pupil on 'OneToOneEvents'
        db.delete_table('TimeTables_onetooneevents_Pupil')

        # Deleting model 'StaffTimeTable'
        db.delete_table('TimeTables_stafftimetable')

        # Deleting model 'GroupTimeTable'
        db.delete_table('TimeTables_grouptimetable')

        # Removing M2M table for field EventGroups on 'GroupTimeTable'
        db.delete_table('TimeTables_grouptimetable_EventGroups')

        # Deleting model 'StaffEvents'
        db.delete_table('TimeTables_staffevents')

        # Removing M2M table for field Staff on 'StaffEvents'
        db.delete_table('TimeTables_staffevents_Staff')

        # Adding field 'TabledEvents.CustomSlotDetails'
        db.add_column('TimeTables_tabledevents', 'CustomSlotDetails', self.gf('picklefield.fields.PickledObjectField')(default=''), keep_default=False)

        # Adding field 'TabledEvents.CustomSlot'
        db.add_column('TimeTables_tabledevents', 'CustomSlot', self.gf('django.db.models.fields.BooleanField')(default=False), keep_default=False)


    models = {
        'MIS.address': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AddressLine1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'AddressLine2': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'AddressLine3': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'AddressLine4': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'HomeSalutation': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'object_name': 'Address'},
            'Note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Phone1': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'Phone2': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'Phone3': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'PostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'PostalTitle': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.group': {
            'MenuName': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'ordering': "['Name']", 'object_name': 'Group'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.menutypes': {
            'Desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Logo': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'MenuLevel': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'ordering': "['Name']", 'object_name': 'MenuTypes'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'schoolMenu'", 'to': "orm['MIS.School']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.pupil': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname', 'Forename']", 'object_name': 'Pupil'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Picture': ('django.db.models.fields.files.ImageField', [], {'default': "'images/StudentPhotos/Student.png'", 'max_length': '100'}),
            'RegistrationDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2013, 4, 12)'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'User': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.school': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Address']"}),
            'HeadTeacher': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'School'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'TemplateBanner': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBanner.jpg'", 'max_length': '200'}),
            'TemplateCSS': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBasic.css'", 'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.staff': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Address']", 'null': 'True', 'blank': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'DefaultMenu': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.MenuTypes']", 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'FailedLoginAttempts': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname']", 'object_name': 'Staff'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'User': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'MIS.subject': {
            'Desc': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Meta': {'ordering': "['Name']", 'object_name': 'Subject'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.eventdetails': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'Event': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['TimeTables.Events']", 'unique': 'True'}),
            'Meta': {'object_name': 'EventDetails'},
            'Room': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['TimeTables.Room']", 'null': 'True', 'blank': 'True'}),
            'Staff': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['MIS.Staff']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.events': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['MIS.Group']", 'symmetrical': 'False'}),
            'Meta': {'object_name': 'Events'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'School': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.School']"}),
            'Subject': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Subject']", 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.grouptimetable': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'EventDate': ('django.db.models.fields.DateField', [], {}),
            'EventEndTime': ('django.db.models.fields.TimeField', [], {}),
            'EventGroups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['MIS.Group']", 'null': 'True', 'blank': 'True'}),
            'EventStaff': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Staff']"}),
            'EventStartTime': ('django.db.models.fields.TimeField', [], {}),
            'Meta': {'object_name': 'GroupTimeTable'},
            'TabledEvent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['TimeTables.TabledEvents']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.onetooneevents': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'Description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'OneToOneEvents'},
            'Pupil': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['MIS.Pupil']", 'null': 'True', 'blank': 'True'}),
            'Staff': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['MIS.Staff']", 'null': 'True', 'blank': 'True'}),
            'TimeSlot': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['TimeTables.TimeSlots']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.onetoonetimetable': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'EventDate': ('django.db.models.fields.DateField', [], {}),
            'EventEndTime': ('django.db.models.fields.TimeField', [], {}),
            'EventStaff': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Pupil']"}),
            'EventStartTime': ('django.db.models.fields.TimeField', [], {}),
            'Meta': {'object_name': 'OneToOneTimeTable'},
            'StaffEvent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['TimeTables.OneToOneEvents']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.room': {
            'Location': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.School']"}),
            'Meta': {'object_name': 'Room'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.staffevents': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'Description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'StaffEvents'},
            'Staff': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': "orm['MIS.Staff']", 'null': 'True', 'blank': 'True'}),
            'TimeSlot': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['TimeTables.TimeSlots']", 'unique': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.stafftimetable': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'EventDate': ('django.db.models.fields.DateField', [], {}),
            'EventEndTime': ('django.db.models.fields.TimeField', [], {}),
            'EventStaff': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Staff']"}),
            'EventStartTime': ('django.db.models.fields.TimeField', [], {}),
            'Meta': {'object_name': 'StaffTimeTable'},
            'StaffEvent': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['TimeTables.StaffEvents']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.tabledevents': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'Event': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['TimeTables.Events']"}),
            'Meta': {'object_name': 'TabledEvents'},
            'TimeSlot': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['TimeTables.TimeSlots']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'TimeTables.timeslots': {
            'BaseGroup': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['MIS.Group']"}),
            'Cycle': ('django.db.models.fields.CharField', [], {'default': "'Weekly'", 'max_length': '20'}),
            'DatePeriod': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'DayOfWeek': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Description': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Duration': ('django.db.models.fields.IntegerField', [], {}),
            'Meta': {'object_name': 'TimeSlots'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'StartTime': ('django.db.models.fields.TimeField', [], {}),
            'StartWeek': ('django.db.models.fields.IntegerField', [], {'default': '1'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['TimeTables']
