# -*- coding: utf-8 -*-
"""
Created on Thu Mar 28 11:11:22 2013

@author: dbmgr
"""

# Django middleware
from django.conf.urls import patterns, url, include
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.conf import settings

from TimeTables.views import *
from MIS.ViewExtras import SchoolAuth

urlpatterns = patterns('',
    url(r'TTmain/$',SchoolAuth(MainPage)),

    url(r'ManageSlots/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(SlotManager)),
    url(r'CreateSlot/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(CreateNewSlot)),
    url(r'ModifySlot/(?P<SlotId>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(ModifySlot)),
    url(r'DeleteSlot/(?P<SlotId>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(DeleteSlot)),    url(r'ManageBase/All/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(BaseManager),{'ViewType':'All'}),
    url(r'ManageBase/Unselected/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(BaseManager),{'ViewType':'Unselected'}),
    url(r'ManageBase/Selected/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(BaseManager),{'ViewType':'Selected'}),
    url(r'ManageBase/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(BaseManager),{'ViewType':'All'}),
    url(r'CreateBase/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(CreateBase)),
    url(r'ModifyBase/(?P<BaseId>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(ModifyBase)),
    url(r'DeleteBase/(?P<BaseId>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(DeleteBase)),
    url(r'ManageEvents/Unscheduled/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(CurrentEventManager),{'ViewType':'Unscheduled'}),
    url(r'ManageEvents/Scheduled/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(CurrentEventManager),{'ViewType':'Scheduled'}),
    url(r'AddEvent/(?P<ViewType>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(AddEvent)),
    url(r'ModifyEvent/(?P<ViewType>[^/]+)/(?P<EventId>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(ModifyEvent)),
    url(r'RemoveEvent/(?P<ViewType>[^/]+)/(?P<EventId>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(RemoveEvent)),
    url(r'ScheduleEvent/(?P<ViewType>[^/]+)/(?P<EventId>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(ScheduleAnEvent)),
    url(r'UnscheduleEvent/(?P<ViewType>[^/]+)/(?P<EventId>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(UnscheduleAnEvent)),
    url(r'SetAsCEvent/(?P<ViewType>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(SetAsCEvent)),
    url(r'ScheduleBEvent/(?P<ViewType>[^/]+)/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/$',SchoolAuth(ScheduleBEvent)),
    url(r'PupilTimeTables/(?P<Group1>[^/]+)/(?P<Group2>[^/]+)/(?P<PupilAcYear>[^/]+)/$',SchoolAuth(PupilGroupTimeTables)),
    url(r'StaffTimeTables/(?P<GroupName>[^/]+)/$',SchoolAuth(StaffGroupTimeTables)),
)