from MIS.models import Subject
from MIS.models import MenuTypes
from MIS.models import Group
from MIS.models import Staff
from TimeTables.models import *
from TimeTables import TimeSlotFunctions

def CreateBase(request,school):
    ''' Create a new Event record'''
    newEvent=Events(Name=request.POST['Name'],
                    Description=request.POST['Description'],
                    School=MenuTypes.objects.get(Name=school).SchoolId,
                    Subject=Subject.objects.get(id=int(request.POST['Subject'])))
    newEvent.save()
    newEvent.Groups.add(Group.objects.get(id=int(request.POST['GroupId'])))
    newEvent.save()
    return newEvent
    
def ModifyBase(request,RecordId):
    ER=Events.objects.get(id=int(RecordId))
    ER.Name=request.POST['Name']
    ER.Description=request.POST['Description']
    ER.Subject=Subject.objects.get(id=int(request.POST['Subject']))
    ER.Groups.clear()
    ER.Groups.add(Group.objects.get(id=int(request.POST['GroupId'])))
    ER.save()
    
def DeleteBase(request,RecordId):
    test=Events.objects.get(id=int(RecordId))
    test.delete()
    return
def GetActiveList():
    ''' Return a list of active Events'''
    return Events.objects.filter(Active=True)

def GetInactiveList():
    ''' Return a list of inactive Events'''
    return Events.objects.filter(Active=False)

def GetAssignedEvents(AcYear,school):
    ''' returns a list of events that have been assigned to EventDetails for 
    a given Academic year'''
    return Events.objects.filter(Active=True,
                                 id__in=[x.Event.id for x in EventDetails.objects.filter(AcademicYear=AcYear)],
                                School=MenuTypes.objects.get(Name=school).SchoolId)

def GetUnassignedEvents(AcYear,school):
    ''' returns a list of events that have not been assigned to EventDetails for
    a given year'''
    return Events.objects.filter(Active=True,
                                 School=MenuTypes.objects.get(Name=school).SchoolId).exclude(id__in=[x.Event.id for x in EventDetails.objects.filter(AcademicYear=AcYear)])
