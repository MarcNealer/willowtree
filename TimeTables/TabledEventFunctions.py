'''
This is a series of functions to help with
EventDetails records 

Created DBMGR: 25/03/2013

'''
from MIS.models import Subject
from MIS.models import MenuTypes
from MIS.models import Group
from MIS.models import Staff
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from TimeTables.models import *
from TimeTables import TimeSlotFunctions

import datetime
Weekdays={'Monday':0,'Tuesday':1,'Wednesday':2,'Thursday':3,'Friday':4,'Saturday':5,'Sunday':6}

def ScheduleEvent(request,EventId,AcYear):
    SlotRec=TimeSlots.objects.get(id=int(request.POST['Slot']))
    EventDetailsRec=EventDetails.objects.get(id=int(EventId))
    if TabledEvents.objects.filter(EventDetails=EventDetailsRec,AcademicYear=AcYear).exists():
        oldrec=TabledEvents.objects.get(EventDetails=EventDetailsRec,AcademicYear=AcYear)
        GroupTimeTable.objects.filter(TabledEvent=oldrec).delete()
        oldrec.delete()
    newTabledEvent=TabledEvents(EventDetails=EventDetailsRec,
                                TimeSlot=SlotRec,
                               AcademicYear=AcYear)
    newTabledEvent.save()
    StartDate,EndDate=getPeriodDates(SlotRec.DatePeriod,AcYear)
    datelist=getEventDates(StartDate,EndDate,SlotRec.Cycle,SlotRec.DayOfWeek)
    StartTime=SlotRec.StartTime
    StopTime=(datetime.datetime.combine(datetime.datetime.today(),SlotRec.StartTime) + datetime.timedelta(minutes=SlotRec.Duration)).time()
    for eachdate in datelist:

        TTRec=GroupTimeTable(TabledEvent=newTabledEvent,
                             EventDate=eachdate,
                             EventStartTime=StartTime,
                             EventEndTime=StopTime,
                             AcademicYear=AcYear)
        TTRec.save()
    return
    
def UnscheduleEvent(request,EventId,AcYear):
    EventDetailsRec=EventDetails.objects.get(id=int(EventId))
    if TabledEvents.objects.filter(EventDetails=EventDetailsRec,AcademicYear=AcYear).exists():
        oldrec=TabledEvents.objects.get(EventDetails=EventDetailsRec,AcademicYear=AcYear)
        GroupTimeTable.objects.filter(TabledEvent=oldrec).delete()
        oldrec.delete()
    return

def getEventDates(StartDate,EndDate,cycle,WeekDay):
    FoundDate=False
    if cycle=='Fortnightly':
        NoDays=13
    else:
        NoDays=6
    while StartDate < EndDate and FoundDate==False:
        if StartDate.weekday()==Weekdays[WeekDay]:
            FoundDate =True
            return [StartDate] + getEventDates(StartDate+datetime.timedelta(days=NoDays),EndDate,cycle,WeekDay)
        else:
            StartDate = StartDate +  datetime.timedelta(days=1)
    return []

def getPeriodDates(DatePeriod,AcYear):
    DateVariables={'Michaelmas Term':['Michaelmas.Start','Michaelmas.Stop'],
                   'Michaelmas 1st Half':['Michaelmas.Start','Michaelmas.HalfTerm.Start'],
                   'Michaelmas 2nd Half':['Michaelmas.HalfTerm.Stop','Michaelmas.Stop'],
                   'Lent Term':['Lent.Start','Lent.Stop'],
                   'Lent 1st Half':['Lent.Start','Lent.HalfTerm.Start'],
                   'Lent 2nd Half':['Lent.HalfTerm.Stop','Lent.Stop'],
                   'Summer Term':['Summer.Start','Summer.Stop'],
                   'Summer 1st Half':['Summer.Start','Summer.HalfTerm.Start'],
                   'Summer 2nd Half':['Summer.HalfTerm.Stop','Summer.Stop'],
                   'All AcYear':['Michaelmas.Start','Summer.Stop']}
    StartDate=GlobalVariables.Get_SystemVar(DateVariables[DatePeriod][0],AcYear)
    EndDate=GlobalVariables.Get_SystemVar(DateVariables[DatePeriod][1],AcYear)
    return [StartDate,EndDate]
    
def SpecialUpdate(tabledEvent,AcYear):
    SlotRec=tabledEvent.TimeSlot
    GroupTimeTable.objects.filter(TabledEvent=tabledEvent).delete()
    StartDate,EndDate=getPeriodDates(SlotRec.DatePeriod,AcYear)
    datelist=getEventDates(StartDate,EndDate,SlotRec.Cycle,SlotRec.DayOfWeek)
    StartTime=SlotRec.StartTime
    StopTime=(datetime.datetime.combine(datetime.datetime.today(),SlotRec.StartTime) + datetime.timedelta(minutes=SlotRec.Duration)).time()
    for eachdate in datelist:

        TTRec=GroupTimeTable(TabledEvent=tabledEvent,
                             EventDate=eachdate,
                             EventStartTime=StartTime,
                             EventEndTime=StopTime,
                             AcademicYear=AcYear)
        TTRec.save()
    return