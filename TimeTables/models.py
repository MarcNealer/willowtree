from django.db import models
import datetime
from picklefield.fields import PickledObjectField

DatePeriod_Choices = (
('Michaelmas Term', 'Michaelmas Term'),
('Michaelmas 1st Half', 'Michaelmas 1st Half'),
('Michaalmas 2nd Half', 'Michaelmas 2nd Half'),
('Lent Term', 'Lent Term'),
('Lent 1st Half', 'Lent 1st Half'),
('Lent 2nd Half', 'Lent 2nd Half'),
('Summer Term', 'Summer Term'),
('Summer 1st Half', 'Summer 1st Half'),
('Summer 2nd Half', 'Summer 2nd Half'),
('All AcYear', 'All AcYear')
)
TimeSlotCycles = (
('Weekly', 'Weekly'),
('Fortnightly', 'Fortnightly') 
)
DaysOfTheWeek = (
('Monday', 'Monday'),
('Tuesday', 'Tuesday'),
('Wednesday', 'Wednesday'),
('Thursday', 'Thursday'),
('Friday', 'Friday'),
('Saturday', 'Saturday'),
('Sunday', 'Sunday')
)
DayNumberOfWeek =(
(0,0),
(1,1),
(2,2),
(3,3),
(4,4),
(5,5),
(6,6),
(7,7))
EventTypes = (
('Class', 'Class'),
('Meeting', 'Meeting'),
('OneToOne', 'OneToOne'),
('AllSchool', 'AllSchool'),
)

#  # # # # # # # # # # # # # # # # # # # # # ##  # #
#   Time table Models
#
# # # # # # # ## # # # # # # # # # ## # # # ## ##


class TimeSlots(models.Model):
    Name = models.CharField(max_length=200)
    Description = models.TextField(blank=True)
    BaseGroup = models.ForeignKey('MIS.Group')
    DatePeriod = models.CharField(max_length=40, choices=DatePeriod_Choices)
    DayNumber = models.IntegerField(choices=DayNumberOfWeek)
    Cycle = models.CharField(max_length=20, choices=TimeSlotCycles,
                             default='Weekly')
    StartTime = models.TimeField()
    Duration = models.IntegerField()
    StartWeek = models.IntegerField(default=1)

    def __unicode__(self):
        return self.Name
    def _get_DayOfWeek(self):
        dayList=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']
        return dayList[self.DayNumber]
    DayOfWeek=property(_get_DayOfWeek)


class Room(models.Model):
    ''' Room objects For time tables '''
    Name = models.CharField(max_length=100)
    Location = models.ForeignKey('MIS.School')

    def __unicode__(self):
        return self.Name


class Events(models.Model):
    ''' Events used for timetables. Holds details on Who, What, and where. When
    is sorted in the Timeslots '''
    Name = models.CharField(max_length=200)
    Description = models.TextField(blank=True)
    School = models.ForeignKey('MIS.School')
    Groups = models.ManyToManyField('MIS.Group')
    Subject = models.ForeignKey('MIS.Subject', blank=True, null=True)
    Active = models.BooleanField(default=True)
    
    def __unicode__(self):
        return self.Name
        

class EventDetails(models.Model):
    ''' These records are links from an event to one ore more Groups
    staff and subject'''
    Event = models.OneToOneField('Events')
    AcademicYear = models.CharField(max_length=15)    
    Staff = models.ManyToManyField('MIS.Staff', blank=True, null=True)
    Room = models.ForeignKey('Room', blank=True, null=True)
    
    def __unicode__(self):
        return "current event for %s" % self.Event.Name
        
    def Teachers(self):
        teacherlist=[]
        for teachers in self.Staff.all():
            teacherlist.append(teachers.PrintName())
        return ' and '.join(teacherlist)
            
    def Groups(self):
        grouplist=[]
        for groups in self.Event.Groups.all():
            grouplist.append('.'.join(groups.Name.split('.')[-3:]))
        return ' and'.join(grouplist)            

    
class StaffEvents(models.Model):
    ''' These records are links from an event to one or more staff members
    for a meeting'''
    Description=models.TextField(blank=True, null=True)
    AcademicYear = models.CharField(max_length=15)    
    TimeSlot = models.OneToOneField('TimeSlots')
    Staff = models.ManyToManyField('MIS.Staff', blank=True, null=True)

class OneToOneEvents(models.Model):
    ''' These records afre links from an event to single pupils for One to One
    classes and sessions'''
    Description=models.TextField(blank=True, null=True)
    TimeSlot = models.OneToOneField('TimeSlots')    
    AcademicYear = models.CharField(max_length=15)    
    Staff = models.ManyToManyField('MIS.Staff', blank=True, null=True)
    Pupil = models.ManyToManyField('MIS.Pupil', blank=True, null=True)    


class TabledEvents(models.Model):
    ''' Record to link Events and Timeslots together'''
    EventDetails=models.ForeignKey('EventDetails')
    TimeSlot = models.ForeignKey('TimeSlots')
    AcademicYear = models.CharField(max_length=10)
    
    def __unicode__(self):
        return "%s : %s" % (self.EventDetails.Event.Name, self.TimeSlot.Name)



class GroupTimeTable(models.Model):
    ''' These are the finished Class or group timetable objects. each one Represents as single
    event at a single date and time. They are created via refences to the
    Tabled events'''
    TabledEvent = models.ForeignKey('TabledEvents')
    EventDate = models.DateField()
    EventStartTime = models.TimeField()
    EventEndTime = models.TimeField()
    AcademicYear = models.CharField(max_length=10)
    Clash=False
    SchoolBreak=False
    testtime=0
    
    def __unicode__(self):
        return "%s: %s : %s " % (self.TabledEvent.EventDetails.Event.Name, self.EventDate,self.Teachers())
    
    def ClashTest(self,TTObject):
        if self.EventEndTime > TTObject.EventStartTime:
            self.Clash=True
            TTObject.Clash=True
        return self.Clash
    def BreakTest(self,TTobject):
        if not self.Clash:
            time1=datetime.datetime(2014,1,1,self.EventEndTime.hour,self.EventEndTime.minute)
            time2=datetime.datetime(2014,1,1,TTobject.EventStartTime.hour,TTobject.EventStartTime.minute)
            testtime=time2-time1
            testtime=(testtime.seconds)/60
            self.testtime=testtime
            if testtime > 15.0:
                self.SchoolBreak=True
        return self.SchoolBreak
           
    def Subject(self):
        return self.TabledEvent.EventDetails.Event.Subject.Name
    def Teachers(self):
        return self.TabledEvent.EventDetails.Teachers()
    def Groups(self):
        return self.TabledEvent.EventDetails.Groups()
    def Room(self):
        if self.TabledEvent.EventDetails.Room:
            return self.TabledEvent.EventDetails.Room.Name
        else:
            return ''
    def Times(self):
        return "%s-%s" % (self.EventStartTime.strftime("%H:%M"),self.EventEndTime.strftime("%H:%M"))
    
class StaffTimeTable(models.Model):
    ''' These are the finished Staff only timetable objects. each one Represents a single
    event at a single date and time. They are created via refences to the
    Tabled events'''
    StaffEvent = models.ForeignKey('StaffEvents')
    EventStaff = models.ForeignKey('MIS.Staff')
    EventDate = models.DateField()
    EventStartTime = models.TimeField()
    EventEndTime = models.TimeField()
    AcademicYear = models.CharField(max_length=10)
 
 
class OneToOneTimeTable(models.Model):
    ''' These are the finished Staff only timetable objects. each one Represents a single
    event at a single date and time. They are created via refences to the
    Tabled events'''
    StaffEvent = models.ForeignKey('OneToOneEvents')
    EventStaff = models.ForeignKey('MIS.Staff')
    EventStaff = models.ForeignKey('MIS.Pupil')    
    EventDate = models.DateField()
    EventStartTime = models.TimeField()
    EventEndTime = models.TimeField()
    AcademicYear = models.CharField(max_length=10)
 
  