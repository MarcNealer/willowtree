'''
This is a series of functions to help with
EventDetails records 

Created DBMGR: 25/03/2013

'''
from MIS.models import Subject
from MIS.models import MenuTypes
from MIS.models import Group
from MIS.models import Staff
from TimeTables.models import *
from TimeTables import TimeSlotFunctions
from TimeTables import TabledEventFunctions

def ModifyEvent(request,EventId):
    EventRec=EventDetails.objects.get(id=int(EventId))
    if 'Staff' in request.POST:
        EventRec.Staff.clear()
        for staffid in request.POST.getlist('Staff'):
            EventRec.Staff.add(Staff.objects.get(id=int(staffid)))
    if 'Room' in request.POST:
        EventRec.Room=Room.objects.get(id=int(request.POST['Room']))
    EventRec.save()
    return
    
def ModifyScheduledEvent(request,EventId,AcYear):
    ModifyEvent(request,EventId)
    TabledEventFunctions.ScheduleEvent(request,EventId,AcYear)
    return

def RemoveEvent(EventId):
    EventRec=EventDetails.objects.get(id=int(EventId))
    EventRec.delete()
    return

def AddEvent(request,AcYear,ViewType):
    EventRec=EventDetails(Event=Events.objects.get(id=int(request.POST['BaseEvent'])),AcademicYear=AcYear)
    EventRec.save()
    if 'Staff' in request.POST:
        for staffids in request.POST.getlist('Staff'):
            EventRec.Staff.add(Staff.objects.get(id=int(staffids)))
    if 'Room' in request.POST:
        EventRec.Room=Room.objects.get(id=int(request.POST['Room']))
    EventRec.save()
    if ViewType == 'Scheduled':
        TabledEventFunctions.ScheduleEvent(request,EventRec.id,AcYear)
    return EventRec
    
class ManageEventDetails():
    def __init__(self,request,EventId):
        self.PostData=request.POST
        self.Event=EventDetails.objects.get(id=int(EventId))
    def DeleteDetails(self):
        self.Event.delete()
        pass

    def ModifyDetails(self):
        if 'Staff' in self.PostData:
            self.Event.Staff.clear()
            for Staff in self.PostData.getlist('Staff'):
                self.Event.Staff.add(Staff.objects.get(id=int('Staff')))
        if 'Room' in self.PostData:
            self.Event.Room=Room.objects.get(Name=self.PostData['Room'])
        pass

    def CopyToNewYear(self,AcYear):
        if not EventDetails.objects.filter(AcademicYear=AcYear,Event=self.Event.Event).exists():
            newEvent=EventDetails(AcademicYear=Acyear,Event=self.Event.Event)
            newEvent.save()
            if self.Event.Room:
                newEvent.Room=self.Event.Room
            for Staff in self.Event.Staff.all():
                newEvent.Staff.add(Staff)
            newEvent.save()
            return newEvent
        else:
            return EventDetails.objects.get(AcademicYear=AcYear,Event=self.Event.Event)
            
def ActiveEventDetails(AcYear, school):
    return EventDetails.objects.filter(AcademicYear=AcYear,
                                       Event__School=MenuType.objects.filter(Name=school).SchoolId)

class ScheduledEvent():
    def __init__(self,AcYear,Event):
        self.EventRecord=Event
        self.AcYear
    def IsScheduled(self):
        if TabledEvents.objects.filter(Event=self.EventRecord,AcademicYear=self.AcYear).exists():
            self.Details=EventDetails.objects.get(Event=self.EventRecord,AcademicYear=self.AcYear)
            self.TabledEvent=TabledEvents.TabledEvents.objects.filter(Event=self.EventRecord,AcademicYear=self.AcYear)
            return True
        else:
            return False

def GroupScheduledEvents(self,AcYear,Group1,Group2):
    if 'None' in Group2:
        GroupList=Group.objects.filter(Name__icontains=Group1).filter(Name__icontains=request.session['School'].Name).filter(Name__istartswith='Current')
    else:
        GroupList=Group.objects.filter(Name__icontains=Group1).filter(Name__icontains=Group2).filter(Name__icontains=request.session['School'].Name).filter(Name__istartswith='Current') 
    SubGroupList=Group.objects.filter(Name__in=BuildGroupList(GroupList[0].Name))
    BaseList=Events.objects.filter(Groups__in=[x for x in SubGroupList])
    EventList=[]
    for items in BaseList:
        item=ScheduledEvent(AcYear,items)
        if item.IsScheduled():
            EventList.append(item)
    return EventList
        
