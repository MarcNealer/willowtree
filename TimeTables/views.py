'''
Views for timetable management.

Created By DBMGR 28/03/2013

'''

import time

from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.conf import settings

from TimeTables.models import *
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.models import Group,Subject,Staff,StaffGroup
from TimeTables.TimeSlotFunctions import *
from TimeTables import EventFunctions
from TimeTables import EventDetailsFunctions
from TimeTables import TabledEventFunctions
from TimeTables import PrintTimeTablesFunctions

DayNumberList=['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday']

def TimeTableContext(request):
    ''' This is the context Proccessor for the Timetable pages'''

    school = request.META['PATH_INFO'].split('/')[1]
    AcYear = request.META['PATH_INFO'].split('/')[2]
    Logo = request.session['WillowLogo']

    if 'Messages' in request.session:
        Messages = request.session['Messages']
        del request.session['Messages']
    else:
        Messages = []

    return {'Logo': request.session['WillowLogo'],
            'school': school,
            'AcYear': AcYear,
            'UserName': request.user.username,
            'ActionTime': time.strftime("%H:%M", time.localtime()),
            'ActionDate': time.strftime("%d/%m/%y", time.localtime()),
            'LoggedIn': True, 'Messages': Messages,
            'CurrentYear': GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')}

def MainPage(request, school, AcYear):

    return render_to_response('TTHomePage.html', {},
                              context_instance=RequestContext(request,processors=[TimeTableContext]))

# # # # # # # #
#
#  Slots
#
# # # # # ## # # # # #

def SlotManager(request, school, AcYear, Group1, Group2):
    if 'None' in Group2:
        GroupList=Group.objects.filter(Name__icontains=Group1).filter(Name__icontains=request.session['School'].Name).filter(Name__istartswith='Current')
    else:
        GroupList=Group.objects.filter(Name__icontains=Group1).filter(Name__icontains=Group2).filter(Name__icontains=request.session['School'].Name).filter(Name__istartswith='Current')
    SubGroupList=Group.objects.filter(Name__in=BuildGroupList(GroupList))
    SlotList=TimeSlots.objects.filter(BaseGroup__in=[x for x in SubGroupList]).order_by('DayNumber','StartTime')
    return render_to_response('TTSlotManager.html',{'Slots':SlotList,'GroupList':SubGroupList,
                                                    'Group1':Group1,'Group2':Group2},
                              context_instance=RequestContext(request,processors=[TimeTableContext]))


def CreateNewSlot(request,school,AcYear,Group1,Group2):
    newSlot=TimeSlots(Name=request.POST['Name'],
                     Description=request.POST['Description'],
                     BaseGroup=Group.objects.get(id=request.POST['GroupName']),
                     DatePeriod=request.POST['DatePeriod'],
                     DayNumber=DayNumberList.index(request.POST['DayOfWeek']),
                     Cycle=request.POST['Cycle'],
                     StartTime=request.POST['StartTime'],
                     Duration=request.POST['Duration'])
    newSlot.save()
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageSlots/%s/%s/' % (school,AcYear,Group1,Group2))

def ModifySlot(request,school,AcYear,SlotId,Group1,Group2):
    ManageSlots.Update(int(SlotId),request.POST)
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageSlots/%s/%s/' % (school,AcYear,Group1,Group2))

def DeleteSlot(request,school,AcYear,SlotId,Group1,Group2):
    SlotRecord=ManageSlots(request,int(SlotId))
    SlotRecord.Delete()
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageSlots/%s/%s/' % (school,AcYear,Group1,Group2))

# # # # # # # # # # # # # # # # # # # # #
#
# Base Events
#
# # # # # # # # # # # # # # # # # # # # #

def BaseManager(request,school,AcYear,Group1, Group2,ViewType):
    if 'None' in Group2:
        GroupList=Group.objects.filter(Name__icontains=Group1).filter(Name__icontains=request.session['School'].Name).filter(Name__istartswith='Current')
    else:
        GroupList=Group.objects.filter(Name__icontains=Group1).filter(Name__icontains=Group2).filter(Name__icontains=request.session['School'].Name).filter(Name__istartswith='Current')
    SubGroupList=Group.objects.filter(Name__in=BuildGroupList(GroupList))
    if ViewType == 'Selected':
        EventList=Events.objects.filter(Groups__in=[x for x in SubGroupList]).filter(id__in=[x.Event.id for x in EventDetails.objects.filter(AcademicYear=AcYear)])
        BannerTitle = 'Selected Base Events Manager'
    elif ViewType == 'Unselected':
        EventList=Events.objects.filter(Groups__in=[x for x in SubGroupList]).exclude(id__in=[x.Event.id for x in EventDetails.objects.filter(AcademicYear=AcYear)])
        BannerTitle = 'Unselected Base Events Manager'
    else:
        EventList=Events.objects.filter(Groups__in=[x for x in SubGroupList])
        BannerTitle='All Base Events Manager'

    SlotList=TimeSlots.objects.filter(BaseGroup__in=[x for x in SubGroupList])
    SubjectList=Subject.objects.all()
    TeacherList=Staff.objects.filter(id__in=[x.Staff.id for x in StaffGroup.objects.filter(Group__Name__icontains=request.session['School'].Name)])
    RoomList=Room.objects.filter(Location=request.session['School'])
    return render_to_response('TTBaseManager.html',{'Events':EventList.order_by('Name'),
                                                    'BannerTitle':BannerTitle,
                                                    'GroupList':SubGroupList,
                                                    'Group1':Group1,
                                                    'Group2':Group2,
                                                    'SubjectList':SubjectList,
                                                    'TeacherList':TeacherList,
                                                    'RoomList':RoomList,
                                                    'ViewType':ViewType,
                                                    'SlotList':SlotList},
                              context_instance=RequestContext(request,processors=[TimeTableContext]))

    EventList=Events.objects.filter(Groups__in=[x for x in SubGroupList]).exclude(id__in=[x.Event.id for x in EventDetails.objects.filter(AcademicYear=AcYear)])


def CreateBase(request,school,AcYear,Group1,Group2):
    NewEvent=EventFunctions.CreateBase(request,school)
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageBase/%s/%s/' % (school,AcYear,Group1,Group2))

def ModifyBase(request,school,AcYear,BaseId,Group1,Group2):
    EventR=EventFunctions.ModifyBase(request,BaseId)
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageBase/%s/%s/' % (school,AcYear,Group1,Group2))

def DeleteBase(request,school,AcYear,BaseId,Group1,Group2):
    EventFunctions.DeleteBase(request,int(BaseId))
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageBase/%s/%s/' % (school,AcYear,Group1,Group2))

def SetAsCEvent(request,school,AcYear,Group1,Group2,ViewType):
    EventDetailsFunctions.AddEvent(request,AcYear,ViewType)
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageBase/%s/%s/%s/' % (school,AcYear,ViewType,Group1,Group2))

def ScheduleBEvent(request,school,AcYear,Group1,Group2,ViewType):
    EventRec=EventDetailsFunctions.AddEvent(request,AcYear,'Scheduled')
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageBase/%s/%s/%s/' % (school,AcYear,ViewType,Group1,Group2))


# # # # # # # # # # # # # # # # # # # # # # #
#
#
# Current Event Views
#
#
# # # # # # # # # # # # # # # # # # # # # # #


def CurrentEventManager(request, school, AcYear, Group1, Group2, ViewType):
    if 'None' in Group2:
        GroupList = Group.objects.filter(Name__icontains=Group1).filter(Name__icontains=request.session['School'].Name).filter(Name__istartswith='Current')
    else:
        GroupList = Group.objects.filter(Name__icontains=Group1).filter(Name__icontains=Group2).filter(Name__icontains=request.session['School'].Name).filter(Name__istartswith='Current')
    SubGroupList=Group.objects.filter(Name__in=BuildGroupList(GroupList))
    if ViewType == 'Scheduled':
        Eventlist = EventDetails.objects.filter(Event__Groups__in=SubGroupList,AcademicYear=AcYear).filter(id__in=[x.EventDetails.id for x in TabledEvents.objects.filter(AcademicYear=AcYear)])
        BannerTitle='Scheduled Events Manager'
    else:
        Eventlist = EventDetails.objects.filter(Event__Groups__in=SubGroupList,AcademicYear=AcYear).exclude(id__in=[x.EventDetails.id for x in TabledEvents.objects.filter(AcademicYear=AcYear)])
        BannerTitle='unscheduled Events Manager'

    SubjectList=Subject.objects.all()
    TeacherList=Staff.objects.filter(id__in=[x.Staff.id for x in StaffGroup.objects.filter(Group__Name__icontains=request.session['School'].Name)])
    RoomList=Room.objects.filter(Location=request.session['School'])

    BaseEventList=Events.objects.filter(Groups__in=[x for x in SubGroupList]).exclude(id__in=[x.Event.id for x in EventDetails.objects.filter(AcademicYear=AcYear)])
    SlotList=TimeSlots.objects.filter(BaseGroup__in=[x for x in SubGroupList])
    return render_to_response('TTEventManager.html',{'Events':Eventlist,
                                                    'BannerTitle':BannerTitle,
                                                    'GroupList':SubGroupList,
                                                    'Group1':Group1,
                                                    'Group2':Group2,
                                                    'SubjectList':SubjectList,
                                                    'TeacherList':TeacherList,
                                                    'RoomList':RoomList,
                                                    'BaseEventList':BaseEventList,
                                                    'SlotList':SlotList,
                                                    'ViewType':ViewType},
                              context_instance=RequestContext(request,processors=[TimeTableContext]))

def AddEvent(request,school,AcYear,Group1,Group2,ViewType):
    EventDetailsFunctions.AddEvent(request,AcYear,ViewType)
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageEvents/%s/%s/%s/' % (school,AcYear,ViewType,Group1,Group2))


def ModifyEvent(request, school, AcYear,EventId,Group1,Group2,ViewType):
    if ViewType=='Scheduled':
        EventDetailsFunctions.ModifyScheduledEvent(request,EventId,AcYear)
    else:
        EventDetailsFunctions.ModifyEvent(request,EventId)
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageEvents/%s/%s/%s/' % (school,AcYear,ViewType,Group1,Group2))

def RemoveEvent(request,school,AcYear,EventId,Group1,Group2,ViewType):
    EventDetailsFunctions.RemoveEvent(int(EventId))
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageEvents/%s/%s/%s/' % (school,AcYear,ViewType,Group1,Group2))

def ScheduleAnEvent(request,school,AcYear,EventId,Group1,Group2,ViewType):
    TabledEventFunctions.ScheduleEvent(request,EventId,AcYear)
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageEvents/%s/%s/%s/' % (school,AcYear,ViewType,Group1,Group2))

def UnscheduleAnEvent(request,school,AcYear,EventId,Group1,Group2,ViewType):
    TabledEventFunctions.UnscheduleEvent(request,EventId,AcYear)
    return HttpResponseRedirect('/WillowTree/%s/%s/TimeTables/ManageEvents/%s/%s/%s/' % (school,AcYear,ViewType,Group1,Group2))



def PupilGroupTimeTables(request,school,AcYear,Group1,Group2,PupilAcYear):
    if 'None' in Group2:
        GroupName = Group.objects.filter(Name__iendswith=Group1).filter(Name__icontains=request.session['School'].Name).filter(Name__istartswith='Current')[0].Name
    else:
        GroupName = Group.objects.filter(Name__icontains=Group2).filter(Name__iendswith=Group1).filter(Name__icontains=request.session['School'].Name).filter(Name__istartswith='Current')[0].Name
    tablelist=PrintTimeTablesFunctions.TableList(GroupName,PupilAcYear,AcYear)
    c={'PupilTables':tablelist}
    return render_to_response('StandardPupilTable.html',c,context_instance=RequestContext(request,processors=[TimeTableContext]))

def StaffGroupTimeTables(request,school,AcYear,GroupName):
    tablelist=PrintTimeTablesFunctions.StaffTimeTableList(GroupName,AcYear)
    c={'StaffTables':tablelist}
    return render_to_response('StandardStaffTable.html',c,context_instance=RequestContext(request,processors=[TimeTableContext]))