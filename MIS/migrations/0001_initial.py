# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'School'
        db.create_table(u'MIS_school', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('HeadTeacher', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('TemplateCSS', self.gf('django.db.models.fields.CharField')(default='LetterCSS/LetterBasic.css', max_length=200)),
            ('TemplateBanner', self.gf('django.db.models.fields.CharField')(default='LetterCSS/LetterBanner.jpg', max_length=200)),
            ('Address', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Address'])),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['School'])

        # Adding model 'Family'
        db.create_table(u'MIS_family', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('FamilyName', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['Family'])

        # Adding M2M table for field Address on 'Family'
        db.create_table(u'MIS_family_Address', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('family', models.ForeignKey(orm[u'MIS.family'], null=False)),
            ('address', models.ForeignKey(orm[u'MIS.address'], null=False))
        ))
        db.create_unique(u'MIS_family_Address', ['family_id', 'address_id'])

        # Adding model 'Pupil'
        db.create_table(u'MIS_pupil', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Forename', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('OtherNames', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('Surname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('NickName', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('EmailAddress', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('Title', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('DateOfBirth', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(1972, 1, 1, 0, 0))),
            ('Gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('Picture', self.gf('django.db.models.fields.files.ImageField')(default='media/images/StudentPhotos/Student.png', max_length=100)),
            ('Old_Id', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('User', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], unique=True, null=True, blank=True)),
            ('RegistrationDate', self.gf('django.db.models.fields.DateField')(default=datetime.date(2014, 4, 25))),
        ))
        db.send_create_signal(u'MIS', ['Pupil'])

        # Adding model 'Contact'
        db.create_table(u'MIS_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Forename', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('OtherNames', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('Surname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('NickName', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('EmailAddress', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('Title', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('DateOfBirth', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(1972, 1, 1, 0, 0))),
            ('Gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('Address', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Address'], null=True, blank=True)),
            ('WorkAddress', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='WorkAddress', null=True, to=orm['MIS.Address'])),
            ('Old_Id', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
        ))
        db.send_create_signal(u'MIS', ['Contact'])

        # Adding M2M table for field FamilyId on 'Contact'
        db.create_table(u'MIS_contact_FamilyId', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('contact', models.ForeignKey(orm[u'MIS.contact'], null=False)),
            ('family', models.ForeignKey(orm[u'MIS.family'], null=False))
        ))
        db.create_unique(u'MIS_contact_FamilyId', ['contact_id', 'family_id'])

        # Adding M2M table for field Pupils on 'Contact'
        db.create_table(u'MIS_contact_Pupils', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('contact', models.ForeignKey(orm[u'MIS.contact'], null=False)),
            ('pupil', models.ForeignKey(orm[u'MIS.pupil'], null=False))
        ))
        db.create_unique(u'MIS_contact_Pupils', ['contact_id', 'pupil_id'])

        # Adding model 'Staff'
        db.create_table(u'MIS_staff', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Forename', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('OtherNames', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('Surname', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('NickName', self.gf('django.db.models.fields.CharField')(max_length=50, blank=True)),
            ('EmailAddress', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('Title', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('DateOfBirth', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(1972, 1, 1, 0, 0))),
            ('Gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('Address', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Address'], null=True, blank=True)),
            ('Picture', self.gf('django.db.models.fields.files.ImageField')(max_length=100, null=True, blank=True)),
            ('Old_Id', self.gf('django.db.models.fields.CharField')(max_length=40, blank=True)),
            ('User', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True, null=True, blank=True)),
            ('DefaultMenu', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.MenuTypes'], null=True, blank=True)),
            ('FailedLoginAttempts', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('ResetPassword', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
        ))
        db.send_create_signal(u'MIS', ['Staff'])

        # Adding model 'StaffEmergencyContact'
        db.create_table(u'MIS_staffemergencycontact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('StaffId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('AddressId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Address'])),
            ('Priority', self.gf('django.db.models.fields.SmallIntegerField')()),
        ))
        db.send_create_signal(u'MIS', ['StaffEmergencyContact'])

        # Adding model 'Address'
        db.create_table(u'MIS_address', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('HomeSalutation', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('PostalTitle', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('AddressLine1', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('AddressLine2', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('AddressLine3', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('AddressLine4', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('PostCode', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Country', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('Phone1', self.gf('django.db.models.fields.CharField')(max_length=150, blank=True)),
            ('Phone2', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('Phone3', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('EmailAddress', self.gf('django.db.models.fields.EmailField')(max_length=75, blank=True)),
            ('Note', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['Address'])

        # Adding model 'FamilyContact'
        db.create_table(u'MIS_familycontact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('FamilyId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Family'])),
            ('Contact', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Contact'])),
            ('Relationship', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.FamilyRelationship'])),
            ('Priority', self.gf('django.db.models.fields.SmallIntegerField')(null=True, blank=True)),
            ('RelationshipDesc', self.gf('django.db.models.fields.TextField')(max_length=500, blank=True)),
        ))
        db.send_create_signal(u'MIS', ['FamilyContact'])

        # Adding model 'PupilContact'
        db.create_table(u'MIS_pupilcontact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('PupilId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('FamilyContactId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.FamilyContact'])),
        ))
        db.send_create_signal(u'MIS', ['PupilContact'])

        # Adding model 'FamilyChildren'
        db.create_table(u'MIS_familychildren', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('FamilyId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Family'])),
            ('Pupil', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('RelationshipDesc', self.gf('django.db.models.fields.TextField')(max_length=500, blank=True)),
            ('FamilyType', self.gf('django.db.models.fields.SmallIntegerField')(default=1)),
        ))
        db.send_create_signal(u'MIS', ['FamilyChildren'])

        # Adding model 'FamilyRelationship'
        db.create_table(u'MIS_familyrelationship', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Type', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('RelType', self.gf('django.db.models.fields.CharField')(max_length=1)),
        ))
        db.send_create_signal(u'MIS', ['FamilyRelationship'])

        # Adding model 'SchoolList'
        db.create_table(u'MIS_schoollist', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('Type', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('HeadTeacher', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('Address', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Address'])),
            ('Desc', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'MIS', ['SchoolList'])

        # Adding model 'PupilFutureSchoolProgress'
        db.create_table(u'MIS_pupilfutureschoolprogress', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Pupil', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('School', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.SchoolList'])),
            ('LastUpdatedBy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('Progress', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('EntryYear', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('Interview', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
            ('Details', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['PupilFutureSchoolProgress'])

        # Adding model 'PupilsPastSchool'
        db.create_table(u'MIS_pupilspastschool', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('PupilId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('SchoolId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.SchoolList'])),
        ))
        db.send_create_signal(u'MIS', ['PupilsPastSchool'])

        # Adding model 'PupilsNextSchool'
        db.create_table(u'MIS_pupilsnextschool', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('PupilId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('SchoolId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.SchoolList'])),
        ))
        db.send_create_signal(u'MIS', ['PupilsNextSchool'])

        # Adding model 'Group'
        db.create_table(u'MIS_group', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=500)),
            ('MenuName', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'MIS', ['Group'])

        # Adding model 'PupilGroup'
        db.create_table(u'MIS_pupilgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('Pupil', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('Group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('DateAdded', self.gf('django.db.models.fields.DateField')(default=datetime.date(2014, 4, 25))),
            ('DateInactivated', self.gf('django.db.models.fields.DateField')(default=datetime.date(2014, 4, 25))),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['PupilGroup'])

        # Adding model 'StaffGroup'
        db.create_table(u'MIS_staffgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('Staff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('Group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
        ))
        db.send_create_signal(u'MIS', ['StaffGroup'])

        # Adding unique constraint on 'StaffGroup', fields ['AcademicYear', 'Staff', 'Group']
        db.create_unique(u'MIS_staffgroup', ['AcademicYear', 'Staff_id', 'Group_id'])

        # Adding model 'StaffGroupFavourite'
        db.create_table(u'MIS_staffgroupfavourite', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Staff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('Group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('PoolName', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'MIS', ['StaffGroupFavourite'])

        # Adding model 'RollOverRule'
        db.create_table(u'MIS_rolloverrule', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('NewGroup', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('OldGroup', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='RollOver_OldGroup', null=True, to=orm['MIS.Group'])),
            ('ApplicantGroup', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='RollOver_AppGroup', null=True, to=orm['MIS.Group'])),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['RollOverRule'])

        # Adding model 'Subject'
        db.create_table(u'MIS_subject', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('Desc', self.gf('django.db.models.fields.CharField')(max_length=300)),
        ))
        db.send_create_signal(u'MIS', ['Subject'])

        # Adding model 'ParentSubjects'
        db.create_table(u'MIS_parentsubjects', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Subject'])),
            ('ParentSubject', self.gf('django.db.models.fields.related.ForeignKey')(related_name='ParentSubject', to=orm['MIS.Subject'])),
        ))
        db.send_create_signal(u'MIS', ['ParentSubjects'])

        # Adding model 'EventQueue'
        db.create_table(u'MIS_eventqueue', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Priority', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('Group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('Type', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Details', self.gf('django.db.models.fields.TextField')()),
            ('TimeStamp', self.gf('django.db.models.fields.DateTimeField')()),
        ))
        db.send_create_signal(u'MIS', ['EventQueue'])

        # Adding model 'EventTrigger'
        db.create_table(u'MIS_eventtrigger', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Priority', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('Group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('Type', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('Staff', self.gf('django.db.models.fields.related.ForeignKey')(related_name='EventStaffGroup', to=orm['MIS.Group'])),
            ('Action', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'MIS', ['EventTrigger'])

        # Adding model 'ExtentionData'
        db.create_table(u'MIS_extentiondata', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('BaseType', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('BaseId', self.gf('django.db.models.fields.PositiveIntegerField')()),
            ('ExtentionRecord', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
            ('Subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Subject'])),
            ('Data', self.gf('picklefield.fields.PickledObjectField')()),
            ('UpdatedOn', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 269283))),
            ('UpdatedBy', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='ExtentionsUpdatedBy', null=True, to=orm['MIS.Staff'])),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['ExtentionData'])

        # Adding model 'ExtentionFields'
        db.create_table(u'MIS_extentionfields', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Type', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('PickList', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionPickList'])),
        ))
        db.send_create_signal(u'MIS', ['ExtentionFields'])

        # Adding model 'ExtentionRecords'
        db.create_table(u'MIS_extentionrecords', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('FullName', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'MIS', ['ExtentionRecords'])

        # Adding M2M table for field Fields on 'ExtentionRecords'
        db.create_table(u'MIS_extentionrecords_Fields', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('extentionrecords', models.ForeignKey(orm[u'MIS.extentionrecords'], null=False)),
            ('extentionfields', models.ForeignKey(orm[u'MIS.extentionfields'], null=False))
        ))
        db.create_unique(u'MIS_extentionrecords_Fields', ['extentionrecords_id', 'extentionfields_id'])

        # Adding model 'ExtentionPickList'
        db.create_table(u'MIS_extentionpicklist', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Data', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'MIS', ['ExtentionPickList'])

        # Adding model 'ExtentionForPupil'
        db.create_table(u'MIS_extentionforpupil', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('PupilId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('ExtentionRecordId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(related_name='ExtentionIndexPupil', to=orm['MIS.ExtentionsIndex'])),
            ('ExtentionSubject', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('ExtentionUpdatedOn', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 271594))),
            ('ExtentionUpdatedBy', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='ExtentionsPupilUpdatedBy', null=True, to=orm['MIS.Staff'])),
        ))
        db.send_create_signal(u'MIS', ['ExtentionForPupil'])

        # Adding unique constraint on 'ExtentionForPupil', fields ['PupilId', 'ExtentionRecordId', 'ExtentionSubject']
        db.create_unique(u'MIS_extentionforpupil', ['PupilId_id', 'ExtentionRecordId_id', 'ExtentionSubject'])

        # Adding model 'ExtentionForStaff'
        db.create_table(u'MIS_extentionforstaff', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('StaffId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('ExtentionRecordId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='ExtentionIndexStaff', null=True, to=orm['MIS.ExtentionsIndex'])),
            ('ExtentionSubject', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('ExtentionUpdatedOn', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 272209))),
            ('ExtentionUpdatedBy', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='ExtentionStaffUpdatedBy', null=True, to=orm['MIS.Staff'])),
        ))
        db.send_create_signal(u'MIS', ['ExtentionForStaff'])

        # Adding unique constraint on 'ExtentionForStaff', fields ['StaffId', 'ExtentionRecordId', 'ExtentionSubject']
        db.create_unique(u'MIS_extentionforstaff', ['StaffId_id', 'ExtentionRecordId_id', 'ExtentionSubject'])

        # Adding model 'ExtentionForFamily'
        db.create_table(u'MIS_extentionforfamily', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('FamilyId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Family'])),
            ('ExtentionRecordId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(related_name='ExtentionIndexFamily', to=orm['MIS.ExtentionsIndex'])),
            ('ExtentionSubject', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('ExtentionUpdatedOn', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 272950))),
            ('ExtentionUpdatedBy', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='ExtentionsFamilyUpdatedBy', null=True, to=orm['MIS.Staff'])),
        ))
        db.send_create_signal(u'MIS', ['ExtentionForFamily'])

        # Adding unique constraint on 'ExtentionForFamily', fields ['FamilyId', 'ExtentionRecordId', 'ExtentionSubject']
        db.create_unique(u'MIS_extentionforfamily', ['FamilyId_id', 'ExtentionRecordId_id', 'ExtentionSubject'])

        # Adding model 'ExtentionForContact'
        db.create_table(u'MIS_extentionforcontact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ContactId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Contact'])),
            ('ExtentionRecordId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(related_name='ExtentionIndexContact', to=orm['MIS.ExtentionsIndex'])),
            ('ExtentionSubject', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('ExtentionUpdatedOn', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 273573))),
            ('ExtentionUpdatedBy', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='ExtentionsContactsUpdatedBy', null=True, to=orm['MIS.Staff'])),
        ))
        db.send_create_signal(u'MIS', ['ExtentionForContact'])

        # Adding unique constraint on 'ExtentionForContact', fields ['ContactId', 'ExtentionRecordId', 'ExtentionSubject']
        db.create_unique(u'MIS_extentionforcontact', ['ContactId_id', 'ExtentionRecordId_id', 'ExtentionSubject'])

        # Adding model 'ExtentionForSchool'
        db.create_table(u'MIS_extentionforschool', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('SchoolId', self.gf('django.db.models.fields.related.ForeignKey')(related_name='school_extention', to=orm['MIS.School'])),
            ('ExtentionRecordId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(related_name='ExtentionIndexSchool', to=orm['MIS.ExtentionsIndex'])),
            ('ExtentionSubject', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('ExtentionUpdatedOn', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 274193))),
            ('ExtentionUpdatedBy', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='ExtentionsSchoolsUpdatedBy', null=True, to=orm['MIS.Staff'])),
        ))
        db.send_create_signal(u'MIS', ['ExtentionForSchool'])

        # Adding unique constraint on 'ExtentionForSchool', fields ['SchoolId', 'ExtentionRecordId', 'ExtentionSubject']
        db.create_unique(u'MIS_extentionforschool', ['SchoolId_id', 'ExtentionRecordId_id', 'ExtentionSubject'])

        # Adding model 'ExtentionsIndex'
        db.create_table(u'MIS_extentionsindex', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ExtentionRecordId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
        ))
        db.send_create_signal(u'MIS', ['ExtentionsIndex'])

        # Adding model 'ExtentionDate'
        db.create_table(u'MIS_extentiondate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionsIndex'])),
            ('FieldId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionFields'])),
            ('Data', self.gf('django.db.models.fields.DateField')()),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['ExtentionDate'])

        # Adding model 'ExtentionTime'
        db.create_table(u'MIS_extentiontime', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionsIndex'])),
            ('FieldId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionFields'])),
            ('Data', self.gf('django.db.models.fields.TimeField')()),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['ExtentionTime'])

        # Adding model 'ExtentionNumerical'
        db.create_table(u'MIS_extentionnumerical', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionsIndex'])),
            ('FieldId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionFields'])),
            ('Data', self.gf('django.db.models.fields.FloatField')()),
            ('AlternativeValue', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['ExtentionNumerical'])

        # Adding model 'ExtentionString'
        db.create_table(u'MIS_extentionstring', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionsIndex'])),
            ('FieldId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionFields'])),
            ('Data', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('AlternativeValue', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['ExtentionString'])

        # Adding model 'ExtentionTextBox'
        db.create_table(u'MIS_extentiontextbox', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionsIndex'])),
            ('FieldId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionFields'])),
            ('Data', self.gf('django.db.models.fields.TextField')()),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['ExtentionTextBox'])

        # Adding model 'ExtentionBoolean'
        db.create_table(u'MIS_extentionboolean', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ExtentionIndexId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionsIndex'])),
            ('FieldId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionFields'])),
            ('Data', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['ExtentionBoolean'])

        # Adding model 'Document'
        db.create_table(u'MIS_document', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('DocumentKeywords', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('DocumentName', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('Filename', self.gf('django.db.models.fields.files.FileField')(max_length=300)),
            ('UploadDate', self.gf('django.db.models.fields.DateField')(default=datetime.date(2014, 4, 25))),
            ('UploadStaff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
        ))
        db.send_create_signal(u'MIS', ['Document'])

        # Adding model 'PupilDocument'
        db.create_table(u'MIS_pupildocument', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('PupilId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('DocumentId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Document'])),
        ))
        db.send_create_signal(u'MIS', ['PupilDocument'])

        # Adding model 'FamilyDocument'
        db.create_table(u'MIS_familydocument', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('FamilyId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Family'])),
            ('DocumentId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Document'])),
        ))
        db.send_create_signal(u'MIS', ['FamilyDocument'])

        # Adding model 'StaffDocument'
        db.create_table(u'MIS_staffdocument', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('StaffId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('DocumentId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Document'])),
        ))
        db.send_create_signal(u'MIS', ['StaffDocument'])

        # Adding model 'SchoolDocument'
        db.create_table(u'MIS_schooldocument', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('SchoolId', self.gf('django.db.models.fields.related.ForeignKey')(related_name='school-docs', to=orm['MIS.School'])),
            ('DocumentId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Document'])),
        ))
        db.send_create_signal(u'MIS', ['SchoolDocument'])

        # Adding model 'PupilAttendance'
        db.create_table(u'MIS_pupilattendance', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Pupil', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('AttendanceDate', self.gf('django.db.models.fields.DateField')()),
            ('AttendanceType', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('Group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('AmPm', self.gf('django.db.models.fields.CharField')(max_length=2, blank=True)),
            ('Teacher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('RecordedDate', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 280216))),
            ('Code', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.AttendanceCode'])),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['PupilAttendance'])

        # Adding model 'AttendanceCode'
        db.create_table(u'MIS_attendancecode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('AttendanceCode', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('AttendanceDesc', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('AttendanceAccess', self.gf('django.db.models.fields.CharField')(max_length=15)),
            ('AttendanceCodeType', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('AttendanceStatistical', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['AttendanceCode'])

        # Adding model 'AttendanceTaken'
        db.create_table(u'MIS_attendancetaken', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('AttendanceDate', self.gf('django.db.models.fields.DateField')()),
            ('AttendanceType', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('Group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('AmPm', self.gf('django.db.models.fields.CharField')(max_length=2, blank=True)),
            ('Teacher', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('RecordedDate', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 281439))),
        ))
        db.send_create_signal(u'MIS', ['AttendanceTaken'])

        # Adding model 'AttendanceNotTakenCode'
        db.create_table(u'MIS_attendancenottakencode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=1)),
            ('Desc', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'MIS', ['AttendanceNotTakenCode'])

        # Adding model 'AttendanceNotTaken'
        db.create_table(u'MIS_attendancenottaken', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Code', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.AttendanceNotTakenCode'])),
            ('Date', self.gf('django.db.models.fields.DateField')()),
            ('Type', self.gf('django.db.models.fields.CharField')(max_length=7)),
            ('Details', self.gf('django.db.models.fields.TextField')()),
            ('Group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'], null=True, blank=True)),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=50)),
        ))
        db.send_create_signal(u'MIS', ['AttendanceNotTaken'])

        # Adding model 'MenuTypes'
        db.create_table(u'MIS_menutypes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('Desc', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('SchoolId', self.gf('django.db.models.fields.related.ForeignKey')(related_name='schoolMenu', to=orm['MIS.School'])),
            ('MenuLevel', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('Logo', self.gf('django.db.models.fields.CharField')(max_length=200)),
        ))
        db.send_create_signal(u'MIS', ['MenuTypes'])

        # Adding model 'Menus'
        db.create_table(u'MIS_menus', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('MenuType', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.MenuTypes'])),
            ('Order', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('Group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('Manage', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['Menus'])

        # Adding unique constraint on 'Menus', fields ['MenuType', 'Order']
        db.create_unique(u'MIS_menus', ['MenuType_id', 'Order'])

        # Adding M2M table for field MenuActions on 'Menus'
        db.create_table(u'MIS_menus_MenuActions', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menus', models.ForeignKey(orm[u'MIS.menus'], null=False)),
            ('menuactions', models.ForeignKey(orm[u'MIS.menuactions'], null=False))
        ))
        db.create_unique(u'MIS_menus_MenuActions', ['menus_id', 'menuactions_id'])

        # Adding M2M table for field SubMenus on 'Menus'
        db.create_table(u'MIS_menus_SubMenus', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menus', models.ForeignKey(orm[u'MIS.menus'], null=False)),
            ('menusubmenu', models.ForeignKey(orm[u'MIS.menusubmenu'], null=False))
        ))
        db.create_unique(u'MIS_menus_SubMenus', ['menus_id', 'menusubmenu_id'])

        # Adding M2M table for field ManagedGroups on 'Menus'
        db.create_table(u'MIS_menus_ManagedGroups', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menus', models.ForeignKey(orm[u'MIS.menus'], null=False)),
            ('group', models.ForeignKey(orm[u'MIS.group'], null=False))
        ))
        db.create_unique(u'MIS_menus_ManagedGroups', ['menus_id', 'group_id'])

        # Adding model 'MenuSubMenu'
        db.create_table(u'MIS_menusubmenu', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=20)),
            ('MenuName', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('MenuOrder', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'MIS', ['MenuSubMenu'])

        # Adding model 'MenuActions'
        db.create_table(u'MIS_menuactions', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('MenuName', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('MenuOrder', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'MIS', ['MenuActions'])

        # Adding model 'MenuItems'
        db.create_table(u'MIS_menuitems', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('ItemURL', self.gf('django.db.models.fields.CharField')(max_length=300)),
        ))
        db.send_create_signal(u'MIS', ['MenuItems'])

        # Adding model 'GroupMenuItems'
        db.create_table(u'MIS_groupmenuitems', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Type', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.MenuSubMenu'])),
            ('Group', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('MenuItem', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.MenuItems'])),
            ('GroupRequirements', self.gf('django.db.models.fields.CharField')(max_length=12, blank=True)),
            ('ReqParms', self.gf('django.db.models.fields.CharField')(max_length=100, blank=True)),
            ('MenuLevel', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
        ))
        db.send_create_signal(u'MIS', ['GroupMenuItems'])

        # Adding model 'GradeInputElement'
        db.create_table(u'MIS_gradeinputelement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('GradeSubject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Subject'])),
            ('GradeRecord', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
            ('PrevGradeRecord', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='PrevGradeRecord', null=True, to=orm['MIS.ExtentionRecords'])),
            ('InputForm', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('DisplayOrder', self.gf('django.db.models.fields.SmallIntegerField')(default=1)),
        ))
        db.send_create_signal(u'MIS', ['GradeInputElement'])

        # Adding model 'GradeInputFields'
        db.create_table(u'MIS_gradeinputfields', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('GradeSubject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Subject'])),
            ('GradeRecord', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
            ('FieldList', self.gf('picklefield.fields.PickledObjectField')()),
        ))
        db.send_create_signal(u'MIS', ['GradeInputFields'])

        # Adding model 'PupilSnapshot'
        db.create_table(u'MIS_pupilsnapshot', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('PupilRecord', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('GradeRecord', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
            ('Snapshot', self.gf('picklefield.fields.PickledObjectField')()),
            ('TimeStamp', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 288185))),
        ))
        db.send_create_signal(u'MIS', ['PupilSnapshot'])

        # Adding model 'ReportingSeason'
        db.create_table(u'MIS_reportingseason', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('GradeRecord', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.ExtentionRecords'])),
            ('GradeGroup', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('GradeStartDay', self.gf('django.db.models.fields.IntegerField')()),
            ('GradeStopDay', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'MIS', ['ReportingSeason'])

        # Adding model 'PupilAlert'
        db.create_table(u'MIS_pupilalert', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('PupilId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('StartDate', self.gf('django.db.models.fields.DateField')()),
            ('StopDate', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('AlertType', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.AlertType'])),
            ('AlertDetails', self.gf('django.db.models.fields.TextField')()),
            ('AlertClosedDetails', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
            ('RaisedBy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('ClosedBy', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='AlertCloseStaffId', null=True, to=orm['MIS.Staff'])),
            ('UpdateDate', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 289194))),
        ))
        db.send_create_signal(u'MIS', ['PupilAlert'])

        # Adding model 'AlertType'
        db.create_table(u'MIS_alerttype', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('AlertGroup', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.AlertGroup'])),
            ('Name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('Desc', self.gf('django.db.models.fields.TextField')()),
            ('Icon', self.gf('django.db.models.fields.files.ImageField')(max_length=100, blank=True)),
        ))
        db.send_create_signal(u'MIS', ['AlertType'])

        # Adding model 'AlertGroup'
        db.create_table(u'MIS_alertgroup', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=100)),
            ('DisplayOrder', self.gf('django.db.models.fields.IntegerField')(max_length=2, blank=True)),
        ))
        db.send_create_signal(u'MIS', ['AlertGroup'])

        # Adding M2M table for field PermissionGroups on 'AlertGroup'
        db.create_table(u'MIS_alertgroup_PermissionGroups', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('alertgroup', models.ForeignKey(orm[u'MIS.alertgroup'], null=False)),
            ('group', models.ForeignKey(orm[u'MIS.group'], null=False))
        ))
        db.create_unique(u'MIS_alertgroup_PermissionGroups', ['alertgroup_id', 'group_id'])

        # Adding model 'NoteDetails'
        db.create_table(u'MIS_notedetails', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Keywords', self.gf('django.db.models.fields.CharField')(max_length=300)),
            ('NoteText', self.gf('django.db.models.fields.TextField')()),
            ('IsAnIssue', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('IssueStatus', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('HousePoints', self.gf('django.db.models.fields.SmallIntegerField')(default=0)),
            ('HousePointsType', self.gf('django.db.models.fields.CharField')(default='NA', max_length=2)),
            ('RaisedBy', self.gf('django.db.models.fields.related.ForeignKey')(related_name='NoteRaisedBy', to=orm['MIS.Staff'])),
            ('RaisedDate', self.gf('django.db.models.fields.DateTimeField')()),
            ('ModifiedBy', self.gf('django.db.models.fields.related.ForeignKey')(related_name='NoteModifiedBy', to=orm['MIS.Staff'])),
            ('ModifiedDate', self.gf('django.db.models.fields.DateTimeField')()),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['NoteDetails'])

        # Adding model 'NoteNotification'
        db.create_table(u'MIS_notenotification', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Keywords', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('Pupilgroup', self.gf('django.db.models.fields.related.ForeignKey')(related_name='PupilGroupNotification', to=orm['MIS.Group'])),
            ('NotificationGroup', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
        ))
        db.send_create_signal(u'MIS', ['NoteNotification'])

        # Adding model 'NoteToIssue'
        db.create_table(u'MIS_notetoissue', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Keywords', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('Pupilgroup', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('NotificationGroup', self.gf('django.db.models.fields.related.ForeignKey')(related_name='IssueStaffLink', to=orm['MIS.Group'])),
        ))
        db.send_create_signal(u'MIS', ['NoteToIssue'])

        # Adding model 'NoteKeywords'
        db.create_table(u'MIS_notekeywords', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Keyword', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('KeywordType', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('KeywordPool', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('HousePointsTrigger', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['NoteKeywords'])

        # Adding model 'PupilNotes'
        db.create_table(u'MIS_pupilnotes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('PupilId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('NoteId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.NoteDetails'])),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['PupilNotes'])

        # Adding model 'StaffNotes'
        db.create_table(u'MIS_staffnotes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('StaffId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('NoteId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.NoteDetails'])),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['StaffNotes'])

        # Adding model 'FamilyNotes'
        db.create_table(u'MIS_familynotes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('FamilyId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Family'])),
            ('NoteId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.NoteDetails'])),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['FamilyNotes'])

        # Adding model 'ContactNotes'
        db.create_table(u'MIS_contactnotes', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('ContactId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Contact'])),
            ('NoteId', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.NoteDetails'])),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['ContactNotes'])

        # Adding model 'LetterBody'
        db.create_table(u'MIS_letterbody', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('AttachedTo', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Group'])),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('Description', self.gf('django.db.models.fields.TextField')(default="'Please enter\n    text to help users use this letter")),
            ('BodyText', self.gf('django.db.models.fields.TextField')()),
            ('SignatureName', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('SignatureTitle', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('BodyVariables', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('LetterTemplate', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.LetterTemplate'])),
            ('ObjectBaseType', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('ExtraStaffAccess', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='LetterStaffAccess', null=True, to=orm['MIS.Group'])),
            ('CreatedBy', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('CreatedOn', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 297185))),
            ('ModifiedBY', self.gf('django.db.models.fields.related.ForeignKey')(related_name='letter_Modified_by', to=orm['MIS.Staff'])),
            ('ModifiedOn', self.gf('django.db.models.fields.DateTimeField')(default=datetime.datetime(2014, 4, 25, 13, 35, 51, 297238))),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['LetterBody'])

        # Adding model 'LetterTemplate'
        db.create_table(u'MIS_lettertemplate', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('Filename', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('LetterBaseType', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['LetterTemplate'])

        # Adding model 'PupilTarget'
        db.create_table(u'MIS_pupiltarget', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Pupil', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('Type', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('Target', self.gf('django.db.models.fields.TextField')()),
            ('Review', self.gf('django.db.models.fields.TextField')()),
            ('Progress', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('ReviewDate', self.gf('django.db.models.fields.DateField')(default=datetime.datetime.now)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['PupilTarget'])

        # Adding model 'PupilProvision'
        db.create_table(u'MIS_pupilprovision', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Pupil', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('Type', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('SENType', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('Details', self.gf('django.db.models.fields.TextField')()),
            ('StartDate', self.gf('django.db.models.fields.DateField')(default=datetime.datetime.now)),
            ('StopDate', self.gf('django.db.models.fields.DateField')(default=datetime.datetime.now)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['PupilProvision'])

        # Adding model 'PupilGiftedTalented'
        db.create_table(u'MIS_pupilgiftedtalented', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Pupil', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Pupil'])),
            ('Subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Subject'])),
            ('Details', self.gf('django.db.models.fields.TextField')()),
            ('Mentor', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('Level', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('Date_Identified', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('Date_Not_Gifted_and_Talented', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('DateAdded', self.gf('django.db.models.fields.DateField')(default=datetime.datetime.now)),
            ('DateClosed', self.gf('django.db.models.fields.DateField')(default=datetime.datetime.now)),
            ('Reasons_for_Removal_Gifted_and_Talented', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['PupilGiftedTalented'])

        # Adding model 'SystemVariable'
        db.create_table(u'MIS_systemvariable', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('VariableType', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('DataType', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'MIS', ['SystemVariable'])

        # Adding model 'SystemVarData'
        db.create_table(u'MIS_systemvardata', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Variable', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.SystemVariable'])),
            ('AcademicYear', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('Data', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'MIS', ['SystemVarData'])

        # Adding unique constraint on 'SystemVarData', fields ['Variable', 'AcademicYear']
        db.create_unique(u'MIS_systemvardata', ['Variable_id', 'AcademicYear'])

        # Adding model 'StaffCpdRecords'
        db.create_table(u'MIS_staffcpdrecords', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('Staff', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['MIS.Staff'])),
            ('Name', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('TimeTaken', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('DateTaken', self.gf('django.db.models.fields.DateField')(default=datetime.date(2014, 4, 25))),
            ('Duration', self.gf('django.db.models.fields.CharField')(max_length=20, blank=True)),
            ('Details', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('DateExpires', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('CertIssued', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('Provider', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('InternalExternal', self.gf('django.db.models.fields.CharField')(max_length=8)),
            ('Active', self.gf('django.db.models.fields.BooleanField')(default=True)),
        ))
        db.send_create_signal(u'MIS', ['StaffCpdRecords'])


    def backwards(self, orm):
        
        # Removing unique constraint on 'SystemVarData', fields ['Variable', 'AcademicYear']
        db.delete_unique(u'MIS_systemvardata', ['Variable_id', 'AcademicYear'])

        # Removing unique constraint on 'Menus', fields ['MenuType', 'Order']
        db.delete_unique(u'MIS_menus', ['MenuType_id', 'Order'])

        # Removing unique constraint on 'ExtentionForSchool', fields ['SchoolId', 'ExtentionRecordId', 'ExtentionSubject']
        db.delete_unique(u'MIS_extentionforschool', ['SchoolId_id', 'ExtentionRecordId_id', 'ExtentionSubject'])

        # Removing unique constraint on 'ExtentionForContact', fields ['ContactId', 'ExtentionRecordId', 'ExtentionSubject']
        db.delete_unique(u'MIS_extentionforcontact', ['ContactId_id', 'ExtentionRecordId_id', 'ExtentionSubject'])

        # Removing unique constraint on 'ExtentionForFamily', fields ['FamilyId', 'ExtentionRecordId', 'ExtentionSubject']
        db.delete_unique(u'MIS_extentionforfamily', ['FamilyId_id', 'ExtentionRecordId_id', 'ExtentionSubject'])

        # Removing unique constraint on 'ExtentionForStaff', fields ['StaffId', 'ExtentionRecordId', 'ExtentionSubject']
        db.delete_unique(u'MIS_extentionforstaff', ['StaffId_id', 'ExtentionRecordId_id', 'ExtentionSubject'])

        # Removing unique constraint on 'ExtentionForPupil', fields ['PupilId', 'ExtentionRecordId', 'ExtentionSubject']
        db.delete_unique(u'MIS_extentionforpupil', ['PupilId_id', 'ExtentionRecordId_id', 'ExtentionSubject'])

        # Removing unique constraint on 'StaffGroup', fields ['AcademicYear', 'Staff', 'Group']
        db.delete_unique(u'MIS_staffgroup', ['AcademicYear', 'Staff_id', 'Group_id'])

        # Deleting model 'School'
        db.delete_table(u'MIS_school')

        # Deleting model 'Family'
        db.delete_table(u'MIS_family')

        # Removing M2M table for field Address on 'Family'
        db.delete_table('MIS_family_Address')

        # Deleting model 'Pupil'
        db.delete_table(u'MIS_pupil')

        # Deleting model 'Contact'
        db.delete_table(u'MIS_contact')

        # Removing M2M table for field FamilyId on 'Contact'
        db.delete_table('MIS_contact_FamilyId')

        # Removing M2M table for field Pupils on 'Contact'
        db.delete_table('MIS_contact_Pupils')

        # Deleting model 'Staff'
        db.delete_table(u'MIS_staff')

        # Deleting model 'StaffEmergencyContact'
        db.delete_table(u'MIS_staffemergencycontact')

        # Deleting model 'Address'
        db.delete_table(u'MIS_address')

        # Deleting model 'FamilyContact'
        db.delete_table(u'MIS_familycontact')

        # Deleting model 'PupilContact'
        db.delete_table(u'MIS_pupilcontact')

        # Deleting model 'FamilyChildren'
        db.delete_table(u'MIS_familychildren')

        # Deleting model 'FamilyRelationship'
        db.delete_table(u'MIS_familyrelationship')

        # Deleting model 'SchoolList'
        db.delete_table(u'MIS_schoollist')

        # Deleting model 'PupilFutureSchoolProgress'
        db.delete_table(u'MIS_pupilfutureschoolprogress')

        # Deleting model 'PupilsPastSchool'
        db.delete_table(u'MIS_pupilspastschool')

        # Deleting model 'PupilsNextSchool'
        db.delete_table(u'MIS_pupilsnextschool')

        # Deleting model 'Group'
        db.delete_table(u'MIS_group')

        # Deleting model 'PupilGroup'
        db.delete_table(u'MIS_pupilgroup')

        # Deleting model 'StaffGroup'
        db.delete_table(u'MIS_staffgroup')

        # Deleting model 'StaffGroupFavourite'
        db.delete_table(u'MIS_staffgroupfavourite')

        # Deleting model 'RollOverRule'
        db.delete_table(u'MIS_rolloverrule')

        # Deleting model 'Subject'
        db.delete_table(u'MIS_subject')

        # Deleting model 'ParentSubjects'
        db.delete_table(u'MIS_parentsubjects')

        # Deleting model 'EventQueue'
        db.delete_table(u'MIS_eventqueue')

        # Deleting model 'EventTrigger'
        db.delete_table(u'MIS_eventtrigger')

        # Deleting model 'ExtentionData'
        db.delete_table(u'MIS_extentiondata')

        # Deleting model 'ExtentionFields'
        db.delete_table(u'MIS_extentionfields')

        # Deleting model 'ExtentionRecords'
        db.delete_table(u'MIS_extentionrecords')

        # Removing M2M table for field Fields on 'ExtentionRecords'
        db.delete_table('MIS_extentionrecords_Fields')

        # Deleting model 'ExtentionPickList'
        db.delete_table(u'MIS_extentionpicklist')

        # Deleting model 'ExtentionForPupil'
        db.delete_table(u'MIS_extentionforpupil')

        # Deleting model 'ExtentionForStaff'
        db.delete_table(u'MIS_extentionforstaff')

        # Deleting model 'ExtentionForFamily'
        db.delete_table(u'MIS_extentionforfamily')

        # Deleting model 'ExtentionForContact'
        db.delete_table(u'MIS_extentionforcontact')

        # Deleting model 'ExtentionForSchool'
        db.delete_table(u'MIS_extentionforschool')

        # Deleting model 'ExtentionsIndex'
        db.delete_table(u'MIS_extentionsindex')

        # Deleting model 'ExtentionDate'
        db.delete_table(u'MIS_extentiondate')

        # Deleting model 'ExtentionTime'
        db.delete_table(u'MIS_extentiontime')

        # Deleting model 'ExtentionNumerical'
        db.delete_table(u'MIS_extentionnumerical')

        # Deleting model 'ExtentionString'
        db.delete_table(u'MIS_extentionstring')

        # Deleting model 'ExtentionTextBox'
        db.delete_table(u'MIS_extentiontextbox')

        # Deleting model 'ExtentionBoolean'
        db.delete_table(u'MIS_extentionboolean')

        # Deleting model 'Document'
        db.delete_table(u'MIS_document')

        # Deleting model 'PupilDocument'
        db.delete_table(u'MIS_pupildocument')

        # Deleting model 'FamilyDocument'
        db.delete_table(u'MIS_familydocument')

        # Deleting model 'StaffDocument'
        db.delete_table(u'MIS_staffdocument')

        # Deleting model 'SchoolDocument'
        db.delete_table(u'MIS_schooldocument')

        # Deleting model 'PupilAttendance'
        db.delete_table(u'MIS_pupilattendance')

        # Deleting model 'AttendanceCode'
        db.delete_table(u'MIS_attendancecode')

        # Deleting model 'AttendanceTaken'
        db.delete_table(u'MIS_attendancetaken')

        # Deleting model 'AttendanceNotTakenCode'
        db.delete_table(u'MIS_attendancenottakencode')

        # Deleting model 'AttendanceNotTaken'
        db.delete_table(u'MIS_attendancenottaken')

        # Deleting model 'MenuTypes'
        db.delete_table(u'MIS_menutypes')

        # Deleting model 'Menus'
        db.delete_table(u'MIS_menus')

        # Removing M2M table for field MenuActions on 'Menus'
        db.delete_table('MIS_menus_MenuActions')

        # Removing M2M table for field SubMenus on 'Menus'
        db.delete_table('MIS_menus_SubMenus')

        # Removing M2M table for field ManagedGroups on 'Menus'
        db.delete_table('MIS_menus_ManagedGroups')

        # Deleting model 'MenuSubMenu'
        db.delete_table(u'MIS_menusubmenu')

        # Deleting model 'MenuActions'
        db.delete_table(u'MIS_menuactions')

        # Deleting model 'MenuItems'
        db.delete_table(u'MIS_menuitems')

        # Deleting model 'GroupMenuItems'
        db.delete_table(u'MIS_groupmenuitems')

        # Deleting model 'GradeInputElement'
        db.delete_table(u'MIS_gradeinputelement')

        # Deleting model 'GradeInputFields'
        db.delete_table(u'MIS_gradeinputfields')

        # Deleting model 'PupilSnapshot'
        db.delete_table(u'MIS_pupilsnapshot')

        # Deleting model 'ReportingSeason'
        db.delete_table(u'MIS_reportingseason')

        # Deleting model 'PupilAlert'
        db.delete_table(u'MIS_pupilalert')

        # Deleting model 'AlertType'
        db.delete_table(u'MIS_alerttype')

        # Deleting model 'AlertGroup'
        db.delete_table(u'MIS_alertgroup')

        # Removing M2M table for field PermissionGroups on 'AlertGroup'
        db.delete_table('MIS_alertgroup_PermissionGroups')

        # Deleting model 'NoteDetails'
        db.delete_table(u'MIS_notedetails')

        # Deleting model 'NoteNotification'
        db.delete_table(u'MIS_notenotification')

        # Deleting model 'NoteToIssue'
        db.delete_table(u'MIS_notetoissue')

        # Deleting model 'NoteKeywords'
        db.delete_table(u'MIS_notekeywords')

        # Deleting model 'PupilNotes'
        db.delete_table(u'MIS_pupilnotes')

        # Deleting model 'StaffNotes'
        db.delete_table(u'MIS_staffnotes')

        # Deleting model 'FamilyNotes'
        db.delete_table(u'MIS_familynotes')

        # Deleting model 'ContactNotes'
        db.delete_table(u'MIS_contactnotes')

        # Deleting model 'LetterBody'
        db.delete_table(u'MIS_letterbody')

        # Deleting model 'LetterTemplate'
        db.delete_table(u'MIS_lettertemplate')

        # Deleting model 'PupilTarget'
        db.delete_table(u'MIS_pupiltarget')

        # Deleting model 'PupilProvision'
        db.delete_table(u'MIS_pupilprovision')

        # Deleting model 'PupilGiftedTalented'
        db.delete_table(u'MIS_pupilgiftedtalented')

        # Deleting model 'SystemVariable'
        db.delete_table(u'MIS_systemvariable')

        # Deleting model 'SystemVarData'
        db.delete_table(u'MIS_systemvardata')

        # Deleting model 'StaffCpdRecords'
        db.delete_table(u'MIS_staffcpdrecords')


    models = {
        u'MIS.address': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AddressLine1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'AddressLine2': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'AddressLine3': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'AddressLine4': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'HomeSalutation': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'object_name': 'Address'},
            'Note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Phone1': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'Phone2': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'Phone3': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'PostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'PostalTitle': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.alertgroup': {
            'DisplayOrder': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'blank': 'True'}),
            'Meta': {'object_name': 'AlertGroup'},
            'Name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'PermissionGroups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['MIS.Group']", 'null': 'True', 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.alerttype': {
            'AlertGroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.AlertGroup']"}),
            'Desc': ('django.db.models.fields.TextField', [], {}),
            'Icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'Meta': {'object_name': 'AlertType'},
            'Name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.attendancecode': {
            'AttendanceAccess': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'AttendanceCode': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'AttendanceCodeType': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'AttendanceDesc': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'AttendanceStatistical': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Meta': {'ordering': "['AttendanceCode']", 'object_name': 'AttendanceCode'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.attendancenottaken': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.AttendanceNotTakenCode']"}),
            'Date': ('django.db.models.fields.DateField', [], {}),
            'Details': ('django.db.models.fields.TextField', [], {}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']", 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'AttendanceNotTaken'},
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.attendancenottakencode': {
            'Code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '1'}),
            'Desc': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'AttendanceNotTakenCode'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.attendancetaken': {
            'AmPm': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'AttendanceDate': ('django.db.models.fields.DateField', [], {}),
            'AttendanceType': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'AttendanceTaken'},
            'RecordedDate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 281439)'}),
            'Teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.contact': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']", 'null': 'True', 'blank': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'FamilyId': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['MIS.Family']", 'null': 'True', 'blank': 'True'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname', 'Forename']", 'object_name': 'Contact'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Pupils': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['MIS.Pupil']", 'null': 'True', 'blank': 'True'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'WorkAddress': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'WorkAddress'", 'null': 'True', 'to': u"orm['MIS.Address']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.contactnotes': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ContactId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Contact']"}),
            'Meta': {'object_name': 'ContactNotes'},
            'NoteId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.NoteDetails']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.document': {
            'DocumentKeywords': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'DocumentName': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Filename': ('django.db.models.fields.files.FileField', [], {'max_length': '300'}),
            'Meta': {'object_name': 'Document'},
            'UploadDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 4, 25)'}),
            'UploadStaff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.eventqueue': {
            'Details': ('django.db.models.fields.TextField', [], {}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'EventQueue'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'TimeStamp': ('django.db.models.fields.DateTimeField', [], {}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.eventtrigger': {
            'Action': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'EventTrigger'},
            'Priority': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'Staff': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'EventStaffGroup'", 'to': u"orm['MIS.Group']"}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionboolean': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Data': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionBoolean'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentiondata': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'BaseId': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'BaseType': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'Data': ('picklefield.fields.PickledObjectField', [], {}),
            'ExtentionRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'Meta': {'object_name': 'ExtentionData'},
            'Subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Subject']"}),
            'UpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionsUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'UpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 269283)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentiondate': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Data': ('django.db.models.fields.DateField', [], {}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionDate'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionfields': {
            'Meta': {'object_name': 'ExtentionFields'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'PickList': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionPickList']"}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionforcontact': {
            'ContactId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Contact']"}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ExtentionIndexContact'", 'to': u"orm['MIS.ExtentionsIndex']"}),
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'ExtentionSubject': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ExtentionUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionsContactsUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'ExtentionUpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 273573)'}),
            'Meta': {'unique_together': "(('ContactId', 'ExtentionRecordId', 'ExtentionSubject'),)", 'object_name': 'ExtentionForContact'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionforfamily': {
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ExtentionIndexFamily'", 'to': u"orm['MIS.ExtentionsIndex']"}),
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'ExtentionSubject': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ExtentionUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionsFamilyUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'ExtentionUpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 272950)'}),
            'FamilyId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Family']"}),
            'Meta': {'unique_together': "(('FamilyId', 'ExtentionRecordId', 'ExtentionSubject'),)", 'object_name': 'ExtentionForFamily'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionforpupil': {
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ExtentionIndexPupil'", 'to': u"orm['MIS.ExtentionsIndex']"}),
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'ExtentionSubject': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ExtentionUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionsPupilUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'ExtentionUpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 271594)'}),
            'Meta': {'unique_together': "(('PupilId', 'ExtentionRecordId', 'ExtentionSubject'),)", 'object_name': 'ExtentionForPupil'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionforschool': {
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ExtentionIndexSchool'", 'to': u"orm['MIS.ExtentionsIndex']"}),
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'ExtentionSubject': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ExtentionUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionsSchoolsUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'ExtentionUpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 274193)'}),
            'Meta': {'unique_together': "(('SchoolId', 'ExtentionRecordId', 'ExtentionSubject'),)", 'object_name': 'ExtentionForSchool'},
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'school_extention'", 'to': u"orm['MIS.School']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionforstaff': {
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionIndexStaff'", 'null': 'True', 'to': u"orm['MIS.ExtentionsIndex']"}),
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'ExtentionSubject': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ExtentionUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionStaffUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'ExtentionUpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 272209)'}),
            'Meta': {'unique_together': "(('StaffId', 'ExtentionRecordId', 'ExtentionSubject'),)", 'object_name': 'ExtentionForStaff'},
            'StaffId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionnumerical': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AlternativeValue': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Data': ('django.db.models.fields.FloatField', [], {}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionNumerical'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionpicklist': {
            'Data': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'ExtentionPickList'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionrecords': {
            'Fields': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['MIS.ExtentionFields']", 'symmetrical': 'False'}),
            'FullName': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'ordering': "['Name']", 'object_name': 'ExtentionRecords'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionsindex': {
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'Meta': {'object_name': 'ExtentionsIndex'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionstring': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AlternativeValue': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Data': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionString'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentiontextbox': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Data': ('django.db.models.fields.TextField', [], {}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionTextBox'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentiontime': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Data': ('django.db.models.fields.TimeField', [], {}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionTime'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.family': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['MIS.Address']", 'symmetrical': 'False'}),
            'FamilyName': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'Meta': {'ordering': "['FamilyName']", 'object_name': 'Family'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.familychildren': {
            'FamilyId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Family']"}),
            'FamilyType': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'Meta': {'ordering': "['FamilyId']", 'object_name': 'FamilyChildren'},
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'RelationshipDesc': ('django.db.models.fields.TextField', [], {'max_length': '500', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.familycontact': {
            'Contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Contact']"}),
            'FamilyId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Family']"}),
            'Meta': {'ordering': "['FamilyId']", 'object_name': 'FamilyContact'},
            'Priority': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Relationship': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.FamilyRelationship']"}),
            'RelationshipDesc': ('django.db.models.fields.TextField', [], {'max_length': '500', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.familydocument': {
            'DocumentId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Document']"}),
            'FamilyId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Family']"}),
            'Meta': {'object_name': 'FamilyDocument'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.familynotes': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'FamilyId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Family']"}),
            'Meta': {'object_name': 'FamilyNotes'},
            'NoteId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.NoteDetails']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.familyrelationship': {
            'Meta': {'object_name': 'FamilyRelationship'},
            'RelType': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.gradeinputelement': {
            'DisplayOrder': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'GradeRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'GradeSubject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Subject']"}),
            'InputForm': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'ordering': "['GradeRecord', 'DisplayOrder']", 'object_name': 'GradeInputElement'},
            'PrevGradeRecord': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'PrevGradeRecord'", 'null': 'True', 'to': u"orm['MIS.ExtentionRecords']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.gradeinputfields': {
            'FieldList': ('picklefield.fields.PickledObjectField', [], {}),
            'GradeRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'GradeSubject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Subject']"}),
            'Meta': {'object_name': 'GradeInputFields'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.group': {
            'MenuName': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'ordering': "['Name']", 'object_name': 'Group'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.groupmenuitems': {
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'GroupRequirements': ('django.db.models.fields.CharField', [], {'max_length': '12', 'blank': 'True'}),
            'MenuItem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.MenuItems']"}),
            'MenuLevel': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'object_name': 'GroupMenuItems'},
            'ReqParms': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.MenuSubMenu']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.letterbody': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AttachedTo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'BodyText': ('django.db.models.fields.TextField', [], {}),
            'BodyVariables': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'CreatedBy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            'CreatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 297185)'}),
            'Description': ('django.db.models.fields.TextField', [], {'default': '"\'Please enter\\n    text to help users use this letter"'}),
            'ExtraStaffAccess': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'LetterStaffAccess'", 'null': 'True', 'to': u"orm['MIS.Group']"}),
            'LetterTemplate': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.LetterTemplate']"}),
            'Meta': {'object_name': 'LetterBody'},
            'ModifiedBY': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'letter_Modified_by'", 'to': u"orm['MIS.Staff']"}),
            'ModifiedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 297238)'}),
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ObjectBaseType': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'SignatureName': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'SignatureTitle': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.lettertemplate': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Filename': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'LetterBaseType': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Meta': {'object_name': 'LetterTemplate'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menuactions': {
            'MenuName': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'MenuOrder': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'object_name': 'MenuActions'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menuitems': {
            'ItemURL': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Meta': {'object_name': 'MenuItems'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menus': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Manage': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ManagedGroups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'SubGroups'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['MIS.Group']"}),
            'MenuActions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['MIS.MenuActions']", 'null': 'True', 'blank': 'True'}),
            'MenuType': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.MenuTypes']"}),
            'Meta': {'ordering': "['MenuType', 'Order']", 'unique_together': "(('MenuType', 'Order'),)", 'object_name': 'Menus'},
            'Order': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'SubMenus': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['MIS.MenuSubMenu']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menusubmenu': {
            'MenuName': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'MenuOrder': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'object_name': 'MenuSubMenu'},
            'Name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menutypes': {
            'Desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Logo': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'MenuLevel': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'ordering': "['Name']", 'object_name': 'MenuTypes'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'schoolMenu'", 'to': u"orm['MIS.School']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.notedetails': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'HousePoints': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'HousePointsType': ('django.db.models.fields.CharField', [], {'default': "'NA'", 'max_length': '2'}),
            'IsAnIssue': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'IssueStatus': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Keywords': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Meta': {'object_name': 'NoteDetails'},
            'ModifiedBy': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'NoteModifiedBy'", 'to': u"orm['MIS.Staff']"}),
            'ModifiedDate': ('django.db.models.fields.DateTimeField', [], {}),
            'NoteText': ('django.db.models.fields.TextField', [], {}),
            'RaisedBy': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'NoteRaisedBy'", 'to': u"orm['MIS.Staff']"}),
            'RaisedDate': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.notekeywords': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'HousePointsTrigger': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Keyword': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'KeywordPool': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'KeywordType': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'Meta': {'object_name': 'NoteKeywords'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.notenotification': {
            'Keywords': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'NoteNotification'},
            'NotificationGroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Pupilgroup': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'PupilGroupNotification'", 'to': u"orm['MIS.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.notetoissue': {
            'Keywords': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'NoteToIssue'},
            'NotificationGroup': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'IssueStaffLink'", 'to': u"orm['MIS.Group']"}),
            'Pupilgroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.parentsubjects': {
            'Meta': {'object_name': 'ParentSubjects'},
            'ParentSubject': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ParentSubject'", 'to': u"orm['MIS.Subject']"}),
            'Subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Subject']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupil': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname', 'Forename']", 'object_name': 'Pupil'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Picture': ('django.db.models.fields.files.ImageField', [], {'default': "'media/images/StudentPhotos/Student.png'", 'max_length': '100'}),
            'RegistrationDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 4, 25)'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'User': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilalert': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AlertClosedDetails': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'AlertDetails': ('django.db.models.fields.TextField', [], {}),
            'AlertType': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.AlertType']"}),
            'ClosedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'AlertCloseStaffId'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'Meta': {'object_name': 'PupilAlert'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'RaisedBy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            'StartDate': ('django.db.models.fields.DateField', [], {}),
            'StopDate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'UpdateDate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 289194)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilattendance': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AmPm': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'AttendanceDate': ('django.db.models.fields.DateField', [], {}),
            'AttendanceType': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.AttendanceCode']"}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'ordering': "['AttendanceDate', 'Pupil']", 'object_name': 'PupilAttendance'},
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'RecordedDate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 280216)'}),
            'Teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilcontact': {
            'FamilyContactId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.FamilyContact']"}),
            'Meta': {'object_name': 'PupilContact'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupildocument': {
            'DocumentId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Document']"}),
            'Meta': {'object_name': 'PupilDocument'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilfutureschoolprogress': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Details': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'EntryYear': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'Interview': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'LastUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            'Meta': {'object_name': 'PupilFutureSchoolProgress'},
            'Progress': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'School': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.SchoolList']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilgiftedtalented': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'DateAdded': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'DateClosed': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'Date_Identified': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'Date_Not_Gifted_and_Talented': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'Details': ('django.db.models.fields.TextField', [], {}),
            'Level': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Mentor': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'PupilGiftedTalented'},
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'Reasons_for_Removal_Gifted_and_Talented': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'Subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Subject']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilgroup': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'DateAdded': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 4, 25)'}),
            'DateInactivated': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 4, 25)'}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'PupilGroup'},
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilnotes': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Meta': {'object_name': 'PupilNotes'},
            'NoteId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.NoteDetails']"}),
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilprovision': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Details': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'PupilProvision'},
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'SENType': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'StartDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'StopDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilsnapshot': {
            'GradeRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'Meta': {'object_name': 'PupilSnapshot'},
            'PupilRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'Snapshot': ('picklefield.fields.PickledObjectField', [], {}),
            'TimeStamp': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 288185)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilsnextschool': {
            'Meta': {'object_name': 'PupilsNextSchool'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.SchoolList']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilspastschool': {
            'Meta': {'object_name': 'PupilsPastSchool'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.SchoolList']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupiltarget': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Meta': {'object_name': 'PupilTarget'},
            'Progress': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'Review': ('django.db.models.fields.TextField', [], {}),
            'ReviewDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'Target': ('django.db.models.fields.TextField', [], {}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.reportingseason': {
            'GradeGroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'GradeRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'GradeStartDay': ('django.db.models.fields.IntegerField', [], {}),
            'GradeStopDay': ('django.db.models.fields.IntegerField', [], {}),
            'Meta': {'object_name': 'ReportingSeason'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.rolloverrule': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ApplicantGroup': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'RollOver_AppGroup'", 'null': 'True', 'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'RollOverRule'},
            'NewGroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'OldGroup': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'RollOver_OldGroup'", 'null': 'True', 'to': u"orm['MIS.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.school': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']"}),
            'HeadTeacher': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'School'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'TemplateBanner': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBanner.jpg'", 'max_length': '200'}),
            'TemplateCSS': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBasic.css'", 'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.schooldocument': {
            'DocumentId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Document']"}),
            'Meta': {'object_name': 'SchoolDocument'},
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'school-docs'", 'to': u"orm['MIS.School']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.schoollist': {
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']"}),
            'Desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'HeadTeacher': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'SchoolList'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staff': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']", 'null': 'True', 'blank': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'DefaultMenu': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.MenuTypes']", 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'FailedLoginAttempts': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname']", 'object_name': 'Staff'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ResetPassword': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'User': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffcpdrecords': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'CertIssued': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'DateExpires': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'DateTaken': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 4, 25)'}),
            'Details': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Duration': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'InternalExternal': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'Meta': {'object_name': 'StaffCpdRecords'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Provider': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Staff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            'TimeTaken': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffdocument': {
            'DocumentId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Document']"}),
            'Meta': {'object_name': 'StaffDocument'},
            'StaffId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffemergencycontact': {
            'AddressId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']"}),
            'Meta': {'object_name': 'StaffEmergencyContact'},
            'Priority': ('django.db.models.fields.SmallIntegerField', [], {}),
            'StaffId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffgroup': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'unique_together': "(('AcademicYear', 'Staff', 'Group'),)", 'object_name': 'StaffGroup'},
            'Staff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffgroupfavourite': {
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'StaffGroupFavourite'},
            'PoolName': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Staff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffnotes': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Meta': {'object_name': 'StaffNotes'},
            'NoteId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.NoteDetails']"}),
            'StaffId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.subject': {
            'Desc': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Meta': {'ordering': "['Name']", 'object_name': 'Subject'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.systemvardata': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'Data': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'unique_together': "(('Variable', 'AcademicYear'),)", 'object_name': 'SystemVarData'},
            'Variable': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.SystemVariable']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.systemvariable': {
            'DataType': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'Meta': {'object_name': 'SystemVariable'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'VariableType': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 406820)'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 35, 51, 406396)'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['MIS']
