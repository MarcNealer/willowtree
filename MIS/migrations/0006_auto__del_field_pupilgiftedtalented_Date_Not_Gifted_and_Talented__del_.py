# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Deleting field 'PupilGiftedTalented.Date_Not_Gifted_and_Talented'
        db.delete_column(u'MIS_pupilgiftedtalented', 'Date_Not_Gifted_and_Talented')

        # Deleting field 'PupilGiftedTalented.Mentor'
        db.delete_column(u'MIS_pupilgiftedtalented', 'Mentor')

        # Deleting field 'PupilGiftedTalented.Reasons_for_Removal_Gifted_and_Talented'
        db.delete_column(u'MIS_pupilgiftedtalented', 'Reasons_for_Removal_Gifted_and_Talented')

        # Deleting field 'PupilGiftedTalented.Date_Identified'
        db.delete_column(u'MIS_pupilgiftedtalented', 'Date_Identified')


    def backwards(self, orm):
        
        # Adding field 'PupilGiftedTalented.Date_Not_Gifted_and_Talented'
        db.add_column(u'MIS_pupilgiftedtalented', 'Date_Not_Gifted_and_Talented', self.gf('django.db.models.fields.DateField')(null=True, blank=True), keep_default=False)

        # Adding field 'PupilGiftedTalented.Mentor'
        db.add_column(u'MIS_pupilgiftedtalented', 'Mentor', self.gf('django.db.models.fields.TextField')(null=True, blank=True), keep_default=False)

        # Adding field 'PupilGiftedTalented.Reasons_for_Removal_Gifted_and_Talented'
        db.add_column(u'MIS_pupilgiftedtalented', 'Reasons_for_Removal_Gifted_and_Talented', self.gf('django.db.models.fields.TextField')(null=True, blank=True), keep_default=False)

        # Adding field 'PupilGiftedTalented.Date_Identified'
        db.add_column(u'MIS_pupilgiftedtalented', 'Date_Identified', self.gf('django.db.models.fields.DateField')(null=True, blank=True), keep_default=False)


    models = {
        u'MIS.address': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AddressLine1': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'AddressLine2': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'AddressLine3': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'AddressLine4': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Country': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'HomeSalutation': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'object_name': 'Address'},
            'Note': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Phone1': ('django.db.models.fields.CharField', [], {'max_length': '150', 'blank': 'True'}),
            'Phone2': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'Phone3': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            'PostCode': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'PostalTitle': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.alertgroup': {
            'DisplayOrder': ('django.db.models.fields.IntegerField', [], {'max_length': '2', 'blank': 'True'}),
            'Meta': {'object_name': 'AlertGroup'},
            'Name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'PermissionGroups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['MIS.Group']", 'null': 'True', 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.alerttype': {
            'AlertGroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.AlertGroup']"}),
            'Desc': ('django.db.models.fields.TextField', [], {}),
            'Icon': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'Meta': {'object_name': 'AlertType'},
            'Name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.attendancecode': {
            'AttendanceAccess': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'AttendanceCode': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'AttendanceCodeType': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'AttendanceDesc': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'AttendanceStatistical': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Meta': {'ordering': "['AttendanceCode']", 'object_name': 'AttendanceCode'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.attendancenottaken': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.AttendanceNotTakenCode']"}),
            'Date': ('django.db.models.fields.DateField', [], {}),
            'Details': ('django.db.models.fields.TextField', [], {}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']", 'null': 'True', 'blank': 'True'}),
            'Meta': {'object_name': 'AttendanceNotTaken'},
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '7'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.attendancenottakencode': {
            'Code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '1'}),
            'Desc': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'AttendanceNotTakenCode'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.attendancetaken': {
            'AmPm': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'AttendanceDate': ('django.db.models.fields.DateField', [], {}),
            'AttendanceType': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'AttendanceTaken'},
            'RecordedDate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 814279)'}),
            'Teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.contact': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']", 'null': 'True', 'blank': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'FamilyId': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['MIS.Family']", 'null': 'True', 'blank': 'True'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname', 'Forename']", 'object_name': 'Contact'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Pupils': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['MIS.Pupil']", 'null': 'True', 'blank': 'True'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'WorkAddress': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'WorkAddress'", 'null': 'True', 'to': u"orm['MIS.Address']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.contactnotes': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ContactId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Contact']"}),
            'Meta': {'object_name': 'ContactNotes'},
            'NoteId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.NoteDetails']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.document': {
            'DocumentKeywords': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'DocumentName': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Filename': ('django.db.models.fields.files.FileField', [], {'max_length': '300'}),
            'Meta': {'object_name': 'Document'},
            'UploadDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 4, 25)'}),
            'UploadStaff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.eventqueue': {
            'Details': ('django.db.models.fields.TextField', [], {}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'EventQueue'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Priority': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'TimeStamp': ('django.db.models.fields.DateTimeField', [], {}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.eventtrigger': {
            'Action': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'EventTrigger'},
            'Priority': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'Staff': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'EventStaffGroup'", 'to': u"orm['MIS.Group']"}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionboolean': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Data': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionBoolean'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentiondata': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'BaseId': ('django.db.models.fields.PositiveIntegerField', [], {}),
            'BaseType': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'Data': ('picklefield.fields.PickledObjectField', [], {}),
            'ExtentionRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'Meta': {'object_name': 'ExtentionData'},
            'Subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Subject']"}),
            'UpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionsUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'UpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 802265)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentiondate': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Data': ('django.db.models.fields.DateField', [], {}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionDate'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionfields': {
            'Meta': {'object_name': 'ExtentionFields'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'PickList': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionPickList']"}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionforcontact': {
            'ContactId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Contact']"}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ExtentionIndexContact'", 'to': u"orm['MIS.ExtentionsIndex']"}),
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'ExtentionSubject': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ExtentionUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionsContactsUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'ExtentionUpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 806371)'}),
            'Meta': {'unique_together': "(('ContactId', 'ExtentionRecordId', 'ExtentionSubject'),)", 'object_name': 'ExtentionForContact'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionforfamily': {
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ExtentionIndexFamily'", 'to': u"orm['MIS.ExtentionsIndex']"}),
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'ExtentionSubject': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ExtentionUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionsFamilyUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'ExtentionUpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 805774)'}),
            'FamilyId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Family']"}),
            'Meta': {'unique_together': "(('FamilyId', 'ExtentionRecordId', 'ExtentionSubject'),)", 'object_name': 'ExtentionForFamily'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionforpupil': {
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ExtentionIndexPupil'", 'to': u"orm['MIS.ExtentionsIndex']"}),
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'ExtentionSubject': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ExtentionUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionsPupilUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'ExtentionUpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 804570)'}),
            'Meta': {'unique_together': "(('PupilId', 'ExtentionRecordId', 'ExtentionSubject'),)", 'object_name': 'ExtentionForPupil'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionforschool': {
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ExtentionIndexSchool'", 'to': u"orm['MIS.ExtentionsIndex']"}),
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'ExtentionSubject': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ExtentionUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionsSchoolsUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'ExtentionUpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 807132)'}),
            'Meta': {'unique_together': "(('SchoolId', 'ExtentionRecordId', 'ExtentionSubject'),)", 'object_name': 'ExtentionForSchool'},
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'school_extention'", 'to': u"orm['MIS.School']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionforstaff': {
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionIndexStaff'", 'null': 'True', 'to': u"orm['MIS.ExtentionsIndex']"}),
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'ExtentionSubject': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ExtentionUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'ExtentionStaffUpdatedBy'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'ExtentionUpdatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 805175)'}),
            'Meta': {'unique_together': "(('StaffId', 'ExtentionRecordId', 'ExtentionSubject'),)", 'object_name': 'ExtentionForStaff'},
            'StaffId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionnumerical': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AlternativeValue': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Data': ('django.db.models.fields.FloatField', [], {}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionNumerical'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionpicklist': {
            'Data': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'ExtentionPickList'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionrecords': {
            'Fields': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['MIS.ExtentionFields']", 'symmetrical': 'False'}),
            'FullName': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'ordering': "['Name']", 'object_name': 'ExtentionRecords'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionsindex': {
            'ExtentionRecordId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'Meta': {'object_name': 'ExtentionsIndex'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentionstring': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AlternativeValue': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Data': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionString'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentiontextbox': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Data': ('django.db.models.fields.TextField', [], {}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionTextBox'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.extentiontime': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Data': ('django.db.models.fields.TimeField', [], {}),
            'ExtentionIndexId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionsIndex']"}),
            'FieldId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionFields']"}),
            'Meta': {'object_name': 'ExtentionTime'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.family': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['MIS.Address']", 'symmetrical': 'False'}),
            'FamilyName': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '100'}),
            'Meta': {'ordering': "['FamilyName']", 'object_name': 'Family'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.familychildren': {
            'FamilyId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Family']"}),
            'FamilyType': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'Meta': {'ordering': "['FamilyId']", 'object_name': 'FamilyChildren'},
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'RelationshipDesc': ('django.db.models.fields.TextField', [], {'max_length': '500', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.familycontact': {
            'Contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Contact']"}),
            'FamilyId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Family']"}),
            'Meta': {'ordering': "['FamilyId']", 'object_name': 'FamilyContact'},
            'Priority': ('django.db.models.fields.SmallIntegerField', [], {'null': 'True', 'blank': 'True'}),
            'Relationship': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.FamilyRelationship']"}),
            'RelationshipDesc': ('django.db.models.fields.TextField', [], {'max_length': '500', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.familydocument': {
            'DocumentId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Document']"}),
            'FamilyId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Family']"}),
            'Meta': {'object_name': 'FamilyDocument'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.familynotes': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'FamilyId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Family']"}),
            'Meta': {'object_name': 'FamilyNotes'},
            'NoteId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.NoteDetails']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.familyrelationship': {
            'Meta': {'object_name': 'FamilyRelationship'},
            'RelType': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.gradeinputelement': {
            'DisplayOrder': ('django.db.models.fields.SmallIntegerField', [], {'default': '1'}),
            'GradeRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'GradeSubject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Subject']"}),
            'InputForm': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'ordering': "['GradeRecord', 'DisplayOrder']", 'object_name': 'GradeInputElement'},
            'PrevGradeRecord': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'PrevGradeRecord'", 'null': 'True', 'to': u"orm['MIS.ExtentionRecords']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.gradeinputfields': {
            'FieldList': ('picklefield.fields.PickledObjectField', [], {}),
            'GradeRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'GradeSubject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Subject']"}),
            'Meta': {'object_name': 'GradeInputFields'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.group': {
            'MenuName': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Meta': {'ordering': "['Name']", 'object_name': 'Group'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '500'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.groupmenuitems': {
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'GroupRequirements': ('django.db.models.fields.CharField', [], {'max_length': '12', 'blank': 'True'}),
            'MenuItem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.MenuItems']"}),
            'MenuLevel': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'object_name': 'GroupMenuItems'},
            'ReqParms': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.MenuSubMenu']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.letterbody': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AttachedTo': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'BodyText': ('django.db.models.fields.TextField', [], {}),
            'BodyVariables': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'CreatedBy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            'CreatedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 830169)'}),
            'Description': ('django.db.models.fields.TextField', [], {'default': '"\'Please enter\\n    text to help users use this letter"'}),
            'ExtraStaffAccess': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'LetterStaffAccess'", 'null': 'True', 'to': u"orm['MIS.Group']"}),
            'LetterTemplate': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.LetterTemplate']"}),
            'Meta': {'object_name': 'LetterBody'},
            'ModifiedBY': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'letter_Modified_by'", 'to': u"orm['MIS.Staff']"}),
            'ModifiedOn': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 830221)'}),
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ObjectBaseType': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'SignatureName': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'SignatureTitle': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.lettertemplate': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Filename': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'LetterBaseType': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Meta': {'object_name': 'LetterTemplate'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menuactions': {
            'MenuName': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'MenuOrder': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'object_name': 'MenuActions'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menuitems': {
            'ItemURL': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Meta': {'object_name': 'MenuItems'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menus': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Manage': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'ManagedGroups': ('django.db.models.fields.related.ManyToManyField', [], {'blank': 'True', 'related_name': "'SubGroups'", 'null': 'True', 'symmetrical': 'False', 'to': u"orm['MIS.Group']"}),
            'MenuActions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['MIS.MenuActions']", 'null': 'True', 'blank': 'True'}),
            'MenuType': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.MenuTypes']"}),
            'Meta': {'ordering': "['MenuType', 'Order']", 'unique_together': "(('MenuType', 'Order'),)", 'object_name': 'Menus'},
            'Order': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'SubMenus': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['MIS.MenuSubMenu']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menusubmenu': {
            'MenuName': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'MenuOrder': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'object_name': 'MenuSubMenu'},
            'Name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.menutypes': {
            'Desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Logo': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'MenuLevel': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'Meta': {'ordering': "['Name']", 'object_name': 'MenuTypes'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'schoolMenu'", 'to': u"orm['MIS.School']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.notedetails': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'HousePoints': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'HousePointsType': ('django.db.models.fields.CharField', [], {'default': "'NA'", 'max_length': '2'}),
            'IsAnIssue': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'IssueStatus': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Keywords': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Meta': {'object_name': 'NoteDetails'},
            'ModifiedBy': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'NoteModifiedBy'", 'to': u"orm['MIS.Staff']"}),
            'ModifiedDate': ('django.db.models.fields.DateTimeField', [], {}),
            'NoteText': ('django.db.models.fields.TextField', [], {}),
            'RaisedBy': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'NoteRaisedBy'", 'to': u"orm['MIS.Staff']"}),
            'RaisedDate': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.notekeywords': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'HousePointsTrigger': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'Keyword': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'KeywordPool': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'KeywordType': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'Meta': {'object_name': 'NoteKeywords'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.notenotification': {
            'Keywords': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'NoteNotification'},
            'NotificationGroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Pupilgroup': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'PupilGroupNotification'", 'to': u"orm['MIS.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.notetoissue': {
            'Keywords': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'NoteToIssue'},
            'NotificationGroup': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'IssueStaffLink'", 'to': u"orm['MIS.Group']"}),
            'Pupilgroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.parentsubjects': {
            'Meta': {'object_name': 'ParentSubjects'},
            'ParentSubject': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'ParentSubject'", 'to': u"orm['MIS.Subject']"}),
            'Subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Subject']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupil': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname', 'Forename']", 'object_name': 'Pupil'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Picture': ('django.db.models.fields.files.ImageField', [], {'default': "'media/images/StudentPhotos/Student.png'", 'max_length': '100'}),
            'RegistrationDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 4, 25)'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'User': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilalert': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AlertClosedDetails': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'AlertDetails': ('django.db.models.fields.TextField', [], {}),
            'AlertType': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.AlertType']"}),
            'ClosedBy': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'AlertCloseStaffId'", 'null': 'True', 'to': u"orm['MIS.Staff']"}),
            'Meta': {'object_name': 'PupilAlert'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'RaisedBy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            'StartDate': ('django.db.models.fields.DateField', [], {}),
            'StopDate': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'UpdateDate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 822139)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilattendance': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'AmPm': ('django.db.models.fields.CharField', [], {'max_length': '2', 'blank': 'True'}),
            'AttendanceDate': ('django.db.models.fields.DateField', [], {}),
            'AttendanceType': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Code': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.AttendanceCode']"}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'ordering': "['AttendanceDate', 'Pupil']", 'object_name': 'PupilAttendance'},
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'RecordedDate': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 813153)'}),
            'Teacher': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilcontact': {
            'FamilyContactId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.FamilyContact']"}),
            'Meta': {'object_name': 'PupilContact'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupildocument': {
            'DocumentId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Document']"}),
            'Meta': {'object_name': 'PupilDocument'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilfutureschoolprogress': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Details': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'EntryYear': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'Interview': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'LastUpdatedBy': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            'Meta': {'object_name': 'PupilFutureSchoolProgress'},
            'Progress': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'School': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.SchoolList']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilgiftedtalented': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'DateAdded': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'DateClosed': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'Details': ('django.db.models.fields.TextField', [], {}),
            'Level': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'object_name': 'PupilGiftedTalented'},
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'Subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Subject']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilgroup': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'DateAdded': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 4, 25)'}),
            'DateInactivated': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 4, 25)'}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'PupilGroup'},
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilnotes': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Meta': {'object_name': 'PupilNotes'},
            'NoteId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.NoteDetails']"}),
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilprovision': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Details': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'object_name': 'PupilProvision'},
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'SENType': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'StartDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'StopDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilsnapshot': {
            'GradeRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'Meta': {'object_name': 'PupilSnapshot'},
            'PupilRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'Snapshot': ('picklefield.fields.PickledObjectField', [], {}),
            'TimeStamp': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 55, 821132)'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilsnextschool': {
            'Meta': {'object_name': 'PupilsNextSchool'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.SchoolList']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupilspastschool': {
            'Meta': {'object_name': 'PupilsPastSchool'},
            'PupilId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.SchoolList']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.pupiltarget': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Meta': {'object_name': 'PupilTarget'},
            'Progress': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'Pupil': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Pupil']"}),
            'Review': ('django.db.models.fields.TextField', [], {}),
            'ReviewDate': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime.now'}),
            'Target': ('django.db.models.fields.TextField', [], {}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.reportingseason': {
            'GradeGroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'GradeRecord': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.ExtentionRecords']"}),
            'GradeStartDay': ('django.db.models.fields.IntegerField', [], {}),
            'GradeStopDay': ('django.db.models.fields.IntegerField', [], {}),
            'Meta': {'object_name': 'ReportingSeason'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.rolloverrule': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'ApplicantGroup': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'RollOver_AppGroup'", 'null': 'True', 'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'RollOverRule'},
            'NewGroup': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'OldGroup': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'RollOver_OldGroup'", 'null': 'True', 'to': u"orm['MIS.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.school': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']"}),
            'HeadTeacher': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'School'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'TemplateBanner': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBanner.jpg'", 'max_length': '200'}),
            'TemplateCSS': ('django.db.models.fields.CharField', [], {'default': "'LetterCSS/LetterBasic.css'", 'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.schooldocument': {
            'DocumentId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Document']"}),
            'Meta': {'object_name': 'SchoolDocument'},
            'SchoolId': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'school-docs'", 'to': u"orm['MIS.School']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.schoollist': {
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']"}),
            'Desc': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'HeadTeacher': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Meta': {'object_name': 'SchoolList'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Type': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staff': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Address': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']", 'null': 'True', 'blank': 'True'}),
            'DateOfBirth': ('django.db.models.fields.DateField', [], {'default': 'datetime.datetime(1972, 1, 1, 0, 0)'}),
            'DefaultMenu': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.MenuTypes']", 'null': 'True', 'blank': 'True'}),
            'EmailAddress': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'FailedLoginAttempts': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'Forename': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'Meta': {'ordering': "['Surname']", 'object_name': 'Staff'},
            'NickName': ('django.db.models.fields.CharField', [], {'max_length': '50', 'blank': 'True'}),
            'Old_Id': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'OtherNames': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'Picture': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'ResetPassword': ('django.db.models.fields.SmallIntegerField', [], {'default': '0'}),
            'Surname': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'Title': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'User': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffcpdrecords': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'CertIssued': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'DateExpires': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'DateTaken': ('django.db.models.fields.DateField', [], {'default': 'datetime.date(2014, 4, 25)'}),
            'Details': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'Duration': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            'InternalExternal': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'Meta': {'object_name': 'StaffCpdRecords'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Provider': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'Staff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            'TimeTaken': ('django.db.models.fields.CharField', [], {'max_length': '20', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffdocument': {
            'DocumentId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Document']"}),
            'Meta': {'object_name': 'StaffDocument'},
            'StaffId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffemergencycontact': {
            'AddressId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Address']"}),
            'Meta': {'object_name': 'StaffEmergencyContact'},
            'Priority': ('django.db.models.fields.SmallIntegerField', [], {}),
            'StaffId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffgroup': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'unique_together': "(('AcademicYear', 'Staff', 'Group'),)", 'object_name': 'StaffGroup'},
            'Staff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffgroupfavourite': {
            'Group': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Group']"}),
            'Meta': {'object_name': 'StaffGroupFavourite'},
            'PoolName': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'Staff': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.staffnotes': {
            'Active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'Meta': {'object_name': 'StaffNotes'},
            'NoteId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.NoteDetails']"}),
            'StaffId': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.Staff']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.subject': {
            'Desc': ('django.db.models.fields.CharField', [], {'max_length': '300'}),
            'Meta': {'ordering': "['Name']", 'object_name': 'Subject'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.systemvardata': {
            'AcademicYear': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'Data': ('django.db.models.fields.TextField', [], {}),
            'Meta': {'unique_together': "(('Variable', 'AcademicYear'),)", 'object_name': 'SystemVarData'},
            'Variable': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['MIS.SystemVariable']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'MIS.systemvariable': {
            'DataType': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'Meta': {'object_name': 'SystemVariable'},
            'Name': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'VariableType': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 56, 156836)'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime(2014, 4, 25, 13, 54, 56, 156410)'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        }
    }

    complete_apps = ['MIS']
