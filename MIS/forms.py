#-------------------------------------------------------------------------------
# Name:        Django Forms for WillowTree
# Purpose:
#
# Author:      DBMgr
#
# Created:     02/02/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from django import forms
from MIS.models import *

Gender_Choices = (
(u'M',u'Male'),
(u'F',u'Female'))
ReadOnlyAttr={'readonly':'readonly','style':'background-color : gainsboro'}
CheckedAttr={'checked':'checked'}
CheckedReadOnlyAttr=dict(ReadOnlyAttr.items()+CheckedAttr.items())
DateInputFormats=['%d-%m-%Y','%d-%m-%y','%d/%m/%Y','%d/%m/%y','%d %m %y','%d %m %Y']

class AddressForm(forms.Form):
    HomeSalutation = forms.CharField(label=' Home Salutation')
    PostalTitle =  forms.CharField(label='Postal Title')
    AddressLine1 =  forms.CharField(label='Address Line 1')
    AddressLine2 =  forms.CharField(label='Address Line 2')
    AddressLine3 =  forms.CharField(label='Address Line 3')    
    AddressLine4 =  forms.CharField(label='Address Line 4')    
    PostCode = forms.CharField()
    Phone1 = forms.CharField(label='Phone 1')
    Phone2 = forms.CharField(label='Phone 2')
    Phone3 = forms.CharField(label='Phone 3')
    EmailAddress = forms.EmailField(label='Email Address')
    Note =  forms.CharField(widget=forms.Textarea,required=False)


class NewBase(forms.Form):
    Forename=forms.CharField(label='Applicant Forename')
    Surname=forms.CharField(label='Applicant Surname')
    OtherNames=forms.CharField(label='Applicant Middle Names',required=False)
    NickName=forms.CharField(label='Applicant Known As',required=False)
    Title=forms.CharField(label='Applicant Title',required=False)
    DOB=forms.DateField(label='Applicant Date of Birth',input_formats=DateInputFormats)
    Gender=forms.ChoiceField(widget=forms.RadioSelect, choices=Gender_Choices,label='Applicant Gender')
    