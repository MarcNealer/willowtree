from MIS.models import *
from django import template


class SchoolLetterRecord():
    """
    This class is used for generating school letters
    """
    def __init__(self, school_id, request, ac_year):
        self.School = SchoolList.objects.get(id=school_id)
        self.request = request
        self.ac_year = ac_year
        self.letter_body = None

    def generate_letter_body(self, letter_details, var_list, other_items):
        t = template.Template(letter_details.BodyText.replace('&quot;', '"'))
        c = template.Context({'School': self.School,
                              'Var': var_list,
                              'OtherItems': other_items})
        self.letter_body = t.render(c)
        return


class generateNewEmailAddress():
    ''' This class generates an Email address for a person based upon
    their type (eg: Pupil), firstname and surname. This cross references
    the database to check no clashes can occur'''
    def __init__(self, firstname, surname, school):
        self.firstname = firstname
        self.surname = surname
        self.schoolName = MenuTypes.objects.get(Name=school).SchoolId.Name

    def pupilEmail(self):
        ''' Generates a unique Email address for a single pupil'''
        EList = []
        for i in Pupil.objects.all():
            EList.append(i.EmailAddress.lower())
        if self.schoolName == 'Battersea':
            emailAddressPartSchool = 'Bat'
        elif self.schoolName == 'Clapham':
            emailAddressPartSchool = 'Cla'
        elif self.schoolName == 'Fulham':
            emailAddressPartSchool = 'Ful'
        elif self.schoolName == 'Kensington':
            emailAddressPartSchool = 'Ken'
        emailAddressPart2 = "%s.thomasvle.org" % emailAddressPartSchool
        emailAddressPart1 = '%s%s' % (self.firstname[0], self.surname[:3])
        for i in range(1, 21):
            pupilEmail = "%s%d@%s" % (emailAddressPart1, i, emailAddressPart2)
            if pupilEmail.lower() not in EList:
                break
            else:
                pass
        return pupilEmail