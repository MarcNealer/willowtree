from MIS.models import Pupil
from MIS.models import PupilProvision
#from MIS.models import PupilTarget


class SenProvisions():
    ''' This Class manages Provisions for a Pupil '''
    def __init__(self, PupilNo):
        self.PupilNo = PupilNo

    def __createPupilObj__(self):
        ''' Creates a pupil object '''
        return Pupil.objects.get(id=self.PupilNo)

    def addProvision(self, provisiontype,
                     startDate,
                     stopDate,
                     provisionText=False):
        ''' Adds a new sen provision to a pupil '''
        provisionObj = PupilProvision(Pupil=self.__createPupilObj__(),
                                      Type=provisiontype)
        if provisionText:
            provisionObj.Details = provisionText
        provisionObj.save()
        return