# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.core.context_processors import csrf
from django.http import *
from django.utils.html import strip_tags
from django.core import serializers


# python imports
import csv
import datetime
import json


# WillowTree system imports
from MIS.ViewExtras import SetMenu
from MIS.ViewExtras import SetLogo
from MIS.models import SchoolList
from MIS.models import Address
from MIS.models import PupilsNextSchool
from MIS.models import PupilFutureSchoolProgress
from MIS.models import MenuTypes
from MIS.models import LetterBody


def pastSchools(request, school, AcYear):
    ''' displays a list of past schools pupil or applicants  '''
    c = {'schools': sorted(SchoolList.objects.all(), key=lambda k: k.Name)}
    c.update({'TypeofSchool': ['KinderGarten', 'Prep', 'Secondary']})
    c.update({'school': school, 'AcYear': AcYear, 'GroupName': 'School'})
    c.update({'LetterList': LetterBody.objects.filter(ObjectBaseType='School', Active=True)})
    conInst = RequestContext(request, processors=[SetMenu])
    return render_to_response('pastSchools.html', c, context_instance=conInst)


def pastSchoolsEdit(request, school, AcYear, SchoolId):
    ''' saves modified school data '''
    school2 = SchoolList.objects.get(id=SchoolId)
    address = Address.objects.get(id=school2.Address.id)
    school2.Name = request.POST['NameofSchool']
    address.HomeSalutation = request.POST['NameofSchool']
    address.PostalTitle = request.POST['PostalTitle']
    school2.HeadTeacher = request.POST['HeadTeacher']
    address.AddressLine1 = request.POST['AddressLine1']
    address.AddressLine2 = request.POST['AddressLine2']
    address.PostCode = request.POST['Postcode']
    if request.POST['Description']:
        school2.Desc = request.POST['Description']
    if request.POST['Type']:
        school2.Type = request.POST['Type']
    if request.POST['AddressLine3']:
        address.AddressLine3 = request.POST['AddressLine3']
    if request.POST['AddressLine4']:
        address.AddressLine4 = request.POST['AddressLine4']
    if request.POST['Country']:
        address.Country = request.POST['Country']
    if request.POST['PhoneNumber1']:
        address.Phone1 = request.POST['PhoneNumber1']
    if request.POST['PhoneNumber2']:
        address.Phone2 = request.POST['PhoneNumber2']
    if request.POST['PhoneNumber3']:
        address.Phone3 = request.POST['PhoneNumber3']
    if request.POST['EmailAddress']:
        address.EmailAddress = request.POST['EmailAddress']
    school2.save()
    address.save()
    varTuple = (school, AcYear)
    return HttpResponseRedirect('/WillowTree/%s/%s/pastSchools/' % varTuple)


def pastSchoolsNew(request, school, AcYear,
                   GroupName=None,
                   fromInterviewPopUp=None,
                   EditPupilOtherInformationPopUp=None,
                   PupilId=None):
    ''' add a new school '''
    ListOfSchools = SchoolList.objects.all()
    error = False
    for i in ListOfSchools:
        if i.Name == request.POST['NameofSchool'] and i.Address.PostCode == request.POST['Postcode']:
            error = True
    if error:
        conInst = RequestContext(request, processors=[SetMenu])
        return render_to_response('pastSchoolsError.html', context_instance=conInst)
    else:
        school2 = SchoolList(Name=request.POST['NameofSchool'],
                             HeadTeacher=request.POST['HeadTeacher'])
        address = Address(HomeSalutation=request.POST['NameofSchool'],
                          PostalTitle=request.POST['PostalTitle'],
                          AddressLine1=request.POST['AddressLine1'],
                          AddressLine2=request.POST['AddressLine2'],
                          PostCode=request.POST['Postcode'])
        if request.POST['Description']:
            school2.Desc = request.POST['Description']
        if request.POST['Type']:
            school2.Type = request.POST['Type']
        if request.POST['AddressLine3']:
            address.AddressLine3 = request.POST['AddressLine3']
        if request.POST['AddressLine4']:
            address.AddressLine4 = request.POST['AddressLine4']
        if request.POST['Country']:
            address.Country = request.POST['Country']
        if request.POST['PhoneNumber1']:
            address.Phone1 = request.POST['PhoneNumber1']
        if request.POST['PhoneNumber2']:
            address.Phone2 = request.POST['PhoneNumber2']
        if request.POST['PhoneNumber3']:
            address.Phone3 = request.POST['PhoneNumber3']
        if request.POST['EmailAddress']:
            address.EmailAddress = request.POST['EmailAddress']
        address.save()
        school2.Address = address
        school2.save()
        if fromInterviewPopUp:
            url = '/WillowTree/%s/%s/ManageThis/%s/'
            varTuple = (school, AcYear, GroupName)
            return HttpResponseRedirect(url % varTuple)
        if EditPupilOtherInformationPopUp:
            url = '/WillowTree/%s/%s/pupil/%s/'
            varTuple = (school, AcYear, PupilId)
            return HttpResponseRedirect(url % varTuple)
        else:
            url = '/WillowTree/%s/%s/pastSchools/'
            varTuple = (school, AcYear)
            return HttpResponseRedirect(url % varTuple)


def pastSchoolsDelete(request, school, AcYear, SchoolId):
    school2 = SchoolList.objects.get(id=int(SchoolId))
    address = Address.objects.get(schoollist__id=int(SchoolId))
    school2.delete()
    address.delete()
    schoolPupilLinks = PupilsNextSchool.objects.filter(SchoolId=int(SchoolId))
    for i in schoolPupilLinks:
        temp = i
        temp.delete()
    varTuple = (school, AcYear)
    return HttpResponseRedirect('/WillowTree/%s/%s/pastSchools/' % varTuple)


def futureSchoolsReportCSV(request, school, AcYear):
    ''' creates a future school CSV report '''
    if 'csv' in request.POST:
        today = datetime.datetime.now()
        todaysDate = '%s-%s-%s' % (today.day, today.month, today.year)
        fuSchs = []
        for pupilId in request.POST.getlist('PupilSelect'):
            for school in PupilFutureSchoolProgress.objects.filter(Pupil__id=int(pupilId),
                                                                   Active=True):
                fuSchs.append(school)
        response= HttpResponse(mimetype="text/csv")
        response_writer= csv.writer(response)
        response_writer.writerow(['Pupil Id',
                                  'Forename',
                                  'Surname',
                                  'Last Updated By',
                                  'Progress',
                                  'EntryYear',
                                  'Interview',
                                  'Details',
                                  'School',
                                  'Type',
                                  'HeadTeacher',
                                  'School Address - Home Salutation',
                                  'School Address - Postal Title',
                                  'School Address - Address Line 1',
                                  'School Address - Address Line 2',
                                  'School Address - Address Line 3',
                                  'School Address - Address Line 4',
                                  'School Address - PostCode',
                                  'School Address - Country',
                                  'School Address - Phone',
                                  'School Address - EmailAddress',
                                  'School Address - Notes'])
        for school in fuSchs:
            try:
                interviewDetails = '%s-%s-%s at %s' % (school.Interview.date().day,
                                                          school.Interview.date().month,
                                                          school.Interview.date().year,
                                                          school.Interview.time())
            except:
                interviewDetails = ''
            response_writer.writerow([school.Pupil.id,
                                      school.Pupil.Forename,
                                      school.Pupil.Surname,
                                      '%s %s' % (school.LastUpdatedBy.Forename,
                                                 school.LastUpdatedBy.Surname),
                                      school.Progress,
                                      school.EntryYear,
                                      interviewDetails,
                                      strip_tags(school.Details),
                                      school.School.Name,
                                      school.School.Type,
                                      school.School.HeadTeacher,
                                      school.School.Address.HomeSalutation,
                                      school.School.Address.PostalTitle,
                                      school.School.Address.AddressLine1,
                                      school.School.Address.AddressLine2,
                                      school.School.Address.AddressLine3,
                                      school.School.Address.AddressLine4,
                                      school.School.Address.PostCode,
                                      school.School.Address.Country,
                                      school.School.Address.Phone1,
                                      school.School.Address.EmailAddress])
        response['Content-Disposition'] = 'attachment; filename=futureSchoolsReport_%s.csv' % todaysDate
        return response
    elif 'html' in request.POST:
        pupil_ids = request.POST.getlist('PupilSelect')
        data_as_json = serializers.serialize("json",
                                             PupilFutureSchoolProgress.objects.filter(Pupil__id__in=pupil_ids),
                                             indent=2,
                                             use_natural_keys=True)
        c = {'future_schools_data': data_as_json,
             'school_and_group': "%s" % MenuTypes.objects.get(Name=school).SchoolId.Name}
        context = RequestContext(request, processors=[SetMenu])
        return render_to_response('Reports/FutureSchoolsReport.html', c,
                                  context_instance=context)