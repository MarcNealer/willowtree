from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache
import MIS.modules.PupilRecs
from MIS.modules.ExtendedRecords import *

class FamilyRecord():
    def __init__(self,request,AcYear,PupilId=None,FamilyId=None,GroupId=None, PupilList=None):
        self.request=request
        self.AcYear=AcYear
        self.FamilyId='Not assigned'
        self.PupilId=PupilId
        self.FamilyRecs=[]
        if FamilyId:
            self.FamilyId=Family.objects.get(id=int(FamilyId))
        elif PupilId:
            self.PupilDetails=MIS.modules.PupilRecs.PupilRecord(PupilId,AcYear)
            self.FamilyId=FamilyChildren.objects.get(Pupil=self.PupilDetails.Pupil.id,FamilyType=1).FamilyId
        if GroupId:
            self.Group=Group.objects.get(id=int(GroupId))
            self.GroupList=GeneralFunctions.Get_PupilList(AcYear,GroupName=self.Group.Name)
        elif PupilList:
            self.GroupId=None
            self.GroupList=PupilList
        else:
            self.GroupId=None
            self.GroupList=[]

        self.FamilyPupils=[]
        for FamilyPupils in FamilyChildren.objects.filter(FamilyId=self.FamilyId):
            if not (GroupId==None and PupilList==None):
                if self.PupilInGroup(FamilyPupils.Pupil.id):
                    self.FamilyPupils.append(MIS.modules.PupilRecs.PupilRecord(FamilyPupils.Pupil.id,self.AcYear))
            else:
                self.FamilyPupils.append(MIS.modules.PupilRecs.PupilRecord(FamilyPupils.Pupil.id,self.AcYear))
            
            

        if len(self.FamilyPupils)==1:
            self.Forenames=self.FamilyPupils[0].Pupil.FirstName()
            self.Surname = self.FamilyPupils[0].Pupil.Surname
            self.FullName = self.FamilyPupils[0].Pupil.FullName()
            self.DateOfBirth =self.FamilyPupils[0].Pupil.DateOfBirth
            self.ChildChildren='child'
            if self.FamilyPupils[0].Pupil.Gender=='M':
                self.HeSheThey='He'
                self.heshethey='he'
                self.HisHerTheir='His'
                self.hishertheir='his'
                self.himherthem = 'him'
            else:
                self.HeSheThey='She'
                self.heshethey='she'
                self.HisHerTheir='Her'
                self.hishertheir='her'
                self.himherthem='her'                   
        else:
            self.ChildChildren='children'
            self.HeSheThey='They'
            self.heshethey='they'
            self.HisHerTheir='Their'
            self.hishertheir='their' 
            self.himherthem='them'               
            self.Forenames=""
            Counter=self.Forenames.count(',and')
            for records in self.FamilyPupils:
                self.Forenames+=" ,and %s" % records.Pupil.FirstName() 
            self.Forenames=self.Forenames.replace(' ,and ','',1)
            while self.Forenames.count(' ,and') > 1:
                self.Forenames=self.Forenames.replace(' ,and ',',',1)
            if Counter > 2:
                self.Forenames=self.Forenames.replace(' ,and ',', and ')
            else:
                self.Forenames=self.Forenames.replace(' ,and ',' and ')
            
    def PupilInGroup(self,PupilId):
        ''' scans the PupilList and sees if a child from the selected family is in the
        selected Group'''
        if len(self.GroupList) > 0:
            for records in self.GroupList:
                if records.id==PupilId:
                    return True
            return False
        else:
            return True
    def FamilyContacts(self):
        if not self.FamilyRecs:
            for ContactRecs in FamilyContact.objects.filter(FamilyId=self.FamilyId).order_by('Priority'):
                Rec={'FamilyRec':ContactRecs,'Extended':ExtendedRecord(BaseType='Contact',
                                                                       BaseId=ContactRecs.Contact.id).ReadExtention(ExtentionName='ContactExtra')}
                self.FamilyRecs.append(copy.deepcopy(Rec))
        return self.FamilyRecs
            
    def GenerateLetterBody(self,LetterDetails,VarList,OtherItems,LetterType='Family'):
        t=template.Template(LetterDetails.BodyText.replace('&quot;','"'))
        if LetterType=='Family':
            c=template.Context({'Family': self,
                                'Var': VarList})
        else:
            c=template.Context({'Pupil': MIS.modules.PupilRecs.PupilRecord(self.PupilId, self.AcYear),
                                'Var': VarList,
                                'OtherItems': OtherItems})
        self.LetterBody=t.render(c)
        return   