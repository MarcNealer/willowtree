from MIS.models import *
from MIS.modules.MenuBuilder.MenuBuilder import *
from MIS.forms import *
import time, datetime, copy
from operator import itemgetter

#-------------------------------------------------------------------------------
# Name:        Clapham Menu Loader
# Purpose:
#
# Author:      DBMgr
#
# Created:     10/04/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def ClaphamMenu(Type='Teacher'):

    currentYear=int(datetime.date.today().year)
    AvailableClasses={
    'Reception':['RCE','RCN','RCS','RCW'],'Year1':['1CE','1CN','1CS','1CW'],
    'Year2':['2CE','2CN','2CS','2CW'],'Year3':['3CE','3CN','3CS','3CW'],
    'Year4':['4CE','4CN','4CS','4CW'],'Year5':['5CE','5CN','5CS','5CW'],
    'Year6':['6CE','6CN','6CS','6CW'],'Year7':['7CE','7CN','7CS','7CW'],
    'Year8':['8CE','8CN','8CS','8CW']}

    Menu_Actions_Form=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Attendance','Grade']
    Menu_Actions_Set=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Grade']
    Menu_Actions_Class=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Grade']
    Menu_Actions=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite']


    if Type=='Admin':
        Menu_Actions=['ManageThis'] + Menu_Actions
        ClaMenu= MenuBuilder(Type='New',MenuType='ClaphamAdmin',BaseGroup='Alumni')
        ClaMenu.SetLevel(0)
        ClaMenu.SetBaseGroup(None)
        ClaMenu.AddItem(GroupName='Alumni',MenuName='Alumni')
        ClaMenu.Up()

        ClaMenu.AddItem(GroupName='Alumni.Clapham',MenuName='All',
                        SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Alumni.Clapham.AskedToLeave',MenuName='Asked To Leave',
                        SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Alumni.Clapham.FormerPupils',MenuName='Former Pupils',
                        SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
        ClaMenu.SetLevel(0)
        ClaMenu.SetBaseGroup('Current.Clapham')
    else:
        ClaMenu= MenuBuilder(Type='New',MenuType='ClaphamTeacher',BaseGroup='Current.Clapham')




    # Lower school
    ClaMenu.AddItem(GroupName='LowerSchool',MenuName='Lower School',menuActions=Menu_Actions)
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions)

    ClaMenu.Up()

    # Reception

    ClaMenu.SetBaseGroup('Current.Clapham.LowerSchool.Reception')
    ClaMenu.CreateGroupOnly(GroupName='Current.Clapham.LowerSchool.Reception.Form')

    ClaMenu.AddItem(GroupName='Form.RCN',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Form.RCE',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Form.RCS',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Form.RCW',menuActions=Menu_Actions_Form)


    ClaMenu.SetLevel(1)

    # Year 1

    ClaMenu.SetBaseGroup('Current.Clapham.LowerSchool')

    ClaMenu.AddItem(GroupName='Year1',MenuName='Year 1',menuActions=Menu_Actions)
    ClaMenu.CreateGroupOnly('Current.Clapham.LowerSchool.Year1.Form')

    # 1BN
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year1.Form.1CN',menuActions=Menu_Actions_Form)
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year1.Form.1CN.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year1.Form.1CN.BalGames.Ballet',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year1.Form.1CN.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.Down()

    # 1BE

    ClaMenu.AddItem(GroupName='Year1.Form.1CE',menuActions=Menu_Actions_Form)
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year1.Form.1CE.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year1.Form.1CE.BalGames.Ballet',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year1.Form.1CE.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.Down()

    # 1BS

    ClaMenu.AddItem(GroupName='Year1.Form.1CS',menuActions=Menu_Actions_Form)
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year1.Form.1CS.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year1.Form.1CS.BalGames.Ballet',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year1.Form.1CS.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.Down()

    # 1BW

    ClaMenu.AddItem(GroupName='Year1.Form.1CW',menuActions=Menu_Actions_Form)
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year1.Form.1CW.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year1.Form.1CW.BalGames.Ballet',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year1.Form.1CW.BalGames.BoysGames',menuActions=Menu_Actions_Class)


    # Year 2

    ClaMenu.SetLevel(1)

    ClaMenu.AddItem(GroupName='Year2',MenuName='Year 2',menuActions=Menu_Actions)
    ClaMenu.CreateGroupOnly('Current.Clapham.LowerSchool.Year2.Form')

    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year2.Games',ManagedGroups=['Boys','Girls'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year2.Games.Boys',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year2.Games.Girls',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='Year2.Form.2CN',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year2.Form.2CE',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year2.Form.2CS',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year2.Form.2CW',menuActions=Menu_Actions_Form)

    # MiddleSchool

    ClaMenu.SetLevel(0)
    ClaMenu.SetBaseGroup('Current.Clapham')

    ClaMenu.AddItem(GroupName='MiddleSchool',MenuName='Middle School',menuActions=Menu_Actions)
    ClaMenu.SetBaseGroup('Current.Clapham.MiddleSchool')
    ClaMenu.Up()


    # Year 3

    ClaMenu.AddItem(GroupName='Year3',MenuName='Year 3',menuActions=Menu_Actions)
    ClaMenu.CreateGroupOnly('Current.Clapham.MiddleSchool.Year3.Form')
    ClaMenu.Up()

    # Year 3 Games
    ClaMenu.AddItem(GroupName='Year3.Games',ManagedGroups=['Boys','Girls'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year3.Games.Boys',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year3.Games.Girls',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 3 Mathematics
    ClaMenu.AddItem(GroupName='Year3.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year3.Mathematics.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year3.Mathematics.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year3.Mathematics.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year3.Mathematics.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 3 Spelling
    ClaMenu.AddItem(GroupName='Year3.English',ManagedGroups=['SetA','SetB','SetC','SetD'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year3.English.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year3.English.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year3.English.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year3.English.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 3 Spelling
    ClaMenu.AddItem(GroupName='Year3.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year3.Spelling.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year3.Spelling.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year3.Spelling.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year3.Spelling.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    ClaMenu.AddItem(GroupName='Year3.Form.3CN',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year3.Form.3CE',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year3.Form.3CS',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year3.Form.3CW',menuActions=Menu_Actions_Form)


    ClaMenu.SetLevel(1)

    # Year 4

    ClaMenu.AddItem(GroupName='Year4',MenuName='Year 4',menuActions=Menu_Actions)
    ClaMenu.CreateGroupOnly('Current.Clapham.MiddleSchool.Year4.Form')
    ClaMenu.Up()

    # Year 4 Games
    ClaMenu.AddItem(GroupName='Year4.Games',ManagedGroups=['Boys','Girls'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year4.Games.Boys',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year4.Games.Girls',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 4 Mathematics
    ClaMenu.AddItem(GroupName='Year4.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year4.Mathematics.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year4.Mathematics.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year4.Mathematics.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year4.Mathematics.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 4 English
    ClaMenu.AddItem(GroupName='Year4.English',ManagedGroups=['SetA','SetB','SetC','SetD'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year4.English.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year4.English.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year4.English.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year4.English.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 4 Spelling
    ClaMenu.AddItem(GroupName='Year4.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year4.Spelling.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year4.Spelling.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year4.Spelling.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year4.Spelling.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    ClaMenu.AddItem(GroupName='Year4.Form.4CN',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year4.Form.4CE',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year4.Form.4CS',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year4.Form.4CW',menuActions=Menu_Actions_Form)


    ClaMenu.SetLevel(1)

 # Year 5

    ClaMenu.AddItem(GroupName='Year5',MenuName='Year 5',menuActions=Menu_Actions)
    ClaMenu.CreateGroupOnly('Current.Clapham.MiddleSchool.Year5.Form')
    ClaMenu.Up()

    # Year 5 Games
    ClaMenu.AddItem(GroupName='Year5.Games',ManagedGroups=['Boys','Girls'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year5.Games.Boys',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Games.Girls',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 5 Mathematics
    ClaMenu.AddItem(GroupName='Year5.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD','SetE'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year5.Mathematics.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Mathematics.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Mathematics.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Mathematics.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Mathematics.SetE',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 5 English
    ClaMenu.AddItem(GroupName='Year5.English',ManagedGroups=['SetA','SetB','SetC','SetD','SetE'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year5.English.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.English.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.English.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.English.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.English.SetE',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 5 Spelling
    ClaMenu.AddItem(GroupName='Year5.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year5.Spelling.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Spelling.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Spelling.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Spelling.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 5 Spelling
    ClaMenu.AddItem(GroupName='Year5.Science',ManagedGroups=['SetA','SetB','SetC','SetD'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year5.Science.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Science.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Science.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year5.Science.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    #Year 5 Form classes

    ClaMenu.AddItem(GroupName='Year5.Form.5CN',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year5.Form.5CE',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year5.Form.5CS',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year5.Form.5CW',menuActions=Menu_Actions_Form)


    ClaMenu.SetLevel(1)

    # Upper School

    ClaMenu.SetLevel(0)
    ClaMenu.SetBaseGroup('Current.Clapham')

    ClaMenu.AddItem(GroupName='UpperSchool',MenuName='Upper School',menuActions=Menu_Actions)
    ClaMenu.SetBaseGroup('Current.Clapham.UpperSchool')
    ClaMenu.Up()

    # Year 6

    ClaMenu.AddItem(GroupName='Year6',MenuName='Year 6',menuActions=Menu_Actions)
    ClaMenu.Up()

    # Year 6 Games
    ClaMenu.AddItem(GroupName='Year6.Games',ManagedGroups=['Boys','Girls'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year6.Games.Boys',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year6.Games.Girls',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 6 Sets

    ClaMenu.AddItem(GroupName='Year6.Yr6_Sets',MenuName='Yr6 Sets',ManagedGroups=['11Plus','13Plus'])
    ClaMenu.Up()
    ClaMenu.SetBaseGroup('Current.Clapham.UpperSchool.Year6.Yr6_Sets')

    # 11 Plus Sets

    ClaMenu.AddItem(GroupName='11Plus',SubMenus=['Lists'],menuActions=Menu_Actions)
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='11Plus.Mathematics',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='11Plus.Mathematics.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.Mathematics.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.Mathematics.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='11Plus.English',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='11Plus.English.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.English.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.English.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='11Plus.Science',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='11Plus.Science.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.Science.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.Science.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='11Plus.French',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='11Plus.French.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.French.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.French.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='11Plus.Geography',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='11Plus.Geography.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.Geography.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.Geography.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='11Plus.RE',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='11Plus.RE.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.RE.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.RE.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='11Plus.History',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='11Plus.History.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.History.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.History.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='11Plus.Latin',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='11Plus.Latin.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.Latin.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='11Plus.Latin.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.Down()

    # 13 Plus Sets

    ClaMenu.AddItem(GroupName='13Plus',SubMenus=['Lists'],menuActions=Menu_Actions)
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='13Plus.Mathematics',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='13Plus.Mathematics.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.Mathematics.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.Mathematics.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='13Plus.English',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='13Plus.English.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.English.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.English.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='13Plus.Science',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='13Plus.Science.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.Science.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.Science.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='13Plus.French',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='13Plus.French.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.French.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.French.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='13Plus.Geography',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='13Plus.Geography.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.Geography.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.Geography.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='13Plus.RE',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='13Plus.RE.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.RE.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.RE.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='13Plus.History',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='13Plus.History.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.History.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.History.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.AddItem(GroupName='13Plus.Latin',ManagedGroups=['SetA','SetB','SetC'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='13Plus.Latin.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.Latin.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='13Plus.Latin.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    ClaMenu.Down()
    ClaMenu.Down()

    #Year 6 Form classes
    ClaMenu.CreateGroupOnly('Current.Clapham.UpperSchool.Year6.Form')
    ClaMenu.SetBaseGroup('Current.Clapham.UpperSchool')

    ClaMenu.AddItem(GroupName='Year6.Form.6CN',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year6.Form.6CE',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year6.Form.6CS',menuActions=Menu_Actions_Form)
#    ClaMenu.AddItem(GroupName='Year6.Form.6CW',menuActions=Menu_Actions_Form)

    ClaMenu.Down()

    # Year 7

    ClaMenu.AddItem(GroupName='Year7',MenuName='Year 7',SubMenus=['Lists'],menuActions=Menu_Actions)
    ClaMenu.CreateGroupOnly('Current.Clapham.UpperSchool.Year7.Form')
    ClaMenu.Up()

    # Year 7 Games
    ClaMenu.AddItem(GroupName='Year7.Games',ManagedGroups=['Boys','Girls'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.Games.Boys',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Games.Girls',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 7 Mathematics
    ClaMenu.AddItem(GroupName='Year7.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.Mathematics.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Mathematics.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Mathematics.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Mathematics.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Mathematics.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 7 English
    ClaMenu.AddItem(GroupName='Year7.English',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.English.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.English.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.English.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.English.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.English.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()


    # Year 7 Science
    ClaMenu.AddItem(GroupName='Year7.Science',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.Science.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Science.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Science.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Science.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Science.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 7 French
    ClaMenu.AddItem(GroupName='Year7.French',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.French.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.French.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.French.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.French.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.French.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 7 Geography
    ClaMenu.AddItem(GroupName='Year7.Geography',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.Geography.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Geography.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Geography.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Geography.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Geography.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 7 History
    ClaMenu.AddItem(GroupName='Year7.History',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.History.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.History.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.History.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.History.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.History.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 7 RE
    ClaMenu.AddItem(GroupName='Year7.RE',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.RE.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.RE.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.RE.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.RE.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.RE.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 7 Latin
    ClaMenu.AddItem(GroupName='Year7.Latin',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.Latin.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Latin.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Latin.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Latin.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Latin.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 7 Latin
    ClaMenu.AddItem(GroupName='Year7.Greek',ManagedGroups=['SetA','SetD'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.Greek.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Greek.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 7 Art
    ClaMenu.AddItem(GroupName='Year7.Art',ManagedGroups=['SetA','SetB','SetC','SetD','SetE'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.Art.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Art.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Art.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Art.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.Art.SetE',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 7 ICT
    ClaMenu.AddItem(GroupName='Year7.ICT',ManagedGroups=['SetA','SetB','SetC','SetD','SetE'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.ICT.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.ICT.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.ICT.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.ICT.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.ICT.SetE',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    ClaMenu.AddItem(GroupName='Year7.RE',ManagedGroups=['SetE'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.RE.SetE',menuActions=Menu_Actions_Class)
    ClaMenu.Down()


    # Year 7 TutorGroups
    ClaMenu.AddItem(GroupName='Year7.TutorGroups',ManagedGroups=['SetA','SetB','SetC','SetD','SetE','SetF','SetG','SetH','SetI','SetJ'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetE',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetF',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetG',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetH',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetI',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetJ',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetK',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetL',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetM',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetN',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetO',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetP',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetQ',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetR',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year7.TutorGroups.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    #Year 7 Form classes

    ClaMenu.AddItem(GroupName='Year7.Form.7CN',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year7.Form.7CE',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year7.Form.7CS',menuActions=Menu_Actions_Form)
#    ClaMenu.AddItem(GroupName='Year7.Form.7CW',menuActions=Menu_Actions_Form)


    ClaMenu.SetLevel(1)


    # Year 8

    ClaMenu.AddItem(GroupName='Year8',MenuName='Year 8',SubMenus=['Lists'],menuActions=Menu_Actions)
    ClaMenu.CreateGroupOnly('Current.Clapham.UpperSchool.Year8.Form')
    ClaMenu.Up()

    # Year 8 Games
    ClaMenu.AddItem(GroupName='Year8.Games',ManagedGroups=['Boys','Girls'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.Games.Boys',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Games.Girls',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 8 Mathematics
    ClaMenu.AddItem(GroupName='Year8.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.Mathematics.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Mathematics.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Mathematics.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Mathematics.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Mathematics.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 8 English
    ClaMenu.AddItem(GroupName='Year8.English',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.English.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.English.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.English.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.English.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.English.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()


    # Year 8 Science
    ClaMenu.AddItem(GroupName='Year8.Science',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.Science.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Science.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Science.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Science.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Science.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()
    # Year 8 French
    ClaMenu.AddItem(GroupName='Year8.French',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.French.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.French.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.French.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.French.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.French.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 8 Geography
    ClaMenu.AddItem(GroupName='Year8.Geography',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.Geography.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Geography.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Geography.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Geography.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Geography.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 8 Geography
    ClaMenu.AddItem(GroupName='Year8.History',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.History.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.History.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.History.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.History.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.History.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 8 RE
    ClaMenu.AddItem(GroupName='Year8.RE',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.RE.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.RE.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.RE.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.RE.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.RE.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 8 Latin
    ClaMenu.AddItem(GroupName='Year8.Latin',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.Latin.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Latin.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Latin.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Latin.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Latin.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 8 Greek
    ClaMenu.AddItem(GroupName='Year8.Greek',ManagedGroups=['SetA','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.Greek.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Greek.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 8 Art
    ClaMenu.AddItem(GroupName='Year8.Art',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.Art.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Art.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Art.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Art.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.Art.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 8 ICT
    ClaMenu.AddItem(GroupName='Year8.ICT',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.ICT.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.ICT.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.ICT.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.ICT.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.ICT.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    # Year 8 TutorGroups
    ClaMenu.AddItem(GroupName='Year8.TutorGroups',ManagedGroups=['SetA','SetB','SetC','SetD','SetE','SetF','SetG','SetH','SetI','SetJ'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetA',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetB',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetC',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetD',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetE',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetF',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetG',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetH',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetI',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetJ',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetK',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetL',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetM',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetN',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetO',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetP',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetQ',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetR',menuActions=Menu_Actions_Class)
    ClaMenu.AddItem(GroupName='Year8.TutorGroups.SetS',menuActions=Menu_Actions_Class)
    ClaMenu.Down()

    #Year 8 Form classes

    ClaMenu.AddItem(GroupName='Year8.Form.8CN',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year8.Form.8CE',menuActions=Menu_Actions_Form)
    ClaMenu.AddItem(GroupName='Year8.Form.8CS',menuActions=Menu_Actions_Form)
#    ClaMenu.AddItem(GroupName='Year8.Form.8CW',menuActions=Menu_Actions_Form)


    ClaMenu.SetLevel(0)


    # Academic Houses
    ClaMenu.SetBaseGroup('Current.Clapham')
    ClaMenu.AddItem(GroupName='House',MenuName='House')
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='House.House',ManagedGroups=['Becket','Hardy','Lawrence','More'])
    ClaMenu.Up()
    ClaMenu.AddItem(GroupName='House.House.Becket',menuActions=Menu_Actions)
    ClaMenu.AddItem(GroupName='House.House.Hardy',menuActions=Menu_Actions)
    ClaMenu.AddItem(GroupName='House.House.Lawrence',menuActions=Menu_Actions)
    ClaMenu.AddItem(GroupName='House.House.More',menuActions=Menu_Actions)
    ClaMenu.SetLevel(0)
    if Type=='Admin':

        #  Staff Groups and Menu Items

        ClaMenu.CreateGroupOnly(GroupName='Staff')
        ClaMenu.SetBaseGroup('Staff')
        ClaMenu.AddItem(GroupName='Clapham',MenuName='Staff')
        ClaMenu.SetBaseGroup('Staff.Clapham')
        ClaMenu.Up()
        ClaMenu.AddItem(GroupName='LowerSchool' ,MenuName='Lower School',menuActions=Menu_Actions)
        ClaMenu.Up()
        ClaMenu.AddItem(GroupName='LowerSchool.Head',MenuName='LS Head',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='LowerSchool.Form',MenuName='LS Form',menuActions=Menu_Actions)

        ClaMenu.Up()
        for classes in AvailableClasses['Reception']:
            ClaMenu.AddItem(GroupName='LowerSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year1']:
            ClaMenu.AddItem(GroupName='LowerSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year2']:
            ClaMenu.AddItem(GroupName='LowerSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        ClaMenu.Down()

        ClaMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions)

        ClaMenu.AddItem(GroupName='LowerSchool.Year1',MenuName='Year 1',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='LowerSchool.Year2',MenuName='Year 2',menuActions=Menu_Actions)
        ClaMenu.Down()
        ClaMenu.AddItem(GroupName='MiddleSchool' ,MenuName='Middle School',menuActions=Menu_Actions)
        ClaMenu.Up()
        ClaMenu.AddItem(GroupName='MiddleSchool.Head',MenuName='MS Head',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='MiddleSchool.Form',MenuName='MS Form',menuActions=Menu_Actions)

        ClaMenu.Up()
        for classes in AvailableClasses['Year3']:
            ClaMenu.AddItem(GroupName='MiddleSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year4']:
            ClaMenu.AddItem(GroupName='MiddleSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year5']:
            ClaMenu.AddItem(GroupName='MiddleSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        ClaMenu.Down()

        ClaMenu.AddItem(GroupName='MiddleSchool.Year3',MenuName='Year 3',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='MiddleSchool.Year4',MenuName='Year 4',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='MiddleSchool.Year5',MenuName='Year 5',menuActions=Menu_Actions)
        ClaMenu.Down()

        ClaMenu.AddItem(GroupName='UpperSchool' ,MenuName='Upper School',menuActions=Menu_Actions)
        ClaMenu.Up()

        ClaMenu.AddItem(GroupName='UpperSchool.Head',MenuName='US Head',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='UpperSchool.Form',MenuName='US Form',menuActions=Menu_Actions)

        ClaMenu.Up()
        for classes in AvailableClasses['Year6']:
            ClaMenu.AddItem(GroupName='UpperSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year7']:
            ClaMenu.AddItem(GroupName='UpperSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year8']:
            ClaMenu.AddItem(GroupName='UpperSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        ClaMenu.Down()

        ClaMenu.AddItem(GroupName='UpperSchool.Year6',MenuName='Year 6',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='UpperSchool.Year7',MenuName='Year 7',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='UpperSchool.Year8',MenuName='Year 8',menuActions=Menu_Actions)
        ClaMenu.Down()

        ClaMenu.AddItem(GroupName='Department' ,MenuName='Departments')
        ClaMenu.Up()
        ClaMenu.AddItem(GroupName='Department.HeadTeacher',MenuName='Head Teacher',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Department.SysAdmin',MenuName='SysAdmin',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Department.OfficeAdmin',MenuName='Office Admin',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Department.Transport',MenuName='Transport',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Department.Maintainance',MenuName='Maintinance',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Department.Catering',MenuName='Catering',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Department.CurrHODS',MenuName="Curr-HoD's",menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Department.CoHODS',MenuName="Co-Curr HoD's",menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Department.SLT',MenuName="SLT",menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Department.PE',MenuName='PE',menuActions=Menu_Actions)
        ClaMenu.AddItem(GroupName='Department.SEN',MenuName='Special Needs',menuActions=Menu_Actions)

        ClaMenu.Down()