from MIS.models import *
from MIS.modules.MenuBuilder.MenuBuilder import *
from MIS.forms import *
import time, datetime, copy
from operator import itemgetter

#-------------------------------------------------------------------------------
# Name:        Kensington Menu Loader
# Purpose:
#
# Author:      DBMgr
#
# Created:     10/04/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def KensingtonMenu(Type='Teacher'):

    currentYear=int(datetime.date.today().year)
    AvailableClasses={
    'Reception':['RKE','RKN','RKS'],'Year1':['1KE','1KN','1KS'],
    'Year2':['2KE','2KN','2KS'],'Year3':['3KE','3KN','3KS'],
    'Year4':['4KE','4KN','4KS'],'Year5':['5KE','5KN','5KS'],'Year6':['6KE','6KN','6KS']}

    Menu_Actions_Form=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Attendance','Grade']
    Menu_Actions_Set=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Grade']
    Menu_Actions_Class=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Grade']
    Menu_Actions=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite']


    if Type=='Admin':
        Menu_Actions.append('ManageThis')
        KenMenu= MenuBuilder(Type='New',MenuType='KensingtonAdmin',BaseGroup='Alumni')
        KenMenu.SetLevel(0)
        KenMenu.SetBaseGroup(None)
        KenMenu.AddItem(GroupName='Alumni',MenuName='Alumni')
        KenMenu.Up()

        KenMenu.AddItem(GroupName='Alumni.Kensington',MenuName='All',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Alumni.Kensington.AskedToLeave',MenuName='Asked To Leave',menuActions=Menu_Actions)
        KenMenu.SetLevel(0)
        KenMenu.SetBaseGroup('Current.Kensington')
    else:
        KenMenu= MenuBuilder(Type='New',MenuType='KensingtonTeacher',BaseGroup='Current.Kensington')




    # Lower school
    KenMenu.AddItem(GroupName='LowerSchool',MenuName='Lower School',menuActions=Menu_Actions)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions)

    KenMenu.Up()

    # Reception

    KenMenu.SetBaseGroup('Current.Kensington.LowerSchool.Reception')
    KenMenu.CreateGroupOnly(GroupName='Current.Kensington.LowerSchool.Reception.Form')

    KenMenu.AddItem(GroupName='Form.RKN',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Form.RKE',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Form.RKS',menuActions=Menu_Actions_Form)


    KenMenu.SetLevel(1)

    # Year 1

    KenMenu.SetBaseGroup('Current.Kensington.LowerSchool')

    KenMenu.AddItem(GroupName='Year1',MenuName='Year 1',menuActions=Menu_Actions)
    KenMenu.CreateGroupOnly('Current.Kensington.LowerSchool.Year1.Form')

    # 1BN
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KN',menuActions=Menu_Actions_Form)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KN.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KN.BalGames.Ballet',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year1.Form.1KN.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    KenMenu.Down()

    # 1BE

    KenMenu.AddItem(GroupName='Year1.Form.1KE',menuActions=Menu_Actions_Form)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KE.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KE.BalGames.Ballet',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year1.Form.1KE.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    KenMenu.Down()

    # 1BS

    KenMenu.AddItem(GroupName='Year1.Form.1KS',menuActions=Menu_Actions_Form)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KS.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KS.BalGames.Ballet',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year1.Form.1KS.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    KenMenu.Down()


    # Year 2

    KenMenu.SetLevel(1)

    KenMenu.AddItem(GroupName='Year2',MenuName='Year 2',menuActions=Menu_Actions)
    KenMenu.CreateGroupOnly('Current.Kensington.LowerSchool.Year2.Form')

    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year2.Games',ManagedGroups=['Boys','Girls'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year2.Games.Boys',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year2.Games.Girls',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='Year2.Form.2KN',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Year2.Form.2KE',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Year2.Form.2KS',menuActions=Menu_Actions_Form)

    # PrepSchool

    KenMenu.SetLevel(0)
    KenMenu.SetBaseGroup('Current.Kensington')

    KenMenu.AddItem(GroupName='PrepSchool',MenuName='Prep School',menuActions=Menu_Actions)
    KenMenu.SetBaseGroup('Current.Kensington.PrepSchool')
    KenMenu.Up()


    # Year 3

    KenMenu.AddItem(GroupName='Year3',MenuName='Year 3',menuActions=Menu_Actions)
    KenMenu.CreateGroupOnly('Current.Kensington.PrepSchool.Year3.Form')
    KenMenu.Up()

    # Year 3 Games
    KenMenu.AddItem(GroupName='Year3.Games',ManagedGroups=['Boys','Girls'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year3.Games.Boys',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year3.Games.Girls',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    # Year 3 Mathematics
    KenMenu.AddItem(GroupName='Year3.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year3.Mathematics.SetA',menuActions=Menu_Actions_Class,ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year3.Mathematics.SetB',menuActions=Menu_Actions_Class,ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year3.Mathematics.SetC',menuActions=Menu_Actions_Class,ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year3.Mathematics.SetD',menuActions=Menu_Actions_Class,ParentSubjects=False)
    KenMenu.Down()
    # Year 3 Spelling
    KenMenu.AddItem(GroupName='Year3.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year3.Spelling.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year3.Spelling.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year3.Spelling.SetC',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year3.Spelling.SetD',menuActions=Menu_Actions_Class)
    KenMenu.Down()

    KenMenu.AddItem(GroupName='Year3.Form.3KN',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Year3.Form.3KE',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Year3.Form.3KS',menuActions=Menu_Actions_Form)


    KenMenu.SetLevel(1)

    # Year 4

    KenMenu.AddItem(GroupName='Year4',MenuName='Year 4',menuActions=Menu_Actions)
    KenMenu.CreateGroupOnly('Current.Kensington.PrepSchool.Year4.Form')
    KenMenu.Up()

    # Year 4 Games
    KenMenu.AddItem(GroupName='Year4.Games',ManagedGroups=['Boys','Girls'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year4.Games.Boys',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year4.Games.Girls',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    # Year 4 Mathematics
    KenMenu.AddItem(GroupName='Year4.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year4.Mathematics.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year4.Mathematics.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year4.Mathematics.SetC',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year4.Mathematics.SetD',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    # Year 4 Spelling
    KenMenu.AddItem(GroupName='Year4.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year4.Spelling.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year4.Spelling.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year4.Spelling.SetC',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year4.Spelling.SetD',menuActions=Menu_Actions_Class)
    KenMenu.Down()

    KenMenu.AddItem(GroupName='Year4.Form.4KN',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Year4.Form.4KE',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Year4.Form.4KS',menuActions=Menu_Actions_Form)


    KenMenu.SetLevel(1)

 # Year 5

    KenMenu.AddItem(GroupName='Year5',MenuName='Year 5',menuActions=Menu_Actions)
    KenMenu.CreateGroupOnly('Current.Kensington.PrepSchool.Year5.Form')
    KenMenu.Up()

    # Year 5 Games
    KenMenu.AddItem(GroupName='Year5.Games',ManagedGroups=['Boys','Girls'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year5.Games.Boys',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year5.Games.Girls',menuActions=Menu_Actions_Class)
    KenMenu.Down()

    # Year 5 Mathematics
    KenMenu.AddItem(GroupName='Year5.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year5.Mathematics.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year5.Mathematics.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year5.Mathematics.SetC',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year5.Mathematics.SetD',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    # Year 5 Spelling
    KenMenu.AddItem(GroupName='Year5.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year5.Spelling.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year5.Spelling.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year5.Spelling.SetC',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Year5.Spelling.SetD',menuActions=Menu_Actions_Class)
    KenMenu.Down()

    #Year 5 Form classes

    KenMenu.AddItem(GroupName='Year5.Form.5KN',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Year5.Form.5KE',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Year5.Form.5KS',menuActions=Menu_Actions_Form)

    KenMenu.SetLevel(1)



    KenMenu.AddItem(GroupName='Year6',MenuName='Year 6',menuActions=Menu_Actions)
    KenMenu.Up()

    # Year 6 Sets

    KenMenu.SetBaseGroup('Current.Kensington.PrepSchool.Year6')

    # Year 6 Games
    KenMenu.AddItem(GroupName='Games',ManagedGroups=['Boys','Girls'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Games.Boys',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Games.Girls',menuActions=Menu_Actions_Class)
    KenMenu.Down()

    # 11 Plus Sets

    KenMenu.AddItem(GroupName='Mathematics',ManagedGroups=['SetA','SetB','SetC', 'SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Mathematics.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Mathematics.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Mathematics.SetC',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Mathematics.SetD',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='English',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='English.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='English.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='English.SetC',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='Science',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Science.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Science.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Science.SetC',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='French',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='French.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='French.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='French.SetC',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='Geography',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Geography.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Geography.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Geography.SetC',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='History',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='History.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='History.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='History.SetC',menuActions=Menu_Actions_Class)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='Latin',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Latin.SetA',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Latin.SetB',menuActions=Menu_Actions_Class)
    KenMenu.AddItem(GroupName='Latin.SetC',menuActions=Menu_Actions_Class)
    KenMenu.Down()

    #Year 6 Form classes
    KenMenu.CreateGroupOnly('Current.Kensington.PrepSchool.Year6.Form')
    KenMenu.SetBaseGroup('Current.Kensington.PrepSchool')

    KenMenu.AddItem(GroupName='Year6.Form.6KN',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Year6.Form.6KE',menuActions=Menu_Actions_Form)
    KenMenu.AddItem(GroupName='Year6.Form.6KS',menuActions=Menu_Actions_Form)

    KenMenu.SetLevel(0)

    # Academic Houses

    KenMenu.SetBaseGroup('Current.Kensington')
    KenMenu.AddItem(GroupName='House',MenuName='House')
    KenMenu.Up()
    KenMenu.AddItem(GroupName='House.House',ManagedGroups=['Becket','Hardy','Lawrence','More'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='House.House.Becket',menuActions=Menu_Actions)
    KenMenu.AddItem(GroupName='House.House.Hardy',menuActions=Menu_Actions)
    KenMenu.AddItem(GroupName='House.House.Lawrence',menuActions=Menu_Actions)
    KenMenu.AddItem(GroupName='House.House.More',menuActions=Menu_Actions)
    KenMenu.SetLevel(0)
    #  Staff Groups and Menu Items

    if Type=='Admin':
        KenMenu.CreateGroupOnly(GroupName='Staff')
        KenMenu.SetBaseGroup('Staff')
        KenMenu.AddItem(GroupName='Kensington',MenuName='Staff')
        KenMenu.SetBaseGroup('Staff.Kensington')
        KenMenu.Up()
        KenMenu.AddItem(GroupName='LowerSchool' ,MenuName='Lower School',menuActions=Menu_Actions)
        KenMenu.Up()
        KenMenu.AddItem(GroupName='LowerSchool.Head',MenuName='LS Head',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='LowerSchool.Form',MenuName='LS Form',menuActions=Menu_Actions)

        KenMenu.Up()
        for classes in AvailableClasses['Reception']:
            KenMenu.AddItem(GroupName='LowerSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year1']:
            KenMenu.AddItem(GroupName='LowerSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year2']:
            KenMenu.AddItem(GroupName='LowerSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        KenMenu.Down()

        KenMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions)

        KenMenu.AddItem(GroupName='LowerSchool.Year1',MenuName='Year 1',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='LowerSchool.Year2',MenuName='Year 2',menuActions=Menu_Actions)
        KenMenu.Down()
        KenMenu.AddItem(GroupName='MiddleSchool' ,MenuName='Prep School',menuActions=Menu_Actions)
        KenMenu.Up()
        KenMenu.AddItem(GroupName='MiddleSchool.Head',MenuName='MS Head',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='MiddleSchool.Form',MenuName='MS Form',menuActions=Menu_Actions)

        KenMenu.Up()
        for classes in AvailableClasses['Year3']:
            KenMenu.AddItem(GroupName='MiddleSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year4']:
            KenMenu.AddItem(GroupName='MiddleSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year5']:
            KenMenu.AddItem(GroupName='MiddleSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        KenMenu.Down()

        KenMenu.AddItem(GroupName='MiddleSchool.Year3',MenuName='Year 3',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='MiddleSchool.Year4',MenuName='Year 4',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='MiddleSchool.Year5',MenuName='Year 5',menuActions=Menu_Actions)
        KenMenu.Down()

        KenMenu.AddItem(GroupName='UpperSchool' ,MenuName='Upper School',menuActions=Menu_Actions)
        KenMenu.Up()
        KenMenu.AddItem(GroupName='UpperSchool.Head',MenuName='US Head',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='UpperSchool.Form',MenuName='US Form',menuActions=Menu_Actions)
        for classes in AvailableClasses['Year6']:
            KenMenu.AddItem(GroupName='UpperSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        KenMenu.Down()
        KenMenu.AddItem(GroupName='UpperSchool.Year6',MenuName='Year 6',menuActions=Menu_Actions)
        KenMenu.Down()

        KenMenu.AddItem(GroupName='Department' ,MenuName='Departments')
        KenMenu.Up()
        KenMenu.AddItem(GroupName='Department.HeadTecher',MenuName='HeadTeacher',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Department.SysAdmin',MenuName='SysAdmin',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Department.OfficeAdmin',MenuName='Office Admin',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Department.Transport',MenuName='Transport',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Department.Maintainance',MenuName='Maintainance',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Department.Catering',MenuName='Catering',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Department.CurrHODS',MenuName="Curr-HoD's",menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Department.CoHODS',MenuName="Co-Curr HoD's",menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Department.SLT',MenuName="SLT",menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Department.PE',MenuName='PE',menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Department.SEN',MenuName='Special Needs',menuActions=Menu_Actions)

        KenMenu.Down()
