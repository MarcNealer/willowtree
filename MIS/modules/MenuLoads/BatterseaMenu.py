from MIS.models import *
from MIS.modules.MenuBuilder.MenuBuilder import *
from MIS.forms import *
import time, datetime, copy
from operator import itemgetter

#-------------------------------------------------------------------------------
# Name:        Battersea Menu Loader
# Purpose:
#
# Author:      DBMgr
#
# Created:     10/04/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def BatterseaMenu(Type='Teacher'):

    currentYear=int(datetime.date.today().year)
    AvailableClasses={
    'Reception':['RBE','RBN','RBS','RBW'],'Year1':['1BE','1BN','1BS','1BW'],
    'Year2':['2BE','2BN','2BS','2BW'],'Year3':['3BE','3BN','3BS','3BW'],
    'Year4':['4BE','4BN','4BS','4BW'],'Year5':['5BE','5BN','5BS','5BW'],
    'Year6':['6BE','6BN','6BS','6BW'],'Year7':['7BE','7BN','7BS','7BW']}


    Menu_Actions_Form=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Attendance','Grade']
    Menu_Actions_Set=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Grade']
    Menu_Actions_Class=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Grade']
    Menu_Actions=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite']


    if Type=='Admin':
        Menu_Actions=['ManageThis'] + Menu_Actions
        BatMenu= MenuBuilder(Type='New',MenuType='BatterseaAdmin',BaseGroup='Alumni')
        BatMenu.SetLevel(0)
        BatMenu.SetBaseGroup(None)
        BatMenu.AddItem(GroupName='Alumni',MenuName='Alumni')
        BatMenu.Up()

        BatMenu.AddItem(GroupName='Alumni.Battersea',MenuName='All',
                        SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Alumni.Battersea.AskedToLeave',MenuName='Asked To Leave',
                        SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
        BatMenu.SetLevel(0)
        BatMenu.SetBaseGroup('Current.Battersea')
    else:
        BatMenu= MenuBuilder(Type='New',MenuType='BatterseaTeacher',BaseGroup='Current.Battersea')




    # Lower school
    BatMenu.AddItem(GroupName='LowerSchool',MenuName='Lower School',menuActions=Menu_Actions)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions)

    BatMenu.Up()

    # Reception

    BatMenu.SetBaseGroup('Current.Battersea.LowerSchool.Reception')
    BatMenu.CreateGroupOnly(GroupName='Current.Battersea.LowerSchool.Reception.Form')

    BatMenu.AddItem(GroupName='Form.RBN',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Form.RBE',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Form.RBS',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Form.RBW',menuActions=Menu_Actions_Form)


    BatMenu.SetLevel(1)

    # Year 1

    BatMenu.SetBaseGroup('Current.Battersea.LowerSchool')

    BatMenu.AddItem(GroupName='Year1',MenuName='Year 1',menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.LowerSchool.Year1.Form')

    # 1BN
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BN',menuActions=Menu_Actions_Form)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BN.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BN.BalGames.Ballet',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year1.Form.1BN.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.Down()

    # 1BE

    BatMenu.AddItem(GroupName='Year1.Form.1BE',menuActions=Menu_Actions_Form)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BE.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BE.BalGames.Ballet',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year1.Form.1BE.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.Down()

    # 1BS

    BatMenu.AddItem(GroupName='Year1.Form.1BS',menuActions=Menu_Actions_Form)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BS.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BS.BalGames.Ballet',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year1.Form.1BS.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.Down()

#    # 1BW
#
 #   BatMenu.AddItem(GroupName='Year1.Form.1BW',menuActions=Menu_Actions_Form)
  #  BatMenu.Up()
   # BatMenu.AddItem(GroupName='Year1.Form.1BW.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    #BatMenu.Up()
#    BatMenu.AddItem(GroupName='Year1.Form.1BW.BalGames.Ballet',menuActions=Menu_Actions_Class)
#    BatMenu.AddItem(GroupName='Year1.Form.1BW.BalGames.BoysGames',menuActions=Menu_Actions_Class)


    # Year 2

    BatMenu.SetLevel(1)

    BatMenu.AddItem(GroupName='Year2',MenuName='Year 2',menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.LowerSchool.Year2.Form')

    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year2.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year2.Games.Boys',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year2.Games.Girls',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='Year2.Form.2BN',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year2.Form.2BE',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year2.Form.2BS',menuActions=Menu_Actions_Form)
#    BatMenu.AddItem(GroupName='Year2.Form.2BW',menuActions=Menu_Actions_Class)

    # MiddleSchool

    BatMenu.SetLevel(0)
    BatMenu.SetBaseGroup('Current.Battersea')

    BatMenu.AddItem(GroupName='MiddleSchool',MenuName='Middle School',menuActions=Menu_Actions)
    BatMenu.SetBaseGroup('Current.Battersea.MiddleSchool')
    BatMenu.Up()


    # Year 3

    BatMenu.AddItem(GroupName='Year3',MenuName='Year 3',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.MiddleSchool.Year3.Form')
    BatMenu.Up()

    # Year 3 Games
    BatMenu.AddItem(GroupName='Year3.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year3.Games.Boys',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year3.Games.Girls',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    # Year 3 Mathematics
    BatMenu.AddItem(GroupName='Year3.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year3.Mathematics.SetA',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Mathematics.SetB',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Mathematics.SetC',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Mathematics.SetD',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.Down()
    # Year 3 Spelling
    BatMenu.AddItem(GroupName='Year3.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year3.Spelling.SetA',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Spelling.SetB',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Spelling.SetC',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Spelling.SetD',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.Down()

    BatMenu.AddItem(GroupName='Year3.Form.3BN',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year3.Form.3BE',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year3.Form.3BS',menuActions=Menu_Actions_Form)
#    BatMenu.AddItem(GroupName='Year3.Form.3BW',menuActions=Menu_Actions_Form)


    BatMenu.SetLevel(1)

    # Year 4

    BatMenu.AddItem(GroupName='Year4',MenuName='Year 4',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.MiddleSchool.Year4.Form')
    BatMenu.Up()

    # Year 4 Games
    BatMenu.AddItem(GroupName='Year4.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year4.Games.Boys',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year4.Games.Girls',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    # Year 4 Mathematics
    BatMenu.AddItem(GroupName='Year4.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year4.Mathematics.SetA',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Mathematics.SetB',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Mathematics.SetC',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Mathematics.SetD',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.Down()
    # Year 4 Spelling
    BatMenu.AddItem(GroupName='Year4.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year4.Spelling.SetA',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Spelling.SetB',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Spelling.SetC',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Spelling.SetD',menuActions=Menu_Actions_Class,ParentSubjects=False)
    BatMenu.Down()

    BatMenu.AddItem(GroupName='Year4.Form.4BN',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year4.Form.4BE',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year4.Form.4BS',menuActions=Menu_Actions_Form)
#    BatMenu.AddItem(GroupName='Year4.Form.4BW',menuActions=Menu_Actions_Form)


    BatMenu.SetLevel(1)

 # Year 5

    BatMenu.AddItem(GroupName='Year5',MenuName='Year 5',menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.MiddleSchool.Year5.Form')
    BatMenu.Up()

    # Year 5 Games
    BatMenu.AddItem(GroupName='Year5.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year5.Games.Boys',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year5.Games.Girls',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    # Year 5 Mathematics
    BatMenu.AddItem(GroupName='Year5.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year5.Mathematics.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year5.Mathematics.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year5.Mathematics.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year5.Mathematics.SetD',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    # Year 5 Spelling
    BatMenu.AddItem(GroupName='Year5.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year5.Spelling.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year5.Spelling.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year5.Spelling.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year5.Spelling.SetD',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    #Year 5 Form classes

    BatMenu.AddItem(GroupName='Year5.Form.5BN',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year5.Form.5BE',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year5.Form.5BS',menuActions=Menu_Actions_Form)
#    BatMenu.AddItem(GroupName='Year5.Form.5BW',menuActions=Menu_Actions_Form)


    BatMenu.SetLevel(1)

    # Upper School

    BatMenu.SetLevel(0)
    BatMenu.SetBaseGroup('Current.Battersea')

    BatMenu.AddItem(GroupName='UpperSchool',MenuName='Upper School',menuActions=Menu_Actions)
    BatMenu.SetBaseGroup('Current.Battersea.UpperSchool')
    BatMenu.Up()

    # Year 6

    BatMenu.AddItem(GroupName='Year6',MenuName='Year 6',menuActions=Menu_Actions)
    BatMenu.Up()

    # Year 6 Games
    BatMenu.AddItem(GroupName='Year6.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year6.Games.Boys',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year6.Games.Girls',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 6 Sets

    BatMenu.AddItem(GroupName='Year6.Yr6_Sets',MenuName='Yr6 Sets',ManagedGroups=['11Plus','13Plus'])
    BatMenu.Up()
    BatMenu.SetBaseGroup('Current.Battersea.UpperSchool.Year6.Yr6_Sets')

    # 11 Plus Sets

    BatMenu.AddItem(GroupName='11Plus',menuActions=Menu_Actions)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.Mathematics',ManagedGroups=['SetD','SetE','SetF'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.Mathematics.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.Mathematics.SetE',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.Mathematics.SetF',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.English',ManagedGroups=['SetD','SetE'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.English.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.English.SetE',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.Science',ManagedGroups=['SetD','SetE'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.Science.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.Science.SetE',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.French',ManagedGroups=['SetD','SetE'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.French.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.French.SetE',menuActions=Menu_Actions_Class)

    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.Geography',ManagedGroups=['SetD','SetE'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.Geography.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.Geography.SetE',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.History',ManagedGroups=['SetD','SetE'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.History.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.History.SetE',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.RE',ManagedGroups=['SetD','SetE'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.RE.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.RE.SetE',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.Latin',ManagedGroups=['SetD','SetE'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.Latin.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.Latin.SetE',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.OtherLanguages',ManagedGroups=['Spanish', 'Spanish1', 'German'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.OtherLanguages.Spanish',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.OtherLanguages.Spanish1',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='11Plus.otherLanguages.German',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.Down()

    # 13 Plus Sets

    BatMenu.AddItem(GroupName='13Plus',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.Mathematics',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.Mathematics.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.Mathematics.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.Mathematics.SetC',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.English',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.English.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.English.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.English.SetC',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.Science',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.Science.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.Science.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.Science.SetC',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.French',ManagedGroups=['SetA','SetB','SetC','SetX'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.French.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.French.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.French.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.French.SetX',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.Geography',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.Geography.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.Geography.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.Geography.SetC',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.History',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.History.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.History.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.History.SetC',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.RE',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.RE.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.RE.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.RE.SetC',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.Latin',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.Latin.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.Latin.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='13Plus.Latin.SetC',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    BatMenu.Down()
    BatMenu.Down()

    #Year 6 Form classes
    BatMenu.CreateGroupOnly('Current.Battersea.UpperSchool.Year6.Form')
    BatMenu.SetBaseGroup('Current.Battersea.UpperSchool')

    BatMenu.AddItem(GroupName='Year6.Form.6BN',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year6.Form.6BE',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year6.Form.6BS',menuActions=Menu_Actions_Form)
#    BatMenu.AddItem(GroupName='Year6.Form.6BW',menuActions=Menu_Actions_Form)

    BatMenu.Down()

    # Year 7

    BatMenu.AddItem(GroupName='Year7',MenuName='Year 7',menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.UpperSchool.Year7.Form')
    BatMenu.Up()

    # Year 7 Games
    BatMenu.AddItem(GroupName='Year7.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.Games.Boys',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Games.Girls',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 7 Mathematics
    BatMenu.AddItem(GroupName='Year7.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.Mathematics.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Mathematics.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Mathematics.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Mathematics.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Mathematics.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    # Year 7 English
    BatMenu.AddItem(GroupName='Year7.English',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.English.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.English.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.English.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.English.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.English.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()


    # Year 7 Science
    BatMenu.AddItem(GroupName='Year7.Science',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.Science.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Science.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Science.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Science.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Science.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    # Year 7 French
    BatMenu.AddItem(GroupName='Year7.French',ManagedGroups=['SetA','SetB','SetC','SetD','SetS','SetX'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.French.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.French.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.French.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.French.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.French.SetS',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.French.SetX',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 7 Geography
    BatMenu.AddItem(GroupName='Year7.Geography',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.Geography.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Geography.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Geography.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Geography.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Geography.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 7 Geography
    BatMenu.AddItem(GroupName='Year7.History',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.History.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.History.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.History.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.History.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.History.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 7 RE
    BatMenu.AddItem(GroupName='Year7.RE',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.RE.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.RE.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.RE.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.RE.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.RE.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 7 Latin
    BatMenu.AddItem(GroupName='Year7.Latin',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.Latin.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Latin.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Latin.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Latin.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year7.Latin.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()


    #Year 7 Form classes

    BatMenu.AddItem(GroupName='Year7.Form.7BN',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year7.Form.7BE',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year7.Form.7BS',menuActions=Menu_Actions_Form)
#    BatMenu.AddItem(GroupName='Year7.Form.7BW',menuActions=Menu_Actions_Form)


    BatMenu.SetLevel(1)


    # Year 8

    BatMenu.AddItem(GroupName='Year8',MenuName='Year 8',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.UpperSchool.Year8.Form')
    BatMenu.Up()

    # Year 8 Games
    BatMenu.AddItem(GroupName='Year8.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.Games.Boys',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Games.Girls',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 8 Mathematics
    BatMenu.AddItem(GroupName='Year8.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.Mathematics.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Mathematics.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Mathematics.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Mathematics.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Mathematics.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    # Year 8 English
    BatMenu.AddItem(GroupName='Year8.English',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.English.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.English.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.English.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.English.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.English.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()


    # Year 8 Science
    BatMenu.AddItem(GroupName='Year8.Science',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.Science.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Science.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Science.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Science.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Science.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()
    # Year 8 French
    BatMenu.AddItem(GroupName='Year8.French',ManagedGroups=['SetA','SetB','SetC','SetD','SetS','SetX'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.French.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.French.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.French.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.French.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.French.SetS',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.French.SetX',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 8 History
    BatMenu.AddItem(GroupName='Year8.History',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.History.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.History.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.History.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.History.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.History.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 8 Geography
    BatMenu.AddItem(GroupName='Year8.Geography',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.Geography.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Geography.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Geography.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Geography.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Geography.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 8 RE
    BatMenu.AddItem(GroupName='Year8.RE',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.RE.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.RE.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.RE.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.RE.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.RE.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()

    # Year 8 Latin
    BatMenu.AddItem(GroupName='Year8.Latin',ManagedGroups=['SetA','SetB','SetC','SetD','SetS'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.Latin.SetA',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Latin.SetB',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Latin.SetC',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Latin.SetD',menuActions=Menu_Actions_Class)
    BatMenu.AddItem(GroupName='Year8.Latin.SetS',menuActions=Menu_Actions_Class)
    BatMenu.Down()


    #Year 8 Form classes

    BatMenu.AddItem(GroupName='Year8.Form.8BN',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year8.Form.8BE',menuActions=Menu_Actions_Form)
    BatMenu.AddItem(GroupName='Year8.Form.8BS',menuActions=Menu_Actions_Form)



    BatMenu.SetLevel(0)

    # Academic Houses

    BatMenu.SetBaseGroup('Current.Battersea')
    BatMenu.AddItem(GroupName='House',MenuName='House')
    BatMenu.Up()
    BatMenu.AddItem(GroupName='House.House',ManagedGroups=['Becket','Hardy','Lawrence','More'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='House.House.Becket',menuActions=Menu_Actions)
    BatMenu.AddItem(GroupName='House.House.Hardy',menuActions=Menu_Actions)
    BatMenu.AddItem(GroupName='House.House.Lawrence',menuActions=Menu_Actions)
    BatMenu.AddItem(GroupName='House.House.More',menuActions=Menu_Actions)
    BatMenu.SetLevel(0)
    #  Staff Groups and Menu Items

    if Type=='Admin':

        BatMenu.CreateGroupOnly(GroupName='Staff')
        BatMenu.SetBaseGroup('Staff')
        BatMenu.AddItem(GroupName='Battersea',MenuName='Staff')
        BatMenu.SetBaseGroup('Staff.Battersea')
        BatMenu.Up()
        BatMenu.AddItem(GroupName='LowerSchool' ,MenuName='Lower School',menuActions=Menu_Actions)
        BatMenu.Up()
        BatMenu.AddItem(GroupName='LowerSchool.Head',MenuName='LS Head',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='LowerSchool.Form',MenuName='LS Form',menuActions=Menu_Actions)
        BatMenu.Up()
        for classes in ['RBE','RBN','RBS','RBW','1BE','1BN','1BS','1BW','2BE','2BN','2BS','2BW']:
            BatMenu.AddItem(GroupName='LowerSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        BatMenu.Down()
        BatMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions)

        BatMenu.AddItem(GroupName='LowerSchool.Year1',MenuName='Year 1',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='LowerSchool.Year2',MenuName='Year 2',menuActions=Menu_Actions)
        BatMenu.Down()
        BatMenu.AddItem(GroupName='MiddleSchool' ,MenuName='Middle School',menuActions=Menu_Actions)
        BatMenu.Up()
        BatMenu.AddItem(GroupName='MiddleSchool.Head',MenuName='MS Head',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='MiddleSchool.Form',MenuName='MS Form',menuActions=Menu_Actions)

        BatMenu.Up()
        for classes in ['3BE','3BN','3BS','3BW','4BE','4BN','4BS','4BW','5BE','5BN','5BS','5BW']:
            BatMenu.AddItem(GroupName='MiddleSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        BatMenu.Down()

        BatMenu.AddItem(GroupName='MiddleSchool.Year3',MenuName='Year 3',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='MiddleSchool.Year4',MenuName='Year 4',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='MiddleSchool.Year5',MenuName='Year 5',menuActions=Menu_Actions)
        BatMenu.Down()

        BatMenu.AddItem(GroupName='UpperSchool' ,MenuName='Upper School',menuActions=Menu_Actions)
        BatMenu.Up()
        BatMenu.AddItem(GroupName='UpperSchool.Head',MenuName='US Head',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='UpperSchool.Form',MenuName='US Form',menuActions=Menu_Actions)

        BatMenu.Up()
        for classes in ['6BE','6BN','6BS','6BW','7BE','7BN','7BS','7BW','8BE','8BN','8BS','8BW']:
            BatMenu.AddItem(GroupName='UpperSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        BatMenu.Down()

        BatMenu.AddItem(GroupName='UpperSchool.Year6',MenuName='Year 6',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='UpperSchool.Year7',MenuName='Year 7',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='UpperSchool.Year8',MenuName='Year 8',menuActions=Menu_Actions)
        BatMenu.Down()

        BatMenu.AddItem(GroupName='Department' ,MenuName='Departments')
        BatMenu.Up()
        BatMenu.AddItem(GroupName='Department.HeadTeacher',MenuName='HeadTeacher',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Department.SysAdmin',MenuName='SysAdmin',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Department.OfficeAdmin',MenuName='Office Admin',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Department.Transport',MenuName='Transport',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Department.Maintainance',MenuName='Maintinance',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Department.Catering',MenuName='Catering',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Department.CurrHODS',MenuName="Curr-HoD's",menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Department.CoHODS',MenuName="Co-Curr HoD's",menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Department.SLT',MenuName="SLT",menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Department.PE',MenuName='PE',menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Department.SEN',MenuName='Special Needs',menuActions=Menu_Actions)

        BatMenu.Down()
