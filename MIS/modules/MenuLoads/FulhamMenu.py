from MIS.models import *
from MIS.modules.MenuBuilder.MenuBuilder import *
from MIS.forms import *
import time, datetime, copy
from operator import itemgetter

#-------------------------------------------------------------------------------
# Name:        Fulham Menu Loader
# Purpose:
#
# Author:      DBMgr
#
# Created:     10/04/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def FulhamMenu(Type='Teacher'):

    currentYear=int(datetime.date.today().year)
    AvailableClasses={
    'Reception':['RFE','RFN','RFS'],'Year1':['1FE','1FN','1FS'],
    'Year2':['2FE','2FN','2FS'],'Year3':['3FE','3FN','3FS'],
    'Year4':['4FE','4FN','4FS'],'Year5':['5FE','5FN','5FS'],'Year6':['6FE','6FN','6FS']}


    Menu_Actions_Form=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Attendance','Grade']
    Menu_Actions_Set=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Grade']
    Menu_Actions_Class=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite','Grade']
    Menu_Actions=['ManageAll','ManageSEN','ManageEAL','MostAbled','MoreAbled','AddFavourite']

    if Type=='Admin':
        FulMenu= MenuBuilder(Type='New',MenuType='FulhamAdmin',BaseGroup='Alumni')
        FulMenu.SetLevel(0)
        FulMenu.SetBaseGroup(None)
        FulMenu.AddItem(GroupName='Alumni',MenuName='Alumni')
        FulMenu.Up()

        FulMenu.AddItem(GroupName='Alumni.Fulham',MenuName='All',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Alumni.Fulham.AskedToLeave',MenuName='Asked To Leave',menuActions=Menu_Actions)
        FulMenu.SetLevel(0)
        FulMenu.SetBaseGroup('Current.Fulham')
    else:
        FulMenu= MenuBuilder(Type='New',MenuType='FulhamTeacher',BaseGroup='Current.Fulham')




    # Lower school
    FulMenu.AddItem(GroupName='LowerSchool',MenuName='Lower School',menuActions=Menu_Actions)
    FulMenu.Up()
    FulMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions)

    FulMenu.Up()

    # Reception

    FulMenu.SetBaseGroup('Current.Fulham.LowerSchool.Reception')
    FulMenu.CreateGroupOnly(GroupName='Current.Fulham.LowerSchool.Reception.Form')

    FulMenu.AddItem(GroupName='Form.RFN',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Form.RFE',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Form.RFS',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Form.RFW',menuActions=Menu_Actions_Form)


    FulMenu.SetLevel(1)

    # Year 1

    FulMenu.SetBaseGroup('Current.Fulham.LowerSchool')

    FulMenu.AddItem(GroupName='Year1',MenuName='Year 1',menuActions=Menu_Actions)
    FulMenu.CreateGroupOnly('Current.Fulham.LowerSchool.Year1.Form')

    # 1BN
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year1.Form.1FN',menuActions=Menu_Actions_Form)
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year1.Form.1FN.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year1.Form.1FN.BalGames.Ballet',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year1.Form.1FN.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    FulMenu.Down()

    # 1BE

    FulMenu.AddItem(GroupName='Year1.Form.1FE',menuActions=Menu_Actions_Form)
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year1.Form.1FE.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year1.Form.1FE.BalGames.Ballet',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year1.Form.1FE.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    FulMenu.Down()

    # 1BS

    FulMenu.AddItem(GroupName='Year1.Form.1FS',menuActions=Menu_Actions_Form)
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year1.Form.1FS.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year1.Form.1FS.BalGames.Ballet',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year1.Form.1FS.BalGames.BoysGames',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    FulMenu.Down()

    # 1BW

    FulMenu.AddItem(GroupName='Year1.Form.1FW',menuActions=Menu_Actions_Form)
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year1.Form.1FW.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year1.Form.1FW.BalGames.Ballet',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year1.Form.1FW.BalGames.BoysGames',menuActions=Menu_Actions_Class)


    # Year 2

    FulMenu.SetLevel(1)

    FulMenu.AddItem(GroupName='Year2',MenuName='Year 2',menuActions=Menu_Actions)
    FulMenu.CreateGroupOnly('Current.Fulham.LowerSchool.Year2.Form')

    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year2.Games',ManagedGroups=['Boys','Girls'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year2.Games.Boys',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year2.Games.Girls',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    FulMenu.AddItem(GroupName='Year2.Form.2FN',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year2.Form.2FE',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year2.Form.2FS',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year2.Form.2FW',menuActions=Menu_Actions_Form)

    # PrepSchool

    FulMenu.SetLevel(0)
    FulMenu.SetBaseGroup('Current.Fulham')

    FulMenu.AddItem(GroupName='PrepSchool',MenuName='Prep School',menuActions=Menu_Actions)
    FulMenu.SetBaseGroup('Current.Fulham.PrepSchool')
    FulMenu.Up()


    # Year 3

    FulMenu.AddItem(GroupName='Year3',MenuName='Year 3',menuActions=Menu_Actions)
    FulMenu.CreateGroupOnly('Current.Fulham.PrepSchool.Year3.Form')
    FulMenu.Up()

    # Year 3 Games
    FulMenu.AddItem(GroupName='Year3.Games',ManagedGroups=['Boys','Girls'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year3.Games.Boys',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year3.Games.Girls',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    # Year 3 Mathematics
    FulMenu.AddItem(GroupName='Year3.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year3.Mathematics.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year3.Mathematics.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year3.Mathematics.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year3.Mathematics.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    # Year 3 Spelling
    FulMenu.AddItem(GroupName='Year3.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year3.Spelling.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year3.Spelling.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year3.Spelling.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year3.Spelling.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()

    FulMenu.AddItem(GroupName='Year3.Form.3FN',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year3.Form.3FE',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year3.Form.3FS',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year3.Form.3FW',menuActions=Menu_Actions_Form)


    FulMenu.SetLevel(1)

    # Year 4

    FulMenu.AddItem(GroupName='Year4',MenuName='Year 4',menuActions=Menu_Actions)
    FulMenu.CreateGroupOnly('Current.Fulham.PrepSchool.Year4.Form')
    FulMenu.Up()

    # Year 4 Games
    FulMenu.AddItem(GroupName='Year4.Games',ManagedGroups=['Boys','Girls'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year4.Games.Boys',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year4.Games.Girls',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    # Year 4 Mathematics
    FulMenu.AddItem(GroupName='Year4.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year4.Mathematics.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year4.Mathematics.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year4.Mathematics.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year4.Mathematics.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    # Year 4 Spelling
    FulMenu.AddItem(GroupName='Year4.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year4.Spelling.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year4.Spelling.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year4.Spelling.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year4.Spelling.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()

    FulMenu.AddItem(GroupName='Year4.Form.4FN',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year4.Form.4FE',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year4.Form.4FS',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year4.Form.4FW',menuActions=Menu_Actions_Form)


    FulMenu.SetLevel(1)

 # Year 5

    FulMenu.AddItem(GroupName='Year5',MenuName='Year 5',menuActions=Menu_Actions)
    FulMenu.CreateGroupOnly('Current.Fulham.PrepSchool.Year5.Form')
    FulMenu.Up()

    # Year 5 Games
    FulMenu.AddItem(GroupName='Year5.Games',ManagedGroups=['Boys','Girls'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year5.Games.Boys',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year5.Games.Girls',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    # Year 5 Mathematics
    FulMenu.AddItem(GroupName='Year5.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year5.Mathematics.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year5.Mathematics.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year5.Mathematics.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year5.Mathematics.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    # Year 5 Spelling
    FulMenu.AddItem(GroupName='Year5.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year5.Spelling.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year5.Spelling.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year5.Spelling.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year5.Spelling.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()

    #Year 5 Form classes

    FulMenu.AddItem(GroupName='Year5.Form.5FN',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year5.Form.5FE',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year5.Form.5FS',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year5.Form.5FW',menuActions=Menu_Actions_Form)


    FulMenu.SetLevel(1)

    # Year 6

    FulMenu.AddItem(GroupName='Year6',MenuName='Year 6',SubMenus=['Lists'],menuActions=Menu_Actions)
    FulMenu.Up()

    # Year 6 Games
    FulMenu.AddItem(GroupName='Year6.Games',ManagedGroups=['Boys','Girls'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Year6.Games.Boys',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Year6.Games.Girls',menuActions=Menu_Actions_Class)
    FulMenu.Down()

    # Year 6 Sets

    FulMenu.SetBaseGroup('Current.Fulham.PrepSchool.Year6')


    FulMenu.AddItem(GroupName='Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Mathematics.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Mathematics.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Mathematics.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Mathematics.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    FulMenu.AddItem(GroupName='English',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='English.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='English.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='English.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='English.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    FulMenu.AddItem(GroupName='Science',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Science.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Science.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Science.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Science.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    FulMenu.AddItem(GroupName='French',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='French.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='French.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='French.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='French.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    FulMenu.AddItem(GroupName='Geography',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Geography.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Geography.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Geography.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Geography.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    FulMenu.AddItem(GroupName='History',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='History.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='History.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='History.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='History.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()
    FulMenu.AddItem(GroupName='Latin',ManagedGroups=['SetA','SetB','SetC','SetD'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='Latin.SetA',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Latin.SetB',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Latin.SetC',menuActions=Menu_Actions_Class)
    FulMenu.AddItem(GroupName='Latin.SetD',menuActions=Menu_Actions_Class)
    FulMenu.Down()

    #Year 6 Form classes
    FulMenu.CreateGroupOnly('Current.Fulham.PrepSchool.Year6.Form')
    FulMenu.SetBaseGroup('Current.Fulham.PrepSchool')

    FulMenu.AddItem(GroupName='Year6.Form.6FN',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year6.Form.6FE',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year6.Form.6FS',menuActions=Menu_Actions_Form)
    FulMenu.AddItem(GroupName='Year6.Form.6FW',menuActions=Menu_Actions_Form)

    FulMenu.SetLevel(0)

    # Academic Houses

    FulMenu.SetBaseGroup('Current.Fulham')
    FulMenu.AddItem(GroupName='House',MenuName='House')
    FulMenu.Up()
    FulMenu.AddItem(GroupName='House.House',ManagedGroups=['Becket','Hardy','Lawrence','More'])
    FulMenu.Up()
    FulMenu.AddItem(GroupName='House.House.Becket',menuActions=Menu_Actions)
    FulMenu.AddItem(GroupName='House.House.Hardy',menuActions=Menu_Actions)
    FulMenu.AddItem(GroupName='House.House.Lawrence',menuActions=Menu_Actions)
    FulMenu.AddItem(GroupName='House.House.More',menuActions=Menu_Actions)
    FulMenu.SetLevel(0)
    #  Staff Groups and Menu Items

    if Type=='Admin':
        FulMenu.CreateGroupOnly(GroupName='Staff')
        FulMenu.SetBaseGroup('Staff')
        FulMenu.AddItem(GroupName='Fulham',MenuName='Staff')
        FulMenu.SetBaseGroup('Staff.Fulham')
        FulMenu.Up()
        FulMenu.AddItem(GroupName='LowerSchool' ,MenuName='Lower School',menuActions=Menu_Actions)
        FulMenu.Up()
        FulMenu.AddItem(GroupName='LowerSchool.Form',MenuName='LS Form',menuActions=Menu_Actions)
        FulMenu.Up()
        for classes in AvailableClasses['Reception']:
            FulMenu.AddItem(GroupName='LowerSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year1']:
            FulMenu.AddItem(GroupName='LowerSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year2']:
            FulMenu.AddItem(GroupName='LowerSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        FulMenu.Down()

        FulMenu.AddItem(GroupName='LowerSchool.Head',MenuName='LS Head',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions)

        FulMenu.AddItem(GroupName='LowerSchool.Year1',MenuName='Year 1',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='LowerSchool.Year2',MenuName='Year 2',menuActions=Menu_Actions)
        FulMenu.Down()
        FulMenu.AddItem(GroupName='MiddleSchool' ,MenuName='Prep School',menuActions=Menu_Actions)
        FulMenu.Up()
        FulMenu.AddItem(GroupName='MiddleSchool.Form',MenuName='MS Form',menuActions=Menu_Actions)
        FulMenu.Up()
        for classes in AvailableClasses['Year3']:
            FulMenu.AddItem(GroupName='MiddleSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year4']:
            FulMenu.AddItem(GroupName='MiddleSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        for classes in AvailableClasses['Year5']:
            FulMenu.AddItem(GroupName='MiddleSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        FulMenu.Down()
        FulMenu.AddItem(GroupName='MiddleSchool.Head',MenuName='MS Head',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='MiddleSchool.Year3',MenuName='Year 3',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='MiddleSchool.Year4',MenuName='Year 4',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='MiddleSchool.Year5',MenuName='Year 5',menuActions=Menu_Actions)
        FulMenu.Down()

        FulMenu.AddItem(GroupName='School' ,MenuName='Upper School',menuActions=Menu_Actions)
        FulMenu.Up()
        FulMenu.AddItem(GroupName='UpperSchool.Form',MenuName='US Form',menuActions=Menu_Actions)
        FulMenu.Up()
        for classes in AvailableClasses['Year6']:
            FulMenu.AddItem(GroupName='UpperSchool.Form.%s' % classes,MenuName='%s Teachers' % classes,menuActions=Menu_Actions)
        FulMenu.Down()
        FulMenu.AddItem(GroupName='UpperSchool.Head',MenuName='US Head',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='UpperSchool.Year6',MenuName='Year 6',menuActions=Menu_Actions)
        FulMenu.Down()

        FulMenu.AddItem(GroupName='Department' ,MenuName='Departments')
        FulMenu.Up()
        FulMenu.AddItem(GroupName='Department.HeadTeacher',MenuName="Head Teacher",menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Department.SysAdmin',MenuName='SysAdmin',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Department.OfficeAdmin',MenuName='Office Admin',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Department.Transport',MenuName='Transport',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Department.Maintainance',MenuName='Maintinance',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Department.Catering',MenuName='Catering',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Department.CurrHODS',MenuName="Curr-HoD's",menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Department.CoHODS',MenuName="Co-Curr HoD's",menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Department.SLT',MenuName="SLT",menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Department.PE',MenuName='PE',menuActions=Menu_Actions)
        FulMenu.AddItem(GroupName='Department.SEN',MenuName='Special Needs',menuActions=Menu_Actions)

        FulMenu.Down()
