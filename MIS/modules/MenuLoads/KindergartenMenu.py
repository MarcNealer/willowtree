from MIS.models import *
from MIS.modules.MenuBuilder.MenuBuilder import *
from MIS.forms import *
import time, datetime, copy
from operator import itemgetter

#-------------------------------------------------------------------------------
# Name:        Battersea Menu Loader
# Purpose:
#
# Author:      DBMgr
#
# Created:     10/04/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def KindergartenMenu():

    currentYear=int(datetime.date.today().year)


    kinMenu= MenuBuilder(Type='New',MenuType='KindergartenApplicants',BaseGroup='Applicants.Kindergarten')
    kinMenu.SetLevel(0)
    kinMenu.AddItem(GroupName='PimMain',MenuName='Pimlico Main',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()

    kinMenu.AddItem(GroupName='PimMain.September',MenuName='September',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='PimMain.September.Lower',MenuName='Lower',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='PimMain.September.Lower.Accepted',MenuName='Accepted',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.AddItem(GroupName='PimMain.September.Lower.Offered',MenuName='Offered',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.Down()
    kinMenu.AddItem(GroupName='PimMain.September.Upper',MenuName='Upper',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='PimMain.September.Upper.Accepted',MenuName='Accepted',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.AddItem(GroupName='PimMain.September.Upper.Offered',MenuName='Offered',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.SetLevel(1)
    kinMenu.AddItem(GroupName='PimMain.January',MenuName='January',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='PimMain.January.Lower',MenuName='Lower',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='PimMain.January.Lower.Accepted',MenuName='Accepted',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.AddItem(GroupName='PimMain.January.Lower.Offered',MenuName='Offered',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.Down()
    kinMenu.AddItem(GroupName='PimMain.January.Upper',MenuName='Upper',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='PimMain.January.Upper.Accepted',MenuName='Accepted',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.AddItem(GroupName='PimMain.January.Upper.Offered',MenuName='Offered',menuActions=['ManageThis'],SubMenus=['Lists'])
    
    kinMenu.SetLevel(0)
    
    kinMenu.AddItem(GroupName='BatMain',MenuName='Battersea Main',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()

    kinMenu.AddItem(GroupName='BatMain.September',MenuName='September',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='BatMain.September.Lower',MenuName='Lower',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='BatMain.September.Lower.Accepted',MenuName='Accepted',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.AddItem(GroupName='BatMain.September.Lower.Offered',MenuName='Offered',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.Down()
    kinMenu.AddItem(GroupName='BatMain.September.Upper',MenuName='Upper',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='BatMain.September.Upper.Accepted',MenuName='Accepted',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.AddItem(GroupName='BatMain.September.Upper.Offered',MenuName='Offered',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.SetLevel(1)
    kinMenu.AddItem(GroupName='BatMain.January',MenuName='January',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='BatMain.January.Lower',MenuName='Lower',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='BatMain.January.Lower.Accepted',MenuName='Accepted',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.AddItem(GroupName='BatMain.January.Lower.Offered',MenuName='Offered',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.Down()
    kinMenu.AddItem(GroupName='BatMain.January.Upper',MenuName='Upper',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='BatMain.January.Upper.Accepted',MenuName='Accepted',menuActions=['ManageThis'],SubMenus=['Lists'])
    kinMenu.AddItem(GroupName='BatMain.January.Upper.Offered',MenuName='Offered',menuActions=['ManageThis'],SubMenus=['Lists'])

    kinMenu.SetLevel(0)
    kinMenu.AddItem(GroupName='Withdrawn',MenuName='Withdrawn',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])

    # Admin menu starts herer
    
    kinMenu= MenuBuilder(Type='New',MenuType='KindergartenAdmin',BaseGroup='Current.Kindergarten')
    kinMenu.SetLevel(0)
    kinMenu.AddItem(GroupName='Pimlico',MenuName='Pimlico',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='Pimlico.Lower',MenuName='Lower',menuActions=['ManageThis','ManageAll','Attendance'],SubMenus=['Lists'])
    kinMenu.AddItem(GroupName='Pimlico.Upper',MenuName='Upper',menuActions=['ManageThis','ManageAll','Attendance'],SubMenus=['Lists'])
    kinMenu.SetLevel(0)
    kinMenu.AddItem(GroupName='Battersea',MenuName='Battersea',menuActions=['ManageThis','ManageAll','Attendance'],SubMenus=['Lists'])
    kinMenu.Up()
    kinMenu.AddItem(GroupName='Battersea.Lower',MenuName='Lower',menuActions=['ManageThis','ManageAll','Attendance'],SubMenus=['Lists'])
    kinMenu.AddItem(GroupName='Battersea.Upper',MenuName='Upper',menuActions=['ManageThis','ManageAll','Attendance'],SubMenus=['Lists'])
    
    


  