# -*- coding: utf-8 -*-
from MIS.models import *
from MIS.modules.MenuBuilder.MenuBuilder import *
from MIS.forms import *
import time, datetime, copy,sys
from operator import itemgetter

def AddAction(ActionName,School):
    Action=MenuActions.objects.get(Name=ActionName)
    MenuItems=Menus.objects.filter(Group__Name__istartswith='Current', Group__Name__icontains=School)
    for Item in MenuItems:
        Item.MenuActions.add(Action)
        Item.save()