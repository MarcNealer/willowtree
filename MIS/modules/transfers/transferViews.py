# Django middleware imports
from django.shortcuts import render_to_response
from django.template import RequestContext


# WillowTree system imports
from MIS.ViewExtras import SetMenu
from MIS.models import Pupil


def transferSearchPage(request, school, AcYear, transferComplete=False):
    ''' displays the search page for pupil transfers to another thomas's
    school '''
    template = 'transferSearch.html'
    contextInstanceData = RequestContext(request,processors=[SetMenu])
    c = {'school': school, 'AcYear': AcYear}
    if transferComplete:
        c.update({'pupilObj': Pupil.objects.get(id=int(transferComplete))})
    return render_to_response(template, c, context_instance=contextInstanceData)