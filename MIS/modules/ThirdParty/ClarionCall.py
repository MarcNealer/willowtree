from MIS.modules import GeneralFunctions
from MIS.modules import PupilRecs
from MIS.models import *
from django.http import *
from django.template.loader import render_to_string
from MIS.modules.globalVariables import GlobalVariablesFunctions
import requests as url_requests
from StringIO import StringIO

# import log
import logging
log = logging.getLogger(__name__)

# how to call the clarioncall extract
# t=ClarionCallExtract(AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear'),
#                      msisdn='07797141417',uploadCode='01132536EF25',schoolName="Thomas's London Day School - Test",
#                      uploadFile='dirsync-ICClub_6B2AFFDE570A540E000001132536EFC6.xml')


def ClarionCallExtractPupil(AcYear, GroupName, msisdn, uploadCode, schoolName, uploadfile):
    """
    Sends pupil data to ClarionCall.
    """
    pupillist = GeneralFunctions.Get_PupilList(AcYear, GroupName)
    outdata = []
    for pupils in pupillist:
        outdata.append(PupilRecs.PupilRecord(pupils.id, AcYear))
    grouplist = Group.objects.filter(Name__istartswith=GroupName).exclude(Name__iendswith='Form')
    c = {'PupilList': outdata, 'GroupList': grouplist,
         'YearGroupCheck':['Year1','Year2','Year3','Year4','Year5','Year6','Year7','Year8']}
    DataFile = render_to_string('ClarionCallUpload.xml', c)
    datalist = {'msisdn': msisdn, 'uploadCode': uploadCode, 'schoolName': schoolName}
    uploadfile = {'uploadFile': (uploadfile, StringIO(DataFile))}
    uploadurl = 'https://www.myclarioncall.co.uk/submitFile.do'
    r = url_requests.post(uploadurl, data=datalist, files=uploadfile)
    return r


def ClarionCallExtractStaff(ac_year, group, msisdn, uploadcode, school_name, uploadfile):
    """
    Sends staff data to ClarionCall.
    """
    data_to_template = [["PREFERRED_NAME",
                         "SURNAME",
                         "EMAIL_ADDRESS",
                         "MOBILE_TELEPHONE",
                         "PIN",
                         "MANAGE_GROUP1",
                         "MANAGE_GROUP2",
                         "MANAGE_GROUP3",
                         "MANAGE_GROUP4",
                         "MANAGE_GROUP5",
                         "MANAGE_GROUP6",
                         "MANAGE_GROUP7",
                         "MANAGE_GROUP8",
                         "MANAGE_GROUP9",
                         "MANAGE_GROUP10",
                         "MANAGE_GROUP11",
                         "MANAGE_GROUP12",
                         "STAFF_ID",
                         "EVENING_TELEPHONE",
                         "HOME_ADDRESS_HOUSE",
                         "HOME_ADDRESS_STREET",
                         "HOME_ADDRESS_TOWN",
                         "HOME_ADDRESS_COUNTY",
                         "HOME_ADDRESS_POSTCODE",
                         "HOME_ADDRESS_COUNTRY",
                         "TITLE",
                         "INITIALS",
                         "DEPARTMENT",
                         "ROLE",
                         "MOBILE_OPT_OUT",
                         "EMAIL2",
                         "EMAIL_OPT_OUT",
                         "EMAIL2_OPT_OUT"]]

    # IMPORTANT! The schools have requested that only staff in the staff groups containing the '_Staff_Register' string
    # are to be brought over to ClarionCall.
    groups_0 = Group.objects.filter(Name__icontains=group).filter(Name__icontains="_Staff_Register").values_list("Name", flat=True)
    groups_1 = Group.objects.filter(Name__icontains=group).filter(Name__icontains='Department.IT').values_list("Name", flat=True)
    groups_2 = Group.objects.filter(Name__icontains=group).filter(Name__icontains="Department.SysAdmin").values_list("Name", flat=True)
    groups = list(groups_0) + list(groups_1) + list(groups_2)
    staff_groups = StaffGroup.objects.filter(Group__Name__in=groups).order_by("Staff__id").distinct("Staff__id").values_list('Staff__id', flat=True)
    for staff_id in staff_groups:
        staff_object = Staff.objects.get(id=staff_id)
        #groups = StaffGroup.objects.filter(Staff__id=staff_id, AcademicYear=ac_year).values_list('Group__Name',
        #                                                                                         flat=True)
        try:
            data_to_template.append([remove_unwanted_characters(staff_object.Forename),
                                     remove_unwanted_characters(staff_object.Surname),
                                     remove_unwanted_characters(staff_object.EmailAddress),
                                     remove_unwanted_characters(staff_object.Address.Phone1),
                                     "1234",
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(staff_object.id),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str(),
                                     str()])
        except Exception as error:
            log.warn('ERROR: %s' % error)
    c = {'staff_data': data_to_template}
    data_file = render_to_string('clarion_call_extract_staff.csv', c)
    data_list = {'msisdn': msisdn, 'uploadCode': uploadcode, 'schoolName': school_name}
    uploadfile = {'uploadFile': (uploadfile, StringIO(data_file))}
    uploadurl = 'https://www.myclarioncall.co.uk/submitFile.do'
    r = url_requests.post(uploadurl, data=data_list, files=uploadfile)
    return r


def remove_unwanted_characters(string):
    """
    Simple remove unwanted characters function defined by ClarionCall for uploading data to their servers.
    """
    if string:
        string = string.strip()
        string = string.replace("'", '')
        string = string.replace('"', '')
        string = string.replace('`', '')
        string = string.replace('/', '')
        string = string.replace(' ', '')
        string = string.replace("\\", '')
        return str(string)
    else:
        return "None"