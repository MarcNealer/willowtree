# python imports
import datetime

# Django middleware imports
from django.http import HttpResponseRedirect


# WillowTree system imports
from MIS.modules.ExtendedRecords import ExtendedRecord
from MIS.models import Pupil
from MIS.models import PupilAlert
from MIS.models import AlertType
from MIS.models import Staff


def addOrModifyMoreAbledAlert(request, school, AcYear, PupilId):
    ''' adds or deletes a new more able record to a Pupil '''
    if PupilAlert.objects.filter(PupilId__id=int(PupilId),
                                 AlertType__AlertGroup__Name='More Abled',
                                 Active=True).exists():
        alertObj = PupilAlert.objects.get(PupilId__id=int(PupilId),
                                          AlertType__AlertGroup__Name='More Abled',
                                          Active=True)
# delete PupilAlert...
        if 'delete' in request.POST:
            alertObj.Active = False
            alertObj.save()
# new PupilAlert...
    else:
        alertDetails = ''
        extObj = ExtendedRecord('Pupil', PupilId)
        for i in range(8):
            try:
                if extObj.ReadExtention('Yr%s_Cat' % str(i))['CATScore'][0] != '':
                    alertDetails += '<br />Cat Score Yr%s: %s' % (str(i),
                                                                  extObj.ReadExtention('Yr%s_Cat' % str(i))['CATScore'][0])
            except:
                pass
        pupilAlertObj = PupilAlert(PupilId=Pupil.objects.get(id=int(PupilId)),
                                   StartDate=datetime.datetime.now(),
                                   AlertType=AlertType.objects.get(Name='More Abled'),
                                   AlertDetails=alertDetails,
                                   RaisedBy=Staff.objects.get(User__id=request.session['_auth_user_id']))
        pupilAlertObj.save()
    httpAddress = '/WillowTree/%s/%s/pupil/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)
