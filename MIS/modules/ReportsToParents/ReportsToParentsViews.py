# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext,Context
from django.core.context_processors import csrf
from django.http import *
from MIS.modules.gradeInput.GradeFunctions import *
from MIS.modules.GeneralFunctions import *

# WillowTree system imports

from MIS.modules.PupilRecs import *
from MIS.modules import GeneralFunctions

def EPIF_Report(request,AcYear,school,GroupName,NewAcYear):
    Classlist=[]
    for PupilIds in request.POST.getlist('PupilSelect'):
        Classlist.append(PupilRecord(PupilIds,AcYear))
    c={'ClassList':Classlist,'AcYear':NewAcYear,'PupilForm':GroupName.split('.')[-1]}
    return render_to_response('EPIF_Report.html',c,context_instance=RequestContext(request))

def Report2Parents(request,AcYear,school,GroupName):
    ReportList=[]
    for PupilIds in request.POST.getlist('PupilSelect'):
        ReportList.append(PupilGradeRecord(PupilIds,AcYear,request.POST['ReportName']))
    c={'ReportList':ReportList}
    return render_to_response('ReportsToParents/%s.html' % request.POST['ReportName'],c,context_instance=RequestContext(request))


def Report2NewParents(request,AcYear,school,GroupName):
    ReportList=[]
    for PupilIds in request.POST.getlist('PupilSelect'):
        ReportList.append(PupilGradeRecord(PupilIds,AcYear,request.POST['ReportName']))
    c=RequestContext(request,{'ReportList':ReportList,'ReportType':request.POST['ReportType']})
    t=loader.select_template(['ReportsToParents/%s_%s_%s.html' % (request.POST['ReportName'],GetSchoolName(school),request.POST['ReportType']),
                              'ReportsToParents/%s_%s.html' % (request.POST['ReportName'],request.POST['ReportType']),
                              'ReportsToParents/%s_%s.html' % (request.POST['ReportName'],GetSchoolName(school)),
                              'ReportsToParents/%s.html' % (request.POST['ReportName']),
                              'ReportsToParents/NoReportError.html'])
    return HttpResponse(t.render(c))
