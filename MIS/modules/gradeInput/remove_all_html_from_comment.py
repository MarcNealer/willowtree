from MIS.modules import ExtendedRecords
import re


def remove_all_html_from_comment(list_of_pupil_ids, reporting_season, subject):
    """
    Does what it says on the tin - remember to restart memcache if needed
    """
    for i in list_of_pupil_ids:
        x = ExtendedRecords.ExtendedRecord('Pupil', i)
        amended_comment = re.sub('<[^<]+?>', '', x.ReadExtention(reporting_season, subject)['Comment'][0])
        x.WriteExtention('Yr6_Eot_S', {"Comment": "<p>%s</p>" % amended_comment}, subject)
