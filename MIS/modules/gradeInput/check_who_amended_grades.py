from MIS.models import ExtentionData


def run(grade_record, pupil_id, subject_name, show_data=False):
    """
    Shows which member of staff updates with pupils grade records
    """
    data = ExtentionData.objects.filter(ExtentionRecord__Name=grade_record,
                                        BaseType='Pupil',
                                        BaseId=pupil_id,
                                        Subject__Name=subject_name)
    for i in data:
        print "%s\n" % i.UpdatedOn.isoformat()
        print "%s\n" % i.UpdatedBy
        if show_data:
            print "%s\n" % i.Data
        print "-----------------------------------------------------------------------------------------------------\n"