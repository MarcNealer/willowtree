# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 08:42:54 2013

@author: dbmgr
"""
from django.core.cache import cache
import datetime
import copy
from MIS.models import *
from MIS.modules.ExtendedRecords import *
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules.attendance.AttendanceFunctions import *
import numpy

class PupilGrade():
    ''' Pupil Grade Object. This object holds basic details on the pupil, along
    with the grades for the Current Record passed as an argument, and its previous
    terms grades as satted in GradeInputElement'''
    def __init__(self,PupilId,CurrentRecord,SingleSubject=None):
        if SingleSubject:
            self.GradeElements=GradeInputElement.objects.filter(GradeRecord__Name=CurrentRecord,GradeSubject__Name=SingleSubject)
        else:
            self.GradeElements=GradeInputElement.objects.filter(GradeRecord__Name=CurrentRecord)
        self.ExtendedRecord=ExtendedRecord(BaseType='Pupil',BaseId=PupilId)
        self.GradeRecords=[]
        self.PupilItems=Pupil.objects.get(id=PupilId)
        for Records in self.GradeElements:
            GradeData={'Pupil':self.PupilItems,'Current':{},'Previous':{},'Form':Records.InputForm,'SubSubject':{},'GradeRecord':Records.GradeRecord.Name}
            CurRec=cache.get('%s_%s_%s' % (CurrentRecord,Records.GradeSubject.Name,PupilId))
            if not CurRec:
                CurRec=self.ExtendedRecord.ReadExtention(ExtentionName=Records.GradeRecord.Name,SubjectName=Records.GradeSubject.Name)
                cache.set('%s_%s_%s' % (CurrentRecord,Records.GradeSubject.Name,PupilId),CurRec,3600)
            GradeData['Current']=CurRec
            if Records.PrevGradeRecord:
                PrevRec=cache.get('%s_%s_%s' % (Records.PrevGradeRecord.Name,Records.GradeSubject.Name,PupilId))
                if not PrevRec:
                    PrevRec=self.ExtendedRecord.ReadExtention(ExtentionName=Records.PrevGradeRecord.Name,SubjectName=Records.GradeSubject.Name)
                    cache.set('%s_%s_%s' % (Records.PrevGradeRecord.Name,Records.GradeSubject.Name,PupilId),PrevRec,3600)
            else:
                PrevRec=None
            GradeData['Previous']=PrevRec

            if ParentSubjects.objects.filter(ParentSubject=Records.GradeSubject).exists():
                SubGradeData={}
                for subSubjects in ParentSubjects.objects.filter(ParentSubject=Records.GradeSubject):

                    subCurrRec=cache.get('%s_%s_%s' % (CurrentRecord,subSubjects.Subject.Name,PupilId))
                    if not subCurrRec:
                        subCurrRec=self.ExtendedRecord.ReadExtention(ExtentionName=Records.GradeRecord.Name,SubjectName=subSubjects.Subject.Name)
                        cache.set('%s_%s_%s' % (CurrentRecord,subSubjects.Subject.Name,PupilId),subCurrRec,3600)
                    subPrevRec={}
                    if Records.PrevGradeRecord:
                        subPrevRec=cache.get('%s_%s_%s' % (Records.PrevGradeRecord.Name,subSubjects.Subject.Name,PupilId))
                        if not subPrevRec:
                            subPrevRec=self.ExtendedRecord.ReadExtention(ExtentionName=Records.PrevGradeRecord.Name,SubjectName=subSubjects.Subject.Name)
                            cache.set('%s_%s_%s' % (Records.PrevGradeRecord.Name,subSubjects.Subject.Name,PupilId),subPrevRec,3600)
                    SubGradeData[subSubjects.Subject.Name]={'Current':subCurrRec,'Previous':subPrevRec}
                GradeData['SubSubject']=copy.deepcopy(SubGradeData)

            self.GradeRecords.append([Records.GradeSubject.Name,GradeData])



class GroupGrade():
    ''' Group Grade Object. This gathers grades for a selected Grade record, for
    a preselected subject and group'''
    def __init__(self,AcademicYear,Group_Name,CurrentRecord,SubjectName):
        self.GradeElements=GradeInputElement.objects.get(GradeRecord__Name=CurrentRecord,GradeSubject__Name=SubjectName)
        self.GroupName=Group_Name
        self.GradeRecord=CurrentRecord
        PupilList=GeneralFunctions.Get_PupilListExact(AcYear=AcademicYear,GroupName=Group_Name)
        self.GroupGrades=[]
        for Pupils in PupilList:
            self.GroupGrades.append([Pupils.id,PupilGrade(Pupils.id,CurrentRecord,SubjectName)])



def UpdatePupilGrade(request,PupilId,CurrentRecord,Subject):
    UpdateData={}
    UpdateData.update(request.POST)
    for items in UpdateData.keys():
        if not len(UpdateData[items][0]):
            del UpdateData[items]
        else:
            if UpdateData[items][0] == 'None':
                UpdateData[items] = ''
            else:
                UpdateData[items] = UpdateData[items][0]
    NewRecord=ExtendedRecord('Pupil', PupilId, request)
    NewRecord.ReplaceExtention(CurrentRecord,UpdateData,Subject)
    cache.delete('%s_%s_%s' %(CurrentRecord,Subject,PupilId))
    return

def UpdateMultiSubjects(request,PupilId,CurrentRecord):
    UpdateData={}
    for items in request.POST.keys():
        if ':' in items:
            Subject, field = items.split(':')
            if not Subject in UpdateData:
                UpdateData[Subject]={}
            if not request.POST[items] == 'None':
                UpdateData[Subject][field]=request.POST[items]
        NewRecord=ExtendedRecord(BaseType='Pupil',BaseId=PupilId)
        mainSubject=set()
        for subjects,data in UpdateData.items():
            pSubject=ParentSubjects.objects.filter(Subject__Name=subjects)
            if pSubject.exists():
                mainSubject.add(pSubject[0].ParentSubject.Name)
            NewRecord.WriteExtention(CurrentRecord,data,subjects)
            cache.delete('%s_%s_%s' %(CurrentRecord,subjects,PupilId))
    return list(mainSubject)[0]


class SelectGradeReports():
    ''' class to find a list of report records for grading. It holds a list
    of report records that are due for this time, plus a list of all the other
    grading reports '''
    def __init__(self,GroupName):
        self.GroupName=GroupName

    def SetCurrentSeason(self,GroupName):
        ''' Recusive function to find a the nearest Grade Report record to the name
        passed'''
        GroupItems=GroupName.split('.')
        GroupTest='.'.join(GroupItems[2:])
        Today=datetime.datetime.now().timetuple().tm_yday
        if ReportingSeason.objects.filter(GradeGroup__Name__icontains=GroupTest,GradeStartDay__lte=Today,GradeStopDay__gte=Today).exists():
            return ReportingSeason.objects.filter(GradeGroup__Name__icontains=GroupTest,GradeStartDay__lte=Today,GradeStopDay__gte=Today)[0]
        else:
            if len(GroupItems)>3:
                 return self.SetCurrentSeason('.'.join(GroupName.split('.'))[0:-1])
            else:
                return 'Not Found'

    def GetAllReports(self):
        ''' Returns a list of all reporting seasons'''
        return sorted(ReportingSeason.objects.all(), key=lambda i: i.GradeRecord.Name)

    def GetAllGradeRecords(self):
        ''' returns a list of Grade Input Elements'''
        return GradeInputElement.objects.all().order_by('GradeRecord','GradeSubject')

    def GetGradeSubjectsDictionary(self):
        ''' returns a dictionary of Subjects associated with a Grade Records '''
        GradeDic={}
        for Records in self.GetAllGradeRecords():
            if Records.GradeRecord.Name in GradeDic:
                GradeDic[Records.GradeRecord.Name].append(Records.GradeSubject.Name)
            else:
                GradeDic[Records.GradeRecord.Name]=[Records.GradeSubject.Name]
        return GradeDic

    def GetCurrentSeason(self):
        ''' Returns the report season record that is closest to the current date
        and the group name. If no match, then it will return false '''
        return self.SetCurrentSeason(self.GroupName)

class SingleGradeRecord():
    def __init__(self,ParentObject,Pupilid,GradeElement,ReportYear,ReportName,SubjectName=None):
        self.PupilId=Pupilid
        self.Parent=ParentObject
        self.ExtendedRecord=ExtendedRecord('Pupil',self.PupilId)
        self.GradeElement=GradeElement
        self.ReportYear=ReportYear
        self.ReportName=ReportName
        if SubjectName:
            self.SubjectName=SubjectName
        else:
            self.SubjectName=self.GradeElement.GradeSubject.Name
        self.PreviousRecord={}
        self.CurrentRecord={}
        self.Averages1=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('%s.%s.%s' % (self.Parent.Parent.PupilSchool(), self.ReportName, self.SubjectName), AcYear=self.ReportYear)
        if not self.Averages1 and self.GradeElement.GradeSubject.Name=='Religious_Education':
            self.Averages1=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('%s.%s.RE' % (self.Parent.Parent.PupilSchool(),self.ReportName),AcYear=self.ReportYear)

        if not self.Averages1==False:
            self.Averages=eval(self.Averages1)
        elif self.ReportName:
            globvars=GlobalVariablesFunctions.GlobalVariables('Yearly',self.ReportYear)
            globvars.create_SystemVar('%s.%s.%s' % (self.Parent.Parent.PupilSchool(),self.ReportName,self.GradeElement.GradeSubject.Name),'String')
            self.Averages={}
        if '_S' in self.ReportName:
            self.Term='Summer'
        elif '_M' in self.ReportName:
            self.Term='Mich'
        elif '_L' in self.ReportName:
            self.Term='Lent'
        else:
            self.Term='Year'
    def Subject(self):
        return self.SubjectName
    def GetTotalCommentLength(self):
        return self.Parent.TotalCommentLength
    def AddTotalCommentLength(self):
        self.Parent.TotalCommentLength+=len(self.Current()['Comment'][0])
        self.Parent.TotalCommentLength+=200
        return ''
    def ResetTotalCommentLength(self):
        self.Parent.TotalCommentLength=0
        return ''
    def TeacherName(self):
        try:
            return self.Parent.Parent.TeacherNames()[self.SubjectName]
        except:
            try:
                return self.Parent.Parent.TeacherNames()[self.GradeElement.GradeSubject.Name]
            except:
                return ''
    def Previous(self):
        if self.PreviousRecord:
            return self.PreviousRecord
        else:
            if self.GradeElement.PrevGradeRecord:
                PrevRec=cache.get('%s_%s_%s' % (self.GradeElement.PrevGradeRecord.Name,self.SubjectName,self.PupilId))
                if PrevRec:
                    self.PreviousRecord=PrevRec
                else:
                    self.PreviousRecord=self.ExtendedRecord.ReadExtention(self.GradeElement.PrevGradeRecord.Name,self.SubjectName)
                    cache.set('%s_%s_%s' % (self.GradeElement.PrevGradeRecord.Name,self.SubjectName,self.PupilId),self.PreviousRecord,3600)
            return self.PreviousRecord
    def Current(self):
        if self.CurrentRecord:
            return self.CurrentRecord
        else:
            CurrRec=cache.get('%s_%s_%s' % (self.GradeElement.GradeRecord.Name,self.SubjectName,self.PupilId))
            if CurrRec:
                self.CurrentRecord=CurrRec
            else:
                self.CurrentRecord=self.ExtendedRecord.ReadExtention(self.GradeElement.GradeRecord.Name,self.SubjectName)
                cache.set('%s_%s_%s' % (self.GradeElement.GradeRecord.Name,self.SubjectName,self.PupilId),self.CurrentRecord,3600)
            return self.CurrentRecord
    def Exam(self):
        self.Exam1=False
        self.Exam2=False
        self.Exam3=False
        if 'Exam1' in self.Current():
            if self.Current()['Exam1'][0]:
                try:
                    self.Exam1 = float(self.Current()['Exam1'][0])
                except:
                    pass
        if 'Exam2' in self.Current():
            if self.Current()['Exam2'][0]:
                try:
                    self.Exam2=float(self.Current()['Exam2'][0])
                except:
                    pass
        if 'Exam3' in self.Current():
            if self.Current()['Exam3'][0]:
                try:
                    self.Exam3=float(self.Current()['Exam3'][0])
                except:
                    pass
        if self.Exam1 and self.Exam2 and self.Exam3:
            ExamTotal = numpy.sum([self.Exam1,self.Exam2,self.Exam3])
            if self.SubjectName =='Mathematics':
                return numpy.around([ExamTotal/2.2],1)
            else:
                return numpy.around([ExamTotal/3],1)
        elif self.Exam1 and self.Exam2:
            return numpy.mean([self.Exam1,self.Exam2])
        elif self.Exam1:
            return self.Exam1
        else:
            return False

    def YearAverage(self):
        if self.Averages:
            if 'Year' in self.Averages:
                return self.Averages['Year']
            else:
                return False
        else:
            return False
    def Spread(self):
        if self.Averages:
            if 'Spread' in self.Averages:
                return self.Averages['Spread']
            else:
                return False
        else:
            return False

    def SetAverage(self):
        pupilset=self.__GetPupilSet__()
        if pupilset and self.Averages:
            if pupilset in self.Averages:
                return self.Averages[pupilset]
            else:
                return False
        else:
            return False

    def SetAverageExam1(self):
        pupilset=self.__GetPupilSet__()
        if pupilset and self.Averages:
            if "%sExam1" % pupilset in self.Averages:
                return self.Averages["%sExam1" % pupilset]
            else:
                return False
        else:
            return False

    def SetAverageExam2(self):
        pupilset=self.__GetPupilSet__()
        if pupilset and self.Averages:
            if "%sExam2" % pupilset in self.Averages:
                return self.Averages["%sExam2" % pupilset]
            else:
                return False
        else:
            return False

    def __GetPupilSet__(self):
        # A report Name is required to find a set Average and thus a set
        if not self.ReportName:
            return False
        if not self.Averages:
            return False
        setGroup=PupilGroup.objects.filter(Pupil__id=self.PupilId,
                                           Group__Name__icontains='%s.Set' % self.GradeElement.GradeSubject.Name,
                                           Active=True,AcademicYear=self.ReportYear)
        if setGroup.exists():
            return setGroup[0].Group.Name.split('.')[-1]
        else:
            SubSubjects=ParentSubjects.objects.filter(Subject__Name=self.GradeElement.GradeSubject.Name)
            if SubSubjects.exists():
                SetRecord=PupilGroup.objects.filter(Pupil__id=self.PupilId,
                                            Group__Name__icontains=SubSubjects[0].ParentSubject.Name,
                                            Active=True,AcademicYear=self.ReportYear)
                if SetRecord.exists():
                    return SetRecord[0].Group.Name.split('.')[-1]
                else:
                    return False
            elif self.GradeElement.GradeSubject.Name=='Religious_Education':
                setGroup=PupilGroup.objects.filter(Pupil__id=self.PupilId,
                                                   Group__Name__icontains='RE.Set',
                                                   Active=True,AcademicYear=self.ReportYear)
                if setGroup.exists():
                    return setGroup[0].Group.Name.split('.')[-1]
                else:
                    return False
    def __getChildSubjects__(self):
        childSubjects=ParentSubjects.objects.filter(ParentSubject=self.GradeElement.GradeSubject)
        if childSubjects.exists():
            return childSubjects
        else:
            return False

    def ChildSubjectLevelAv(self):
        subjects=self.__getChildSubjects__()
        returnLevel=False
        if subjects:
            total=0
            counter=0
            for subject in subjects:
                APS=self.Parent.Grades[subject.GradeSubject.Name].Current.Level[1]
                picklist=self.Parent.Grades[subject.GradeSubject.Name].Current.Level[2]
                try:
                    total+=int(APS)
                    counter+=1
                except:
                    pass
            if total > 0 and counter > 0:
                AvAPS=int(numpy.around([total/counter],0))
                for levels in picklist:
                    if int(levels[1])==AvAPS:
                        returnLevel=False
        return returnLevel

    def ChildSubjectEffortAv(self):
        subjects=self.__getChildSubjects__()
        returnEffort=False
        if subjects:
            total=0
            counter=0
            for subject in subjects:
                APS=self.Parent.Grades[subject.GradeSubject.Name].Current.Effort[1]
                picklist=self.Parent.Grades[subject.GradeSubject.Name].Current.Effort[2]
                try:
                    total+=int(APS)
                    counter+=1
                except:
                    pass
            if total > 0 and counter > 0:
                AvAPS=int(numpy.around([total/counter],0))
                for effort in picklist:
                    if int(effort[1])==AvAPS and not returnEffort:
                        returnEffort=effort[0]
        return returnEffort
    def ChildSubjectAttAv(self):
        subjects=self.__getChildSubjects__()
        returnAtt=False
        if subjects:
            total=0
            counter=0
            for subject in subjects:
                APS=self.Parent.Grades[subject.GradeSubject.Name].Current.Attainment[1]
                try:
                    total+=int(APS)
                    counter+=1
                except:
                    pass
            if total > 0 and counter > 0:
                returnAtt=int(numpy.around([total/counter],0))
        return returnAtt
    def ChildSubjectExamAv(self):
        subjects=self.__getChildSubjects__()
        returnExam=False
        if subjects:
            total=0
            counter=0
            for subject in subjects:
                APS=self.Parent.Grades[subject.GradeSubject.Name].Exam
                try:
                    total+=int(APS)
                    counter+=1
                except:
                    pass
            if total > 0 and counter > 0:
                returnExam=int(numpy.around([total/counter],0))
        return returnExam


class GradeRecordObject():
    def __init__(self,ParentObject,PupilId,gradeRecord,ReportYear=None):
        self.Parent=ParentObject
        self.ReportYear=ReportYear
        if not ReportYear:
            self.ReportYear=GlobalVariablesFunctions.Get_SystemVar('CurrentYear')
        self.PupilId=PupilId
        self.GradeRecord=gradeRecord
        self.GradeData={}
        self.CatData={}
        self.TotalCommentLength=0

    def __AddTotalCommentLength__(self,commentlength):
        self.TotalCommentLength+=commentlength
    def ResetTotalCommentLength(self):
        self.TotalCommentLength=0
    def Grades(self):
        if not self.GradeData:
            GradeInputElements=GradeInputElement.objects.filter(GradeRecord__Name=self.GradeRecord)
            for inputElement in GradeInputElements:
                self.GradeData[inputElement.GradeSubject.Name]=SingleGradeRecord(self,self.PupilId,inputElement,self.ReportYear,self.GradeRecord)
                ChildSubjects=ParentSubjects.objects.filter(ParentSubject=inputElement.GradeSubject)
                if ChildSubjects.exists():
                    for subjects in ChildSubjects:
                        self.GradeData[subjects.Subject.Name]=SingleGradeRecord(self,self.PupilId,
                                                                                inputElement,
                                                                                self.ReportYear,self.GradeRecord,
                                                                                subjects.Subject.Name)
        return self.GradeData
    def CatGrades(self):
        if not self.CatData:
            CatRec='%s_Ca' % self.GradeRecord.split('_')[0]
            ExRecord=ExtendedRecord('Pupil',self.PupilId)
            self.CatData=ExRecord.ReadExtention(CatRec,'Form_Comment')


class PupilGradeRecord(PupilAttendanceRec):
    def __init__(self,PupilId,AcYear,ReportName=None):
        PupilAttendanceRec.__init__(self,PupilId,AcYear)
        self.ReportName=ReportName
        self.ReportYear=None
        self.ReportAcYear=None
        self.pupilschool=None
        self.__getPupilYear__()
        self.TeacherList={}
        if self.ReportName:
            self.__getReportYear__()
            self.__ReportAcYear__()
        self.ReportAcYearPart=self.ReportAcYear.split('-')
        self.Grades={}
        GradeInputElements=GradeInputElement.objects.all()
        for recs in GradeInputElements:
            if not recs.GradeRecord.Name in self.Grades:
                self.Grades[recs.GradeRecord.Name]=GradeRecordObject(self,self.PupilId,recs.GradeRecord.Name,self.ReportAcYear)

    def __getReportYear__(self):
        if self.ReportName:
            if 'Yr' in self.ReportName:
                if 'YrR' in self.ReportName:
                    self.ReportYear='Reception'
                else:
                    self.ReportYear=int(self.ReportName.split('Yr',1)[1].split('_',1)[0])

    def __getPupilYear__(self):
        YearGroups=PupilGroup.objects.filter(Pupil__id=self.PupilId,AcademicYear=self.AcYear, Group__Name__icontains='year', Active=True).filter(Group__Name__icontains='Form')
        if YearGroups.exists():
            YearGroup=YearGroups[0]
            self.PupilYear=int(YearGroup.Group.Name.split('Year',1)[1].split('.',1)[0])
        elif PupilGroup.objects.filter(Pupil__id=self.PupilId,AcademicYear=self.AcYear, Group__Name__icontains='Reception').exists():
            self.PupilYear='Reception'
        return self.PupilYear
    def __ReportAcYear__(self):
        self.ReportAcYear=None
        if self.PupilYear=='Reception':
            self.ReportAcYear=self.AcYear
        else:
            pupilyear=self.PupilYear
            years=self.AcYear.split('-')
            if self.ReportYear=='Reception':
                yearsDiff=pupilyear
            else:
                yearsDiff=pupilyear-self.ReportYear
            self.ReportAcYear='%d-%d' % (int(years[0])-yearsDiff,int(years[1])-yearsDiff)
    def HeaderReportYear(self):
        if 'Mich' in self.ReportTerm():
            return self.ReportAcYear.split('-')[0]
        else:
            return self.ReportAcYear.split('-')[1]
    def PupilSchool(self):
        if not self.pupilschool:
            form=PupilGroup.objects.filter(Pupil__id=self.PupilId,Group__Name__icontains='Form',Active=True,AcademicYear=self.ReportAcYear).filter(Group__Name__istartswith='Current')
            if form.exists():
                self.pupilschool=form[0].Group.Name.split('.')[1]
        return self.pupilschool
    def Form(self):
        if not self.PupilForm:
            form=PupilGroup.objects.filter(Pupil__id=self.PupilId,Group__Name__icontains='Form',Active=True,AcademicYear=self.ReportAcYear).filter(Group__Name__istartswith='Current')
            if form.exists():
                for recs in form:
                    if recs.Group.Name.split('.')[-2]=='Form':
                        self.PupilForm=form[0].Group.Name.split('.')[-1]
        return self.PupilForm
    def ReportTitle(self):
        if self.ReportName:
            if 'Eot' in self.ReportName or 'Hf' in self.ReportName:
                reportdetails=self.ReportName.split('_')
                reportYear=reportdetails[0].split('Yr')[1]
                if len(reportdetails) > 2:
                    if reportdetails[2] =='M':
                        term='Michaelmas'
                    elif reportdetails[2] =='L':
                        term='Lent'
                    else:
                        term='Summer'
                else:
                    term='N/A'
            if 'Eot' in self.ReportName:
                return 'Year %s %s Term Report' % (reportYear, term)
            elif 'Hf' in self.ReportName:
                return 'Year %s %s Half Term Report' % (reportYear, term)
            else:
                'undetermined report title'
    def ReportTerm(self):
        if self.ReportName:
            reportdetails=self.ReportName.split('_')
            if len(reportdetails) > 2:
                if reportdetails[2] =='M':
                    term='Michaelmas'
                elif reportdetails[2] =='L':
                    term='Lent'
                else:
                    term='Summer'
            else:
                term='N/A'
            return term

    def TeacherNames(self):
        ''' This gets a list of All teachers assigned to a child groups. It works on the
        basis of getting the Teachers assigned to the form groups first, then searches
        the other groups a child belongs to. These other groups will override the teacher
        set in the form class'''
        if not self.TeacherList:
            FormList=PupilGroup.objects.filter(Group__Name__icontains='form.',Pupil__id=self.PupilId,AcademicYear=self.ReportAcYear,Active=True)
            for groups in FormList:
                names=GlobalVariablesFunctions.TeacherNames(groups.Group.Name,self.ReportAcYear)
                self.TeacherList.update(names.SubjectTeachers())
                if names.FormTeacher():
                    self.TeacherList['FormTeacher']=names.FormTeacher()
                if names.AllTeachers():
                    self.TeacherList['AllTeachers']=names.AllTeachers()
            SetList=PupilGroup.objects.filter(Group__Name__icontains='Set',Pupil__id=self.PupilId,AcademicYear=self.ReportAcYear,Active=True)
            for groups in SetList:
                names=GlobalVariablesFunctions.TeacherNames(groups.Group.Name,self.ReportAcYear)
                self.TeacherList.update(names.SubjectTeachers())
            SetList=PupilGroup.objects.filter(Group__Name__icontains='Games',Pupil__id=self.PupilId,AcademicYear=self.ReportAcYear,Active=True)
            for groups in SetList:
                names=GlobalVariablesFunctions.TeacherNames(groups.Group.Name,self.ReportAcYear)
                self.TeacherList.update(names.SubjectTeachers())

            SetList=PupilGroup.objects.filter(Group__Name__icontains='TutorGroup',Pupil__id=self.PupilId,AcademicYear=self.ReportAcYear,Active=True)
            for groups in SetList:
                names=GlobalVariablesFunctions.TeacherNames(groups.Group.Name,self.ReportAcYear)
                self.TeacherList['Tutor']=names.Tutor()
        return self.TeacherList






