from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from MIS.modules import GeneralFunctions
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from MIS.modules.ExtendedRecords import *
from MIS.modules.Extracts import ExtendedRecordExtract
import urllib2
import threading
import Queue
import gspread
import simplejson
from django.core.mail import send_mail, EmailMessage
import logging
log = logging.getLogger(__name__)

class AgeCensus():
    def __init__(self,SchoolGroup):
        self.AcYear=GlobalVariables.Get_SystemVar('CurrentYear')
        self.Year=int(self.AcYear.split('-')[0])
        self.PupilList=Pupil.objects.filter(id__in=[x.id for x in GeneralFunctions.Get_PupilList(self.AcYear,SchoolGroup)])
    def GetCounts(self):
        ReturnData=[] 
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-15),'%d-08-31' % (self.Year-14)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-14),'%d-08-31' % (self.Year-13)))        
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-13),'%d-08-31' % (self.Year-12)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-12),'%d-08-31' % (self.Year-11)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-11),'%d-08-31' % (self.Year-10)))  
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-10),'%d-08-31' % (self.Year-9)))        
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-9),'%d-08-31' % (self.Year-8)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-8),'%d-08-31' % (self.Year-7)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-7),'%d-08-31' % (self.Year-6)))        
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-6),'%d-08-31' % (self.Year-5)))         
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-5),'%d-12-31' % (self.Year-5)))
        ReturnData.append(self.DataCounts('%d-01-01' % (self.Year-4),'%d-03-31' % (self.Year-4)))
        ReturnData.append(self.DataCounts('%d-04-01' % (self.Year-4),'%d-08-31' % (self.Year-4)))         
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-4),'%d-12-31' % (self.Year-4)))
        ReturnData.append(self.DataCounts('%d-01-01' % (self.Year-3),'%d-03-31' % (self.Year-3)))        
        ReturnData.append(self.DataCounts('%d-04-01' % (self.Year-3),'%d-08-31' % (self.Year-3)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-3),'%d-08-31' % (self.Year-2))) 
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-2),'%d-03-31' % (self.Year-1))) 
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-1),'%d-08-31' % (self.Year))) 
        return ReturnData
    def DataCounts(self,Start,Stop):
        Data={}
        Data['Total']=self.PupilList.filter(DateOfBirth__gte=Start,DateOfBirth__lte=Stop).count()
        Data['Boys']=self.PupilList.filter(DateOfBirth__gte=Start,DateOfBirth__lte=Stop,Gender='M').count()
        Data['Girls']=self.PupilList.filter(DateOfBirth__gte=Start,DateOfBirth__lte=Stop,Gender='F').count()
        return {'%s-%s' % (Start,Stop):Data}

class EYFS_Map():
    def __init__(self,GroupName,AcYear,EYFSArea):
        self.pupillist=set([x.Pupil for x in PupilGroup.objects.filter(Group__Name__istartswith=GroupName,AcademicYear=AcYear,Active=True)])
        self.AcYear=AcYear
        self.EYFSArea=EYFSArea
        self.GroupName=GroupName
        self.OnEntryData={'3050':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},        
                   '4060':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                    'ELG':{'Emerging':{'Pupils':[],'Percentage':0},'Expected':{'Pupils':[],'Percentage':0},'Exceeded':{'Pupils':[],'Percentage':0}}}
                    
        self.MichData={'3050':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},        
                   '4060':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                    'ELG':{'Emerging':{'Pupils':[],'Percentage':0},'Expected':{'Pupils':[],'Percentage':0},'Exceeded':{'Pupils':[],'Percentage':0}}}
                    
        self.LentData={'3050':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},        
                   '4060':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                    'ELG':{'Emerging':{'Pupils':[],'Percentage':0},'Expected':{'Pupils':[],'Percentage':0},'Exceeded':{'Pupils':[],'Percentage':0}}}
                    
        self.SummerData={'3050':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},        
                   '4060':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                    'ELG':{'Emerging':{'Pupils':[],'Percentage':0},'Expected':{'Pupils':[],'Percentage':0},'Exceeded':{'Pupils':[],'Percentage':0}}}
        self.__PopulateGrid__()
        self.__SetPercentages__()
    def __PopulateGrid__(self):
        for pupils in self.pupillist:
            eyfsrec=ExtendedRecord('Pupil',pupils.id)
            OnEntry=eyfsrec.ReadExtention('EYFS_OnEntry','Form_Comment')[self.EYFSArea][0].replace('-','').split(':')

            print OnEntry
            MichTerm=eyfsrec.ReadExtention('EYFS_Mich','Form_Comment')[self.EYFSArea][0].replace('-','').split(':')
            print OnEntry
            LentTerm=eyfsrec.ReadExtention('EYFS_Lent','Form_Comment')[self.EYFSArea][0].replace('-','').split(':')
            print OnEntry
            SummerTerm=eyfsrec.ReadExtention('EYFS_Summer','Form_Comment')[self.EYFSArea][0].replace('-','').split(':')

 
            if len(OnEntry) > 1:
                if OnEntry[1]=='beginning':
                    OnEntry[1]='Beginning'
                self.OnEntryData[OnEntry[0]][OnEntry[1]]['Pupils'].append(pupils.FullName())
            if len(MichTerm) > 1:
                if MichTerm[1]=='beginning':
                    MichTerm[1]='Beginning'
                self.MichData[MichTerm[0]][MichTerm[1]]['Pupils'].append(pupils.FullName())
            if len(LentTerm) > 1:
                if LentTerm[1]=='beginning':
                    LentTerm[1]='Beginning'
                self.LentData[LentTerm[0]][LentTerm[1]]['Pupils'].append(pupils.FullName())
            if len(SummerTerm) > 1:
                if SummerTerm[1]=='beginning':
                    SummerTerm[1]='Beginning'
                self.SummerData[SummerTerm[0]][SummerTerm[1]]['Pupils'].append(pupils.FullName())
    def __SetPercentages__(self):
        PupilCount=len(self.pupillist)

        for area1 in ['3050','4060']:
            for area2 in ['Beginning','Development','Secure']:
                if len(self.OnEntryData[area1][area2]['Pupils']) > 0:
                    self.OnEntryData[area1][area2]['Percentage']=float(len(self.OnEntryData[area1][area2]['Pupils']))/PupilCount * 100

                if len(self.MichData[area1][area2]['Pupils']) > 0:
                    self.MichData[area1][area2]['Percentage']=float(len(self.MichData[area1][area2]['Pupils']))/PupilCount * 100
            
                if len(self.LentData[area1][area2]['Pupils']) > 0:
                    self.LentData[area1][area2]['Percentage']=float(len(self.LentData[area1][area2]['Pupils']))/PupilCount * 100
            
                if len(self.SummerData[area1][area2]['Pupils']) > 0:
                    self.SummerData[area1][area2]['Percentage']=float(len(self.SummerData[area1][area2]['Pupils']))/PupilCount * 100

        for area in ['Emerging','Expected','Exceeded']:
            if len(self.OnEntryData['ELG'][area]['Pupils']) > 0:
                self.OnEntryData['ELG'][area]['Percentage']=float(len(self.OnEntryData['ELG'][area]['Pupils']))/PupilCount * 100
            if len(self.MichData['ELG'][area]['Pupils']) > 0:
                self.MichData['ELG'][area]['Percentage']=float(len(self.MichData['ELG'][area]['Pupils']))/PupilCount * 100
            if len(self.LentData['ELG'][area]['Pupils']) > 0:
                self.LentData['ELG'][area]['Percentage']=float(len(self.LentData['ELG'][area]['Pupils']))/PupilCount * 100
            if len(self.SummerData['ELG'][area]['Pupils']) > 0:
                self.SummerData['ELG'][area]['Percentage']=float(len(self.SummerData['ELG'][area]['Pupils']))/PupilCount * 100


class LevelsReview():
    def __init__(self,AcYear,GroupName,NoYears,SubjectName):
        self.AcYear=AcYear
        self.GroupName=GroupName
        self.NoYears=NoYears
        self.SubjectName=SubjectName
        self.ClassList=self.__GetClasses__()
        self.RecordList=self.__GetRecordList__()
        self.LevelNames=self.__LevelNames__()
    def __GetClasses__(self): 
        if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % self.SubjectName).exists():
            classlist=[x.MenuName for x in Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % self.SubjectName)]
        elif  ParentSubjects.objects.filter(Subject__Name=self.SubjectName).exists():
            parent=ParentSubjects.objects.filter(Subject__Name=self.SubjectName)[0].ParentSubject.Name
            if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % parent).exists():
                classlist=[x.MenuName for x in Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % parent)]
            else:
                if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames').exists():
                    classlist=[x.MenuName for x in Group.objects.filter(Name=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames')]
        else:
            if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames').exists():
                classlist=[x.MenuName for x in Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames')]             
        return classlist
    def __GetRecordList__(self):
        if 'Year1' in self.GroupName:
            return ['Yr1_Eot_M','Yr1_Eot_L','Yr1_Eot_S']
        elif 'Year2' in self.GroupName:
            return ['Yr1_Eot_M','Yr1_Eot_L','Yr1_Eot_S',
            'Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S']
        elif 'Year3' in self.GroupName:
            if self.NoYears==2:
                return ['Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S',
                        'Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S']
            else:
                return ['Yr1_Eot_M','Yr1_Eot_L','Yr1_Eot_S',
                        'Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S',
                        'Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S']
        elif 'Year4' in self.GroupName:
            if self.NoYears==2:
                return ['Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S',
                        'Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S']
            elif self.NoYears==5 or self.NoYears==8:
                return ['Yr1_Eot_M','Yr1_Eot_L','Yr1_Eot_S',
                        'Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S',
                        'Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S',              
                        'Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S']
            else:
                return ['Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S',
                        'Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S',              
                        'Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S']                
        elif 'Year5' in self.GroupName:
            if self.NoYears==2:
                return ['Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S',
                        'Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S']
            elif self.NoYears==5 or self.NoYears==8:
                return ['Yr1_Eot_M','Yr1_Eot_L','Yr1_Eot_S',
                        'Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S',
                        'Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S',              
                        'Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S',
                        'Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S']
            else:
                return ['Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S',
                        'Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S',              
                        'Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S']              
        elif 'Year6' in self.GroupName:
            if self.NoYears==2:
                return ['Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S',
                        'Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S']
            elif self.NoYears==8:
                return ['Yr1_Eot_M','Yr1_Eot_L','Yr1_Eot_S',
                        'Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S',
                        'Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S',              
                        'Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S',
                        'Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S',
                        'Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S']
            if self.NoYears==5:
                return ['Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S',
                        'Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S',              
                        'Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S',
                        'Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S',
                        'Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S']
            else:
                return ['Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S',
                        'Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S',              
                        'Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S']
        elif 'Year7' in self.GroupName:
            if self.NoYears==2:
                return ['Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S',
                        'Yr7_Eot_M','Yr7_Eot_L','Yr7_Eot_S']
            elif self.NoYears==8:
                return ['Yr1_Eot_M','Yr1_Eot_L','Yr1_Eot_S',
                        'Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S',
                        'Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S',              
                        'Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S',
                        'Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S',
                        'Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S',
                        'Yr7_Eot_M','Yr7_Eot_L','Yr7_Eot_S']                        
            elif self.NoYears==5:
                return ['Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S',              
                        'Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S',
                        'Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S',
                        'Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S',
                        'Yr7_Eot_M','Yr7_Eot_L','Yr7_Eot_S']  
            else:
                return ['Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S',              
                        'Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S',
                        'Yr7_Eot_M','Yr7_Eot_L','Yr7_Eot_S']  
        elif 'Year8' in self.GroupName:
            if self.NoYears==2:
                return ['Yr7_Eot_M','Yr7_Eot_L','Yr7_Eot_S',
                        'Yr8_Eot_M','Yr8_Eot_L','Yr8_Eot_S']
            elif self.NoYears==8:
                return ['Yr1_Eot_M','Yr1_Eot_L','Yr1_Eot_S',
                        'Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S',
                        'Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S',              
                        'Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S',
                        'Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S',
                        'Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S',
                        'Yr7_Eot_M','Yr7_Eot_L','Yr7_Eot_S',
                        'Yr8_Eot_M','Yr8_Eot_L','Yr8_Eot_S']                          
            elif self.NoYears==5:
                return ['Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S',
                        'Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S',
                        'Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S',
                        'Yr7_Eot_M','Yr7_Eot_L','Yr7_Eot_S',
                        'Yr8_Eot_M','Yr8_Eot_L','Yr8_Eot_S'] 
            else:
                return ['Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S',
                        'Yr7_Eot_M','Yr7_Eot_L','Yr7_Eot_S',
                        'Yr8_Eot_M','Yr8_Eot_L','Yr8_Eot_S'] 
                        
                        
    def __LevelNames__(self):
        namelist=[]
        for names in self.RecordList:
            year=names.split('_')[0]
            term=''
            if 'M' in names:
                term='Mich'
            elif 'L' in names:
                term='Lent'
            else:
                term='Summer'
            namelist.append('%s %s' % (term,year))
        return namelist
    def __PupilInClass__(self,pupilrec):
        for classes in self.ClassList:
            if PupilGroup.objects.filter(Pupil=pupilrec,Active=True,Group__Name__iendswith=classes).exists():
                return classes
        return ''
    def __BolCon__(self,Value):
        if Value:
            return 'T'
        else:
            return 'F'
    def JSONData(self):
        returnlist=[]
        for pupils in GeneralFunctions.Get_PupilList(self.AcYear,self.GroupName):
            print pupils
            rec=ExtendedRecordExtract.GradeAnalysisExtract(pupils.id,self.AcYear)
            pupilrec= {'Fullname':rec.Pupil.FullName(),
            'Class':self.__PupilInClass__(pupils),
            'Gender':rec.Pupil.Gender,
            'SEN':self.__BolCon__(rec.IsSEN()),
            'EAL':self.__BolCon__(rec.IsEAL()),
            'G&T':self.__BolCon__(rec.IsGiftedAndTalented()),
            'MoreAble':self.__BolCon__(rec.IsMoreAble()),
            'SummBirth':self.__BolCon__(rec.IsSumBirth()),
            'Level':[]}
            for extrecs in self.RecordList:
                pupilrec['Level'].append({extrecs:rec.GradeRecords[extrecs][self.SubjectName].Grade()['Level'][0]})
            returnlist.append(pupilrec)
        return simplejson.dumps(returnlist)
            
            



class BatterseaYear3(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
        YearSheet=self.GoogleLink.worksheet('Year 3')
        counter=10
        GradeRec='Yr3_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English_Reading'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['English_Writing'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['English_Speaking'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Level'][0])
            except:
                pass
            counter+=1
        log.warn('Year3 done')
        return


       
class BatterseaYear4(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
        YearSheet=self.GoogleLink.worksheet('Year 4')
        counter=10
        GradeRec='Yr4_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English_Reading'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['English_Writing'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['English_Speaking'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Level'][0])
            except:
                pass
            counter+=1  
        log.warn('Year4 done')
        
        
class BatterseaYear5(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
        YearSheet=self.GoogleLink.worksheet('Year 5')
        counter=6
        GradeRec='Yr5_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English_Reading'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ') 
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['English_Writing'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['English_Speaking'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ') 
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('P%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Level'][0])
            except:
                pass
            counter+=1 
        log.warn('Year5 done') 

class BatterseaYear611(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
        YearSheet=self.GoogleLink.worksheet('Year 611')
        counter=10
        GradeRec='Yr6_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ') 
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1              
class BatterseaYear613(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
        YearSheet=self.GoogleLink.worksheet('Year 613')
        counter=10
        GradeRec='Yr6_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ') 
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1  

class BatterseaYear7(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
        YearSheet=self.GoogleLink.worksheet('Year 7')
        counter=10
        GradeRec='Yr7_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam3'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Biology'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Chemistry'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Physics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('P%d' % counter,' ')
                YearSheet.update_acell('P%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('Q%d' % counter,' ')
                YearSheet.update_acell('Q%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('R%d' % counter,' ')
                YearSheet.update_acell('R%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('S%d' % counter,' ')
                YearSheet.update_acell('S%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('T%d' % counter,' ')
                YearSheet.update_acell('T%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('U%d' % counter,' ')
                YearSheet.update_acell('U%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1
class BatterseaYear8(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
            
        YearSheet=self.GoogleLink.worksheet('Year 8')
        counter=10
        GradeRec='Yr8_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam3'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Biology'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Chemistry'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Physics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('P%d' % counter,' ')
                YearSheet.update_acell('P%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('Q%d' % counter,' ')
                YearSheet.update_acell('Q%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('R%d' % counter,' ')
                YearSheet.update_acell('R%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('S%d' % counter,' ')
                YearSheet.update_acell('S%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('T%d' % counter,' ')
                YearSheet.update_acell('T%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('U%',' ') 
                YearSheet.update_acell('U%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1            
class ExamDataExtract(threading.Thread):
    def __init__(self,AcYear,school,Term, StaffId):
        threading.Thread.__init__(self)
        self.AcYear=AcYear
        self.school=school
        self.StaffId=StaffId
        self.FullTerm=Term
        if 'Mich' in Term:
            self.Term='M'
        elif 'Lent' in Term:
            self.Term='L'
        else:
            self.Term='S'

        log.warn('Exam email init completed')
    def run(self):
        if 'Battersea' in self.school:
            self.Battersea()
        elif 'Clapham' in self.school:
            self.Clapham()
        elif 'Fulham' in self.school:
            self.Fulham()
        elif 'Kensington' in self.school:
            self.Kensington
    def Clapham(self):
        log.warn('Battersea method Triggered')
        # Create dictionary for data
    
        returnData={'Year 5':[],'Year 611':[],'Year 613':[],'Year 7':[],'Year 8':[]}
        log.warn('returnData Created')
        # for each year group get a list of pupils and create the grade object
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Clapham.MiddleSchool.Year5'):
            returnData['Year 5'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Clapham.UpperSchool.Year6.Yr6_Sets.11Plus'):
            returnData['Year 611'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Clapham.UpperSchool.Year6.Yr6_Sets.13Plus'):
            returnData['Year 613'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))        
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Clapham.UpperSchool.Year7'):
            returnData['Year 7'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Clapham.UpperSchool.Year8'):
            returnData['Year 8'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        log.warn('GradeData objects created')
        # link to the Google acccout
        self.GoogleClient=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook=self.GoogleClient.open_by_key('0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE')
        
        log.warn('connection to google open')
        
        YearSheet=self.GoogleWorkBook.worksheet('Year 5')
        counter=6
        GradeRec='Yr5_Eot_%s' % self.Term
        for records in returnData['Year 5']:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ') 
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1 
        log.warn('Year5 done')            
        
        YearSheet=self.GoogleWorkBook.worksheet('Year 611')
        counter=6
        GradeRec='Yr6_Eot_%s' % self.Term
        for records in returnData['Year 611']:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ') 
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1 
        log.warn('Year611 done')           
        
        YearSheet=self.GoogleWorkBook.worksheet('Year 613')
        counter=6
        GradeRec='Yr6_Eot_%s' % self.Term
        for records in returnData['Year 613']:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ') 
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1 
        log.warn('Year613 done')           
        
        YearSheet=self.GoogleWorkBook.worksheet('Year 7')
        counter=6
        GradeRec='Yr7_Eot_%s' % self.Term
        for records in returnData['Year 7']:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ') 
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Biology'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Chemistry'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Physics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('P%d' % counter,' ')
                YearSheet.update_acell('P%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('Q%d' % counter,' ')
                YearSheet.update_acell('Q%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1 
        log.warn('Year7 done')           
        
        YearSheet=self.GoogleWorkBook.worksheet('Year 8')
        counter=6
        GradeRec='Yr8_Eot_%s' % self.Term
        for records in returnData['Year 8']:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ') 
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ') 
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Biology'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Chemistry'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Physics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('P%d' % counter,' ')
                YearSheet.update_acell('P%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('Q%d' % counter,' ')
                YearSheet.update_acell('Q%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('R%d' % counter,' ')
                YearSheet.update_acell('R%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('S%d' % counter,' ')
                YearSheet.update_acell('S%d' % counter, records.GradeRecords[GradeRec]['Greek'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1 
        log.warn('Year8 done')           
        
        
        log.warn('sending Email')           
        url_format='https://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=0Av-_dbDrUv-KdDE4SzZKTExTbTZwYzR4bUhFWi1FbHc&exportFormat=xls'
        Header='Exam data for %s %s' % (self.FullTerm,self.AcYear)
        BodyText='Dear %s \nPlease find attached your requested Exam data for %s %s \n\nWillowTree' % (self.StaffId.Forename,self.FullTerm,self.AcYear)
        filename='battersea_%s_%s.xls' % (self.FullTerm,self.AcYear)
        self.__SendEmail__(Header,BodyText,url_format,filename)       
        
                
        
                
        
    def Fulham(self):
        log.warn('Fulham method Triggered')
        # Create dictionary for data
        Sheet_Key='0Av-_dbDrUv-KdE14MUFBSGtQZzl4QWpzZS1Fel84M3c'
        returnData={'Year 5':[],'Year 6':[]}
        log.warn('returnData Created')
        # for each year group get a list of pupils and create the grade object
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Fulham.PrepSchool.Year5'):
            returnData['Year 5'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Fulham.PrepSchool.Year6'):
            returnData['Year 6'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        log.warn('GradeData objects created')
        # link to the Google acccout
        self.GoogleClient=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook=self.GoogleClient.open_by_key(Sheet_Key)
        
        log.warn('connection to google open')
        
        thread3=FulhamYear5(returnData['Year 5'],self.GoogleWorkBook,self.Term)
        thread3.start()
        thread4=FulhamYear6(returnData['Year 6'],self.GoogleWorkBook,self.Term)
        thread4.start()        
        
        log.warn('sending Email')           
        url_format='https://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE&exportFormat=xls'
        Header='Exam data for %s %s' % (self.FullTerm,self.AcYear)
        BodyText='Dear %s \nPlease find attached your requested Exam data for %s %s \n\nWillowTree' % (self.StaffId.Forename,self.FullTerm,self.AcYear)
        filename='Clapham_%s_%s.xls' % (self.FullTerm,self.AcYear)
        self.__SendEmail__(Header,BodyText,url_format,filename)
    def Kensington(self):
        pass
    def Battersea(self):
        log.warn('Battersea method Triggered')
        # Create dictionary for data
    
        returnData={'Year 3':[],'Year 4':[],'Year 5':[],'Year 611':[],'Year 613':[],'Year 7':[],'Year 8':[]}
        log.warn('returnData Created')
        # for each year group get a list of pupils and create the grade object
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Battersea.MiddleSchool.Year3'):
            returnData['Year 3'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        log.warn('Year3 created')
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Battersea.MiddleSchool.Year4'):
            returnData['Year 4'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        log.warn('Year 4 crewated')
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Battersea.MiddleSchool.Year5'):
            returnData['Year 5'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Battersea.UpperSchool.Year6.Yr6_Sets.11Plus'):
            returnData['Year 611'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Battersea.UpperSchool.Year6.Yr6_Sets.13Plus'):
            returnData['Year 613'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))        
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Battersea.UpperSchool.Year7'):
            returnData['Year 7'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Battersea.UpperSchool.Year8'):
            returnData['Year 8'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        log.warn('GradeData objects created')
        # link to the Google acccout
        self.GoogleClient=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook=self.GoogleClient.open_by_key('0Av-_dbDrUv-KdDE4SzZKTExTbTZwYzR4bUhFWi1FbHc')
        
        log.warn('connection to google open')
        
        thread1=BatterseaYear3(returnData['Year 3'],self.GoogleWorkBook,self.Term)
        thread1.start()
        thread2=BatterseaYear4(returnData['Year 4'],self.GoogleWorkBook,self.Term)
        thread2.start()
        thread3=BatterseaYear5(returnData['Year 5'],self.GoogleWorkBook,self.Term)
        thread3.start()
        thread4=BatterseaYear611(returnData['Year 611'],self.GoogleWorkBook,self.Term)
        thread4.start()        
        thread5=BatterseaYear613(returnData['Year 613'],self.GoogleWorkBook,self.Term)
        thread5.start()
        thread6=BatterseaYear7(returnData['Year 7'],self.GoogleWorkBook,self.Term)
        thread6.start()  
        thread7=BatterseaYear8(returnData['Year 8'],self.GoogleWorkBook,self.Term)
        thread7.start()
        
        log.warn('sending Email')           
        url_format='https://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE&exportFormat=xls'
        Header='Exam data for %s %s' % (self.FullTerm,self.AcYear)
        BodyText='Dear %s \nPlease find attached your requested Exam data for %s %s \n\nWillowTree' % (self.StaffId.Forename,self.FullTerm,self.AcYear)
        filename='Clapham_%s_%s.xls' % (self.FullTerm,self.AcYear)
        self.__SendEmail__(Header,BodyText,url_format,filename)
           
    def __SendEmail__(self,Header, Text ,urlLink, filename):
        log.warn('SendEmail reached')
        if self.StaffId.EmailAddress:
            req= urllib2.Request(urlLink,headers=self.GoogleClient.session.headers)
            SenderEmail=self.StaffId.EmailAddress
            print SenderEmail
            msg=EmailMessage(Header,
                             'Please find your requested file attached \n WillowTree',
                             SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data,
                             [SenderEmail])
            downloadfile=urllib2.urlopen(req)
  
            
            print downloadfile
            msg.attach(filename,downloadfile.read(),'application/vnd.ms-excel')
            msg.send()
            log.warn('Email sent')
            

class FulhamYear5(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
        YearSheet=self.GoogleLink.worksheet('Year 5')
        counter=10
        GradeRec='Yr5_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1
        log.warn('Year3 done')
        return


class FulhamYear6(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
        YearSheet=self.GoogleLink.worksheet('Year 6')
        counter=10
        GradeRec='Yr6_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1
        log.warn('Year3 done')
        return

def APS_Average(GroupName,AcYear,GradeRec,SubjectName):
    Counter = 0
    Total = 0
    try:
        pupillist=GeneralFunctions.Get_PupilList(AcYear,GroupName)
    except:
        return False       
    for pupils in pupillist:
        GradeExt=ExtendedRecord('Pupil',pupils.id)
        GradeItem=GradeExt.ReadExtention(GradeRec,SubjectName)
        if GradeItem['Level'][1]:
            try:
                Total+=int(GradeItem['Level'][1])
                Counter+=1
            except:
                pass
    if Counter > 0 and Total > 0:
        cache.set('%s-%s-s%s' % (GradeRec,SubjectName,AcYear),(Total/Counter),3600)
        return (float(Total)/float(Counter))
    else:
        return False
        
def APSProgress(GroupName,AcYear,PreviousRecord,CurrentRecord,SubjectName):
    print PreviousRecord
    print CurrentRecord
    PreviousAPS=cache.get('%s-%s-%s' % (PreviousRecord,SubjectName,AcYear),False)
    if not PreviousAPS:
        PreviousAPS=APS_Average(GroupName,AcYear,PreviousRecord,SubjectName)
    CurrentAPS=cache.get('%s-%s-%s' % (CurrentRecord,SubjectName,AcYear),False)
    if not CurrentAPS:
        CurrentAPS=APS_Average(GroupName,AcYear,CurrentRecord,SubjectName)
    if CurrentAPS and PreviousAPS:
        print CurrentAPS
        print PreviousAPS
        return '%3.0f (%3.0f)' % (CurrentAPS-PreviousAPS,CurrentAPS)
    else:
        print CurrentAPS
        print PreviousAPS
        return 'Calc Error'
