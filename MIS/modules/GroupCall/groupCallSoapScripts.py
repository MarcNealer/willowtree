"""
Created on Tue Sep 24 14:37:51 2013

@author: roy
"""

# urllib2 imports
import urllib2


def urllib2Test():
    soapXml = """
<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/"><s:Body xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"><dataChange xmlns="http://alert.groupcall.com/"><crud><APIKey>7853db4d-4892-4bdd-bbe6-b83de1f5b9ea905a</APIKey><type>Add</type><contacts><Contact><ID>C1</ID><forename>Test</forename><surname>Contact</surname><name>Test Contact</name><contacttype>Student</contacttype></Contact></contacts><addresses><Address><ID>C1A001</ID><ContactId>C1</ContactId><type>Telephone</type><location>Mobile</location><value>07959000001</value><priority>1</priority></Address></addresses><attributes><Attribute><ID>C1A1</ID><ContactId>C1</ContactId><name>Gender</name><value>M</value></Attribute><Attribute><ID>C1A2</ID><ContactId>C1</ContactId><name>DOB</name><value>2000-02-28</value></Attribute></attributes><groups><Group><ID>G1</ID><name>Students</name><type>Student</type></Group></groups><groupMembership><Membership><ID>M1</ID><ContactId>C1</ContactId><GroupId>G1</GroupId></Membership></groupMembership></crud></dataChange></s:Body></s:Envelope>
    """
    headers = {
        'POST': 'https://m5testing.groupcall.com/webservices/data.asmx HTTP/1.1',
        'Content-Type': 'text/xml; charset=utf-8',
        'VsDebuggerCausalityData': 'uIDPo5ujmePS5WZOmO+OwV1VWVYAAAAAQwoYkNZd/kyobqNYvc4KkK2Jh5QjjKxBoasPFHFreQYACQAA',
        'SOAPAction': '"http://alert.groupcall.com/dataChange"',
        'Host': 'm5testing.groupcall.com',
        'Content-Length': '%d' % len(soapXml),
        'Expect': '100-continue',
        'Accept-Encoding': 'gzip, deflate',
        'Connection': 'Keep-Alive'
        }
    url = 'https://m5testing.groupcall.com/webservices/data.asmx'
    auth_handler = urllib2.HTTPBasicAuthHandler()
    auth_handler.add_password(realm='m5testing.groupcall.com',
                              uri=url,
                              user='test1',
                              passwd='football101')
    opener = urllib2.build_opener(auth_handler)
    urllib2.install_opener(opener)
    page = urllib2.urlopen(url)
    req = urllib2.Request(url, soapXml, headers)
    response = urllib2.urlopen(req)
    the_page = response.read()
    print(the_page)
    return response
