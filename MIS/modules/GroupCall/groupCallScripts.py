"""
Created on Thu Aug  1 13:12:18 2013
@author: roy

These scripts are for getting groups from WillowTree into GroupCall 4
"""
# WillowTree Imports
from MIS.modules import PupilRecs
from MIS.models import PupilGroup
from MIS.modules.GeneralFunctions import Get_PupilList

# Python Imports
import csv


class createCsvFiles():
    '''

    creates CSV files for each school that are then imported into each
    Group call 4 server

    REMEMBER:
    test == pupil_id!!!!

    '''
    def __init__(self, AcYear, test=False, allSchool=False, allSchoolOnly=False):
        self.test = test
        self.AcYear = AcYear
        if not allSchoolOnly:
            self.schools = ['Battersea',
                            'Clapham',
                            'Fulham',
                            'Kensington',
                            'Kindergarten']
            self.__createCsvFile__()
        if allSchool or allSchoolOnly:
            self.__createCsvFileAllSchool__()


    def __getPupilRecObjs__(self, school):
        ''' gets all PupilRec Objs '''
        return

    def __getPupilGrpObjs__(self, school, AcYear):
        if self.test:
            return PupilGroup.objects.filter(Pupil__id=self.test,
                                             Group__Name__istartswith='Current.%s' % school,
                                             AcademicYear=self.AcYear,
                                             Active=True).exclude(Group__Name__icontains='House')
        else:
            return PupilGroup.objects.filter(Group__Name__istartswith='Current.%s' % school,
                                             AcademicYear=self.AcYear,
                                             Active=True).exclude(Group__Name__icontains='House')

    def __createCsvFile__(self):
        ''' creates csv files '''
        for school in self.schools:
            with open('%sGroupCall.csv' % school, 'wb') as csvfile:
                grpCallWriter = csv.writer(csvfile, delimiter=',')
                grpCallWriter.writerow(['StudentID',
                                       'FirstName',
                                       'LastName',
                                       'ClassGroup',
                                       'YearGroup',
                                       'HouseGroup',
                                       'HomeTelephone',
                                       'MobileNumber',
                                       'EmailAddress'])
                lengthOfTask = len(self.__getPupilGrpObjs__(school, self.AcYear))
                counter = 0
                for pupilGroup in self.__getPupilGrpObjs__(school, self.AcYear):
                    if 'Applicant' not in pupilGroup.Group.Name:
                        pupilRecObj = PupilRecs.PupilRecord(pupilGroup.Pupil.id,
                                                            self.AcYear)
                        print 'trying id:%s ----> %s: %s of %s completed' % (str(pupilRecObj.Pupil.id),
                                                                             school,
                                                                             str(counter),
                                                                             str(lengthOfTask))
                        if 'Year' in pupilGroup.Group.Name:
                            yearGroup = pupilGroup.Group.Name.split('.')[3]
                        elif 'Reception' in pupilGroup.Group.Name:
                            yearGroup = 'Year0'
                        else:
                            yearGroup = ''
                        mobileNumber = ''
                        email = ''
                        for contact in pupilRecObj.EmergencyContacts():
                            if contact['Record'].Priority == 1:
                                mobileNumber = contact['Extended']['mobileNumber'][0].replace(' ', '')
                                if mobileNumber[:2] == '44':
                                    mobileNumber = '0%s' % str(mobileNumber[2:])
                                elif mobileNumber[:3] == '+44':
                                    mobileNumber = '0%s' % str(mobileNumber[3:])
                                email = contact['Record'].Contact.EmailAddress
                        grpCallWriter.writerow([pupilRecObj.Pupil.id,
                                               pupilRecObj.Pupil.Forename,
                                               pupilRecObj.Pupil.Surname,
                                               pupilGroup.Group.Name,
                                               yearGroup,
                                               pupilRecObj.AcademicHouse(),
                                               '',
                                               str(mobileNumber),
                                               email])
                        counter += 1

    def __createCsvFileAllSchool__(self):
        ''' creates csv files '''
        with open('%sGroupCall.csv' % "allSchool", 'wb') as csvfile:
            grpCallWriter = csv.writer(csvfile, delimiter=',')
            grpCallWriter.writerow(['StudentID',
                                   'FirstName',
                                   'LastName',
                                   'ClassGroup',
                                   'YearGroup',
                                   'HouseGroup',
                                   'HomeTelephone',
                                   'MobileNumber',
                                   'EmailAddress'])
            pupils = Get_PupilList(self.AcYear, 'Current')
            lengthOfTask = len(pupils)
            counter = 0
            for pupil in pupils:
                pupilRecObj = PupilRecs.PupilRecord(pupil.id, self.AcYear)
                print 'trying id:%s ----> %s: %s of %s completed' % (str(pupilRecObj.Pupil.id),
                                                                     "allSchool",
                                                                     str(counter),
                                                                     str(lengthOfTask))
                mobileNumber = ''
                email = ''
                for contact in pupilRecObj.EmergencyContacts():
                    if contact['Record'].Priority == 1:
                        mobileNumber = contact['Extended']['mobileNumber'][0].replace(' ', '')
                        if mobileNumber[:2] == '44':
                            mobileNumber = '0%s' % str(mobileNumber[2:])
                        elif mobileNumber[:3] == '+44':
                            mobileNumber = '0%s' % str(mobileNumber[3:])
                        email = contact['Record'].Contact.EmailAddress
                grpCallWriter.writerow([pupilRecObj.Pupil.id,
                                       pupilRecObj.Pupil.Forename,
                                       pupilRecObj.Pupil.Surname,
                                       "allSchool_Group",
                                       "allSchool_Year",
                                       "allSchool_%s" % pupilRecObj.AcademicHouse(),
                                       '',
                                       str(mobileNumber),
                                       email])
                counter += 1
        return
