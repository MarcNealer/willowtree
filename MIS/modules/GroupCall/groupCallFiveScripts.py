"""
Created on Thu Aug  1 13:12:18 2013
@author: roy

These scripts are for getting groups from WillowTree into GroupCall 5
"""
# WillowTree Imports
from MIS.modules import PupilRecs
from MIS.models import PupilGroup
from MIS.models import SystemVarData
from MIS.modules.ExtendedRecords import *
from django.db.models import Q

# python imports
import json
import datetime

class GroupCallDataExtract():
    ''' gets pupil contact data for Group call 5 server '''
    def __init__(self, school):
        self.AcYear = SystemVarData.objects.get(Variable__Name='CurrentYear').Data
        self.school = school

    def __getPupilGrpObjs__(self):
        self.pupilGrps = PupilGroup.objects.filter(Group__Name__istartswith='Current.%s' % self.school,
                                                   AcademicYear=self.AcYear,
                                                   Active=True).exclude(Group__Name__icontains='House')
        return

    def __getAllPupilObjs__(self):
        self.pupilObjs = {}
        for pupilGroup in self.pupilGrps:
            self.pupilObjs.update({pupilGroup.Pupil.id: PupilRecs.PupilRecord(pupilGroup.Pupil.id, self.AcYear)})
        return

    def __convertToJson__(self, dicObj):
        return json.dumps(dicObj)

    def getData(self):
        ''' main data collection '''
        returnData = {}
        self.__getPupilGrpObjs__()
        self.__getAllPupilObjs__()
        for pupilGroup in self.pupilGrps:
            if 'Applicant' not in pupilGroup.Group.Name:
                pupilRecObj = self.pupilObjs[pupilGroup.Pupil.id]
                if 'Year' in pupilGroup.Group.Name:
                    yearGroup = pupilGroup.Group.Name.split('.')[3]
                elif 'Reception' in pupilGroup.Group.Name:
                    yearGroup = 'Year0'
                else:
                    yearGroup = ''
                mobileNumber = ''
                email = ''
                returnData["%s_%s" % (pupilRecObj.Pupil.id,
                           pupilGroup.Group.Name)] = {}
                returnData["%s_%s" % (pupilRecObj.Pupil.id,
                           pupilGroup.Group.Name)]['pupil_Data'] = {'Id': pupilRecObj.Pupil.id,
                                                                    'FirstName': pupilRecObj.Pupil.Forename,
                                                                    'LastName': pupilRecObj.Pupil.Surname,
                                                                    'ClassGroup': pupilGroup.Group.Name,
                                                                    'YearGroup': yearGroup,
                                                                    'HouseGroup': pupilRecObj.AcademicHouse()}
                for contact in pupilRecObj.EmergencyContacts():
                    dataDic = {}
                    if contact['Record'].Priority == 1 or contact['Record'].Priority == 2:
                        if contact['Record'].Priority == 1:
                            contactType = "contact_1"
                        if contact['Record'].Priority == 2:
                            contactType = "contact_2"
                        mobileNumber = contact['Extended']['mobileNumber'][0].replace(' ', '')
                        if mobileNumber[:2] == '44':
                            mobileNumber = '0%s' % str(mobileNumber[2:])
                        elif mobileNumber[:3] == '+44':
                            mobileNumber = '0%s' % str(mobileNumber[3:])
                        email = contact['Record'].Contact.EmailAddress
                        dataDic = {'MobileNumber': str(mobileNumber),
                                   'EmailAddress': email}
                        returnData["%s_%s" % (pupilRecObj.Pupil.id,
                                              pupilGroup.Group.Name)][contactType] = dataDic
        return self.__convertToJson__(returnData)


class GroupCallDataExtractAmended():
    """ This is the same as the GroupCallDataExtract() class only with data re-arranged so output is in the follow:

{u'contact_1': {u'EmailAddress': u'francesca.griffiths@casa-londra.com',
  u'Id': 9534,
  u'MobileNumber': u'07966877270'},
 u'contact_2': {u'EmailAddress': u'dgriffiths@mayerbrown.com',
  u'Id': 9535,
  u'MobileNumber': u'07841887052'},
 u'group_Data': [{u'GroupName': u'Current.Fulham.PrepSchool.Year3.Games.Boys',
   u'Id': 6142},
  {u'GroupName': u'Current.Fulham.PrepSchool.Year3.Mathematics.SetC',
   u'Id': 6149},
  {u'GroupName': u'Alumni.Battersea', u'Id': 5630},
  {u'GroupName': u'Alumni.Fulham', u'Id': 6066},
  {u'GroupName': u'Current.Fulham.PrepSchool.Year3.Form.3FE', u'Id': 6173}],
 u'pupil_data': {u'FirstName': u'Giacomo',
  u'HouseGroup': u'Lawrence',
  u'Id': u'10296',
  u'LastName': u'Griffiths',
  u'YearGroup': u'3'}}

    """
    def __init__(self, school):
        self.AcYear = SystemVarData.objects.get(Variable__Name='CurrentYear').Data
        self.school = school
        self.current_string = "Current.%s" % school.capitalize()
        self.time_stamp = datetime.datetime.today().isoformat()

    def __convertToJson__(self, dicObj):
        return json.dumps(dicObj)

    def __process_mobile__(self, mobile_number):
        """
        Corrects mobile number so GroupCall5 can process it
        """
        mobile_number = mobile_number.replace(' ', '')
        if mobile_number[:2] == '44':
            mobile_number = '0%s' % str(mobile_number[2:])
        elif mobile_number[:3] == '+44':
            mobile_number = '0%s' % str(mobile_number[3:])
        return str(mobile_number)

    def getData(self):
        """
        main data collection
        """
        #file_object = open("group_call_five.log", "a")
        try:
            pupil_groups = PupilGroup.objects.filter(Group__Name__icontains=self.current_string,
                                                     AcademicYear=self.AcYear,
                                                     Active=True).exclude(Group__Name__icontains="Applicants.").distinct('Pupil__id')
            main_data_return = {}
            for pupil_group in pupil_groups:
                returnData = {}
                pupil_obj = PupilRecs.PupilRecord(pupil_group.Pupil.id, self.AcYear)
                returnData['pupil_data'] = {'FirstName': pupil_obj.Pupil.Forename,
                                            'LastName': pupil_obj.Pupil.Surname,
                                            'HouseGroup': pupil_obj.AcademicHouse(),
                                            'Id': str(pupil_obj.Pupil.id),
                                            'YearGroup': pupil_obj.Form()[0]}
                for contact in pupil_obj.EmergencyContacts():
                    if contact['Record'].Priority == 1:
                        returnData['contact_1'] = {'Id': contact['Record'].Contact.id,
                                                   'MobileNumber': self.__process_mobile__(contact['Extended']['mobileNumber'][0]),
                                                   'EmailAddress': contact['Record'].Contact.EmailAddress.replace(' ', '')}
                    if contact['Record'].Priority == 2:
                        returnData['contact_2'] = {'Id': contact['Record'].Contact.id,
                                                   'MobileNumber': self.__process_mobile__(contact['Extended']['mobileNumber'][0]),
                                                   'EmailAddress': contact['Record'].Contact.EmailAddress.replace(' ', '')}
                group_list = []
                for group in pupil_obj.GroupList():
                    group_list.append({'Id': group.Group.id, 'GroupName': group.Group.Name})
                returnData['group_Data'] = group_list
                main_data_return[str(pupil_obj.Pupil.id)] = returnData
                #file_object.write("%s: API accessed by %s\n" % (self.time_stamp, self.school))
            return self.__convertToJson__(main_data_return)
        except Exception as error:
            pass
            #file_object.write("%s: ERROR accessing api for %s: %s\n" % (self.time_stamp, self.school, error))
