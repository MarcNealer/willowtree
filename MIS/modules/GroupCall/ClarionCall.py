from MIS.modules import GeneralFunctions
from MIS.modules import PupilRecs
from MIS.models import *
from django.http import *
from django.template.loader import render_to_string
from MIS.modules.globalVariables import GlobalVariablesFunctions
import requests as url_requests
from StringIO import StringIO

def ClarionCallExtract():
    AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    pupillist=GeneralFunctions.Get_PupilList(AcYear,'Current.Battersea')
    outdata=[]
    for pupils in pupillist:
        outdata.append(PupilRecs.PupilRecord(pupils.id,AcYear))
    grouplist=Group.objects.filter(Name__istartswith='Current.Battersea').exclude(Name__iendswith='Form ')
    c={'PupilList':outdata,'GroupList':grouplist}
    DataFile=render_to_string('ClarionCallUpload.xml',c)
    print DataFile
    datalist={'msisdn':'07797141417','uploadCode':'01132536EF25','schoolName':"Thomas's London Day School - Test"}
    uploadfile={'uploadFile':('dirsync-ICClub_6B2AFFDE570A540E000001132536EFC6.xml',StringIO(DataFile))}
    uploadurl='https://www.myclarioncall.co.uk/submitFile.do'
    r=url_requests.post(uploadurl,data=datalist,files=uploadfile)
    return r
