from MIS.models import *
from django import template
from django.core.cache import cache
import GeneralFunctions
from MIS.modules.ExtendedRecords import *
from MIS.models import StaffGroup

# logging
import logging
log = logging.getLogger(__name__)


class StaffLetterRecord():
    """
    This class is used for generating staff letters
    """
    def __init__(self, staff_id, request, ac_year):
        self.Staff = Staff.objects.get(id=staff_id)
        self.request = request
        self.ac_year = ac_year
        self.letter_body = None

    def generate_letter_body(self, letter_details, var_list, other_items):
        t = template.Template(letter_details.BodyText.replace('&quot;', '"'))
        c = template.Context({'Staff': self.Staff,
                              'Var': var_list,
                              'OtherItems': other_items})
        self.letter_body = t.render(c)
        return


class StaffRecord():
    '''This class holds all the details used for a staff record, and needed
    for the staff page
    '''
    def __init__(self, StaffId, request, AcYear=None):
        self.request = request
        self.StaffId = StaffId
        self.AcYear = AcYear
        self.StaffRec = Staff.objects.get(id=StaffId)
        self.ExtendedObj = ExtendedRecord('Staff', StaffId, self.request)
        self.Extended = self.ExtendedObj.ReadExtention('StaffExtra')

    def UpdateBase(self):
        if 'Surname' in self.request.POST:
            self.StaffRec.Surname = self.request.POST['Surname']
        if 'Forename' in self.request.POST:
            self.StaffRec.Forename = self.request.POST['Forename']
        if 'OtherNames' in self.request.POST:
            self.StaffRec.OtherNames = self.request.POST['OtherNames']
        if 'NickName' in self.request.POST:
            self.StaffRec.NickName = self.request.POST['NickName']
        if 'Title' in self.request.POST:
            self.StaffRec.Title = self.request.POST['Title']
        if 'Gender' in self.request.POST:
            self.StaffRec.Gender = self.request.POST['Gender']
        if 'DateOfBirth' in self.request.POST:
            self.StaffRec.DateOfBirth = self.request.POST['DateOfBirth']
        if 'EmailAddress' in self.request.POST:
            self.StaffRec.EmailAddress = self.request.POST['EmailAddress']
            self.UpdateAddress()
        if 'Picture' in self.request.POST:
            self,StaffRec.Picture = self.request.POST['Picture']
        self.StaffRec.save()
        return

    @staticmethod
    def NewStaff(request):
        NewStaffRec = Staff(Surname=request.POST['Surname'],
                            Forename=request.POST['Forename'],
                            OtherNames=request.POST['OtherNames'],
                            EmailAddress=request.POST['EmailAddress'],
                            Title=request.POST['Title'],
                            Gender=request.POST['Gender'],
                            DateOfBirth=request.POST['DateOfBirth'])
        NewStaffRec.save()
        log.warn('New staff with id:%s created by %s' % (NewStaffRec.id,
                                                         self.request.user.username))
        return StaffObj(NewStaffRec.id)

    def NewStaffAddress(self, request):
        NewAddress=Address(HomeSalutation=request.POST['HomeSalutation'],
                           PostalTitle=request.POST['PostalTitle'],
                           AddressLine1=request.POST['AddressLine1'],
                           AddressLine2=request.POST['AddressLine2'],
                           PostCode=request.POST['PostCode'],
                           Phone1=request.POST['Phone1'],
                           EmailAddress=request.POST['EmailAddress'])
        NewAddress.save()
        if 'Note' in request.POST:
            NewAddress.Note = request.POST['Note']
        if 'AddressLine3' in request.POST:
            NewAddress.AddressLine3 = request.POST['AddressLine3']
        if 'AddressLine4' in request.POST:
            NewAddress.AddressLine4 = request.POST['AddressLine4']
        if 'Country' in request.POST:
            NewAddress.Country = request.POST['Country']
        if 'Phone2' in request.POST:
            NewAddress.Phone2 = request.POST['Phone2']
        if 'Phone3' in request.POST:
            NewAddress.Phone3 = request.POST['Phone3']
        NewAddress.save()
        log.warn('New staff address with id:%s created by %s' % (NewAddress.id,
                                                                 self.request.user.username))
        self.StaffRec.Address=NewAddress
        self.StaffRec.save()

    def UpdateAddress(self):
        AddressRec=self.StaffRec.Address
        if 'HomeSalutation' in self.request.POST:
            AddressRec.HomeSalutation = self.request.POST['HomeSalutation']
        if 'PostalTitle' in self.request.POST:
            AddressRec.PostalTitle = self.request.POST['PostalTitle']
        if 'AddressLine1' in self.request.POST:
            AddressRec.AddressLine1 = self.request.POST['AddressLine1']
        if 'AddressLine2' in self.request.POST:
            AddressRec.AddressLine2 = self.request.POST['AddressLine2']
        if 'AddressLine3' in self.request.POST:
            AddressRec.AddressLine3 = self.request.POST['AddressLine3']
        if 'AddressLine4' in self.request.POST:
            AddressRec.AddressLine4 = self.request.POST['AddressLine4']
        if 'PostCode' in self.request.POST:
            AddressRec.PostCode = self.request.POST['PostCode']
        if 'Country' in self.request.POST:
            AddressRec.Country = self.request.POST['Country']
        if 'Phone1' in self.request.POST:
            AddressRec.Phone1 = self.request.POST['Phone1']
        if 'Phone2' in self.request.POST:
            AddressRec.Phone2 = self.request.POST['Phone2']
        if 'Phone3' in self.request.POST:
            AddressRec.Phone3 = self.request.POST['Phone3']
        if 'EmailAddress' in self.request.POST:
            try:
                AddressRec.EmailAddress = self.request.POST['EmailAddress']
            except:
                pass
        if 'Note' in self.request.POST:
            AddressRec.Note = self.request.POST['Note']
        AddressRec.save()
        log.warn('Staff address with id:%s updated by %s' % (AddressRec.id,
                                                             self.request.user.username))
        return

    def ListGroups(self):
        return StaffGroup.objects.filter(Staff=self.StaffRec,AcademicYear=self.AcYear)

    def AddToGroup(self, GroupName):
        if not StaffGroup.objects.filter(Staff__id=self.StaffId,Group__Name=GroupName).exists():
            group = Group.objects.get(Name=GroupName)
            NewGroup = StaffGroup(Staff=self.StaffRec, AcademicYear=self.AcYear, Group=group)
            NewGroup.save()
            log.warn('Staff with id:%s has been added to the group:%s by %s' % (self.StaffId,
                                                                                GroupName,
                                                                                self.request.user.username))
        return

    def RemoveFromGroup(self, GroupName):
        try:
            group = Group.objects.get(Name=GroupName)
            OldRec = StaffGroup.objects.get(Staff=self.StaffRec,AcademicYear=self.AcYear, Group=group)
            OldRec.delete()
            log.warn('Staff with id:%s has been removed from the group:%s by %s' % (self.StaffId,
                                                                                   GroupName,
                                                                                   self.request.user.username))
        except:
            pass
        return

    def IsInGroup(self, GroupName):
        if StaffGroup.objects.filter(Staff=self.StaffRec,AcademicYear=self.AcYear).exists():
            return True
        else:
            return False

    def emergencyContacts(self):
        ''' returns a list of staff emergency contacts '''
        if StaffEmergencyContact.objects.filter(StaffId__id=int(self.StaffId)).count > 0:
            return StaffEmergencyContact.objects.filter(StaffId__id=int(self.StaffId)).order_by('Priority')
        else:
            return False


def getAllStaffForAGrp(Group, request, AcYear):
    ''' returns a list of StaffObject() objects that are a member of the
    same group.'''
    staffList = set(StaffGroup.objects.filter(Group__Name__icontains=Group))
    newStaffList = []
    StaffListFinal = []
    for i in staffList:
        newStaffList.append(Staff.objects.get(id=int(i.Staff.id)))
    for i in set(newStaffList):
        StaffListFinal.append(StaffRecord(i.id, request, AcYear))
    return StaffListFinal