from MIS.models import *
from MIS.modules import GeneralFunctions
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache

class MenuBuilder():
    def __init__(self,MenuType, BaseGroup,Type='New',Subjects=None):
        self.OrderList=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        # set order depth to 0
        self.OrderDepth=0
        # Check to see if the MenuType exists. If not issue error msg
        # Set MenuType and Base Group
        if Type=='New':
            self.NewMenu(MenuType,BaseGroup,Subjects)
        else:
            if MenuTypes.objects.filter(Name=MenuType).exists():
                self.MenuType=MenuTypes.objects.get(Name=MenuType)
                if not Group.objects.filter(Name=BaseGroup).exists():
                    GroupName=''
                    GroupElements=BaseGroup.split('.')
                    for GroupBits in GroupElements:
                        GroupName+=GroupBits
                        if not Group.objects.filter(Name=GroupName).exists():
                            NewGroup=Group(Name=GroupName,MenuName=GroupElements[-1])
                            NewGroup.save()
                        GroupName+='.'
                self.BaseGroup=BaseGroup
            else:
                raise MenuError, "MenuType Does not exist"
    def NewMenu(self,MenuType,BaseGroup,Subjects=None):
        # Check the Menu table and delete items if they already exist
        if Menus.objects.filter(MenuType__Name=MenuType):
            Menus.objects.filter(MenuType__Name=MenuType).delete()
        # Check Base Group. If it exists, create it with the list of subjects (if any)
        self.BaseGroup=BaseGroup
        print MenuType
        self.MenuType=MenuTypes.objects.get(Name=MenuType)
        if not Group.objects.filter(Name=BaseGroup).exists():
            if not Group.objects.filter(Name=BaseGroup).exists():
                NewGroup=Group(Name=BaseGroup,MenuName=BaseGroup.split('.')[-1])
                NewGroup.save()
        # Set the Order list to [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]
        self.OrderList=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        # set order depth to 0
        self.OrderDepth=0
        return
    def AddItem(self,GroupName,MenuName=None,Subjects=None,
    ParentSubjects=True,menuActions=None,SubMenus=None, ManagedGroups=None,NotSets=False,Active=True):
        '''
        MenuBuilder.AddItem:

        This method adds a menu Item to the class
        Parameters
        Group : GroupName (required)
        MenuName : MenuName to be assgined to the group if created, else last element in the name is used (NotReq)
        Subjects : List of Subjects that must already exist in the database (NotReq)
        ParentSubjects : Boolean. Get a merged list of subjects from itself and parents (default=True)
        Attendance : Boolean. Is Daily attendance an option (default=False)
        LessonAtt : Boolean. Can it be used for lesson Attendance (default=False)
        Grade : Boolean. Can grades be entered for this groups (default =False)
        SubMenu: List. List of SubMenu options to be available (NotReq)
        ManagedGroups. List. List of Groups to be managed by this group(Set Mangement) (NotReq)
        Active : Boolean. Active (Default=True)
        '''
        # check to see if the group exists and create if not
        if not self.BaseGroup ==None:
            GroupName="%s.%s" % (self.BaseGroup,GroupName)
        GroupId=self.CreateGroupOnly(GroupName,MenuName)
        # Set menu item in 'Menu' table using current order. Order to be set to the string representation of the number, with leading 0's
        self.OrderList[self.OrderDepth]+=10
        menuOrder='.'.join([str(item) for item in self.OrderList[0:self.OrderDepth+1]])
        if ManagedGroups:
            Managed=True
        else:
            Managed=False
        NewMenuItem=Menus(MenuType=self.MenuType, Order=menuOrder,Group=GroupId,Manage=Managed,Active=Active)
        NewMenuItem.save()
        if ManagedGroups:
            for Groups in ManagedGroups:
                if NotSets:
                    ManagedGroupName=Groups
                else:
                    ManagedGroupName="%s.%s" % (GroupName,Groups)
                Mgroup=self.CreateGroupOnly(ManagedGroupName)
                NewMenuItem.ManagedGroups.add(Mgroup)
        if menuActions:
            for Actions in menuActions:
                NewMenuItem.MenuActions.add(MenuActions.objects.get(Name=Actions))
        if SubMenus:
            for Items in SubMenus:
                if MenuSubMenu.objects.filter(Name=Items).exists():
                    NewMenuItem.SubMenus.add(MenuSubMenu.objects.get(Name=Items))
        NewMenuItem.save()

        return
    def Up(self):
        # Add 1 to the order Depth
        self.OrderDepth+=1
    def Down(self):
        # Reset the OrderList[OrderDepth] to 0
        # subtract 1 from order depth
        # Add 10 to OrderList[orderDepth]
        if self.OrderDepth > 0:
            for elements in range(self.OrderDepth,len(self.OrderList)):
                self.OrderList[elements]=0
            self.OrderDepth-=1

    def SetLevel(self,LevelNo):
        # Set orderDepth to LevelNo
        # for Items in OrderList from LevelNo+1 to the end, set to 0
        # add 10 to OrderList[orderdepth]
        if self.OrderDepth > 0 and LevelNo > -1:
            for elements in range(LevelNo+1,len(self.OrderList)):
                self.OrderList[elements]=0
            self.OrderDepth=LevelNo
    def SetBaseGroup(self,BaseGroup):
        if not BaseGroup==None:
            if not Group.objects.filter(Name=BaseGroup).exists():
                GroupName=''
                GroupElements=BaseGroup.split('.')
                for GroupBits in GroupElements:
                    GroupName+=GroupBits
                    if not Group.objects.filter(Name=GroupName).exists():
                        NewGroup=Group(Name=GroupName,MenuName=GroupElements[-1])
                        NewGroup.save()
                    GroupName+='.'
        self.BaseGroup=BaseGroup
    def CreateGroupOnly(self,GroupName,MenuName=None):
        if not Group.objects.filter(Name=GroupName).exists():
            if not MenuName:
                MenuName=GroupName.split(".")[-1]
            Gp=Group(Name=GroupName,MenuName=MenuName)
            Gp.save()
        else:
            Gp=Group.objects.get(Name=GroupName)
        return Gp
    def GetBaseGroup(self):
        return self.BaseGroup

    @staticmethod
    def AllGroups(MenuName,AcYear):
        MenuDetails=Menus.objects.filter(MenuType__Name=MenuName,Active=True).order_by('Order')
        DPE=0
        for items in MenuDetails:
            ItemDPE=len(items.Order.split('.'))
            if ItemDPE > DPE:
                DPE=ItemDPE
        UpperMap={}
        CurrentMap={}
        for counter in range(DPE,0,-1):
            for items in MenuDetails:
                menuLevel=items.MenuType.MenuLevel
                if len(items.Order.split('.'))==counter:
                    itemshtml= "<li><a href='#'>%s *</a>\r\n<ul>\r\n" % items.Group.MenuName

                    if items.Manage==True:
                        GroupList=''
                        for Groups in items.ManagedGroups.all():
                            GroupList+='%d-' % Groups.id
                        GroupList=GroupList[0:-1]
                        itemshtml+="<li><a href='/WillowTree/%s/%s/SetManager/%s/%s/'>Manage Set</a></li>\r\n" % (MenuName,AcYear,items.Group.id,GroupList)

                    itemshtml+="<li><a href='#'>Tasks</a>\r\n"
                    itemshtml+="<ul class='Tasks'>\r\n <li>\r\n"
                    itemshtml+="<table border=0><tr>\r\n"
                    itemshtml+="<td valign='top'>\r\n"
                    itemshtml+='<dl>\r\n <dt>Actions</dt>\r\n'                    
                    GroupActions=[]
                    for Actions in items.MenuActions.all().order_by('MenuOrder'):

                        GroupActions.append(Actions)
                        itemshtml+="<dd><a href='/WillowTree/%s/%s/%s/%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,Actions.Name,items.Group,Actions.MenuName)
                    itemshtml+="</dl>\r\n </td>"
                    for Sub_Menus in items.SubMenus.all().order_by('MenuOrder'):
                        Found_Items=False
                        Temp_HTML="<td valign='top'>\r\n  <dl>\r\n"
                        Temp_HTML+="<dt>%s</dt>\r\n" % Sub_Menus.Name
                        GroupsIn=GeneralFunctions.SubGroups(items.Group.Name)
                        Lists=GroupMenuItems.objects.filter(Type__Name=Sub_Menus.Name,Group__Name__in=GroupsIn)

                        # Go through the items in to add to the submenu. Checks each to see if
                        # they should only be avaiable to items that have other elements already
                        # defined such as being a set manger, have attendance required, or
                        # have s selected name
                        for menuitems in Lists:
                            if menuitems.MenuLevel >= menuLevel:
                                if menuitems.GroupRequirements=='Managed':
                                    if items.Manage:
                                        Temp_HTML+="<dd><a href='/WillowTree/%s/%s%s%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,menuitems.MenuItem.ItemURL,items.Group,menuitems.MenuItem.Name)
                                        Found_Items=True
                                elif menuitems.GroupRequirements=='Attenance':
                                    if 'Attendance' in GroupActions:
                                        Temp_HTML+="<dd><a href='/WillowTree/%s/%s%s%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,menuitems.MenuItem.ItemURL,items.Group,menuitems.MenuItem.Name)
                                        Found_Items=True
                                elif menuitems.GroupRequirements=='LessonAtt':
                                    if 'LessonAtt' in GroupActions:
                                        Temp_HTML+="<dd><a href='/WillowTree/%s/%s%s%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,menuitems.MenuItem.ItemURL,items.Group,menuitems.MenuItem.Name)
                                        Found_Items=True
                                elif menuitems.GroupRequirements=='NameContains':
                                     if items.Group.Name.find(menuitems.ReqParms):
                                        Temp_HTML+="<dd><a href='/WillowTree/%s/%s%s%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,menuitems.MenuItem.ItemURL,items.Group,menuitems.MenuItem.Name)
                                        Found_Items=True
                                else:
                                    Temp_HTML+="<dd><a href='/WillowTree/%s/%s%s%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,menuitems.MenuItem.ItemURL,items.Group,menuitems.MenuItem.Name)
                                    Found_Items=True

                        Temp_HTML+="</dl>\r\n</td>\r\n"
                        if Found_Items:
                            itemshtml+=Temp_HTML
                    itemshtml+="</tr>\r\n</table>\r\n"
                    itemshtml+="</li>\r\n </ul>\r\n"                    
                    for UpperElements in range(0,500,10):
                        keyItem="%s.%s" %(items.Order,str(UpperElements))
                        if UpperMap.has_key(keyItem):
                            itemshtml+=UpperMap[keyItem]
                    itemshtml+="\r\n</ul></li>\r\n"
                    CurrentMap[items.Order]=itemshtml

            UpperMap=copy.deepcopy(CurrentMap)
            CurrentMap={}
        MenuItems=UpperMap.keys()
        MenuItems.sort()
        ReturnMenu=''

        for keys in MenuItems:
            ReturnMenu+=UpperMap[keys]
        return ReturnMenu