# Django middleware imports
from django.http import HttpResponseRedirect


# WillowTree system imports
from MIS.models import Pupil
from MIS.models import PupilDocument
from MIS.models import Document
from MIS.models import Staff
from MIS.models import User


# logging
import logging
log = logging.getLogger(__name__)


def pupilAddDocument(request, school, AcYear, PupilId):
    ''' Adds a document/file to a Pupil '''
    userId = User.objects.get(id=request.session['_auth_user_id'])
    doc = Document(DocumentName=request.POST['documentName'],
                   Filename=request.FILES['documentUploadField'],
                   UploadStaff=Staff.objects.get(id=int(userId.staff.id)))
    doc.save()
    pupDoc = PupilDocument(PupilId=Pupil.objects.get(id=int(PupilId)),
                           DocumentId=doc)
    pupDoc.save()
    log.warn('%s has added "%s" document to PupilId:%s' % (request.user.username,
                                                           request.POST['documentName'],
                                                           PupilId))
    httpAddress = '/WillowTree/%s/%s/pupil/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def pupilRemoveDocument(request, school, AcYear, PupilId, DocId):
    ''' Removes document/file from Pupil '''
    doc = PupilDocument.objects.get(DocumentId__id=int(DocId))
    log.warn('%s has removed document with id of "%s" from PupilId:%s' % (request.user.username,
                                                                          DocId,
                                                                          PupilId))
    doc.delete()
    httpAddress = '/WillowTree/%s/%s/pupil/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)
