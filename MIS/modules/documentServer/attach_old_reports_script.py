# imports
from MIS.modules.documentServer.AttachReports import AttachReport
from MIS.modules.globalVariables.GlobalVariablesFunctions import GlobalVariables
from MIS.modules import GeneralFunctions


def run(term, report_type):
    """
    Attaches last academic years reports to pupils to all pupils.
    """
    for school in ['Battersea', 'Clapham', 'Fulham', 'Kensington']:
        group_list = list()
        this_ac_year = GlobalVariables.Get_SystemVar('PrevYear')

        # Fulham and Kensington
        group_list_small_school = [['Current.%s.LowerSchool.Year1' % school, 'YrR_Eot_%s' % term, 'Default'],
                                   ['Current.%s.LowerSchool.Year2' % school, 'Yr1_Eot_%s' % term, 'Default'],
                                   ['Current.%s.PrepSchool.Year3' % school, 'Yr2_Eot_%s' % term, 'Default'],
                                   ['Current.%s.PrepSchool.Year4' % school, 'Yr3_Eot_%s' % term, 'Default'],
                                   ['Current.%s.PrepSchool.Year5' % school, 'Yr4_Eot_%s' % term, 'Default'],
                                   ['Current.%s.PrepSchool.Year6.' % school, 'Yr5_Eot_%s' % term, 'Default']]

        # Battersea and Clapham
        group_list_large_school = [['Current.%s.LowerSchool.Year1' % school, 'YrR_Eot_%s' % term, 'Default'],
                                   ['Current.%s.LowerSchool.Year2' % school, 'Yr1_Eot_%s' % term, 'Default'],
                                   ['Current.%s.MiddleSchool.Year3' % school, 'Yr2_Eot_%s' % term, 'Default'],
                                   ['Current.%s.MiddleSchool.Year4' % school, 'Yr3_Eot_%s' % term, 'Default'],
                                   ['Current.%s.MiddleSchool.Year5' % school, 'Yr4_Eot_%s' % term, 'Default'],
                                   ['Current.%s.UpperSchool.Year6.Yr6_Sets.11Plus' % school, 'Yr5_Eot_%s' % term,
                                    '11Plus'],
                                   ['Current.%s.UpperSchool.Year6.Yr6_Sets.13Plus' % school, 'Yr5_Eot_%s' % term,
                                    '13Plus'],
                                   ['Current.%s.UpperSchool.Year7' % school, 'Yr6_Eot_%s' % term, '11Plus'],
                                   ['Current.%s.UpperSchool.Year7' % school, 'Yr6_Eot_%s' % term, '13Plus'],
                                   ['Current.%s.UpperSchool.Year8' % school, 'Yr7_Eot_%s' % term, 'Default']]
        test_group = [['Current.Battersea.MiddleSchool.Year3.Form.3BN', 'Yr2_Eot_%s' % term, 'Default']]

        # Test school size
        if school in ['Fulham', 'Kensington']:
            group_list = group_list_small_school
        if school in ['Battersea', 'Clapham']:
            group_list = group_list_large_school
        if school == 'test':
            school = 'Battersea'
            group_list = test_group

        # Generate and attach reports
        for group in group_list:
            pupil_list = GeneralFunctions.Get_PupilList(this_ac_year, group[0])
            print str(pupil_list)
            for pupil in pupil_list:
                print 'pupil --> %s' % str(pupil.id)
                if report_type is not "None":
                    report_type_here = report_type
                else:
                    report_type_here = group[2]
                attach_report = AttachReport(pupil_id=pupil.id,
                                             report_name=group[1],
                                             report_type=report_type_here,
                                             ac_year=this_ac_year,
                                             school=school)
                attach_report.run()