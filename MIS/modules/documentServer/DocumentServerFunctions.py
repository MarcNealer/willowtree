# Django middleware imports
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import loader, RequestContext,Context


# WillowTree system imports
from MIS.models import Pupil
from MIS.models import PupilDocument
from MIS.models import Document
from MIS.models import Staff
from MIS.models import User
from MIS.modules import GeneralFunctions
from MIS.modules.gradeInput.GradeFunctions import *

import datetime
import threading

class EOTReportAttachManager():
    def __init__(self,Term,AcYear):
        self.Term=Term
        self.AcYear=AcYear
    def AttachBattersea(self):
        self.school='BatterseaAdmin'
        self.__AttachLargeSchool('Battersea')
    def AttachClapham(self):
        self.school='ClaphamAdmin'
        self.__AttachLargeSchool('Clapham')
    def AttachFulham(self):
        self.school='FulhamAdmin'
        self.__AttachSmallSchool('Fulham')
    def AttachKensington(self):
        self.school='KensingtonAdmin'
        self.__AttachSmallSchool('Kensington')
    def __AttachLargeSchool(self,School):
        GroupList=[['Current.%s.LowerSchool.Year1' % School,'Yr1_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.LowerSchool.Year2' % School,'Yr2_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.MiddleSchool.Year3' % School,'Yr3_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.MiddleSchool.Year4' % School,'Yr4_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.MiddleSchool.Year5' % School,'Yr5_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.UpperSchool.Year6.Yr6_Sets.11Plus' % School,'Yr6_Eot_%s' % self.Term,'11Plus'],
                   ['Current.%s.UpperSchool.Year6.Yr6_Sets.13Plus' % School,'Yr6_Eot_%s' % self.Term,'13Plus'],
                   ['Current.%s.UpperSchool.Year7' % School,'Yr7_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.UpperSchool.Year8' % School,'Yr8_Eot_%s' % self.Term,'Default']]
        for eachGroup in GroupList:
            PupilList=GeneralFunctions.Get_PupilList(self.AcYear,eachGroup[0])
            runlist=[]
            for pupils in PupilList:
                print 'Attaching %d for %s Report %s' % (pupils.id,School,eachGroup[1])
                runlist.append(EOTReportAttach(pupils.id,eachGroup[1],eachGroup[2],self.AcYear,self.school,eachGroup[0]))
            for items in runlist:
                items.start()
            for items in runlist:
                items.join()
    def __AttachSmallSchool(self,School):
        GroupList=[['Current.%s.LowerSchool.Year1' % School,'Yr1_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.LowerSchool.Year2' % School,'Yr2_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.PrepSchool.Year3' % School,'Yr3_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.PrepSchool.Year4' % School,'Yr4_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.PrepSchool.Year5' % School,'Yr5_Eot_%s' % self.Term,'Default'],
                   ['Current.%s.PrepSchool.Year6.' % School,'Yr6_Eot_%s' % self.Term,'Default']]
        for eachGroup in GroupList:
            PupilList=GeneralFunctions.Get_PupilList(self.AcYear,eachGroup[0])
            runlist=[]
            for pupils in PupilList:
                print 'Attaching %d for %s Report %s' % (pupils.id,School,eachGroup[1])
                runlist.append(EOTReportAttach(pupils.id,eachGroup[1],eachGroup[2],self.AcYear,self.school,eachGroup[0]))
            for items in runlist:
                items.start()
            for items in runlist:
                items.join()
class EOTReportAttach(threading.Thread):
    def __init__(self,PupilId,ReportName,ReportType,AcYear,school,GroupName):
        threading.Thread.__init__(self)
        self.AcYear=AcYear
        self.PupilId=PupilId
        self.ReportName=ReportName
        self.ReportType=ReportType
        self.school=school
        self.SchoolName=GeneralFunctions.GetSchoolName(self.school)
        self.GroupName=GroupName
        self.Today=datetime.datetime.today().strftime('%d%m%Y')
    def run(self):
        if self.ReportType:
            reportname='%s_%s_%s_%s.html' % (self.ReportName,self.ReportType,self.PupilId,self.Today)
        else:
            reportname='%s_%d_%s.html' % (self.ReportName,self.PupilId,self.Today)

        ReportList=[(PupilGradeRecord(self.PupilId,self.AcYear,self.ReportName))]
        print ReportList

        c=Context({'ReportList':ReportList,'ReportType':self.ReportName})
        savetext=loader.render_to_string(['ReportsToParents/%s_%s_%s.html' % (self.ReportName,self.SchoolName,self.ReportType),
                                  'ReportsToParents/%s_%s.html' % (self.ReportName,self.ReportType),
                                  'ReportsToParents/%s_%s.html' % (self.ReportName,self.SchoolName),
                                  'ReportsToParents/%s.html' % (self.ReportName),
                                  'ReportsToParents/NoReportError.html'],c)
        ##f=open('/home/media/WillowTree/DocumentStorage/%s' % reportname,'w').write(savetext.encode('utf8'))
        f=open('/home/media/WillowTree/media/DocumentStorage/%s' % reportname,'w').write(savetext.encode('utf8'))
        staff=Staff.objects.get(Surname='Nealer',Forename='Marc')
        NewDoc=Document(DocumentKeywords=reportname,
                        DocumentName=reportname,
                        Filename='DocumentStorage/%s' % reportname,
                        UploadStaff=staff)
        NewDoc.save()
        NewPupildoc=PupilDocument(PupilId=Pupil.objects.get(id=self.PupilId),DocumentId=NewDoc)
        NewPupildoc.save()