# imports
from MIS.modules.gradeInput.GradeFunctions import PupilGradeRecord
from django.core.files.storage import default_storage
from django.conf import settings
from django.template import loader, RequestContext, Context
from MIS.modules.AnalysisReports.AnalysisReportsNew import *
import datetime
import pdfkit
import boto
import os
# import log
import logging
log = logging.getLogger(__name__)


class AttachReport():
    """
    Attaches a report to a pupil record's document storage.

    Usage:
    attach_report = AttachReport(8658, 'Yr3_Eot_M', 'Default', '2014-2015', 'Battersea')
    attach_report.run()
    """
    def __init__(self,
                 pupil_id,
                 report_name,
                 report_type,
                 ac_year,
                 school):
        self.pupil_id = pupil_id
        self.report_name = report_name
        self.report_type = report_type
        self.ac_year = ac_year
        self.school = school

    def run(self):
        # If pupil does not have a report attached already attach new report
        try:
            if not PupilDocument.objects.filter(PupilId__id=self.pupil_id,
                                                DocumentId__DocumentName__icontains=self.report_name).exists():
                report_list = [(PupilGradeRecord(self.pupil_id, self.ac_year, self.report_name))]
                c = Context({'ReportList': report_list, 'ReportType': self.report_name})
                print 'get data'
                save_text = loader.render_to_string(['ReportsToParents/%s_%s_%s.html' % (self.report_name, self.school, self.report_type),
                                                     'ReportsToParents/%s_%s.html' % (self.report_name, self.report_type),
                                                     'ReportsToParents/%s_%s.html' % (self.report_name, self.school),
                                                     'ReportsToParents/%s.html' % self.report_name,
                                                     'ReportsToParents/NoReportError.html'], c)

                filename='%s_%s_%s.pdf' % (self.report_name,self.report_type,self.pupil_id)
                pdfkit.from_string(save_text, '/tmp/'+filename)
                self.push_pdf_to_s3(filename)
                filename ='DocumentStorage/'+filename
                staff_object = Staff.objects.get(Surname='Nealer', Forename='Marc')
                new_doc = Document(DocumentKeywords=self.report_name,
                                   DocumentName=self.report_name,
                                   Filename=default_storage.open(filename, 'r'),
                                   UploadStaff=staff_object)
                new_doc.save()
                new_pupil_doc = PupilDocument(PupilId=Pupil.objects.get(id=self.pupil_id), DocumentId=new_doc)
                new_pupil_doc.save()
                message = 'SUCCESS: cron_attach_eot_reports.py attached %s to %s' % (self.report_name, self.pupil_id)
                log.warn(message)
            else:
                message = 'ERROR: cron_attach_eot_reports.py tried to attach %s to %s. Report already exists' % (self.report_name,
                                                                                                                 self.pupil_id)
                log.warn(message)
        except Exception as error:
            message = "ERROR: cron_attach_eot_reports.py tried to attach %s to %s. (%s)" % (self.report_name,
                                                                                            self.pupil_id,
                                                                                            error)
            return message
        return message

    def push_pdf_to_s3(self,filename):
        try:
            bucket_name = settings.AWS_STORAGE_BUCKET_NAME
            conn = boto.connect_s3(settings.AWS_ACCESS_KEY_ID,
                                   settings.AWS_SECRET_ACCESS_KEY,
                                   is_secure=True)
            bucket=conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
            fn = '/tmp/' + filename
            full_key_name = os.path.join('media/DocumentStorage', filename)
            k = bucket.new_key(full_key_name)
            k.set_contents_from_filename(fn)
            os.remove(fn)
        except:
            return False