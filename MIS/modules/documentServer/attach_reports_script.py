# imports
from MIS.modules.documentServer.AttachReports import AttachReport
from MIS.modules import GeneralFunctions


def run(this_ac_year, group_list):
    """
    Attaches reports to pupils.

    Example 1:

    # all of last years Battersea Mich reports
    run(this_ac_year='2013-2014',
        group_lists=[['Current.Battersea.LowerSchool.Year1', 'YrR_Eot_M', 'Default'],
                     ['Current.Battersea.LowerSchool.Year2', 'Yr1_Eot_M', 'Default'],
                     ['Current.Battersea.MiddleSchool.Year3', 'Yr2_Eot_M', 'Default'],
                     ['Current.Battersea.MiddleSchool.Year4', 'Yr3_Eot_M', 'Default'],
                     ['Current.Battersea.MiddleSchool.Year5', 'Yr4_Eot_M', 'Default'],
                     ['Current.Battersea.UpperSchool.Year6.', 'Yr5_Eot_M', 'Default'],
                     ['Current.Battersea.UpperSchool.Year7', 'Yr6_Eot_M', '11Plus'],
                     ['Current.Battersea.UpperSchool.Year7', 'Yr6_Eot_M', '13Plus'],
                     ['Current.Battersea.UpperSchool.Year8', 'Yr7_Eot_M', 'Default']])

    Example 2:

    # all of last years Fulham Mich reports
    run(this_ac_year='2013-2014',
        group_lists=[['Current.Fulham.LowerSchool.Year1', 'YrR_Eot_M', 'Default'],
                     ['Current.Fulham.LowerSchool.Year2', 'Yr1_Eot_M', 'Default'],
                     ['Current.Fulham.PrepSchool.Year3', 'Yr2_Eot_M', 'Default'],
                     ['Current.Fulham.PrepSchool.Year4', 'Yr3_Eot_M', 'Default'],
                     ['Current.Fulham.PrepSchool.Year5', 'Yr4_Eot_M', 'Default'],
                     ['Current.Fulham.PrepSchool.Year6.', 'Yr5_Eot_M', 'Default']])
    """
    # Generate and attach reports
    for group in group_list:
        pupil_list = GeneralFunctions.Get_PupilList(this_ac_year, group[0])
        school = group[0].split('.')[1]
        for pupil in pupil_list:
            print 'pupil --> %s' % str(pupil.id)
            attach_report = AttachReport(pupil_id=pupil.id,
                                         report_name=group[1],
                                         report_type=group[2],
                                         ac_year=this_ac_year,
                                         school=school)
            attach_report.run()