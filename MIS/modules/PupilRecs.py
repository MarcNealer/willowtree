from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache
from django.db.models import Q

import GeneralFunctions
from MIS.modules.ExtendedRecords import *
from MIS.modules.FamilyRecord import *
from MIS.modules.globalVariables import GlobalVariablesFunctions
from django.core.files.storage import default_storage as storage
import logging
log = logging.getLogger(__name__)

# for image re sizing
import PIL
from PIL import Image


def PupilCacheLoad():
    AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    PupillistA=PupilGroup.objects.filter(AcademicYear=AcYear)
    PupilList=set()
    for pupils in PupillistA:
        PupilList.add(pupils.Pupil.id)
    for pupils in PupilList:
        x=PupilRecord(pupils,AcYear)
    return

def PupilRecord(PupilId,AcYear):
    Pupilrec=cache.get('Pupil_%s' % PupilId)
    if not Pupilrec:
        PupilOb=PupilObject(PupilId,AcYear)
        cache.set('Pupil_%s' % PupilId,PupilOb,7200)
        return PupilOb
    else:
        return Pupilrec


class PupilObject():
    ''' Use this Class to create objects to collect all the infomation on a pupil
    inlcuding alerts and issues. Notes must have their own class as they may be
    attached to multiple pupils
    Please note, if you update any section of the pupil record, then you will
    have to create a new Pupil Object. Properties of this class are set once and
    are then not updates
    '''
    def __init__(self,PupilId,AcYear):
        self.PupilId = PupilId
        self.AcYear = AcYear
        self.CurrentYear = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
        self.Pupil=Pupil.objects.get(id=PupilId)
        self.ExtendedRecord = None
        self.ExtendedSenRecord = None
        self.ExtendedEalRecord = None
        self.ContactList = None
        self.EmergencyTable = None
        self.EmergencyUnordered = None
        self.SiblingList = None
        self.siblingsTable = None
        self.SiblingsUL = None
        self.FamilyRec = None
        self.Pupil_Status = None
        self.AcHouse = None
        self.PupilGroupList = None
        self.PupilDocs = None
        self.PupilForm = None
        self.SubjectTeacherList=None
        self.RecordedGrades = None
        self.ExtendedSenRecord=None
        self.ExtendedEalRecord=None
        self.TeacherList={}

    def __CacheSelf__(self):
        cache.set('Pupil_%s' % self.PupilId,self,7200)
        return 'orginal'

    def FamilyRecord(self):
        """ If the FamilyRec property is set, this method will return the
        property value, else it sets the value and then returns it"""
        if not self.FamilyRec:
            self.FamilyRec=FamilyChildren.objects.get(Pupil=self.Pupil,FamilyType=1).FamilyId
        return self.FamilyRec

    def PupilStatus(self):
        ''' This method sets and gets the Pupil_Status property'''
        if not self.Pupil_Status:
            if PupilGroup.objects.filter(Pupil=self.Pupil,Group__Name__icontains='Current', AcademicYear=self.AcYear).exists():
                self.Pupil_Status= 'Current'
            elif PupilGroup.objects.filter(Pupil=self.Pupil,Group__Name__icontains='Applicant', AcademicYear='Applicants').exists():
                self.Pupil_Status= 'Applicant'
            elif PupilGroup.objects.filter(Pupil=self.Pupil,Group__Name__icontains='Alumni', AcademicYear='Alumni').exists():
                self.Pupil_Status= 'Alumni'
            else:
                self.Pupil_Status= 'Not found'
        return self.Pupil_Status

    def Extended(self):
        ''' Creates a dictionary of the extended field data for a given child. These are
        the fields listed in the PupilExtra extended record
        Once this record has been called once, it saves the extended data as a property
        of this class and will just return that property if called again.
        '''
        if not self.ExtendedRecord:
            self.ExtendedRecord=ExtendedRecord(BaseType='Pupil',BaseId=self.Pupil.id).ReadExtention(ExtentionName='PupilExtra')
            self.__CacheSelf__()
        return self.ExtendedRecord

    def ExtendedSen(self):
        ''' Returns a dictionary of SEN extended records '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = ExtendedRecord(BaseType='Pupil',
                                                    BaseId=self.Pupil.id).ReadExtention(ExtentionName='PupilSEN')
            self.__CacheSelf__()
        return self.ExtendedSenRecord

    def ExtendedEal(self):
        ''' Returns a dictionary of EAL extended records '''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = ExtendedRecord(BaseType='Pupil',
                                                    BaseId=self.Pupil.id).ReadExtention(ExtentionName='PupilEAL')
            self.__CacheSelf__()
        return self.ExtendedEalRecord

    def returnYearPupilIsIn(self):
        ''' Returns what year a pupil in in '''
        return self.Form()[0]

    def AcademicHouse(self):
        ''' Sets and gets the AcHouse property. AcHouse holds the name of the academic
        house a pupil belongs to. AcHouse is stored as a String data type '''
        if not self.AcHouse:
            self.AcHouse=GeneralFunctions.Get_PupilHouse(self.PupilId)
        return self.AcHouse

    def Form(self):
        ''' Sets and gets the PupilForm property. PupilForm holds the name of the
        form a pupil belongs to for this academic year '''
        if not self.PupilForm:
            self.PupilForm='Alumni'
            GroupSet=GeneralFunctions.ListPupilGroups(self.PupilId)
            FindForm=False
            for items in self.GroupList():
                if not FindForm:
                    if len(items.Group.Name.split('.')) > 3:
                        if items.Group.Name.split('.')[-2]=='Form':
                            self.PupilForm=items.Group.Name.split('.')[-1]
                            FindForm=True
                            return self.PupilForm
                    if 'Applicant' in items.Group.Name:
                        self.PupilForm='Applicant'
                    if 'Current.Kindergarten' in items.Group.Name:
                        self.PupilForm='Kindergarten'
            self.__CacheSelf__()
        return self.PupilForm

    def GroupList(self):
        ''' Sets and gets the PupilGroupList property. This returns a Queryset
        of PupilGroup Records'''
        if not self.PupilGroupList:
            groups = PupilGroup.objects.filter(Pupil__id=self.PupilId,
                                               Active=True)
            self.PupilGroupList = groups.exclude((~Q(AcademicYear=self.CurrentYear)),
                                                 Group__Name__istartswith="Current")
        return self.PupilGroupList

    def isInGroup(self,GroupName):
        ''' Returns True if a child is in a selected group'''
        return PupilGroup.objects.filter(Pupil=self.Pupil,
                                         Active=True,
                                         Group__Name=GroupName,
                                         AcademicYear=self.AcYear).exists()

    def EmergencyContacts(self):
        ''' Sets and gets the ContactList property. ContactList a list collection
        of Contact records stored in emergency contact priority order'''
        if not self.ContactList:
            self.ContactList=[]
            if PupilContact.objects.filter(PupilId=self.Pupil).exists():
                ContactDetails=PupilContacts.objects.filter(PupilId=self.Pupil).order_by('Priority')
                for Contacts in ContactDetails:
                    self.ContactList.append({'Record':Contacts.FamilyContactId,
                                             'Extended':ExtendedRecord(BaseType='Contact',
                                                                       BaseId=Contacts.FamilyContactId.Contact.id).ReadExtention(ExtentionName='ContactExtra')})
            else:
                ContactDetails=FamilyContact.objects.filter(FamilyId=FamilyChildren.objects.get(Pupil=self.Pupil,FamilyType=1).FamilyId).order_by('Priority')
                for Contacts in ContactDetails:
                    self.ContactList.append({'Record':Contacts,
                                             'Extended':ExtendedRecord(BaseType='Contact',
                                                                       BaseId=Contacts.Contact.id).ReadExtention(ExtentionName='ContactExtra')})
            self.__CacheSelf__()

        return self.ContactList

    def FamilyContacts(self):
        '''This is a subset if contacts that don't include any contact marked as
        other'''
        contactRecs=[]
        for recs in self.EmergencyContacts():
            if not "Other" in recs['Record'].Relationship.Type:
                contactRecs.append(recs)
        return contactRecs

    def EmergencyContactsUL(self):
        ''' Sets and Gets the EmergencyUnorderd property. EmergencyUnordered in a
        string containing the Emergency contacts set in a html unordered list
        To use, just embed into your relevant template using a safe filter
        '''
        if not self.EmergencyUnordered:
            if not self.ContactList:
                self.EmergencyContacts()
            Template=template.loader.get_template('includes/EmergencyContactsList.html')
            self.EmergencyUnordered=Template.render(template.Context({'ContactList':self.ContactList,'Type':'List'}))
        return self.EmergencyUnordered

    def EmergencyContactsTable(self):
        ''' Sets and gets the EmergencyTable property.

        The EmergencyTable Propery is a string containing a list of
        emegency contacts, in order, in a HTML table.

        To use call this function in a template with the safe filter

        '''
        if not self.EmergencyTable:
            if not self.ContactList:
                self.EmergencyContacts()
            Template=template.loader.get_template('includes/EmergencyContactsList.html')
            self.EmergencyTable=Template.render(template.Context({'ContactList':self.ContactList,'Type':'Table'}))
        return self.EmergencyTable

    def Siblings(self):
        '''Sets and gets the SibingList property.

        The SiblingList property is a List collection of Pupilrecord objects.

        '''
        self.SiblingList=[]
        SiblingRecs=FamilyChildren.objects.filter(FamilyId=FamilyChildren.objects.get(Pupil=self.Pupil,FamilyType=1).FamilyId).exclude(Pupil=self.Pupil)
        for Sibs in SiblingRecs:
            self.SiblingList.append(PupilRecord(Sibs.Pupil.id,self.AcYear))
        return self.SiblingList

    def SiblingsUL(self):
        ''' Sets and gets the siblingUnordered property.

        siblingUnordered is a string containing a list of siblings in a HTML
        unordered list.
        '''
        if not self.siblingsUnordered:
            if not self.SiblingsList:
                self.Siblings()
            Template=template.loader.get_template('includes/SiblingsList.html')
            self.siblingsUnordered=Template.render(template.Context({'SiblingList':self.SiblingList,'Type':'List'}))
        return self.sibingsUnordered

    def SiblingsTable(self):
        '''Sets and gets the siblingTable property.

        siblingTable is a string containing sibling data in a HTML table.

        To use, call this method with a safe filter

        '''
        if not self.siblingsTable:
            if not self.SiblingsList:
                self.Siblings()
            Template=template.loader.get_template('includes/SiblingsList.html')
            self.siblingsTable=Template.render(template.Context({'SiblingList':self.SiblingList,'Type':'Table'}))
        return self.sibingsTable


    def PupilDocuments(self):
        ''' Sets and gets the PupilDocs propery.

        PupilDocs is a Queryset of PupilDocument objects

        '''
        if not self.PupilDocs:
            self.PupilDocs=PupilDocument.objects.filter(PupilId=self.Pupil)
        return self.PupilDocs

    def PupilIssueCount(self):
        ''' Gets a count of Active issues for a pupil.

        Returned data is a integer
        '''
        return PupilNotes.objects.filter(PupilId=self.Pupil,NoteId__IsAnIssue=True,NoteId__Active=True).exclude(NoteId__IssueStatus='Closed').count()

    def ActivePupilAlerts(self):
        '''
        Gets a list of active alerts against a pupil.

        Returned data is a list collection of PupilAlert objects
        '''
        self.AlertList=[]
        if PupilAlert.objects.filter(PupilId=self.PupilId,Active=True,StartDate__lte=datetime.datetime.today()).exists:
            Alerts=PupilAlert.objects.filter(PupilId=self.PupilId,Active=True,StartDate__lte= datetime.datetime.today).order_by('AlertType__AlertGroup__DisplayOrder')
            self.AlertList.append(copy.deepcopy(Alerts))
            return self.AlertList
        else:
            return ''

    def hasAlert(self, AlertName):
        for alert_items in self.ActivePupilAlerts():
            for alerts in alert_items:
                if alerts.AlertType.AlertGroup.Name == AlertName:
                    return alerts
        return False

    def hasInactiveAlert(self, AlertName):
        for alert_items in self.AllPupilAlerts():
            for alerts in alert_items:
                if alerts.AlertType.AlertGroup.Name == AlertName and alerts.Active == False:
                    return alerts
        return False

    def AllPupilAlerts(self):
        '''
        Gets a list of all alerts against a pupil.

        Returned data is a list collection of PupilAlert objects
        '''
        self.AlertList=[]
        if PupilAlert.objects.filter(PupilId=self.PupilId,StartDate__lte=datetime.datetime.today()).exists:
            Alerts=PupilAlert.objects.filter(PupilId=self.PupilId,StartDate__lte= datetime.datetime.today).order_by('AlertType__AlertGroup__DisplayOrder')
            self.AlertList.append(copy.deepcopy(Alerts))
            return self.AlertList
        else:
            return ''

    def ReturnCurrentActiveEal(self):
        ''' Returns current active EAL alert for a Pupil '''
        return self.hasAlert('EAL')

    def ReturnCurrentActiveSen(self):
        ''' Returns current active SEN alert for a Pupil '''
        return self.hasAlert('SEND')

    def ReturnCurrentActiveGT(self):
        ''' Returns current active GT alert for a Pupil '''
        return self.hasAlert('Gifted and Talented')

    def ReturnCurrentActiveMoreAbled(self):
        ''' Returns current active more abled alert for a Pupil '''
        return self.hasAlert('More Abled')

    def ReturnCurrentInActiveEal(self):
        ''' Returns current active EAL alert for a Pupil '''
        return self.hasInactiveAlert('EAL')

    def ReturnCurrentInActiveSen(self):
        ''' Returns current active SEN alert for a Pupil '''
        return self.hasInactiveAlert('SEND')

    def ReturnCurrentInActiveGT(self):
        ''' Returns current active GT alert for a Pupil '''
        return self.hasInactiveAlert('Gifted and Talented')

    def ReturnCurrentInActiveMoreAbled(self):
        ''' Returns current active more abled alert for a Pupil '''
        return self.hasInactiveAlert('More Abled')

    def pastSchool(self):
        ''' returns the pupils past school object'''
        return PupilsNextSchool.objects.get(PupilId=int(self.PupilId))

    def returnGiftedTalented(self):
        ''' returns current active gifted and talented data on a child '''
        return PupilGiftedTalented.objects.filter(Pupil__Id=int(self.PupilId),
                                                  Active=True)
    def SubjectTeachers(self):
        if not self.SubjectTeacherList:
            self.SubjectTeacherList={}
            for groups in self.GroupList().filter(Group__Name__iendswith=self.Form()):
                Teachername=GlobalVariablesFunctions.TeacherNames(group.Group.Name,self.AcYear)
                for keys,values in Teachername.SubjectTeachers().items():
                    self.SubjectTeacherList[keys]=values
            for groups in self.GroupList().exlcude(Group__Name__iendswith=self.Form()):
                Teachername=GlobalVariablesFunctions.TeacherNames(group.Group.Name,self.AcYear)
                for keys,values in Teachername.SubjectTeachers().items():
                    self.SubjectTeacherList[keys]=values
        return self.SubjectTeacherList

    def PupilReportedGrades(self):
        ''' This class gets all the Recorded grade data for a selected pupil'''
        if not self.RecordedGrades:
            RecordedGrades={}
            Extended=ExtendedRecord('Pupil',self.PupilId)
            recordTypes=ExtentionRecords.objects.all()
            for records in recordTypes:
                test=ExtentionForPupil.objects.filter(ExtentionRecordId=records,PupilId__id=self.PupilId)
                if test.exists():
                    RecordedGrades[records.Name]={}
                    subjects=set([x.ExtentionSubject for x in test])
                    for subjectName in subjects:
                        RecordedGrades[records.Name][subjectName]=Extended.ReadExtention(records.Name,subjectName)
            self.RecordedGrades=RecordedGrades
        return self.RecordedGrades

    def FutureAttendances(self):
        return PupilAttendance.objects.filter(Pupil=self.Pupil,AttendanceDate__gt=datetime.datetime.today(),Active=True)

    def AbsentToday(self):
        if datetime.datetime.today().hour < 12:
            Am_Pm='AM'
        else:
            Am_Pm='PM'
        if PupilAttendance.objects.filter(Pupil=self.Pupil,
                                          AttendanceDate=datetime.datetime.today(),
                                          AmPm=Am_Pm,
                                          Active=True,
                                          Code__AttendanceCodeType__in=['Authorised','Unauthorised']).exists():
            return True
        else:
            return False

    def PupilFutureSchoolProgress(self):
        ''' This show pupils progress in their future/next school '''
        if len(PupilFutureSchoolProgress.objects.filter(Pupil__id=self.PupilId)) >= 1:
            school = PupilFutureSchoolProgress.objects.filter(Pupil__id=self.PupilId)
        else:
            school = False
        return school

    def TeacherNames(self):
        ''' This gets a list of All teachers assigned to a child groups. It works on the
        basis of getting the Teachers assigned to the form groups first, then searches
        the other groups a child belongs to. These other groups will override the teacher
        set in the form class'''
        if not self.TeacherList:
            FormList=PupilGroup.objects.filter(Group__Name__icontains='form.',Pupil__id=self.PupilId,AcademicYear=self.AcYear)
            for groups in FormList:
                names=GlobalVariablesFunctions.TeacherNames(groups.Group.Name,self.AcYear)
                self.TeacherList.update(names.SubjectTeachers())
                if names.FormTeacher():
                    self.TeacherList['FormTeacher']=names.FormTeacher()
                if names.AllTeachers():
                    self.TeacherList['AllTeachers']=names.AllTeachers()
                if names.Tutor():
                    self.TeacherList['Tutor']=names.Tutor()
            SetList=PupilGroup.objects.filter(Group__Name__icontains='Set',Pupil__id=self.PupilId,AcademicYear=self.AcYear)
            for groups in SetList:
                names=GlobalVariablesFunctions.TeacherNames(groups.Group.Name,self.AcYear)
                self.TeacherList['Subject']=names.SubjectTeachers()
        return self.TeacherList
    def IsSEN(self):
        if self.hasAlert('SEND'):
            return True
        else:
            return False
    def IsEAL(self):
        if self.hasAlert('EAL'):
            return True
        else:
            return False
    def IsMoreAble(self):
        if self.hasAlert('More Abled'):
            return True
        else:
            return False
    def IsGiftedAndTalented(self,SubjectName):
        return PupilGiftedTalented.objects.filter(Pupil__id=int(self.PupilId),
                                                  Active=True,Subject__Name=SubjectName).exists()
    def IsSumBirth(self):
        return self.Pupil.DateOfBirth.month > 4 and self.Pupil.DateOfBirth.month < 9

    def get_amount_of_notes_for_pupil(self):
        """
        Returns the amount of new notes a pupil has for today and this week
        """
        today = datetime.datetime.today().date().isoformat()
        return GetAmountOfNotesForPupil(self.PupilId, today).return_notes_data()


class GetAmountOfNotesForPupil():
    """
    Returns the amount of new notes a pupil has in a given day and week
    """
    def __init__(self, pupil_id, today_date):
        self.pupil_id = pupil_id
        self.today_date = today_date

    def __determine_week_start_end_dates__(self):
        return_dates = {}
        dt = datetime.datetime.strptime(self.today_date, "%Y-%m-%d")
        start = dt - timedelta(days=dt.weekday())
        end = start + timedelta(days=7)
        return_dates['start_date'] = start.strftime("%Y-%m-%d")
        return_dates['end_date'] = end.strftime("%Y-%m-%d")
        return return_dates

    def __determine_today_start_end_dates__(self):
        return_dates = {}
        start = datetime.datetime.strptime(self.today_date, "%Y-%m-%d")
        end = start + timedelta(days=1)
        return_dates['start_date'] = start.strftime("%Y-%m-%d")
        return_dates['end_date'] = end.strftime("%Y-%m-%d")
        return return_dates

    def __get_notes_data__(self, start_date, end_date):
        """
        Gets notes data
        """
        notes = PupilNotes.objects.filter(PupilId__id=self.pupil_id,
                                          NoteId__RaisedDate__range=(start_date, end_date),
                                          Active='True')
        return len(notes)

    def return_notes_data(self):
        """
        Returns notes data
        """
        return_data = {}
        week_dates = self.__determine_week_start_end_dates__()
        today_dates = self.__determine_today_start_end_dates__()
        return_data['today'] = self.__get_notes_data__(today_dates['start_date'],
                                                       today_dates['end_date'])
        return_data['this_week'] = self.__get_notes_data__(week_dates['start_date'],
                                                           week_dates['end_date'])
        return return_data


class UpdatePupilRecord():
    """
    This class is used to update various parts of the pupil Record.

    Usage  x=UpdatePupilRecord(request,school,AcYear,Pupilid)

    Where

    request =  the request objects containing items such as post data
    school =MenuName
    AcYear= Academic Year usually passed from the url
    Pupilid= Pupil No passed as an int or string.
    """
    def __init__(self,request,school,AcYear,PupilNo):
        self.request = request
        self.PupilRec = Pupil.objects.get(id=int(PupilNo))
        self.PupilNo = PupilNo
        self.AcYear = AcYear
        self.School = school
        self.schoolName = MenuTypes.objects.get(Name=self.School).SchoolId.Name
        self.PostData = request.POST
        self.FileData = request.FILES
        self.childsSchool = self.__getSchoolChildIsIn__()

    def __getSchoolChildIsIn__(self):
        grps = PupilGroup.objects.filter(Group__Name__startswith='Current.',
                                         Pupil__id=self.PupilRec.id,
                                         Active=True)
        if len(grps) >= 1:
            test = grps[0].Group.Name.split('.')[1]
            if test in ['Battersea','Clapham', 'Fulham','Kensington']:
                return test
            else:
                return 'error - child is not in a school'
        else:
            return 'error - child is not in a school'

    def Base(self):
        ''' Update base Pupil information.

        Update elements
            Surname
            Forename
            Gender
            DateOfBirth
            Title
            NickName
            OtherNames
            EmailAddress

        Once Updated, it will refesh the cached PupilRecord
        '''
        self.PupilRec.Surname = self.PostData['Surname']
        self.PupilRec.Forename = self.PostData['Forename']
        self.PupilRec.Gender = self.PostData['Gender']
        self.PupilRec.DateOfBirth = self.PostData['DateOfBirth']
        self.PupilRec.Title = self.PostData['Title']
        self.PupilRec.NickName = self.PostData['NickName']
        self.PupilRec.OtherNames = self.PostData['OtherNames']
        self.PupilRec.EmailAddress = self.PostData['EmailAddress']
        if self.PostData['registrationDate'] != '':
            self.PupilRec.RegistrationDate = self.PostData['registrationDate']
        self.PupilRec.save()
        self.updateCache()
        return

    def MoveForm(self,NewForm):
        ''' This method changes a pupils form class'''
        if PupilGroup.objects.filter(AcademicYear=self.AcYear,Pupil=self.PupilRec,Group__Name__istartswith='Current').exists():
            formGroups=PupilGroup.objects.filter(AcademicYear=self.AcYear,Pupil=self.PupilRec,Group__Name__icontains='Form')
            for eachGroup in formGroups:
                item=eachGroup
                item.Active=False
                item.save()
        NewPupilGroup=PupilGroup(AcademicYear=self.AcYear,
                                 Pupil=self.PupilRec,
                                 Group=Group.objects.get(Name=NewForm))
        NewPupilGroup.save()
        log.warn('Pupil (%s) was moved form by %s' % (self.PupilNo,self.request.user.username))

        self.updateCache()
        return

    def MoveHouse(self,NewHouse):
        ''' This method changes or adds a Pupils House'''
        if PupilGroup.objects.filter(Pupil=self.PupilRec,Group__Name__istartswith='Current').exists():
            houseGroups=PupilGroup.objects.filter(Pupil=self.PupilRec,Group__Name__icontains='House')
            for eachGroup in houseGroups:
                item=eachGroup
                item.Active=False
                item.save()
        NewPupilGroup=PupilGroup(AcademicYear=self.AcYear,
                                 Pupil=self.PupilRec,
                                 Group=Group.objects.get(Name=NewHouse))
        NewPupilGroup.save()
        log.warn('Pupil (%s) was moved House by %s' % (self.PupilNo,self.request.user.username))
        self.updateCache()
        return

    def updatePassPupilReferenceCode(self, passPupilReferenceCode):
        pupilExtentionRecord = ExtendedRecord(BaseType='Pupil',
                                              BaseId=self.PupilNo,
                                              request=self.request)
        pupilExtentionRecord.WriteExtention('PupilExtra',
                                            {'PassPupilReferenceCode': passPupilReferenceCode})
        self.updateCache()
        return

    def AssignHouseAsSib(self):
        '''This Helper method searches siblings and assigns this child to the same
        house as their siblings. It adds the Pupil to the same house that the first sibling
        found with a house assigned'''
        if not PupilGroup.objects.filter(Group__Name__icontains='House').exists():
            if len(self.Siblings()) > 0:
                for Sibs in self.Siblings():
                    for groups in Sibs.GroupList():
                        if "House" in groups.Group.Name:
                            newRec=PupilGroup(AcademicYouse=self.AcYear,Pupil=self.PupilRec,Group=groups.Group)
                            newRec.save()
                            self.updateCache()
                            return
        return

    def moveToGroup(self,oldGroup,newGroup):
        '''
        Moves a pupil from one group to another

        Roy, can you change. You should also check first if the child is in the old
        group and not in the new group before actioning any changes.
        '''
        self.removeFromGroup(oldGroup)
        group = Group.objects.get(Name=newGroup)
        newPupilGroupRecord = PupilGroup(AcademicYear=self.AcYear, Pupil=self.PupilRec, Group=group)
        newPupilGroupRecord.save()
        self.updateCache()
        log.warn('Pupil (%s) was moved from %s to %s  by %s' % (self.PupilNo,oldGroup, newGroup,self.request.user.username))
        return

    def removeFromGroup(self,group):
        ''' Removes a pupil from a selected Group'''
        try:
            oldgroup = PupilGroup.objects.get(Pupil__id=self.PupilNo,Group__Name=group,Active=True)
            oldgroup.Active = False
            oldgroup.save()
            log.warn('Pupil (%s) was removed from %s by %s' % (self.PupilNo,group,self.request.user.username))
        except:
            pass
        self.updateCache()
        return

    def MoveBackToHolding(self):
        ''' Roy.

        Compare this one with your one!
        '''
        PupilGroups = Pupilgroups.objects.filter(Pupil=self.PupilRec,Group__Name__icontains=MenuTypes.objects.get(Name=self.School).SchoolId.Name,Active=True)
        for ActiveGroups in PupilGroups:
            item=ActiveGroup
            item.Active=False
            item.save()
        if not PupilGroup.objects.filter(Pupil=self.PupilRec,Active=True,Group__Name='Applicants.Holding').exists():
            NewPupilGroup=PupilGroup(Pupil=self.PupilRec,Group=Group.objects.get(Name='Applicants.Holing'))
            NewPupilGroup.save()
        self.updateCache()
        log.warn('Pupil (%s) was moved to holding by %s' % (self.PupilNo,self.request.user.username))
        return

    def Permissions(self,internetPermissions,mediaPermissions):
        ''' Updates Pupil school permissions that are in the PupilExtra extended record'''
        pupilExtentionRecord = ExtendedRecord(BaseType='Pupil',
                                              BaseId=self.PupilNo,
                                              request=self.request)
        pupilExtentionRecord.WriteExtention('PupilExtra', {'P_Internet':internetPermissions})
        pupilExtentionRecord.WriteExtention('PupilExtra', {'P_Media':mediaPermissions})
        self.updateCache()
        return

    def getPupilOtherInformationPickLists(self):
        pupilExtentionRecord = ExtendedRecord(BaseType='Pupil',
                                              BaseId=self.PupilNo,
                                              request=self.request)
        test = pupilExtentionRecord.ReadExtention('PupilExtra')
        if bool(test) == False:
            pupilExtentionRecord.WriteExtention('PupilExtra',{'P_Internet':False})
        self.updateCache()
        return pupilExtentionRecord.ReadExtention('PupilExtra')

    def UpdatePupilOtherInformation(self):
        pupilExtentionRecord = ExtendedRecord(BaseType='Pupil',
                                              BaseId=self.PupilNo,
                                              request=self.request)
        pupilExtentionRecord.WriteExtention('PupilExtra',self.CheckItem(['UPN',
                                                                         'EntryYear',
                                                                         'Ethnicity',
                                                                         'Nationality',
                                                                         'Religion',
                                                                         'PrimaryLanguage']))
        cache.delete('PupilEAL_%s' % self.PupilNo)
        self.updateCache()
        return

    def UpdateLeavingDetails(self):
        pupilExtentionRecord = ExtendedRecord(BaseType='Pupil',
                                              BaseId=self.PupilNo,
                                              request=self.request)
        pupilExtentionRecord.WriteExtention('PupilExtra',self.CheckItem(['LeaverDetails','LeaverStatus']))
        ExtendedData=pupilExtentionRecord.ReadExtention('PupilExtra')
        if ExtendedData['LeaverStatus'][0]=='LEFT' and ExtendedData['LeaverDate'][0] == '' :
            pupilExtentionRecord.WriteExtention('PupilExtra',{'LeaverDate':datetime.datetime.today()})
            GeneralFunctions.RemoveFromCurrent(self.School,self.PupilNo,self.AcYear)
        elif ExtendedData['LeaverStatus'][0] == 'EOY' and ExtendedData['LeaverDate'][0] =='':
            pupilExtentionRecord.WriteExtention('PupilExtra',{'LeaverDate':datetime.datetime(int(self.AcYear.split('-')[1]),8,31)})
        self.updateCache()
        return

    def CheckItem(self,ItemList):
        '''Scans for a list of items in Post data. If the item is there and
        has some data, then its added to a dictionary, where each key is the
        item name.
        usage: self.CheckItem(['item','item','item'])
        '''
        UpdateList = {}
        for Item in ItemList:
            if Item in self.PostData:
                if len(self.PostData[Item]) > 0:
                    UpdateList.update({Item: self.PostData[Item]})
        return UpdateList

    def picture(self):
        ''' Update Pupil Record picture field'''
        old_picture = self.PupilRec.Picture
        try:
            self.PupilRec.Picture = self.FileData['Picture']
            self.PupilRec.save()
            self.updateCache()

            # re-size picture
            image = Image.open(self.PupilRec.Picture)
            width = 170
            height = 125
            image = image.resize((width, height), Image.ANTIALIAS)
            #image.save(self.PupilRec.Picture.path)
            s3_store = storage.open(self.PupilRec.Picture.file.name, "w")
            #image_format = self.PupilRec.Picture.file.name
            image.save(s3_store)
            s3_store.close()
        except Exception as error:
            self.PupilRec.Picture = old_picture
            self.PupilRec.save()
            self.updateCache()
            self.request.session['Messages'] = ["ERROR: %s" % error]
        return

    def SpecialNeeds(self):
        ''' Methods for updating special needs sections'''
        pass

    def removeFromAllGrpsMvToHolding(self):
        ''' Removes a single pupil or applicant, from the main pupil record
        page, from all groups and puts them in the Holdings group '''
        groups = PupilGroup.objects.filter(Pupil__id=int(self.PupilNo))
        for group in groups:
            temp = group
            temp.Active = False
            temp.save()
        pupil = Pupil.objects.get(id=int(self.PupilNo))
        group = Group.objects.get(Name="Applicants.Holding")
        putInHolding = PupilGroup(Pupil=pupil,
                                  Group=group,
                                  AcademicYear="Holding")
        putInHolding.save()
        self.updateCache()
        return

    def UpdateNewFormClass(self):
        ''' Update the NewFormClass field in the Extended record'''
        pupilExtentionRecord = ExtendedRecord(BaseType='Pupil',
                                              BaseId=self.PupilNo,
                                              request=self.request)
        pupilExtentionRecord.WriteExtention('PupilExtra',{'NewFormClass': "%s %s" % (self.PostData['NewAcYear'],self.PostData['NewGroup'])})
        self.updateCache()

        return

    def updateCache(self):
        ''' Quick method to update the cached PupilRecord'''
        cache.delete('Pupil_%s' % self.PupilNo)
        return

    def addGiftedTalented(self):
        ''' add a pupil's gitfted and talented data '''
        # change post data code for 'subject' if needed
        # change post data code for 'details' if needed
        pupilGATObj = PupilGiftedTalented(Pupil=self.PupilRec,
                                          Subject=Subject.objects.get(Name=request.POST['subject']),
                                          Details=request.POST['details'])
        pupilGATObj.save()
        return

    def removeGiftedTalented(self, pupilGiftedTalentedId):
        ''' a pupil's gitfted and talented data '''
        oldGATObj = PupilGiftedTalented.objects.get(id=int(pupilGiftedTalentedId))
        oldGATObj.Active = False
        oldGATObj.save()
        return

    def modifyGiftedTalented(self, pupilGiftedTalentedId):
        ''' modify a pupil's gitfted and talented data '''
        self.removeGiftedTalented(pupilGiftedTalentedId)
        self.addGiftedTalented()
        return

