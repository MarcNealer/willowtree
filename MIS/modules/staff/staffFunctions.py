# Python imports
import threading

# Django imports
from django.core.mail import send_mail

# WillowTree system imports
from MIS.models import Staff
from MIS.models import MenuTypes
from MIS.models import Address
from MIS.models import SystemVarData
from MIS.modules.emailFromWillowTree.emailFunctions import newEmailAccountRequest
from MIS.modules.ExtendedRecords import *

import logging
log = logging.getLogger(__name__)


class emailRingwoodStaffChanges(threading.Thread):
    ''' emails staff changes to ringwood '''
    def __init__(self, data, userEmail):
        threading.Thread.__init__(self)
        self.userEmail = [userEmail]
        self.data = data
        self.willowTreeEmail = SystemVarData.objects.get(Variable__Name='WillowTree Email Address').Data
        self.ringwoodEmail = [SystemVarData.objects.get(Variable__Name='Ringwood Email Address 2').Data]

    def run(self):
        try:
            self.__sendEmail__('Changes to staff record from WillowTree',
                               self.data,
                               self.willowTreeEmail,
                               self.ringwoodEmail)
        except Exception, e:
            log.warn("EMAIL ERROR FROM emailRingwoodStaffChanges CLASS: %s" % e)
        try:
            self.__sendEmail__('Message from WillowTree',
                               '\nRingwood has been notified of changes to the staff record you recently modified.\n',
                               self.willowTreeEmail,
                               self.userEmail)
        except Exception, e:
            log.warn("EMAIL ERROR FROM emailRingwoodStaffChanges CLASS: %s" % e)

    def __sendEmail__(self, header, data, sentFrom, sentToList):
        ''' simple function that sends emails '''
        send_mail(header,
                  data,
                  sentFrom,
                  sentToList,
                  fail_silently=False)


class newStaff():
    def __init__(self, request, AcYear):
        self.request = request
        self.AcYear = AcYear

    def createNewStaff(self):
        ''' creates a new staff member from the new staff page '''
        newStaff = Staff(Forename=self.request.POST['teacherForename'],
                         Surname=self.request.POST['teacherSurname'],
                         DateOfBirth=self.request.POST['teacherDOB'],
                         Gender=self.request.POST['TeacherGender'],
                         DefaultMenu=self.__createMenuType__(),
                         Address=self.__createAddress__())
        if 'teacherOtherName' in self.request.POST:
            newStaff.OtherNames = self.request.POST['teacherOtherName']
        if 'teacherKnownAs' in self.request.POST:
            newStaff.NickName = self.request.POST['teacherKnownAs']
        if 'teacherTitle' in self.request.POST:
            newStaff.Title = self.request.POST['teacherTitle']
        email = self.__generateStaffEmail__(self.request.POST['teacherForename'],
                                              self.request.POST['teacherSurname'])
        newStaff.EmailAddress = email
        newStaff.save()
        if 'teacherJobTitle' in self.request.POST or 'teacherStartDate' in self.request.POST:
            staffExtRecord = ExtendedRecord(BaseType='Staff',
                                            BaseId=newStaff.id,
                                            request=self.request)
        if 'teacherStartDate' in self.request.POST:
            staffExtRecord.WriteExtention('StaffExtra',
                                         {'staffStartDate': self.request.POST['teacherStartDate']})
        if 'teacherJobTitle' in self.request.POST:
            staffExtRecord.WriteExtention('StaffExtra',
                                         {'staffJobTitle': self.request.POST['teacherJobTitle']})
        newEmailAccountRequest(newStaff, 'staff')
        return newStaff

    def __createMenuType__(self):
        ''' creates the menu type for the new staff member '''
        menuTypesName = '%s%s' % (self.request.POST['school'],
                                  self.request.POST['TeacherOrAdmin'])
        return MenuTypes.objects.get(Name=menuTypesName)

    def __createAddress__(self):
        ''' creates the address for the new staff member '''
        teacherAddress = Address(HomeSalutation=self.request.POST['teacherHomeSalutation'],
                                PostalTitle=self.request.POST['postalTitle'],
                                AddressLine1=self.request.POST['teacherAddressLine1'],
                                AddressLine2=self.request.POST['teacherAddressLine2'],
                                PostCode=self.request.POST['teacherPostCode'])
        if 'teacherAddressLine3' in self.request.POST:
            teacherAddress.AddressLine3 = self.request.POST['teacherAddressLine3']
        if 'teacherAddressLine4' in self.request.POST:
            teacherAddress.AddressLine4 = self.request.POST['teacherAddressLine4']
        if 'teacherCountry' in self.request.POST:
            teacherAddress.Country = self.request.POST['teacherCountry']
        if 'teacherPhone1' in self.request.POST:
            teacherAddress.Phone1 = self.request.POST['teacherPhone1']
        if 'teacherPhone2' in self.request.POST:
            teacherAddress.Phone2 = self.request.POST['teacherPhone2']
        if 'teacherPhone3' in self.request.POST:
            teacherAddress.Phone3 = self.request.POST['teacherPhone3']
        email = self.__generateStaffEmail__(self.request.POST['teacherForename'],
                                              self.request.POST['teacherSurname'])
        teacherAddress.EmailAddress = email
        teacherAddress.save()
        return teacherAddress

    def __generateStaffEmail__(self, firstname, surname):
        ''' Generates a unique Email address for a single staff member'''
        EList = []
        for i in Staff.objects.all():
            EList.append(i.EmailAddress.lower())
            try:
                EList.append(i.Address.EmailAddress.lower())
            except:
                pass
        emailAddressPart2 = "thomas-s.co.uk"
        emailAddressPart1 = '%s%s' % (firstname[0], surname)
        tempEmail = "%s@%s" % (emailAddressPart1, emailAddressPart2)
        counter = 0
        inList = False
        for i in EList:
            if emailAddressPart1.lower() in i.lower():
                inList = True
                counter += 1
        if inList is False:
            staffEmail = tempEmail
        else:
            staffEmail = "%s%d@%s" % (emailAddressPart1,
                                      counter,
                                      emailAddressPart2)
        return staffEmail.lower()
