# Django middleware imports
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.http import HttpResponseBadRequest
from django.http import HttpResponse
from django.core.context_processors import csrf
from django.db.models import Q
from django.utils.html import strip_tags
from django.core.mail import send_mail
from django.utils import simplejson

# WillowTree system imports
from MIS.ViewExtras import SetMenu
from MIS.models import School
from MIS.models import Group
from MIS.models import Staff
from MIS.models import StaffGroup
from MIS.models import Address
from MIS.models import StaffEmergencyContact
from MIS.modules.staff import staffFunctions
from MIS.modules.StaffRecs import StaffRecord
from MIS.modules import StaffRecs
from MIS.modules.ExtendedRecords import *

# Python imports
import csv
import datetime

# logging
import logging
log = logging.getLogger(__name__)


def staffSearchPage(request, school, AcYear):
    ''' Shows page for searching for staff '''
    c = {'school': school, 'AcYear': AcYear}
    contextInstanceData = RequestContext(request, processors=[SetMenu])
    return render_to_response('staffSearch.html', c,
                              context_instance=contextInstanceData)


def staffSearch(request, school, AcYear):
    c = {'school': school, 'AcYear': AcYear}
    err_msg = ''
    if 'SearchText' in request.POST:
        searchText = request.POST['SearchText']
        if len(searchText) > 0:
            if 'active' in request.POST and not 'notActive' in request.POST:
                staffs = Staff.objects.filter(Active=True)
            elif 'notActive' in request.POST and not 'active' in request.POST:
                staffs = Staff.objects.filter(Active=False)
            else:
                staffs = Staff.objects.all()
            for SearchWords in searchText.split():
                staffs = staffs.filter(Q(Forename__icontains=SearchWords) |
                Q(Surname__icontains=SearchWords) |
                Q(NickName__icontains=SearchWords) |
                Q(OtherNames__icontains=SearchWords) |
                Q(id__icontains=SearchWords))
            SearchResults = []
            for staff in staffs:
                SearchResults.append(Staff.objects.get(id=staff.id))
            c.update({'SearchResults': SearchResults,
            'BannerTitle': 'Staff Search'})
            c.update(csrf(request))
    else:
        err_msg = 'No String Found'
    c.update({'err_msg': err_msg})
    contextInstanceData = RequestContext(request, processors=[SetMenu])
    return render_to_response('staffSearchResults.html', c,
                              context_instance=contextInstanceData)


def myStaffPage(request, school, AcYear):
    ''' Returns the staff view of the user logged on '''
    StaffNo = request.session['StaffId'].id
    StaffDetails = StaffRecs.StaffRecord(int(StaffNo), request, AcYear)
    c = {'school': school,
         'AcYear': AcYear,
         'StaffDetails': StaffDetails,
         'myStaffPage': True,
         'path': request.get_full_path}
    # groups staff can be added to from staff page - this will change as
    # per the school menu the admin user is currently in.
    schoolsList1 = ['Battersea', 'Clapham', 'Fulham', 'Kensington']
    thisSchool = MenuTypes.objects.get(Name=school).SchoolId.Name
    schoolsList = [thisSchool]
    for school in schoolsList1:
        if thisSchool != school:
            schoolsList.append(school)
    finalGroups = []
    for school in schoolsList:
        groups = Group.objects.filter(Name__icontains="Staff")
        groups = groups.filter(Name__icontains=school)
        staffGrps = []
        for i in StaffGroup.objects.filter(Staff__id=int(StaffNo)):
            staffGrps.append(i.Group.Name)
        tempGroups = []
        for i in groups:
            inGroup = False
            temp = i.Name
            temp2 = temp.split('.')
            if len(temp2) > 3 or "Staff_Register" in temp:
                if i.Name in staffGrps:
                    inGroup = True
                tempGroups.append([i.Name, inGroup])
        finalGroups.append(tempGroups)
    c.update({'groups': finalGroups})
    # end
    contextInstanceData = RequestContext(request, processors=[SetMenu])
    return render_to_response('StaffRecord.html',
                              c, context_instance=contextInstanceData)


def staffDeactivate(request, school, AcYear, StaffNo):
    ''' Deactivates a staff record '''
    staffObj = Staff.objects.get(id=int(StaffNo), Active=True)
    staffObj.Active = False
    staffObj.save()
    httpAddress = '/WillowTree/%s/%s/staff/%s/'
    tupleData = (school, AcYear, StaffNo)
    return HttpResponseRedirect(httpAddress % tupleData)


def staffActivate(request, school, AcYear, StaffNo):
    ''' Deactivates a staff record '''
    staffObj = Staff.objects.get(id=int(StaffNo), Active=False)
    staffObj.Active = True
    staffObj.save()
    httpAddress = '/WillowTree/%s/%s/staff/%s/'
    tupleData = (school, AcYear, StaffNo)
    return HttpResponseRedirect(httpAddress % tupleData)


def newStaffPage(request, school, AcYear):
    ''' Displays the add a new staff member page. '''
    template = 'staffNew.html'
    contextInstanceData = RequestContext(request, processors=[SetMenu])
    c = {'school': school, 'AcYear': AcYear}
    c.update({'schools': School.objects.all()})
    return render_to_response(template, c,
                              context_instance=contextInstanceData)


def saveStaffDetails(request, school, AcYear):
    ''' This saves the staff School, MenuType, Main details such as Forename
    etc and Address details. '''
    staff = staffFunctions.newStaff(request, AcYear).createNewStaff()
    template = 'staffNewChooseGroups.html'
    contextInstanceData = RequestContext(request, processors=[SetMenu])
    c = {'school': school, 'AcYear': AcYear}
    schoolOnly = request.POST['school']
    groups = Group.objects.filter(Name__icontains="Staff")
    groups = groups.filter(Name__icontains=schoolOnly)
    finalGroups = []
    for i in groups:
        temp = i.Name
        temp2 = temp.split('.')
        if len(temp2) > 3:
            finalGroups.append(i.Name)
    c.update({'groups': finalGroups})
    c.update({'staff': staff})
    try:
        send_mail('New staff member created in WillowTree',
                   'New staff here: https://willowtree.thomas-s.co.uk/WillowTree/BatterseaAdmin/2014-2015/staff/%s/' % str(staff.id),
                   SystemVarData.objects.get(Variable__Name='WillowTree Email Address').Data,
                   [SystemVarData.objects.get(Variable__Name='Active Directory Email').Data],
                   fail_silently=False)
    except Exception as error:
        log.warn("ERROR: Email to Active Directory Email not sent (%s)" % error)
    return render_to_response(template, c,
                              context_instance=contextInstanceData)


def saveStaffGroups(request, school, AcYear, StaffNo, fromStaffPage=False):
    '''
    1) This saves staff groups to the new staff record made from the
       saveStaffDetails view.

    2) Used when modifing staff groups from the
       staff page by admin staff
    '''
    if fromStaffPage:
        for i in StaffGroup.objects.filter(Staff__id=int(StaffNo)):
            currentStaffGrp = i
            currentStaffGrp.delete()
    groups = []
    for key, value in request.POST.iteritems():
        if 'groupCheckBox_' in key:
            groups.append(value)
    staffObj = Staff.objects.get(id=StaffNo)
    for group in groups:
        groupObj = Group.objects.get(Name=group)
        temp = StaffGroup(Staff=staffObj,
                          Group=groupObj,
                          AcademicYear=AcYear)
        temp.save()
    if 'myStaffPage' in request.POST:
        httpAddress = '/WillowTree/%s/%s/myStaffPage/'
        tupleData = (school, AcYear)
        return HttpResponseRedirect(httpAddress % tupleData)
    if fromStaffPage:
        httpAddress = '/WillowTree/%s/%s/staff/%d/'
        tupleData = (school, AcYear, int(StaffNo))
        return HttpResponseRedirect(httpAddress % tupleData)
    else:
        template = 'staffNewFinishPage.html'
        contextInstanceData = RequestContext(request, processors=[SetMenu])
        c = {'school': school, 'AcYear': AcYear}
        c.update({'staff': staffObj})
        return render_to_response(template, c,
                                  context_instance=contextInstanceData)


def EditStaffDetails(request, school, AcYear, StaffNo, mode):
    ''' Updates the fields shown in the staff record, the mode lets this view
    which part of the staff record you wish to update. eg: Main'''
    if mode == 'Main':
        staffObj = StaffRecord(StaffNo, request, AcYear=AcYear)
        staffObj.UpdateBase()
    if mode == 'Address':
        staffObj = StaffRecord(StaffNo, request, AcYear=AcYear)
        staffObj.UpdateAddress()
    if 'teacherJobTitle' in request.POST or 'teacherStartDate' in request.POST or 'staffHouse' in request.POST:
        staffExtRecord = ExtendedRecord(BaseType='Staff',
                                        BaseId=StaffNo,
                                        request=request)
    if 'teacherStartDate' in request.POST:
        if request.POST['teacherStartDate'] == '':
            pass
        else:
            staffExtRecord.WriteExtention('StaffExtra',
                                         {'staffStartDate': request.POST['teacherStartDate']})
    if 'teacherJobTitle' in request.POST:
        staffExtRecord.WriteExtention('StaffExtra',
                                     {'staffJobTitle': request.POST['teacherJobTitle']})
    if 'staffHouse' in request.POST:
        staffExtRecord.WriteExtention('StaffExtra',
                                     {'staffHouse': request.POST['staffHouse']})
    if mode == 'Main':
        emailToRingWoodData = """
        Change Type: Change to Main details...\n\n
        Staff id/Name: %s: %s %s\n\n
        Job Title: %s
        Start Date: %s
        Surname: %s
        Forename: %s
        Other Names: %s
        Known As: %s
        Title: %s
        Gender: %s
        DOB: %s
        Email: %s\n""" % (staffObj.StaffRec.id,
                          staffObj.StaffRec.Forename,
                          staffObj.StaffRec.Surname,
                          staffObj.Extended['staffJobTitle'][0],
                          staffObj.Extended['staffStartDate'][0],
                          staffObj.StaffRec.Surname,
                          staffObj.StaffRec.Forename,
                          staffObj.StaffRec.OtherNames,
                          staffObj.StaffRec.NickName,
                          staffObj.StaffRec.Title,
                          staffObj.StaffRec.Gender,
                          staffObj.StaffRec.DateOfBirth,
                          staffObj.StaffRec.Address.EmailAddress)
    if mode == 'Address':
        emailToRingWoodData = """
        Change Type: Change to Address details...\n
        Staff id/Name: %s: %s %s\n
        Home Salutation: %s
        Postal Title: %s
        Address Line 1: %s
        Address Line 2: %s
        Address Line 3: %s
        Address Line 4: %s
        Country: %s
        Post Code: %s
        Phone 1: %s
        Phone 2: %s
        Phone 3: %s\n """ % (staffObj.StaffRec.id,
                          staffObj.StaffRec.Forename,
                          staffObj.StaffRec.Surname,
                          staffObj.StaffRec.Address.HomeSalutation,
                          staffObj.StaffRec.Address.PostalTitle,
                          staffObj.StaffRec.Address.AddressLine1,
                          staffObj.StaffRec.Address.AddressLine2,
                          staffObj.StaffRec.Address.AddressLine3,
                          staffObj.StaffRec.Address.AddressLine4,
                          staffObj.StaffRec.Address.Country,
                          staffObj.StaffRec.Address.PostCode,
                          staffObj.StaffRec.Address.Phone1,
                          staffObj.StaffRec.Address.Phone2,
                          staffObj.StaffRec.Address.Phone3)
    staffEmailAddress = Staff.objects.get(User__id=request.session['_auth_user_id']).Address.EmailAddress
    sendEmail = staffFunctions.emailRingwoodStaffChanges(emailToRingWoodData,
                                                         staffEmailAddress)
    sendEmail.start()
    if 'myStaffPage' in request.POST:
        httpAddress = '/WillowTree/%s/%s/myStaffPage/'
        tupleData = (school, AcYear)
        return HttpResponseRedirect(httpAddress % tupleData)
    else:
        httpAddress = '/WillowTree/%s/%s/staff/%d/'
        tupleData = (school, AcYear, int(StaffNo))
        return HttpResponseRedirect(httpAddress % tupleData)


def staffModifyMedicalDetails(request, school, AcYear, StaffNo):
    ''' adds or modifies a staffs medical details '''
    staffExtended = ExtendedRecord('Staff', StaffNo)
    staffExtended.WriteExtention('StaffExtra',
                                 {'staff_medical_details': request.POST['staffModifyMedicalDetails']})
    if 'myStaffPage' in request.POST:
        httpAddress = '/WillowTree/%s/%s/myStaffPage/'
        tupleData = (school, AcYear)
        return HttpResponseRedirect(httpAddress % tupleData)
    else:
        httpAddress = '/WillowTree/%s/%s/staff/%d/'
        tupleData = (school, AcYear, int(StaffNo))
        return HttpResponseRedirect(httpAddress % tupleData)


def staffAddContactDetails(request, school, AcYear, StaffNo):
    ''' adds a staffs contact details '''
    contactAddress = Address(HomeSalutation=request.POST['HomeSalutation'],
                             PostalTitle=request.POST['PostalTitle'],
                             AddressLine1=request.POST['AddressLine1'],
                             AddressLine2=request.POST['AddressLine2'],
                             PostCode=request.POST['PostCode'],
                             Phone1=request.POST['Phone1'])
    if 'AddressLine3' in request.POST:
        contactAddress.AddressLine3 = request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        contactAddress.AddressLine4 = request.POST['AddressLine4']
    if 'Country' in request.POST:
        contactAddress.Country = request.POST['Country']
    if 'Phone2' in request.POST:
        contactAddress.Phone2 = request.POST['Phone2']
    if 'Phone3' in request.POST:
        contactAddress.Phone3 = request.POST['Phone3']
    if 'EmailAddress' in request.POST:
        contactAddress.EmailAddress = request.POST['EmailAddress']
    if 'Note' in request.POST:
        contactAddress.Note = request.POST['Note']
    contactAddress.save()
    staffContact = StaffEmergencyContact(StaffId=Staff.objects.get(id=int(StaffNo)),
                                         AddressId=contactAddress,
                                         Priority=int(request.POST['Priority']))
    staffContact.save()
    if 'myStaffPage' in request.POST:
        httpAddress = '/WillowTree/%s/%s/myStaffPage/'
        tupleData = (school, AcYear)
        return HttpResponseRedirect(httpAddress % tupleData)
    else:
        httpAddress = '/WillowTree/%s/%s/staff/%d/'
        tupleData = (school, AcYear, int(StaffNo))
        return HttpResponseRedirect(httpAddress % tupleData)


def staffModifyContactDetails(request, school, AcYear, StaffNo, contactId):
    ''' modify a staffs contact details '''
    contactData = StaffEmergencyContact.objects.get(id=int(contactId))
    contactAddress = Address.objects.get(id=int(contactData.AddressId.id))
    if 'Priority' in request.POST:
        contactData.Priority = request.POST['Priority']
    if 'HomeSalutation' in request.POST:
        contactAddress.HomeSalutation = request.POST['HomeSalutation']
    if 'PostalTitle' in request.POST:
        contactAddress.PostalTitle = request.POST['PostalTitle']
    if 'AddressLine1' in request.POST:
        contactAddress.AddressLine1 = request.POST['AddressLine1']
    if 'AddressLine2' in request.POST:
        contactAddress.AddressLine2 = request.POST['AddressLine2']
    if 'PostCode' in request.POST:
        contactAddress.PostCode = request.POST['PostCode']
    if 'Phone1' in request.POST:
        contactAddress.Phone1 = request.POST['Phone1']
    if 'AddressLine3' in request.POST:
        contactAddress.AddressLine3 = request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        contactAddress.AddressLine4 = request.POST['AddressLine4']
    if 'Country' in request.POST:
        contactAddress.Country = request.POST['Country']
    if 'Phone2' in request.POST:
        contactAddress.Phone2 = request.POST['Phone2']
    if 'Phone3' in request.POST:
        contactAddress.Phone3 = request.POST['Phone3']
    if 'EmailAddress' in request.POST:
        contactAddress.EmailAddress = request.POST['EmailAddress']
    if 'Note' in request.POST:
        contactAddress.Note = request.POST['Note']
    contactAddress.save()
    contactData.save()
    if 'myStaffPage' in request.POST:
        httpAddress = '/WillowTree/%s/%s/myStaffPage/'
        tupleData = (school, AcYear)
        return HttpResponseRedirect(httpAddress % tupleData)
    else:
        httpAddress = '/WillowTree/%s/%s/staff/%d/'
        tupleData = (school, AcYear, int(StaffNo))
        return HttpResponseRedirect(httpAddress % tupleData)


def staffDeleteContactDetails(request, school, AcYear, StaffNo, contactId):
    ''' deletes a staff contact - meh '''
    contactData = StaffEmergencyContact.objects.get(id=int(contactId))
    contactAddress = Address.objects.get(id=int(contactData.AddressId.id))
    contactAddress.delete()
    contactData.delete()
    if 'myStaffPage' in request.POST:
        httpAddress = '/WillowTree/%s/%s/myStaffPage/'
        tupleData = (school, AcYear)
        return HttpResponseRedirect(httpAddress % tupleData)
    else:
        httpAddress = '/WillowTree/%s/%s/staff/%d/'
        tupleData = (school, AcYear, int(StaffNo))
        return HttpResponseRedirect(httpAddress % tupleData)


def staffCpdCsvExtract(request, school, AcYear):
    ''' Creates staff Cpd Csv Extract from staff manager page '''
    today = datetime.datetime.now()
    todaysDate = '%s-%s-%s' % (today.day, today.month, today.year)
    cpdRecords = []
    for staffId in request.POST.getlist('PupilSelect'):
        cpdsForSingleStaff = StaffCpdRecords.objects.filter(Staff__id=staffId,
                                                            Active=True)
        for cpdRecord in cpdsForSingleStaff:
            cpdRecords.append(cpdRecord)
    response= HttpResponse(mimetype="text/csv")
    response_writer= csv.writer(response)
    response_writer.writerow(['Staff Id',
                              'Staff Forename',
                              'Staff Surname',
                              'Course Title',
                              'Name of Provider',
                              'Time Taken',
                              'Date Taken',
                              'Duration',
                              'Details',
                              'Date Expires',
                              'Cert Issued',
                              'Internal/External'])
    for cpdRecord in cpdRecords:
        dateTaken = ''
        try:
            dateTaken = '%s/%s/%s' % (cpdRecord.DateTaken.day,
                                      cpdRecord.DateTaken.month,
                                      cpdRecord.DateTaken.year)
        except:
            pass
        dateExpires = ''
        try:
            dateExpires = '%s/%s/%s' % (cpdRecord.DateExpires.day,
                                        cpdRecord.DateExpires.month,
                                        cpdRecord.DateExpires.year)
        except:
            pass
        response_writer.writerow([cpdRecord.Staff.id,
                                 cpdRecord.Staff.Forename,
                                 cpdRecord.Staff.Surname,
                                 cpdRecord.Name,
                                 cpdRecord.Provider,
                                 cpdRecord.TimeTaken,
                                 dateTaken,
                                 cpdRecord.Duration,
                                 strip_tags(cpdRecord.Details),
                                 dateExpires,
                                 cpdRecord.CertIssued,
                                 cpdRecord.InternalExternal])
    response['Content-Disposition'] = 'attachment; filename=StaffCpdReport_%s.csv' % todaysDate
    return response


def view_staff_groups_for_a_school_as_a_list(request, school, ac_year):
    """
    View staff groups for a school as a list.
    """
    try:
        school_name = MenuTypes.objects.get(Name=school).SchoolId.Name
        staff = Group.objects.filter(Name__icontains='Staff.%s' % school_name).order_by('Name').values_list('Name',
                                                                                                            flat=True)
        return HttpResponse(simplejson.dumps(staff), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)
