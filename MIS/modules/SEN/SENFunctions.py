# django imports
from django.core.cache import cache

# willow tree imports
from MIS.models import Pupil
from MIS.models import PupilProvision
from MIS.models import PupilTarget
from MIS.modules.PupilRecs import PupilObject
from MIS.modules.ExtendedRecords import *

# logging
import logging
log = logging.getLogger(__name__)

def PupilRecordWithSen(PupilId, AcYear):
    Pupilrec = cache.get('PupilSEN_%s' % PupilId)
    if not Pupilrec:
        PupilOb = PupilObjectWithSen(PupilId, AcYear)
        cache.set('PupilSEN_%s' % PupilId, PupilOb, 900)
        return PupilOb
    else:
        return Pupilrec


def updateSenCache(PupilId, AcYear):
    ''' Quick method to update the cached PupilRecordWithSen obj '''
    cache.delete('PupilSEN_%s' % PupilId)
    PupilRecordWithSen(PupilId, AcYear)
    return


class PupilObjectWithSen(PupilObject):
    ''' A child of the pupil object with extended sen methods '''
    def __CacheSelf__(self):
        cache.set('PupilSEN_%s' % self.PupilId, self, 900)
        return 'new sen'

    def ReturnPastSENs(self):
        ''' Returns a list of past SEN alerts for a Pupil '''
        pastSENList = []
        PupilAlerts = self.AllPupilAlerts()
        for i in PupilAlerts:
            for alert in i:
                if alert.Active == False and alert.AlertType.AlertGroup.Name == 'SEND':
                    pastSENList.append(alert)
        return pastSENList

    def ExtendedSen(self):
        ''' Returns a dictionary of SEN extended records '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = ExtendedRecord(BaseType='Pupil',
                                                    BaseId=self.Pupil.id).ReadExtention(ExtentionName='PupilSEN')
            self.__CacheSelf__()
        return self.ExtendedSenRecord

    def returnSenExtraTimeSpecialPermissions(self):
        ''' returns the Extra Time SEN special permission for a pupil'''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senExtraTime'][0]

    def returnSenExtraTimeSpecialPermissionsPickList(self):
        ''' returns the Extra Time SEN special permission pick list for a
        pupil'''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senExtraTime'][2]

    def returnSenLaptopSpecialPermissions(self):
        ''' returns the Laptop SEN special permission for a pupil'''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senLaptop'][0]

    def returnSenLaptopSpecialPermissionsPickList(self):
        ''' returns the Laptop SEN special permission pick list for a pupil'''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senLaptop'][2]

    def returnSenOtherSpecialPermissions(self):
        ''' returns theOther SEN special permission for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senOther'][0]

    def returnSenLiteracy(self):
        ''' returns sen Literacy for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senLiteracy'][0]

    def returnSenNumeracy(self):
        ''' returns sen Numeracy for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senNumeracy'][0]

    def returnSenBehavior(self):
        ''' returns sen Behavior for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senBehavior'][0]

    def returnSenCommunication(self):
        ''' returns sen Communication for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senCommunication'][0]

    def returnSenPhysical(self):
        ''' returns sen Physical for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senPhysical'][0]

    def returnSenGeneral(self):
        ''' returns sen General for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senGeneral'][0]

    def returnSenLiteracyTeacherNotes(self):
        ''' returns sen Literacy TeacherNotes for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senLiteracyTeacherNotes'][0]

    def returnSenNumeracyTeacherNotes(self):
        ''' returns sen Numeracy TeacherNotes for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senNumeracyTeacherNotes'][0]

    def returnSenBehaviorTeacherNotes(self):
        ''' returns sen Behavior TeacherNotes for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senBehaviorTeacherNotes'][0]

    def returnSenCommunicationTeacherNotes(self):
        ''' returns sen Communication TeacherNotes for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senCommunicationTeacherNotes'][0]

    def returnSenPhysicalTeacherNotes(self):
        ''' returns sen Physical TeacherNotes for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senPhysicalTeacherNotes'][0]

    def returnSenGeneralTeacherNotes(self):
        ''' returns sen General TeacherNotes for a pupil '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senGeneralTeacherNotes'][0]

    def returnSENAreasOfConcern(self):
        ''' returns the SEN areas of concern for a pupil '''
        concernDict = []
        areaOfConcernList = ['Literacy',
                             'Numeracy',
                             'Behavior',
                             'Communication',
                             'Physical',
                             'General']
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        for i in areaOfConcernList:
            concernDict.append([i, exRecord['sen%s' % i][0],
                                exRecord['sen%sTeacherNotes' % i][0]])
        return concernDict

    def returnSENAreasOfConcernBool(self):
        ''' returns if a pupil has any SEN areas of concern '''
        concernDict = []
        areaOfConcernList = ['Literacy',
                             'Numeracy',
                             'Behavior',
                             'Communication',
                             'Physical',
                             'General']
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        for i in areaOfConcernList:
            if exRecord['sen%s' % i][0]:
                concernDict.append(exRecord['sen%s' % i][0])
        for i in areaOfConcernList:
            if exRecord['sen%sTeacherNotes' % i][0]:
                concernDict.append(exRecord['sen%sTeacherNotes' % i][0])
        return bool(concernDict)

    def returnSenAssessmentHistory(self):
        ''' returns SEN assessment history '''
        if not self.ExtendedSenRecord:
            self.ExtendedSenRecord = self.ExtendedSen()
        exRecord = self.ExtendedSenRecord
        return exRecord['senAssessmentHistory'][0]

    def senFunctions(self):
        ''' returns extra functions used for sen management '''
        return SenProvisionsTargets(self.PupilId)


class SenProvisionsTargets():
    ''' This Class manages Provisions and Targets for a Pupil '''
    def __init__(self, PupilNo, request=None):
        self.PupilNo = PupilNo
        self.request = request

    def __createPupilObj__(self):
        ''' Creates a pupil object '''
        return Pupil.objects.get(id=self.PupilNo)

    def addProvision(self, request):
        ''' Adds a new sen provision to a pupil '''
        provisionObj = PupilProvision(Pupil=self.__createPupilObj__(),
                                      Type=request.POST['provisiontype'],
                                      SENType=request.POST['senType'],
                                      StartDate=request.POST['startDate'],
                                      StopDate=request.POST['stopDate'])
        if 'provisionText' in request.POST:
            provisionObj.Details = request.POST['provisionText']
        provisionObj.save()
        log.warn('Pupil %s has been given a the new SEN provision id:%s by %s' % (self.PupilNo,
                                                                                  provisionObj.id,
                                                                                  self.request.user.username))
        return

    def returnProvisions(self):
        ''' Returns a dictionary holding active and inactive provisions for a
        child '''
        if PupilProvision.objects.filter(Pupil__id=self.PupilNo,
                                         Type='SEN',
                                         Active=True).exists():
            activeP = PupilProvision.objects.filter(Pupil__id=self.PupilNo,
                                                    Type='SEN',
                                                    Active=True)
        else:
            activeP = False
        if PupilProvision.objects.filter(Pupil__id=self.PupilNo,
                                         Type='SEN',
                                         Active=False).exists():
            inActiveP = PupilProvision.objects.filter(Pupil__id=self.PupilNo,
                                                      Type='SEN',
                                                      Active=False)
        else:
            inActiveP = False
        return {'activeProvisions': activeP, 'inActiveProvisions': inActiveP}

    def modifyProvisionOrTarget(self, request, provisionId,
                                Provision=False,
                                Target=False):
        ''' This modifies a pupils provision or target. To do so the old
        provision or target is set to false and a new one created. This is
        done so we can access old provisions or targets '''
        if Provision:
            obj = PupilProvision.objects.get(id=provisionId)
            self.addProvision(request)
        if Target:
            obj = PupilTarget.objects.get(id=provisionId)
            self.addTarget(request)
        obj.Active = False
        obj.save()
        if Provision:
            log.warn('Pupil %s\'s SEN provision id:%s modified by %s' % (self.PupilNo,
                                                                         obj.id,
                                                                         self.request.user.username))
        if Target:
            log.warn('Pupil %s\'s SEN Target id:%s modified by %s' % (self.PupilNo,
                                                                      obj.id,
                                                                      self.request.user.username))
        return

    def DeleteProvisionOrTarget(self, provisionId,
                                Provision=False,
                                Target=False):
        ''' Sets a pupils provision to False, thus deleting it from the user
        '''
        if Provision:
            obj = PupilProvision.objects.get(id=provisionId)
        if Target:
            obj = PupilTarget.objects.get(id=provisionId)
        obj.Active = False
        obj.save()
        if Provision:
            log.warn('Pupil %s\'s SEN provision id:%s deleted by %s' % (self.PupilNo,
                                                                        obj.id,
                                                                        self.request.user.username))
        if Target:
            log.warn('Pupil %s\'s SEN Target id:%s deleted by %s' % (self.PupilNo,
                                                                     obj.id,
                                                                     self.request.user.username))
        return

    def addTarget(self, request):
        ''' Adds a new sen target to a pupil '''
        targetObj = PupilTarget(Pupil=self.__createPupilObj__(),
                                Type=request.POST['targetType'],
                                ReviewDate=request.POST['reviewDate'])
        if 'targetText' in request.POST:
            targetObj.Target = request.POST['targetText']
        if 'reviewText' in request.POST:
            targetObj.Review = request.POST['reviewText']
        if "Progress" in request.POST:
            targetObj.Progress = request.POST['Progress']
        else:
            targetObj.Progress = "Not Met"
        targetObj.save()
        log.warn('Pupil %s has been given a the new SEN Target id:%s by %s' % (self.PupilNo,
                                                                               targetObj.id,
                                                                               self.request.user.username))
        return

    def returnTargets(self):
        ''' Returns a dictionary holding active and inactive targets for a
        child '''
        if PupilTarget.objects.filter(Pupil__id=self.PupilNo,
                                      Type='SEN',
                                      Active=True).exists():
            activeT = PupilTarget.objects.filter(Pupil__id=self.PupilNo,
                                                 Type='SEN',
                                                 Active=True)
        else:
            activeT = False
        if PupilTarget.objects.filter(Pupil__id=self.PupilNo,
                                      Type='SEN',
                                      Active=False):
            inActiveT = PupilTarget.objects.filter(Pupil__id=self.PupilNo,
                                                   Type='SEN',
                                                   Active=False)
        else:
            inActiveT = False
        return {'activeTargets': activeT, 'inActiveTargets': inActiveT}


class ExtraSenOptions():
    ''' This class saves and extracts extra sen options into a string to be
    saved into extended records  - use keywordsOnly option if only keywords are
    needed '''
    def __init__(self, acYear=False, pupilId=False, keywordsOnly=False):
        if not keywordsOnly:
            self.acYear = acYear
            self.pupilId = pupilId
            self.__extraOptionsInList__()
        self.standard_SEN_search_list = ['OT',
                                        'SaLT',
                                        'Physio',
                                        '1_To_1_Specialist_Teacher',
                                        'DTC',
                                        'Subject_Teacher',
                                        'Counselling',
                                        'Small_Group:_Maths',
                                        'Small_Group:_Reading',
                                        'Small_Group:_Spelling',
                                        'Small_Group:_Co-ordination',
                                        'Small_Group:_Phonics',
                                        'Small_Group:_Handwriting',
                                        'Small_Group:_Comprehension',
                                        'Small_Group:_Social_Skills',
                                        'Touch_Typing_(learning)',
                                        'Hearing_Support',
                                        'Prompter',
                                        'Exam_Scribe',
                                        'Coloured_Overlays',
                                        'Medication']
        self.difficulties_SEN_search_list = ['Dyslexia',
                                            'Dyspraxia',
                                            'Dyscalculia',
                                            'SpLD',
                                            'Severe_Maths_Difficulties',
                                            'Attention_Difficulties',
                                            'Motor_Difficulties',
                                            'ADHD',
                                            'ADD',
                                            'ASD',
                                            'VI_(Visually_Impaired)',
                                            'HI_(Hearing_Impaired)',
                                            'PD_(Physically_Impaired)',
                                            'Visual_Difficulties',
                                            'Auditory_Processing',
                                            'Glasses_Worn',
                                            'Hearing_Support',
                                            'Behavioural',
                                            'Other']

    def __getExtraOptions__(self):
        ''' gets the extended record for senExtraOptions for a pupil '''
        self.extraOptions = ExtendedRecord(BaseType='Pupil',
                                           BaseId=self.pupilId).ReadExtention(ExtentionName='PupilSEN')['senExtraOptions'][0]
        return

    def __extraOptionsInList__(self):
        ''' arranges senExtraOptions into a list '''
        self.__getExtraOptions__()
        if self.extraOptions:
            self.extraOptionsInList = sorted(self.extraOptions.split(','))
            self.hasExtraOptions = True
        else:
            self.extraOptionsInList = ['']
            self.hasExtraOptions = False
        return

    def modifyExtraOptions(self, listOfKeywords):
        ''' modifies senExtraOptions '''
        stringToAdd = ''
        for keyword in listOfKeywords:
            stringToAdd += '%s,' % keyword
        exObj = ExtendedRecord(BaseType='Pupil', BaseId=self.pupilId)
        exObj.WriteExtention('PupilSEN', {'senExtraOptions': stringToAdd})
        updateSenCache(self.pupilId, self.acYear)
        return
