# Django middleware imports
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.utils import simplejson
from django.http import HttpResponseBadRequest
from django.http import HttpResponse


# WillowTree system imports
from MIS.ViewExtras import SetMenu
from MIS.modules.ExtendedRecords import *
from MIS.modules.notes import NoteFunctions
from MIS.modules.SEN import SENFunctions
from MIS.modules.GeneralFunctions import isUserInThisGroup


def PupilSENRecord(request, school, AcYear, PupilId):
    ''' displays a Pupils SEN page '''
    if 'NotesKWordFilter' in request.session:
        del request.session['NotesKWordFilter']
    PupilDetails = SENFunctions.PupilRecordWithSen(int(PupilId), AcYear)
    c = {'PupilDetails': PupilDetails,
         'school': school,
         'AcYear': AcYear,
         'inSEN': isUserInThisGroup(request, 'SEN'),
         'KWList': NoteFunctions.KeywordList('Notes')}
    c.update(NoteFunctions.GetPupilNotes(request,
                                         Pupil_Id=PupilId,
                                         allSenNotes=True))
    provisionsObj = SENFunctions.SenProvisionsTargets(int(PupilId), request)
    c.update(provisionsObj.returnProvisions())
    c.update(provisionsObj.returnTargets())
    c.update({'extraOptions': SENFunctions.ExtraSenOptions(AcYear, PupilId)})
    conInst = RequestContext(request, processors=[SetMenu])
    return render_to_response('PupilSENRecord.html', c,
                              context_instance=conInst)


def pupilModifySENSpecialPermissions(request, school, AcYear, PupilId):
    ''' updates Pupil SEN Special Permissions '''
    senExtraTime = request.POST['senExtraTime']
    senLaptop = request.POST['senLaptop']
    senOther = request.POST['senOther']
    pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=PupilId)
    pupilExtentionRecord.WriteExtention('PupilSEN',
                                        {'senExtraTime': senExtraTime})
    pupilExtentionRecord.WriteExtention('PupilSEN',
                                        {'senLaptop': senLaptop})
    pupilExtentionRecord.WriteExtention('PupilSEN',
                                        {'senOther': senOther})
    SENFunctions.updateSenCache(PupilId, AcYear)
    httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def pupilModifySENAreasOfConcern(request, school, AcYear, PupilId):
    ''' Updates Pupil SEN Areas of Concern '''
    areaOfConcernList = ['senLiteracy',
                         'senNumeracy',
                         'senBehavior',
                         'senCommunication',
                         'senPhysical',
                         'senGeneral',
                         'senLiteracyTeacherNotes',
                         'senNumeracyTeacherNotes',
                         'senBehaviorTeacherNotes',
                         'senCommunicationTeacherNotes',
                         'senPhysicalTeacherNotes',
                         'senGeneralTeacherNotes']
    pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=PupilId)
    for i in areaOfConcernList:
        if i in request.POST:
            pupilExtentionRecord.WriteExtention('PupilSEN',
                                                {i: request.POST[i]})
    SENFunctions.updateSenCache(PupilId, AcYear)
    httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def AddProvision(request, school, AcYear, PupilId):
    ''' Adds a new sen provision to a pupil '''
    provisionsObj = SENFunctions.SenProvisionsTargets(int(PupilId), request)
    provisionsObj.addProvision(request)
    httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def ModifyProvision(request, school, AcYear, PupilId, ProvisionId):
    ''' Modifies a pupils provision '''
    provisionsObj = SENFunctions.SenProvisionsTargets(int(PupilId), request)
    provisionsObj.modifyProvisionOrTarget(request, ProvisionId,
                                          Provision=True)
    httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def DeleteProvision(request, school, AcYear, PupilId, ProvisionId):
    ''' Sets a pupils provision to False, thus deleting it from the user '''
    provisionsObj = SENFunctions.SenProvisionsTargets(int(PupilId), request)
    provisionsObj.DeleteProvisionOrTarget(ProvisionId, Provision=True)
    httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def AddTarget(request, school, AcYear, PupilId):
    ''' Adds a new sen target to a pupil '''
    targetObj = SENFunctions.SenProvisionsTargets(int(PupilId), request)
    targetObj.addTarget(request)
    httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def DeleteTarget(request, school, AcYear, PupilId, ProvisionId):
    ''' Sets a pupils target to False, thus deleting it from the user '''
    targetObj = SENFunctions.SenProvisionsTargets(int(PupilId), request)
    targetObj.DeleteProvisionOrTarget(ProvisionId, Target=True)
    httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def ModifyTarget(request, school, AcYear, PupilId, ProvisionId):
    ''' Modifies a pupils provision '''
    provisionsObj = SENFunctions.SenProvisionsTargets(int(PupilId), request)
    provisionsObj.modifyProvisionOrTarget(request, ProvisionId,
                                          Target=True)
    httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def addAssessmentHistory(request, school, AcYear, PupilId):
    ''' Adds SEN assessment history '''
    pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=PupilId)
    pupilExtentionRecord.WriteExtention('PupilSEN',
                                        {'senAssessmentHistory': request.POST['assessmentText']})
    SENFunctions.PupilRecordWithSen(int(PupilId), AcYear).ExtendedRecord = False
    SENFunctions.updateSenCache(PupilId, AcYear)
    httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def ViewSenProvisionMap(request, school, AcYear, PupilId, mode):
    ''' View a pupils SEN provision mapping report '''
    if mode == 'singlePupil':
        pupils = [PupilId]
    if mode == 'multiplePupil':
        pupils = request.POST.getlist('PupilSelect')
    returnList = []
    for i in pupils:
        temp = []
        temp.append(SENFunctions.PupilRecordWithSen(int(i), AcYear))
        temp.append(NoteFunctions.GetPupilNotes(request,
                                                Pupil_Id=i,
                                                allSenNotes=True))
        temp.append(SENFunctions.ExtraSenOptions(AcYear, PupilId))
        returnList.append(temp)
    c = {'pupilList': returnList}
    conInst = RequestContext(request, processors=[SetMenu])
    return render_to_response('PupilSenMap.html', c,
                              context_instance=conInst)


def modifyExtraOptions(request, school, AcYear, PupilId):
    ''' modifies senExtraOptions for a pupil '''
    keywords = []
    for k, v in request.POST.iteritems():
        if 'extraOptions' in k:
            keywords.append(v)
    modExOptions = SENFunctions.ExtraSenOptions(AcYear, PupilId)
    modExOptions.modifyExtraOptions(keywords)
    httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def view_sen_details_for_a_group(request, school, ac_year, pupil_group):
    """
    View sen details for a group.
    """
    try:
        # get pupil ids for a group
        pupil_ids = PupilGroup.objects.filter(Group__Name=pupil_group,
                                              AcademicYear=ac_year,
                                              Active=True).distinct('Pupil__id').values_list('Pupil__id',
                                                                                             flat=True)

        # collect sen data
        returned_data = list()
        for pupil in pupil_ids:
            pupil_sen_object = SENFunctions.PupilObjectWithSen(pupil, ac_year)
            pupil_name = pupil_sen_object.Pupil.FullName()
            pupil_form = pupil_sen_object.Form()
            if pupil_sen_object.hasAlert('SEND'):
                alert_details = pupil_sen_object.hasAlert('SEND').AlertDetails
            else:
                alert_details = str()
            target_data = []
            if SENFunctions.SenProvisionsTargets(pupil, ac_year).returnTargets()['activeTargets']:
                for target in SENFunctions.SenProvisionsTargets(pupil, ac_year).returnTargets()['activeTargets']:
                    target_data.append({'target': target.Target,
                                        'review': target.Review,
                                        'progress': target.Progress,
                                        'review_date': target.ReviewDate.isoformat()})
            returned_data.append({'pupil_name': pupil_name,
                                  'pupil_form': pupil_form,
                                  'alert_details': alert_details,
                                  'target_data': target_data})

        return HttpResponse(simplejson.dumps(returned_data), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def view_most_able_details_for_a_group(request, school, ac_year, pupil_group):
    """
    View most able details for a group.
    """
    try:
        # get pupil ids for a group
        pupil_ids = PupilGroup.objects.filter(Group__Name=pupil_group,
                                              AcademicYear=ac_year,
                                              Active=True).distinct('Pupil__id').values_list('Pupil__id',
                                                                                             flat=True)

        # collect sen data
        returned_data = list()
        for pupil in pupil_ids:
            pupil_sen_object = SENFunctions.PupilObjectWithSen(pupil, ac_year)
            pupil_name = pupil_sen_object.Pupil.FullName()
            pupil_form = pupil_sen_object.Form()
            most_able_subjects = PupilGiftedTalented.objects.filter(Pupil__id=pupil,
                                                                    Active=True).values_list('Subject__Name',
                                                                                             flat=True)
            target_data = []
            if PupilTarget.objects.filter(Pupil__id=pupil, Type="G/T", Active=True):
                for target in PupilTarget.objects.filter(Pupil__id=pupil, Type="G/T", Active=True):
                    target_data.append({'target': target.Target,
                                        'review': target.Review,
                                        'progress': target.Progress,
                                        'review_date': target.ReviewDate.isoformat()})
            returned_data.append({'pupil_name': pupil_name,
                                  'pupil_form': pupil_form,
                                  'most_able_subjects': most_able_subjects,
                                  'target_data': target_data})
        return HttpResponse(simplejson.dumps(returned_data), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)