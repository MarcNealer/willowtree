from MIS.models import *
from MIS.modules import GeneralFunctions
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from MIS.modules.PotentialGroups.PotentialGroupsFunctions import *

import csv

'''
Roll Over scripts

'''

def RollOverScript(xAcYear,xNewAcYear):
    rollOverRecs=RollOverRule.objects.filter(Active=True)
    for rules in rollOverRecs:
        pg=PotentialGroup(school=None,NewGroup=rules.NewGroup.Name,AcYear=xAcYear,NewAcYear=xNewAcYear)
        pg.RollOver()
    GlobalVariables.Write_SystemVar('CurrentYear',xNewAcYear)


class LeaversList():
    def __init__(self,ThisYear,LastYear,School):
        self.ThisYearsGroup=set(GeneralFunctions.Get_PupilList(ThisYear,"Current.%s" % School))
        self.LastYearGroup=set(GeneralFunctions.Get_PupilList(LastYear,"Current.%s" % School))
        self.ThisYearAll=set(GeneralFunctions.Get_PupilList(ThisYear,"Current"))
        self.ThisYear=ThisYear
        self.LastYear=LastYear
        self.School=School
    def leavers(self):
        outputReport=[]
        leavers_set=self.LastYearGroup.difference(self.ThisYearAll)
        print leavers_set
        for items in leavers_set:
            outputReport.append([items.FullName(),items.id])
        fileobj=open('leavers_%s.csv' % self.School,'wb')
        csvObj=csv.writer(fileobj)
        csvObj.writerows(outputReport)
        fileobj.close()
    def transfers(self):
        outputReport=[]
        leaver_set=self.LastYearGroup.difference(self.ThisYearsGroup)
        transfer_set=leaver_set.intersection(self.ThisYearAll)
        print transfer_set
        for items in transfer_set:
            outputReport.append([items.FullName(),items.id])
        fileobj=open('transfers_%s.csv' % self.School,'wb')
        csvObj=csv.writer(fileobj)
        csvObj.writerows(outputReport)
        fileobj.close()

    def create_new_pupil_houses(self):
        """
        Creates new pupil houses.
        """
        for i in Pupil.objects.all():
            pupil = Pupil.objects.get(id=i.id)
            if PupilGroup.objects.filter(Group__Name__icontains='.House.',
                                         AcademicYear=self.LastYear,
                                         Pupil__id=i.id).exists():
                group = Group.objects.get(Name=PupilGroup.objects.filter(Group__Name__icontains='.House.',
                                                                         AcademicYear=self.LastYear,
                                                                         Pupil__id=i.id)[0].Group.Name)
            if not PupilGroup.objects.filter(Group__Name__icontains=group,
                                             AcademicYear=self.ThisYear,
                                             Pupil__id=i.id).exists():
                new_group = PupilGroup(Group=group, Pupil=pupil, AcademicYear=self.ThisYear)
                new_group.save()