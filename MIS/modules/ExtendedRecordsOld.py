from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date


class ExtendedRecord():
    def __init__(self, BaseType, BaseId, request=None):
        self.BaseType = BaseType
        self.request = request
        if self.BaseType == 'Pupil':
            self.BaseId = Pupil.objects.get(id=int(BaseId))
        elif self.BaseType == 'Contact':
            self.BaseId = Contact.objects.get(id=int(BaseId))
        elif self.BaseType=='Staff':
            self.BaseId=Staff.objects.get(id=int(BaseId))
        elif self.BaseType=='Family':
            self.BaseId=Family.objects.get(id=int(BaseId))
        elif self.BaseType=='School':
            self.BaseId=School.objects.get(id=int(BaseId))
        else:
            return False

    def WriteExtention(self, ExtentionName, ExtentionData, SubjectName=None):
        '''Writes/Updates data in the extentions system.
        Usage

        Create the object with BaseType and Id
        Newobject=ExtendedRecord(request,BaseType='Pupil',BaseId=4)
        Then write data
        NewObject.WriteExtention(ExtentionName='Yr3_EndofTerm_Mich',ExtentionData={#Field:Data},SubjectName=#Optional)
        '''
        # get the extention index id via GetExtentionRecordId
        ExtentionIndexId=self.GetExtentionRecordIdForUpdate(ExtentionName,SubjectName)

        # get a list of the fields for looping through vua GetFields
        for eachField in self.GetFieldList(ExtentionName):

        # loop through the fields.
            if eachField['FieldName'] in ExtentionData:
                #Save each found field Item
                self.SaveFieldData(ExtentionIndexId,eachField,ExtentionData[eachField['FieldName']])
        return

    def ReplaceExtention(self, ExtentionName, ExtentionData, SubjectName=None):
        '''overwrites data in the extentions system.
        Usage

        Create the object with BaseType and Id
        Newobject=ExtendedRecord(request,BaseType='Pupil',BaseId=4)
        Then write data
        NewObject.ReplaceExtention(ExtentionName='Yr3_EndofTerm_Mich',ExtentionData={#Field:Data},SubjectName=#Optional)
        '''
        # get the extention index id via GetExtentionRecordId
        ExtentionIndexId=self.GetExtentionRecordIdForUpdate(ExtentionName,SubjectName)

        # get a list of the fields for looping through vua GetFields
        for eachField in self.GetFieldList(ExtentionName):
            self.DeleteFieldData(ExtentionIndexId,eachField)
        # loop through the fields.
            if eachField['FieldName'] in ExtentionData:
                #Save each found field Item
                self.SaveFieldData(ExtentionIndexId,eachField,ExtentionData[eachField['FieldName']])
        return

    def DeleteExtention(self, ExtentionName, SubjectName=None):
        '''overwrites data in the extentions system.
        Usage

        Create the object with BaseType and Id
        Newobject=ExtendedRecord(request,BaseType='Pupil',BaseId=4)
        Then write data
        NewObject.ReplaceExtention(ExtentionName='Yr3_EndofTerm_Mich',ExtentionData={#Field:Data},SubjectName=#Optional)
        '''
        # get the extention index id via GetExtentionRecordId
        ExtentionIndexId=self.GetExtentionRecordIdForUpdate(ExtentionName,SubjectName)

        # get a list of the fields for looping through vua GetFields
        for eachField in self.GetFieldList(ExtentionName):
            self.DeleteFieldData(ExtentionIndexId,eachField)

        return

    def ReadExtention(self,ExtentionName,SubjectName=None):
        ''' This methon if passed and Extention Name and Subject will return
        a completed dictionary with each key being the the field name and the
        value being a list where element 0 is the data and Element 1 is the #
        alternative value. If there is no alternative value, the a blank is returned'''
        #get the ExtextionIndex Number
        ExtentionIndexId=self.GetExtentionRecordIdForRead(ExtentionName,SubjectName)
        # get a list of the fields for the extentionName
        if type(ExtentionIndexId).__name__=='bool':
            self.WriteExtention(ExtentionName,{},SubjectName)
            ExtentionIndexId=self.GetExtentionRecordIdForRead(ExtentionName,SubjectName)
        ExtentionData={}
        for eachField in self.GetFieldList(ExtentionName):
            #loop through the fields and read the data from the table and store in the dictionary
            ExtentionData[eachField['FieldName']]=self.ReadFieldData(ExtentionIndexId,eachField)
            # Return the completed dictionary
        return ExtentionData

    def ReadExtentionAllSubjects(self,ExtentionName):
        ''' This does a read of all files for a given Exentionname, no matter the subject
        It returns a Dictionary of dictionaries as for ReadExtention, with the Key
        being the subject name. It returns only created records

        '''
        #get the ExtextionIndex Number
        IndexIdList=ExtentionForPupil.objects.filter(PupilId=self.BaseId,ExtentionRecordId__Name=ExtentionName)

        # get a list of the fields for the extentionName
        ExtentionData={}
        for ExtentionIndexId in IndexIdList:
            if ExtentionIndexId.ExtentionSubject==None:
                SubjectName='N/A'
            else:
                SubjectName=ExtentionIndexId.ExtentionSubject
            ExtentionData[SubjectName]={}
            for eachField in self.GetFieldList(ExtentionName):
                #loop through the fields and read the data from the table and store in the dictionary
                ExtentionData[SubjectName][eachField['FieldName']]=self.ReadFieldData(ExtentionIndexId,eachField)
                # Return the completed dictionary
        return ExtentionData
    def GetFieldList(self,ExtentionName):
        ''' Get a list of fields for a selected Extention Record, Returned data
        in a List of Dictionaries, where each Dictionary is of the format
        Fieldname,FieldType,FieldId, Picklist'''
        ThisExtentionRecord=ExtentionRecords.objects.get(Name=ExtentionName)
        ThisFieldsList=[]
        for eachField in ThisExtentionRecord.Fields.all():
            try:
                Picklistdata=eval(eachField.PickList.Data)
            except:
                Picklistdata=[]
            ThisFieldsList.append({'FieldName':eachField.Name,'FieldType':eachField.Type,'FieldId':eachField.id,'PickList':Picklistdata})
        return ThisFieldsList

    def GetExtentionRecordIdForUpdate(self,ExtentionName,SubjectName):
        ''' check to see if the extention record already exists.
        if not, create the base extention record and the extention index.
        Return the extention index id in either case. This also updates
        the updateby and UpdatedOn fields'''
        if self.BaseType=='Pupil':
            if ExtentionForPupil.objects.filter(PupilId=self.BaseId,ExtentionRecordId__Name__iexact=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForPupil.objects.get(PupilId=self.BaseId,ExtentionRecordId__Name__iexact=ExtentionName,ExtentionSubject=SubjectName)
                ThisRecord.ExtentionUpdatedOn=datetime.datetime.now()
                if not self.request==None:
                    ThisRecord.ExtentionUpdatedBy=self.request.session['StaffId']
                ThisRecord.save()
                return ThisRecord
            else:
                NewIndex=ExtentionsIndex(ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName))
                NewIndex.save()
                if not self.request==None:
                    NewRecord=ExtentionForPupil(PupilId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now(),ExtentionUpdatedBy=self.request.session['StaffId'])
                else:
                    NewRecord=ExtentionForPupil(PupilId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now())

                NewRecord.save()
                return NewRecord

        elif self.BaseType=='Staff':
            if ExtentionForStaff.objects.filter(StaffId=self.BaseId,ExtentionRecordId__Name__iexact=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForStaff.objects.get(StaffId=self.BaseId,ExtentionRecordId__Name__iexact=ExtentionName,ExtentionSubject=SubjectName)
                ThisRecord.ExtentionUpdatedOn=datetime.datetime.now()
                if not self.request==None:
                    ThisRecord.ExtentionUpdatedBy=self.request.session['StaffId']
                ThisRecord.save()
                return ThisRecord
            else:
                NewIndex=ExtentionsIndex(ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName))
                NewIndex.save()
                if not self.request==None:
                    NewRecord=ExtentionForStaff(StaffId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now(),ExtentionUpdatedBy=self.request.session['StaffId'])
                else:
                    NewRecord=ExtentionForStaff(StaffId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now())

                NewRecord.save()
                return NewRecord
        elif self.BaseType=='Contact':
            if ExtentionForContact.objects.filter(ContactId=self.BaseId,ExtentionRecordId__Name__iexact=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForContact.objects.get(ContactId=self.BaseId,ExtentionRecordId__Name__iexact=ExtentionName,ExtentionSubject=SubjectName)
                ThisRecord.ExtentionUpdatedOn=datetime.datetime.now()
                if not self.request==None:
                    ThisRecord.ExtentionUpdatedBy=self.request.session['StaffId']
                ThisRecord.save()
                return ThisRecord
            else:
                NewIndex=ExtentionsIndex(ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName))
                NewIndex.save()
                if not self.request==None:
                    NewRecord=ExtentionForContact(ContactId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now(),ExtentionUpdatedBy=self.request.session['StaffId'])
                else:
                    NewRecord=ExtentionForContact(ContactId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now())

                NewRecord.save()
                return NewRecord
        elif self.BaseType=='Family':
            if ExtentionForFamily.objects.filter(FamilyId=self.BaseId,ExtentionRecordId__Name__iexact=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForStaff.objects.get(FamilyId=self.BaseId,ExtentionRecordId__Name__iexact=ExtentionName,ExtentionSubject=SubjectName)
                ThisRecord.ExtentionUpdatedOn=datetime.datetime.now()
                if not self.request==None:
                    ThisRecord.ExtentionUpdatedBy=self.request.session['StaffId']
                ThisRecord.save()
                return ThisRecord
            else:
                NewIndex=ExtentionsIndex(ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName))
                NewIndex.save()
                if not self.request==None:
                    NewRecord=ExtentionForFamily(ContactId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now(),ExtentionUpdatedBy=self.request.session['StaffId'])
                else:
                    NewRecord=ExtentionForFamily(ContactId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now())

                NewRecord.save()
                return NewRecord
        elif self.BaseType=='School':
            if ExtentionForSchool.objects.filter(SchoolId=self.BaseId,ExtentionRecordId__Name__iexact=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForSchool.objects.get(SchoolId=self.BaseId,ExtentionRecordId__Name__iexact=ExtentionName,ExtentionSubject=SubjectName)
                ThisRecord.ExtentionUpdatedOn=datetime.datetime.now()
                if not self.request==None:
                    ThisRecord.ExtentionUpdatedBy=self.request.session['StaffId']
                ThisRecord.save()
                return ThisRecord
            else:
                NewIndex=ExtentionsIndex(ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName))
                NewIndex.save()
                if not self.request==None:
                    NewRecord=ExtentionForSchool(SchoolId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now(),ExtentionUpdatedBy=self.request.session['StaffId'])
                else:
                    NewRecord=ExtentionForSchool(SchoolId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name__iexact=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now())

                NewRecord.save()
                return NewRecord
        else:
            return False
    def GetExtentionRecordIdForRead(self,ExtentionName,SubjectName):
        ''' check to see if the extention record already exists.
        if not, create the base extention record and the extention index.
        Return the extention index id in either case. This also updates
        the updateby and UpdatedOn fields'''
        if self.BaseType=='Pupil':
            if ExtentionForPupil.objects.filter(PupilId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForPupil.objects.get(PupilId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)
                return ThisRecord
            else:
                return False
        elif self.BaseType=='Staff':
            if ExtentionForStaff.objects.filter(StaffId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForStaff.objects.get(StaffId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)
                return ThisRecord
            else:
                return False
        elif self.BaseType=='Contact':
            if ExtentionForContact.objects.filter(ContactId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForContact.objects.get(ContactId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)
                return ThisRecord
            else:
                return False
        elif self.BaseType=='Family':
            if ExtentionForFamily.objects.filter(FamilyId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForFamily.objects.get(FamilyId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)
                return ThisRecord
            else:
                return False
        elif self.BaseType=='School':
            if ExtentionForSchool.objects.filter(SchoolId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForSchool.objects.get(SchoolId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)
                return ThisRecord
            else:
                return False
        else:
            return False

    def SaveFieldData(self,IndexId,FieldDetails,DataElement):
        ''' This method saves a data for a single field. It should be passed
        the data item, the details on the field and the Extention index id
        Usage
        object.SaveFieldData(IndexId=*, fieldDetails=*, DataElement=*)
        '''
        if not DataElement:
            return

        if unicode(DataElement) in unicode(FieldDetails['PickList']) and len(DataElement) > 0:
            try:
                AlternativeDataElement=[y for x,y in FieldDetails['PickList'] if x==(DataElement)][0]
            except:
                AlternativeDataElement='Not in PickList'

        else:
            AlternativeDataElement=''
        if FieldDetails['FieldType']=='String':
            if ExtentionString.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                StringField=ExtentionString.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                StringField.Active=False
                StringField.save()
            StringNewField=ExtentionString(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement,AlternativeValue=AlternativeDataElement)
            StringNewField.save()
        elif FieldDetails['FieldType']=='Numerical':
            if ExtentionNumerical.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                NumField=ExtentionNumerical.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                NumField.Active=False
                NumField.save()
            NumNewField=ExtentionNumerical(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement,AlternativeValue=AlternativeDataElement)
            NumNewField.save()
        elif FieldDetails['FieldType']=='Date':
            if ExtentionDate.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionDate.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
            NewField=ExtentionDate(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement)
            NewField.save()
        elif FieldDetails['FieldType']=='Time':
            if ExtentionTime.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionTime.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
            NewField=ExtentionTime(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement)
            NewField.save()
        elif FieldDetails['FieldType']=='Boolean':
            if ExtentionBoolean.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionBoolean.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
            NewField=ExtentionBoolean(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement)
            NewField.save()
        elif FieldDetails['FieldType']=='TextBox':
            if ExtentionTextBox.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                TextField=ExtentionTextBox.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                TextField.Active=False
                TextField.save()
            NewField=ExtentionTextBox(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement)
            NewField.save()
        else:
            return False

    def ReadFieldData(self,IndexId,FieldDetails):
        ''' Using the passed IndexId and Field ID will return the values for
        the said fields in a list'''
        FieldRecord=ExtentionFields.objects.get(id=int(FieldDetails['FieldId']))
        if FieldDetails['FieldType']=='String':
            if ExtentionString.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionString.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,ThisField.AlternativeValue,eval(FieldRecord.PickList.Data)]
            else:
                return ['','',eval(FieldRecord.PickList.Data)]
        elif FieldDetails['FieldType']=='Numerical':
            if ExtentionNumerical.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionNumerical.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,ThisField.AlternativeValue,eval(FieldRecord.PickList.Data)]
            else:
                return ['','',eval(FieldRecord.PickList.Data)]
        elif FieldDetails['FieldType']=='Date':
            if ExtentionDate.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionDate.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,'',eval(FieldRecord.PickList.Data)]
            else:
                return ['','',eval(FieldRecord.PickList.Data)]
        elif FieldDetails['FieldType']=='Time':
            if ExtentionTime.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionTime.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,'',eval(FieldRecord.PickList.Data)]
            else:
                return ['','',eval(FieldRecord.PickList.Data)]
        elif FieldDetails['FieldType']=='Boolean':
            if ExtentionBoolean.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionBoolean.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,'',eval(FieldRecord.PickList.Data)]
            else:
                return ['','',eval(FieldRecord.PickList.Data)]
        elif FieldDetails['FieldType']=='TextBox':
            if ExtentionTextBox.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionTextBox.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,'',eval(FieldRecord.PickList.Data)]
            else:
                return ['','',eval(FieldRecord.PickList.Data)]
        else:
            return false

    def DeleteFieldData(self,IndexId,FieldDetails):
        ''' This method saves a data for a single field. It should be passed
        the data item, the details on the field and the Extention index id
        Usage
        object.SaveFieldData(IndexId=*, fieldDetails=*, DataElement=*)
        '''
        if FieldDetails['FieldType']=='String':
            if ExtentionString.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                StringField=ExtentionString.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                StringField.Active=False
                StringField.save()
        elif FieldDetails['FieldType']=='Numerical':
            if ExtentionNumerical.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                NumField=ExtentionNumerical.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                NumField.Active=False
                NumField.save()
        elif FieldDetails['FieldType']=='Date':
            if ExtentionDate.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionDate.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
        elif FieldDetails['FieldType']=='Time':
            if ExtentionTime.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionTime.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
        elif FieldDetails['FieldType']=='Boolean':
            if ExtentionBoolean.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionBoolean.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
        elif FieldDetails['FieldType']=='TextBox':
            if ExtentionTextBox.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                TextField=ExtentionTextBox.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                TextField.Active=False
                TextField.save()
        else:
            return False