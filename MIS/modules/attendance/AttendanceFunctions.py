# willowTree imports
from MIS.models import *
from django import template
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from MIS.modules.PupilRecs import *
from MIS.modules import GeneralFunctions
from django.template import Context
from django.template.loader import get_template
from django.core.mail import EmailMessage

# standard imports
import datetime, copy
import threading
from datetime import timedelta, date
import sys
import traceback

# logging
import logging
log = logging.getLogger(__name__)

class DailyAttendance():
    ''' This class is used to create objects used in the Daily attendance
    reports, accessed via the Tasks menu'''
    def __init__(self,RequiredDate,AttendanceGroup):
        self.AttendanceGroup=AttendanceGroup
        self.AttendanceClassName=self.AttendanceGroup.Name.split('.')[-1]
        self.RequiredDate=RequiredDate
        self.AttendanceData=PupilAttendance.objects.filter(AttendanceDate=self.RequiredDate,Group=self.AttendanceGroup,Active=True)
        self.AcYear=GlobalVariables.Get_SystemVar('CurrentYear')
        self.form_tutor_name = self.get_form_tutor_name()

    def get_form_tutor_name(self):
        """
        Returns the form teacher for a class.
        """
        try:
            form_teacher_name = SystemVarData.objects.get(Variable__Name="%s.FormTeacher" % self.AttendanceGroup,
                                                          AcademicYear=self.AcYear).Data
            return form_teacher_name
        except Exception as error:
            return error

    def HasBeenTakenAM(self):
        ''' this method checks to see if the AM attendance for the set group has
        been taken or not. If attendance has been taken, then there would be a record
        in the AttendanceTaken table for this day and AM'''
        if AttendanceTaken.objects.filter(AttendanceDate=self.RequiredDate,Group=self.AttendanceGroup,AttendanceType='D',AmPm='AM').exists():
            return 'Y'
        else:
            return 'N'
    def HasBeenTakenPM(self):
        ''' this method checks to see if the PM attendance for the set group has
        been taken or not. If attendance has been taken, then there would be a record
        in the AttendanceTaken table for this day and PM'''
        if AttendanceTaken.objects.filter(AttendanceDate=self.RequiredDate,Group=self.AttendanceGroup,AttendanceType='D',AmPm='PM').exists():
            return 'Y'
        else:
            return 'N'

    def AuthorisedListAM(self):
        ''' Returns a list of pupils marked as Authorised absence for AM attendance'''
        return self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Authorised')
    def AuthorisedCountAM(self):
        ''' Returns a count of Authorised absences for Am Attendance'''
        return len(self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Authorised'))
    def AuthorisedListPM(self):
        ''' Returns a list of pupils marked as Authorised absence for PM attendance'''
        return self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Authorised')
    def AuthorisedCountPM(self):
        ''' Returns a count of Authorised absences for PM Attendance'''
        return len(self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Authorised'))


    def UnauthorisedListAM(self):
        ''' Returns a list of pupils marked as Unauthorised absence for AM attendance'''
        return self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Unauthorised')
    def UnauthorisedCountAM(self):
        ''' Returns a count of Unauthorised absences for AM Attendance'''
        return len(self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Unauthorised'))
    def UnauthorisedListPM(self):
        ''' Returns a list of pupils marked as Unauthorised absence for PM attendance'''
        return self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Unauthorised')
    def UnauthorisedCountPM(self):
        ''' Returns a count of Unauthorised absences for PM Attendance'''
        return len(self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Unauthorised'))
    def TotalPresentAM(self):
        if self.HasBeenTakenAM()=='Y':
            return len(PupilGroup.objects.filter(Group=self.AttendanceGroup,AcademicYear=self.AcYear,Active=True))-self.AuthorisedCountAM()-self.UnauthorisedCountAM()
        else:
            return 'N/A'
    def TotalPresentPM(self):
        if self.HasBeenTakenPM()=='Y':
            return len(PupilGroup.objects.filter(Group=self.AttendanceGroup,AcademicYear=self.AcYear,Active=True))-self.AuthorisedCountPM()-self.UnauthorisedCountPM()
        else:
            return 'N/A'
    def LateListAM(self):
        ''' Returns a list of pupils marked as Late for AM attendance'''
        return self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Late')
    def LateCountAM(self):
        ''' Returns a count of Lates for AM Attendance'''
        return len(self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Late'))
    def LateListPM(self):
        ''' Returns a list of pupils marked as Late for PM attendance'''
        return self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Late')
    def LateCountPM(self):
        ''' Returns a count of Lates for PM Attendance'''
        return len(self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Late'))

    def Report(self):
        ''' Returns a list of all the Pupils in the Selected group as a list of Dictionaries
        The dictionary is in the for {PupilDetails:PupilDetails, AM:AmAttCode,PM:PMAttCode}'''
        PupilList=GeneralFunctions.Get_PupilList(self.AcYear,GroupNo=self.AttendanceGroup.id)
        AttendanceReport=[]
        for eachpupil in PupilList:
            AttReport={'PupilDetails':eachpupil,'AM':'/','PM':"\\"}
            if self.AttendanceData.filter(Pupil=eachpupil,AmPm='AM').exists():
                AttReport['AM']=self.AttendanceData.filter(Pupil=eachpupil,AmPm='AM')[0].Code.AttendanceCode
            if self.AttendanceData.filter(Pupil=eachpupil,AmPm='PM').exists():
                AttReport['PM']=self.AttendanceData.filter(Pupil=eachpupil,AmPm='PM')[0].Code.AttendanceCode
            AttendanceReport.append(AttReport)

        return sorted(AttendanceReport, key=lambda pupil: pupil['PupilDetails'].Surname.lower())
    @staticmethod
    def Get_ClassList(CurrentMenu):
        ''' A static method that returns all the possible classes, for the set school
        that are maked as requireing attendance to be taken'''
        menuList=Menus.objects.filter(MenuType__Name=CurrentMenu,MenuActions__Name='Attendance')
        return [element.Group for element in menuList]


class PupilAttendanceStats(DailyAttendance):
    ''' Basic Attdendance stats for a Pupil Report'''
    def __init__(self,StartDate,StopDate,Pupil_id):
        self.AttendanceData=PupilAttendance.objects.filter(AttendanceDate__gte=StartDate,
                                                           AttendanceDate__lte=StopDate,
                                                           Pupil__id=Pupil_id,
                                                           Active=True)
    def  AuthorisedCount(self):
        return self.AuthorisedCountAM() + self.AuthorisedCountPM()
    def UnauthorisedCount(self):
        return self.UnauthorisedCountAM() + self.UnauthorisedCountPM()
    def LateCount(self):
        return self.LateCountAM() + self.LateCountPM()

class PupilAttendanceRec(PupilObject):
    ''' This class is a full attendance object for a pupil. It is a child object
    of the pupil object, thusa has access to all pupil methods and properties.
    It is used to produce an attendance report and attendance stats for a given period.
    The periods for this are preset to follow the academic year. Thus there are methods
    prebuilt to follow set periods in the school year.

    Initialise the object with the PupilId and academic year. Then use, Yearly
    Michaelmas, Summer, Lent, MichHalf1, MichHalf2 etc to setup the attendance
    reports. Dates used by these methods are set in the yearly global variables
    setting the start and stop dates for the related periods.
    self.Report will return a list of weeks with each element being a list where
    the first element is the date and the second, the data'''
    def __init__(self,PupilId,AcYear):
        PupilObject.__init__(self,PupilId,AcYear)
        self.YearDates=YearDates(self.AcYear)
        self.NoSchoolDays=AttendanceNotTaken.objects.filter(Code__Code='#',AcademicYear=self.AcYear).order_by('Date')
        self.AttendanceRecords=PupilAttendance.objects.filter(Pupil=self.Pupil,
                                                             Active=True,
                                                             AttendanceDate__gte=self.YearDates.MichStart()).order_by('AttendanceDate')
        self.AttendanceRecords.filter(AttendanceDate__lte=self.YearDates.SummerStop())
    def __BuildAttendanceGrid__(self,date1,date2):
        self.AttendanceGrid={}
        for dates in self.__builddatelist__(self.__FindStartDate__(date1),date2):
            weeklyGrid=[]
            for weekdays in range(0,5):
                if dates+datetime.timedelta(weekdays) >= date1 and dates+datetime.timedelta(weekdays) <= date2:
                    weeklyGrid.append('/')
                    weeklyGrid.append('/')
                else:
                    weeklyGrid.append('#')
                    weeklyGrid.append('#')
            self.AttendanceGrid[dates]=copy.deepcopy(weeklyGrid)
        return self.AttendanceGrid # added by roy 08/10/13

    def __FindStartDate__(self,date1):
        while not date1.weekday() == 0:
            date1=date1-datetime.timedelta(1)
        return date1
    def __FindStopDate__(self,date1):
        while not date1.weekday() == 4:
            date1=date1+datetime.timedelta(1)
        return date1
    def __builddatelist__(self,date1,date2):
        if date1 > date2:
            return[]
        else:
            return [date1] + self.__builddatelist__(date1+datetime.timedelta(7),date2)
    def __AddAttendanceRecords__(self,date1,date2):
        AttRecords=self.AttendanceRecords.filter(AttendanceDate__gte=self.__FindStartDate__(date1))
        AttRecords=AttRecords.filter(AttendanceDate__lte=date2)
        for recs in AttRecords:
            for dates in self.AttendanceGrid:
                if dates.date() == self.__FindStartDate__(recs.AttendanceDate):
                    Attday = (recs.AttendanceDate.weekday()) * 2
                    if recs.AmPm=='PM':
                        Attday+=1
                    self.AttendanceGrid[dates][Attday]=recs.Code.AttendanceCode
    def __AddNonSchoolDays__(self,date1,date2):
        NonSchoolDays=self.NoSchoolDays.filter(Date__gte=date1).filter(Date__lte=date2)
        for days in NonSchoolDays:
            for dates in self.AttendanceGrid:
                if dates.date() == self.__FindStartDate__(days.Date):
                    Attday = (days.Date.weekday())*2
                    if Attday < 10:
                        if days.Type=='AM':
                            self.AttendanceGrid[dates][Attday]='#'
                        elif days.Type=='PM':
                            self.AttendanceGrid[dates][Attday+1]='#'
                        else:
                            self.AttendanceGrid[dates][Attday]='#'
                            self.AttendanceGrid[dates][Attday+1]='#'
    def __RmFutureDates__(self):
        timeNow=datetime.datetime.now()
        for daterows in self.AttendanceGrid:
            if timeNow < daterows:
                self.AttendanceGrid[daterows]=['#','#','#','#','#','#','#','#','#','#']
            elif daterows == self.__FindStartDate__(timeNow.date()):
                for index in range(timeNow.weekday()*2+2,10):
                    self.AttendanceGrid[daterows][index]='#'
    def __BuildAttendanceReport__(self,Date1,Date2):
        self.__BuildAttendanceGrid__(Date1,Date2)
        self.__AddAttendanceRecords__(Date1,Date2)
        self.__AddNonSchoolDays__(Date1,Date2)
        self.__RmFutureDates__()
        return self.AttendanceGrid

    def __formatdata__(self,data):
        datelist=[x for x in data]
        datelist.sort()
        returndata=[]
        for items in datelist:
            returndata.append([items,data[items]])
        self.Stats=AttendanceStats(returndata)
        self.Report=returndata
        return returndata

    def these_dates(self, start, stop):
        ''' Builds Attendance data for set dates '''
        start_date = datetime.datetime.strptime(str(start), '%Y-%m-%d')
        stop_date = datetime.datetime.strptime(str(stop), '%Y-%m-%d')
        stop_date += datetime.timedelta(days=1)
        self.__formatdata__(self.__BuildAttendanceReport__(start_date, stop_date))
        return

    def Yearly(self):
        ''' Builds Attendance data for the Whole Academic Year'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.MichStart(),self.YearDates.SummerStop()))
        return
    def Michaelmas(self):
        ''' Builds Attdendance data for the Whole Michaelmas Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.MichStart(),self.YearDates.MichStop()))
        return
    def Lent(self):
        ''' Builds Attendance data for the Whole Lent Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.LentStart(),self.YearDates.LentStop()))
        return
    def Summer(self):
        ''' Builds Attendance data for the Whole Summer Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.SummerStart(),self.YearDates.SummerStop()))
        return
    def MichaelmasHalf1(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.MichStart(),self.YearDates.MichHalfStart()))
        return
    def MichaelmasHalf2(self):
        ''' Builds Attendance data for the Second half of the Mich term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.MichHalfStop(),self.YearDates.MichStop()))
        return
    def LentHalf1(self):
        ''' Builds Attendance data for the first hald of the Lent Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.LentStart(),self.YearDates.LentHalfStart()))
        return
    def LentHalf2(self):
        ''' Builds Attendance data for the second half of the Lent Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.LentHalfStop(),self.YearDates.LentStop()))
        return
    def SummerHalf1(self):
        ''' BUilds Attendance data for the First half of the Summer Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.SummerStart(),self.YearDates.SummerHalfStart()))
        return
    def SummerHalf2(self):
        ''' Builds Attendance data for the second half of the summer Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.SummerHalfStop(),self.YearDates.SummerStop()))
        return
    def September(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.MichStart(),
                                                           datetime.datetime(int(self.AcYear.split('-')[0]),10,1)))
        return
    def October(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(datetime.datetime(int(self.AcYear.split('-')[0]),10,1),
                                                           datetime.datetime(int(self.AcYear.split('-')[0]),11,1)))
        return
    def November(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(datetime.datetime(int(self.AcYear.split('-')[0]),11,1),
                                                           datetime.datetime(int(self.AcYear.split('-')[0]),12,1)))
        return
    def December(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(datetime.datetime(int(self.AcYear.split('-')[0]),12,1),
                                                           self.YearDates.MichStop()))
        return
    def January(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.YearDates.LentStart(),
                                                           datetime.datetime(int(self.AcYear.split('-')[1]),2,1)))
        return
    def February(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(datetime.datetime(int(self.AcYear.split('-')[1]),2,1),
                                                           datetime.datetime(int(self.AcYear.split('-')[1]),3,1)))
        return
    def March(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(datetime.datetime(int(self.AcYear.split('-')[1]),3,1),
                                                           datetime.datetime(int(self.AcYear.split('-')[1]),4,1)))
        return
    def April(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(datetime.datetime(int(self.AcYear.split('-')[1]),4,1),
                                                           datetime.datetime(int(self.AcYear.split('-')[1]),5,1)))
        return
    def May(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(datetime.datetime(int(self.AcYear.split('-')[1]),5,1),
                                                           datetime.datetime(int(self.AcYear.split('-')[1]),6,1)))
        return
    def June(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(datetime.datetime(int(self.AcYear.split('-')[1]),6,1),
                                                           datetime.datetime(int(self.AcYear.split('-')[1]),7,1)))
        return
    def July(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(datetime.datetime(int(self.AcYear.split('-')[1]),7,1),
                                                           self.YearDates.SummerStop()))
        return
    def ThisWeek(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.__FindStartDate__(datetime.datetime.today()),
                                                           self.__FindStopDate__(datetime.datetime.today())))
        return
    def LastWeek(self):
        ''' Builds Attendance data for the First half of the Mich Term'''
        self.__formatdata__(self.__BuildAttendanceReport__(self.__FindStartDate__(datetime.datetime.today())-datetime.timedelta(days=7),
                                                           self.__FindStopDate__(datetime.datetime.today())-datetime.timedelta(days=7)))
        return

class AttendanceStats():
    ''' This class is used from within the PupilAttdenanceRec class to
    calculate and return the stats for a pupils attendance '''
    def __init__(self, AttendanceReport):
        self.Report=AttendanceReport
        self.codeList=AttendanceCode.objects.all()
        self.__countSchoolDays__()
        self.__countAbsences__()
    def __countSchoolDays__(self):
        self.schooldayCount=0
        self.schoolHolidays=0
        for days in self.Report:
            for AttendanceItem in days[1]:
                if not AttendanceItem=='#':
                    self.schooldayCount+=1
                elif AttendanceItem=='#':
                    self.schoolHolidays+=1
    def __countAbsences__(self):
        self.absenceCount={}
        for days in self.Report:
            for AttendanceItem in days[1]:
                if not (AttendanceItem=='/' or AttendanceItem=='#'):
                    if AttendanceItem in self.absenceCount:
                        self.absenceCount[AttendanceItem]+=1
                    else:
                        self.absenceCount[AttendanceItem]=1
    @property
    def NumberOfSessions(self):
        ''' Returns the number of possible attendances'''
        return self.schooldayCount
    @property
    def NonStatAbsenceCount(self):
        ''' returns a the number of non statistical absences'''
        count=float(0)
        for code in self.codeList:
            if code.AttendanceStatistical and ('Authorised' in code.AttendanceCodeType or 'Unauthorised' in code.AttendanceCodeType):
                if code.AttendanceCode  in self.absenceCount:
                    count+=self.absenceCount[code.AttendanceCode]
        return count
    @property
    def NonStatAbsencePercentage(self):
        '''Returns the percentage of Non Staticical abcences'''
        Count=self.NonStatAbsenceCount
        if Count > 0:
            return Count/self.schooldayCount * 100
        else:
            return Count
    @property
    def StatisticalAbsenceCount(self):
        ''' Returns the number of Statisical Absences'''
        count=float(0)
        count=self.ASAbsencesCount+ self.USAbsencesCount
        return count
    @property
    def StatisticalAbsencePercentage(self):
        Count=self.StatisticalAbsenceCount
        if Count > 0:
            return Count/self.schooldayCount * 100
        else:
            return Count
    @property
    def TotalAbsencesCount(self):
        ''' Returns the total number of absences'''
        count=float(0)
        for code in self.codeList:
            if 'Authorised' in code.AttendanceCodeType or 'Unauthorised' in code.AttendanceCodeType:
                if code.AttendanceCode  in self.absenceCount:
                    count+=self.absenceCount[code.AttendanceCode]
        return count
    @property
    def TotalAbsencesPercentage(self):
        ''' Returns the percentages of total absences'''
        Count=self.TotalAbsencesCount
        if Count > 0:
            return Count/self.schooldayCount * 100
        else:
            return Count
    @property
    def ASAbsencesCount(self):
        ''' returns the number of Authorised Statsicical absences'''
        count=float(0)
        for code in self.codeList:
            if code.AttendanceStatistical and 'Authorised' in code.AttendanceCodeType:
                if code.AttendanceCode  in self.absenceCount:
                    count+=self.absenceCount[code.AttendanceCode]
        return count
    @property
    def ASAbsencesPercentage(self):
        ''' returns the percentages of Authorised statisitcal absences'''
        Count=self.ASAbsencesCount
        if Count > 0:
            return Count/self.schooldayCount * 100
        else:
            return Count
    @property
    def USAbsencesCount(self):
        ''' Returns the number of Unauthorised Staticical Absences'''
        count=float(0)
        for code in self.codeList:
            if code.AttendanceStatistical and 'Unauthorised' in code.AttendanceCodeType:
                if code.AttendanceCode  in self.absenceCount:
                    count+=self.absenceCount[code.AttendanceCode]
        return count
    @property
    def USAbsencesPercentage(self):
        '''Returns the percentages of Unauthroised Statstical absences '''
        Count=self.USAbsencesCount
        if Count > 0:
            return Count/self.schooldayCount * 100
        else:
            return Count
    @property
    def AttendedWithNonStatCount(self):
        '''Returns the number of Attdenances minus the staticical absences'''
        return self.schooldayCount - self.StatisticalAbsenceCount
    @property
    def AttendedWithNonStatPercentage(self):
        '''Returns a Percentage of Attendances minus the statistcal absences'''
        Count=self.AttendedWithNonStatCount
        if Count > 0:
            return Count/self.schooldayCount * 100
        else:
            return Count
    @property
    def AttendedExcludeNonStatCount(self):
        '''Number of attendances minus all absences'''
        return (self.schooldayCount - self.NonStatAbsenceCount)
    @property
    def AttendedExcludeNonStatPercentage(self):
        '''Percentages of \attendances minus all absences'''
        Count=self.AttendedExcludeNonStatCount
        if Count > 0:
            return Count/self.schooldayCount * 100
        else:
            return Count
    @property
    def AuthorisedLatesCount(self):
        '''Number of authorised lates'''
        count=float(0)
        for code in self.codeList:
            if code.AttendanceStatistical and 'Late' in code.AttendanceCodeType:
                if code.AttendanceCode  in self.absenceCount:
                    count+=self.absenceCount[code.AttendanceCode]
        return count
    @property
    def AuthorisedLatesPercentage(self):
        '''Percenages of authorised lates'''
        Count=self.AuthorisedLatesCount
        if Count > 0:
            return Count/self.schooldayCount * 100
        else:
            return Count
    @property
    def UnauthorisedLatesCount(self):
        '''Count of unaurthoised lates'''
        count=float(0)
        for code in self.codeList:
            if not code.AttendanceStatistical and 'Late' in code.AttendanceCodeType:
                if code.AttendanceCode  in self.absenceCount:
                    count+=self.absenceCount[code.AttendanceCode]
        return count
    @property
    def UnauthorisedLatesPercentage(self):
        '''Unauthorised lates as a percentage'''
        Count=self.UnauthorisedLatesCount
        if Count > 0:
            return Count/self.schooldayCount * 100
        else:
            return Count

class AttendanceReport:
    def __init__(self,request,AcYear,GroupName):
        self.request=request
        self.AcYear=AcYear
        self.GroupName=GroupName
        try:
            self.TimePeriod=self.request.POST['TimePeriod']
        except:
            self.TimePeriod='N/A'
        if 'DailyAttendance' in self.request.POST['ReportName']:
            self.__DailyReport__()
        else:
            self.DailyReport=[]
            self.__setPupilAttData__()
            self.__setGroupStatsData__()
    @property
    def ReportTemplate(self):
        return 'Reports/%s' % self.request.POST['ReportName']
    def FullDateList(self):
        report=self.PupilList[0].Report
        datelist=[]
        for recs in report:
            datelist.append(recs[0])
            datelist.append(recs[0]+datetime.timedelta(days=1))
            datelist.append(recs[0]+datetime.timedelta(days=2))
            datelist.append(recs[0]+datetime.timedelta(days=3))
            datelist.append(recs[0]+datetime.timedelta(days=4))
        return datelist
    def FullList(self):
        reportdata=[]
        for reports in self.PupilList:
            attdataline=[]
            for attdata in reports.Report:
                attdataline+=attdata[1]
            reportdata.append([reports.Pupil.FullName(),attdataline,
                            reports.Stats.NumberOfSessions,reports.Stats.StatisticalAbsenceCount,
                            reports.Stats.AttendedExcludeNonStatPercentage])
        reportdata.sort(key=lambda x: x[4])
        return reportdata


    def __DailyReport__(self):
        self.DailyReport=DailyAttendance(datetime.datetime.today().date(),Group.objects.get(Name=self.GroupName))
    def __setPupilAttData__(self):
        self.PupilList=[]
        for PupilId in self.request.POST.getlist('PupilSelect'):
            record=PupilAttendanceRec(PupilId,self.AcYear)
            TP=self.request.POST['TimePeriod']
            if 'Yearly' in TP:
                record.Yearly()
            elif TP=='Michaelmas':
                record.Michaelmas()
            elif TP=='Lent':
                record.Lent()
            elif TP=='Summer':
                record.Summer
            elif TP=='Mich1':
                record.MichaelmasHalf1()
            elif TP=='Mich2':
                record.MichaelmasHalf2()
            elif TP=='Lent1':
                record.LentHalf1()
            elif TP=='Lent2':
                record.LentHalf2()
            elif TP=='Summer1':
                record.SummerHalf1()
            elif TP=='Summer2':
                record.SummerHalf2()
            elif TP=='September':
                record.September()
            elif TP=='October':
                record.October()
            elif TP=='November':
                record.November()
            elif TP=='December':
                record.December()
            elif TP=='January':
                record.January()
            elif TP=='February':
                record.February()
            elif TP=='March':
                record.March()
            elif TP=='April':
                record.April()
            elif TP=='May':
                record.May()
            elif TP=='June':
                record.June()
            elif TP=='July':
                record.July()
            elif TP=='ThisWeek':
                record.ThisWeek()
            elif TP=='LastWeek':
                record.LastWeek()
            self.PupilList.append(copy.deepcopy(record))
        return
    def __setGroupStatsData__(self):
        self.Stats=GroupAttendanceStats(self.PupilList)

class GroupAttendanceStats():
    ''' This class is used from within the PupilAttdenanceRec class to
    calculate and return the stats for a pupils attendance '''
    def __init__(self, ClassList):
        self.ClassList=ClassList
        self.ClassCount=len(self.ClassList)
        if self.ClassCount > 0:
            self.Class=True
        else:
            self.Class=False

    @property
    def NumberOfSessions(self):
        ''' Returns the number of possible attendances'''
        if self.Class:
            return self.ClassList[0].Stats.schooldayCount
        else:
            return
    @property
    def NonStatAbsenceCount(self):
        ''' returns a the number of non statistical absences'''
        if self.Class:
            Count=0
            for recs in self.ClassList:
                Count+=recs.Stats.NonStatAbsenceCount
            return Count/self.ClassCount
        else:
            return 0

    @property
    def NonStatAbsencePercentage(self):
        '''Returns the percentage of Non Staticical abcences'''
        Count=self.NonStatAbsenceCount
        if Count > 0:
            return Count/self.NumberOfSessions * 100
        else:
            return Count
    @property
    def StatisticalAbsenceCount(self):
        ''' Returns the number of Statisical Absences'''
        if self.Class:
            Count=0
            for recs in self.ClassList:
                Count+=recs.Stats.StatisticalAbsenceCount
            return Count/self.ClassCount
        else:
            return 0
    @property
    def StatisticalAbsencePercentage(self):
        if self.Class:
            Count=self.StatisticalAbsenceCount
            if Count > 0:
                return Count/self.NumberOfSessions * 100
            else:
                return Count
        else:
            return 0
    @property
    def TotalAbsencesCount(self):
        ''' Returns the total number of absences'''
        if self.Class:
            Count=0
            for recs in self.ClassList:
                Count+=recs.Stats.TotalAbsencesCount
            return Count/self.ClassCount
        else:
            return 0
    @property
    def TotalAbsencesPercentage(self):
        ''' Returns the percentages of total absences'''
        Count=self.TotalAbsencesCount
        if Count > 0:
            return Count/self.NumberOfSessions * 100
        else:
            return Count
    @property
    def ASAbsencesCount(self):
        ''' returns the number of Authorised Statsicical absences'''
        if self.Class:
            Count=0
            for recs in self.ClassList:
                Count+=recs.Stats.ASAbsencesCount
            return Count/self.ClassCount
        else:
            return 0
    @property
    def ASAbsencesPercentage(self):
        ''' returns the percentages of Authorised statisitcal absences'''
        if self.Class:
            Count=self.ASAbsencesCount
            if Count > 0:
                return Count/self.NumberOfSessions * 100
            else:
                return Count
        else:
            return 0
    @property
    def USAbsencesCount(self):
        ''' Returns the number of Unauthorised Staticical Absences'''
        if self.Class:
            Count=0
            for recs in self.ClassList:
                Count+=recs.Stats.USAbsencesCount
            return Count/self.ClassCount
        else:
            return 0
    @property
    def USAbsencesPercentage(self):
        '''Returns the percentages of Unauthroised Statstical absences '''
        if self.Class:
            Count=self.USAbsencesCount
            if Count > 0:
                return Count/self.NumberOfSessions * 100
            else:
                return Count
        else:
            return 0
    @property
    def AttendedWithNonStatCount(self):
        '''Returns the number of Attdenances minus the staticical absences'''
        return self.NumberOfSessions - self.StatisticalAbsenceCount
    @property
    def AttendedWithNonStatPercentage(self):
        '''Returns a Percentage of Attendances minus the statistcal absences'''
        if self.Class:
            Count=self.AttendedWithNonStatCount
            if Count > 0:
                return Count/self.NumberOfSessions * 100
            else:
                return Count
        else:
            return 0
    @property
    def AttendedExcludeNonStatCount(self):
        '''Number of attendances minus all absences'''
        return (self.schooldayCount - self.StatisticalAbsenceCount - self.NonStatAbsenceCount)
    @property
    def AttendedExcludeNonStatPercentage(self):
        '''Percentages of \attendances minus all absences'''
        if self.Class:
            Count=self.AttendedExcludeNonStatCount
            if Count > 0:
                return Count/self.NumberOfSessions * 100
            else:
                return Count
        else:
            return 0
    @property
    def AuthorisedLatesCount(self):
        '''Number of authorised lates'''
        if self.Class:
            Count=0
            for recs in self.ClassList:
                Count+=recs.Stats.AuthorisedLatesCount
            return Count/self.ClassCount
        else:
            return 0
    @property
    def AuthorisedLatesPercentage(self):
        '''Percenages of authorised lates'''
        if self.Class:
            Count=self.AuthorisedLatesCount
            if Count > 0:
                return Count/self.NumberOfSessions * 100
            else:
                return Count
        else:
            return 0
    @property
    def UnauthorisedLatesCount(self):
        '''Count of unaurthoised lates'''
        if self.Class:
            Count=0
            for recs in self.ClassList:
                Count+=recs.Stats.UnauthorisedLatesCount
            return Count/self.ClassCount
        else:
            return 0
    @property
    def UnauthorisedLatesPercentage(self):
        '''Unauthorised lates as a percentage'''
        if self.Class:
            Count=self.UnauthorisedLatesCount
            if Count > 0:
                return Count/self.NumberOfSessions * 100
            else:
                return Count
        else:
            return 0


class WholeSchoolAtt(threading.Thread):
    ''' Generate a whole school report for a set date range '''
    def __init__(self, acYear, schoolMenu, dateRange, userObj):
        threading.Thread.__init__(self)
        self.todaysDate()
        self.acYear = acYear
        self.school = MenuTypes.objects.get(Name=schoolMenu).SchoolId.Name
        if type(dateRange) is tuple:
            self.dateRange = "%s --> %s" % (str(dateRange[0]), str(dateRange[1]))
            self.date_range_is_a_tuple = dateRange
        else:
            self.dateRange = dateRange
            self.date_range_is_a_tuple = False
        self.userObj = userObj
        self.classGrps = self.__getPupilGroupsForSchool__()

    def todaysDate(self):
        ''' Todays date in UK format '''
        today = datetime.datetime.now()
        self.todaysDate = '%s-%s-%s' % (today.day, today.month, today.year)
        return

    def __getPupilGroupsForSchool__(self):
        ''' gets pupils for the whole school and year grps for a school '''
        returnData = []
        returnData.append(Group.objects.get(Name='Current.%s' % self.school))
        for i in Group.objects.filter(Name__icontains='Current.%s' % self.school):
            temp = i.Name
            if len(temp.split('.')) == 4 and 'House' not in i.Name:
                returnData.append(Group.objects.get(Name=i.Name))
        for i in GeneralFunctions.getPupilSchoolClassGrps('%sAdmin' % self.school):
            returnData.append(i)
        return returnData

    def __renderHtml__(self):
        ''' render html for report '''
        template = get_template('Reports/wholeSchoolAttendanceReport1.html')
        context = Context({'todaysDate': self.todaysDate,
                           'returnData': self.returnData,
                           'school': self.school,
                           'dateRange': self.dateRange})
        renderedHtml = template.render(context)
        return renderedHtml

    def __emailUserReports__(self, htmlAttachedFile):
        ''' email reports to the user '''
        if self.userObj.EmailAddress:
            msg=EmailMessage('WILLOWTREE: Whole school attendance report for %s' % self.school,
                             'MSG FROM WILLOWTREE:\n\nWhole school attendance report for %s %s attached' % (self.school, self.dateRange),
                             SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data,
                             [self.userObj.EmailAddress])
            msg.attach('Attendance_Report_%s.html' % self.school,
                       htmlAttachedFile)
            msg.send()
        return

    def run(self):
        ''' gets attendance data formatted for html template '''
        try:
            toAddToTempHeader = ['',
                                 'Tot',
                                 'Boys',
                                 'Girls',
                                 'Poss(T)',
                                 'Poss(B)',
                                 'Poss(G)',
                                 'Auth(T)',
                                 'Auth(B)',
                                 'Auth(G)',
                                 'Unauth(T)',
                                 'Unauth(B)',
                                 'Unauth(G)',
                                 'Late(T)',
                                 'Late(B)',
                                 'Late(G)',
                                 'Attends',
                                 '% Att']
            returnData = [toAddToTempHeader]
            for grp in self.classGrps:
                temp = []
                totalBoysGirls = 0
                boys = 0
                girls = 0
                possTotal = 0
                possBoys = 0
                possGirls = 0
                authTotal = 0
                authBoys = 0
                authGirls = 0
                unAuthTotal = 0
                unAuthBoys = 0
                unAuthGirls = 0
                lateTotal = 0
                lateBoys = 0
                lateGirls = 0
                attends = 0
                StatisticalAbsenceCount = 0
                for pupil in GeneralFunctions.Get_PupilList(self.acYear,
                                                            grp.Name):
                    pupilStatsObj = PupilAttendanceRec(pupil.id, self.acYear)
                    if self.date_range_is_a_tuple:
                        pupilStatsObj.these_dates(self.date_range_is_a_tuple[0],
                                                  self.date_range_is_a_tuple[1])
                    else:
                        eval("pupilStatsObj.%s()" % self.dateRange)
                    temp2 = []
                    # totalBoysGirls
                    totalBoysGirls += 1
                    # boys + girls
                    if pupilStatsObj.Pupil.Gender == 'M':
                        boys += 1
                    else:
                        girls += 1
                    # possTotal
                    possTotal = pupilStatsObj.Stats.NumberOfSessions
                    # possBoys + girls
                    if pupilStatsObj.Pupil.Gender == 'M':
                        possBoys
                    else:
                        possGirls
                    # authTotal
                    authTotal += pupilStatsObj.Stats.ASAbsencesCount
                    # authBoys + girls
                    if pupilStatsObj.Pupil.Gender == 'M':
                        authBoys += pupilStatsObj.Stats.ASAbsencesCount
                    else:
                        authGirls += pupilStatsObj.Stats.ASAbsencesCount
                    # unAuthTotal
                    unAuthTotal += pupilStatsObj.Stats.USAbsencesCount
                    # unAuthBoys + girls
                    if pupilStatsObj.Pupil.Gender == 'M':
                        unAuthBoys += pupilStatsObj.Stats.USAbsencesCount
                    else:
                        unAuthGirls += pupilStatsObj.Stats.USAbsencesCount
                    # lateTotal
                    lates = pupilStatsObj.Stats.AuthorisedLatesCount + pupilStatsObj.Stats.UnauthorisedLatesCount
                    lateTotal += lates
                    # lateBoys + girls
                    if pupilStatsObj.Pupil.Gender == 'M':
                        lates = pupilStatsObj.Stats.AuthorisedLatesCount + pupilStatsObj.Stats.UnauthorisedLatesCount
                        lateBoys += lates
                    else:
                        lates = pupilStatsObj.Stats.AuthorisedLatesCount + pupilStatsObj.Stats.UnauthorisedLatesCount
                        lateGirls += lates
                    # StatisticalAbsenceCount
                    StatisticalAbsenceCount += pupilStatsObj.Stats.StatisticalAbsenceCount
                # attends
                attends = (possTotal * totalBoysGirls) - StatisticalAbsenceCount
                # working out percent att
                p = possTotal * totalBoysGirls
                try:
                    finalAttPercentFloat = float(100 - ((float(p)-float(attends))/float(attends) * 100))
                    finalAttPercent = "%.1f" % finalAttPercentFloat
                except:
                    finalAttPercent = 'error'
                temp = [grp.Name.split('.')[-1],
                        totalBoysGirls,
                        boys,
                        girls,
                        possTotal * totalBoysGirls,
                        possTotal * boys,
                        possTotal * girls,
                        int(authTotal),
                        int(authBoys),
                        int(authGirls),
                        int(unAuthTotal),
                        int(unAuthBoys),
                        int(unAuthGirls),
                        int(lateTotal),
                        int(lateBoys),
                        int(lateGirls),
                        int(attends),
                        finalAttPercent]
                returnData.append(temp)
            self.returnData = returnData
            html = self.__renderHtml__()
            self.__emailUserReports__(html)
        except Exception, error:
            log.warn('school attendance report for %s %s failed because:\n\n%s\n\n%s' % (self.school,
                                                                                         self.dateRange,
                                                                                         traceback.format_exc(),
                                                                                         str(self.date_range_is_a_tuple)))
        return


def FutureAttendances(request,PupilId,AcYear):
    pupilRec=PupilRecord(PupilId,AcYear)
    attendancegroup=None
    for groups in pupilRec.GroupList():
        if 'Form' in groups.Group.Name and not 'BalGames' in groups.Group.Name:
            attendancegroup=groups.Group
    if 'AttendanceCode' in request.POST and  'AttendanceDate' in request.POST and 'NoAttendances' in request.POST and 'AmPm' in request.POST and attendancegroup:
        CounterAmPm=request.POST['AmPm']
        CounterDate=datetime.datetime.strptime(request.POST['AttendanceDate'],'%Y-%m-%d')
        if '/' in request.POST['AttendanceCode']:
            for counter in range(0,int(request.POST['NoAttendances'])):
                oldAtt=PupilAttendance.objects.filter(Pupil=pupilRec.Pupil,AttendanceDate=CounterDate.date(),AmPm=CounterAmPm,Active=True)
                if oldAtt.exists():
                    for recs in oldAtt:
                        recs.Active=False
                        recs.save()
                if CounterAmPm =='PM':
                    CounterAmPm='AM'
                    if CounterDate.weekday() == 4:
                        CounterDate=CounterDate+datetime.timedelta(days=3)
                    else:
                        CounterDate=CounterDate+datetime.timedelta(days=1)
                else:
                    CounterAmPm='PM'
            return
        attCode=AttendanceCode.objects.get(AttendanceCode=request.POST['AttendanceCode'])
        for counter in range(0,int(request.POST['NoAttendances'])):
            oldAtt=PupilAttendance.objects.filter(Pupil=pupilRec.Pupil,AttendanceDate=CounterDate.date(),AmPm=CounterAmPm,Active=True)
            if oldAtt.exists():
                for recs in oldAtt:
                    recs.Active=False
                    recs.save()
            newAtt=PupilAttendance(Pupil=pupilRec.Pupil,
                                   AttendanceDate=CounterDate.date(),
                                   AttendanceType='D',
                                   Group=attendancegroup,
                                   AmPm=CounterAmPm,
                                   Teacher=request.session['StaffId'],
                                   RecordedDate=CounterDate.date(),
                                   Code=attCode)
            newAtt.save()
            if CounterAmPm =='PM':
                CounterAmPm='AM'
                if CounterDate.weekday() == 4:
                    CounterDate=CounterDate+datetime.timedelta(days=3)
                else:
                    CounterDate=CounterDate+datetime.timedelta(days=1)
            else:
                CounterAmPm='PM'


def EditAttendanceResultsPageHelperFunction(AcYear, startDate, endDate, groupName):
    ''' Helper function used to collect attendance data for a group of pupils -
    data is formed in a certain way so to work correctly with the:

    EditAttendanceResultsPage() and EditAttendanceCode()

    functions/views in AttendanceViews '''
    AcYear = AcYear
    startDate = startDate
    endDate = endDate
    groupName = groupName
    numDays = endDate - startDate
    numDays = numDays.days
    pupils = GeneralFunctions.Get_PupilList(AcYear, groupName)
    dateRange = [startDate + datetime.timedelta(days=x) for x in range(0,numDays)]
    # collecting attendance data
    attendanceData = []
    for pupil in pupils:
        tempAttendance = PupilAttendance.objects.filter(AttendanceDate__gte=startDate,
                                                        AttendanceDate__lte=endDate,
                                                        Pupil__id=pupil.id,
                                                        Active=True)
        tempList = []
        for date in dateRange:
            dateForData = date.date().isoformat()
            tempAm = ['/', dateForData, 'NoId', 'AM', date]
            tempPm = ['/', dateForData, 'NoId', 'PM', date]
            for att in tempAttendance:
                if date.date() == att.AttendanceDate and att.AmPm == 'AM':
                    tempAm = [att.Code.AttendanceCode,
                              dateForData,
                              att.id,
                              'AM',
                              date]
                elif date.date() == att.AttendanceDate and att.AmPm == 'PM':
                    tempPm = [att.Code.AttendanceCode,
                              dateForData,
                              att.id,
                              'PM',
                              date]
            tempList.append(tempAm)
            tempList.append(tempPm)
        tempDict = {'Pupil': pupil,
                    'Data': tempList}
        attendanceData.append(tempDict)
    # end
    attendanceCodes = [[u'/', 'Pupil Present']]
    for codes in AttendanceCode.objects.all():
        attendanceCodes.append([unicode(codes.AttendanceCode), codes.AttendanceDesc])
    c = {'Pupils': pupils,
        'DateRange': dateRange,
        'AttendanceData': attendanceData,
        'AttendanceCodes': attendanceCodes,
        'GroupName': groupName,
        'GroupNameShort': groupName.split('.')[-1],
        'startDate': startDate,
        'endDate': endDate,
        'groupName': groupName}
    return c


class AttendanceDataFromDateRange(threading.Thread):
    ''' Gets Attendance Data based upon a date range '''
    def __init__(self,
                 AcYear,
                 school,
                 startDate,
                 endDate,
                 groupList,
                 userObj):
        threading.Thread.__init__(self)
        self.todaysDate()
        self.AcYear = AcYear
        self.school = school
        self.startDate = startDate
        self.endDate = endDate
        self.startDateUkFormat = '%s-%s-%s' % (startDate.day,
                                               startDate.month,
                                               startDate.year)
        self.endDateUkFormat = '%s-%s-%s' % (endDate.day,
                                             endDate.month,
                                             endDate.year)
        self.groupList = groupList
        self.userObj = userObj

    def todaysDate(self):
        ''' Todays date in UK format '''
        today = datetime.datetime.now()
        self.todaysDate = '%s-%s-%s' % (today.day, today.month, today.year)
        return

    def __renderHtml__(self):
        ''' render html for report '''
        template = get_template('Reports/classSchoolAttendanceReport1.html')
        context = Context(self.returnData)
        renderedHtml = template.render(context)
        return renderedHtml

    def __emailUserReports__(self, htmlAttachedFile):
        ''' email reports to the user '''
        if self.userObj.EmailAddress:
            msg=EmailMessage('WILLOWTREE: class attendance report for %s (%s to %s)' % (MenuTypes.objects.get(Name=self.school).SchoolId.Name,
                                                                                          self.startDateUkFormat,
                                                                                          self.endDateUkFormat),
                             'MSG FROM WILLOWTREE:\n\nClass school attendance report for %s attached' % MenuTypes.objects.get(Name=self.school).SchoolId.Name,
                             SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data,
                             [self.userObj.EmailAddress])
            msg.attach('Attendance_Report_%s_%s_%s.html' % (MenuTypes.objects.get(Name=self.school).SchoolId.Name,
                                                           self.startDateUkFormat,
                                                           self.endDateUkFormat),
                       htmlAttachedFile)
            msg.send()
        return

    def run(self):
        ''' gets attendance data formatted for html template '''
        self.returnData = {}
        self.returnData['data'] = []
        try:
            for group in self.groupList:
                c = EditAttendanceResultsPageHelperFunction(self.AcYear,
                                                            self.startDate,
                                                            self.endDate,
                                                            group)
                returnDict = {'todaysDate': self.todaysDate,
                              'school': MenuTypes.objects.get(Name=self.school).SchoolId.Name,
                              'startDate': self.startDate,
                              'endDate': self.endDate}
                returnDict.update(c)
                self.returnData['data'].append(returnDict)
            html = self.__renderHtml__()
            self.__emailUserReports__(html)
        except:
            errorMsg = str(sys.exc_info())
            fullErrorMsg = 'class attendance report for %s (%s-%s) failed because: %s' % (MenuTypes.objects.get(Name=self.school).SchoolId.Name,
                                                                                          self.startDate,
                                                                                          self.endDate,
                                                                                          errorMsg)
#            f = open('/home/login/testing.txt','w')
#            f.write(fullErrorMsg)
#            f.close()
            log.warn(fullErrorMsg)
        return


class AttendanceHasBeenTaken():
    """
    Returns if attendance has been taken for the am or pm of today
    """
    def __init__(self, group_name):
        self.group_name = group_name
        self.today_date_object = datetime.datetime.today().date()
        self.morning_attendance_taken = False
        self.afternoon_attendance_taken = False
        self.__determine_if_attendance_taken__()

    def __determine_if_attendance_taken__(self):
        if AttendanceTaken.objects.filter(Group__Name=self.group_name,
                                          AttendanceDate=self.today_date_object).exists():
            for attendance_taken_object in AttendanceTaken.objects.filter(Group__Name=self.group_name,
                                                                          AttendanceDate=self.today_date_object):
                if attendance_taken_object.AmPm == "AM":
                    self.morning_attendance_taken = True
                if attendance_taken_object.AmPm == "PM":
                    self.afternoon_attendance_taken = True
        return


