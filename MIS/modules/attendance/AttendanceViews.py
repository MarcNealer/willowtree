# Base python libray imports
import time
import datetime
from operator import itemgetter
import csv
import json
import sys

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q

# WillowTree system imports
from StaffAttendance.models import DailyAttendanceStaff
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules import GeneralFunctions
from MIS.models import *
from MIS.modules.alerts import AlertFunctions
from MIS.modules.attendance import AttendanceFunctions

# # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  *******  Attendance System ********************
#
# # # # # # # # # # # # # # # # # # # # # # # # # # #

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Creates the primary Attendance page when Attendance is chosen from the menu
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def AttendanceForm(request,school,AcYear,GroupName,Am_Pm=None):
    ''' This view creates the Attendance input form for teachers'''
    c={'school':school,'GroupName':GroupName,'AcYear':AcYear}
    c.update(csrf(request))
    c.update({'attendance_taken': AttendanceFunctions.AttendanceHasBeenTaken(GroupName)})
    TimeNow=datetime.datetime.now()

    if TimeNow.hour > 8 and TimeNow.hour < 13 and TimeNow.hour > 13 and school.find('Admin') < 0:
            return HttpResponseRedirect('/WillowTree/%s/%s/AttendanceClosed/' % (school,AcYear))
    else:
        PupilList=GeneralFunctions.Get_PupilList(AcYear,GroupName=GroupName)
        if Am_Pm==None:
            c['AmPm']=datetime.datetime.today().strftime('%p')
            Am_Pm=c['AmPm']
        else:
            c['AmPm']=Am_Pm
        AttendanceData=[]
        for pupils in PupilList:
            AlertItems=AlertFunctions.Alerts(request,pupils.id)
            AttendanceRecord=[pupils.id,pupils.Surname,pupils.Forename,pupils.Picture,'/',True,AlertItems.ActivePupilAlerts(),False,'/']

            if PupilAttendance.objects.filter(Pupil=pupils.id,AttendanceDate=datetime.datetime.today(),Active=True,AmPm=Am_Pm).exists():
                NewCode=PupilAttendance.objects.get(Pupil=pupils,AttendanceDate=datetime.datetime.today(),Active=True,AmPm=Am_Pm)
                AttendanceRecord[4]=str(NewCode.Code.AttendanceCode)
                AttendanceRecord[7]=True
                AdminPerm=False
                for Perms in NewCode.Teacher.User.get_all_permissions():
                    if Perms.find('Admin') > -1:
                        AdminPerm=True
                if not AdminPerm and school.find('Admin') < 0:
                    AttendanceRecord[5]=False

            if Am_Pm.lower()=='pm':
                AmRecord=PupilAttendance.objects.filter(Pupil=pupils.id,AttendanceDate=datetime.datetime.today(),Active=True,AmPm__iexact='Am')
                if AmRecord.exists():
                    AttendanceRecord[8]=str(AmRecord[0].Code.AttendanceCode)
            AttendanceData.append(AttendanceRecord)
        c.update({'PupilList':sorted(AttendanceData,key=itemgetter(1,0))})
        if school.find('Admin') > -1:
            c.update({'AttendanceCodes':AttendanceCode.objects.all()})
        else:
            c.update({'AttendanceCodes':AttendanceCode.objects.filter(AttendanceAccess='Teacher')})
        return render_to_response('DailyAttendanceFormNew.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def AttendanceByGroupForm(request,school,AcYear,AttDate):
    ''' This view creates the Attendance input form for teachers'''
    schoolname=GeneralFunctions.GetSchoolName(school)
    Am_Pm=request.POST['AmPm']
    if 'WholeSchool' in request.POST['AttendanceGroup']:
        groupList=PupilGroup.objects.filter(AcademicYear=AcYear,Active=True,
                                            Group__Name__istartswith='Current.%s' % schoolname)
    else:
        groupList=PupilGroup.objects.filter(AcademicYear=AcYear,Active=True,
                                            Group__Name__icontains=request.POST['AttendanceGroup']).filter(Group__Name__istartswith='Current.%s' % schoolname)
    AttDate=AttendanceDate=datetime.datetime.strptime(AttDate,'%Y-%m-%d')
    pupilList=set()
    for items in groupList:
        pupilList.add(items.Pupil)
    PupilList=list(pupilList)
    sorted(PupilList, key=lambda pupil:pupil.Surname.lower())

    AttendanceData=[]
    for pupils in PupilList:
        AttendanceRecord=[pupils.id,pupils.Surname,pupils.Forename,pupils.Picture,'/',True,' ',False]
        if PupilAttendance.objects.filter(Pupil=pupils.id,AttendanceDate=AttDate,Active=True,AmPm=Am_Pm).exists():
            NewCode=PupilAttendance.objects.get(Pupil=pupils,AttendanceDate=AttDate,Active=True,AmPm=Am_Pm)
            AttendanceRecord[4]=str(NewCode.Code.AttendanceCode)
            AttendanceRecord[7]=True
            AdminPerm=False
            for Perms in NewCode.Teacher.User.get_all_permissions():
                if Perms.find('Admin') > -1:
                    AdminPerm=True
            if not AdminPerm and school.find('Admin') < 0:
                AttendanceRecord[5]=False
        AttendanceData.append(AttendanceRecord)
    c={'school':school,'AcYear':AcYear,'SchoolName':schoolname,'ListGroup':request.POST['AttendanceGroup'],'AmPm':Am_Pm,'AttDate':AttDate}
    c.update(csrf(request))
    c.update({'PupilList':sorted(AttendanceData,key=itemgetter(1,0))})
    if school.find('Admin') > -1:
        c.update({'AttendanceCodes':AttendanceCode.objects.all()})
    else:
        c.update({'AttendanceCodes':AttendanceCode.objects.filter(AttendanceAccess='Teacher')})
    return render_to_response('GroupAttendanceFormAdmin.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def AttendanceByGroupProccess(request,school,AcYear,Am_Pm,AttDate):
    ''' This view is used to proccess the Attendance page form data.
    It looks at the return data and stores new records where needed, or
    updates records if this is a modification of an attendance record already
    done'''
    AttDate=GeneralFunctions.ReadFormDate(AttDate)
    request.session['AttendanceDate']=AttDate
    for items in request.POST:
        if items.find('Pupil_')> -1:
            Pupil_Id=items.split('_')[1]
            PupilObject=Pupil.objects.get(id=int(Pupil_Id))
            FormGroup=PupilGroup.objects.filter(AcademicYear=AcYear,Active=True,
                                                Group__Name__icontains='Form',
                                                Pupil__id=Pupil_Id).filter(Group__Name__istartswith='Current').exclude(Group__Name__icontains='Games').exclude(Group__Name__icontains='Ballet')
            if request.POST[items]=='/':
                if PupilAttendance.objects.filter(Pupil=PupilObject,AttendanceDate=AttDate,AmPm=Am_Pm,Active=True).exists():
                    AttendanceRecord=PupilAttendance.objects.get(Pupil=PupilObject,AttendanceDate=AttDate,AmPm=Am_Pm,Active=True)
                    AttendanceRecord.Active=False
                    AttendanceRecord.save()
            else:
                if PupilAttendance.objects.filter(Pupil=PupilObject,AttendanceDate=AttDate,AmPm=Am_Pm,Active=True).exists():
                    AttendanceRecord=PupilAttendance.objects.get(Pupil=Pupil_Id,AttendanceDate=AttDate,AmPm=Am_Pm,Active=True)
                    if not AttendanceRecord.Code==AttendanceCode.objects.get(AttendanceCode=request.POST[items]):
                        AttendanceRecord.Active=False
                        AttendanceRecord.save()
                        # test
                        NewRecord=PupilAttendance(Pupil=PupilObject,
                        AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm,
                        AttendanceType='D',Group=FormGroup[0].Group,
                        Teacher=request.session['StaffId'],
                        Code=AttendanceCode.objects.get(AttendanceCode=request.POST[items]),
                        RecordedDate=datetime.datetime.today(),Active=True)
                        NewRecord.save()
                else:
                        NewRecord=PupilAttendance(Pupil=PupilObject,
                        AttendanceDate=AttDate,
                        AmPm=Am_Pm,AttendanceType='D',
                        Group=FormGroup[0].Group,
                        Teacher=request.session['StaffId'],
                        Code=AttendanceCode.objects.get(AttendanceCode=request.POST[items]),
                        RecordedDate=datetime.datetime.today(),
                        Active=True)
                        NewRecord.save()

    return HttpResponseRedirect('/WillowTree/%s/%s/DailyAttendanceReport/' % (school,AcYear))



def AttendanceP(request,school,AcYear,GroupName,Am_Pm):
    ''' This view is used to proccess the Attendance page form data.
    It looks at the return data and stores new records where needed, or
    updates records if this is a modification of an attendance record already
    done'''
    AttendanceReport=[]
    for items in request.POST:
        if items.find('Code_')> -1:
            Pupil_Id=items.split('_')[1]
            PupilObject=Pupil.objects.get(id=int(Pupil_Id))
            if request.POST[items]=='/':
                if PupilAttendance.objects.filter(Pupil=PupilObject,AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm,Active=True).exists():
                    AttendanceRecord=PupilAttendance.objects.get(Pupil=Pupil_Id,AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm,Active=True)
                    AttendanceRecord.Active=False
                    AttendanceRecord.save()
                    AttendanceReport.append('%s is has been Updated at present' % PupilObject.FullName())
            else:
                if PupilAttendance.objects.filter(Pupil=PupilObject,AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm,Active=True).exists():
                    AttendanceRecord=PupilAttendance.objects.get(Pupil=Pupil_Id,AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm,Active=True)
                    if not AttendanceRecord.Code==AttendanceCode.objects.get(AttendanceCode=request.POST[items]):
                        AttendanceRecord.Active=False
                        AttendanceRecord.save()
                        # test
                        NewRecord=PupilAttendance(Pupil=PupilObject,
                        AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm,
                        AttendanceType='D',Group=Group.objects.get(Name=GroupName),
                        Teacher=request.session['StaffId'],
                        Code=AttendanceCode.objects.get(AttendanceCode=request.POST[items]),
                        RecordedDate=datetime.datetime.today(),Active=True)
                        NewRecord.save()
                        AttendanceReport.append('%s has been updated as %s' % (PupilObject.FullName(), request.POST[items]))
                else:
                        NewRecord=PupilAttendance(Pupil=PupilObject,
                        AttendanceDate=datetime.datetime.today(),
                        AmPm=Am_Pm,AttendanceType='D',
                        Group=Group.objects.get(Name=GroupName),
                        Teacher=request.session['StaffId'],
                        Code=AttendanceCode.objects.get(AttendanceCode=request.POST[items]),
                        RecordedDate=datetime.datetime.today(),
                        Active=True)
                        NewRecord.save()
                        AttendanceReport.append('%s has been marked as %s' % (PupilObject.FullName(), request.POST[items]))


    if not AttendanceTaken.objects.filter(AttendanceDate=datetime.datetime.today(),Group__Name=GroupName,AmPm=Am_Pm).exists():
        AttTakenRecord=AttendanceTaken(AttendanceDate=datetime.datetime.today(),
        AttendanceType='D',Group=Group.objects.get(Name=GroupName),AmPm=Am_Pm,
        RecordedDate=datetime.datetime.now(),Teacher=request.session['StaffId'])
        AttTakenRecord.save()
    request.session['Messages'] = ['Attendance Successfully Taken...']
    return HttpResponseRedirect('/WillowTree/%s/%s/Attendance/%s/%s/' % (school,AcYear,GroupName,Am_Pm))

def DailyAttendanceReport(request,school,AcYear):
    ''' This view will create the Daily Attendance report, viewed via the tasks
    menu. If no date is specified on the URL request, then it takes the default
    of todays date'''
    if 'AttendanceDate' in request.POST:
        AttendanceDate=datetime.datetime.strptime(request.POST['AttendanceDate'],'%Y-%m-%d')
    elif 'AttendanceDate' in request.session:
        AttendanceDate =request.session['AttendanceDate']
        del request.session['AttendanceDate']
    else:
        AttendanceDate=datetime.date.today()

    ClassList=AttendanceFunctions.DailyAttendance.Get_ClassList(school)
    DisplayDate=AttendanceDate.strftime("%d-%m-%Y")
    AttReport=[]
    for Classes in ClassList:
        AttReport.append(AttendanceFunctions.DailyAttendance(AttendanceDate,Classes))
    request.session.set_expiry(1800)
    c={'school':school,'AcYear':AcYear,
       'BannerTitle':'Daily Attendance Report for %s' % DisplayDate,
       'AttDate':AttendanceDate,
       'Counter':GeneralFunctions.templateCounter()}
    c.update(csrf(request))
    c.update({'AttendanceReport':AttReport})
    return render_to_response('DailyAttendanceReport.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def FireDrill(request, school):
    """
    School fire drill.
    """
    # collecting data for pupils
    AttReport = []
    AttendanceDate = datetime.date.today()
    DisplayDate = AttendanceDate.strftime("%d-%m-%Y")
    ClassList = AttendanceFunctions.DailyAttendance.Get_ClassList(school)
    for Classes in ClassList:
        AttReport.append(AttendanceFunctions.DailyAttendance(AttendanceDate, Classes))

    # collecting data for staff
    today_date = datetime.date.today()
    staff_fire_drill_list = []
    school_name = MenuTypes.objects.get(Name=school).SchoolId.Name
    staff_groups = Group.objects.filter(Name__icontains='Staff.%s' % school_name).filter(Name__icontains='_School_Staff_Register').values_list('Name', flat=True)
    for group in staff_groups:
        group_name_to_return_to_template = group.replace("Staff.%s." % school_name, "")
        group_name_to_return_to_template = group_name_to_return_to_template.replace("_School_Staff_Register", "")
        temp_array = {'group_name': group_name_to_return_to_template,
                      'total_present_pm': 0,
                      'total_present_am': 0,
                      'data': []}
        staff_ids_in_group = StaffGroup.objects.filter(Group__Name=group).order_by('Staff__Surname').values_list('Staff__id', flat=True)
        staff_ids_in_group = list(set(staff_ids_in_group))
        total_present_am_counter = 0
        total_present_pm_counter = 0
        for staff_id in staff_ids_in_group:

            # AM data
            if DailyAttendanceStaff.objects.filter(staff__id=staff_id,
                                                   active=True,
                                                   am_pm='AM',
                                                   date_attendance_taken=today_date).exists():
                attendance_code_for_am = DailyAttendanceStaff.objects.get(staff__id=staff_id,
                                                                          active=True,
                                                                          am_pm='AM',
                                                                          date_attendance_taken=today_date).attendance_code.attendance_code
                if attendance_code_for_am == '/':
                    total_present_am_counter += 1
            else:
                attendance_code_for_am = ''

            # PM data
            if DailyAttendanceStaff.objects.filter(staff__id=staff_id,
                                                   active=True,
                                                   am_pm='PM',
                                                   date_attendance_taken=today_date).exists():
                attendance_code_for_pm = DailyAttendanceStaff.objects.get(staff__id=staff_id,
                                                                          active=True,
                                                                          am_pm='PM',
                                                                          date_attendance_taken=today_date).attendance_code.attendance_code
                if attendance_code_for_pm == '/':
                    total_present_pm_counter += 1
            else:
                attendance_code_for_pm = ''

            staff_name = Staff.objects.get(id=staff_id).FullName()
            temp_array['data'].append([staff_name, attendance_code_for_am, attendance_code_for_pm])
        temp_array['total_present_am'] = total_present_am_counter
        temp_array['total_present_pm'] = total_present_pm_counter
        staff_fire_drill_list.append(temp_array)

    # data to send to template
    c = {'school': school,
         'ReportDate': DisplayDate,
         'FireDrillList': AttReport,
         'Counter': GeneralFunctions.templateCounter(),
         'staff_fire_drill_list': staff_fire_drill_list}
    return render_to_response('Reports/FireDrillReport.html',
                              c, context_instance=RequestContext(request, processors=[SetMenu]))


def MissingReport(request,AcYear,school):
    AttDate=datetime.date.today()
    DisplayDate=AttDate.strftime("%d-%m-%Y")
    AttReport = sorted(PupilAttendance.objects.filter(Active=True,
                                                      AttendanceDate=AttDate,
                                                      Pupil__id__in=GeneralFunctions.GetPupilInSchool(school, AcYear)),
                       key=lambda k: k.Group.MenuName)
    c={'school':school,
       'ReportDate':DisplayDate,
       'MissingList':AttReport,
       'Counter':GeneralFunctions.templateCounter()}
    return render_to_response('Reports/MissingTodayReport.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def number_of_girls_and_boys_present_today(request, AcYear, school):
    AttDate=datetime.date.today()
    missing = PupilAttendance.objects.filter(Active=True,
                                             AttendanceDate=AttDate,
                                             Pupil__id__in=GeneralFunctions.GetPupilInSchool(school, AcYear))
    boys = 0
    girls = 0
    for i in GeneralFunctions.GetPupilInSchool(school, AcYear):
        if i not in missing:
            temp_pupil_object = Pupil.objects.get(id=i)
            if temp_pupil_object.Gender == 'M':
                boys += 1
            else:
                girls += 1
    return HttpResponse('Boys and Girls Present today in %s\n\nNumber of Boys:%s\nNumber of Girls:%s' % (school,
                                                                                                         str(boys),
                                                                                                         str(girls)),
                        content_type="text/plain")


def MissingReportCSV(request,AcYear,school):
    AttDate=datetime.date.today()
    AttReport = PupilAttendance.objects.filter(Active=True,AttendanceDate=AttDate,Pupil__id__in=GeneralFunctions.GetPupilInSchool(school,AcYear))
    response= HttpResponse(mimetype="text/csv")
    response_writer= csv.writer(response)
    response_writer.writerow(['Pupil','Group','AM/PM','Code','Desc','Teacher'])
    for records in AttReport:
        response_writer.writerow([records.Pupil.FullName(),
                                  records.Group.MenuName,
                                  records.AmPm,
                                  records.Code.AttendanceCode,
                                  records.Code.AttendanceDesc,
                                  records.Teacher.FullName()])
    response['Content-Disposition'] = 'attachment; filename=MissingList.csv'
    return response


def AddFutureAttendances(request,AcYear,school,PupilNo):
    AttendanceFunctions.FutureAttendances(request,PupilNo,AcYear)
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,AcYear,PupilNo))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Creates Primary attendance page from the Admin Attendance Page
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def AttendanceFormAdmin(request,school,AcYear,GroupName,Am_Pm, AttDate):
    ''' This view creates the Attendance input form for teachers'''
    c={'school':school,'GroupName':GroupName,'AcYear':AcYear,'AttDate':AttDate,'AmPm':Am_Pm}
    c.update(csrf(request))
    AttDate=GeneralFunctions.ReadFormDate(AttDate)
    AttendanceData=[]
    PupilList=GeneralFunctions.Get_PupilList(AcYear,GroupName=GroupName)
    for pupils in PupilList:
        AlertItems=AlertFunctions.Alerts(request,pupils.id)
        AttendanceRecord=[pupils.id,pupils.Surname,pupils.Forename,pupils.Picture,'/',True,AlertItems.ActivePupilAlerts(),False]
        if PupilAttendance.objects.filter(Pupil=pupils.id,AttendanceDate=AttDate,Active=True,AmPm=Am_Pm).exists():
            NewCode=PupilAttendance.objects.get(Pupil=pupils,AttendanceDate=AttDate,Active=True,AmPm=Am_Pm)
            AttendanceRecord[4]=str(NewCode.Code.AttendanceCode)
            AttendanceRecord[7]=True
        AttendanceData.append(AttendanceRecord)
    c.update({'PupilList':sorted(AttendanceData,key=itemgetter(1,0))})
    if school.find('Admin') > -1:
        c.update({'AttendanceCodes':AttendanceCode.objects.all()})
    else:
        c.update({'AttendanceCodes':AttendanceCode.objects.filter(AttendanceAccess='Teacher')})
    request.session['AttendanceDate']=AttDate
    return render_to_response('DailyAttendanceFormAdmin.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

#
# Same as AttendanceP except an argument of the Date is passed as well
#

def AttendancePAdmin(request,school,AcYear,GroupName,Am_Pm,AttDate):
    ''' This view is used to proccess the Attendance page form data.
    It looks at the return data and stores new records where needed, or
    updates records if this is a modification of an attendance record already
    done'''
    AttendanceReport=[]
    AttDate=GeneralFunctions.ReadFormDate(AttDate)
    for items in request.POST:
        if items.find('Code_')> -1:
            Pupil_Id=items.split('_')[1]
            PupilObject=Pupil.objects.get(id=int(Pupil_Id))
            if request.POST[items]=='/':
                if PupilAttendance.objects.filter(Pupil=PupilObject,AttendanceDate=AttDate,AmPm=Am_Pm,Active=True).exists():
                    AttendanceRecord=PupilAttendance.objects.get(Pupil=PupilObject,AttendanceDate=AttDate,AmPm=Am_Pm,Active=True)
                    AttendanceRecord.Active=False
                    AttendanceRecord.save()
                    AttendanceReport.append('%s is has been Updated at present' % PupilObject.FullName())
            else:
                if PupilAttendance.objects.filter(Pupil=PupilObject,AttendanceDate=AttDate,AmPm=Am_Pm,Active=True).exists():
                    AttendanceRecord=PupilAttendance.objects.get(Pupil=Pupil_Id,AttendanceDate=AttDate,AmPm=Am_Pm,Active=True)
                    if not AttendanceRecord.Code==AttendanceCode.objects.get(AttendanceCode=request.POST[items]):
                        AttendanceRecord.Active=False
                        AttendanceRecord.save()
                        # test
                        NewRecord=PupilAttendance(Pupil=PupilObject,
                        AttendanceDate=AttDate,AmPm=Am_Pm,
                        AttendanceType='D',Group=Group.objects.get(Name=GroupName),
                        Teacher=request.session['StaffId'],
                        Code=AttendanceCode.objects.get(AttendanceCode=request.POST[items]),
                        RecordedDate=datetime.datetime.today(),Active=True)
                        NewRecord.save()
                        AttendanceReport.append('%s has been updated as %s' % (PupilObject.FullName(), request.POST[items]))
                else:
                        NewRecord=PupilAttendance(Pupil=PupilObject,
                        AttendanceDate=AttDate,
                        AmPm=Am_Pm,AttendanceType='D',
                        Group=Group.objects.get(Name=GroupName),
                        Teacher=request.session['StaffId'],
                        Code=AttendanceCode.objects.get(AttendanceCode=request.POST[items]),
                        RecordedDate=datetime.datetime.today(),
                        Active=True)
                        NewRecord.save()
                        AttendanceReport.append('%s has been marked as %s' % (PupilObject.FullName(), request.POST[items]))


    if not AttendanceTaken.objects.filter(AttendanceDate=AttDate,Group__Name=GroupName,AmPm=Am_Pm).exists():
        AttTakenRecord=AttendanceTaken(AttendanceDate=AttDate,
        AttendanceType='D',Group=Group.objects.get(Name=GroupName),AmPm=Am_Pm,
        RecordedDate=datetime.datetime.now(),Teacher=request.session['StaffId'])
        AttTakenRecord.save()
    c={'school':school,'AcYear':AcYear,'BannerTitle':'Attendance records saved for %s' % GroupName,'return_msg':AttendanceReport}
    c.update(csrf(request))
    return HttpResponseRedirect('/WillowTree/%s/%s/DailyAttendanceReport/' % (school,AcYear))


def AttendanceMarkGroup(request,AcYear,school,Am_Pm,AttDate,GroupName):
    AttendanceReport=[]
    AttDate=GeneralFunctions.ReadFormDate(AttDate)
    PupilList=GeneralFunctions.Get_PupilList(AcYear,GroupName=GroupName)
    group=Group.objects.get(Name=GroupName)
    for pupils in PupilList:
        if not PupilAttendance.objects.filter(Pupil=pupils,AttendanceDate=AttDate,Active=True).exists():
            newAtt=PupilAttendance(Pupil=pupils,
                                   AttendanceType='D',
                                   AttendanceDate=AttDate,
                                   Group=group,
                                   AmPm=Am_Pm,
                                   Teacher=request.session['StaffId'],
                                   Code=AttendanceCode.objects.get(AttendanceCode=request.POST['AttendanceCode']))
            newAtt.save()
            AttendanceReport.append('%s has been marked as %s' % (pupils.FullName(), request.POST['AttendanceCode']))

    # set AttendanceTaken object
    if not AttendanceTaken.objects.filter(AttendanceDate=datetime.datetime.today(),
                                          Group__Name=GroupName,
                                          AmPm=Am_Pm).exists():
        AttTakenRecord = AttendanceTaken(AttendanceDate=datetime.datetime.today(),
                                         AttendanceType='D',
                                         Group=Group.objects.get(Name=GroupName),
                                         AmPm=Am_Pm,
                                         RecordedDate=datetime.datetime.now(),
                                         Teacher=request.session['StaffId'])
        AttTakenRecord.save()
    c={'school':school,'AcYear':AcYear,'BannerTitle':'Attendance records saved for %s' % GroupName,'return_msg':AttendanceReport}
    c.update(csrf(request))
    return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def EditAttendanceMainPage(request, AcYear, school):
    ''' Displays the main edit attendance page '''
    listOfGroups = []
    schoolName = MenuTypes.objects.get(Name=school).SchoolId.Name
    for i in Group.objects.filter(Name__icontains=schoolName).filter(Name__icontains='Current').filter(Name__icontains='Form'):
        temp = i.Name
        temp = temp.split('.')
        if len(temp) == 6:
            listOfGroups.append(i)
    startDate = datetime.date.today() - datetime.timedelta(days=7)
    c = {'Groups': listOfGroups,
         'startDate': startDate.isoformat(),
         'endDate': datetime.date.today().isoformat()}
    ReqCon = RequestContext(request,processors=[SetMenu])
    return render_to_response('EditAttendanceMainPage.html', c,
                              context_instance=ReqCon)


def EditAttendanceResultsPage(request, AcYear, school):
    ''' Displays the edit attendance results page - as well as viewing the user
    it is possible to edit attendance from here '''
    groupName = request.POST['groupName']
    startDate = datetime.datetime.strptime(request.POST['startDate'], '%Y-%m-%d')
    endDate = datetime.datetime.strptime(request.POST['endDate'], '%Y-%m-%d')
    endDate = endDate + datetime.timedelta(days=1)
    c = AttendanceFunctions.EditAttendanceResultsPageHelperFunction(AcYear,
                                                                    startDate,
                                                                    endDate,
                                                                    groupName)
    ReqCon = RequestContext(request,processors=[SetMenu])
    return render_to_response('EditAttendanceSearchResults.html', c,
                              context_instance=ReqCon)


def EditAttendanceCode(request, AcYear, school):
    ''' Edit attendance code from edit attendance page '''
    try:
        pupilId = request.POST['pupilId']
        pupilAttendanceId = request.POST['newAttendanceCode'].split('_')[2]
        newAttendanceCode = request.POST['newAttendanceCode'].split('_')[0]
        newDate = datetime.datetime.strptime(request.POST['newAttendanceCode'].split('_')[1],
                                             '%Y-%m-%d')
        # delete dups and deactive old records
        oldAttObjs = PupilAttendance.objects.filter(Pupil__id=pupilId,
                                                   AmPm=request.POST['newAttendanceCode'].split('_')[3],
                                                   AttendanceDate=newDate,
                                                   Active=True)
        if len(oldAttObjs) != 0:
            for oldAttObj in oldAttObjs:
                temp = oldAttObj
                temp.Active = False
                temp.save()
        # end
        # create new attendance record
        if newAttendanceCode != '/':
            newAttCodeLink = AttendanceCode.objects.get(AttendanceCode=newAttendanceCode)
            pupilObj = Pupil.objects.get(id=request.POST['newAttendanceCode'].split('_')[4])
            pupilGroupObj = Group.objects.get(Name=request.POST['GroupName'])
            teacherObj = Staff.objects.get(id=User.objects.get(id=request.session['_auth_user_id']).staff.id)
            attendanceObj = PupilAttendance(Pupil=pupilObj,
                                            AttendanceDate=newDate,
                                            Group=pupilGroupObj,
                                            AmPm=request.POST['newAttendanceCode'].split('_')[3],
                                            Teacher= teacherObj,
                                            Code=newAttCodeLink)
            attendanceObj.save()
            # end
            #  json reponse code
            newAttObj = PupilAttendance.objects.get(Pupil__id=pupilId,
                                                    AmPm=request.POST['newAttendanceCode'].split('_')[3],
                                                    AttendanceDate=newDate,
                                                    Active=True)
            newAttCode = newAttObj.Code.AttendanceCode
        else:
            newAttCode = '/'
        pupilName = Pupil.objects.get(id=pupilId).FullName()
        dateChangedList = request.POST['newAttendanceCode'].split('_')[1].split('-')
        dateChanged = '%s-%s-%s' % (dateChangedList[2],
                                    dateChangedList[1],
                                    dateChangedList[0])
        response_data = '{"pupilName":"%s", "newAttendanceCode":"%s", "dateChanged":"%s", "newAttObj": "%s", "divId":"%s", "AmPm":"%s"}' % (pupilName,
                                                                                                                                            newAttendanceCode,
                                                                                                                                            dateChanged,
                                                                                                                                            newAttCode,
                                                                                                                                            request.POST['divId'],
                                                                                                                                            request.POST['newAttendanceCode'].split('_')[3])
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    except:
        x = str(sys.exc_info())
        f = open('/home/login/json_error_file.txt','w')
        f.write('%s\n' % x)
        f.close()


def SchoolAttendanceReportSelectPage(request, AcYear, school):
    ''' School Attendance Report Select Page '''
    listOfGroups = []
    schoolName = MenuTypes.objects.get(Name=school).SchoolId.Name
    for i in Group.objects.filter(Name__icontains=schoolName).filter(Name__icontains='Current').filter(Name__icontains='Form'):
        temp = i.Name
        temp = temp.split('.')
        if len(temp) == 6:
            listOfGroups.append(i)
    startDate = datetime.date.today() - datetime.timedelta(days=7)
    dateRange = ['Lent',
                 'LentHalf1',
                 'LentHalf2',
                 'Michaelmas',
                 'MichaelmasHalf1',
                 'MichaelmasHalf2',
                 'Summer',
                 'SummerHalf1',
                 'SummerHalf2',
                 'Yearly',
                 'April',
                 'December',
                 'February',
                 'January',
                 'July',
                 'June',
                 'March',
                 'May',
                 'November',
                 'October',
                 'September',
                 'ThisWeek',
                 'LastWeek']
    c = {'dateRange': dateRange,
         'Groups': listOfGroups,
         'startDate': startDate.isoformat(),
         'endDate': datetime.date.today().isoformat(),
         'Counter': GeneralFunctions.templateCounter()}
    ReqCon = RequestContext(request,processors=[SetMenu])
    return render_to_response('SchoolAttendanceReportSelectPage.html', c,
                              context_instance=ReqCon)


def SchoolAttendanceReportGenerate(request, AcYear, school):
    ''' Generates the whole school attendance report and emails it to the user '''
    report = AttendanceFunctions.WholeSchoolAtt(AcYear,
                                                school,
                                                request.POST['dateRange'],
                                                request.session['StaffId'])
    report.start()
    return HttpResponse('WillowTree will email you the report requested shortly',
                        content_type="text/plain")


def SchoolAttendanceReportGenerateForNonStandardDateRange(request, AcYear, school):
    """
    Generates the whole school attendance report and emails it to the user. The difference between this view and
    the SchoolAttendanceReportGenerate view is that this one gets data for non standard dates. For example 2013-08-11
    to 2013-08-21
    """
    report = AttendanceFunctions.WholeSchoolAtt(AcYear,
                                                school,
                                                (request.POST['startDate2'], request.POST['endDate2']),
                                                request.session['StaffId'])
    report.start()
    return HttpResponse('WillowTree will email you the report requested shortly',
                        content_type="text/plain")


def classAttendanceReportGenerate(request, AcYear, school):
    ''' Generates class attendance reports for selected classes and emails them
    to the user '''
    groupList = []
    for k, v in request.POST.iteritems():
        if 'Current.' in v:
            groupList.append(v)
    startDate = datetime.datetime.strptime(request.POST['startDate'], '%Y-%m-%d')
    endDate = datetime.datetime.strptime(request.POST['endDate'], '%Y-%m-%d')
    endDate = endDate + datetime.timedelta(days=1)
    report = AttendanceFunctions.AttendanceDataFromDateRange(AcYear,
                                                             school,
                                                             startDate,
                                                             endDate,
                                                             sorted(groupList),
                                                             request.session['StaffId'])
    report.start()
    return HttpResponse('WillowTree will email you the report requested shortly',
                        content_type="text/plain")