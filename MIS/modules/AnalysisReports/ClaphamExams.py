from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from MIS.modules import GeneralFunctions
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from MIS.modules.ExtendedRecords import *
from MIS.modules.Extracts import ExtendedRecordExtract
from MIS.modules.AnalysisReports import AnalysisReports
import urllib2
import threading
import Queue
import gspread
import simplejson
from django.core.mail import send_mail, EmailMessage
import logging
log = logging.getLogger(__name__)
from django.template import loader
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import render_to_response

class ClaphamYear3(threading.Thread):
    def __init__(self,AcYear,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.AcYear=AcYear
        self.Term=Term
        self.GoogleLink=GoogleLink

    def run(self):
        if 'M' in self.Term:
            self.RecGrade='Yr3_Eot_M'
        elif 'L' in self.Term:
            self.RecGrade='Yr3_Eot_L'
        else:
            self.RecGrade='Yr3_Eot_S'
        self.datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.MiddleSchool.Year3'):
            self.datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,
                                                                            self.AcYear,
                                                                            ['English','Mathematics'],
                                                                            [self.RecGrade]))
        self.YearSheet=self.GoogleLink.worksheet('Year 3')
        self.counter=6
        GradeRec=self.RecGrade
        self.__UploadData__()

    def __UploadData__(self):
        for records in self.datalist:
            self.YearSheet.update_acell('B%d' % self.counter,' ')
            self.YearSheet.update_acell('B%d' % self.counter, records.Pupil.Forename)
            self.YearSheet.update_acell('C%d' % self.counter,' ')
            self.YearSheet.update_acell('C%d' % self.counter, records.Pupil.Surname)
            self.YearSheet.update_acell('D%d' % self.counter,' ')
            self.YearSheet.update_acell('D%d' % self.counter, records.Pupil.Gender)
            self.YearSheet.update_acell('E%d' % self.counter,' ')
            self.YearSheet.update_acell('E%d' % self.counter, records.Form())
            self.YearSheet.update_acell('F%d' % self.counter,' ')
            self.YearSheet.update_acell('F%d' % self.counter, 'Add Later')
            try:
                self.YearSheet.update_acell('G%d' % self.counter,' ')
                self.YearSheet.update_acell('G%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('H%d' % self.counter,' ')
                self.YearSheet.update_acell('H%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('I%d' % self.counter,' ')
                self.YearSheet.update_acell('I%d' % self.counter, records.GradeRecords[self.RecGrade]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            self.counter+=1
        return

class ClaphamYear4(ClaphamYear3):

    def run(self):
        if 'M' in self.Term:
            self.RecGrade='Yr4_Eot_M'
        elif 'L' in self.Term:
            self.RecGrade='Yr4_Eot_L'
        else:
            self.RecGrade='Yr4_Eot_S'
        self.datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.MiddleSchool.Year4'):
            self.datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics','Science'],
                                                            [self.RecGrade]))
        self.YearSheet=self.GoogleLink.worksheet('Year 4')
        self.counter=10
        GradeRec=self.RecGrade
        self.__UploadData__()

class ClaphamYear5(ClaphamYear3):

    def run(self):
        if 'M' in self.Term:
            self.RecGrade='Yr5_Eot_M'
        elif 'L' in self.Term:
            self.RecGrade='Yr5_Eot_L'
        else:
            self.RecGrade='Yr5_Eot_S'
        self.datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.MiddleSchool.Year5'):
            self.datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics','Science'],
                                                            [self.RecGrade]))
        self.YearSheet=self.GoogleLink.worksheet('Year 5')
        self.counter=10
        GradeRec=self.RecGrade
        self.__UploadData__()

    def __UploadData__(self):
        for records in self.datalist:
            self.YearSheet.update_acell('B%d' % self.counter,' ')
            self.YearSheet.update_acell('B%d' % self.counter, records.Pupil.Forename)
            self.YearSheet.update_acell('C%d' % self.counter,' ')
            self.YearSheet.update_acell('C%d' % self.counter, records.Pupil.Surname)
            self.YearSheet.update_acell('D%d' % self.counter,' ')
            self.YearSheet.update_acell('D%d' % self.counter, records.Pupil.Gender)
            self.YearSheet.update_acell('E%d' % self.counter,' ')
            self.YearSheet.update_acell('E%d' % self.counter, records.Form())
            self.YearSheet.update_acell('F%d' % self.counter,' ')
            self.YearSheet.update_acell('F%d' % self.counter, 'Add Later')
            try:
                self.YearSheet.update_acell('G%d' % self.counter,' ')
                self.YearSheet.update_acell('G%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('H%d' % self.counter,' ')
                self.YearSheet.update_acell('H%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('I%d' % self.counter,' ')
                self.YearSheet.update_acell('I%d' % self.counter, records.GradeRecords[self.RecGrade]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('J%d' % self.counter,' ')
                self.YearSheet.update_acell('J%d' % self.counter, records.GradeRecords[self.RecGrade]['Science'].Grade()['Exam1'][0])
            except:
                pass
            self.counter+=1
        return

class ClaphamYear611(ClaphamYear5):

    def run(self):
        if 'M' in self.Term:
            self.RecGrade='Yr6_Eot_M'
        elif 'L' in self.Term:
            self.RecGrade='Yr6_Eot_L'
        else:
            self.RecGrade='Yr6_Eot_S'
        self.datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.UpperSchool.Year6.Yr6_Sets.11Plus'):
            self.datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics','Science'],
                                                            [self.RecGrade]))
        self.YearSheet=self.GoogleLink.worksheet('Year 611')
        self.counter=10
        GradeRec=self.RecGrade
        self.__UploadData__()

class ClaphamYear613(ClaphamYear5):

    def run(self):
        if 'M' in self.Term:
            self.RecGrade='Yr6_Eot_M'
        elif 'L' in self.Term:
            self.RecGrade='Yr6_Eot_L'
        else:
            self.RecGrade='Yr6_Eot_S'
        self.datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.UpperSchool.Year6.Yr6_Sets.13Plus'):
            self.datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics',
                                                                                               'Science','History','Geography',
                                                                                               'French','Religious_Education',
                                                                                               'Latin'],
                                                            [self.RecGrade]))
        self.YearSheet=self.GoogleLink.worksheet('Year 613')
        self.counter=10
        GradeRec=self.RecGrade
        self.__UploadData__()
    def __UploadData__(self):
        for records in self.datalist:
            self.YearSheet.update_acell('B%d' % self.counter,' ')
            self.YearSheet.update_acell('B%d' % self.counter, records.Pupil.Forename)
            self.YearSheet.update_acell('C%d' % self.counter,' ')
            self.YearSheet.update_acell('C%d' % self.counter, records.Pupil.Surname)
            self.YearSheet.update_acell('D%d' % self.counter,' ')
            self.YearSheet.update_acell('D%d' % self.counter, records.Pupil.Gender)
            self.YearSheet.update_acell('E%d' % self.counter,' ')
            self.YearSheet.update_acell('E%d' % self.counter, records.Form())
            self.YearSheet.update_acell('F%d' % self.counter,' ')
            self.YearSheet.update_acell('F%d' % self.counter, 'Add Later')
            try:
                self.YearSheet.update_acell('G%d' % self.counter,' ')
                self.YearSheet.update_acell('G%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('H%d' % self.counter,' ')
                self.YearSheet.update_acell('H%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('I%d' % self.counter,' ')
                self.YearSheet.update_acell('I%d' % self.counter, records.GradeRecords[self.RecGrade]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('J%d' % self.counter,' ')
                self.YearSheet.update_acell('J%d' % self.counter, records.GradeRecords[self.RecGrade]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('K%d' % self.counter,' ')
                self.YearSheet.update_acell('K%d' % self.counter, records.GradeRecords[self.RecGrade]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('L%d' % self.counter,' ')
                self.YearSheet.update_acell('L%d' % self.counter, records.GradeRecords[self.RecGrade]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('M%d' % self.counter,' ')
                self.YearSheet.update_acell('M%d' % self.counter, records.GradeRecords[self.RecGrade]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('N%d' % self.counter,' ')
                self.YearSheet.update_acell('N%d' % self.counter, records.GradeRecords[self.RecGrade]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('O%d' % self.counter,' ')
                self.YearSheet.update_acell('O%d' % self.counter, records.GradeRecords[self.RecGrade]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            self.counter+=1
        return

class ClaphamYear7E(ClaphamYear5):

    def run(self):
        if 'M' in self.Term:
            self.RecGrade='Yr7_Eot_M'
        elif 'L' in self.Term:
            self.RecGrade='Yr7_Eot_L'
        else:
            self.RecGrade='Yr7_Eot_S'
        self.datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.UpperSchool.Year7.Science.SetS'):
            self.datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics',
                                                                                               'Science','History','Geography',
                                                                                               'French','Religious_Education',
                                                                                               'Latin'],
                                                            [self.RecGrade]))
        self.YearSheet=self.GoogleLink.worksheet('Year 7E')
        self.counter=10
        GradeRec=self.RecGrade
        self.__UploadData__()
    def __UploadData__(self):
        for records in self.datalist:
            self.YearSheet.update_acell('B%d' % self.counter,' ')
            self.YearSheet.update_acell('B%d' % self.counter, records.Pupil.Forename)
            self.YearSheet.update_acell('C%d' % self.counter,' ')
            self.YearSheet.update_acell('C%d' % self.counter, records.Pupil.Surname)
            self.YearSheet.update_acell('D%d' % self.counter,' ')
            self.YearSheet.update_acell('D%d' % self.counter, records.Pupil.Gender)
            self.YearSheet.update_acell('E%d' % self.counter,' ')
            self.YearSheet.update_acell('E%d' % self.counter, records.Form())
            self.YearSheet.update_acell('F%d' % self.counter,' ')
            self.YearSheet.update_acell('F%d' % self.counter, 'Add Later')
            try:
                self.YearSheet.update_acell('G%d' % self.counter,' ')
                self.YearSheet.update_acell('G%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('H%d' % self.counter,' ')
                self.YearSheet.update_acell('H%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('I%d' % self.counter,' ')
                self.YearSheet.update_acell('I%d' % self.counter, records.GradeRecords[self.RecGrade]['Mathematics'].Grade()['Exam2'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('J%d' % self.counter,' ')
                self.YearSheet.update_acell('J%d' % self.counter, records.GradeRecords[self.RecGrade]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('K%d' % self.counter,' ')
                self.YearSheet.update_acell('K%d' % self.counter, records.GradeRecords[self.RecGrade]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('L%d' % self.counter,' ')
                self.YearSheet.update_acell('L%d' % self.counter, records.GradeRecords[self.RecGrade]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('M%d' % self.counter,' ')
                self.YearSheet.update_acell('M%d' % self.counter, records.GradeRecords[self.RecGrade]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('N%d' % self.counter,' ')
                self.YearSheet.update_acell('N%d' % self.counter, records.GradeRecords[self.RecGrade]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('O%d' % self.counter,' ')
                self.YearSheet.update_acell('O%d' % self.counter, records.GradeRecords[self.RecGrade]['Latin'].Grade()['Exam1'][0])
            except:
                pass

            self.counter+=1
        return


class ClaphamYear8E(ClaphamYear5):

    def run(self):
        if 'M' in self.Term:
            self.RecGrade='Yr8_Eot_M'
        elif 'L' in self.Term:
            self.RecGrade='Yr8_Eot_L'
        else:
            self.RecGrade='Yr8_Eot_S'
        self.datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.UpperSchool.Year8.Science.SetS'):
            self.datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics',
                                                                                               'Science','History','Geography',
                                                                                               'French','Religious_Education',
                                                                                               'Latin'],
                                                            [self.RecGrade]))
        self.YearSheet=self.GoogleLink.worksheet('Year 8E')
        self.counter=10
        GradeRec=self.RecGrade
        self.__UploadData__()
    def __UploadData__(self):
        for records in self.datalist:
            self.YearSheet.update_acell('B%d' % self.counter,' ')
            self.YearSheet.update_acell('B%d' % self.counter, records.Pupil.Forename)
            self.YearSheet.update_acell('C%d' % self.counter,' ')
            self.YearSheet.update_acell('C%d' % self.counter, records.Pupil.Surname)
            self.YearSheet.update_acell('D%d' % self.counter,' ')
            self.YearSheet.update_acell('D%d' % self.counter, records.Pupil.Gender)
            self.YearSheet.update_acell('E%d' % self.counter,' ')
            self.YearSheet.update_acell('E%d' % self.counter, records.Form())
            self.YearSheet.update_acell('F%d' % self.counter,' ')
            self.YearSheet.update_acell('F%d' % self.counter, 'Add Later')
            try:
                self.YearSheet.update_acell('G%d' % self.counter,' ')
                self.YearSheet.update_acell('G%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('H%d' % self.counter,' ')
                self.YearSheet.update_acell('H%d' % self.counter, records.GradeRecords[self.RecGrade]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('I%d' % self.counter,' ')
                self.YearSheet.update_acell('I%d' % self.counter, records.GradeRecords[self.RecGrade]['Science'].Grade()['Exam1'][0])
            except:
                pass

            try:
                self.YearSheet.update_acell('J%d' % self.counter,' ')
                self.YearSheet.update_acell('J%d' % self.counter, records.GradeRecords[self.RecGrade]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('K%d' % self.counter,' ')
                self.YearSheet.update_acell('K%d' % self.counter, records.GradeRecords[self.RecGrade]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('L%d' % self.counter,' ')
                self.YearSheet.update_acell('L%d' % self.counter, records.GradeRecords[self.RecGrade]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('M%d' % self.counter,' ')
                self.YearSheet.update_acell('M%d' % self.counter, records.GradeRecords[self.RecGrade]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('N%d' % self.counter,' ')
                self.YearSheet.update_acell('N%d' % self.counter, records.GradeRecords[self.RecGrade]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('O%d' % self.counter,' ')
                self.YearSheet.update_acell('O%d' % self.counter, records.GradeRecords[self.RecGrade]['Greek'].Grade()['Exam1'][0])
            except:
                pass
            self.counter+=1
        return

class ClaphamYear7(ClaphamYear5):

    def run(self):
        if 'M' in self.Term:
            self.RecGrade='Yr7_Eot_M'
        elif 'L' in self.Term:
            self.RecGrade='Yr7_Eot_L'
        else:
            self.RecGrade='Yr7_Eot_S'
        self.datalist=[]
        excemptList=GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.UpperSchool.Year7.Science.SetS')
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.UpperSchool.Year7'):
            if not records in excemptList:
                self.datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics',
                                                                                               'Biology','Chemistry','Physics','History','Geography',
                                                                                               'French','Religious_Education',
                                                                                               'Latin'],
                                                                           [self.RecGrade]))
        self.YearSheet=self.GoogleLink.worksheet('Year 7')
        self.counter=10
        GradeRec=self.RecGrade
        self.__UploadData__()
    def __UploadData__(self):
        for records in self.datalist:
            self.YearSheet.update_acell('B%d' % self.counter,' ')
            self.YearSheet.update_acell('B%d' % self.counter, records.Pupil.Forename)
            self.YearSheet.update_acell('C%d' % self.counter,' ')
            self.YearSheet.update_acell('C%d' % self.counter, records.Pupil.Surname)
            self.YearSheet.update_acell('D%d' % self.counter,' ')
            self.YearSheet.update_acell('D%d' % self.counter, records.Pupil.Gender)
            self.YearSheet.update_acell('E%d' % self.counter,' ')
            self.YearSheet.update_acell('E%d' % self.counter, records.Form())
            self.YearSheet.update_acell('F%d' % self.counter,' ')
            self.YearSheet.update_acell('F%d' % self.counter, 'Add Later')
            try:
                self.YearSheet.update_acell('G%d' % self.counter,' ')
                self.YearSheet.update_acell('G%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('H%d' % self.counter,' ')
                self.YearSheet.update_acell('H%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('I%d' % self.counter,' ')
                self.YearSheet.update_acell('I%d' % self.counter, records.GradeRecords[self.RecGrade]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('J%d' % self.counter,' ')
                self.YearSheet.update_acell('J%d' % self.counter, records.GradeRecords[self.RecGrade]['Biology'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('K%d' % self.counter,' ')
                self.YearSheet.update_acell('K%d' % self.counter, records.GradeRecords[self.RecGrade]['Chemistry'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('L%d' % self.counter,' ')
                self.YearSheet.update_acell('L%d' % self.counter, records.GradeRecords[self.RecGrade]['Physics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('M%d' % self.counter,' ')
                self.YearSheet.update_acell('M%d' % self.counter, records.GradeRecords[self.RecGrade]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('N%d' % self.counter,' ')
                self.YearSheet.update_acell('N%d' % self.counter, records.GradeRecords[self.RecGrade]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('O%d' % self.counter,' ')
                self.YearSheet.update_acell('O%d' % self.counter, records.GradeRecords[self.RecGrade]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('P%d' % self.counter,' ')
                self.YearSheet.update_acell('P%d' % self.counter, records.GradeRecords[self.RecGrade]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('Q%d' % self.counter,' ')
                self.YearSheet.update_acell('Q%d' % self.counter, records.GradeRecords[self.RecGrade]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            self.counter+=1
        return
class ClaphamYear8(ClaphamYear5):

    def run(self):
        if 'M' in self.Term:
            self.RecGrade='Yr8_Eot_M'
        elif 'L' in self.Term:
            self.RecGrade='Yr8_Eot_L'
        else:
            self.RecGrade='Yr8_Eot_S'
        self.datalist=[]
        excemptList=GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.UpperSchool.Year8.Science.SetS')
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Clapham.UpperSchool.Year8'):
            if not records in excemptList:
                self.datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics',
                                                                                               'Biology','Chemistry','History','Geography',
                                                                                               'French','Religious_Education','Physics',
                                                                                               'Latin','Greek'],
                                                                           [self.RecGrade]))
        self.YearSheet=self.GoogleLink.worksheet('Year 8')
        self.counter=10
        GradeRec=self.RecGrade
        self.__UploadData__()
    def __UploadData__(self):
        for records in self.datalist:
            self.YearSheet.update_acell('B%d' % self.counter,' ')
            self.YearSheet.update_acell('B%d' % self.counter, records.Pupil.Forename)
            self.YearSheet.update_acell('C%d' % self.counter,' ')
            self.YearSheet.update_acell('C%d' % self.counter, records.Pupil.Surname)
            self.YearSheet.update_acell('D%d' % self.counter,' ')
            self.YearSheet.update_acell('D%d' % self.counter, records.Pupil.Gender)
            self.YearSheet.update_acell('E%d' % self.counter,' ')
            self.YearSheet.update_acell('E%d' % self.counter, records.Form())
            self.YearSheet.update_acell('F%d' % self.counter,' ')
            self.YearSheet.update_acell('F%d' % self.counter, 'Add Later')
            try:
                self.YearSheet.update_acell('G%d' % self.counter,' ')
                self.YearSheet.update_acell('G%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('H%d' % self.counter,' ')
                self.YearSheet.update_acell('H%d' % self.counter, records.GradeRecords[self.RecGrade]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('I%d' % self.counter,' ')
                self.YearSheet.update_acell('I%d' % self.counter, records.GradeRecords[self.RecGrade]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('J%d' % self.counter,' ')
                self.YearSheet.update_acell('J%d' % self.counter, records.GradeRecords[self.RecGrade]['Mathematics'].Grade()['Exam2'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('K%d' % self.counter,' ')
                self.YearSheet.update_acell('K%d' % self.counter, records.GradeRecords[self.RecGrade]['Biology'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('L%d' % self.counter,' ')
                self.YearSheet.update_acell('L%d' % self.counter, records.GradeRecords[self.RecGrade]['Chemistry'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('M%d' % self.counter,' ')
                self.YearSheet.update_acell('M%d' % self.counter, records.GradeRecords[self.RecGrade]['Physics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('N%d' % self.counter,' ')
                self.YearSheet.update_acell('N%d' % self.counter, records.GradeRecords[self.RecGrade]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('O%d' % self.counter,' ')
                self.YearSheet.update_acell('O%d' % self.counter, records.GradeRecords[self.RecGrade]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('P%d' % self.counter,' ')
                self.YearSheet.update_acell('P%d' % self.counter, records.GradeRecords[self.RecGrade]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('Q%d' % self.counter,' ')
                self.YearSheet.update_acell('Q%d' % self.counter, records.GradeRecords[self.RecGrade]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('R%d' % self.counter,' ')
                self.YearSheet.update_acell('R%d' % self.counter, records.GradeRecords[self.RecGrade]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            try:
                self.YearSheet.update_acell('S%d' % self.counter,' ')
                self.YearSheet.update_acell('S%d' % self.counter, records.GradeRecords[self.RecGrade]['Greek'].Grade()['Exam1'][0])
            except:
                pass
            self.counter+=1
        return