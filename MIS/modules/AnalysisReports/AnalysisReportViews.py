# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader
from django.template import RequestContext
from django.core.context_processors import csrf
from django.http import *
from django.core import serializers
from MIS.modules import GeneralFunctions
import simplejson

# Django imports
from MIS.models import MenuTypes
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules.Extracts import ExtendedRecordExtract
from MIS.modules.AnalysisReports import ReportFunctions
from MIS.modules.AnalysisReports.AnalysisReports import WholeSchoolNumbersReport
from MIS.modules.dataimports.howManyGirlsBoysAndFamiliesInSchool import how_many_girls_boys_and_families_in_school


def AnalysisReportsView(request, school, AcYear, GroupName):
    ''' contains code to view analysis reports created from the pop-out in the
    pupil manager '''
    requestList = [['effortGrade', 'Effort'],
                   ['attainmentGrade', 'Attainment'],
                   ['levelGrade', 'Level'],
                   ['commentGrade', 'Comment']]
    for i in requestList:
        if request.POST['ReportName'] == i[0]:
            report = ExtendedRecordExtract.ExtendedRecordExtracts(AcYear).type1Report(request.POST['ReportSeason'],
                                                                                      request.POST.getlist('PupilSelect'),
                                                                                      i[1],
                                                                                      withSubSubjects=True)
            c = {'report': report}
    contextInstance = RequestContext(request,processors=[SetMenu])
    return render_to_response('Reports/analysisReport__effort_or_attainment_or_level_or_comment.html',
                              c, context_instance=contextInstance)

def AnalysisReportList(request,school,AcYear,GroupName,Term):
    data=ReportFunctions.EmailAnalysisReports(request,AcYear,school,GroupName,Term)
    data.start()
    return HttpResponse('You reports are being Processed and will be sent via email', content_type="text/plain")

def APSAverageRequest(request,school,AcYear,SubjectName):
    c={'ClassYears':GeneralFunctions.GetYearsAndForms(school),'AcYear':AcYear,
    'school':school,'SubjectName':SubjectName,'SchoolName':GeneralFunctions.GetSchoolName(school)}
    return render_to_response('Reports/APSSchoolReview.html',c,context_instance=RequestContext(request))

def APSAverage(request,school,AcYear,GroupName,PreviousRecord,CurrentRecord,SubjectName):
    return HttpResponse(ReportFunctions.APSProgress(GroupName,AcYear,PreviousRecord,CurrentRecord,SubjectName), content_type="text/plain")

def LevelsReview(request,school,AcYear,GroupName,SubjectName,NoYears):
    data=ReportFunctions.LevelsReview(request,AcYear,school,GroupName,int(NoYears),SubjectName)
    data.start()
    return HttpResponse('You report is being Processed and will be sent via email', content_type="text/plain")

def APSPupilProgress(request,school,AcYear,GroupName,SubjectName):
    data=ReportFunctions.APSPupilsProgress(request,AcYear,school,GroupName,SubjectName)
    data.start()
    return HttpResponse('You report is being Processed and will be sent via email', content_type="text/plain")


def whole_school_numbers_report(request, school, ac_year):
    """
    Produces a numbers report for a school
    """
    school_name = MenuTypes.objects.get(Name=school).SchoolId.Name
    header = "Thomas's %s Pupil Numbers Report" % school_name
    c = {'data': WholeSchoolNumbersReport(school, ac_year).get_data_for_template(),
         'number_of_families': how_many_girls_boys_and_families_in_school(school_name, ac_year)['families'],
         'header': header}
    context_instance = RequestContext(request, processors=[SetMenu])
    return render_to_response('Reports/whole_school_numbers_report.html',
                              c, context_instance=context_instance)
