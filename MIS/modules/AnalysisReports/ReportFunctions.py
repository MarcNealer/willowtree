from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from MIS.modules import GeneralFunctions
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from MIS.modules.ExtendedRecords import *
from MIS.modules.Extracts import ExtendedRecordExtract
from MIS.modules.AnalysisReports import AnalysisReports
import urllib2
import threading
import Queue
import gspread
import simplejson
from django.core.mail import send_mail, EmailMessage
import logging
log = logging.getLogger(__name__)
from django.template import loader
from django.template import RequestContext
from django.http import HttpResponse
from django.shortcuts import render_to_response
import ClaphamExams

class AgeCensus():
    def __init__(self,SchoolGroup):
        self.AcYear=GlobalVariables.Get_SystemVar('CurrentYear')
        self.Year=int(self.AcYear.split('-')[0])
        self.PupilList=Pupil.objects.filter(id__in=[x.id for x in GeneralFunctions.Get_PupilList(self.AcYear,SchoolGroup)])
    def GetCounts(self):
        ReturnData=[]
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-15),'%d-08-31' % (self.Year-14)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-14),'%d-08-31' % (self.Year-13)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-13),'%d-08-31' % (self.Year-12)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-12),'%d-08-31' % (self.Year-11)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-11),'%d-08-31' % (self.Year-10)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-10),'%d-08-31' % (self.Year-9)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-9),'%d-08-31' % (self.Year-8)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-8),'%d-08-31' % (self.Year-7)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-7),'%d-08-31' % (self.Year-6)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-6),'%d-08-31' % (self.Year-5)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-5),'%d-12-31' % (self.Year-5)))
        ReturnData.append(self.DataCounts('%d-01-01' % (self.Year-4),'%d-03-31' % (self.Year-4)))
        ReturnData.append(self.DataCounts('%d-04-01' % (self.Year-4),'%d-08-31' % (self.Year-4)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-4),'%d-12-31' % (self.Year-4)))
        ReturnData.append(self.DataCounts('%d-01-01' % (self.Year-3),'%d-03-31' % (self.Year-3)))
        ReturnData.append(self.DataCounts('%d-04-01' % (self.Year-3),'%d-08-31' % (self.Year-3)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-3),'%d-08-31' % (self.Year-2)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-2),'%d-03-31' % (self.Year-1)))
        ReturnData.append(self.DataCounts('%d-09-01' % (self.Year-1),'%d-08-31' % (self.Year)))
        return ReturnData
    def DataCounts(self,Start,Stop):
        Data={}
        Data['Total']=self.PupilList.filter(DateOfBirth__gte=Start,DateOfBirth__lte=Stop).count()
        Data['Boys']=self.PupilList.filter(DateOfBirth__gte=Start,DateOfBirth__lte=Stop,Gender='M').count()
        Data['Girls']=self.PupilList.filter(DateOfBirth__gte=Start,DateOfBirth__lte=Stop,Gender='F').count()
        return {'%s-%s' % (Start,Stop):Data}

class EYFS_Map():
    def __init__(self,GroupName,AcYear,EYFSArea):
        self.pupillist=set([x.Pupil for x in PupilGroup.objects.filter(Group__Name__istartswith=GroupName,AcademicYear=AcYear,Active=True)])
        self.AcYear=AcYear
        self.EYFSArea=EYFSArea
        self.GroupName=GroupName
        self.OnEntryData={'3050':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                   '4060':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                    'ELG':{'Emerging':{'Pupils':[],'Percentage':0},'Expected':{'Pupils':[],'Percentage':0},'Exceeded':{'Pupils':[],'Percentage':0}}}

        self.MichData={'3050':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                   '4060':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                    'ELG':{'Emerging':{'Pupils':[],'Percentage':0},'Expected':{'Pupils':[],'Percentage':0},'Exceeded':{'Pupils':[],'Percentage':0}}}

        self.LentData={'3050':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                   '4060':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                    'ELG':{'Emerging':{'Pupils':[],'Percentage':0},'Expected':{'Pupils':[],'Percentage':0},'Exceeded':{'Pupils':[],'Percentage':0}}}

        self.SummerData={'3050':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                   '4060':{'Beginning':{'Pupils':[],'Percentage':0},'Development':{'Pupils':[],'Percentage':0},'Secure':{'Pupils':[],'Percentage':0}},
                    'ELG':{'Emerging':{'Pupils':[],'Percentage':0},'Expected':{'Pupils':[],'Percentage':0},'Exceeded':{'Pupils':[],'Percentage':0}}}
        self.__PopulateGrid__()
        self.__SetPercentages__()
    def __PopulateGrid__(self):
        for pupils in self.pupillist:
            eyfsrec=ExtendedRecord('Pupil',pupils.id)
            OnEntry=eyfsrec.ReadExtention('EYFS_OnEntry','Form_Comment')[self.EYFSArea][0].replace('-','').split(':')

            print OnEntry
            MichTerm=eyfsrec.ReadExtention('EYFS_Mich','Form_Comment')[self.EYFSArea][0].replace('-','').split(':')
            print OnEntry
            LentTerm=eyfsrec.ReadExtention('EYFS_Lent','Form_Comment')[self.EYFSArea][0].replace('-','').split(':')
            print OnEntry
            SummerTerm=eyfsrec.ReadExtention('EYFS_Summer','Form_Comment')[self.EYFSArea][0].replace('-','').split(':')


            if len(OnEntry) > 1:
                if OnEntry[1]=='beginning':
                    OnEntry[1]='Beginning'
                self.OnEntryData[OnEntry[0]][OnEntry[1]]['Pupils'].append(pupils.FullName())
            if len(MichTerm) > 1:
                if MichTerm[1]=='beginning':
                    MichTerm[1]='Beginning'
                self.MichData[MichTerm[0]][MichTerm[1]]['Pupils'].append(pupils.FullName())
            if len(LentTerm) > 1:
                if LentTerm[1]=='beginning':
                    LentTerm[1]='Beginning'
                self.LentData[LentTerm[0]][LentTerm[1]]['Pupils'].append(pupils.FullName())
            if len(SummerTerm) > 1:
                if SummerTerm[1]=='beginning':
                    SummerTerm[1]='Beginning'
                self.SummerData[SummerTerm[0]][SummerTerm[1]]['Pupils'].append(pupils.FullName())
    def __SetPercentages__(self):
        PupilCount=len(self.pupillist)

        for area1 in ['3050','4060']:
            for area2 in ['Beginning','Development','Secure']:
                if len(self.OnEntryData[area1][area2]['Pupils']) > 0:
                    self.OnEntryData[area1][area2]['Percentage']=float(len(self.OnEntryData[area1][area2]['Pupils']))/PupilCount * 100

                if len(self.MichData[area1][area2]['Pupils']) > 0:
                    self.MichData[area1][area2]['Percentage']=float(len(self.MichData[area1][area2]['Pupils']))/PupilCount * 100

                if len(self.LentData[area1][area2]['Pupils']) > 0:
                    self.LentData[area1][area2]['Percentage']=float(len(self.LentData[area1][area2]['Pupils']))/PupilCount * 100

                if len(self.SummerData[area1][area2]['Pupils']) > 0:
                    self.SummerData[area1][area2]['Percentage']=float(len(self.SummerData[area1][area2]['Pupils']))/PupilCount * 100

        for area in ['Emerging','Expected','Exceeded']:
            if len(self.OnEntryData['ELG'][area]['Pupils']) > 0:
                self.OnEntryData['ELG'][area]['Percentage']=float(len(self.OnEntryData['ELG'][area]['Pupils']))/PupilCount * 100
            if len(self.MichData['ELG'][area]['Pupils']) > 0:
                self.MichData['ELG'][area]['Percentage']=float(len(self.MichData['ELG'][area]['Pupils']))/PupilCount * 100
            if len(self.LentData['ELG'][area]['Pupils']) > 0:
                self.LentData['ELG'][area]['Percentage']=float(len(self.LentData['ELG'][area]['Pupils']))/PupilCount * 100
            if len(self.SummerData['ELG'][area]['Pupils']) > 0:
                self.SummerData['ELG'][area]['Percentage']=float(len(self.SummerData['ELG'][area]['Pupils']))/PupilCount * 100

class EmailAnalysisReports(threading.Thread):
    ''' Thread used to create a list of requested reports attach them in an email and return them'''
    def __init__(self,request,AcYear,school,GroupName,Term):
        threading.Thread.__init__(self)
        self.AcYear=AcYear
        self.request=request
        self.UserRec=request.session['StaffId']
        self.school=school
        self.SchoolName=GeneralFunctions.GetSchoolName(self.school)
        self.GroupName=GroupName
        self.Term=Term
    def run(self):
        reports=self.request.POST.getlist('AnalysisReportList')
        FileList=[]
        SubjectList=[]
        for report in reports:
            ReportDetails=simplejson.loads(report)
            ThisReport=getattr(AnalysisReports,ReportDetails['Report'])
            SubjectList.append(ReportDetails['Subject'])
            try:
                rep=ThisReport(self.request,self.AcYear,self.school,self.SchoolName,self.GroupName,ReportDetails)
                FileList.append(rep.RenderReport())
            except:
                log.warn('Report Failed')
        log.warn(FileList)
        self.SendEmailWithAttachments(FileList,SubjectList)

    def SendEmailWithAttachments(self,FileList,SubjectList):
        if self.UserRec.EmailAddress:
            msg=EmailMessage('Analysis files for %s %s' % ('.'.join(self.GroupName.split('.')[2:]),','.join(set(SubjectList))),
                             'Analysis files for %s' % ','.join(SubjectList),
                             SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data,
                            [self.UserRec.EmailAddress])
            for reports in FileList:
                if reports:
                    msg.attach(reports['Name'],reports['Data'],reports['Type'])
            msg.send()



class BatterseaYear3(threading.Thread):
    def __init__(self,AcYear,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.AcYear=AcYear
        self.Term=Term
        self.GoogleLink=GoogleLink

    def run(self):
        if 'M' in self.Term:
            RecGrade='Yr3_Eot_M'
        elif 'L' in self.Term:
            RecGrade='Yr3_Eot_L'
        else:
            RecGrade='Yr3_Eot_S'
        datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Battersea.MiddleSchool.Year3'):
            datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','English_Reading',
                                                                                    'English_Writing','English_Speaking',
                                                                                    'Mathematics','Science'],
                                                            [RecGrade]))
        YearSheet=self.GoogleLink.worksheet('Year 3')
        counter=10
        GradeRec='Yr3_Eot_%s' % self.Term
        for records in datalist:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English_Reading'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['English_Writing'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['English_Speaking'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Level'][0])
            except:
                pass
            counter+=1
        return



class BatterseaYear4(BatterseaYear3):
    def run(self):
        if 'M' in self.Term:
            RecGrade='Yr4_Eot_M'
        elif 'L' in self.Term:
            RecGrade='Yr4_Eot_L'
        else:
            RecGrade='Yr4_Eot_S'
        datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Battersea.MiddleSchool.Year4'):
            datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','English_Reading',
                                                                                    'English_Writing','English_Speaking',
                                                                                    'Mathematics','Science'],
                                                            [RecGrade]))
        YearSheet=self.GoogleLink.worksheet('Year 4')
        counter=10
        GradeRec='Yr4_Eot_%s' % self.Term
        for records in datalist:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English_Reading'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['English_Writing'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['English_Speaking'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Level'][0])
            except:
                pass
            counter+=1

class BatterseaYear5(BatterseaYear3):
    def run(self):
        if 'M' in self.Term:
            RecGrade='Yr5_Eot_M'
        elif 'L' in self.Term:
            RecGrade='Yr5_Eot_L'
        else:
            RecGrade='Yr5_Eot_S'
        datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Battersea.MiddleSchool.Year5'):
            datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','English_Reading',
                                                                                    'English_Writing','English_Speaking',
                                                                                    'Mathematics','Science','French'],
                                                            [RecGrade]))

        YearSheet=self.GoogleLink.worksheet('Year 5')
        counter=6
        GradeRec='Yr5_Eot_%s' % self.Term
        for records in datalist:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English_Reading'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['English_Writing'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['English_Speaking'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Level'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('P%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Level'][0])
            except:
                pass
            counter+=1

class BatterseaYear611(BatterseaYear3):
    def run(self):
        if 'M' in self.Term:
            RecGrade='Yr6_Eot_M'
        elif 'L' in self.Term:
            RecGrade='Yr6_Eot_L'
        else:
            RecGrade='Yr6_Eot_S'
        datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Battersea.UpperSchool.Year6.Yr6_Sets.11Plus'):
            datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics','Science'],
                                                            [RecGrade]))

        YearSheet=self.GoogleLink.worksheet('Year 611')
        counter=10
        GradeRec='Yr6_Eot_%s' % self.Term
        for records in datalist:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1
class BatterseaYear613(BatterseaYear3):
    def run(self):
        if 'M' in self.Term:
            RecGrade='Yr6_Eot_M'
        elif 'L' in self.Term:
            RecGrade='Yr6_Eot_L'
        else:
            RecGrade='Yr6_Eot_S'
        datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Battersea.UpperSchool.Year6.Yr6_Sets.13Plus'):
            datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics','Religious_Education','Latin',
                                                                                   'Science','Geography','French','History'],
                                                            [RecGrade]))

        YearSheet=self.GoogleLink.worksheet('Year 613')
        counter=10
        GradeRec='Yr6_Eot_%s' % self.Term
        for records in datalist:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1

class BatterseaYear7(BatterseaYear3):
    def run(self):
        if 'M' in self.Term:
            RecGrade='Yr7_Eot_M'
        elif 'L' in self.Term:
            RecGrade='Yr7_Eot_L'
        else:
            RecGrade='Yr7_Eot_S'
        datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Battersea.UpperSchool.Year7'):
            datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics','Religious_Education','French',
                                                                                   'Biology','Physics','Chemistry','Latin','Geography','History'],
                                                            [RecGrade]))

        YearSheet=self.GoogleLink.worksheet('Year 7')
        counter=10
        GradeRec='Yr7_Eot_%s' % self.Term
        for records in datalist:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam3'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Biology'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Chemistry'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Physics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('P%d' % counter,' ')
                YearSheet.update_acell('P%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('Q%d' % counter,' ')
                YearSheet.update_acell('Q%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('R%d' % counter,' ')
                YearSheet.update_acell('R%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('S%d' % counter,' ')
                YearSheet.update_acell('S%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('T%d' % counter,' ')
                YearSheet.update_acell('T%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('U%d' % counter,' ')
                YearSheet.update_acell('U%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1
class BatterseaYear8(BatterseaYear3):
    def run(self):
        if 'M' in self.Term:
            RecGrade='Yr8_Eot_M'
        elif 'L' in self.Term:
            RecGrade='Yr8_Eot_L'
        else:
            RecGrade='Yr8_Eot_S'
        datalist=[]
        for records in GeneralFunctions.Get_PupilList(self.AcYear,
                                                      GroupName='Current.Battersea.UpperSchool.Year8'):
            datalist.append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear,['English','Mathematics','Religious_Education','French',
                                                                                   'Biology','Physics','Chemistry','Latin','Geography','History'],
                                                            [RecGrade]))


        YearSheet=self.GoogleLink.worksheet('Year 8')
        counter=10
        GradeRec='Yr8_Eot_%s' % self.Term
        for records in datalist:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam3'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Biology'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['Chemistry'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Physics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('P%d' % counter,' ')
                YearSheet.update_acell('P%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('Q%d' % counter,' ')
                YearSheet.update_acell('Q%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1Type'][0])
            except:
                pass
            try:
                YearSheet.update_acell('R%d' % counter,' ')
                YearSheet.update_acell('R%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('S%d' % counter,' ')
                YearSheet.update_acell('S%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('T%d' % counter,' ')
                YearSheet.update_acell('T%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                print records.GradeRecords[GradeRec]['Religious_Education'].Grade()
                YearSheet.update_acell('U%d' % counter,' ')
                YearSheet.update_acell('U%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])

            except:
                pass
            counter+=1
class ExamDataExtract(threading.Thread):
    def __init__(self,AcYear,school,Term, StaffId):
        threading.Thread.__init__(self)
        self.AcYear=AcYear
        self.school=school
        self.StaffId=StaffId
        self.FullTerm=Term
        if 'Mich' in Term:
            self.Term='M'
        elif 'Lent' in Term:
            self.Term='L'
        else:
            self.Term='S'

    def run(self):
        if 'Battersea' in self.school:
            self.Battersea()
        elif 'Clapham' in self.school:
            self.Clapham()
        elif 'Fulham' in self.school:
            self.Fulham()
        elif 'Kensington' in self.school:
            self.Kensington
    def Clapham(self):

        # Create dictionary for data

        self.GoogleClient1=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook1=self.GoogleClient1.open_by_key('0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE')
        print self.GoogleWorkBook1
        thread1=ClaphamExams.ClaphamYear3(self.AcYear,self.GoogleWorkBook1,self.Term)
        thread1.start()

        self.GoogleClient2=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook2=self.GoogleClient2.open_by_key('0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE')
        print self.GoogleWorkBook2
        thread2=ClaphamExams.ClaphamYear4(self.AcYear,self.GoogleWorkBook2,self.Term)
        thread2.start()

        self.GoogleClient3=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook3=self.GoogleClient3.open_by_key('0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE')
        print self.GoogleWorkBook3
        thread3=ClaphamExams.ClaphamYear5(self.AcYear,self.GoogleWorkBook3,self.Term)
        thread3.start()

        self.GoogleClient4=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook4=self.GoogleClient4.open_by_key('0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE')
        print self.GoogleWorkBook4
        thread4=ClaphamExams.ClaphamYear611(self.AcYear,self.GoogleWorkBook4,self.Term)
        thread4.start()

        self.GoogleClient5=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook5=self.GoogleClient5.open_by_key('0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE')
        print self.GoogleWorkBook2
        thread5=ClaphamExams.ClaphamYear613(self.AcYear,self.GoogleWorkBook5,self.Term)
        thread5.start()

        self.GoogleClient6=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook6=self.GoogleClient6.open_by_key('0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE')
        print self.GoogleWorkBook6
        thread6=ClaphamExams.ClaphamYear7(self.AcYear,self.GoogleWorkBook6,self.Term)
        thread6.start()

        self.GoogleClient7=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook7=self.GoogleClient7.open_by_key('0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE')
        print self.GoogleWorkBook7
        thread7=ClaphamExams.ClaphamYear8(self.AcYear,self.GoogleWorkBook7,self.Term)
        thread7.start()

        self.GoogleClient8=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook8=self.GoogleClient8.open_by_key('0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE')
        print self.GoogleWorkBook8
        thread8=ClaphamExams.ClaphamYear7E(self.AcYear,self.GoogleWorkBook8,self.Term)
        thread8.start()

        self.GoogleClient9=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook9=self.GoogleClient9.open_by_key('0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE')
        print self.GoogleWorkBook9
        thread9=ClaphamExams.ClaphamYear8E(self.AcYear,self.GoogleWorkBook9,self.Term)
        thread9.start()


    def Fulham(self):
        # Create dictionary for data
        Sheet_Key='0Av-_dbDrUv-KdE14MUFBSGtQZzl4QWpzZS1Fel84M3c'
        returnData={'Year 5':[],'Year 6':[]}
        # for each year group get a list of pupils and create the grade object
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Fulham.PrepSchool.Year5'):
            returnData['Year 5'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        for records in GeneralFunctions.Get_PupilList(self.AcYear,GroupName='Current.Fulham.PrepSchool.Year6'):
            returnData['Year 6'].append(ExtendedRecordExtract.GradeAnalysisExtract(records.id,self.AcYear))
        # link to the Google acccout
        self.GoogleClient=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook=self.GoogleClient.open_by_key(Sheet_Key)

        thread3=FulhamYear5(returnData['Year 5'],self.GoogleWorkBook,self.Term)
        thread3.start()
        thread4=FulhamYear6(returnData['Year 6'],self.GoogleWorkBook,self.Term)
        thread4.start()

        url_format='https://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=0Av-_dbDrUv-KdHVDM0ZBaEcySTRoSHFkYVFacVNBalE&exportFormat=xls'
        Header='Exam data for %s %s' % (self.FullTerm,self.AcYear)
        BodyText='Dear %s \nPlease find attached your requested Exam data for %s %s \n\nWillowTree' % (self.StaffId.Forename,self.FullTerm,self.AcYear)
        filename='Clapham_%s_%s.xls' % (self.FullTerm,self.AcYear)
        self.__SendEmail__(Header,BodyText,url_format,filename)
    def Kensington(self):
        pass
    def Battersea(self):
        # Create dictionary for data

        returnData={'Year 3':[],'Year 4':[],'Year 5':[],'Year 611':[],'Year 613':[],'Year 7':[],'Year 8':[]}

        self.GoogleClient1=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook1=self.GoogleClient1.open_by_key('0Av-_dbDrUv-KdFkxbGtfU20zbmxMTmFxU0VuMUQ4VWc')
        thread1=BatterseaYear3(self.AcYear,self.GoogleWorkBook1,self.Term)
        thread1.start()

        self.GoogleClient2=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook2=self.GoogleClient2.open_by_key('0Av-_dbDrUv-KdFkxbGtfU20zbmxMTmFxU0VuMUQ4VWc')
        thread2=BatterseaYear4(self.AcYear,self.GoogleWorkBook2,self.Term)
        thread2.start()

        self.GoogleClient3=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook3=self.GoogleClient3.open_by_key('0Av-_dbDrUv-KdFkxbGtfU20zbmxMTmFxU0VuMUQ4VWc')
        thread3=BatterseaYear5(self.AcYear,self.GoogleWorkBook3,self.Term)
        thread3.start()

        self.GoogleClient4=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook4=self.GoogleClient4.open_by_key('0Av-_dbDrUv-KdFkxbGtfU20zbmxMTmFxU0VuMUQ4VWc')
        thread4=BatterseaYear611(self.AcYear,self.GoogleWorkBook4,self.Term)
        thread4.start()

        self.GoogleClient5=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook5=self.GoogleClient5.open_by_key('0Av-_dbDrUv-KdFkxbGtfU20zbmxMTmFxU0VuMUQ4VWc')
        thread5=BatterseaYear613(self.AcYear,self.GoogleWorkBook5,self.Term)
        thread5.start()

        self.GoogleClient6=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook6=self.GoogleClient6.open_by_key('0Av-_dbDrUv-KdFkxbGtfU20zbmxMTmFxU0VuMUQ4VWc')
        thread6=BatterseaYear7(self.AcYear,self.GoogleWorkBook6,self.Term)
        thread6.start()

        self.GoogleClient7=gspread.login('marcnealer@gmail.com','lqcqynvkyxvlcsqx')
        self.GoogleWorkBook7=self.GoogleClient7.open_by_key('0Av-_dbDrUv-KdFkxbGtfU20zbmxMTmFxU0VuMUQ4VWc')
        thread7=BatterseaYear8(self.AcYear,self.GoogleWorkBook7,self.Term)
        thread7.start()

        url_format='https://spreadsheets.google.com/feeds/download/spreadsheets/Export?key=0Av-_dbDrUv-KdFkxbGtfU20zbmxMTmFxU0VuMUQ4VWc&exportFormat=xls'
        #Header='Exam data for %s %s' % (self.FullTerm,self.AcYear)
        #BodyText='Dear %s \nPlease find attached your requested Exam data for %s %s \n\nWillowTree' % (self.StaffId.Forename,self.FullTerm,self.AcYear)
        #filename='Clapham_%s_%s.xls' % (self.FullTerm,self.AcYear)
        #self.__SendEmail__(Header,BodyText,url_format,filename)

    def __SendEmail__(self,Header, Text ,urlLink, filename):
        if self.StaffId.EmailAddress:
            req= urllib2.Request(urlLink,headers=self.GoogleClient.session.headers)
            SenderEmail=self.StaffId.EmailAddress
            msg=EmailMessage(Header,
                             'Please find your requested file attached \n WillowTree',
                             SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data,
                             [SenderEmail])
            downloadfile=urllib2.urlopen(req)


            msg.attach(filename,downloadfile.read(),'application/vnd.ms-excel')
            msg.send()
            log.warn('Email sent')


class FulhamYear5(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
        YearSheet=self.GoogleLink.worksheet('Year 5')
        counter=10
        GradeRec='Yr5_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1
        return


class FulhamYear6(threading.Thread):
    def __init__(self,Data,GoogleLink,Term):
        threading.Thread.__init__(self)
        self.Data=Data
        self.Term=Term
        self.GoogleLink=GoogleLink
    def run(self):
        YearSheet=self.GoogleLink.worksheet('Year 6')
        counter=10
        GradeRec='Yr6_Eot_%s' % self.Term
        for records in self.Data:
            YearSheet.update_acell('B%d' % counter,' ')
            YearSheet.update_acell('B%d' % counter, records.Pupil.Forename)
            YearSheet.update_acell('C%d' % counter,' ')
            YearSheet.update_acell('C%d' % counter, records.Pupil.Surname)
            YearSheet.update_acell('D%d' % counter,' ')
            YearSheet.update_acell('D%d' % counter, records.Pupil.Gender)
            YearSheet.update_acell('E%d' % counter,' ')
            YearSheet.update_acell('E%d' % counter, records.Form())
            YearSheet.update_acell('F%d' % counter,' ')
            YearSheet.update_acell('F%d' % counter, 'Add Later')
            try:
                YearSheet.update_acell('G%d' % counter,' ')
                YearSheet.update_acell('G%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('H%d' % counter,' ')
                YearSheet.update_acell('H%d' % counter, records.GradeRecords[GradeRec]['English'].Grade()['Exam2'][0])
            except:
                pass
            try:
                YearSheet.update_acell('I%d' % counter,' ')
                YearSheet.update_acell('I%d' % counter, records.GradeRecords[GradeRec]['Mathematics'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('J%d' % counter,' ')
                YearSheet.update_acell('J%d' % counter, records.GradeRecords[GradeRec]['Science'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('K%d' % counter,' ')
                YearSheet.update_acell('K%d' % counter, records.GradeRecords[GradeRec]['French'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('L%d' % counter,' ')
                YearSheet.update_acell('L%d' % counter, records.GradeRecords[GradeRec]['Geography'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('M%d' % counter,' ')
                YearSheet.update_acell('M%d' % counter, records.GradeRecords[GradeRec]['History'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('N%d' % counter,' ')
                YearSheet.update_acell('N%d' % counter, records.GradeRecords[GradeRec]['Latin'].Grade()['Exam1'][0])
            except:
                pass
            try:
                YearSheet.update_acell('O%d' % counter,' ')
                YearSheet.update_acell('O%d' % counter, records.GradeRecords[GradeRec]['Religious_Education'].Grade()['Exam1'][0])
            except:
                pass
            counter+=1
        return

def APS_Average(GroupName,AcYear,GradeRec,SubjectName):
    Counter = 0
    Total = 0
    try:
        pupillist=GeneralFunctions.Get_PupilList(AcYear,GroupName)
    except:
        return False
    for pupils in pupillist:
        GradeExt=ExtendedRecord('Pupil',pupils.id)
        GradeItem=GradeExt.ReadExtention(GradeRec,SubjectName)
        if GradeItem['Level'][1]:
            try:
                Total+=int(GradeItem['Level'][1])
                Counter+=1
            except:
                pass
    if Counter > 0 and Total > 0:
        cache.set('%s-%s-s%s' % (GradeRec,SubjectName,AcYear),(Total/Counter),3600)
        return (float(Total)/float(Counter))
    else:
        return False

def APSProgress(GroupName,AcYear,PreviousRecord,CurrentRecord,SubjectName):
    print PreviousRecord
    print CurrentRecord
    PreviousAPS=cache.get('%s-%s-%s' % (PreviousRecord,SubjectName,AcYear),False)
    if not PreviousAPS:
        PreviousAPS=APS_Average(GroupName,AcYear,PreviousRecord,SubjectName)
    CurrentAPS=cache.get('%s-%s-%s' % (CurrentRecord,SubjectName,AcYear),False)
    if not CurrentAPS:
        CurrentAPS=APS_Average(GroupName,AcYear,CurrentRecord,SubjectName)
    if CurrentAPS and PreviousAPS:
        print CurrentAPS
        print PreviousAPS
        return '%3.2f (%3.2f)' % (CurrentAPS-PreviousAPS,CurrentAPS)
    else:
        print CurrentAPS
        print PreviousAPS
        return 'Calc Error'
