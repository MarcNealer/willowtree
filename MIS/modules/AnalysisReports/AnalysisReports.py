from django.shortcuts import render_to_response
from django.template.loader import render_to_string
from django.template import loader
from django.template import RequestContext
from django.core.context_processors import csrf
from django.http import HttpResponse
from django.conf import settings
import logging
import cPickle
import base64
import simplejson
import datetime
import threading
import numpy
import collections
import csv
# Django imports

from MIS.modules.Extracts import ExtendedRecordExtract
from MIS.models import *
from MIS.modules.PupilRecs import *
from MIS.modules import GeneralFunctions

log = logging.getLogger(__name__)

class AnalysisStoreReports(threading.Thread):
    ''' Thread used to create a list of requested reports attach them in an email and return them'''
    def __init__(self,request,AcYear,school,GroupName,Term):
        threading.Thread.__init__(self)
        self.AcYear=AcYear
        self.request=request
        self.UserRec=request.session['StaffId']
        self.school=school
        self.SchoolName=GeneralFunctions.GetSchoolName(self.school)
        self.GroupName=GroupName
        self.Term=Term
    def run(self):
        reports=self.request.POST.getlist('AnalysisReportList')
        FileList=[]
        SubjectList=[]
        for report in reports:
            ReportDetails=simplejson.loads(report)
            ThisReport=getattr(AnalysisReports,ReportDetails['Report'])
            SubjectList.append(ReportDetails['Subject'])
            try:
                rep=ThisReport(self.request,self.AcYear,self.school,self.SchoolName,self.GroupName,ReportDetails)
                FileList.append(rep.RenderReport())
            except:
                log.warn('Report Failed')
        log.warn(FileList)
        self.SendEmailWithAttachments(FileList,SubjectList)

    def SendEmailWithAttachments(self,FileList,SubjectList):
        if self.UserRec.EmailAddress:
            msg=EmailMessage('Analysis files for %s %s' % ('.'.join(self.GroupName.split('.')[2:]),','.join(set(SubjectList))),
                             'Analysis files for %s' % ','.join(SubjectList),
                             SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data,
                            [self.UserRec.EmailAddress])
            for reports in FileList:
                if reports:
                    msg.attach(reports['Name'],reports['Data'],reports['Type'])
            msg.send()

class LevelsReview():
    ''' Creates the Levels review reports '''

    def __init__(self, request, AcYear, school,
                 SchoolName, GroupName, ReportDetails):
        self.request = request
        self.AcYear = AcYear
        self.school = school
        self.SchoolName = SchoolName
        self.GroupName = GroupName
        self.ReportDetails = ReportDetails
        self.SubjectName = ReportDetails['Subject']
        self.NoYears = 6
        self.ClassList = self.__GetClasses__()
        self.RecordList = self.__GetRecordList__()
        self.LevelNames = self.__LevelNames__()
        self.today = datetime.datetime.today()

    def RenderReport(self):
        c = {'ClassYears': GeneralFunctions.GetYearsAndForms(self.school),
             'AcYear': self.AcYear,'school': self.school,
             'SubjectName': self.SubjectName,
             'SchoolName': GeneralFunctions.GetSchoolName(self.school),
             'DataObj': self}
        DataFile = render_to_string('Reports/LevelsReview.html', c)
        return {'Name': 'LevelsReview_%s_%s.html' % (self.GroupName.split('.')[-1],
                                                     self.SubjectName),
                'Data': DataFile, 'Type': 'text/html'}

    def __GetClasses__(self):
        self.parentSubject=None
        if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % self.SubjectName).exists():
            classlist = [x.MenuName for x in Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % self.SubjectName)]

        elif ParentSubjects.objects.filter(Subject__Name=self.SubjectName).exists():
            self.parentSubject=ParentSubjects.objects.filter(Subject__Name=self.SubjectName)[0].ParentSubject.Name
            if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % self.parentSubject).exists():
                classlist=[x.MenuName for x in Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % self.parentSubject)]
            else:
                if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames').exists():
                    classlist=[x.MenuName for x in Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames')]
        else:
            if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames').exists():
                classlist=[x.MenuName for x in Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames')]
        return set(classlist)

    def __GetRecordList__(self):
        if 'Year1' in self.GroupName:
            return ['Yr1_Eot_M', 'Yr1_Eot_L', 'Yr1_Eot_S']
        elif 'Year2' in self.GroupName:
            return ['Yr1_Eot_M', 'Yr1_Eot_L', 'Yr1_Eot_S',
                    'Yr2_Eot_M', 'Yr2_Eot_L', 'Yr2_Eot_S']
        elif 'Year3' in self.GroupName:
             return ['Yr1_Eot_M', 'Yr1_Eot_L', 'Yr1_Eot_S',
                     'Yr2_Eot_M', 'Yr2_Eot_L', 'Yr2_Eot_S',
                     'Yr3_Eot_M', 'Yr3_Eot_L', 'Yr3_Eot_S']
        elif 'Year4' in self.GroupName:
            return ['Yr1_Eot_M', 'Yr1_Eot_L', 'Yr1_Eot_S',
                    'Yr2_Eot_M', 'Yr2_Eot_L', 'Yr2_Eot_S',
                    'Yr3_Eot_M', 'Yr3_Eot_L', 'Yr3_Eot_S',
                    'Yr4_Eot_M', 'Yr4_Eot_L', 'Yr4_Eot_S']
        elif 'Year5' in self.GroupName:
            return ['Yr1_Eot_M', 'Yr1_Eot_L', 'Yr1_Eot_S',
                    'Yr2_Eot_M', 'Yr2_Eot_L', 'Yr2_Eot_S',
                    'Yr3_Eot_M', 'Yr3_Eot_L', 'Yr3_Eot_S',
                    'Yr4_Eot_M', 'Yr4_Eot_L', 'Yr4_Eot_S',
                    'Yr5_Eot_M', 'Yr5_Eot_L', 'Yr5_Eot_S']
        elif 'Year6' in self.GroupName:
            return ['Yr1_Eot_M', 'Yr1_Eot_L', 'Yr1_Eot_S',
                    'Yr2_Eot_M', 'Yr2_Eot_L', 'Yr2_Eot_S',
                    'Yr3_Eot_M', 'Yr3_Eot_L', 'Yr3_Eot_S',
                    'Yr4_Eot_M', 'Yr4_Eot_L', 'Yr4_Eot_S',
                    'Yr5_Eot_M', 'Yr5_Eot_L', 'Yr5_Eot_S',
                    'Yr6_Eot_M', 'Yr6_Eot_L', 'Yr6_Eot_S']
        elif 'Year7' in self.GroupName:
            return ['Yr2_Eot_M', 'Yr2_Eot_L', 'Yr2_Eot_S',
                    'Yr3_Eot_M', 'Yr3_Eot_L', 'Yr3_Eot_S',
                    'Yr4_Eot_M', 'Yr4_Eot_L', 'Yr4_Eot_S',
                    'Yr5_Eot_M', 'Yr5_Eot_L', 'Yr5_Eot_S',
                    'Yr6_Eot_M', 'Yr6_Eot_L', 'Yr6_Eot_S',
                    'Yr7_Eot_M', 'Yr7_Eot_L', 'Yr7_Eot_S']
        elif 'Year8' in self.GroupName:
            return ['Yr3_Eot_M', 'Yr3_Eot_L', 'Yr3_Eot_S',
                    'Yr4_Eot_M', 'Yr4_Eot_L', 'Yr4_Eot_S',
                    'Yr5_Eot_M', 'Yr5_Eot_L', 'Yr5_Eot_S',
                    'Yr6_Eot_M', 'Yr6_Eot_L', 'Yr6_Eot_S',
                    'Yr7_Eot_M', 'Yr7_Eot_L', 'Yr7_Eot_S',
                    'Yr8_Eot_M', 'Yr8_Eot_L', 'Yr8_Eot_S']
    def __LevelNames__(self):
        namelist = []
        for names in self.RecordList:
            year = names.split('_')[0]
            term = ''
            if 'M' in names:
                term = 'Mich'
            elif 'L' in names:
                term = 'Lent'
            else:
                term = 'Summer'
            namelist.append('%s %s' % (term,year))
        return namelist

    def __PupilInClass__(self,pupilrec):
        for classes in self.ClassList:
            if self.parentSubject:
                if PupilGroup.objects.filter(Pupil=pupilrec,
                                             Active=True,
                                             AcademicYear=self.AcYear,
                                             Group__Name__iendswith=classes).filter(Group__Name__icontains=self.parentSubject).exists():
                    return classes
            else:
                 if PupilGroup.objects.filter(Pupil=pupilrec,
                                             Active=True,
                                             AcademicYear=self.AcYear,
                                             Group__Name__iendswith=classes).filter(Group__Name__icontains=self.SubjectName).exists():
                    return classes
        for classes in self.ClassList:
            if PupilGroup.objects.filter(Pupil=pupilrec,
                                         Active=True,
                                         AcademicYear=self.AcYear,
                                         Group__Name__iendswith=classes).exists():
                return classes
        return ''

    def __BolCon__(self,Value):
        if Value:
            return 'T'
        else:
            return 'F'

    def JSONData(self):
        returnlist = []
        for pupils in GeneralFunctions.Get_PupilList(self.AcYear,
                                                     self.GroupName):
            rec = ExtendedRecordExtract.GradeAnalysisExtract(pupils.id,self.AcYear,self.SubjectName,self.RecordList)
            pupilrec = {'Fullname': rec.Pupil.FullName(),
                        'Class': self.__PupilInClass__(pupils),
                        'Gender': rec.Pupil.Gender,
                        'SEN': rec.IsSEN(),
                        'EAL': rec.IsEAL(),
                        'MostAble': rec.IsGiftedAndTalented(),
                        'MoreAble': self.__BolCon__(rec.IsMoreAble()),
                        'SumBirth': self.__BolCon__(rec.IsSumBirth()),
                        'Level': []}
            for extrecs in self.RecordList:
                if len(rec.GradeRecords[extrecs][self.SubjectName].Grade()['Level'][0]) > 0:
                    pupilrec['Level'].append({extrecs: '%s(%s)' % (rec.GradeRecords[extrecs][self.SubjectName].Grade()['Level'][0],
                                                                   rec.GradeRecords[extrecs][self.SubjectName].Grade()['Level'][1])})
                else:
                    pupilrec['Level'].append({extrecs:''})
            returnlist.append(pupilrec)
        returndata = simplejson.dumps(returnlist)
        returndata = [repr(returndata[i: i+250]) for i in range(0, len(returndata), 250)]
        return returndata


class APSPupilProgress(LevelsReview):
    ''' Creates the APSPupilProgress reports '''

    def __init__(self, request, AcYear, school,
                 SchoolName, GroupName, ReportDetails):
        self.request = request
        self.AcYear = AcYear
        self.school = school
        self.SchoolName = SchoolName
        self.GroupName = GroupName
        self.ReportDetails = ReportDetails
        self.SubjectName = ReportDetails['Subject']
        self.ClassList = self.__GetClasses__()
        self.RecordList = self.__GetRecordList__()
        self.LevelNames = self.__LevelNames__()
        self.today=datetime.datetime.today()

    def RenderReport(self):
        c={'ClassYears': GeneralFunctions.GetYearsAndForms(self.school),
           'AcYear': self.AcYear,
           'school': self.school,
           'SubjectName': self.SubjectName,
           'SchoolName': GeneralFunctions.GetSchoolName(self.school),
            'DataObj': self}
        DataFile = render_to_string('Reports/APSPupilsProgress.html',
                                    c,context_instance=RequestContext(self.request))
        return {'Name': 'APSPupilProgress_%s_%s.html' % (self.GroupName.split('.')[-1],self.SubjectName),
                'Data': DataFile, 'Type': 'text/html'}

    def JSONData(self):
        returnlist = []
        for pupils in GeneralFunctions.Get_PupilList(self.AcYear,
                                                     self.GroupName):
            rec = ExtendedRecordExtract.GradeAnalysisExtract(pupils.id,
                                                             self.AcYear,
                                                             self.SubjectName,self.RecordList)
            pupilrec = {'Fullname': rec.Pupil.FullName(),
                        'Class': self.__PupilInClass__(pupils),
                        'Gender': rec.Pupil.Gender,
                        'SEN': rec.IsSEN(),
                        'EAL': rec.IsEAL(),
                        'MostAble': rec.IsGiftedAndTalented(),
                        'MoreAble': self.__BolCon__(rec.IsMoreAble()),
                        'SumBirth': self.__BolCon__(rec.IsSumBirth())}
            if self.RecordList:
                if self.RecordList[0]:
                    pupilrec['PrevSummer']=rec.GradeRecords[self.RecordList[0]][self.SubjectName].Grade()['Level'][1]
                else:
                    pupilrec['PrevSummer']=''
                pupilrec['Mich']=rec.GradeRecords[self.RecordList[1]][self.SubjectName].Grade()['Level'][1]
                pupilrec['Lent']=rec.GradeRecords[self.RecordList[2]][self.SubjectName].Grade()['Level'][1]
                pupilrec['Summer']=rec.GradeRecords[self.RecordList[3]][self.SubjectName].Grade()['Level'][1]
            else:
                pupilrec['Mich']=''
                pupilrec['Lent']=''
                pupilrec['Summer']=''
            pupilrec=self.__setKSData(pupilrec,rec,self.__KSRecords())

            returnlist.append(pupilrec)
        returndata=simplejson.dumps(returnlist)
        returndata=[repr(returndata[i:i+250]) for i in range(0, len(returndata),250)]
        return returndata
    def __KSRecords(self):
        if 'Year1' in self.GroupName:
            return ['Yr1_Eot_M', 'Yr1_Eot_L', 'Yr1_Eot_S']
        elif 'Year2' in self.GroupName:
            return ['Yr1_Eot_M', 'Yr1_Eot_L', 'Yr1_Eot_S',
                    'Yr2_Eot_M', 'Yr2_Eot_L', 'Yr2_Eot_S']
        elif 'Year3' in self.GroupName:
            return ['Yr2_Eot_S','Yr3_Eot_M', 'Yr3_Eot_L', 'Yr3_Eot_S']
        elif 'Year4' in self.GroupName:
           return ['Yr2_Eot_S','Yr3_Eot_M','Yr3_Eot_L', 'Yr3_Eot_S',
                   'Yr4_Eot_M', 'Yr4_Eot_L', 'Yr4_Eot_S']
        elif 'Year5' in self.GroupName:
            return ['Yr2_Eot_S','Yr3_Eot_M', 'Yr3_Eot_L', 'Yr3_Eot_S',
                    'Yr4_Eot_M', 'Yr4_Eot_L', 'Yr4_Eot_S','Yr5_Eot_M', 'Yr5_Eot_L', 'Yr5_Eot_S']
        elif 'Year6' in self.GroupName:
            return ['Yr2_Eot_S','Yr3_Eot_M', 'Yr3_Eot_L', 'Yr3_Eot_S',
                    'Yr4_Eot_M', 'Yr4_Eot_L', 'Yr4_Eot_S','Yr5_Eot_M', 'Yr5_Eot_L', 'Yr5_Eot_S',
                    'Yr6_Eot_M', 'Yr6_Eot_L', 'Yr6_Eot_S']
        elif 'Year7' in self.GroupName:
            return ['Yr6_Eot_S','Yr7_Eot_M', 'Yr7_Eot_L', 'Yr7_Eot_S']
        elif 'Year8' in self.GroupName:
            return ['Yr6_Eot_S', 'Yr7_Eot_M', 'Yr7_Eot_L', 'Yr7_Eot_S','Yr8_Eot_M', 'Yr8_Eot_L', 'Yr8_Eot_S']
    def __setKSData(self,Record,PupilRecord,RecordList):

        datarecords=ExtentionData.objects.filter(BaseType='Pupil',
                                                 BaseId=PupilRecord.Pupil.id,
                                                 ExtentionRecord__Name=RecordList[0],
                                                 Subject__Name=self.SubjectName,
                                                  Active=True)
        if datarecords.exists():
            firstrecData=datarecords[0].Data['Level'][1]
            if len(firstrecData) > 0:
                Record['KSstart']=firstrecData
                Record['KSstartpoint']=True
                return Record

        for recs in RecordList[1:]:
            datarecords=ExtentionData.objects.filter(BaseType='Pupil',
                                                     BaseId=PupilRecord.Pupil.id,
                                                     ExtentionRecord__Name=recs,
                                                     Subject__Name=self.SubjectName,
                                                     Active=True)
            if datarecords.exists():
                Record['KSstartpoint']=False
                Record['KSstart']=''
                recData=datarecords[0].Data['Level'][1]
                if len(str(recData)) > 0:
                    Record['KSstart']=recData
                    return Record
        return Record

    def __GetRecordList__(self):
        if 'Year1' in self.GroupName:
            return ['','Yr1_Eot_M','Yr1_Eot_L','Yr1_Eot_S']
        elif 'Year2' in self.GroupName:
            return ['Yr1_Eot_S','Yr2_Eot_M','Yr2_Eot_L','Yr2_Eot_S']
        elif 'Year3' in self.GroupName:
                return ['Yr2_Eot_S','Yr3_Eot_M','Yr3_Eot_L','Yr3_Eot_S']
        elif 'Year4' in self.GroupName:
                return ['Yr3_Eot_S','Yr4_Eot_M','Yr4_Eot_L','Yr4_Eot_S']
        elif 'Year5' in self.GroupName:
                return ['Yr4_Eot_S','Yr5_Eot_M','Yr5_Eot_L','Yr5_Eot_S']
        elif 'Year6' in self.GroupName:
                return ['Yr5_Eot_S','Yr6_Eot_M','Yr6_Eot_L','Yr6_Eot_S']
        elif 'Year7' in self.GroupName:
                return ['Yr6_Eot_S','Yr7_Eot_M','Yr7_Eot_L','Yr7_Eot_S']
        elif 'Year8' in self.GroupName:
                return ['Yr7_Eot_S','Yr8_Eot_M','Yr8_Eot_L','Yr8_Eot_S']
        else:
            return False

class LevelsPercentage(LevelsReview):
    ''' Creates the LevelsPercentage reports '''
    def __init__(self,request,AcYear,school,SchoolName,GroupName,ReportDetails):
        log.warn('LevelsPercentage init started')
        self.request=request
        self.AcYear=AcYear
        self.school=school
        self.SChoolName=SchoolName
        self.GroupName=GroupName
        self.ReportDetails=ReportDetails
        self.SubjectName=ReportDetails['Subject']
        self.ClassList=self.__GetClasses__()
        self.RecordList=self.__GetRecordList__()
        self.today=datetime.datetime.today()
    def RenderReport(self):
        log.warn('LevelsPercentage started')
        c={'ClassYears':GeneralFunctions.GetYearsAndForms(self.school),'AcYear':self.AcYear,
           'school':self.school,'SubjectName':self.SubjectName,'SchoolName':GeneralFunctions.GetSchoolName(self.school),
        'DataObj':self}
        log.warn('Ready to render')
        log.warn(self.ClassList)
        log.warn(self.RecordList)
        DataFile=render_to_string('Reports/LevelsPercentage.html',c,context_instance=RequestContext(self.request))
        log.warn('Render Done')
        return {'Name': 'LevelsPercentage_%s_%s.html' % (self.GroupName.split('.')[-1],self.SubjectName),'Data':DataFile,'Type':'text/html'}
    def __GetRecordList__(self):
        YrStr=''
        log.warn('Getting Record List')
        if 'Year1' in self.GroupName:
            YrStr='Yr1'
        elif 'Year2' in self.GroupName:
            YrStr='Yr2'
        elif 'Year3' in self.GroupName:
            YrStr='Yr3'
        elif 'Year4' in self.GroupName:
            YrStr='Yr4'
        elif 'Year5' in self.GroupName:
            YrStr='Yr5'
        elif 'Year6' in self.GroupName:
            YrStr='Yr6'
        elif 'Year7' in self.GroupName:
            YrStr='Yr7'
        elif 'Year8' in self.GroupName:
            YrStr='Yr8'
        else:
            return False
        if self.ReportDetails['Term']=='Mich':
            return ['%s_Eot_M' % YrStr]
        elif self.ReportDetails['Term']=='Lent':
            return ['%s_Eot_L' % YrStr]
        else:
            return ['%s_Eot_S' % YrStr]
    def JSONData(self):
        returnlist=[]
        for pupils in GeneralFunctions.Get_PupilList(self.AcYear,self.GroupName):
            log.warn(pupils)
            rec=ExtendedRecordExtract.GradeAnalysisExtract(pupils.id,
                                                           self.AcYear,
                                                           self.SubjectName,self.RecordList)
            pupilrec= {'Fullname':rec.Pupil.FullName(),
            'Class':self.__PupilInClass__(pupils),
            'Gender':rec.Pupil.Gender,
            'SEN':rec.IsSEN(),
            'EAL':rec.IsEAL(),
            'MostAble':rec.IsGiftedAndTalented(),
            'MoreAble':self.__BolCon__(rec.IsMoreAble()),
            'SumBirth':self.__BolCon__(rec.IsSumBirth()),
            'Level':[]}
            log.warn('Pupil Base Rec created')
            for extrecs in self.RecordList:
                log.warn(extrecs)
                pupilrec['Level'].append({'Level':rec.GradeRecords[self.RecordList[0]][self.SubjectName].Grade()['Level'][0]})
            returnlist.append(pupilrec)
        returndata=simplejson.dumps(returnlist)
        returndata=[repr(returndata[i:i+250]) for i in range(0, len(returndata),250)]
        return returndata

class PercentageAbove(LevelsPercentage):
    def RenderReport(self):
        log.warn('PercentageAbove started')
        c={'ClassYears':GeneralFunctions.GetYearsAndForms(self.school),'AcYear':self.AcYear,
           'school':self.school,'SubjectName':self.SubjectName,'SchoolName':GeneralFunctions.GetSchoolName(self.school),
        'DataObj':self}
        log.warn('Ready to render')
        log.warn(self.ClassList)
        log.warn(self.RecordList)
        DataFile=render_to_string('Reports/PercentageAbove.html',c,context_instance=RequestContext(self.request))
        log.warn('Render Done')
        return {'Name': 'PercentageAbove_%s_%s.html' % (self.GroupName.split('.')[-1],self.SubjectName),'Data':DataFile,'Type':'text/html'}

class Mapping(LevelsReview):
    ''' Creates the LevelsPercentage reports '''
    def __init__(self,request,AcYear,school,SchoolName,GroupName,ReportDetails):
        log.warn('Mapping init started')
        self.request=request
        self.AcYear=AcYear
        self.school=school
        self.SChoolName=SchoolName
        self.GroupName=GroupName
        self.ReportDetails=ReportDetails
        self.SubjectName=ReportDetails['Subject']
        self.ClassList=self.__GetClasses__()
        self.RecordList=self.__GetRecordList__()
        self.today=datetime.datetime.today()
    def RenderReport(self):
        c={'ClassYears':GeneralFunctions.GetYearsAndForms(self.school),'AcYear':self.AcYear,
           'school':self.school,'SubjectName':self.SubjectName,'SchoolName':GeneralFunctions.GetSchoolName(self.school),
        'DataObj':self}
        DataFile=render_to_string('Reports/Mapping.html',c,context_instance=RequestContext(self.request))
        return {'Name': 'Mapping_%s_%s.html' % (self.GroupName.split('.')[-1],self.SubjectName),'Data':DataFile,'Type':'text/html'}

    def __GetRecordList__(self):
        if 'Year1' in self.GroupName:
            return False

        elif 'Year2' in self.GroupName:
            if 'Mich' in self.ReportDetails['Term']:
                return ['Yr1_Eot_M','Yr2_Eot_M']
            elif 'Lent' in self.ReportDetails['Term']:
                return ['Yr1_Eot_L','Yr2_Eot_L']
            else:
                return ['Yr1_Eot_S','Yr2_Eot_S']

        elif 'Year3' in self.GroupName:
            if 'Mich' in self.ReportDetails['Term']:
                return ['Yr2_Eot_M','Yr3_Eot_M']
            elif 'Lent' in self.ReportDetails['Term']:
                return ['Yr2_Eot_L','Yr3_Eot_L']
            else:
                return ['Yr2_Eot_S','Yr3_Eot_S']

        elif 'Year4' in self.GroupName:
            if 'Mich' in self.ReportDetails['Term']:
                return ['Yr3_Eot_M','Yr4_Eot_M']
            elif 'Lent' in self.ReportDetails['Term']:
                return ['Yr3_Eot_L','Yr4_Eot_L']
            else:
                return ['Yr3_Eot_S','Yr4_Eot_S']

        elif 'Year5' in self.GroupName:
            if 'Mich' in self.ReportDetails['Term']:
                return ['Yr4_Eot_M','Yr5_Eot_M']
            elif 'Lent' in self.ReportDetails['Term']:
                return ['Yr4_Eot_L','Yr5_Eot_L']
            else:
                return ['Yr4_Eot_S','Yr5_Eot_S']

        elif 'Year6' in self.GroupName:
            if 'Mich' in self.ReportDetails['Term']:
                return ['Yr5_Eot_M','Yr6_Eot_M']
            elif 'Lent' in self.ReportDetails['Term']:
                return ['Yr5_Eot_L','Yr6_Eot_L']
            else:
                return ['Yr5_Eot_S','Yr6_Eot_S']

        elif 'Year7' in self.GroupName:
            if 'Mich' in self.ReportDetails['Term']:
                return ['Yr6_Eot_M','Yr7_Eot_M']
            elif 'Lent' in self.ReportDetails['Term']:
                return ['Yr6_Eot_L','Yr7_Eot_L']
            else:
                return ['Yr6_Eot_S','Yr7_Eot_S']

        elif 'Year8' in self.GroupName:
            if 'Mich' in self.ReportDetails['Term']:
                return ['Yr7_Eot_M','Yr8_Eot_M']
            elif 'Lent' in self.ReportDetails['Term']:
                return ['Yr7_Eot_L','Yr8_Eot_L']
            else:
                return ['Yr7_Eot_S','Yr8_Eot_S']

        else:
            return False
    def JSONData(self):
        returnlist=[]
        for pupils in GeneralFunctions.Get_PupilList(self.AcYear,self.GroupName):
            log.warn(pupils)
            rec=ExtendedRecordExtract.GradeAnalysisExtract(pupils.id,
                                                           self.AcYear,
                                                           self.SubjectName,self.RecordList)
            pupilrec= {'Fullname':rec.Pupil.FullName(),
            'Class':self.__PupilInClass__(pupils),
            'Gender':rec.Pupil.Gender,
            'SEN':rec.IsSEN(),
            'EAL':rec.IsEAL(),
            'MostAble':rec.IsGiftedAndTalented(),
            'MoreAble':self.__BolCon__(rec.IsMoreAble()),
            'SumBirth':self.__BolCon__(rec.IsSumBirth()),
            'Level':[]}
            log.warn('Pupil Base Rec created')
            pupilrec['Level'].append({'Level1':rec.GradeRecords[self.RecordList[0]][self.SubjectName].Grade()['Level'][0],
                                     'Level2':rec.GradeRecords[self.RecordList[1]][self.SubjectName].Grade()['Level'][0]})
            returnlist.append(pupilrec)
        returndata=simplejson.dumps(returnlist)
        returndata=[repr(returndata[i:i+250]) for i in range(0, len(returndata),250)]
        return returndata

class APSYearReview(APSPupilProgress):
    def JSONData(self):
        returnlist=[]
        for pupils in GeneralFunctions.Get_PupilList(self.AcYear,self.GroupName):
            log.warn(pupils)
            rec=ExtendedRecordExtract.GradeAnalysisExtract(pupils.id,
                                                           self.AcYear,
                                                           self.SubjectName,self.RecordList)
            pupilrec= {'Fullname':rec.Pupil.FullName(),
            'Class':self.__PupilInClass__(pupils),
            'Gender':rec.Pupil.Gender,
            'SEN':rec.IsSEN(),
            'EAL':rec.IsEAL(),
            'MostAble':rec.IsGiftedAndTalented(),
            'MoreAble':self.__BolCon__(rec.IsMoreAble()),
            'SumBirth':self.__BolCon__(rec.IsSumBirth())}
            if self.RecordList:
                if self.RecordList[0]:
                    pupilrec['PrevSummer']=rec.GradeRecords[self.RecordList[0]][self.SubjectName].Grade()['Level'][1]
                else:
                    pupilrec['PrevSummer']=''
                pupilrec['Mich']=rec.GradeRecords[self.RecordList[1]][self.SubjectName].Grade()['Level'][1]
                pupilrec['Lent']=rec.GradeRecords[self.RecordList[2]][self.SubjectName].Grade()['Level'][1]
                pupilrec['Summer']=rec.GradeRecords[self.RecordList[3]][self.SubjectName].Grade()['Level'][1]
            else:
                pupilrec['Mich']=''
                pupilrec['Lent']=''
                pupilrec['Summer']=''
            returnlist.append(pupilrec)
        return returnlist

class APSSchoolReview(LevelsReview):
    ''' Creates the Levels review reports '''

    def __init__(self, request, AcYear, school,
                 SchoolName, GroupName,ReportDetails):
        log.warn('Started APSSchoolReview')
        self.request = request
        self.AcYear = AcYear
        self.school = school
        self.SchoolName = SchoolName
        self.ReportDetails = ReportDetails
        self.SubjectName = ReportDetails['Subject']
        self.today=datetime.datetime.today()
    def RenderReport(self):
        c = {'AcYear': self.AcYear, 'school': self.school,
             'SubjectName': self.SubjectName,
             'SchoolName': GeneralFunctions.GetSchoolName(self.school),
             'DataObj': self}
        log.warn('Render SChool Review')
        DataFile = render_to_string('Reports/APSSchoolReviewNew.html',
                                    c,
                                    context_instance=RequestContext(self.request))
        return {'Name': 'APSSchoolReview_%s.html' % (self.SubjectName),
                'Data': DataFile, 'Type': 'text/html'}
    def __GetRecordList__(self,YearGroup):
        if 'Year1' in YearGroup:
            year='Yr1'
            oldyear='YrR'
        elif 'Year2' in YearGroup:
            year='Yr2'
            oldyear='Yr1'
        elif 'Year3' in YearGroup:
            year='Yr3'
            oldyear='Yr2'
        elif 'Year4' in YearGroup:
            year='Yr4'
            oldyear='Yr3'
        elif 'Year5' in YearGroup:
            year='Yr5'
            oldyear='Yr4'
        elif 'Year6' in YearGroup:
            year='Yr6'
            oldyear='Yr5'
        elif 'Year7' in YearGroup:
            year='Yr7'
            oldyear='Yr6'
        elif 'Year8' in YearGroup:
            year='Yr8'
            oldyear='Yr7'
        return ['%s_Eot_S' % oldyear,'%s_Eot_M' % year,'%s_Eot_L' % year,'%s_Eot_S' % year]
    def __GetYearGroupRecords(self,YearGroup):
        ''' this should return a set of records in form
        {'Name':,'Gender','SEN':,'EAL':,'G&T':,'MoreAble':,'SumBirth':,Grades:{Sum0:Grade,Mich:Grade,Lent:Grade,Sum:Grade}}
        '''
        RecordList=self.__GetRecordList__(YearGroup)
        PupilIdList=set([x.Pupil.id for x in PupilGroup.objects.filter(Group__Name__istartswith=YearGroup,
                                                                   AcademicYear=self.AcYear,
                                                                   Active=True)])
        GradeData=ExtentionData.objects.filter(BaseType='Pupil',BaseId__in=PupilIdList,Subject__Name=self.SubjectName,
                                               Active=True,
                                               ExtentionRecord__Name__in=RecordList).values('BaseId','ExtentionRecord__Name','Data')

        for records in GradeData:
            records['Data']=cPickle.loads(base64.b64decode(records['Data']))
        PupilRecordList={}
        for pupilId in PupilIdList:
            PupilRec=ExtendedRecordExtract.PupilRecAnalysis(pupilId,self.AcYear)
            PupilRecordList[pupilId]={'Gender':PupilRec.Pupil.Gender,
                                      'PupilId':pupilId,
                                      'SEN':PupilRec.IsSEN(),
                                      'EAL':PupilRec.IsEAL(),
                                      'MostAble':PupilRec.IsGiftedAndTalented(self.SubjectName),
                                      'MoreAble':PupilRec.IsMoreAble(),
                                      'SumBirth':PupilRec.IsSumBirth(),
                                      'Grades':{'Prev':'','Mich':'','Lent':'','Summer':''}}
        for records in GradeData:
            if records['ExtentionRecord__Name']==RecordList[0]:
                if 'Level' in records['Data']:
                    PupilRecordList[records['BaseId']]['Grades']['Prev']=records['Data']['Level'][1]
                else:
                    PupilRecordList[records['BaseId']]['Grades']['Prev']='0'
            elif records['ExtentionRecord__Name']==RecordList[1]:
                if 'Level' in records['Data']:
                    PupilRecordList[records['BaseId']]['Grades']['Mich']=records['Data']['Level'][1]
                else:
                    PupilRecordList[records['BaseId']]['Grades']['Mich']='0'
            elif records['ExtentionRecord__Name']==RecordList[2]:
                if 'Level' in records['Data']:
                    PupilRecordList[records['BaseId']]['Grades']['Lent']=records['Data']['Level'][1]
                else:
                    PupilRecordList[records['BaseId']]['Grades']['Lent']='0'
            elif records['ExtentionRecord__Name']==RecordList[3]:
                if 'Level' in records['Data']:
                    PupilRecordList[records['BaseId']]['Grades']['Summer']=records['Data']['Level'][1]
                else:
                    PupilRecordList[records['BaseId']]['Grades']['Summer']='0'
        return [data for keys,data in PupilRecordList.items()]


    def __APSAverages(self,GradeData,PrevTerm,Term):
        returnrecord={}
        returnrecord['All']=self.__APSSubGroupAverage(GradeData,PrevTerm,Term)
        returnrecord['Female']=self.__APSSubGroupAverage(self.__FilterGender('F',GradeData),PrevTerm,Term)
        returnrecord['Male']=self.__APSSubGroupAverage(self.__FilterGender('M',GradeData),PrevTerm,Term)
        returnrecord['SEND 1']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('SEN', 'SEND 1',GradeData),PrevTerm,Term)
        returnrecord['SEND 2']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('SEN', 'SEND 2',GradeData),PrevTerm,Term)
        returnrecord['SEND 3']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('SEN', 'SEND 3',GradeData),PrevTerm,Term)
        returnrecord['SEND 4']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('SEN', 'SEND 4',GradeData),PrevTerm,Term)
        returnrecord['SEND 5']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('SEN', 'SEND 5',GradeData),PrevTerm,Term)
        returnrecord['SEND All']=self.__APSSubGroupAverage(self.__FilterSubGroupAll('SEN', 'SEND',GradeData),PrevTerm,Term)
        returnrecord['EAL 1']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('EAL','EAL 1',GradeData),PrevTerm,Term)
        returnrecord['EAL 2']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('EAL','EAL 2',GradeData),PrevTerm,Term)
        returnrecord['EAL 3']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('EAL','EAL 3',GradeData),PrevTerm,Term)
        returnrecord['EAL 4']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('EAL','EAL 4',GradeData),PrevTerm,Term)
        returnrecord['EAL EMT']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('EAL','EAL EMT',GradeData),PrevTerm,Term)
        returnrecord['EAL All']=self.__APSSubGroupAverage(self.__FilterSubGroupAll('EAL','EAL',GradeData),PrevTerm,Term)
        returnrecord['MostAble 1']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('MostAble','MostAble 1',GradeData),PrevTerm,Term)
        returnrecord['MostAble 2']=self.__APSSubGroupAverage(self.__FilterSubGroupComplex('MostAble','MostAble 2',GradeData),PrevTerm,Term)
        returnrecord['MostAble All']=self.__APSSubGroupAverage(self.__FilterSubGroupAll('MostAble','MostAble',GradeData),PrevTerm,Term)
        returnrecord['MoreAble']=self.__APSSubGroupAverage(self.__FilterSubGroup('MoreAble',GradeData),PrevTerm,Term)
        returnrecord['SumBirth']=self.__APSSubGroupAverage(self.__FilterSubGroup('SumBirth',GradeData),PrevTerm,Term)

        return returnrecord

    def __APSSubGroupAverage(self,GradeData,PrevTerm,Term):
        if len(GradeData):
            if PrevTerm in GradeData[0]['Grades']:
                PrevTermData=[int(x['Grades'][PrevTerm]) for x in GradeData if len(str(x['Grades'][PrevTerm])) > 0]
                PrevTermSum=float(sum(PrevTermData))
                PrevTermLen=len(PrevTermData)
                if PrevTermSum and PrevTermLen:
                    PrevTermAv=PrevTermSum/PrevTermLen
                else:
                    PrevTermAv=0.0
            else:
                PrevTermAv=0.0
            if Term in GradeData[0]['Grades']:
                TermData=[int(x['Grades'][Term]) for x in GradeData if len(str(x['Grades'][Term])) > 0]
                TermSum=float(sum(TermData))
                TermLen=len(TermData)
                if TermSum and TermLen:
                    TermAv=TermSum/TermLen
                else:
                    TermAv=0.0
            else:
                TermAv=0.0
        else:
            TermAv=0.0
            PrevTermAv=0.0
        return '%3.2f (%3.2f)' % (TermAv-PrevTermAv,TermAv)

    def __FilterGender(self,Gender,GradeData):
        return [x for x in GradeData if x['Gender']==Gender]

    def __FilterSubGroup(self,SubGroup,GradeData):
        return [ x for x in GradeData if x[SubGroup]==True]
    def __FilterSubGroupComplex(self,SubGroup,Field,GradeData):
        return [ x for x in GradeData if x[SubGroup]==Field]
    def __FilterSubGroupAll(self,SubGroup,Field,GradeData):
        return [ x for x in GradeData if Field in x[SubGroup]]
    def __GetYearGroupList__(self):
        return [x.Name for x in Group.objects.filter(Name__iregex=r'^Current.%s.*Year[1-8]$' % self.SchoolName)]

    def JSONData(self):
        returndata=[]
        for YearGroup in self.__GetYearGroupList__():
            GradeData=self.__GetYearGroupRecords(YearGroup)
            print GradeData
            DataRecord={'Name':YearGroup.split('.')[-1]}
            DataRecord['Mich']={}
            DataRecord['Lent']={}
            DataRecord['Summer']={}
            DataRecord['Mich']['Year']=self.__APSAverages(GradeData,'Prev','Mich')
            DataRecord['Lent']['Year']=self.__APSAverages(GradeData,'Mich','Lent')
            DataRecord['Summer']['Year']=self.__APSAverages(GradeData,'Lent','Summer')
            DataRecord['Mich']['Classes']=[]
            DataRecord['Lent']['Classes']=[]
            DataRecord['Summer']['Classes']=[]
            self.GroupName=YearGroup
            ClassData=[]
            for classes in self.__GetClasses__():
                NewGradeData=[x for x in GradeData if PupilGroup.objects.filter(Pupil__id=x['PupilId'],AcademicYear=self.AcYear,Group=classes).exists()]
                DataRecord['Mich']['Classes'].append({'Name':classes.MenuName,'Data':self.__APSAverages(NewGradeData,'Prev','Mich')})
                DataRecord['Lent']['Classes'].append({'Name':classes.MenuName,'Data':self.__APSAverages(NewGradeData,'Mich','Lent')})
                DataRecord['Summer']['Classes'].append({'Name':classes.MenuName,'Data':self.__APSAverages(NewGradeData,'Lent','Summer')})


            returndata.append(DataRecord)
        returndata = simplejson.dumps(returndata)
        returndata = [repr(returndata[i: i+250]) for i in range(0, len(returndata), 250)]
        return returndata
    def __GetClasses__(self):
        if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % self.SubjectName).exists():
            classlist = Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % self.SubjectName)

        elif ParentSubjects.objects.filter(Subject__Name=self.SubjectName).exists():
            parent=ParentSubjects.objects.filter(Subject__Name=self.SubjectName)[0].ParentSubject.Name
            if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % parent).exists():
                classlist=Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="%s." % parent)
            else:
                if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames').exists():
                    classlist=Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames')
        else:
            if Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames').exists():
                classlist=Group.objects.filter(Name__icontains=self.GroupName).filter(Name__icontains="Form.").exclude(Name__icontains='BalGames')
        return classlist

class CohortAnalysis(LevelsReview):
    ''' Creates the Levels review reports '''

    def __init__(self, request, AcYear, school,
                 SchoolName, GroupName,ReportDetails):
        log.warn('Started CohortAnalysis')
        self.request = request
        self.AcYear = AcYear
        self.school = school
        self.SchoolName = SchoolName
        self.ReportDetails = ReportDetails
        self.SubjectName = ReportDetails['Subject']
        self.AnalysisGrade=ReportDetails['AnalysisGrade']
        self.__setGroupName()
        self.__getGradeData()
        self.today=datetime.datetime.today()
    def RenderReport(self):
        c = {'AcYear': self.AcYear, 'school': self.school,
             'SubjectName': self.SubjectName,
             'SchoolName': GeneralFunctions.GetSchoolName(self.school),
             'DataObj': self}
        print 'Rendering Cohort Report'
        print 'Rendering Cohort'
        DataFile = render_to_string('Reports/CohortAnalysis.html',
                                    c,
                                    context_instance=RequestContext(self.request))
        return {'Name': 'CohortAnalysis_%s.html' % (self.SubjectName),
                'Data': DataFile, 'Type': 'text/html'}
    def __YearList(self,Counter,Year):
        if Counter==0:
            return []
        else:
            Year1,Year2= Year.split('-')
            Year='%d-%d' % (int(Year1)-1, int(Year2)-1)
            Counter-=1
            return [Year] + self.__YearList(Counter,Year)
    def __setGroupName(self):
        if 'Yr2' in self.AnalysisGrade:
            self.GroupName='Current.%s.LowerSchool.Year2' % self.SchoolName
        elif 'Yr4' in self.AnalysisGrade:
            if 'Fulham' in self.SchoolName or 'Kensington' in self.SchoolName:
                self.GroupName='Current.%s.PrepSchool.Year4' % self.SchoolName
            else:
                self.GroupName='Current.%s.MiddleSchool.Year4' % self.SchoolName
        elif 'Yr6' in self.AnalysisGrade:
            if 'Fulham' in self.SchoolName or 'Kensington' in self.SchoolName:
                  self.GroupName='Current.%s.PrepSchool.Year6' % self.SchoolName
            else:
                self.GroupName='Current.%s.UpperSchool.Year6' % self.SchoolName
        elif 'Yr8' in self.AnalysisGrade:
            self.GroupName='Current.%s.UpperSchool.Year8' % self.SchoolName
        return self.GroupName

    def __getGradeData(self):
        self.GradeData=[]
        for years in self.__YearList(5,self.AcYear):
            self.GradeData.append({'Year':years,'Data':self.__gradeExtract(years)})
        return self.GradeData

    def __gradeExtract(self,Year):
        PupilList=set([x.Pupil for x in PupilGroup.objects.filter(Group__Name__istartswith=self.GroupName,
                                                                   AcademicYear=Year,
                                                                   Active=True)])
        GradeData=ExtentionData.objects.filter(BaseType='Pupil',BaseId__in=[x.id for x in PupilList],
                                               Subject__Name=self.SubjectName,
                                               Active=True,
                                               ExtentionRecord__Name=self.AnalysisGrade).values('BaseId','ExtentionRecord__Name','Data')

        for records in GradeData:
            records['Data']=cPickle.loads(base64.b64decode(records['Data']))
            records['Gender']=[x.Gender for x in PupilList if x.id==records['BaseId']][0]
        return GradeData

    def __buildTable1(self):
        if 'Yr2' in self.AnalysisGrade:
            LevelRange = ['W','1c','1b','1a','2c','2b','2a','3c','3b','3a','4c','4b']
        elif 'Yr4' in self.AnalysisGrade:
            LevelRange = ['2c','2b','2a','3c','3b','3a','4c','4b','4a','5c','5b','5a']
        elif 'Yr6' in self.AnalysisGrade:
            LevelRange = ['3c','3b','3a','4c','4b','4a','5c','5b','5a','6c','6b','6a']
        else:
            LevelRange = ['4b','4a','5c','5b','5a','6c','6b','6a','7c','7b','7a','8c','8b','8a']
        Table1=[]
        for levels in LevelRange:
            datarecord={'Level':levels,'Data':[]}
            for datareport in self.GradeData:
                PupilCount=len(datareport['Data'])
                levelcounter=len([x for x in datareport['Data'] if x['Data']['Level'][0]==levels])
                if levelcounter and PupilCount:
                    datarecord['Data'].append([datareport['Year'],((float(levelcounter)/float(PupilCount))*100)])
                else:
                    datarecord['Data'].append([datareport['Year'],0.0])
            Table1.append(datarecord)
        return Table1

    def __buildTable1Gender(self,Gender):
        if 'Yr2' in self.AnalysisGrade:
            LevelRange = ['W','1c','1b','1a','2c','2b','2a','3c','3b','3a','4c','4b']
        elif 'Yr4' in self.AnalysisGrade:
            LevelRange = ['2c','2b','2a','3c','3b','3a','4c','4b','4a','5c','5b','5a']
        elif 'Yr6' in self.AnalysisGrade:
            LevelRange = ['3c','3b','3a','4c','4b','4a','5c','5b','5a','6c','6b','6a']
        else:
            LevelRange = ['4b','4a','5c','5b','5a','6c','6b','6a','7c','7b','7a','8c','8b','8a']
        Table1=[]
        for levels in LevelRange:
            datarecord={'Level':levels,'Data':[]}
            for datareport in self.GradeData:
                PupilCount=len([x for x in datareport['Data'] if x['Gender']==Gender])
                levelcounter=len([x for x in datareport['Data'] if x['Data']['Level'][0]==levels and x['Gender']==Gender])
                if levelcounter and PupilCount:
                    datarecord['Data'].append([datareport['Year'],((float(levelcounter)/float(PupilCount))*100)])
                else:
                    datarecord['Data'].append([datareport['Year'],0.0])
            Table1.append(datarecord)
        return Table1

    def __buildTable2(self):
        if 'Yr2' in self.AnalysisGrade:
            LevelRange=['2b','2a','3c']
        elif 'Yr4' in self.AnalysisGrade:
            LevelRange=['3b','3a','4c']
        elif 'Yr6' in self.AnalysisGrade:
            LevelRange=['4b','4a','5c']
        elif 'Yr8' in self.AnalysisGrade:
            LevelRange=['5b','5a','6c']
        AllData=self.__buildTable1()
        BoysData=self.__buildTable1Gender('M')
        GirlsData=self.__buildTable1Gender('F')
        Table2=[]
        for years in self.__YearList(6,self.AcYear):
            Table2Row={'Year':years,"Data":[]}
            for levels in LevelRange:
                Table2Row['Data'].append([levels,{'All':self.__percentageAbove(levels,years,AllData),
                                                  'M':self.__percentageAbove(levels,years,GirlsData),
                                                  'F':self.__percentageAbove(levels,years,BoysData)}])
            Table2.append(Table2Row)
        return Table2

    def __percentageAbove(self,level,dataYear,dataObj):
        found = False
        total = 0
        for records in dataObj:
            if level in records['Level']:
                found=True
            if found:
                total+=sum([x[1] for x in records['Data'] if x[0]==dataYear])
        return total

    def JSONData(self):
        returndata = simplejson.dumps({'Table1':self.__buildTable1(),'Table2':self.__buildTable2()})
        returndata = [repr(returndata[i: i+250]) for i in range(0, len(returndata), 250)]
        return returndata

class APSks2Review(LevelsReview):
    ''' Creates the Levels review reports '''

    def __init__(self, request, AcYear, school,
                 SchoolName, GroupName, ReportDetails):
        self.request = request
        self.AcYear = AcYear
        self.school = school
        self.SChoolName = SchoolName
        self.GroupName = GroupName
        self.ReportDetails = ReportDetails
        self.SubjectName = ReportDetails['Subject']
        self.ClassList = self.__GetClasses__()
        self.RecordList = self.__GetRecordList__()
        self.LevelNames = self.__LevelNames__()
        self.today=datetime.datetime.today()

    def RenderReport(self):
        c = {'AcYear': self.AcYear,'school': self.school,
             'SubjectName': self.SubjectName,
             'SchoolName': GeneralFunctions.GetSchoolName(self.school),
             'DataObj': self}
        DataFile = render_to_string('Reports/APSks2Review.html',
                                    c,
                                    context_instance=RequestContext(self.request))
        return {'Name': 'APSks2Review_%s_%s.html' % (self.GroupName.split('.')[-1],
                                                     self.SubjectName),
                'Data': DataFile, 'Type': 'text/html'}
    def __GetRecordList__(self):
        return ['Yr2_Eot_S','Yr6_Eot_S']
    def JSONData(self):
        returnlist = []
        for pupils in GeneralFunctions.Get_PupilList(self.AcYear,
                                                     self.GroupName):
            rec = ExtendedRecordExtract.GradeAnalysisExtract(pupils.id,self.AcYear,self.SubjectName,self.RecordList)
            pupilrec = {'Fullname': rec.Pupil.FullName(),
                        'Class': self.__PupilInClass__(pupils),
                        'Gender': rec.Pupil.Gender,
                        'SEN': rec.IsSEN(),
                        'EAL': rec.IsEAL(),
                        'MostAble': rec.IsGiftedAndTalented(),
                        'MoreAble': self.__BolCon__(rec.IsMoreAble()),
                        'SumBirth': self.__BolCon__(rec.IsSumBirth()),
                        'Level': []}
            pupilrec['Level'].append({'APS1': rec.GradeRecords['Yr1_Eot_S'][self.SubjectName].Grade()['Level'][1]})
            pupilrec['Level'].append({'APS2': rec.GradeRecords['Yr6_Eot_S'][self.SubjectName].Grade()['Level'][1]})
            returnlist.append(pupilrec)
        returndata = simplejson.dumps(returnlist)
        returndata = [repr(returndata[i: i+250]) for i in range(0, len(returndata), 250)]
        return returndata


class CoreAboveLevel(LevelsReview):
    ''' Creates the Levels review reports '''
    def __GetRecordList__(self):
        self.TestLevel=self.ReportDetails['TestLevel']
        return [self.ReportDetails['AnalysisYear']]
    def JSONData(self):
        returnlist = []
        for pupils in GeneralFunctions.Get_PupilList(self.AcYear,
                                                     self.GroupName):
            rec = ExtendedRecordExtract.GradeAnalysisExtract(pupils.id,self.AcYear,self.SubjectName)
            pupilrec = {'Fullname': rec.Pupil.FullName(),
                        'Class': self.__PupilInClass__(pupils),
                        'Gender': rec.Pupil.Gender,
                        'SEN': rec.IsSEN(),
                        'EAL': rec.IsEAL(),
                        'MostAble': rec.IsGiftedAndTalented(),
                        'MoreAble': self.__BolCon__(rec.IsMoreAble()),
                        'SumBirth': self.__BolCon__(rec.IsSumBirth()),
                        'Level': []}
            pupilrec['Level'].append({'English_Reading': rec.GradeRecords[self.RecordList[0]]['English_Reading'].Grade()['Level'][0]})
            pupilrec['Level'].append({'English_Writing': rec.GradeRecords[self.RecordList[0]]['English_Writing'].Grade()['Level'][0]})
            pupilrec['Level'].append({'Mathematics': rec.GradeRecords[self.RecordList[0]]['Mathematics'].Grade()['Level'][0]})
            returnlist.append(pupilrec)
        returndata = simplejson.dumps(returnlist)
        returndata = [repr(returndata[i: i+250]) for i in range(0, len(returndata), 250)]
        return returndata

class WholeSchoolNumbersReport():
    """
    Produces a numbers report for a school
    """
    def __init__(self, school, ac_year):
        self.school = MenuTypes.objects.get(Name=school).SchoolId.Name
        self.ac_year = ac_year
        self.groups = self.__get_all_groups__()

    def __get_pupil_objects__(self, group):
        pupil_ids = list(set(PupilGroup.objects.filter(Group__Name=group,
                                                       Active=True,
                                                       AcademicYear=self.ac_year).values_list("Pupil__id",
                                                                                              flat=True)))
        pupil_objects = []
        for pupil_id in pupil_ids:
            pupil_objects.append(PupilRecord(pupil_id, self.ac_year))
        return pupil_objects

    def __get_all_groups__(self):
        """
        Returns all needed groups for reports
        """
        all_group_names = Group.objects.filter(Name__icontains=".%s." % self.school).filter(Name__icontains="Form").exclude(Name="Applicants").values_list("Name", flat=True)
        needed_group_names = []
        for group in all_group_names:
            if len(group.split(".")) == 6:
                if PupilGroup.objects.filter(Group__Name=group, AcademicYear=self.ac_year, Active=True).exists():
                    needed_group_names.append(group)
        return needed_group_names

    def __get_all_teacher_names__(self):
        pass

    def get_data_for_template(self):
        """
        Main logic for getting data in format that is template language friendly

        data returned per group:

        ["group",
        "Girls",
        "Boys",
        "Becket",
        "Hardy",
        "Lawrence",
        "More",
        "SEN",
        "EAL",
        "GT",
        "All",
        "Teachers"]
        """
        return_data = []

        # whole school variables
        whole_school_girls = 0
        whole_school_boys = 0
        whole_school_becket = 0
        whole_school_hardy = 0
        whole_school_lawrence = 0
        whole_school_more = 0
        whole_school_sen = 0
        whole_school_eal = 0
        whole_school_gifted_and_talented = 0

        # school section variables
        school_section_girls = 0
        school_section_boys = 0
        school_section_becket = 0
        school_section_hardy = 0
        school_section_lawrence = 0
        school_section_more = 0
        school_section_sen = 0
        school_section_eal = 0
        school_section_gifted_and_talented = 0

        # yearly variables
        yearly_girls = 0
        yearly_boys = 0
        yearly_becket = 0
        yearly_hardy = 0
        yearly_lawrence = 0
        yearly_more = 0
        yearly_sen = 0
        yearly_eal = 0
        yearly_gifted_and_talented = 0

        for group in self.groups:

            # class variables
            girls = 0
            boys = 0
            becket = 0
            hardy = 0
            lawrence = 0
            more = 0
            sen = 0
            eal = 0
            gifted_and_talented = 0
            try:
                teacher_name = SystemVarData.objects.get(Variable__Name='%s.FormTeachersAll' % group,
                                                         AcademicYear=self.ac_year).Data
            except Exception as error:
                teacher_name = "error" % error
            for pupil_object in self.__get_pupil_objects__(group):
                # get girls data
                if pupil_object.Pupil.Gender == "F":
                    girls += 1
                    school_section_girls += 1
                    yearly_girls += 1
                    whole_school_girls += 1

                # get boys data
                elif pupil_object.Pupil.Gender == "M":
                    boys += 1
                    school_section_boys += 1
                    yearly_boys += 1
                    whole_school_boys += 1

                # get house data
                if pupil_object.AcademicHouse() == "Becket":
                    becket += 1
                    school_section_becket += 1
                    yearly_becket += 1
                    whole_school_becket += 1
                if pupil_object.AcademicHouse() == "Hardy":
                    hardy += 1
                    school_section_hardy += 1
                    yearly_hardy += 1
                    whole_school_hardy += 1
                if pupil_object.AcademicHouse() == "Lawrence":
                    lawrence += 1
                    school_section_lawrence += 1
                    yearly_lawrence += 1
                    whole_school_lawrence += 1
                if pupil_object.AcademicHouse() == "More":
                    more += 1
                    school_section_more += 1
                    yearly_more += 1
                    whole_school_more += 1

                # get alerts data
                for i in pupil_object.AllPupilAlerts():
                    for a in i:
                        if a.Active:
                            if "SEND" in a.AlertType.AlertGroup.Name:
                                sen += 1
                                school_section_sen += 1
                                yearly_sen += 1
                                whole_school_sen += 1
                            if "EAL" in a.AlertType.AlertGroup.Name:
                                eal += 1
                                school_section_eal += 1
                                yearly_eal += 1
                                whole_school_eal += 1
                            if "Gifted and Talented" in a.AlertType.AlertGroup.Name:
                                gifted_and_talented += 1
                                school_section_gifted_and_talented += 1
                                yearly_gifted_and_talented += 1
                                whole_school_gifted_and_talented += 1
            all_variable = girls + boys
            return_data.append([group.split(".")[-1],
                                girls,
                                boys,
                                becket,
                                hardy,
                                lawrence,
                                more,
                                sen,
                                eal,
                                gifted_and_talented,
                                all_variable,
                                teacher_name])

            # yearly logic
            this_group_year = self.groups[self.groups.index(group)].split(".")[3]
            try:
                next_group_year = self.groups[self.groups.index(group) + 1].split(".")[3]
            except Exception as error:
                next_group_year = error
            if this_group_year != next_group_year:
                yearly_all_variable = yearly_girls + yearly_boys
                return_data.append(["%s Total" % this_group_year,
                                    yearly_girls,
                                    yearly_boys,
                                    yearly_becket,
                                    yearly_hardy,
                                    yearly_lawrence,
                                    yearly_more,
                                    yearly_sen,
                                    yearly_eal,
                                    yearly_gifted_and_talented,
                                    yearly_all_variable,
                                    " "])
                yearly_girls = 0
                yearly_boys = 0
                yearly_becket = 0
                yearly_hardy = 0
                yearly_lawrence = 0
                yearly_more = 0
                yearly_sen = 0
                yearly_eal = 0
                yearly_gifted_and_talented = 0

            # school section logic
            this_school_section = self.groups[self.groups.index(group)].split(".")[2]
            try:
                next_school_section = self.groups[self.groups.index(group) + 1].split(".")[2]
            except Exception as error:
                next_school_section = error
            if this_school_section != next_school_section:
                school_section_all_variable = school_section_girls + school_section_boys
                return_data.append(["%s Total" % this_school_section.split("Sc")[0],
                                    school_section_girls,
                                    school_section_boys,
                                    school_section_becket,
                                    school_section_hardy,
                                    school_section_lawrence,
                                    school_section_more,
                                    school_section_sen,
                                    school_section_eal,
                                    school_section_gifted_and_talented,
                                    school_section_all_variable,
                                    " "])
                school_section_girls = 0
                school_section_boys = 0
                school_section_becket = 0
                school_section_hardy = 0
                school_section_lawrence = 0
                school_section_more = 0
                school_section_sen = 0
                school_section_eal = 0
                school_section_gifted_and_talented = 0

        # whole school
        whole_school_all = whole_school_boys + whole_school_girls
        return_data.append(["School Total",
                           whole_school_girls,
                           whole_school_boys,
                           whole_school_becket,
                           whole_school_hardy,
                           whole_school_lawrence,
                           whole_school_more,
                           whole_school_sen,
                           whole_school_eal,
                           whole_school_gifted_and_talented,
                           whole_school_all,
                           ""])
        return return_data

class ThomasStats():
    def __init__(self,AcYear):
        self.AcYear=AcYear
        self.__run()
    def __run(self):
        yearlist=[('Year1','Yr1'),('Year2','Yr2'),('Year3','Yr3'),('Year4','Yr4'),
                  ('Year5','Yr5'),('Year6','Yr6'),('Year7','Yr7'),('Year8','Yr8')]
        for items in yearlist:
            print items[0]
            self.PupilList=self.__getPupilList(items[0])
            print 'got pupils'
            self.DataList=self.__getExtentionData(self.PupilList,items[1])
            print ' got records'
            self.CompiledData=self.__compileData(self.DataList)
            print 'compiling data'
            self.Stats=self.__getStats(self.CompiledData)
            print 'calc stats'
            self.__saveStats(self.Stats,items[0])
    def __getPupilList(self,yeargroup):
        return set([x.Pupil.id for x in PupilGroup.objects.filter(AcademicYear=self.AcYear,
                                                              Group__Name__istartswith='Current',
                                                              Active=True).filter(Group__Name__icontains=yeargroup)])
    def __getExtentionData(self,PupilList,recordyear):
        recordlist=['%s_Eot_M' % recordyear,'%s_Eot_L' % recordyear, '%s_Eot_S' % recordyear]
        return ExtentionData.objects.filter(BaseType='Pupil',
                                            BaseId__in=PupilList,
                                            ExtentionRecord__Name__in=recordlist,
                                            Active=True)
    def __compileData(self,DataObj):
        results={}
        counter=len(DataObj)
        for items in DataObj:
            counter -=1
            print counter
            key= '%s-%s' % (items.ExtentionRecord.Name, items.Subject.Name)
            if not key in results:
                if 'Level' in items.Data:
                    if str(items.Data['Level'][1]) > 0:
                        try:
                            results[key]=[int(items.Data['Level'][1])]
                        except:
                            pass
            else:
                if 'Level' in items.Data:
                    if str(items.Data['Level'][1]) > 0:
                        try:
                            results[key].append(int(items.Data['Level'][1]))
                        except:
                            pass
        return results
    def __getStats(self,CompiledData):
        results={}
        for key,DataList in CompiledData.items():
            results[key]={'Mean':numpy.mean(DataList),
                          'Median':numpy.median(DataList),
                          'Mode':collections.Counter(DataList).most_common(1)[0][0],
                          'Minimum':min(DataList),
                          'Maximum':max(DataList)}
        return results

    def __saveStats(self,Stats,datayear):
        output = open('StatsFor%s.csv' % datayear,'w')
        outlist=["Subject','Mean','Median','Mode','Minimum','Maximum'\n"]

        for item in Stats:
            outlist.append("%s,%s,%s,%s,%s,%s\n" % (item,Stats[item]['Mean'],Stats[item]['Median'],
                                                  Stats[item]['Mode'],
                                                  Stats[item]['Minimum'],Stats[item]['Maximum']))
        output.writelines(outlist)
        output.close()
        return