# django imports
from django.core.cache import cache

# willow tree imports
from MIS.models import Pupil
from MIS.models import PupilProvision
from MIS.models import PupilTarget
from MIS.modules.PupilRecs import PupilObject
from MIS.modules.ExtendedRecords import *

# logging
import logging
log = logging.getLogger(__name__)

#def PupilRecordWithSen(PupilId, AcYear):
#    Pupilrec = cache.get('PupilSEN_%s' % PupilId)
#    if not Pupilrec:
#        PupilOb = PupilObjectWithSen(PupilId, AcYear)
#        cache.set('PupilSEN_%s' % PupilId, PupilOb, 900)
#        return PupilOb
#    else:
#        return Pupilrec

#class PupilRecordWithGT(PupilObject):
#    ''' shows  '''
#    def