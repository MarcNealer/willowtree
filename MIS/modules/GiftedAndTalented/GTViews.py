# python imports
import datetime

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect


# WillowTree system imports
from MIS.modules.notes import NoteFunctions
from MIS.ViewExtras import SetMenu
from MIS.modules.ExtendedRecords import *
from MIS.modules.GeneralFunctions import isUserInThisGroup
from django.core.cache import cache
from MIS.modules.PupilRecs import PupilRecord


def PupilGTRecord(request, school, AcYear, PupilId):
    ''' displays a Pupils Gifted and Talented page '''
    if 'NotesKWordFilter' in request.session:
        del request.session['NotesKWordFilter']
    PupilDetails = Pupil.objects.get(id=int(PupilId))
    PupilDetails2 = PupilRecord(PupilId, AcYear)
    yr = PupilDetails2.Form()[0]
    cat_data = ExtendedRecord('Pupil', PupilId).ReadExtention('Yr%s_Cat' % yr, 'Form_Comment')
    c = {'PupilDetails': PupilDetails,
         'PupilDetails2': PupilDetails2,
         'school': school,
         'AcYear': AcYear,
         'cat_data': cat_data,
         'inGT': isUserInThisGroup(request, 'GT'),
         'subjects': Subject.objects.all(),
         'fromMostAbledPage': True,
         'GTData': PupilGiftedTalented.objects.filter(Pupil__id=int(PupilId),
                                                      Active=True),
         'GTData_old': PupilGiftedTalented.objects.filter(Pupil__id=int(PupilId),
                                                          Active=False),
         'activeTargets': PupilTarget.objects.filter(Pupil__id=int(PupilId),
                                                     Type="G/T",
                                                     Active=True),
         'inActiveTargets': PupilTarget.objects.filter(Pupil__id=int(PupilId),
                                                       Type="G/T",
                                                       Active=False),
         'KWList': NoteFunctions.KeywordList('Notes')}
    c.update(NoteFunctions.GetPupilNotes(request,
                                         Pupil_Id=PupilId,
                                         allMostAbleNotes=True))
    urlString = 'PupilGTRecord.html'
    conInst = RequestContext(request, processors=[SetMenu])
    return render_to_response(urlString,
                              c,
                              context_instance=conInst)


def addNewGT(request, school, AcYear, PupilId):
    ''' adding a new Gifted and Talented record to a Pupil  - this will also
    set a new Gifted and Talented alert if one is not already set '''
    if request.POST['Level'] == "2":
        alertGroupName = "Gifted and Talented 2"
        alertLevel = "2"
    if request.POST['Level'] == "1":
        alertGroupName = "Gifted and Talented"
        alertLevel = "1"
    subjects = 'Most Abled in the following subjects:<br />'
    if PupilGiftedTalented.objects.filter(Pupil__id=int(PupilId),
                                          Active=True,
                                          Level=alertLevel).exists():
        for i in PupilGiftedTalented.objects.filter(Pupil__id=int(PupilId),
                                                    Active=True,
                                                    Level=alertLevel):
            subjects += '%s,<br />' % i.Subject.Name
    subjects += request.POST['subject']
    # alert code
    if PupilAlert.objects.filter(PupilId__id=int(PupilId),
                                 AlertType__AlertGroup__Name=alertGroupName,
                                 Active=True).exists():
        alertObj = PupilAlert.objects.get(PupilId__id=int(PupilId),
                                          AlertType__AlertGroup__Name=alertGroupName,
                                          Active=True)
        alertObj.AlertDetails = subjects
        alertObj.save()
    else:
        pupilAlertObj = PupilAlert(PupilId=Pupil.objects.get(id=int(PupilId)),
                                   StartDate=datetime.datetime.now(),
                                   AlertType=AlertType.objects.get(Name=alertGroupName),
                                   AlertDetails=subjects,
                                   RaisedBy=Staff.objects.get(User__id=request.session['_auth_user_id']))
        pupilAlertObj.save()
    # GT code
    if request.POST['identified'] == "":
        identified = datetime.datetime.now()
    else:
        identified = request.POST['identified']
    pupilGTObj = PupilGiftedTalented(Pupil=Pupil.objects.get(id=int(PupilId)),
                                     Subject=Subject.objects.get(Name=request.POST['subject']),
                                     Details=request.POST['gtTextArea'],
                                     Level=request.POST['Level'],
                                     Mentor=request.POST['mentor'],
                                     DateIdentified=identified)
    pupilGTObj.save()
    cache.delete('Pupil_%s' % PupilId)
    httpAddress = '/WillowTree/%s/%s/pupilGiftedAndTalented/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def modifyGT(request, school, AcYear, PupilId, giftedAndTalentedId):
    ''' modify a Gifted and Talented record '''
    if request.POST['Level'] == "2":
        alertGroupName = "Gifted and Talented 2"
        alertLevel = "2"
    if request.POST['Level'] == "1":
        alertGroupName = "Gifted and Talented"
        alertLevel = "1"
    gtObj = PupilGiftedTalented.objects.get(id=int(giftedAndTalentedId))
    gtObj.Subject = Subject.objects.get(Name=request.POST['subject'])
    gtObj.Details = request.POST['gtTextArea']
    gtObj.Mentor = request.POST['mentor']
    if request.POST['identified'] == "":
        identified = datetime.datetime.now()
    else:
        identified = request.POST['identified']
    gtObj.DateIdentified = identified
    gtObj.save()
    # update pupilAlert code
    pupilAlertObj = PupilAlert.objects.get(PupilId__id=int(PupilId),
                                           AlertType__AlertGroup__Name=alertGroupName,
                                           Active=True)
    subjects = 'Most Abled in the following subjects:<br />'
    for i in PupilGiftedTalented.objects.filter(Pupil__id=int(PupilId),
                                                Level=alertLevel,
                                                Active=True):
            subjects += '%s,<br />' % i.Subject.Name
    pupilAlertObj.AlertDetails = subjects
    pupilAlertObj.save()
    cache.delete('Pupil_%s' % PupilId)
    httpAddress = '/WillowTree/%s/%s/pupilGiftedAndTalented/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def deleteGT(request, school, AcYear, PupilId, giftedAndTalentedId):
    ''' deleted a Gifted and Talented record  - also checks to see if this is
    the last Gifted and Talented record linked to this pupil. If it is then
    the pupilAlert linked to this pupil is also closed '''
    # delete Gifted and Talented record
    gtObj = PupilGiftedTalented.objects.get(id=int(giftedAndTalentedId))
    gtObj.ReasonsForRemovalGiftedAndTalented = request.POST['reasons_for_removal_of_status']
    if request.POST['date_status_removed'] == "":
        date_status_removed = datetime.datetime.now()
    else:
        date_status_removed = request.POST['date_status_removed']
    gtObj.DateNotGiftedAndTalented = date_status_removed
    gtObj.Active = False
    gtObj.save()
    if gtObj.Level == "1":
        alertGroupName = "Gifted and Talented"
        level = "1"
    if gtObj.Level == "2":
        alertGroupName = "Gifted and Talented 2"
        level = "2"
    pupilAlertObj = PupilAlert.objects.get(PupilId__id=int(PupilId),
                                       AlertType__AlertGroup__Name=alertGroupName,
                                       Active=True)
    # if that was last Gifted and Talented record - pupilAlert also closed
    if not PupilGiftedTalented.objects.filter(Pupil__id=int(PupilId),
                                              Level=level,
                                              Active=True).exists():
        pupilAlertObj = PupilAlert.objects.get(PupilId__id=int(PupilId),
                                               AlertType__AlertGroup__Name=alertGroupName,
                                               Active=True)
        pupilAlertObj.Active = False
    # update pupilAlert code
    subjects = 'Most Abled in the following subjects:<br />'
    for i in PupilGiftedTalented.objects.filter(Pupil__id=int(PupilId),
                                                Active=True,
                                                Level=level):
        subjects += '%s,<br />' % i.Subject.Name
    pupilAlertObj.AlertDetails = subjects
    pupilAlertObj.save()
    cache.delete('Pupil_%s' % PupilId)
    httpAddress = '/WillowTree/%s/%s/pupilGiftedAndTalented/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def add_target(request, school, AcYear, PupilId):
    """
    adds a most abled target for a pupil
    """
    targetObj = PupilTarget(Pupil=Pupil.objects.get(id=PupilId),
                            Type=request.POST['targetType'],
                            ReviewDate=request.POST['reviewDate'])
    if 'targetText' in request.POST:
        targetObj.Target = request.POST['targetText']
    if 'reviewText' in request.POST:
        targetObj.Review = request.POST['reviewText']
    if "Progress" in request.POST:
        targetObj.Progress = request.POST['Progress']
    else:
        targetObj.Progress = "Not Met"
    targetObj.save()
    httpAddress = '/WillowTree/%s/%s/pupilGiftedAndTalented/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def modify_target(request, school, AcYear, PupilId, target_id):
    """
    modifies a most abled target
    """
    targetObj = PupilTarget.objects.get(id=target_id)
    if 'targetText' in request.POST:
        targetObj.ReviewDate = request.POST['reviewDate']
    if 'targetText' in request.POST:
        targetObj.Target = request.POST['targetText']
    if 'reviewText' in request.POST:
        targetObj.Review = request.POST['reviewText']
    if "Progress" in request.POST:
        targetObj.Progress = request.POST['Progress']
    targetObj.save()
    httpAddress = '/WillowTree/%s/%s/pupilGiftedAndTalented/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def close_target(request, school, AcYear, PupilId, target_id):
    """
    closes a most abled target
    """
    target = PupilTarget.objects.get(id=target_id)
    target.Active = False
    target.save()
    httpAddress = '/WillowTree/%s/%s/pupilGiftedAndTalented/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)