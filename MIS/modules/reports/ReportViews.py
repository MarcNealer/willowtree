# Python imports
import json

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader
from django.template import RequestContext
from django.core.context_processors import csrf
from django.http import *

# WillowTree system imports
from MIS.ViewExtras import SetMenu
from MIS.ViewExtras import SetLogo
from MIS.models import * ## CHANGE????
from MIS.modules.attendance import AttendanceFunctions
from MIS.modules.classlists import ClassListFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.AnalysisReports import ReportFunctions
from MIS.modules.housePoints import housePointsFunctions
from MIS.modules.manager import ManagerFunctions
from MIS.modules.Extracts.GradeExtract import RawGradeExtract
from MIS.modules.Extracts.GradeExtract import GradeExtract


# Other Python Imports
import csv


def AdminApplicantsAmtInGrps(request, school, AcYear):
    ''' This report shows how many applicants are in each of the applicant
    groups for the next ten years...
    The groups in the output dictionary are in lists where the year is their
    key.
    The groups are as follows:
    Reception,Year1,Year2,Year3,Year4,Year5,Year6,Year7,Year8,'''
    schoolString = MenuTypes.objects.get(Name=school).SchoolId.Name
    acYearList = []
    AcYear2 = AcYear.split('-')
    counter = 0
    for i in range(10):
        year = AcYear2
        year = [int(year[0]) + counter, int(year[1]) + counter]
        year = [unicode(year[0]), unicode(year[1])]
        year = '-'.join(year)
        acYearList.append(year)
        counter += 1
    classYears = ['Reception', 'Year1', 'Year2', 'Year3', 'Year4',
                  'Year5', 'Year6', 'Year7', 'Year8']
    yearDic = {}
    for year in acYearList:
        yearDic.update({year: [0, 0, 0, 0, 0, 0, 0, 0, 0]})
    for Year in acYearList:
        counter2 = 0
        for classYear in classYears:
            grpName = 'Applicants.%s.Main.%s' % (schoolString, classYear)
            grp = PupilGroup.objects.filter(Group__Name=grpName,
                                            AcademicYear=str(Year),
                                            Active=True)
            yearDic[Year][counter2] = len(grp)
            counter2 += 1
    response = HttpResponse(mimetype="text/csv")
    response_writer = csv.writer(response)
    response_writer.writerow(['', 'Reception', 'Year 1', 'Year 2', 'Year 3',
                              'Year 4', 'Year 5', 'Year 6', 'Year 7',
                              'Year 8'])
    for year in acYearList:
        response_writer.writerow([year, yearDic[year][0],
                                 yearDic[year][1],
                                 yearDic[year][2],
                                 yearDic[year][3],
                                 yearDic[year][4],
                                 yearDic[year][5],
                                 yearDic[year][6],
                                 yearDic[year][7],
                                 yearDic[year][8]])
    csvFile = 'attachment; filename=AmountOfApplicantsInEachGroup.csv'
    response['Content-Disposition'] = csvFile
    return response

def AttendanceReports(request,school,AcYear,GroupName):
    Reportdata=AttendanceFunctions.AttendanceReport(request,AcYear,GroupName)
    c={'ReportData':Reportdata}
    c.update({'AttCodes': AttendanceCode.objects.all()})
    return render_to_response(Reportdata.ReportTemplate,c,context_instance=RequestContext(request,processors=[SetMenu]))

def OtherReports(request, school, AcYear, GroupName):
    ''' Generate reports from Other Reports drop down '''
    try:
        grpNme = '.'.join(GroupName.split('.')[3:])
    except:
        grpNme = 'None'
    # pupil picture report
    if 'PupilPictureReport.html' in request.POST['ReportName']:
        pupilDetails = []
        for i in request.POST.getlist('PupilSelect'):
            pupilDetails.append(PupilRecord(i, AcYear))
        c = {'class': grpNme, 'pupilDetails': pupilDetails}
        return render_to_response('Reports/PupilPictureReport.html',c,
                                  context_instance=RequestContext(request,
                                                                  processors=[SetMenu]))
    # house point report
    if 'housePointsReport' in request.POST['ReportName']:
        if '(per class)' in request.POST['ReportName']:
            c={'reportData': housePointsFunctions.housePointsReport(GroupName, AcYear)}
            contextInstance = RequestContext(request,processors=[SetMenu])
            return render_to_response('housePointsReport.html', c,
                                      context_instance=contextInstance)
        if '(per pupil)' in request.POST['ReportName']:
            c={'reportData': housePointsFunctions.housePointsReport(GroupName, AcYear)}
            contextInstance = RequestContext(request,processors=[SetMenu])
            return render_to_response('housePointsReportPerPupil.html', c,
                                      context_instance=contextInstance)
    # trip list reports
    if 'Trips List' in request.POST['ReportName']:
        pupilDetails = []
        for i in request.POST.getlist('PupilSelect'):
            pupilDetails.append(PupilRecord(i, AcYear))
        c = {'class': grpNme, 'pupilDetails': pupilDetails}
        if 'Medical Details' in request.POST['ReportName']:
            return render_to_response('tripsFormWithMedicalDetails.html',c,
                                      context_instance=RequestContext(request,
                                                                      processors=[SetMenu]))
        else:
            return render_to_response('tripsForm.html',c,
                                      context_instance=RequestContext(request,
                                                                      processors=[SetMenu]))
    # EPIF reports
    if 'EPIF' in request.POST['ReportName']:
        Classlist=[]
        for PupilIds in request.POST.getlist('PupilSelect'):
            Classlist.append(PupilRecord(PupilIds,AcYear))
        c={'ClassList':Classlist,'AcYear':AcYear,'PupilForm':GroupName.split('.')[-1]}
        return render_to_response('EPIF_Report.html',c,context_instance=RequestContext(request))
    # pupil sibling report
    if 'PupilSibling.html' in request.POST['ReportName']:
        Classlist=[]
        for PupilIds in request.POST.getlist('PupilSelect'):
            Classlist.append(PupilRecord(PupilIds,AcYear))
        c={'class':Classlist,
           'AcYear':AcYear,
           'PupilForm':GroupName.split('.')[-1]}
        return render_to_response('Reports/%s' % request.POST['ReportName'],c,
                                  context_instance=RequestContext(request,processors=[SetMenu]))
    # birthday reports
    else:
        c={'class': ClassListFunctions.GroupClassList(request.POST,GroupName,school,AcYear)}
        return render_to_response('Reports/%s' % request.POST['ReportName'],c,
                                  context_instance=RequestContext(request,processors=[SetMenu]))


def PupilGradeReport(request,school,AcYear,PupilNo):
    c={'Record':PupilRecord(PupilNo,AcYear)}
    return render_to_response('Reports/GradeReport.html' ,c)


def EYFSMapping(request,school,AcYear,GroupName):
    c={'Report':ReportFunctions.EYFS_Map(GroupName,AcYear,request.POST['ReportSubject'])}
    return render_to_response('Reports/EYFSMap.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def schoolStats(request, school, AcYear):
    ''' creates a whole school stats reports shows girls/boys/houses '''
    school_name = MenuTypes.objects.get(Name=school).SchoolId.Name
    c = {'report': ManagerFunctions.statsDataOnGroup(school_name,
                                                     AcademicYear=AcYear,
                                                     wholeSchool=True)}
    context_instance = RequestContext(request, processors=[SetMenu])
    return render_to_response('Reports/schoolStats.html',
                              c, context_instance=context_instance)


def return_raw_grade_data_csv(request, school, AcYear, GroupName):
    """ Returns all raw grade data for a given group """
    grade_data = RawGradeExtract(AcYear,
                                 GroupName,
                                 request.POST['GradeRecord'],
                                 request.POST['ReportSubject']).run()
    response = HttpResponse(mimetype="text/csv")
    response_writer = csv.writer(response)
    for data_line in grade_data:
        response_writer.writerow(data_line)
    response['Content-Disposition'] = 'attachment; filename=grade_extract_%s_%s_%s_%s.csv' % (school,
                                                                                              GroupName,
                                                                                              request.POST['GradeRecord'],
                                                                                              request.POST['ReportSubject'])
    return response


def grade_extract_options_page(request, school, AcYear):
    """
    Returns the page for giving option to generate the grade extract report.
    """
    try:
        # Get pupil groups for the school
        school_name = MenuTypes.objects.get(Name=school).SchoolId.Name
        pupil_groups = Group.objects.filter(Q(Name__icontains='Current') & Q(Name__icontains=school_name)).exclude(Name__contains='.House').exclude(Name__contains='Kindergarten').values_list('Name', flat=True)

        # Get only grade record names for end of term and half term records as a flat list
        grade_records = ExtentionRecords.objects.filter(Q(Name__icontains='Eot') | Q(Name__icontains='Hf')).values_list('Name', flat=True)

        # Subjects to grade record array
        subjects_to_grade_record = {}
        for grade_record in grade_records:
            subjects_to_grade_record.update({grade_record: list(GradeInputElement.objects.filter(GradeRecord__Name=grade_record).values_list('GradeSubject__Name', flat=True))})

        # Get exam types as a list
        exam_types = eval(ExtentionPickList.objects.get(Name='ExamTypes').Data)

        # Data to return
        returned_data = {'pupil_groups': pupil_groups,
                         'grade_records': grade_records,
                         'subjects_to_grade_record': subjects_to_grade_record,
                         'exam_types': exam_types}

        return render_to_response('grade_extract_options_page.html',
                                  returned_data,
                                  context_instance=RequestContext(request, processors=[SetMenu]))
    except Exception as error:
        HttpResponseBadRequest(error)


def grade_extract_generate(request, school, AcYear):
    """
    Returns a CSV file containing a pupil groups grade data based upon post data returned from
    grade_extract_options_page
    """
    today_date = datetime.date.today().isoformat()
    pupil_group = request.POST['pupil_group']
    grade_record = request.POST['grade_record']

    subject_present = False
    subject_list = []
    for key, value in request.POST.iteritems():
        if "subjects_to_grade_record_" in key:
            subject_list.append(value)
            subject_present = True

    data_to_return_effort_present = False
    effort = False
    level = False
    attainment = False
    exam_1 = False
    exam_2 = False
    exam_3 = False
    if "data_to_return_effort" in request.POST:
        effort = request.POST['data_to_return_effort']
        data_to_return_effort_present = True
    if "data_to_return_level" in request.POST:
        level = request.POST['data_to_return_level']
        data_to_return_effort_present = True
    if "data_to_return_attainment" in request.POST:
        attainment = request.POST['data_to_return_attainment']
        data_to_return_effort_present = True
    if "data_to_return_exam_1" in request.POST:
        exam_1 = request.POST['data_to_return_exam_1']
        data_to_return_effort_present = True
    if "data_to_return_exam_2" in request.POST:
        exam_2 = request.POST['data_to_return_exam_2']
        data_to_return_effort_present = True
    if "data_to_return_exam_3" in request.POST:
        exam_3 = request.POST['data_to_return_exam_3']
        data_to_return_effort_present = True

    # Check to see if a grade record, at least one subject and at least one data_to_return value was selected.
    if request.POST['grade_record'] == "None" or subject_present is False or data_to_return_effort_present is False:
        if request.POST['grade_record'] == "None":
            error_message = "You did not select a grade record"
        elif subject_present is False:
            error_message = "You did not select at least one subject"
        else:
            error_message = "You did not select a data value to return"
        return HttpResponse("<h2>ERROR: %s. Please click <a href='/WillowTree/%s/%s/grade_extract_options_page/'>here</a> to try again.</h2>" % (error_message,
                                                                                                                                                 school,
                                                                                                                                                 AcYear))
    else:
        grade_extract = GradeExtract(AcYear,
                                     pupil_group,
                                     grade_record,
                                     subject_list,
                                     effort,
                                     level,
                                     attainment,
                                     exam_1,
                                     exam_2,
                                     exam_3)
        grade_extract_in_csv_format = grade_extract.run()
        response = HttpResponse(mimetype="text/csv")
        response_writer = csv.writer(response)
        for row_line in grade_extract_in_csv_format:
            response_writer.writerow(row_line)
        response['Content-Disposition'] = 'attachment; filename=grade_extract_%s_%s.csv' % (pupil_group, today_date)
        return response