#-------------------------------------------------------------------------------
# Name:        DefaultData
# Purpose:      Pushes default records into the WillowTree MIS
#
# Author:      DBMgr
#
# Created:     12/02/2012
# Copyright:   (c) DBMgr 2012
# Licence:
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from WillowTree.MIS import models
from MIS.models import *
import datetime, copy, ConfigParser, csv
from MIS.modules.ExtendedRecords import *
from MIS.modules import GeneralFunctions
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from MIS.modules.attendance import AttendanceFunctions

class SnapShotRecord():
    def __init__(self,Pupil_Id,GradeRecord,AcademicYear,SnapshotStart,SnapshotFinish):
        self.Pupil_Id=Pupil_Id
        self.PupilRec=Pupil.objects.get(id=self.Pupil_Id)
        self.StartDate=SnapshotStart
        self.StopDate=SnapshotFinish
        #get a list of groups the pupil belongs to
        self.GroupList=GeneralFunctions.ListPupilGroups(self.Pupil_Id,AcademicYear)
        # from the grade input elements, get a list of subjects for this report
        self.GradeSubjectList=GradeInputElement.objects.filter(GradeRecord__Name=GradeRecord)
        #Set the Sets in a dictionary
        self.__SetPupilSets__()
        #Set the From Class  form:TeacherVariable
        self.__SetPupilForm__()
        self.__SetPupilFormTeacher__()
        #Set Detail of Flags acitve List of dictionaries
        self.__SetPupilFlagsStatus__()
        #Set Details of any Issues active List of Dictonaries
        self.__SetPupilIssues__()
        #Set details of Attendance Dictionary {'Lates',Unauth absences, Auth Absences}
        self.__AttendanceStats__()

    def __AttendanceStats__(self):
        Attendance=AttendanceFunctions.PupilAttendanceStats(self.StartDate,self.StopDate,self.Pupil_Id)
        self.AttendanceStats={'Auth':Attendance.AuthorisedCount(),'Unauth':Attendance.UnauthorisedCount(),'Lates':Attendance.LateCount()}
    def PupilAttendanceStats(self):
        ''' Getter Method for Attendance stats'''
        return self.AttendanceStats
    def __SetPupilSets__(self):
        ''' Generate a list of Pupil Sets data'''
        # setup dictionary to hold the set details
        self.SubjectSets={}
        for subjects in self.GradeSubjectList:
            for groups in self.GroupList:
                if subjects.GradeSubject.Name in groups and 'Set' in groups:
                    self.SubjectSets[subjects.GradeSubject.Name]=groups.split('.')[-1]
            if ParentSubjects.objects.filter(Subject=subjects.GradeSubject).exists():
                for parentSubjects in ParentSubjects.objects.filter(Subject=subject.GradeSubject):
                    for groups in self.GroupList:
                        if parentSubjects.ParentSubject.Name in groups and 'Set' in groups:
                            self.SubjectSets[Subjects.GradeSubject.Name]=groups.split('.')[-1]        
    def PupilSets(self):
        ''' Getter method for sets'''
        return self.SubjectSets
    def __SetPupilFlagsStatus__(self):
        ''' Sets the Actrive Flags property '''
        self.ActiveFlags=[]
        for flags in PupilAlert.objects.filter(PupilId=self.Pupil_Id,Active=True,StartDate__lte= datetime.datetime.today).order_by('AlertType__AlertGroup__DisplayOrder'):
            self.ActiveFlags.append({'AlertType':flags.AlertType.Name,'AlertDetails':flags.AlertDetails})

    def PupilFlagsStatus(self):
        ''' Getter method for Flags'''
        return self.ActiveFlags
    def __SetPupilForm__(self):
        
        ''' Sets up the Form variable '''
        self.Form=''
        for groups in self.GroupList:
            if 'Form' in groups:
                self.Form=groups.split('.')[-1]
    def PupilForm(self):
        '''Getter method for Form class'''
        return self.Form
    def __SetPupilFormTeacher__(self):
        '''Set the Form teacher Property'''
        try:
            self.FormTeacher=GlobalVariables.Get_SystemVar('FormTeacher.%s' % self.Form)
        except:
            self.FormTeacher='Not Specified'
    def PupilFormTeacher(self):
        '''Getter method for Form Teacher'''
        return self.FormTeacher
    def __SetPupilIssues__(self):
        '''Sets the list of Pupil Issues'''
        self.IssueCount=PupilNotes.objects.filter(PupilId=self.Pupil_Id,NoteId__IsAnIssue=True,NoteId__Active=True).exclude(NoteId__IssueStatus='Closed').count()

    def PupilIssuesCount(self):
        '''Getter method for basic issue details'''
        return self.IssueCount
    