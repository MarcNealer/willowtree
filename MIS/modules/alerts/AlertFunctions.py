# python imports
import datetime, copy
import logging
log = logging.getLogger(__name__)

# Willow Tree Imports
from MIS.models import *
from MIS.modules import GeneralFunctions


class Alerts():
    ''' This class is used to get Active Alert objects for a selected pupil.
    The whole object is passed to the html rendering engine'''
    def __init__(self, request, Pupil_Id):
        self.request = request
        self.Pupil_Id = Pupil.objects.get(id=int(Pupil_Id))
        self.request = request

    def ActivePupilAlerts(self):
        ''' returns a list collection of active alerts'''
        return PupilAlert.objects.filter(PupilId=self.Pupil_Id,Active=True,StartDate__lte= datetime.datetime.today).order_by('AlertType__AlertGroup__DisplayOrder')

    def Close(self):
        ''' This method is used to close and selected alert.
        This method expects to find the details of which alert to close
        within the request POST data object. It also searches the request object
        for possible closure details to save with the report
        '''
        if 'AlertClosedDetails' in self.request.POST:
            Closed_Details=self.request.POST['AlertClosedDetails']
        else:
            Closed_Details="No details supplied"
        OpenAlert=PupilAlert.objects.get(id=self.request.POST['AlertId'])
        if len(self.request.POST['StopDate']) > 2:
            OpenAlert.StopDate=self.request.POST['StopDate']
        else:
            OpenAlert.StopDate=datetime.datetime.today()
        OpenAlert.AlertClosedDetails=Closed_Details
        OpenAlert.Active=False
        OpenAlert.ClosedBy=self.request.session['StaffId']
        OpenAlert.UpdateDate=datetime.datetime.today()
        OpenAlert.save()
        log.warn('Alert %s has been closed by %s ' % (self.request.POST['AlertId'],
                                                      self.request.user.username))
        return True
    def Save(self):
        ''' saved a new Alert. Details of the Alert should be passed in the
        request.POST elements from the html form'''
        if len(self.request.POST['StartDate']) > 2:
            Start_Date=self.request.POST['StartDate']
        else:
            Start_Date=datetime.datetime.today()
        alertDetails = self.request.POST['AlertDetails'].replace('\'', '')
        RecordDetails=PupilAlert(PupilId=self.Pupil_Id,
                                 Active=True,
                                 AlertType=AlertType.objects.get(Name=self.request.POST['AlertType']),
                                 StartDate=Start_Date,
                                 AlertDetails=alertDetails,
                                 RaisedBy=self.request.session['StaffId'],
                                 UpdateDate=datetime.datetime.today())
        RecordDetails.save()
        if len(self.request.POST['StopDate']) > 2:
            RecordDetails.StopDate=self.request.POST['StopDate']
            RecordDetails.ClosedBy=self.request.session['StaffId']
            RecordDetails.save()
        if 'AlertClosedDetails' in self.request.POST:
            RecordDetails.AlertClosedDetails=self.request.POST['AlertClosedDetails']
            RecordDetails.save()
        log.warn('Alert %s has been saved by %s ' % (RecordDetails.id,
                                                     self.request.user.username))
        return True
    def Modify(self):
        ''' This method is used to modify a given Alert object. However, it does
        not modify an existing record, instead it sets the current object to Active=False
        and then creates a new object'''
        RecordDetails=PupilAlert.objects.get(id=self.request.POST['AlertId'])
        RecordDetails.StartDate=self.request.POST['StartDate']
        if len(self.request.POST['StopDate']) > 2:
            RecordDetails.StopDate=self.request.POST['StopDate']
        RecordDetails.AlertType=AlertType.objects.get(Name=self.request.POST['AlertType'])
        alertDetails = self.request.POST['AlertDetails'].replace('\'', '')
        RecordDetails.AlertDetails = alertDetails
        if 'AlertClosedDetails' in self.request.POST:
            RecordDetails.AlertClosedDetails=self.request.POST['AlertClosedDetails']
        RecordDetails.RaisedBy=self.request.session['StaffId']
        RecordDetails.UpdateDate=datetime.datetime.today()
        RecordDetails.save()
        log.warn('Alert %s has been modified by %s ' % (RecordDetails.id,
                                                        self.request.user.username))
        return

    def AlertList(self):
        ''' Returns a list of alerts the requesting user is allow to create, modify
        or remove. Any give alert is marked as only being available for creation or
        modification by a staff in a given staff group, or child groups of that group.

        Returned data is in a list collection.
        '''
        PupilAlertList=[]
        ListOfAlertGroups=AlertGroup.objects.filter().order_by('DisplayOrder')
        for Groups in ListOfAlertGroups:
            PupilAlertGroup={'HasPermission':False}
            for StaffGroups in Groups.PermissionGroups.all():
                if not PupilAlertGroup['HasPermission']:
                    PupilAlertGroup['HasPermission']=GeneralFunctions.StaffInGroup(self.request, StaffGroups)

            PupilAlertGroup['Active']=False
            PupilAlertGroup['Name']=Groups.Name
            PupilAlertGroup['ListOfAlerts']=[]
            ListOfAlertTypes=AlertType.objects.filter(AlertGroup=Groups)
            for Types in ListOfAlertTypes:
                PupilAlertGroup['ListOfAlerts'].append({'Name':Types.Name,'Active':False})
            if PupilAlert.objects.filter(PupilId=self.Pupil_Id,Active=True,AlertType__AlertGroup=Groups).exists():
                PupilAlertGroup['Active']=True
                PupilAlertGroup['Alert']=PupilAlert.objects.get(PupilId=self.Pupil_Id,Active=True,AlertType__AlertGroup=Groups)
                for items in PupilAlertGroup['ListOfAlerts']:
                    if items['Name'].find(PupilAlertGroup['Alert'].AlertType.Name) > -1:
                        items['Active']=True
            PupilAlertList.append(PupilAlertGroup)
        return PupilAlertList

    def Valid(self):
        ''' Thif method is used to vaildate an alert creation (either though create
        or modify)

        Return data is either True, or html based error messages
        '''
        return_valid=True
        self.ErrMsg=[]
        self.AlertItems=self.request.POST
        if self.request.POST['save']=='Activate' and PupilAlert.objects.filter(PupilId=self.Pupil_Id,AlertType__Name=self.request.POST['AlertType'],Active=True).exists():
            return_valid=False
            self.ErrMsg.append('System Error: An Alert of this nature already exists. Please refresh your Alerts Page')
        if self.request.POST['save']=='Activate' or self.request.POST['save']=='New':
            if len(self.request.POST['AlertDetails']) < 3:
                self.ErrMsg.append('Alert details comment is not long enough')
                return_valid=False
            if not 'AlertType' in self.request.POST:
                self.ErrMsg.append('No Alert Type was chosen')
                return_valid=False
        elif self.request.POST['save']=='Modify':
            if len(self.request.POST['AlertDetails']) < 3:
                self.ErrMsg.append('Alert details comment is not long enough')
                return_valid=False
        if not return_valid:
            self.ErrMsg.append('<br><br>')
            self.ErrMsg.append('Changes have been rejected')
            self.ErrMsg.append('Using the back page on the brower may recover your items')
        return return_valid