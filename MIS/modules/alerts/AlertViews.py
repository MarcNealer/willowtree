# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.core.context_processors import csrf
from django.http import *

# WillowTree system imports
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.models import *
from MIS.modules.alerts import AlertFunctions
from MIS.modules.PupilRecs import PupilRecord
from MIS.modules.GeneralFunctions import isUserInThisGroup

def ViewAlerts(request, school, AcYear, PupilNo):
    ''' View to create the edit Alerts page'''
    c = {'school': school, 'AcYear': AcYear}
    c.update(csrf(request))
    AlertDetails = AlertFunctions.Alerts(request, PupilNo)
    c.update({'PupilAlertList': AlertDetails.AlertList(),
              'PupilDetails': PupilRecord(int(PupilNo), AcYear),
              'isSEN': isUserInThisGroup(request, 'SEN'),
              'isGT': isUserInThisGroup(request, 'GT')})
    return render_to_response('EditAlerts.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def UpdateAlerts(request, school, AcYear, PupilNo, Alert_Group):
    '''View used to proccess returned forms from the ViewAlerts view'''
    AlertItem=AlertFunctions.Alerts(request,PupilNo)
    return_msg=[]
    if AlertItem.Valid():
        if request.POST['save'].find('Activate') > -1:
            AlertItem.Save()
            return_msg.append('%s Alert Actived for Pupil No %s' %(Alert_Group,PupilNo))
        elif request.POST['save'].find('New') > -1:
            AlertItem.Close()
            AlertItem.Save()
            return_msg.append('New %s Alert Actived for Pupil No %s' %(Alert_Group,PupilNo))
        elif request.POST['save'].find('Modify') > -1:
            AlertItem.Modify()
            return_msg.append('%s Alert Modified for Pupil No %s' %(Alert_Group,PupilNo))
        elif request.POST['save'].find('Close') > -1:
            AlertItem.Close()
            return_msg.append('%s Alert Closed for Pupil No %s' %(Alert_Group,PupilNo))
    else:
        return_msg=AlertItem.ErrMsg
    c = {'school': school,
         'AcYear': AcYear,
         'return_msg': return_msg,
         'PupilAlertList': AlertItem.AlertList(),
         'PupilDetails': PupilRecord(int(PupilNo),AcYear),
         'isSEN': isUserInThisGroup(request, 'SEN'),
         'isGT': isUserInThisGroup(request, 'GT')}
    return render_to_response('EditAlerts.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
