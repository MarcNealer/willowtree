from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
import cPickle
from django.core.exceptions import *


class ExtendedRecord():
    def __init__(self, BaseType, BaseId, request=None):
        self.BaseType = BaseType
        self.BaseId = BaseId
        self.request = request

    def WriteExtention(self, ExtentionName, FieldDataNew, SubjectName=None):
        '''Writes/Updates data in the extentions system.
        Usage

        Create the object with BaseType and Id
        Newobject=ExtendedRecord(request,BaseType='Pupil',BaseId=4)
        Then write data
        NewObject.WriteExtention(ExtentionName='Yr3_EndofTerm_Mich',
        ExtentionData={#Field:Data},SubjectName=#Optional)
        '''
        # get ExtentionField Record
        OldRecExists = True
        try:
            ExtRec = ExtentionRecords.objects.get(Name=ExtentionName)
        except:
            return False
        try:
            if SubjectName:
                CurrentRec = ExtentionData.objects.get(ExtentionRecord=ExtRec,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Subject__Name=SubjectName,
                                                       Active=True)
            else:
                CurrentRec = ExtentionData.objects.get(ExtentionRecord=ExtRec,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Active=True)
        except MultipleObjectsReturned:
            if SubjectName:
                BadRecs = ExtentionData.objects.filter(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Subject__Name=SubjectName,
                                                       Active=True)
                for recs in BadRecs:
                    CurrentRec = recs
                    CurrentRec.Active = False
                    CurrentRec.save()
                CurrentRec.Active = True
                CurrentRec.save()
            else:
                BadRecs = ExtentionData.objects.filter(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Active=True)
                for recs in BadRecs:
                    CurrentRec = recs
                    CurrentRec.Active = False
                    CurrentRec.save()
                CurrentRec.Active = True
                CurrentRec.save()
            return self.WriteExtention(ExtentionName,
                                       FieldDataNew,
                                       SubjectName)
        except ObjectDoesNotExist:
            OldRecExists = False
        if OldRecExists:
            FieldData = CurrentRec.Data
        else:
            FieldData = {}
        NewFieldsAdded = False
        for NewKey, NewData in FieldDataNew.items():
            if ExtRec.FieldExists(NewKey):
                try:
                    FieldData[NewKey] = [NewData,
                                        ExtRec.GetPickListValue(NewKey, NewData)]
                    NewFieldsAdded = True
                except:
                    pass
        if NewFieldsAdded:
            if OldRecExists:
                CurrentRec.Active = False
                CurrentRec.save()
            if SubjectName:
                NewRecord=ExtentionData(ExtentionRecord=ExtentionRecords.objects.get(Name=ExtentionName),
                                        BaseType=self.BaseType,
                                        BaseId=self.BaseId,
                                        Subject=Subject.objects.get(Name=SubjectName),
                                        UpdatedOn=datetime.datetime.now(),
                                        Data=FieldData)
                NewRecord.save()
            else:
                NewRecord=ExtentionData(ExtentionRecord=ExtentionRecords.objects.get(Name=ExtentionName),
                                        BaseType=self.BaseType,
                                        BaseId=self.BaseId,
                                        Subject=Subject.objects.get(Name='Form_Comment'),
                                        UpdatedOn=datetime.datetime.now(),
                                        Data=FieldData)
                NewRecord.save()
            if self.request:
                NewRecord.UpdatedBy = self.request.session['StaffId']
                NewRecord.save()
        return

    def ReplaceExtention(self, ExtentionName, FieldDataNew, SubjectName=None):
        '''Writes/Updates data in the extentions system.
        Usage

        Create the object with BaseType and Id
        Newobject=ExtendedRecord(request,BaseType='Pupil',BaseId=4)
        Then write data
        NewObject.WriteExtention(ExtentionName='Yr3_EndofTerm_Mich',
        ExtentionData={#Field:Data},SubjectName=#Optional)
        '''
        # get ExtentionField Record
        OldRecExists = True
        try:
            ExtRec = ExtentionRecords.objects.get(Name=ExtentionName)
        except:
            return False
        try:
            if SubjectName:
                CurrentRec = ExtentionData.objects.get(ExtentionRecord=ExtRec,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Subject__Name=SubjectName,
                                                       Active=True)
            else:
                CurrentRec = ExtentionData.objects.get(ExtentionRecord=ExtRec,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Active=True)
        except MultipleObjectsReturned:
            if SubjectName:
                BadRecs = ExtentionData.objects.filter(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Subject__Name=SubjectName,
                                                       Active=True)
                for recs in BadRecs:
                    CurrentRec = recs
                    CurrentRec.Active = False
                    CurrentRec.save()
                CurrentRec.Active = True
                CurrentRec.save()
            else:
                BadRecs = ExtentionData.objects.filter(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Active=True)
                for recs in BadRecs:
                    CurrentRec = recs
                    CurrentRec.Active = False
                    CurrentRec.save()
                CurrentRec.Active = True
                CurrentRec.save()
            return self.WriteExtention(ExtentionName,
                                       FieldDataNew,
                                       SubjectName)
        except ObjectDoesNotExist:
            OldRecExists = False
        FieldData = {}
        NewFieldsAdded = False
        for NewKey, NewData in FieldDataNew.items():
            if ExtRec.FieldExists(NewKey):
                try:
                    print 'Adding new Fields'
                    FieldData[NewKey] = [NewData,
                                        ExtRec.GetPickListValue(NewKey, NewData)]
                    NewFieldsAdded = True
                except:
                    pass
        if NewFieldsAdded:
            print 'Saving Record'
            if OldRecExists:
                CurrentRec.Active = False
                CurrentRec.save()
            if SubjectName:
                NewRecord=ExtentionData(ExtentionRecord=ExtentionRecords.objects.get(Name=ExtentionName),
                                        BaseType=self.BaseType,
                                        BaseId=self.BaseId,
                                        Subject=Subject.objects.get(Name=SubjectName),
                                        UpdatedOn=datetime.datetime.now(),
                                        Data=FieldData)
                NewRecord.save()
            else:
                NewRecord=ExtentionData(ExtentionRecord=ExtentionRecords.objects.get(Name=ExtentionName),
                                        BaseType=self.BaseType,
                                        BaseId=self.BaseId,
                                        Subject=Subject.objects.get(Name='Form_Comment'),
                                        UpdatedOn=datetime.datetime.now(),
                                        Data=FieldData)
                NewRecord.save()
            if self.request:
                NewRecord.UpdatedBy = self.request.session['StaffId']
                NewRecord.save()
        return


    def ReadExtention(self,ExtentionName,SubjectName=None):
        ''' This methon if passed and Extention Name and Subject will return
        a completed dictionary with each key being the the field name and the
        value being a list where element 0 is the data and Element 1 is the #
        alternative value. If there is no alternative value, the a blank is
        returned'''
        OldRecExists = True
        try:
            ExtRec = ExtentionRecords.objects.get(Name=ExtentionName)
        except:
            return False
        try:
            if SubjectName:
                CurrentRec = ExtentionData.objects.get(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Subject__Name=SubjectName,
                                                       Active=True)
            else:
                CurrentRec = ExtentionData.objects.get(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Active=True)
        except MultipleObjectsReturned:
            if SubjectName:
                BadRecs = ExtentionData.objects.filter(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Subject__Name=SubjectName,
                                                       Active=True)
                for recs in BadRecs:
                    CurrentRec = recs
                    CurrentRec.Active = False
                    CurrentRec.save()
                CurrentRec.Active = True
                CurrentRec.save()
            else:
                BadRecs = ExtentionData.objects.filter(ExtentionRecord__Name=ExtentionName,
                                                    BaseType=self.BaseType,
                                                    BaseId=self.BaseId,
                                                    Active=True)
                for recs in BadRecs:
                    CurrentRec = recs
                    CurrentRec.Active = False
                    CurrentRec.save()
                CurrentRec.Active = True
                CurrentRec.save()
            return self.ReadExtention(ExtentionName, SubjectName)
        except ObjectDoesNotExist:
            OldRecExists = False
        if OldRecExists:
            FieldData = CurrentRec.Data
        else:
            FieldData = {}
        for fields in ExtRec.Fields.all():
            if fields.Name in FieldData:
                FieldData[fields.Name].append(ExtRec.GetPickList(fields.Name))
            else:
                FieldData[fields.Name]=['', '', ExtRec.GetPickList(fields.Name)]
        return FieldData

    def ReadExtentionSimple(self,ExtentionName,SubjectName=None):
        ''' This methon if passed and Extention Name and Subject will return
        a completed dictionary with each key being the the field name and the
        value being a list where element 0 is the data and Element 1 is the #
        alternative value. If there is no alternative value, the a blank is
        returned'''
        OldRecExists = True
        try:
            ExtRec = ExtentionRecords.objects.get(Name=ExtentionName)
        except:
            return False
        try:
            if SubjectName:
                CurrentRec = ExtentionData.objects.get(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Subject__Name=SubjectName,
                                                       Active=True)
            else:
                CurrentRec = ExtentionData.objects.get(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Active=True)
        except MultipleObjectsReturned:
            if SubjectName:
                BadRecs = ExtentionData.objects.get(ExtentionRecord__Name=ExtentionName,
                                                    BaseType=self.BaseType,
                                                    BaseId=self.BaseId,
                                                    Subject__Name=SubjectName,
                                                    Active=True)
                for recs in BadRecs:
                    CurrentRec = recs
                    CurrentRec.Active = False
                    CurrentRec.save()
                CurrentRec.Active = True
                CurrentRec.save()
            else:
                BadRecs = ExtentionData.objects.get(ExtentionRecord__Name=ExtentionName,
                                                    BaseType=self.BaseType,
                                                    BaseId=self.BaseId,
                                                    Active=True)
                for recs in BadRecs:
                    CurrentRec = recs
                    CurrentRec.Active = False
                    CurrentRec.save()
                CurrentRec.Active = True
                CurrentRec.save()
            return self.ReadExtention(ExtentionName, SubjectName)
        except ObjectDoesNotExist:
            OldRecExists = False
        if OldRecExists:
            FieldData = CurrentRec.Data
        else:
            FieldData = {}
        return FieldData


    def ReadExtentionAllSubjects(self,ExtentionName):
        ''' This does a read of all files for a given Exention name,
        no matter the subject
        It returns a Dictionary of dictionaries as for ReadExtention,
        with the Key
        being the subject name. It returns only created records
        '''
        CurrentRecs = ExtentionData.objects.filter(ExtentionRecordExtRec,
                                                   BaseType=self.BaseType,
                                                   BaseId=self.BaseId,
                                                   Active=True)
        returndata = {}
        for recs in CurrentRecs:
            try:
                returndata[recs.Subject.Name]=recs.Data
            except:
                pass
        return returndata


class ExtendedRecordCSV(ExtendedRecord):
    def WriteExtention(self, ExtentionName, FieldDataNew, SubjectName=None):
        '''Writes/Updates data in the extentions system.
        Usage

        Create the object with BaseType and Id
        Newobject=ExtendedRecord(request,BaseType='Pupil',BaseId=4)
        Then write data
        NewObject.WriteExtention(ExtentionName='Yr3_EndofTerm_Mich',
        ExtentionData={#Field:Data},SubjectName=#Optional)
        '''
        # get ExtentionField Record
        OldRecExists = True
        try:
            ExtRec = ExtentionRecords.objects.get(Name=ExtentionName)
        except:
            return False
        try:
            if SubjectName:
                CurrentRec = ExtentionData.objects.get(ExtentionRecord=ExtRec,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Subject__Name=SubjectName,
                                                       Active=True)
            else:
                CurrentRec = ExtentionData.objects.get(ExtentionRecord=ExtRec,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Active=True)
        except MultipleObjectsReturned:
            if SubjectName:
                BadRecs = ExtentionData.objects.filter(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Subject__Name=SubjectName,
                                                       Active=True)
                for recs in BadRecs:
                    CurrentRec = recs
                    CurrentRec.Active = False
                    CurrentRec.save()
                CurrentRec.Active = True
                CurrentRec.save()
            else:
                BadRecs = ExtentionData.objects.filter(ExtentionRecord__Name=ExtentionName,
                                                       BaseType=self.BaseType,
                                                       BaseId=self.BaseId,
                                                       Active=True)
                for recs in BadRecs:
                    CurrentRec = recs
                    CurrentRec.Active = False
                    CurrentRec.save()
                CurrentRec.Active = True
                CurrentRec.save()
            return self.WriteExtention(ExtentionName,
                                       FieldDataNew,
                                       SubjectName)
        except ObjectDoesNotExist:
            OldRecExists = False
        if OldRecExists:
            FieldData = CurrentRec.Data
        else:
            FieldData = {}
        NewFieldsAdded = False
        for NewKey, NewData in FieldDataNew.items():
            if ExtRec.FieldExists(NewKey):
                try:
                    FieldData[NewKey] = [NewData,
                                        ExtRec.GetPickListValue(NewKey, NewData)]
                    NewFieldsAdded = True
                except:
                    pass
        if NewFieldsAdded:
            if OldRecExists:
                # This here is the extra code for this class
                # Basically its reads the old data record and if there is data in there already
                # it overwrites the new input data. Thus staff cannot add in data more than once
                olddata=CurrentRec.Data
                for fieldname, fieldvalue in olddata.items():
                    if len(fieldvalue[0]) > 0:
                        FieldData[filename]=fieldvalue
                CurrentRec.Active = False
                CurrentRec.save()
            if SubjectName:
                NewRecord=ExtentionData(ExtentionRecord=ExtentionRecords.objects.get(Name=ExtentionName),
                                        BaseType=self.BaseType,
                                        BaseId=self.BaseId,
                                        Subject=Subject.objects.get(Name=SubjectName),
                                        UpdatedOn=datetime.datetime.now(),
                                        Data=FieldData)
                NewRecord.save()
            else:
                NewRecord=ExtentionData(ExtentionRecord=ExtentionRecords.objects.get(Name=ExtentionName),
                                        BaseType=self.BaseType,
                                        BaseId=self.BaseId,
                                        Subject=Subject.objects.get(Name='Form_Comment'),
                                        UpdatedOn=datetime.datetime.now(),
                                        Data=FieldData)
                NewRecord.save()
            if self.request:
                NewRecord.UpdatedBy = self.request.session['StaffId']
                NewRecord.save()
        return

