# -*- coding: utf-8 -*-
"""
Created on Mon Feb 10 15:05:31 2014

@author: marc
"""
import sys, os
sys.path.append('/home/login/Dropbox/Projects/')
sys.path.append('/home/login/Dropbox/Projects/WillowTree/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings
import unittest
from django.test import Client

class unittest1(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        client=Client()
        client.login(username='mnealer',password='one two three')
    def test1a(self):
        client=Client()
        result=client.get('/login/')
        self.assertEqual(result.status_code,200)
    def test1b(self):
        client=Client()
        result=client.post('/BatterseaAdmin/Authenticated/',{'username':'mnealer','password':'one two three'})
        self.assertEqual(result.status_code,302)
if __name__ =='__main__':
    unittest.main()
