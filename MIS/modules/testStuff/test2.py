# unit test imports
import sys
import os
sys.path.append('/home/login/Dropbox/Projects/')
sys.path.append('/home/login/Dropbox/Projects/WillowTree/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings
import unittest
from django.test import Client

# Django database imports
from MIS.modules.GeneralFunctions import Get_PupilList
from MIS.models import SystemVarData
from MIS.models import Group
from MIS.models import PupilGroup
from MIS.models import Pupil
from MIS.modules.SpecialGroups.SpecialGroupsFunctions import GetSpecialGroups

# Setting up test environment
from django.test.utils import setup_test_environment
setup_test_environment()


class RoyTests(unittest.TestCase):
    """
    Django unittests using a mixture of the standard python unittest module and django unittest module. database entries
    are writen to the database. Remember to delete these afterwards in the tear down
    """
    def setUp(self):
        self.ac_year = SystemVarData.objects.get(Variable__Name='CurrentYear')
        self.client = Client()
        self.client.login(username='royroy21', password='football101')
        self.client.get('/login/')

    def test_special_group_admin_page(self):
        """
        from SpecialGroupsViews tests special_group_admin_page
        """
        response = self.client.get('/BatterseaAdmin/%s/special_group_admin_page/' % self.ac_year)
        self.assertEqual(response.status_code, 200)

    def test_create_new_valid_special_group(self):
        """
        from SpecialGroupsViews tests create_new_special_group
        """
        self.create_new_valid_special_group(perform_test=True)

    def create_new_valid_special_group(self, perform_test=False):
        post_data = {'special_group_name': 'test459 football group',
                     'school': 'BatterseaAdmin'}
        response = self.client.post('/BatterseaAdmin/%s/create_new_special_group/' % self.ac_year, post_data)
        if perform_test:
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.context['operation_success'], True)
            self.assertEqual(len(Group.objects.filter(Name='Special.Battersea.test459_football_group')) == 1,
                             True)

    def test_get_special_groups(self):
        """
        Tests the GetSpecialGroups class
        """
        self.create_new_valid_special_group()
        pupil_group = PupilGroup(AcademicYear=self.ac_year.Data,
                                 Pupil=Pupil.objects.all()[5],
                                 Group=Group.objects.get(Name='Special.Battersea.test459_football_group'))
        pupil_group.save()
        for group in GetSpecialGroups('BatterseaAdmin').get_special_groups():
            if group[0] == "Special.Battersea.test459_football_group":
                self.assertEqual(group[0] == "Special.Battersea.test459_football_group", True)
                self.assertEqual(group[1], True)

    def test_do_not_create_new_invalid_special_group(self):
        """
        from SpecialGroupsViews tests if groups with invalid group names are not created via the
        create_new_special_group view
        """
        check_words = ['roy',
                       'Marc',
                       'year',
                       'my',
                       'january',
                       'february',
                       'March',
                       'april',
                       'may',
                       'june',
                       'July',
                       'august',
                       'september',
                       'october',
                       'november',
                       'december']
        ac_year_list = self.ac_year.Data.split('-')
        check_words.append(ac_year_list[0])
        check_words.append(ac_year_list[1])
        for check_word in check_words:
            post_data = {'special_group_name': 'test459 %s football group' % check_word,
                         'school': 'BatterseaAdmin'}
            response = self.client.post('/BatterseaAdmin/%s/create_new_special_group/' % self.ac_year, post_data)
            self.assertEqual(response.status_code, 200)
            self.assertEqual(response.context['operation_success'], False)
            error_message = 'error: %s is not allowed in special group name' % check_word
            self.assertEqual(response.context['error_message'] == error_message, True)
            group_name = 'Special.Battersea.test459_%s_football_group' % check_word
            self.assertEqual(len(Group.objects.filter(Name=group_name)) == 0, True)

    def test_if_system_will_create_not_duplicate_special_groups(self):
        """
        Tests if system will not create duplicate special groups
        """
        self.create_new_valid_special_group()
        post_data = {'special_group_name': 'test459 football group',
                     'school': 'BatterseaAdmin'}
        response = self.client.post('/BatterseaAdmin/%s/create_new_special_group/' % self.ac_year, post_data)
        self.assertEqual('This group already exists' in response.context['error_message'], True)
        self.assertEqual(response.context['operation_success'], False)

    def test_pupils_add_to_a_special_group_then_remove(self):
        """
        Adds then removes a class of pupils to special group
        """
        test_special_group = "Special.Battersea.test459_football_group"
        school_group = "Current.Battersea.LowerSchool.Year2.Form.2BN"
        target_add_url = "/BatterseaAdmin/%s/special_groups_add_pupils/%s/"
        target_remove_url = "/BatterseaAdmin/%s/special_groups_remove_pupils/%s/"
        self.create_new_valid_special_group()
        pupil_ids = [i.id for i in Get_PupilList(self.ac_year.Data, school_group)]
        post_data = {'special_group_name': test_special_group,
                     'PupilSelect': pupil_ids,
                     'ac_year': self.ac_year.Data}

        # adding pupils
        response = self.client.post(target_add_url % (self.ac_year.Data, school_group), post_data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(PupilGroup.objects.filter(Group__Name=test_special_group,
                                                       Active=True)) == len(pupil_ids), True)
        self.assertEqual(PupilGroup.objects.filter(Group__Name=test_special_group,
                                                   Active=True)[0].Pupil.id in pupil_ids, True)

        # removing pupils
        response = self.client.post(target_remove_url % (self.ac_year.Data, school_group), post_data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(PupilGroup.objects.filter(Group__Name=test_special_group,
                                                       Active=True)) == 0, True)

    def test_deactivating_a_special_group(self):
        """
        This tests the deactivating of a special group. deactivating means removing all pupils from a special.
        """
        test_special_group = "Special.Battersea.test459_football_group"
        school_group = "Current.Battersea.LowerSchool.Year2.Form.2BN"
        target_url = "/BatterseaAdmin/%s/special_groups_deactivate/"
        self.create_new_valid_special_group()
        pupil_ids = [i.id for i in Get_PupilList(self.ac_year.Data, school_group)]
        post_data = {'special_group_name': test_special_group,
                     'PupilSelect': pupil_ids,
                     'ac_year': self.ac_year.Data}
        response = self.client.post(target_url % self.ac_year.Data, post_data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(PupilGroup.objects.filter(Group__Name=test_special_group,
                                                       Active=True)) == 0, True)

    def test_removes_members_from_a_special_group_then_deletes_special_group(self):
        """
        Creates and populates a special group then removes members then deletes group
        """
        self.create_new_valid_special_group()

        # creates and populates a special group
        test_special_group = "Special.Battersea.test459_football_group"
        school_group = "Current.Battersea.LowerSchool.Year2.Form.2BN"
        target_add_url = "/BatterseaAdmin/%s/special_groups_add_pupils/%s/"
        self.create_new_valid_special_group()
        pupil_ids = [i.id for i in Get_PupilList(self.ac_year.Data, school_group)]
        post_data = {'special_group_name': test_special_group,
                     'PupilSelect': pupil_ids,
                     'ac_year': self.ac_year.Data}

        # adding pupils
        response = self.client.post(target_add_url % (self.ac_year.Data, school_group), post_data)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(len(PupilGroup.objects.filter(Group__Name=test_special_group,
                                                       Active=True)) == len(pupil_ids), True)
        self.assertEqual(PupilGroup.objects.filter(Group__Name=test_special_group,
                                                   Active=True)[0].Pupil.id in pupil_ids, True)

        # removes members from a group
        test_special_group = "Special.Battersea.test459_football_group"
        target_removes_members_url = "/BatterseaAdmin/%s/special_groups_remove_all_members/"
        post_data = {'special_group_name': test_special_group,
                     'ac_year': self.ac_year.Data}
        response = self.client.post(target_removes_members_url % self.ac_year.Data, post_data)
        self.assertEqual(response.status_code, 302)
        pupil_groups = PupilGroup.objects.filter(Group__Name=test_special_group, Active=True)

        for i in pupil_groups:
            print i, i.Group.Name, i.Active

        self.assertEqual(len(pupil_groups) == 0, True)

        # deletes the special group
        test_special_group = "Special.Battersea.test459_football_group"
        target_removes_members_url = "/BatterseaAdmin/%s/special_groups_delete_group/"
        post_data = {'special_group_name': test_special_group,
                     'ac_year': self.ac_year.Data}
        response = self.client.post(target_removes_members_url % self.ac_year.Data, post_data)
        self.assertEqual(response.status_code, 302)
        test_special_group = Group.objects.filter(Name=test_special_group)
        self.assertEqual(len(test_special_group) == 0, True)

    def tearDown(self):
        group_name = 'Special.Battersea.test459_football_group'
        if len(Group.objects.filter(Name__icontains=group_name)) == 1:
            group1 = Group.objects.get(Name__icontains=group_name)
            group1.delete()
        if len(PupilGroup.objects.filter(Group__Name=group_name)) > 0:
            for pupil_group in PupilGroup.objects.filter(Group__Name=group_name):
                pupil_group_object = pupil_group
                pupil_group_object.delete()


if __name__ == '__main__':
    unittest.main()