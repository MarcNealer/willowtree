from MIS.models import *
import csv

class findMissingUsers():

    def __init__(self,Mailfile,OliverFile,AcYear):
        self.Mailfile=Mailfile
        self.Oliverfile=OliverFile
        self.AcYear=AcYear
        self.__ReadFiles()
        self.pupilrecords=set([x.Pupil for x in PupilGroup.objects.filter(Group__Name__istartswith='Current',
                                                                          Active=True,
                                                                          AcademicYear=self.AcYear).filter(Group__Name__icontains='Form')])
        self.outMailMissing=[]
        self.outMailWrong=[]
        self.outOliverMissing=[]
        self.outOliverWrong=[]

    def checkRecords(self):
        for items in self.pupilrecords:
            self.isPupilInMailList(items)
            self.isPupilInOliverList(items)
        self.__writeFiles()

    def __ReadFiles(self):
        mailImport=csv.DictReader(open(self.Mailfile,'rb'))
        self.MailData=[]
        for items in mailImport:
            self.MailData.append(items)
        oliverImport=csv.DictReader(open(self.Oliverfile,'rb'))
        self.OliverData=[]
        for items in oliverImport:
            if len(items['EmailAddress']) > 5:
                self.OliverData.append(items)
        return

    def isPupilInMailList(self,PupilRec):
        print 'check mail'
        find=False
        try:
            for items in self.MailData:
                if PupilRec.EmailAddress.lower() in items['Email'].lower():
                    find=True
                    if not self.__isTheRecordCorrect(items['Surname'],items['Forename'],PupilRec):
                        self.outOliverWrong.append(items)
                    break
            if not find:
                self.outMailMissing.append([PupilRec.EmailAddress,PupilRec.EmailAddress.split('@')[0],
                                            PupilRec.EmailAddress.split('@')[1],PupilRec.FullName().encode('utf-8')])
        except Exception as Error:
            print Error

    def isPupilInOliverList(self,PupilRec):
        print 'check Oliver'
        find=False
        try:
            for items in self.OliverData:
                if PupilRec.EmailAddress.lower() in items['EmailAddress'].lower():
                    find=True
                    if not self.__isTheRecordCorrect(items['Surname'],items['Forename'],PupilRec) and not self.__isTheRecordCorrect(items['Forename'],items['Surname'],PupilRec):
                        self.outOliverWrong.append(items['StudentCode'],Items['EmailAddress'],items['Surname'],items['Forename'])
                    break
            if not find:
                self.outOliverMissing.append([PupilRec.EmailAddress.split('@')[0],
                                              PupilRec.FirstName().encode('utf-8'),PupilRec.Surname,
                                              'Student',PupilRec.EmailAddress,'Students',
                                              PupilRec.EmailAddress.split('@')[1].split('.')[0],
                                              self.getPupilYear(PupilRec),self.getPupilForm(PupilRec),
                                              '%s0%s' % (PupilRec.EmailAddress.split('@')[0][0:-1],PupilRec.EmailAddress.split('@')[0][-1]),
                                              PupilRec.Gender,PupilRec.DateOfBirth.strftime('%d/%m/%Y')])
        except Exception as Error:
            print Error

    def __isTheRecordCorrect(self,Surname,Forename, PupilRec):
        if Surname.lower() in PupilRec.Surname.lower() and Forename.lower in PupilRec.FirstName().lower():
            return True
        else:
            return False

    def __writeFiles(self):
        fileobj=open('OliverMissing.csv','wb')
        csvObj=csv.writer(fileobj)
        csvObj.writerows(self.outOliverMissing)
        fileobj.close()

        fileobj=open('OliverWrong.csv','wb')
        csvObj=csv.writer(fileobj)
        csvObj.writerows(self.outOliverWrong)
        fileobj.close()

        fileobj=open('MailMissing.csv','wb')
        csvObj=csv.writer(fileobj)
        csvObj.writerows(self.outMailMissing)
        fileobj.close()

        fileobj=open('MailWrong.csv','wb')
        csvObj=csv.writer(fileobj)
        csvObj.writerows(self.outMailWrong)
        fileobj.close()

    def getPupilForm(self,Pupil):
        try:
            record=PupilGroup.objects.filter(Pupil=Pupil,
                                             Group__Name__istartswith='Current',
                                             Active=True,
                                             AcademicYear=self.AcYear).filter(Group__Name__icontains='Form')
            found=False
            for items in record:
                if items.Group.Name.split('.')[-2]=='Form':
                    PupilForm=items.Group.Name.split('.')[-1]
                    found=True
                    break
            if found:
                return PupilForm
            else:
                return 'Unknown'
        except Exception as Error:
            print Error
            return 'Unknown'

    def getPupilYear(self,Pupil):
        form=self.getPupilForm(Pupil)
        if form[0] =='R':
            return 0
        else:
            return form[0]


