from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from FamilyRecord import FamilyRecord
from django.contrib.auth.models import User
from django.core.mail import send_mail, EmailMessage
import logging
log = logging.getLogger(__name__)

def AssignApplicantGroup(PupilId,GroupName,School,AcYear):
    ''' AssignApplicantGroup
    Function to assgin a pupil to a selected Applicant group for a specified Academic Year.
    This function is used by the various import scripts
    Usage

    AssginGroup(PupilId,'GroupName','Academic Year')
    '''
    PupilRec=Pupil.objects.get(id=PupilId)
    print PupilId
    if len(GroupName) > 0 and Group.objects.filter(Name__iendswith=GroupName,Name__icontains=School,Name__startswith='Applicants').exists():
        if PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name__iendswith=GroupName,Name__icontains=School,Name__startswith='Applicants')).exists():
            OldGroup=PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name__iendswith=GroupName,Name__icontains=School,Name__startswith='Applicants'))
            for oldgroups in OldGroup:
                item=oldgroups
                item.Active=False
                item.DateInactivated=datetime.date.today()
                item.save()

        groupclass = PupilGroup(AcademicYear=AcYear,Pupil=Pupil.objects.get(id=PupilId),Group=Group.objects.get(Name__iendswith=GroupName,Name__istartswith='Applicants.%s' % School))
        groupclass.save()


    elif  Group.objects.filter(Name='Applicant').exists() and not PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name='Applicants')).exists():

        groupassign = PupilGroup(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name='Current'))
        groupassign.save()

def AssignGroup(PupilId,GroupName,School,AcYear):
    ''' AssignGroup
    Function to assgin a pupil to a selected group for a specified Academic Year.
    This function is used by the various import scripts
    Usage

    AssginGroup(PupilId,'GroupName','Academic Year')
    '''
    PupilRec=Pupil.objects.get(id=PupilId)
    print PupilId
    if len(GroupName) > 0 and Group.objects.filter(Name__iendswith=GroupName,Name__icontains=School,Name__startswith='Current').exists():
        if PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name__iendswith=GroupName,Name__icontains=School,Name__startswith='Current')).exists():
            OldGroup=PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name__iendswith=GroupName,Name__icontains=School,Name__startswith='Current'))
            for oldgroups in OldGroup:
                item=oldgroups
                item.Active=False
                item.DateInactivated=datetime.date.today()
                item.save()

        groupclass = PupilGroup(AcademicYear=AcYear,Pupil=Pupil.objects.get(id=PupilId),Group=Group.objects.get(Name__iendswith=GroupName,Name__istartswith='Current.%s' % School))
        groupclass.save()


    elif  Group.objects.filter(Name='Current').exists() and not PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name='Current')).exists():

        groupassign = PupilGroup(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name='Current'))
        groupassign.save()
    print 'test for Alumni'
    print PupilGroup.objects.filter(Group__Name__icontains='Alumni.%s' % School,Pupil=PupilRec).exists()
    if not PupilGroup.objects.filter(Group__Name__icontains='Alumni.%s' % School,Pupil=PupilRec).exists():
        print 'Adding Alumni Record'
        groupAlumni = PupilGroup(Group=Group.objects.get(Name='Alumni.%s' % School),Pupil=PupilRec,AcademicYear='Alumni')
        groupAlumni.save()

def ChildGroups(GroupName):
    ''' This function accepts a GroupName as a String and returns a set collection
    of the Childgroups for use in searching for database matches.

    Example 'Current.Battersea.Year1' will return
    ['Current.Battersea.Year1.1BE','Current.Battersea.Year1.1BN','Current.Battersea.Year1.1BW']
    '''
    Groups=set()
    GroupList=Group.objects.filter(Name__contains=GroupName)
    for groups in GroupList:
        if not groups.Name == GroupName:
            Groups.add(groups.Name)
    return Groups

def RemoveFromSubGroups(Groupid,PupilId,AcYear):
    ''' This function removed a child from the group passed as an argument, plus any child groups
    of the said group

    Usage

    RemoveFromSubGroups(Groupid,PupilId,AcYear)'''
    msg_return=[]
    GroupName=Group.objects.get(id=int(Groupid))
    Grouplist=Group.objects.filter(Name__contains=GroupName)
    for groups in Grouplist:
        if PupilGroup.objects.filter(AcademicYear=AcYear,
                                     Group=groups,
                                     Pupil=Pupil.objects.get(id=int(PupilId)),
                                     Active=True).exists():
            rec=PupilGroup.objects.get(AcademicYear=AcYear,
                                       Group=groups,
                                       Pupil=Pupil.objects.get(id=int(PupilId)),
                                       Active=True)
            rec.Active=False
            rec.save()

            msg_return.append(groups.Name)
    return msg_return

def RemoveFromCurrent(School,PupilId,AcYear):
    ''' This function inactivates a child in all current groups

    Usage

    RemoveFromSubGroups(school(menuname),PupilId,AcYear)'''
    msg_return=[]
    msg_return.append('Got here')
    SchoolName=MenuTypes.objects.get(Name=School).SchoolId.Name
    pupilrec=Pupil.objects.get(id=PupilId)
    if not PupilGroup.objects.filter(Group__Name__istartswith='Alumni.%s' % SchoolName,Pupil=pupilrec).exists():
        newrec=PupilGroup(AcademicYear='Alumni',Pupil=pupilrec,Group=Group.objects.get(Name='Alumni.%s' % SchoolName))
        newrec.save()

    GroupList=PupilGroup.objects.filter(AcademicYear=AcYear,
                                        Group__Name__istartswith='Current.%s' % SchoolName,
                                        Pupil=pupilrec,Active=True)
    for groups in GroupList:
        grouprec=groups
        grouprec.Active=False
        grouprec.save()

        msg_return.append(groups.Group.Name)
    return msg_return

def SubGroups(GroupName):
    ''' This function accepts a GroupName as a String and returns a set collection
    of the subgroups for use in searching for database matches.

    Example 'Current.Battersea.Year1.RBE' will return
    ['Current','Current.Battersea','Current.Battersea.Year1','Current.Battersea.Year1.RBE']
    '''
    Groups=set()
    for counter in range(0,len(GroupName.split('.'))):
        Groups.add('.'.join(GroupName.split('.')[0:counter]))
    return Groups

def ListPupilGroups(PupilId,AcYear=None):
    ''' Lists all the Groups a child belongs to

    Returned data is a set

    Usage
    ListPupilGroups(id,AcYear)
    '''
    GroupSet=set()
    if PupilGroup.objects.filter(AcademicYear='Holding',Pupil=PupilId,Active=True).exists():
        PupilGroupList=PupilGroup.objects.filter(AcademicYear='Holding',Pupil=PupilId,Active=True)
        for groups in PupilGroupList:
            GroupSet.add(groups.Group.Name)
    if PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilId,Active=True).exists():
        PupilGroupList=PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilId,Active=True)
        for groups in PupilGroupList:
            GroupSet.add(groups.Group.Name)
    if PupilGroup.objects.filter(AcademicYear='Alumni',Pupil=PupilId,Active=True).exists():
        PupilGroupList=PupilGroup.objects.filter(AcademicYear='Alumni',Pupil=PupilId,Active=True)
        for groups in PupilGroupList:
            GroupSet.add(groups.Group.Name)
    if PupilGroup.objects.filter(AcademicYear='',Pupil=PupilId,Active=True).exists():
        PupilGroupList=PupilGroup.objects.filter(AcademicYear='Alumni',Pupil=PupilId,Active=True)
        for groups in PupilGroupList:
            GroupSet.add(groups.Group.Name)
    if len(GroupSet) == 0:
        PupilGroupList=PupilGroup.objects.filter(Pupil=PupilId,Active=True)
        for groups in PupilGroupList:
            GroupSet.add(groups.Group.Name)
    return sorted(GroupSet)

def ListStaffGroups(StaffId,AcYear):
    ''' Lists all the Groups a child belongs to

    Returned data is in a set collection

    Usage:
        ListStaffGroup(id,AcYear)

    '''
    GroupSet=set()
    if StaffGroup.objects.filter(AcademicYear=AcYear,Staff=StaffId).exists():
        StaffGroupList=StaffGroup.objects.filter(AcademicYear=AcYear,Staff=StaffId)
        for groups in StaffGroupList:
            GroupSet.add(groups.Group.Name)
    return sorted(GroupSet)

def Get_PupilList(AcYear,GroupName=None,GroupNo=None):
    ''' Returns a list of pupils in a selected group

    if a Group Number is supplied, it will return a list of pupils
    explisitly assigned to that group.
    If a Group Name is supplied, it will return a list of pupils in this group
    and all sub groups

    Data is returned in a set collection

    Usage

    1) Get_PupilList(AcYear,GroupNo=**)
    2) Get_PupilList(AcYear,GroupName=********)
    '''
    PupilSet=set()
    if GroupName:
        PupilList=PupilGroup.objects.filter(Group__Name__icontains=GroupName,AcademicYear=AcYear,Active=True)
    else:
        PupilList=PupilGroup.objects.filter(Group__id=int(GroupNo),AcademicYear=AcYear,Active=True)
    for eachPupil in PupilList:
        PupilSet.add(eachPupil.Pupil)
    return sorted(PupilSet, key=lambda pupil: pupil.Surname.lower())


def Get_PupilListExact(AcYear,GroupName=None,GroupNo=None):
    ''' This function returns a list of pupils for a given groupname or number, but
    only children assigned directly to that given group
    PupilSet=set()
    Data is returned in a set collection

    Usage

    1) Get_PupilList(AcYear,GroupNo=**)
    2) Get_PupilList(AcYear,GroupName=********)
    '''
    PupilSet=set()
    if GroupName:
        PupilList=PupilGroup.objects.filter(Group__Name__iexact=GroupName,AcademicYear=AcYear,Active=True)
    else:
        PupilList=PupilGroup.objects.filter(Group__id=int(GroupNo),Active=True)
    for eachPupil in PupilList:
        PupilSet.add(eachPupil.Pupil)
    return sorted(PupilSet, key=lambda pupil: pupil.Surname.lower())

def Get_PupilSet(GroupDetail,PupilId,AcYear):
    ''' This function returns the set that a pupil beings to, from a given
    set manager.

    Usage: Get_PupilSet('Battersea.UpperSchool.Year8.Maths',12,'2012-2013')

    Returned data is a string containing the Menu name of the given set group

    '''

    GroupSet=ListPupilGroups(PupilId,AcYear)
    SetId=''
    for items in GroupSet:
        if items.find(GroupDetail) > -1:
            GroupName=Group.objects.get(Name__iexact=items)
            SetId=GroupName.MenuName
    return SetId

def Get_StaffList(AcYear,GroupName=None,GroupNo=None):
    ''' Returns a list of Staff objects for staff that belong to a selected
    staff group

    Usage: Get_StaffList('2012-2013','Battersea.Staff.UpperSchool.SLT')

    Returned data is a set collection of Staff objects'''
    StaffSet=set()
    if GroupName:
        StaffList=StaffGroup.objects.filter(Group__Name__icontains=GroupName,AcademicYear=AcYear)
    else:
        StaffList=StaffGroup.objects.filter(Group__id=int(GroupNo))
    for eachStaff in StaffList:
        StaffSet.add(eachStaff.Staff)
    return StaffSet


def StaffInGroup(request,GroupId):
    ''' Tests to see if a staff member is part of a selected group.

    Usage StaffInGroup(request,GroupId)

    Returns Boolean
    '''
    Found=False
    for Groups in request.session['StaffGroups']:
        if Groups.Name.find(GroupId.Name) > -1:
            Found=True
    if request.user.is_superuser:
        Found=True
    return Found



def PupilStatus(Pupil_Id,AcYear):
    ''' returns current, applicant, or alumni, plus school'''
    if PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name__istartswith='Current',AcademicYear=AcYear).exists():
        PupilStatus=PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name__istartswith='Current',AcademicYear=AcYear)
        return '.'.join(PupilStatus[len(PupilStatus)-1].Group.Name.split('.')[0:2])
    elif PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name_istartswith='Applicant',AcademicYear=AcYear).exists():
        PupilStatus=PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name__istartswith='Applicant',AcademicYear=AcYear)
        return '.'.join(PupilStatus[len(PupilStatus)-1].Group.Name.split('.')[0:2])
    elif PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name_istartswith='Alumni',AcademicYear=AcYear).exists():
        PupilStatus=PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name__istartswith='Alumni',AcademicYear=AcYear)
        return '.'.join(PupilStatus[len(PupilStatus)-1].Group.Name.split('.')[0:2])
    else:
        return 'Unknown'


def FamiliesInAGroup(request,AcYear,Group_Id=None,PupilList=None):
    ''' This method takes a list of pupils as either a GroupId, or a straight list
    and return a list of Familiy records. The Group_Id has to be specified, but the
    pupil list only if this is to be a subset of that group'''
    if PupilList:
        FamilyConnections=FamilyChildren.objects.filter(Pupil__in=PupilList)
    else:
        FamilyConnections=FamilyChildren.objects.filter(Pupil__in=Get_PupilList(AcYear,GroupName=Group.objects.get(id=int(Group_Id)).Name))
    FamilyIdSet=set([x.FamilyId.id for x in FamilyConnections])
    FamilyList=[]
    for records in FamilyIdSet:
        if PupilList:
            FamilyList.append(FamilyRecord(request,AcYear,FamilyId=records,PupilList=PupilList))
        else:
            FamilyList.append(FamilyRecord(request,AcYear,FamilyId=records,GroupId=Group_Id))

    FamilyList = sorted(FamilyList, key=lambda k: k.FamilyId.FamilyName.lower())
    return FamilyList


def getUserEmailAddress(request):
    ''' gets a users email address if it exists, returns an error message
    if none exists '''
    userObj = User.objects.get(id=int(request.session.get('_auth_user_id')))
    if userObj.email:
        userEmail = userObj.email
    else:
        staffObj = Staff.objects.get(id=int(request.session['StaffId'].id))
        try:
            userEmail = staffObj.Address.EmailAddress
        except:
            userEmail = "Email address for you is not present in Willow Tree"
    return userEmail


def isUserInThisGroup(request, group):
    ''' pass a group, for example "SEN" and this function will return bool
    that signalifies if they are in the group or not '''
    result = False
    staffId = int(request.session['StaffId'].id)
    staffGroupsObj = StaffGroup.objects.filter(Staff__id=staffId)
    for i in staffGroupsObj:
        if group in i.Group.Name:
            result = True
    return result


class SchoolGroupsList():
    ''' a class to return groups that are for the currently viewed school
    only'''
    def __init__(self,MenuName):
        self.GroupList=Group.objects.filter(Name__icontains=MenuTypes.objects.get(Name=MenuName).SchoolId.Name)
    def Test(self):
        return 'test test test'
    def All(self):
        return self.GroupList
    def PupilGroups(self):
        return self.GroupList.exclude(Name__istartswith='Staff')
    def StaffGroups(self):
        return self.GroupList.filter(Name__istartswith='Staff')
    def CurrentGroups(self):
        return self.GroupList.filter(Name__istartswith='Current')
    def ApplicantGroups(self):
        return self.GroupList.filter(Name__istartswith='Applicants')
    def AlumniGroups(self):
        return self.GroupList.filter(Name__istartswith='Alumni')


class ConfigParser():
    ''' Override class used for reading MS ini style files. The major difference
    is that this class keeps case sensitivity'''
    def __init__(self,FileName):
        parmfile=open(FileName,'rb')
        self.Parmdata={}
        Section=None
        for lines in parmfile.readlines():
            if lines.find('[') > -1 and lines.find(']') > -1 and lines.find('=') < 0:
                Section=lines.split('[')[1].split(']')[0]
                self.Parmdata[Section]={}
            elif Section is not None and lines.find('=') > -1:
                pname,pdata=lines.split('=',1)
                if pdata.find('\r') > -1:
                    pdata=pdata.split('\r')[0]
                elif pdata.find('\r\n') > -1:
                    pdata=pdata.split('\r\n')[0]
                else:
                    pdata=pdata.split('\n')[0]
                self.Parmdata[Section][pname]=pdata
    def items(self,SectionName):
        if self.Parmdata.has_key(SectionName):
            return self.Parmdata[SectionName]
    def get(self,SectionName,ItemName):
        if self.Parmdata.has_key(SectionName):
            if self.Parmdata[SectionName].has_key(ItemName):
                return self.Parmdata[SectionName][ItemName]

class RelationshipTypes():
    ''' Reads realtionship types from the FamilyRealtionships table. Different
    types are passed back according to which method is called'''
    def __init__(self):
        self.Records=FamilyRelationship.objects.all()
    def Adult(self):
        ''' Returns a list of adult relationship types'''
        return self.Records.filter(RelType='A')
    def Child(self):
        ''' returns a list if child relationship types'''
        return self.Records.filter(RelType='C')

class SchoolGroups():
    '''Class to gather all the Pupil groups for a selected school. This class
    is used for selected pages to give lists of current and applicant groups
    for group selection. It's used in such areas as moving or copying
    pupils to group.

    Results in all cases are returned as query sets of group objects.
    '''
    def __init__(self,school):
        self.School=MenuTypes.objects.get(Name=school).SchoolId
        self.GroupList=Group.objects.filter(Name__icontains=self.School.Name)
    def All(self):
        ''' returns a query set of all groups for the selected school'''
        return self.GroupList
    def Current(self):
        ''' returns all Current groups for a selected school'''
        return self.GroupList.filter(Name__istartswith='Current')
    def Applicants(self):
        ''' Returns a list of all applicant groups for a selected school'''
        return self.GroupList.filter(Name__istartswith='Applicants')

class GetAcHousesInSchool():
    """ Returns a list of academic house for a school, beware this will return a
    full name for the group. for example: Current.Battersea.AcHouse.House.Lawrence"""
    def __init__(self,school):
        self.school = school
        self.AcHouseList = []
        self.grps = Group.objects.filter(Name__contains=MenuTypes.objects.get(Name=self.school).SchoolId.Name)
        self.grps = self.grps.filter(Name__contains="House.House.")
        for i in self.grps:
            self.AcHouseList.append(str(i))
    def getHouses(self):
        return self.AcHouseList

class GetFormsInSchool():
    """ Returns a list of forms for a school, beware this will return a
    full name for the group. for example: Current.Battersea.UpperSchool.Year8.Form.8BS"""
    def __init__(self,school):
        self.school = school
        self.forms = []
        self.grps = Group.objects.filter(Name__contains=MenuTypes.objects.get(Name=self.school).SchoolId.Name)
        self.grps = self.grps.filter(Name__contains="Form.").exclude(Name__icontains='BalGames')
        for i in self.grps:
            self.forms.append(str(i))
    def getForms(self):
        return self.forms
def GetYearsAndForms(school):
    school = school
    ClassYears={}
    grps = Group.objects.filter(Name__contains=MenuTypes.objects.get(Name=school).SchoolId.Name).filter(Name__istartswith='Current')
    for groups in grps:
        test=groups.Name.split('.')
        if len(test)>3:
            if not test[3] in ClassYears:
                ClassYears[test[3]]={'Group':groups}
                clsgroups=grps.filter(Name__icontains=test[3]).filter(Name__icontains='Form.').exclude(Name__icontains='BalGames')
                Counter=1
                for items in clsgroups:
                    ClassYears[test[3]]['Class%d' % Counter] =items
                    Counter+=1
    return ClassYears


def GetParentGroup(GroupName):
    return '.'.join(GroupName.split('.')[:-1])

def GetSchoolName(Menuname):
    School=MenuTypes.objects.get(Name=Menuname).SchoolId
    return School.Name

def Get_PupilHouse(PupilId):
    ''' Return the house a child belongs to'''
    Grouplist=PupilGroup.objects.filter(Pupil__id=PupilId,Group__Name__icontains='House',Active=True)
    HouseName=''
    for groups in Grouplist:
        HouseName=groups.Group.MenuName
    return HouseName

    return SetId

class TemplateCounter():
    ''' Simple counter to be used in templates to count items produced in
    nested for loops'''
    def __init__(self):
        self.Counter=0
    def increment(self):
        self.Counter+=1
        return ''
    def decrement(self):
        self.Counter-=1
        return ''
    def getCount(self):
        return self.Counter

def GetForms(school):
    ''' Returns a list of forms for a given school menu name'''
    schoolname=MenuTypes.objects.get(Name=school).SchoolId.Name
    grouplist=Group.objects.filter(Name__icontains=schoolname).filter(Name__icontains='Current').filter(Name__icontains='Form').exclude(Name__icontains='Games').exclude(Name__icontains='Balletls').exclude(Name__iendswith='Form')
    return grouplist

def GetPupilInSchool(SchoolName,AcYear):
    school=GetSchoolName(SchoolName)
    pupillist=set([x.Pupil.id for x in PupilGroup.objects.filter(Group__Name__icontains=school,AcademicYear=AcYear,Active=True).filter(Group__Name__istartswith='Current')])
    return list(pupillist)

def ReadFormDate(FormDate):
    return datetime.datetime.strptime(FormDate,'%Y-%m-%d')

def SendEmailWithAttachment(UserRec,Header, BodyText ,AttachmentObj,AttachmentType, Attachmentfilename):
    log.warn('SendEmail reached')
    if UserRec.EmailAddress:
        SenderEmail=UserRec.EmailAddress
        msg=EmailMessage(Header,
                         BodyText,
                         SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data,
                        [SenderEmail])
        msg.attach(Attachmentfilename,AttachmentObj,AttachmentType)
        msg.send()
        log.warn('Email sent')

def getPupilSchoolClassGrps(school):
    ''' returns pupil class grps for a school only if the group has members '''
    listOfGroups = []
    schoolName = MenuTypes.objects.get(Name=school).SchoolId.Name
    for i in Group.objects.filter(Name__icontains=schoolName).filter(Name__icontains='Current').filter(Name__icontains='Form'):
        temp = i.Name
        temp = temp.split('.')
        membersTest = i.pupilgroup_set
        if len(temp) == 6 and len(membersTest.all()) != 0:
            listOfGroups.append(i)
    return listOfGroups


class templateCounter():
    '''
    this class adds a counter class that can be used in templates
    obj.Counter returns the counter number
    obj.AddToCounter adds 1 to the counter
    obj.RemoveFromCounter subtracts 1 form the counter
    '''
    def __init__(self):
        self.Counter = 0

    def AddToCounter(self):
        self.Counter += 1

    def RemoveFromCounter(self):
        self.Counter -= 1

    def resetCounter(self):
        self.Counter = 0