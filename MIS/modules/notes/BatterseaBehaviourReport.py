# Python Imports
import datetime

# Django imports
from django.core.mail import send_mail
from django.db.models import Q

# WillowTree Imports
from MIS.models import PupilNotes, PupilGroup, StaffGroup, SystemVarData
from MIS.modules.globalVariables import GlobalVariablesFunctions

# logging
import logging
log = logging.getLogger(__name__)


class BatterseaBehaviourReport():
    """

    This class sends emails to staff if a certain number of behavior related notes are raised against a child during a
    term. The class accepts a config dictionary (see below) as settings.

    example of config_dictionary:

    config_dictionary = {'behaviour_keywords': ['Conduct','Detention'],
                         'groups': (('Staff.Battersea.Special.MS_PASTORAL', [3,4,5]),
                                   ('Staff.Battersea.Special.MS_HOMS_PASTORAL', [6,7,8]),
                                   ('Staff.Battersea.Special.MS_LT_PASTORAL', [9]),
                                   ('Staff.Battersea.Special.US_PASTORAL', [3,4,5]),
                                   ('Staff.Battersea.Special.US_HOMS_PASTORAL', [6,7,8]),
                                   ('Staff.Battersea.Special.US_LT_PASTORAL', [9]))}

    The numbers coming after the group names are number of notes. For example the Staff.Battersea.Special.MS_PASTORAL
    group will be sent an email informing them of behavior related issues for a pupil if they get 3,4 or 5 behavior
    related notes. Specified by the behaviour_keywords dictionary list.

    """
    def __init__(self, config_dictionary):
        self.ac_year = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
        self.behaviour_keywords = config_dictionary['behaviour_keywords']
        self.groups = config_dictionary['groups']
        self.today_date = datetime.datetime.now().date().isoformat()
        self.this_month = datetime.datetime.now().month
        self.this_year = datetime.datetime.now().year

        # get start and end dates for note filtering
        start_and_end_dates = self.__find_dates__()
        self.start_date = start_and_end_dates['start_date']
        self.end_date = start_and_end_dates['end_date']
        self.pupil_notes = self.__get_pupil_notes__()

        # create messages to bge emails dictionary
        self.messages = {}
        for group in self.groups:
            self.messages.update({group[0]: []})

    def main_logic(self):
        """
        Works out if emails need to be sent and to which staff in which groups.
        """
        send_email = False
        for note in self.pupil_notes:
            # check if a note was raised today for a pupil. If true continue with operation and send email
            if self.__check_if_note_was_raised_today__(note.PupilId.id):

                # check what group pupil is in
                form_groups = PupilGroup.objects.filter(Group__Name__icontains=".Form.",
                                                        Pupil__id=note.PupilId.id,
                                                        Active=True,
                                                        AcademicYear=self.ac_year)
                form_group = form_groups[0]

                # check if the pupil is in lower, middle or upper school
                lower_middle_upper = None
                for i in ['LowerSchool', 'MiddleSchool', 'UpperSchool']:
                    if i in form_group.Group.Name:
                        lower_middle_upper = ".%sS_" % i[0]

                # check how many behaviour notes pupil has in query
                number_of_notes = len(self.pupil_notes.filter(PupilId__id=note.PupilId.id))

                # email which group logic
                for group in self.groups:
                    if number_of_notes in group[1]:
                        if lower_middle_upper in group[0]:
                            message_to_send = "%s has %s behaviour related notes this term." % (note.PupilId.FullName(),
                                                                                                number_of_notes)
                            if message_to_send not in self.messages[group[0]]:
                                send_email = True
                                self.messages[group[0]].append(message_to_send)
        # send emails
        if send_email:
            self.__send_emails__()
        return self.messages

    def __check_if_note_was_raised_today__(self, pupil_id):
        """
        check if a note was raised today for a pupil.
        """
        for note in self.pupil_notes.filter(PupilId__id=pupil_id):
            if note.NoteId.RaisedDate.date().isoformat() == self.today_date:
                return True

    def __find_dates__(self):
        """
        Finds the start and stop date for use in note searching.
        """
        mich = [9, 10, 11, 12]
        lent = [1, 2, 3]
        summer = [4, 5, 6, 7, 8]
        if self.this_month in mich:
            return {'start_date': '%s-09-01' % self.this_year,
                    'end_date': '%s-12-31' % self.this_year}
        if self.this_month in lent:
            return {'start_date': '%s-01-01' % self.this_year,
                    'end_date': '%s-03-30' % self.this_year}
        if self.this_month in summer:
            return {'start_date': '%s-04-01' % self.this_year,
                    'end_date': '%s-08-31' % self.this_year}

    def __get_pupil_notes__(self):
        """
        Gets all pupil notes for Battersea that match the date range and keywords from the config tuple.
        """
        notes = PupilNotes.objects.filter(NoteId__RaisedDate__range=[self.start_date, self.end_date], Active=True)
        for keyword in self.behaviour_keywords:
            notes = notes.filter(Q(NoteId__Keywords__icontains=keyword))
        return notes

    def __get_staff_emails__(self, group):
        """
        Get all staff emails linked with Battersea behaviour special groups.
        """
        return StaffGroup.objects.filter(Group__Name=group).distinct('Staff__EmailAddress').values_list("Staff__EmailAddress",
                                                                                                        flat=True)
    def __send_emails__(self):
        """
        Send emails.
        """
        for key, value in self.messages.iteritems():
            if value:
                for email_address in self.__get_staff_emails__(key):
                    try:
                        message = '\n'.join(value)
                        send_mail('Battersea Behaviour Report',
                                  message,
                                  SystemVarData.objects.get(Variable__Name='WillowTree Email Address').Data,
                                  [str(email_address)])
                        log.warn('Battersea Behaviour Report email sent to %s' % email_address)
                    except Exception as error:
                        log.warn('Failed! Battersea Behaviour Report email sent to %s (%s)' % (email_address, error))
        return