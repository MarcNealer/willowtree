# unit test imports
import sys
import os
sys.path.append('/home/ubuntu/sites/WillowTree')
#sys.path.append('/home/login/Dropbox/Projects/WillowTree/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings
import unittest
from django.test import Client

# Setting up test environment
from django.test.utils import setup_test_environment
setup_test_environment()

# Python imports
import json
import datetime

# Database imports
from MIS.models import *
from MIS.modules.PupilRecs import PupilRecord


class NotesExplorerTests(unittest.TestCase):
    def setUp(self):
        # client 1 exists with battersea admin access
        self.client1 = Client()
        self.client1.login(username='royroy21', password='football101')

    def create_notes_data_for_testing(self):
        """
        Creates notes for testing
        """
        # get all pupil ids for the group 'Current.Battersea.LowerSchool.Reception.Form.RBN'
        pupil_ids = PupilGroup.objects.filter(Group__Name='Current.Battersea.LowerSchool.Reception.Form.RBN',
                                              Active=True,
                                              AcademicYear='2014-2015').order_by('Pupil__id').distinct('Pupil__id').values_list('Pupil__id', flat=True)

        # create notes batch 1 for testing
        creation_date = '2014-09-02'
        for i in pupil_ids:
            test_note = NoteDetails(Keywords='Conduct:Detention:Pastoral',
                                    NoteText='TEST FOR NOTES EXPLORER - x712zx',
                                    RaisedBy=Staff.objects.get(Forename='Roy', Surname='Hanley'),
                                    ModifiedBy=Staff.objects.get(Forename='Roy', Surname='Hanley'),
                                    RaisedDate=creation_date,
                                    ModifiedDate=creation_date)
            test_note.save()
            pupil_note = PupilNotes(PupilId=Pupil.objects.get(id=i),
                                    NoteId=test_note)
            pupil_note.save()

        # create note for the first pupil in the list
        test_note_2 = NoteDetails(Keywords='Learning Support:EAL:Concern',
                                  NoteText='TEST FOR NOTES EXPLORER - x712zx',
                                  RaisedBy=Staff.objects.get(Forename='Roy', Surname='Hanley'),
                                  ModifiedBy=Staff.objects.get(Forename='Roy', Surname='Hanley'),
                                  RaisedDate=creation_date,
                                  ModifiedDate=creation_date)
        test_note_2.save()
        pupil_note = PupilNotes(PupilId=Pupil.objects.get(id=pupil_ids[0]),
                                NoteId=test_note_2)
        pupil_note.save()
        return pupil_ids

    def test_get_all_keywords(self):
        """
        Tests if keywords are correctly returned.
        """
        keywords = NoteKeywords.objects.all().order_by('Keyword').values_list('Keyword', flat=True)
        url = '/BatterseaAdmin/2014-2015/notes_explorer/get_all_keywords/'
        response = self.client1.get(url)
        response_as_python_data_structure = json.loads(response.content)
        for i in response_as_python_data_structure:
            self.assertEqual(i in keywords, True)

    def test_get_all_current_pupil_groups(self):
        """
        Test to see if all current pupil groups are returned and in the correct format.
        """
        all_current_pupil_groups_for_battersea = Group.objects.filter(Name__icontains='Current.Battersea.')
        url = '/BatterseaAdmin/2014-2015/notes_explorer/get_all_current_pupil_groups/'
        response = self.client1.get(url)
        response_as_python_data_structure = json.loads(response.content)
        for i in all_current_pupil_groups_for_battersea:
            self.assertEqual(i.Name in response_as_python_data_structure, True)

    def test_get_pupil_notes_count(self):
        """
        Test to see if pupil notes are counted correctly for the date range given.
        """
        pupil_ids = self.create_notes_data_for_testing()
        url = '/BatterseaAdmin/2014-2015/notes_explorer/get_pupil_notes_count/'
        post_data = {"group_name": "Current.Battersea.LowerSchool.Reception.Form.RBN",
                     "start_date": "2014-09-01",
                     "end_date": "2014-09-05",
                     "keyword_1": "Conduct:Detention:Pastoral",
                     "keyword_2": "Learning Support:EAL:Concern"}
        response = self.client1.post(url, post_data)
        response_as_python_data_structure = json.loads(response.content)
        for i in response_as_python_data_structure:
            # test if returned data is for pupils in the class 'Current.Battersea.LowerSchool.Reception.Form.RBN'
            self.assertEqual(i[0] in pupil_ids, True)
            pupil_object = PupilRecord(i[0], '2014-2015')
            self.assertEqual(i[1] == pupil_object.Pupil.FullName(), True)
            self.assertEqual(i[2] == pupil_object.Form(), True)
            self.assertEqual(i[3] == pupil_object.AcademicHouse(), True)
            self.assertEqual(int(i[4]) == 1, True)
            if response_as_python_data_structure.index(i) == 0:
                self.assertEqual(int(i[5]) == 1, True)
            else:
                self.assertEqual(int(i[5]) == 0, True)
            self.assertEqual(int(i[6]) == 0, True)
            self.assertEqual(int(i[7]) == 0, True)

    def test_get_pupil_notes_count_individual(self):
        """
        Test to see if individual pupil notes are coming out correctly.
        """
        pupil_ids = self.create_notes_data_for_testing()

        # test on first pupil
        url = '/BatterseaAdmin/2014-2015/notes_explorer/get_pupil_notes_individual/'
        post_data = {"pupil_id": "%s" % pupil_ids[0],
                     "start_date": "2014-09-01",
                     "end_date": "2014-09-05",
                     "keywords": "Learning Support:EAL:Concern"}
        response = self.client1.post(url, post_data)
        response_as_python_data_structure = json.loads(response.content)
        self.assertEqual('TEST FOR NOTES EXPLORER - x712zx' in response.content, True)
        self.assertEqual(len(response_as_python_data_structure) == 1, True)
        url = '/BatterseaAdmin/2014-2015/notes_explorer/get_pupil_notes_individual/'
        post_data = {"pupil_id": "%s" % pupil_ids[0],
                     "start_date": "2014-09-01",
                     "end_date": "2014-09-05",
                     "keywords": "Conduct:Detention:Pastoral"}
        response = self.client1.post(url, post_data)
        response_as_python_data_structure = json.loads(response.content)
        self.assertEqual('TEST FOR NOTES EXPLORER - x712zx' in response.content, True)
        self.assertEqual(len(response_as_python_data_structure) == 1, True)

        # test on second pupil
        url = '/BatterseaAdmin/2014-2015/notes_explorer/get_pupil_notes_individual/'
        post_data = {"pupil_id": "%s" % pupil_ids[1],
                     "start_date": "2014-09-01",
                     "end_date": "2014-09-05",
                     "keywords": "Conduct:Detention:Pastoral"}
        response = self.client1.post(url, post_data)
        response_as_python_data_structure = json.loads(response.content)
        self.assertEqual('TEST FOR NOTES EXPLORER - x712zx' in response.content, True)
        self.assertEqual(len(response_as_python_data_structure) == 1, True)
        url = '/BatterseaAdmin/2014-2015/notes_explorer/get_pupil_notes_individual/'
        post_data = {"pupil_id": "%s" % pupil_ids[1],
                     "start_date": "2014-09-01",
                     "end_date": "2014-09-05",
                     "keywords": "Learning Support:EAL:Concern"}
        response = self.client1.post(url, post_data)
        response_as_python_data_structure = json.loads(response.content)
        self.assertEqual(len(response_as_python_data_structure) == 0, True)

    def tearDown(self):
        notes_to_delete = NoteDetails.objects.filter(NoteText='TEST FOR NOTES EXPLORER - x712zx')
        for i in notes_to_delete:
            i.delete()
        pass


if __name__ == '__main__':
    unittest.main()
