# Django imports
from django.shortcuts import HttpResponse
from django.http import HttpResponseBadRequest
from django.core import serializers
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.db.models import Q

# Database imports
from MIS.modules.PupilRecs import PupilRecord
from MIS.ViewExtras import SetMenu
from MIS.models import *

# Python imports
import json


def home_page(request, school, ac_year):
    """
    Displays home page from the staff attendance app.
    """
    c = {'school': school,
         'AcYear': ac_year}
    con_inst = RequestContext(request, processors=[SetMenu])
    return render_to_response('notes_explorer_home.html', c,
                              context_instance=con_inst)


def get_all_keywords(request, school, ac_year):
    """
    Returns all keywords in a list form.
    """
    try:
        keywords = NoteKeywords.objects.all().order_by('Keyword').values_list('Keyword', flat=True)
        return HttpResponse(json.dumps(list(keywords)), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)



def get_all_current_pupil_groups(request, school, ac_year):
    """
    Returns all current pupil groups for a school.
    """
    try:
        school_from_menu_name = MenuTypes.objects.get(Name=school).SchoolId.Name
        groups = Group.objects.filter(Name__icontains='Current.%s.' % school_from_menu_name).order_by('Name').distinct('Name').values_list('Name',
                                                                                                                                           flat=True)
        return HttpResponse(json.dumps(list(groups)), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def get_pupil_notes_count(request, school, ac_year):
    """
    Returns pupil notes counted for a given set of keywords based upon post data received.
    """
    try:
        # set post data to variables

        group_name = request.POST['group_name']
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']
        if 'keyword_1' in request.POST:
            keyword_1 = request.POST['keyword_1'].split(':')
        else:
            keyword_1 = list()
        if 'keyword_2' in request.POST:
            keyword_2 = request.POST['keyword_2'].split(':')
        else:
            keyword_2 = list()
        if 'keyword_3' in request.POST:
            keyword_3 = request.POST['keyword_3'].split(':')
        else:
            keyword_3 = list()
        if 'keyword_4' in request.POST:
            keyword_4 = request.POST['keyword_4'].split(':')
        else:
            keyword_4 = list()

        # get all pupils for a group
        pupil_ids = PupilGroup.objects.filter(Group__Name__icontains=group_name,
                                              Active=True,
                                              AcademicYear=ac_year).order_by('Pupil__id').distinct('Pupil__id').values_list('Pupil__id', flat=True)

        # collect and count notes
        return_date = []
        keyword_1_count = 0
        keyword_2_count = 0
        keyword_3_count = 0
        keyword_4_count = 0
        for i in pupil_ids:
            notes_for_this_pupil = PupilNotes.objects.filter(PupilId=i,
                                                             NoteId__RaisedDate__range=(start_date, end_date),
                                                             Active=True)

            # collecting data for keyword_1
            if keyword_1:
                notes_from_keyword_1 = notes_for_this_pupil
                for keyword in keyword_1:
                    notes_from_keyword_1 = notes_from_keyword_1.filter(Q(NoteId__Keywords__contains=keyword))
                keyword_1_count = len(notes_from_keyword_1)

            # collecting data for keyword_2
            if keyword_2:
                notes_from_keyword_2 = notes_for_this_pupil
                for keyword in keyword_2:
                    notes_from_keyword_2 = notes_from_keyword_2.filter(Q(NoteId__Keywords__contains=keyword))
                keyword_2_count = len(notes_from_keyword_2)

            # collecting data for keyword_3
            if keyword_3:
                notes_from_keyword_3 = notes_for_this_pupil
                for keyword in keyword_3:
                    notes_from_keyword_3 = notes_from_keyword_3.filter(Q(NoteId__Keywords__contains=keyword))
                keyword_3_count = len(notes_from_keyword_3)

            # collecting data for keyword_4
            if keyword_4:
                notes_from_keyword_4 = notes_for_this_pupil
                for keyword in keyword_4:
                    notes_from_keyword_4 = notes_from_keyword_4.filter(Q(NoteId__Keywords__contains=keyword))
                keyword_4_count = len(notes_from_keyword_4)

            pupil_object = PupilRecord(i, ac_year)
            temp_array = [pupil_object.Pupil.id,
                          pupil_object.Pupil.FullName(),
                          pupil_object.Form(),
                          pupil_object.AcademicHouse(),
                          keyword_1_count,
                          keyword_2_count,
                          keyword_3_count,
                          keyword_4_count]
            return_date.append(temp_array)
        return HttpResponse(json.dumps(return_date), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def get_pupil_notes_count_individual(request, school, ac_year):
    """
    Returns pupil notes for a given pupil based upon post data received.
    """
    try:
        pupil_id = request.POST['pupil_id']
        start_date = request.POST['start_date']
        end_date = request.POST['end_date']
        if 'keywords' in request.POST:
            keywords = request.POST['keywords'].split(':')
        else:
            keywords = list()

        pupil_notes = PupilNotes.objects.filter(PupilId=pupil_id,
                                                NoteId__RaisedDate__range=(start_date, end_date),
                                                Active=True)
        for keyword in keywords:
            pupil_notes = pupil_notes.filter(Q(NoteId__Keywords__contains=keyword))
        return_data = serializers.serialize('json', pupil_notes,
                                            indent=2,
                                            use_natural_keys=True)
        return HttpResponse(return_data, 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)
