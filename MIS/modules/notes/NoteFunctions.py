from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache
from MIS.modules.emailFromWillowTree.emailFunctions import *
#from MIS.globalVariables.GlobalVariablesFunctions import *
import logging
log = logging.getLogger(__name__)

def AddPupilNotes(request, PupilList, fromEmail=False, StaffId=False, RaisedDate=False):
    """Method to add notes to pupils. Data is collected from form POST
    for example 'NoteTextArea' for the note itself and 'StaffId' for
    storing the staff member who raised the note."""
    KWList=':'.join(request.POST.getlist('KWSelect'))
    if fromEmail:
        NoteTextArea = fromEmail
    else:
        NoteTextArea = request.POST['NoteTextArea']
    if StaffId:
        staffObj = Staff.objects.get(id=int(StaffId))
    else:
        staffObj = request.session['StaffId']
    if RaisedDate:
        raisedDate = RaisedDate
    else:
        raisedDate = datetime.datetime.now()
    NoteRecord=NoteDetails(Keywords=KWList,
                           NoteText=NoteTextArea,
                           RaisedBy=staffObj,
                           RaisedDate=raisedDate,
                           ModifiedBy=request.session['StaffId'],
                           ModifiedDate=datetime.datetime.now())
    NoteRecord.save()
    for Pupil_Id in PupilList:
        PupilNoteRecord=PupilNotes(PupilId=Pupil_Id,NoteId=NoteRecord)
        PupilNoteRecord.save()
    # start thread to see if emails should be issues for this Note
    sendemails=EmailFromNoteSendThread(NoteRecord,
                                       request.session['StaffId'],
                                       request.session['School'].Name)
    sendemails.start()
    # start Thread to see if this Note should be prompoted to an isse
    changeToIssue=EmailFromIssueThread(NoteRecord,
                                       request.session['StaffId'],
                                       request.session['School'].Name)
    changeToIssue.start()
    return NoteRecord

def IsAnIssue(PupilRec,KWList):
    """This method determines if the note is an issues or not. Provide
    the PupilRec object and a list a keywords a associated with the
    note."""
    IssueList=NoteToIssue.objects.all()
    NoteIsAnIssue=False
    for records in IssueList:
        FindAllKeywords=True
        IssueKWList=eval(records.Keywords)
        for Words in IssueKWList:
            if KWList.find(Words) < 0:
                FindAllKeywords=False
        if FindAllKeywords:
            NoteIsAnIssue=True
            # send notification here
    return NoteIsAnIssue
def AddNote(request,BaseId,Type):
    """Adds a note to a pupil record. Example for Usage:
    x.AddNote(request=request,BaseId=3231,Type='Pupil')
    NOTE: BaseId is the Id of the Type. Type can be either Staff,Family,
    Contact or Pupil."""
    KWList=':'.join(request.POST.getlist('KWSelect'))
    NoteRecord=NoteDetails(Keywords=KWList,NoteText=request.POST['NoteTextArea'],RaisedBy=request.session['StaffId'],
    RaisedDate=datetime.datetime.now(),ModifiedBy=request.session['StaffId'],ModifiedDate=datetime.datetime.now())
    NoteRecord.save()
    if 'HousePoints' in request.POST:
        if not request.POST['HousePoints'] =='None':
            NoteRecord.HousePoints=int(request.POST['HousePoints'])
            NoteRecord.save()
    if Type=='Staff':
        StaffNoteRecord=StaffNotes(StaffId=BaseId,NoteId=NoteRecord)
        StaffNoteRecord.save()
    elif Type=='Family':
        FamilyNoteRecord=FamilyNotes(FamilyId=BaseId,NoteId=NoteRecord)
        FamilyNoteRecord.save()
    elif Type=='Contact':
        ContactNoteRecord=ContactNotes(ContactId=BaseId,NoteId=NoteRecord)
        ContactNoteRecord.save()
    elif Type=='Pupil':
        PupilNoteRecord=PupilNotes(PupilId=BaseId,NoteId=NoteRecord)
        PupilNoteRecord.save()
    return NoteRecord
def GetPupilNotes(request,Pupil_Id=None,Pupil_List=None,Group_Name=None,
                  achievementsOnly=False,
                  allNotesButAchievements=False,
                  allSenNotes=False,
                  allEalNotes=False,
                  allMostAbleNotes=False):
    """Returns a filtered list for a single, list of pupils or by group.
    Example usage:
    GetPupilNotes(request,Pupil_Id=3231) or,
    GetPupilNotes(request,Pupil_List=listOfPupils) or,
    GetPupilNotes(request,Group_Name='Applicants.Kensington.Accepted.Year1.1KN')
    """
    NoteRecords=PupilNotes.objects.all().order_by('NoteId__RaisedDate').reverse()
    if achievementsOnly==True:
        NoteRecords = NoteRecords.filter(NoteId__Keywords__icontains="achievement", Active=True)
    elif allMostAbleNotes == True:
        mostAbleKeywords = ['Most Able'] # add more GT keywords here where needed
        for mostAbleKeyword in mostAbleKeywords:
            NoteRecords = NoteRecords.filter(NoteId__Keywords__icontains=mostAbleKeyword, Active=True)
    elif allSenNotes == True:
        senKeywords = ['SEN'] # add more sen keywords here where needed
        for senKeyword in senKeywords:
            NoteRecords = NoteRecords.filter(NoteId__Keywords__icontains=senKeyword, Active=True)
    elif allEalNotes == True:
        senKeywords = ['EAL'] # add more eal keywords here where needed
        for senKeyword in senKeywords:
            NoteRecords = NoteRecords.filter(NoteId__Keywords__icontains=senKeyword, Active=True)
    elif allNotesButAchievements == True:
        NoteRecords = NoteRecords.filter(Active=True).exclude(NoteId__Keywords__icontains="achievement")
    else:
        NoteRecords=NoteRecords.filter(Active=True)
    if not Pupil_Id==None:
        NoteRecords=NoteRecords.filter(PupilId=Pupil.objects.get(id=int(Pupil_Id)))
    elif not Pupil_List==None:
        NoteRecords=NoteRecords.filter(PupilId__in=Pupil_List)
    else:
        NoteRecords=NoteRecords.filter(PupilId__in=GroupFunctions.Get_PupilList(AcYear=request.session['AcYear'],GroupName=Group_Name))
    Filters=''
    if 'NotesKWordFilter' in request.session:
        Filters=','.join(request.session['NotesKWordFilter'])
        for words in request.session['NotesKWordFilter']:
            NoteRecords=NoteRecords.filter(NoteId__Keywords__icontains=words)
    test = False
    for i in request.session.keys():
        if 'NotesStartFilter' == i:
            test = True
        elif 'NotesStopFilter' == i:
            test = True
    if test:
        if 'NotesStopFilter' not in request.session:
            temp = datetime.datetime.today()
            request.session['NotesStopFilter']= str(temp.date())
        if 'NotesStartFilter' not in request.session:
            lastYear = date.today().year-1
            lastSeptember = datetime.datetime.strptime("%s-09-01" % lastYear ,"%Y-%m-%d")
            request.session['NotesStartFilter']= str(lastSeptember.date())
        startDate=request.session['NotesStartFilter']
        if startDate == '':
            startDate = '1900-01-01 00:00'
        endDate=request.session['NotesStopFilter']
        if endDate == '':
            endDate = '2999-01-01'
        endDatePlusOne = datetime.datetime.strptime(endDate,"%Y-%m-%d")
        endDatePlusOne = endDatePlusOne+timedelta(days=1)
        endDatePlusOne = str(endDatePlusOne.date())
        NoteRecords=NoteRecords.filter(NoteId__RaisedDate__range=(startDate,endDatePlusOne))
        Filters+=' StartDate:%s ' % request.session['NotesStartFilter']
        Filters+=' StopDate:%s ' % request.session['NotesStopFilter']
    return {'NoteRecords':NoteRecords,'Filters':Filters}

def setFiltersFromManager(request):
    ''' used to set session data for note filters when coming from manager's '''
    if 'KWSelect2' in request.POST:
        request.session['NotesKWordFilter'] = request.POST['KWSelect2']
    if 'StartDate' in request.POST:
        request.session['NotesStartFilter'] = request.POST['StartDate']
    else:
        request.session['NotesKWordFilter'] = '1900-01-01'
    if 'StopDate' in request.POST:
        request.session['NotesStopFilter'] = request.POST['StopDate']
    else:
        request.session['NotesStopFilter'] = '2999-01-01'
    return

def GetNotes(request,Type,BaseId):
    """Returns a list of filtered notes. Example usage:
    GetNotes(request,Type='Staff',BaseId=3231)
    NOTE: BaseId is the Id of the Type. Type can be either Staff,Family
    or Contact"""
    if Type=='Staff':
        NoteRecords=StaffNotes.objects.filter(StaffId=Staff.objects.get(id=int(BaseId))).order_by('NoteId__RaisedDate')
    elif Type=='Family':
        NoteRecords=FamilyNotes.objects.filter(FamilyId=Family.objects.get(id=int(BaseId))).order_by('NoteId__RaisedDate')
    elif Type=='Contact':
        NoteRecords=ContactNotes.objects.filter(ContactId=Contact.objects.get(id=int(BaseId))).order_by('NoteId__RaisedDate')
    NoteRecords=NoteRecords.filter(Active=True)
    Filters=''
    if 'NotesKWordFilter' in request.session:
        Filters=','.join(request.session['NotesKWordFilter'])
        for words in request.session['NotesKWordFilter']:
            NoteRecords=NoteRecords.filter(NoteId__Keywords__icontains=words)
    if 'NotesStartFilter' in request.session:
        NoteRecords=NoteRecords.filter(NoteId__RaisedDate__gte=request.session['NotesStartFilter'])
        Filters+=' StartDate:%s ' % request.session['NotesStartFilter']
    if 'NotesStopFilter' in request.session:
        NoteRecords=NoteRecords.filter(NoteId__RaisedDate__gte=request.session['NoteStopFilter'])
        Filters+=' StopDate:%s ' % request.session['NotesStopFilter']
    return {'NoteRecords':NoteRecords,'Filters':Filters}
def GetSingleNote(Note_Id):
    """Returns a single note of any kind base upon its Id."""
    if NoteDetails.objects.filter(id=int(Note_Id)).exists():
        return NoteDetails.objects.get(id=int(Note_Id))
    else:
        return False
def UpdateNoteFromRequest(request,ObjectType):
    '''This fuction Updates a Note. it does this by first setting the original
    Note to Inactive, and then saves a new note'''
    ObjectList=NoteFunctions.RemoveNote(request.POST['NoteId'],Type=ObjectType)
    return NoteFunctions.AddNote(request,ObjectList[0])
def PermissionToRemove(request):
    '''Checks permission to Remove, or inactivate a note against a pupils record.
    This action will not delete a Note, just mark as inactive'''
    return True
def PermissionToUpdate(request,NoteRecord):
    '''Checks permissions to Update a Note '''
    return True
def RemoveSingleNote(Note_Id=None,BaseId=None,NoteType=None):
    '''This function removes a single relationship to Note. Example usage:
    RemoveSingleNote(Note_Id=32,BaseId=3231,NoteType='Pupil')
    '''
    Note_Id=int(Note_Id)
    if NoteDetails.objects.filter(id=Note_Id).exists():
        if PupilNotes.objects.filter(NoteId__id=Note_Id,PupilId__id=BaseId,Active=True).exists() and NoteType.find('Pupil') > -1:
            PupilRecords=PupilNotes.objects.get(NoteId__id=Note_Id,PupilId__id=BaseId,Active=True)
            PupilRecords.Active=False
            PupilRecords.save()
            return [PupilRecords]
        elif StaffNotes.objects.filter(NoteId__id=Note_Id,StaffId__id=BaseId,Active=True).exists() and NoteType.find('Staff') > -1:
            StaffRecords=StaffNotes.objects.filter(NoteId__id=Note_Id,StaffId__id=BaseId)
            StaffList=[]
            for records in StaffRecords:
                records.Active=False
                records.save()
                StaffList.append(records.StaffId)
            return StaffList
        elif ContactNotes.objects.filter(NoteId__id=Note_Id,ContactId__id=BaseId,Active=True).exists() and NoteType.find('Contact') > -1:
            ContactRecords=ContactNotes.objects.filter(NoteId__id=Note_Id,ContactId__id=BaseId)
            ContactList=[]
            for records in ContactRecords:
                records.Active=False
                records.save()
                ContactList.append(records.ContactId)
            return ContactList
        elif FamilyNotes.objects.filter(NoteId__id=Note_Id,FamilyId__id=BaseId,Active=True).exists() and NoteType.find('Family') > -1:
            FamilyRecords=FamilyNotes.objects.filter(NoteId__id=Note_Id,FamilyId__id=BaseId)
            FamilyList=[]
            for records in FamilyRecords:
                records.Active=False
                records.save()
                FamilyList.append(records.FamilyId)
            return FamilyList
        else:
            return False
    else:
        return False


def RemoveNote(Note_Id,Type):
    '''This Sets a note and its related realationship records to Inactive.
    Example usage: RemoveNote(Note_Id=32,Type='Pupil')
    NOTE: Type can be either Pupil, Staff, Contact or Family'''
    Note_Id=int(Note_Id)
    if NoteDetails.objects.filter(id=Note_Id,Active=True).exists():
        if PupilNotes.objects.filter(NoteId__id=Note_Id,Active=True).exists() and Type=='Pupil':
            PupilRecords=PupilNotes.objects.filter(NoteId__id=Note_Id)
            PupilList=[]
            for records in PupilRecords:
                records.Active=False
                records.save()
                PupilList.append(records.PupilId)
            return PupilList
        elif StaffNotes.objects.filter(NoteId__id=Note_Id,Active=True).exists() and Type=='Staff':
            StaffRecords=StaffNotes.objects.filter(NoteId__id=Note_Id)
            StaffList=[]
            for records in StaffRecords:
                records.Active=False
                records.save()
                StaffList.append(records.StaffId)
            return StaffList
        elif ContactNotes.objects.filter(NoteId__id=Note_Id,Active=True).exists() and Type=='Contact':
            ContactRecords=ContactNotes.objects.filter(NoteId__id=Note_Id)
            ContactList=[]
            for records in ContactRecords:
                records.Active=False
                records.save()
                ContactList.append(records.ContactId)
            return ContactList
        elif FamilyNotes.objects.filter(NoteId__id=Note_Id,Active=True).exists() and Type=='Family':
            FamilyRecords=FamilyNotes.objects.filter(NoteId_id=Note_Id)
            FamilyList=[]
            for records in FamilyRecords:
                records.Active=False
                records.save()
                FamilyList.append(records.FamilyPupilId)
            return FamilyList
        else:
            return False


def NotifyStaffOfNote(Details):
    """Notify staff of note"""
    return True


def returnRaisedByIdOfNote(Note_Id):
    ''' return Raised By staff Id and raised by date of a Note '''
    if NoteDetails.objects.filter(id=Note_Id,Active=True).exists():
        NoteDetails.objects.get(id=Note_Id, Active=True)
    return {'staffId': NoteDetails.objects.get(id=Note_Id, Active=True).RaisedBy.id,
            'raisedDate': NoteDetails.objects.get(id=Note_Id, Active=True).RaisedDate}


class notesReport():
    ''' Gets notes for pupils based on  keywords for a set date range '''
    def __init__(self, pupils, keywords, startDate=False, endDate=False):
        self.pupils = pupils
        self.keywords = keywords
        if startDate:
            self.startDate = startDate
        else:
            self.startDate = datetime.datetime.strptime('2000-01-01', "%Y-%m-%d")
        if endDate:
            self.endDate = endDate
        else:
            self.endDate = datetime.datetime.strptime('2100-01-01', "%Y-%m-%d")

    def getData(self):
        ''' gets notes data '''
        pn = []
        for i in self.pupils:
            for keyword in self.keywords:
                notes = PupilNotes.objects.filter(PupilId__id=i,
                                                  NoteId__Keywords__icontains=keyword,
                                                  NoteId__RaisedDate__range=(self.startDate, self.endDate),
                                                  Active='True')
                if len(notes) != 0:
                    for n in notes:
                        pn.append(n)
        return pn


class KeywordList():
    ''' This class produces an object for accessing the Keywords
    for Notes and Issues '''
    def __init__(self,Type):
        self.Keywords=NoteKeywords.objects.filter(Active=True).order_by('Keyword')
    def Primary(self):
        return self.Keywords.filter(KeywordType='Primary')
    def Secondary(self):
        return self.Keywords.filter(KeywordType='Secondary')
    def Tertiary(self):
        return self.Keywords.filter(KeywordType='Tertiary')
    def Subject(self):
        return self.Keywords.filter(KeywordType='Subject')
    def HousePointWords(self):
        return self.Keywords.filter(HousePointsTrigger=True)
    def All(self):
        return self.Keywords
    def StringListOnlyNoRepeats(self):
        Keywords = self.Keywords.values('Keyword').distinct()
        KeywordListStringOnly = []
        for i in Keywords:
            KeywordListStringOnly.append(i.values()[0])
        return KeywordListStringOnly
    def achievementKeywordsOnly(self):
        secondaryKeywords = []
        tertiaryKeywords = []
        for keyword in self.Keywords:
            if keyword.KeywordPool == 'Achievement':
                secondaryKeywords.append(str(keyword.Keyword))
        for i in self.Keywords:
            for secondaryKeyword in secondaryKeywords:
                if i.KeywordPool == secondaryKeyword:
                    tertiaryKeywords.append(str(i.Keyword))
        achievementKeywordsOnlyList = [u'Achievement']+secondaryKeywords+tertiaryKeywords
        return achievementKeywordsOnlyList


def NoteNotification(PupilList,Keywords,AcYear):
    grouplist=set()
    for pupils in PupilList:
        groups =PupilGroup.objects(id=pupils,AcademicYear=AcYear)

class GetGroupNotes():
    def __init__(self,PupilList):
        self.PupilList=PupilList
        self.AcYear=GlobalVariables.Get_SystemVar('CurrentYear')
    def __previousYear(self):
        """ takes the current AcYear and works out AcYear-1 """
        Year1, Year2 = self.AcYear.split('-')
        return "%d-%d" % (int(Year1) + NoYears, int(Year2) + NoYears)
    def ThisYearsNotes(self):
        pass
    def TwoYearsNotes(self):
        pass
