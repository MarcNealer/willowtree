# unit test imports
import sys
import os
sys.path.append('/home/ubuntu/sites/WillowTree')
#sys.path.append('/home/login/Dropbox/Projects/WillowTree/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings
import unittest
from django.test import Client

# Setting up test environment
from django.test.utils import setup_test_environment
setup_test_environment()

# other imports
from MIS.modules.notes.BatterseaBehaviourReport import BatterseaBehaviourReport
from MIS.models import *
import datetime


class BatterseaBehaviourReportTests(unittest.TestCase):
    def setUp(self):
        self.pupil_id = 8269
        self.d = {'behaviour_keywords': ['Conduct', 'Detention'],
                  'groups': (('Staff.Battersea.Behavior_Report.MS_PASTORAL', [3, 4, 5]),
                            ('Staff.Battersea.Behavior_Report.MS_HOMS_PASTORAL', [6, 7, 8]),
                            ('Staff.Battersea.Behavior_Report.MS_LT_PASTORAL', [9]),
                            ('Staff.Battersea.Behavior_Report.US_PASTORAL', [3, 4, 5]),
                            ('Staff.Battersea.Behavior_Report.US_HOMS_PASTORAL', [6, 7, 8]),
                            ('Staff.Battersea.Behavior_Report.US_LT_PASTORAL', [9, 10, 11, 12, 13, 14, 15, 16, 17]))}
        self.today_as_iso_format = datetime.datetime.now().date().isoformat()
        self.today_as_object = datetime.datetime.now().date()

    def run_report(self):
        return BatterseaBehaviourReport(self.d).main_logic()

    def create_note(self, creation_date):
        test_note = NoteDetails(Keywords='Conduct:Detention:Pastoral',
                                NoteText='THIS IS A TEST ONLY - roy hanley',
                                RaisedBy=Staff.objects.get(Forename='Roy', Surname='Hanley'),
                                ModifiedBy=Staff.objects.get(Forename='Roy', Surname='Hanley'),
                                RaisedDate=creation_date,
                                ModifiedDate=creation_date)
        test_note.save()
        pupil_note = PupilNotes(PupilId=Pupil.objects.get(id=self.pupil_id),
                                NoteId=test_note)
        pupil_note.save()
        return

    def create_notes_run_report(self, number_of_notes):
        for i in range(number_of_notes):
            test_date = self.today_as_object - datetime.timedelta(days=i)
            self.create_note(test_date)
        return self.run_report()

    def test_create_3_notes_run_report(self):
        """
        Should trigger email for US_PASTORAL
        """
        report = self.create_notes_run_report(3)
        print report

    def test_create_6_notes_run_report(self):
        """
        Should trigger email for US_HOMS_PASTORAL
        """
        report = self.create_notes_run_report(7)
        print report

    def test_create_9_notes_run_report(self):
        """
        Should trigger email for US_PASTORAL
        """
        report = self.create_notes_run_report(11)
        print report

    def tearDown(self):
        for note in NoteDetails.objects.filter(NoteText__icontains='THIS IS A TEST ONLY - roy hanley', Active=True):
            note.Active = False
            note.save()
        for note in PupilNotes.objects.filter(NoteId__NoteText__icontains='THIS IS A TEST ONLY - roy hanley'):
            note.Active = False
            note.save()


if __name__ == '__main__':
    unittest.main()