# Base python libray imports
import datetime
import time
from operator import itemgetter
import csv

# Django middleware imports
from django.utils.html import strip_tags
from django.shortcuts import HttpResponse
from django.http import HttpResponseBadRequest
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q
from django.core import serializers
from django.utils import simplejson

# WillowTree system imports
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.models import *
from MIS.modules.notes import NoteFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.emailFromWillowTree import emailFunctions
from MIS.modules.globalVariables.GlobalVariablesFunctions import GlobalVariables


def CreateAStaffNote(request, school, AcYear):
    ''' This view sends out the Create a Note page. ikts trigged by either the
    NoteForAPupil, or NoteForAGroup View '''
    StaffId=request.session['NoteStaffId']
    Keywords=NoteKeywords.objects.filter(Active=True)
    KWPrimary=[]
    KWSecondary=[]
    for words in Keywords:
        if words.KeywordType=='Primary':
            KWPrimary.append(words.Keyword)
        else:
            KWSecondary.append(words.Keyword)
    c={'school':school,'AcYear':AcYear,'KWList':Keywords,'KWPrimary':KWPrimary,'KWSecondary':KWSecondary,'StaffId':StaffId}
    return render_to_response('CreateAStaffNote.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def NoteForStaff(request,school,AcYear,StaffNo):
    ''' Triggered from the pupil Page to add a note for the selected pupil. It
    does this by storing the Pupil ID in NotePupilList in the session data and
    then calling Create a Note via a redirect'''
    request.session['NoteStaffId']=Staff.objects.get(id=int(StaffNo))
    return HttpResponseRedirect('/WillowTree/%s/%s/CreateAStaffNote/' % (school,AcYear))


def SaveANoteManager(request, school, AcYear, GroupName):
    ''' called by the CreateANote page to save the created Note '''
    PupilList = []
    emailResult = False
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        PupilList.append(Pupil.objects.get(id=int(Pupil_Id)))
    if "email" in request.POST:
        emailObj = emailFunctions.willowTreeEmails(request,
                                                   PupilList,
                                                   fromNoteViews=True)
        emailResult = emailObj.send()
    if len(PupilList) > 0:
        if emailResult:
            emailSendLst = ""
            for i in emailResult[2]:
                emailSendLst += "%s, " % i
            emailSendLstFinal = emailSendLst[:-1]
            emailMsg = "EMAIL SENT FROM: %s<br />" % emailResult[1]
            emailMsg += "EMAIL SEND LIST: %s<br />" % emailSendLstFinal
            emailMsg += "EMAIL HEADER: %s<br /><br />" % emailResult[0]
            requestObj = request.POST.copy()
            emailMsg += requestObj['NoteTextArea2']
            NoteFunctions.AddPupilNotes(request, PupilList, emailMsg)
        else:
            NoteFunctions.AddPupilNotes(request, PupilList)
        if len(PupilList) == 1:
            if emailResult:
                msg = ['Note emailed and saved for %s' % PupilList[0].FullName()]
            else:
                msg = ['Note saved for %s' % PupilList[0].FullName()]
            request.session['Messages'] = msg
        else:
            if emailResult:
                msg = ['Note emailed and saved for %d Pupils' % len(PupilList)]
            else:
                msg = ['Note saved for %d Pupils' % len(PupilList)]
            request.session['Messages'] = msg
    else:
        msg = ['No pupils selected thus no Note has been saved']
        request.session['Messages'] = msg
    tupForReturn = (school, AcYear, GroupName)
    if "senView" in request.POST:
        httpRetrn = '/WillowTree/%s/%s/ManageSEN/%s/'
    elif "ealView" in request.POST:
        httpRetrn = '/WillowTree/%s/%s/ManageEAL/%s/'
    else:
        httpRetrn = '/WillowTree/%s/%s/Manage/%s/'
    return HttpResponseRedirect(httpRetrn % tupForReturn)


def SaveANoteManagerSinglePupil(request, school, AcYear, PupilNo,
                                fromSEN=False,
                                fromEAL=False,
                                fromMostAble=False):
    ''' called by the CreateANote page to save the created Note for a single
    pupil'''
    emailResult = False
    pupilObj = Pupil.objects.get(id=int(PupilNo))
    PupilList = []
    PupilList.append(pupilObj)
    if "email" in request.POST:
        emailObj = emailFunctions.willowTreeEmails(request,
                                                   [pupilObj],
                                                   fromNoteViews=True)
        emailResult = emailObj.send()
    if emailResult:
        emailSendLst = ""
        for i in emailResult[2]:
            emailSendLst += "%s, " % i
        emailSendLstFinal = emailSendLst[:-1]
        emailMsg = "EMAIL SENT FROM: %s<br />" % emailResult[1]
        emailMsg += "EMAIL SEND LIST: %s<br />" % emailSendLstFinal
        emailMsg += "EMAIL HEADER: %s<br /><br />" % emailResult[0]
        requestObj = request.POST.copy()
        emailMsg += requestObj['NoteTextArea2']
        NoteFunctions.AddPupilNotes(request, PupilList, emailMsg)
    else:
        NoteFunctions.AddPupilNotes(request, PupilList)
    tupleArray = (school, AcYear, PupilNo,)
    if fromSEN == True:
        httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%s/'
        return HttpResponseRedirect(httpAddress % tupleArray)
    elif fromEAL == True:
        httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%s/'
        return HttpResponseRedirect(httpAddress % tupleArray)
    elif fromMostAble == True:
        httpAddress = '/WillowTree/%s/%s/pupilGiftedAndTalented/%s/'
        return HttpResponseRedirect(httpAddress % tupleArray)
    else:
        httpAddress = '/WillowTree/%s/%s/pupil/%s/'
        return HttpResponseRedirect(httpAddress % tupleArray)

def SaveAStaffNote(request,school,AcYear):
    ''' called by the CreateANote page to save the created Note '''
    # save the Note and return the saved record
    Staff_Id=request.session['NoteStaffId']
    del request.session['NoteStaffId']
    NoteRecord=NoteFunctions.AddNote(request,BaseId=Staff_Id,Type='Staff')

    c={'school':school,'AcYear':AcYear,'BaseId':Staff_Id,'NoteRecord':NoteRecord,'BaseType':'Staff'}
    # Send data to see if any staff should be notified
    NoteFunctions.NotifyStaffOfNote(c)
    return render_to_response('NoteSaved.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def ViewPupilNotes(request,school,AcYear,PupilNo,achievementsOnly,allNotesButAchievements):
    ''' Displays all Notes that meet the filter criteria. This is called from the
    Pupil Record Page'''
    PupilDetails=PupilRecord(PupilNo,AcYear)
    c={'school':school,'AcYear':AcYear,'PupilDetails':PupilDetails}
    if achievementsOnly == True:
        request.session['visitedAchievementNotes'] = True
        try:
            if request.session['visitedStandardNotes'] == True:
                del request.session['NotesKWordFilter']
                del request.session['NotesStartFilter']
                del request.session['NotesStopFilter']
        except:
            pass
        request.session['visitedStandardNotes'] = False
        c.update(NoteFunctions.GetPupilNotes(request,Pupil_Id=PupilNo,achievementsOnly=True))
        keywords = NoteFunctions.KeywordList('Notes')
        c.update({'KWList':keywords})
    elif allNotesButAchievements == True:
        request.session['visitedStandardNotes'] = True
        try:
            if request.session['visitedAchievementNotes'] == True:
                del request.session['NotesKWordFilter']
                del request.session['NotesStartFilter']
                del request.session['NotesStopFilter']
        except:
            pass
        request.session['visitedAchievementNotes'] = False
        c.update(NoteFunctions.GetPupilNotes(request,Pupil_Id=PupilNo,allNotesButAchievements=True))
        keywordsFull = NoteFunctions.KeywordList('Notes').StringListOnlyNoRepeats()
        keywordsAchievements = NoteFunctions.KeywordList('Notes').achievementKeywordsOnly()
        keywords = []
        for i in keywordsFull:
            if i not in keywordsAchievements:
                keywords.append(i)
        c.update({'KWList':keywords})
    # below else statement to show old code, not really used anymore.
    else:
        c.update(NoteFunctions.GetPupilNotes(request,Pupil_Id=PupilNo))
        keywords = NoteFunctions.KeywordList('Notes')
        c.update({'KWList':keywords})
    try:
        c.update({"NotesKWordFilter":request.session['NotesKWordFilter']})
    except:
        pass
    try:
        c.update({"NotesStartFilter":request.session['NotesStartFilter']})
    except:
        pass
    try:
        c.update({"NotesStopFilter":request.session['NotesStopFilter']})
    except:
        pass
    if achievementsOnly == True:
        c.update({'achievementsOnly': achievementsOnly})
    else:
        c.update({'standardNotesOnly': 'standardNotesOnly'})
    return render_to_response('ViewPupilNotes.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def ViewPupilIssues(request,school,AcYear,PupilNo):
    ''' Displays all Notes that meet the filter criteria. This is called from the
    Pupil Record Page'''
    PupilDetails=PupilRecord(PupilNo,AcYear)
    c={'school':school,'AcYear':AcYear,'PupilDetails':PupilDetails,'KWList':NoteFunctions.KeywordList('Notes')}
    c.update(NoteFunctions.GetPupilNotes(request,Pupil_Id=PupilNo))
    return render_to_response('ViewPupilIssues.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def ViewStaffNotes(request,school,AcYear,StaffNo):
    ''' Displays all Notes that meet the filter criteria. This is called from the
    Pupil Record Page'''
    c={'school':school,'AcYear':AcYear,'StaffDetails':Staff.objects.get(id=int(StaffNo)),'KWList':NoteKeywords.objects.filter(Active=True)}
    c.update(NoteFunctions.GetNotes(request,BaseId=int(StaffNo),Type='Staff'))
    return render_to_response('ViewStaffNotes.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def FilterNotes(request,school,AcYear,DisplayType,FilterachievementNotes):
    ''' This is called by the View Notes page. It reads the filter form entries and
    puts them in to the session data before calling the Notes display pages. These
    pages then use this filter data to filter the Notes to be displayed'''
    if 'NotesKWordFilter' in request.session:
        del request.session['NotesKWordFilter']
    if 'NotesStartFilter' in request.session:
        del request.session['NotesStartFilter']
    if 'NotesStopFilter' in request.session:
        del request.session['NotesStopFilter']

    FilterWords=[]
    if 'KWSelect' in request.POST:
        for words in request.POST.getlist('KWSelect'):
            FilterWords.append(words)
        request.session['NotesKWordFilter']=FilterWords
    if 'StartDate' in request.POST:
        if len(request.POST['StartDate']) > 0:
            request.session['NotesStartFilter']=request.POST['StartDate']
    if 'StopDate' in request.POST:
        if len(request.POST['StopDate']) > 0:
            request.session['NotesStopFilter']=request.POST['StopDate']
    if DisplayType=='Pupil':
        if FilterachievementNotes == True:
            return HttpResponseRedirect('/WillowTree/%s/%s/ViewPupilAchievements/%s' % (school,AcYear,request.POST['PupilNo']))
        else:
            return HttpResponseRedirect('/WillowTree/%s/%s/ViewPupilNotes/%s' % (school,AcYear,request.POST['PupilNo']))
    elif DisplayType=='Group':
        return HttpResponseRedirect('/WillowTree/%s/%s/ViewGroupNotes/%s' % (school,AcYear,request.POST['GroupName']))
    elif DisplayType=='Staff':
        return HttpResponseRedirect('/WillowTree/%s/%s/ViewStaffNotes/%s' % (school,AcYear,request.POST['StaffId']))


def ListGroupNotes(request,school,AcYear,GroupName):
    ''' To display Notes for a group, we show a filter page first as there could be a large number of
    Notes for the selected group. The user then sets filter options before going to the
    ViewGroupNotes page'''
    Keywords=NoteKeywords.objects.filter(Active=True)
    c={'school':school,'AcYear':AcYear,'GroupName':GroupName,'KWList':Keywords}
    return render_to_response('FilterGroupNotes.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def ViewGroupNotes(request,school,AcYear):
    ''' Displayes the Notes for a selected group that match the filter criteria'''
    Pupils=[]
    for pupils in request.POST['PupilList']:
        Pupils.append(Pupil.objects.get(id=int(Pupils)).id)
    c={'school':school,'AcYear':AcYear,'PupilList':Pupils,'KWList':NoteKeywords.objects.filter(Active=True)}
    c.update(NoteFunctions.GetPupilNotes(request,Pupil_List=Pupils))
    return render_to_response('ViewGroupNotes.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def ModifyPupilNote(request,school,AcYear,PupilNo, NoteId):
    ''' Used to either inactivate or modify a Note. If remove is selected, it removes the Note and
    sends a message to say the Note has been removed.
    If its a modify, it gets the Notes details and sends the data to the ModifyANote template'''
    if 'Remove' in request.POST:
        if NoteFunctions.PermissionToRemove(request):
            if NoteFunctions.RemoveSingleNote(Note_Id=NoteId,BaseId=int(PupilNo),NoteType='Pupil'):
                PupilDetails=PupilRecord(PupilNo,AcYear)
                Message='Note Removed for <a href="/WillowTree/%s/%s/pupil/%s">%s %s</a>' % (school, AcYear, PupilDetails.Pupil.id,PupilDetails.Pupil.Forename,PupilDetails.Pupil.Surname)
            else:
                Message='No corresponding Note found. No action has been taken NoteId:%s PupilId:%s' % (NoteId, PupilNo)
        else:
            Message='You do not have permission to close this Note. Please contact your dept head'
        c={'school':school,'AcYear':AcYear,'return_msg':[Message]}
        return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    else:
        NoteRecord=NoteFunctions.GetSingleNote(Note_Id=NoteId)

        if NoteRecord and NoteFunctions.PermissionToUpdate(request,NoteId):
            Keywords=NoteKeywords.objects.filter(Active=True)
            KWPrimary=[]
            KWSecondary=[]
            for words in Keywords:
                if words.KeywordType=='Primary':
                    KWPrimary.append(words.Keyword)
                else:
                    KWSecondary.append(words.Keyword)
            c={'school':school,'AcYear':AcYear,'NoteRecord':NoteRecord,'KWList':NoteFunctions.KeywordList('Note'),'PupilList':[Pupil.objects.get(id=int(PupilNo))]}
            return render_to_response('ModifyANote_New.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
        else:
            c={'school':school,'AcYear':AcYear,'return_msg':["Sorry, but you don't have access to Modify this record"]}
            return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def RemovePupilNote(request,school,AcYear,PupilNo, NoteId):
    ''' Used to either inactivate or modify a Note. If remove is selected, it removes the Note and
    sends a message to say the Note has been removed.
    If its a modify, it gets the Notes details and sends the data to the ModifyANote template'''
    if NoteFunctions.PermissionToRemove(request):
        if NoteFunctions.RemoveSingleNote(Note_Id=NoteId,BaseId=int(PupilNo),NoteType='Pupil'):
            PupilDetails=PupilRecord(PupilNo,AcYear)
            Message='Note Removed for <a href="/WillowTree/%s/%s/pupil/%s">%s %s</a>' % (school, AcYear, PupilDetails.Pupil.id,PupilDetails.Pupil.Forename,PupilDetails.Pupil.Surname)
        else:
            Message='No corresponding Note found. No action has been taken NoteId:%s PupilId:%s' % (NoteId, PupilNo)
    else:
        Message='You do not have permission to close this Note. Please contact your dept head'
    Message = Message+"<br /><br /><a href='/WillowTree/%s/%s/ViewPupilNotes/%s/'>Back to Notes?</a>" % (school, AcYear, PupilNo)
    c={'school':school,'AcYear':AcYear,'return_msg':[Message]}
    return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def RemovePupilNote2(request,school,AcYear,PupilNo, NoteId, fromSEN=False, fromEAL=False, fromMostAble=False):
    '''ONLY Removes a pupil note (no modify function) and returns the user to the view pupil notes
    page without going to a note deleted confirmation page as with the orginal function'''
    NoteFunctions.RemoveSingleNote(Note_Id=NoteId,BaseId=int(PupilNo),NoteType='Pupil')
    tupleArray = (school, AcYear, PupilNo,)
    if fromSEN == True:
        httpAddress = '/WillowTree/%s/%s/pupilSENRecord/%s/'
        return HttpResponseRedirect(httpAddress % tupleArray)
    elif fromEAL == True:
        httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%s/'
        return HttpResponseRedirect(httpAddress % tupleArray)
    elif fromMostAble == True:
        httpAddress = '/WillowTree/%s/%s/pupilGiftedAndTalented/%s/'
        return HttpResponseRedirect(httpAddress % tupleArray)
    else:
        httpAddress = '/WillowTree/%s/%s/ViewPupilNotes/%s/'
        return HttpResponseRedirect(httpAddress % tupleArray)


def ModifyPupilNotePopUp(request,school,AcYear,PupilNo, NoteId):
    ''' Used to either inactivate or modify a Note. If remove is selected, it removes the Note and
    sends a message to say the Note has been removed.
    If its a modify, it gets the Notes details and sends the data to the ModifyANote template'''
    if 'Remove' in request.POST:
        if NoteFunctions.PermissionToRemove(request):
            if NoteFunctions.RemoveSingleNote(Note_Id=NoteId,BaseId=int(PupilNo),NoteType='Pupil'):
                PupilDetails=PupilRecord(PupilNo,AcYear)
                Message='Note Removed for <a href="/WillowTree/%s/%s/pupil/%s">%s %s</a>' % (school, AcYear, PupilDetails.Pupil.id,PupilDetails.Pupil.Forename,PupilDetails.Pupil.Surname)
            else:
                Message='No corresponding Note found. No action has been taken NoteId:%s PupilId:%s' % (NoteId, PupilNo)
        else:
            Message='You do not have permission to close this Note. Please contact your dept head'
        c={'school':school,'AcYear':AcYear,'return_msg':[Message]}
        return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    else:
        NoteRecord=NoteFunctions.GetSingleNote(Note_Id=NoteId)

        if NoteRecord and NoteFunctions.PermissionToUpdate(request,NoteId):
            Keywords=NoteKeywords.objects.filter(Active=True)
            KWPrimary=[]
            KWSecondary=[]
            for words in Keywords:
                if words.KeywordType=='Primary':
                    KWPrimary.append(words.Keyword)
                else:
                    KWSecondary.append(words.Keyword)
            c={'school':school,'AcYear':AcYear,'NoteRecord':NoteRecord,'KWList':KeywordList('Note'),'PupilList':[Pupil.objects.get(id=int(PupilNo))]}
            return render_to_response('ModifyANote_New.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
        else:
            c={'school':school,'AcYear':AcYear,'return_msg':["Sorry, but you don't have access to Modify this record"]}
            return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def ModifyStaffNote(request,school,AcYear,StaffNo, NoteId):
    ''' Used to either inactivate or modify a Note. If remove is selected, it removes the Note and
    sends a message to say the Note has been removed.
    If its a modify, it gets the Notes details and sends the data to the ModifyANote template'''
    if 'Remove' in request.POST:
        if NoteFunctions.PermissionToRemove(request):
            if NoteFunctions.RemoveSingleNote(Note_Id=NoteId,BaseId=int(StaffNo),NoteType='Staff'):
                Message='Note %s has been marked as Inactive and will not appear on the Staff %s record' % (NoteId, StaffNo)
            else:
                Message='No corresponding Note found. No action has been taken NoteId:%s StaffId:%s' % (NoteId, StaffNo)
        else:
            Message='You do not have permission to close this Note. Please contact your dept head'
        c={'school':school,'AcYear':AcYear,'return_msg':[Message]}
        return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

    else:
        NoteRecord=NoteFunctions.GetSingleNote(Note_Id=NoteId)


        if NoteRecord and NoteFunctions.PermissionToUpdate(request,NoteId):
            Keywords=NoteKeywords.objects.filter(Active=True)
            KWPrimary=[]
            KWSecondary=[]
            for words in Keywords:
                if words.KeywordType=='Primary':
                    KWPrimary.append(words.Keyword)
                else:
                    KWSecondary.append(words.Keyword)
            c={'school':school,'AcYear':AcYear,'NoteRecord':NoteRecord,'KWList':Keywords,'KWPrimary':KWPrimary,'KWSecondary':KWSecondary}
            return render_to_response('ModifyANote.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
        else:
            c={'school':school,'AcYear':AcYear,'return_msg':["Sorry, but you don't have access to Modify this record"]}
            return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def UpdateANote(request,school,AcYear):
    ''' Tiggered by the ModifyANote page. It triggers the UpdateNoteFromRequest function
    which first off inactivates the old Note before adding a new one'''
    PupilList=NoteFunctions.RemoveNote(request.POST['NoteId'],Type='Pupil')
    NoteFunctions.AddPupilNotes(request,PupilList)
    c={'school':school,'AcYear':AcYear,'PupilList':PupilList}
    return render_to_response('UpdatedMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def UpdateANote2(request, school, AcYear, PupilNo, NoteId):
    ''' Tiggered by the ModifyANote page. It triggers the UpdateNoteFromRequest function
    which first off inactivates the old Note before adding a new one. The difference from
    this method and UpdateANote() is this method passes NoteId as a through the method NOT
    as post data'''
    oldNoteData = NoteFunctions.returnRaisedByIdOfNote(NoteId)
    PupilList = NoteFunctions.RemoveNote(NoteId, Type='Pupil')
    NoteFunctions.AddPupilNotes(request,
                                PupilList,
                                StaffId=oldNoteData['staffId'],
                                RaisedDate=oldNoteData['raisedDate'])
    c={'school':school,'AcYear':AcYear,'PupilList':PupilList}
    return render_to_response('UpdatedMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def UpdateAStaffNote(request,school,AcYear):
    ''' Tiggered by the ModifyANote page. It triggers the UpdateNoteFromRequest function
    which first off inactivates the old Note before adding a new one'''
    if NoteFunctions.UpdateNoteFromRequest(request,Type='Staff'):
        Messages=['Note successfully Updated']
    else:
        Messages['Note Failed to Update successfully']
    c={'school':school,'AcYear':AcYear,'return_msg':Messages}
    return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def UpdateIssue(request,school,AcYear,IssueNo,PupilNo):
    """Update an issue. Uses POST data 'NoteTextArea' to do this"""
    NoteRecord=NoteDetails.objects.get(id=int(IssueNo))
    NoteRecord.NoteText+="<br>   ********* %s : %s %s ************* <br>" % (datetime.datetime.today().strftime('%d/%m/%y'),request.session['StaffId'].User.first_name,request.session['StaffId'].User.last_name)
    NoteRecord.NoteText+=request.POST['NoteTextArea']
    NoteRecord.IssueStatus=request.POST['IssueStatus%s' % IssueNo]
    NoteRecord.save()
    request.session['Messages']=['Text Added to Issue %d' % NoteRecord.id]
    return HttpResponseRedirect('/WillowTree/%s/%s/ViewPupilIssues/%s/' % (school, AcYear, PupilNo))


def notesReportCSV(request, school, AcYear, GroupName):
    ''' Creates a CSV or html report showing filtered pupil notes '''
    if request.POST['StartDate']:
        startDate = datetime.datetime.strptime(request.POST['StartDate'],"%Y-%m-%d")
        startD = "%s-%s-%s" % (request.POST['StartDate'].split('-')[2],
                               request.POST['StartDate'].split('-')[1],
                               request.POST['StartDate'].split('-')[0])
    else:
        startDate = ''
        startD = 'none'
    if request.POST['StopDate']:
        endDate = datetime.datetime.strptime(request.POST['StopDate'],"%Y-%m-%d")
        stopD = "%s-%s-%s" % (request.POST['StopDate'].split('-')[2],
                              request.POST['StopDate'].split('-')[1],
                              request.POST['StopDate'].split('-')[0])
    else:
        endDate = ''
        stopD = 'none'
    notesReportObj = NoteFunctions.notesReport(request.POST.getlist('PupilSelect'),
                                               request.POST.getlist('KWSelect2'),
                                               startDate,
                                               endDate)
    notesData = notesReportObj.getData()
    today = datetime.datetime.now()
    todaysDate = '%s-%s-%s' % (today.day, today.month, today.year)
    if "html" in request.POST:
        c = {'group_name': GroupName,
             'school': school,
             'today_date': todaysDate,
             'start_date': startD,
             'stop_date': stopD,
             'data': notesData}
        return render_to_response('Reports/notes_report.html',
                                  c,
                                  context_instance=RequestContext(request,processors=[SetMenu]))
    else:
        response= HttpResponse(mimetype="text/csv")
        response_writer= csv.writer(response)
        response_writer.writerow(['Pupil Id',
                                  'Forename',
                                  'Surname',
                                  'Filter Date Range',
                                  'Keywords',
                                  'Note Text',
                                  'IsAnIssue',
                                  'IssueStatus',
                                  'RaisedBy',
                                  'RaisedDate',
                                  'ModifiedBy',
                                  'ModifiedDate'])
        for i in notesData:
            response_writer.writerow([i.PupilId.id,
                                      i.PupilId.Forename,
                                      i.PupilId.Surname,
                                      '%s - %s' % (startD, stopD),
                                      i.NoteId.Keywords,
                                      strip_tags(i.NoteId.NoteText),
                                      i.NoteId.IsAnIssue,
                                      i.NoteId.IssueStatus,
                                      '%s %s' % (i.NoteId.RaisedBy.Forename,i.NoteId.RaisedBy.Surname),
                                      str(i.NoteId.RaisedDate).split(' ')[0],
                                      i.NoteId.ModifiedBy,
                                      str(i.NoteId.ModifiedDate).split(' ')[0]])
        response['Content-Disposition'] = 'attachment; filename=notesReport_%s.csv' % todaysDate
        return response


def notes_report(request, school, ac_year):
    """
    Generates a notes report in json form filtered only on a start and end date
    """
    pupil_list = request.POST.getlist('PupilSelect')
    start_date = request.POST['start_date']
    end_date = datetime.datetime.strptime(request.POST['end_date'], "%Y-%m-%d")
    end_date_plus_one = end_date+datetime.timedelta(days=1)
    end_date_plus_one = end_date_plus_one.date().isoformat()
    notes_data = PupilNotes.objects.filter(PupilId__id__in=pupil_list,
                                           NoteId__RaisedDate__range=(start_date,
                                                                      end_date_plus_one),
                                           Active='True').order_by('PupilId__Surname',
                                                                   '-NoteId__RaisedDate',
                                                                   '-NoteId__ModifiedDate')
    data_as_json = serializers.serialize("json",
                                         notes_data,
                                         indent=2,
                                         use_natural_keys=True)
    c = {'notes_data': data_as_json,
         'school': MenuTypes.objects.get(Name=school).SchoolId.Name,
         'ac_year': ac_year,
         'date_range': '%s - %s' % (start_date, end_date.date().isoformat())}
    context = RequestContext(request, processors=[SetMenu])
    return render_to_response('Reports/NotesReport.html', c,
                              context_instance=context)


def view_note_notifications_for_a_staff_group(request, staff_group):
    """
    View note notifications for a staff group.
    """
    try:
        note_notification_objects = NoteNotification.objects.filter(NotificationGroup__Name=staff_group)
        return_data = serializers.serialize('json', note_notification_objects,
                                            indent=2,
                                            use_natural_keys=True)
        return HttpResponse(return_data, 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def add_note_notification(request):
    """
    Add note notification
    """
    try:
        note_notification = NoteNotification(Keywords=request.POST['keywords'],
                                             Pupilgroup=Group.objects.get(Name=request.POST['pupil_group']),
                                             NotificationGroup=Group.objects.get(Name=request.POST['staff_group']))
        note_notification.save()
        return HttpResponse(simplejson.dumps({'message_from_server': 'SUCCESS: new note notification created'}),
                            'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def delete_note_notification(request, object_id):
    """
    Delete note notification
    """
    try:
        note_notification = NoteNotification.objects.get(id=object_id)
        note_notification.delete()
        return HttpResponse(simplejson.dumps({'message_from_server': 'SUCCESS: note notification deleted'}),
                            'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def view_note_to_issues_for_a_staff_group(request, staff_group):
    """
    View note to issues for a staff group.
    """
    try:
        note_to_issue_objects = NoteToIssue.objects.filter(NotificationGroup__Name=staff_group)
        return_data = serializers.serialize('json', note_to_issue_objects,
                                            indent=2,
                                            use_natural_keys=True)
        return HttpResponse(return_data, 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def add_note_to_issue(request):
    """
    Add note to issue
    """
    try:
        note_to_issue = NoteToIssue(Keywords=request.POST['keywords'],
                                    Pupilgroup=Group.objects.get(Name=request.POST['pupil_group']),
                                    NotificationGroup=Group.objects.get(Name=request.POST['staff_group']))
        note_to_issue.save()
        return HttpResponse(simplejson.dumps({'message_from_server': 'SUCCESS: new note to issue created'}),
                            'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def delete_note_to_issue(request, object_id):
    """
    Delete note to issue
    """
    try:
        note_to_issue = NoteToIssue.objects.get(id=object_id)
        note_to_issue.delete()
        return HttpResponse(simplejson.dumps({'message_from_server': 'SUCCESS: note to issue deleted'}),
                            'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def note_notifications_home_page(request, school, ac_year):
    """
    Displays home page from the staff attendance app.
    """
    c = {'school': school,
         'AcYear': ac_year}
    con_inst = RequestContext(request, processors=[SetMenu])
    return render_to_response('note_notifications_home_page.html', c,
                              context_instance=con_inst)