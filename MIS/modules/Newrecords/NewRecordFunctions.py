'''
Updated by DBMGR 20/03/2013
    Added FamilyRecordName, a recusive function that returns a unique
    family name
'''
from MIS.models import *
from MIS.modules.PupilRecs import *

class generateNewEmailAddress():
    ''' This class generates an Email address for a person based upon
    their type (eg: Pupil), firstname and surname. This cross references
    the database to check no clashes can occur'''
    def __init__(self, firstname, surname, school):
        self.firstname = firstname
        self.surname = surname
        self.schoolName = MenuTypes.objects.get(Name=school).SchoolId.Name

    def pupilEmail(self):
        ''' Generates a unique Email address for a single pupil'''
        EList = []
        for i in Pupil.objects.all():
            EList.append(i.EmailAddress.lower())
        emailAddressPartSchool = self.schoolName[:3]

        emailAddressPart2 = "%s.thomasvle.org" % emailAddressPartSchool
        emailAddressPart1 = '%s%s' % (self.firstname[0], self.surname[:3])
        for i in range(1, 21):
            pupilEmail = "%s%d@%s" % (emailAddressPart1, i, emailAddressPart2)
            if pupilEmail.lower() not in EList:
                break
            else:
                pass
        return pupilEmail

def FamilyRecordName(FamilyName,FamilyNumber=None):
    if FamilyNumber==None:
        if not Family.objects.filter(FamilyName=FamilyName).exists():
            return FamilyName
        else:
            return FamilyRecordName(FamilyName,2)
    else:
        if not Family.objects.filter(FamilyName="%s-%d" % (FamilyName,FamilyNumber)).exists():
            return "%s-%d" % (FamilyName,FamilyNumber)
        else:
            return FamilyRecordName(FamilyName,FamilyNumber+1)

def UpdatePupilCache(FamilyRec,AcYear):
    pupillist=FamilyChildren.objects.filter(FamilyId=FamilyRec)
    for pupils in pupillist:
        cache.delete('Pupil_%s' % pupils.Pupil.id)
    return