'''
Modified by DBMGR 20/03/2012 :
    added a function to NewFamily that calls a recursive function in NewRecordsfuntions
    and returns a unique family name

Modified by Roy 21/03/2012 :
    added a 'search by contact name' ability to the FindFamilies view. This
    has been tested and uploaded to the live server. Code can found at lines
    41-49
'''

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q
from django.core.cache import cache
import logging
log = logging.getLogger(__name__)

# WillowTree system imports
from MIS.modules import PupilRecs
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules import GeneralFunctions
from MIS.modules.ExtendedRecords import *
from MIS.modules.FamilyRecord import *
from MIS.modules.Newrecords import NewRecordFunctions
from MIS.modules.emailFromWillowTree.emailFunctions import EmailToRingwood


def FamilySearchForm(request,school,AcYear):
    '''View to return the Family search page'''
    c={'school':school,'AcYear':AcYear}
    return render_to_response('FamilySearchForm.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def FindFamilies(request,school,AcYear):
    ''' View to proccess the results of the FamilySearchPage and display the results'''
    c={'school':school,'AcYear':AcYear}
    if 'Postcode' in request.POST:
        FamilyList = Family.objects.filter(Address__PostCode__icontains=request.POST['Postcode'],
                                           Active=True)
    if 'Surname' in request.POST:
        FamilyList = Family.objects.filter(FamilyName__icontains=request.POST['Surname'],
                                           Active=True)
    if 'Contact' in request.POST:
        FamilyList = Family.objects.filter(familycontact__Contact__Surname__icontains=request.POST['Contact'],
                                           Active=True)
    if 'car_reg_details' in request.POST:
        contact_id_list = []
        for i in Contact.objects.all():
            try:
                car_reg = ExtendedRecord('Contact', i.id).ReadExtentionSimple('ContactExtra')['car_reg_details'][0]
                if car_reg == request.POST['car_reg_details']:
                    contact_id_list.append(i.id)
            except:
                pass
        FamilyList = Family.objects.filter(familycontact__Contact__id__in=contact_id_list,
                                           Active=True)
    if len(FamilyList) > 0:
        c.update({'FamilyList':FamilyList})
    else:
        c.update({'errmsg':['No matching records found']})
    return render_to_response('FamilySelect.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def EditFamily(request,school,AcYear,FamilyNo):
    '''View to create the Edit Family page. This page is the main working area
    to view and update family Details'''
    # getting nationality list for new contacts popout
    nationality = ExtentionFields.objects.get(Name="Nationality")
    nationality = nationality.PickList.Data
    nationality = str(nationality)
    nationality = nationality.replace('],[', ']#,#[')
    nationality = nationality.replace('\'', '')
    nationality = nationality.replace('[[', '')
    nationality = nationality.split(']#,#[')
    nationalityList = []
    for i in nationality:
        temp = i.split(',')
        if ']]' in temp[1]:
            temp[1] = temp[1].replace(']]', '')
        if ' ' in temp[1]:
            temp[1] = temp[1].replace(' ', '')
        nationalityList.append(temp)
    nationalityList2 = []
    for i in nationalityList:
        nationalityList2.append(i[1])
    # end
    c={'school':school,
       'AcYear':AcYear,
       'FamilyDetails':FamilyRecord(request,AcYear,FamilyId=FamilyNo),
       'Relationships':GeneralFunctions.RelationshipTypes()}
    c.update({'nationalityList': nationalityList})
    c.update({'languageList': eval(ExtentionPickList.objects.get(Name="PrimaryLanguage").Data)})
    c.update({'nationalityList2': nationalityList2})
    log.warn('Family (%s) was Updated by %s' % (FamilyNo,request.user.username))
    return render_to_response('EditFamilyRecord.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def NewFamilyAddress(request,school,AcYear,FamilyNo):
    ''' This view proccesses requests from the Edit Family page to add new Addresses
    to a family record (Postal Address)'''
    NewAddress=Address(HomeSalutation=request.POST['HomeSalutation'],PostalTitle=request.POST['PostalTitle'],
                      AddressLine1=request.POST['AddressLine1'],AddressLine2=request.POST['AddressLine2'],
                      PostCode=request.POST['PostCode'],Phone1=request.POST['Phone1'],EmailAddress=request.POST['EmailAddress'])
    NewAddress.save()
    if 'AddressLine3' in request.POST:
        NewAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        NewAddress.AddressLine4=request.POST['AddressLine4']
    if 'Country' in request.POST:
        NewAddress.Country=request.POST['Country']
    if 'Phone2' in request.POST:
        NewAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        NewAddress.Phone3=request.POST['Phone3']
    if 'Note' in request.POST:
        NewAddress.Note=request.POST['Note']
    NewAddress.save()
    FamilyRec=Family.objects.get(id=int(FamilyNo))
    FamilyRec.Address.add(NewAddress)
    FamilyRec.save()
    log.warn('Address (%s) was added to Family (%s) by %s' % (FamilyRec.id,NewAddress.id,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%d/' % (school,AcYear,FamilyRec.id))


def ModifyFamilyAddress(request,school,AcYear,FamilyNo,AddressNo):
    ''' This view procceses forms from the Edit Family page, for modifying
    an existing family Postal address

    This has been updated so to send an email to Ringwood informing them of a change in family address - Roy
    '''
    FamilyAddress = Address.objects.get(id=int(AddressNo))

    # Compiling email to Ringwood
    old_address_details = """Family Name: %s,
Home Salutation: %s,
Postal Title: %s,
Address Line 1: %s,
Address Line 2: %s,
Address Line 3: %s,
Address Line 4: %s,
PostCode: %s,
Phone 1: %s,
Email Address: %s """ % (Family.objects.get(id=FamilyNo).FamilyName,
                         FamilyAddress.HomeSalutation,
                         FamilyAddress.PostalTitle,
                         FamilyAddress.AddressLine1,
                         FamilyAddress.AddressLine2,
                         FamilyAddress.AddressLine3,
                         FamilyAddress.AddressLine4,
                         FamilyAddress.PostCode,
                         FamilyAddress.Phone1,
                         FamilyAddress.EmailAddress)
    new_address_details = """Family Name: %s,
Home Salutation: %s,
Postal Title: %s,
Address Line 1: %s,
Address Line 2: %s,
Address Line 3: %s,
Address Line 4: %s,
PostCode: %s,
Phone 1: %s,
Email Address: %s""" % (Family.objects.get(id=FamilyNo).FamilyName,
                        request.POST['HomeSalutation'],
                        request.POST['PostalTitle'],
                        request.POST['AddressLine1'],
                        request.POST['AddressLine2'],
                        request.POST['AddressLine3'],
                        request.POST['AddressLine4'],
                        request.POST['PostCode'],
                        request.POST['Phone1'],
                        request.POST['EmailAddress'])
    email_body = "Family address change:\n\nOLD ADDRESS:\n%s\n\nNEW ADDRESS:\n%s" % (old_address_details,
                                                                                     new_address_details)
    FamilyAddress.HomeSalutation = request.POST['HomeSalutation']
    FamilyAddress.PostalTitle = request.POST['PostalTitle']
    FamilyAddress.AddressLine1 = request.POST['AddressLine1']
    FamilyAddress.AddressLine2 = request.POST['AddressLine2']
    FamilyAddress.PostCode = request.POST['PostCode']
    FamilyAddress.Phone1 = request.POST['Phone1']
    FamilyAddress.EmailAddress = request.POST['EmailAddress']
    FamilyAddress.save()
    if 'AddressLine3' in request.POST:
        FamilyAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        FamilyAddress.AddressLine4=request.POST['AddressLine4']
    if 'Country' in request.POST:
        FamilyAddress.Country=request.POST['Country']
    if 'Phone2' in request.POST:
        FamilyAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        FamilyAddress.Phone3=request.POST['Phone3']
    if 'Note' in request.POST:
        FamilyAddress.Note=request.POST['Note']
    FamilyAddress.save()
    log.warn('Address (%s) was Updated by %s' % (AddressNo,request.user.username))

    # email Ringwood
    send_email = EmailToRingwood(email_body)
    send_email.start()
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def RemoveFamilyAddress(request,school,AcYear,FamilyNo,AddressNo):
    ''' This view proccesses a request from the Edit Family page, to remove a
    postal address. It does not delete an address, but detaches the Family Record
    from the said address'''
    FamilyRec=Family.objects.get(id=int(FamilyNo))
    FamilyRec.Address.remove(Address.objects.get(id=int(AddressNo)))
    log.warn('Address (%s) was removed from Family (%s) by %s' % (AddressNo,FamilyNo,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def FindExistingFamilyAddress(request,school,AcYear,FamilyNo):
    ''' Create the form page for searching existing addresses that can be added to
    a family record as a postal address '''
    c={'school':school,'AcYear':AcYear,'FamilyId':FamilyNo}
    c.update({'AddressList':Address.objects.filter(PostCode__icontains=request.POST['PostCode'])})
    return render_to_response('SelectFamilyAddress.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def AddExistingFamilyAddress(request,school,AcYear,FamilyNo,AddressNo):
    ''' View to proccess the results from FindExistsingFamilyAddress, to add the
    selected address to the family record'''
    FamilyRec=Family.objects.get(id=int(FamilyNo))
    FamilyRec.Address.add(Address.objects.get(id=int(AddressNo)))
    log.warn('Address (%) was added to Family (%s) by %s' % (AddressNo,FamilyNo,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def NewFamilyContact(request,school,AcYear,FamilyNo):
    ''' View to proccess a form from EditFamily, to add a new Contact to the Family Record'''
    deleteEalPupilCache = False

    # CHECK THE NEW CONTACT DOES NOT HAVE THE SAME EMAIL ADDRESS AS AN EXISTING CONTACT
    if Contact.objects.filter(EmailAddress__iexact=request.POST['EmailAddress']).exists() and len(request.POST['EmailAddress']) > 3:
        error_messages = ['The following contacts with <span style="color:red;">%s</span> as their email address already exists:<br />' % request.POST['EmailAddress']]
        for contact in Contact.objects.filter(EmailAddress__iexact=request.POST['EmailAddress']):
            family_contact = FamilyContact.objects.filter(Contact__EmailAddress__iexact=request.POST['EmailAddress'])
            error_messages.append('%s from <a href="https://willowtree.thomas-s.co.uk/WillowTree/%s/%s/EditFamily/%s/" target="_blank">%s</a> family' % (contact.FullName(),
                                                                                                                                                         school,
                                                                                                                                                         AcYear,
                                                                                                                                                         family_contact[0].FamilyId.id,
                                                                                                                                                         family_contact[0].FamilyId.FamilyName))
            error_messages.append('<br />Sorry but you will have to remove or change these email addresses before adding this contact<br />')
            error_messages.append('<span style="font-weight:1.0em;color:black;">HINT: Enter a blank space to remove an email address</span>')
            error_messages.append('<span style="font-weight:1.0em;color:black;">HINT: It is important for contacts 1 and 2 to have email accounts to log into the VLE</span>')
        request.session['Messages'] = error_messages

    else:
        newContact = Contact(Title=request.POST['Title'], Forename=request.POST['Forename'],
                             Surname=request.POST['Surname'],
                             Gender=request.POST['Gender'],
                             EmailAddress=request.POST['EmailAddress'])
        newContact.save()
        if 'OtherNames' in request.POST:
            newContact.OtherNames=request.POST['OtherNames']
            newContact.save()
        newFamilyContact=FamilyContact(FamilyId=Family.objects.get(id=int(FamilyNo)),
                                       Contact=newContact,
                                       Relationship=FamilyRelationship.objects.get(Type=request.POST['Relationship']))
        if "SMS" in request.POST:
            newFamilyContact.SMS = True
        if "EMAIL" in request.POST:
            newFamilyContact.EMAIL = True
        newFamilyContact.save()
        if request.POST['Priority'].find('None') < 0:
            newFamilyContact.Priority=int(request.POST['Priority'])
            newFamilyContact.save()
            # adding mobile number, Nationality and Occupation to new contact

        contactExtRec = ExtendedRecord('Contact', newContact.id)
        if request.POST['mobileNumber']:
            contactExtRec.WriteExtention('ContactExtra', {'mobileNumber': request.POST['mobileNumber']})
        if request.POST['occupation']:
            contactExtRec.WriteExtention('ContactExtra', {'occupation': request.POST['occupation']})
        if request.POST['car_reg_details']:
            contactExtRec.WriteExtention('ContactExtra', {'car_reg_details': request.POST['car_reg_details']})
        if request.POST['Nationality']:
            contactExtRec.WriteExtention('ContactExtra', {'Nationality': request.POST['Nationality']})
        if request.POST['Language']:
            contactExtRec.WriteExtention('ContactExtra', {'PrimaryLanguage': request.POST['Language']})
        deleteEalPupilCache = True
    # end
    # delete pupil objects in cache
        for i in FamilyChildren.objects.filter(FamilyId=FamilyNo):
            cache.delete('PupilEAL_%s' % i.Pupil.id)
            cache.delete('PupilSEN_%s' % i.Pupil.id)
            cache.delete('Pupil_%s' % i.Pupil.id)
    # end
        log.warn('Contact (%s) was added to Family (%s) by %s' % (newFamilyContact.id,FamilyNo,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def RemoveFamilyContact(request,school,AcYear,FamilyNo,ContactNo):
    ''' View to proccess a request from EditFamily, to remove them from the family
    Record'''
    FamilyCont=FamilyContact.objects.get(FamilyId__id=int(FamilyNo),Contact__id=int(ContactNo))
    FamilyCont.delete()
    for i in FamilyChildren.objects.filter(FamilyId=FamilyNo):
        cache.delete('PupilEAL_%s' % i.Pupil.id)
        cache.delete('PupilSEN_%s' % i.Pupil.id)
        cache.delete('Pupil_%s' % i.Pupil.id)
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))


def contact_skills_bank(request, school, ac_year, contact_id, family_id):
    """
    For editing the contact skills bank.
    """
    contact_extension_record = ExtendedRecord('Contact', int(contact_id), request)
    contact_extension_record.WriteExtention('ContactExtra',
                                            {"contact_skills_bank": request.POST['contact_skills_bank']})
    # delete pupil objects in cache
    for i in FamilyChildren.objects.filter(FamilyId=family_id):
        cache.delete('PupilEAL_%s' % i.Pupil.id)
        cache.delete('PupilSEN_%s' % i.Pupil.id)
        cache.delete('Pupil_%s' % i.Pupil.id)
    # end
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school, ac_year, family_id))


def ModifyFamilyContact(request,school,AcYear,FamilyNo,ContactNo):
    ''' View to proccess a form from EditFamily to modify a Family Contact Record
    '''
    FamilyCont=FamilyContact.objects.get(FamilyId__id=int(FamilyNo),Contact__id=int(ContactNo))
    FamilyCont.Relationship=FamilyRelationship.objects.get(Type=request.POST['Relationship'])
    if request.POST['Priority'].find('None') < 0:
        FamilyCont.Priority=int(request.POST['Priority'])
    FamilyCont.Contact.Title=request.POST['Title']
    FamilyCont.Contact.Forename=request.POST['Forename']
    FamilyCont.Contact.Surname=request.POST['Surname']
    FamilyCont.Contact.Gender=request.POST['Gender']
    FamilyCont.Contact.EmailAddress=request.POST['EmailAddress']
    if "SMS" in request.POST:
        FamilyCont.SMS = True
    else:
        FamilyCont.SMS = False
    if "EMAIL" in request.POST:
        FamilyCont.EMAIL = True
    else:
        FamilyCont.EMAIL = False
    FamilyCont.Contact.save()
    FamilyCont.save()
    # adding mobile number, Nationality etc.. to new contact
    contactExtRec = ExtendedRecord('Contact', int(ContactNo),request)
    Updatedata={}
    DoUpdate=False
    if request.POST['mobileNumber']:
        Updatedata['mobileNumber']=request.POST['mobileNumber']
        DoUpdate=True
    if request.POST['occupation']:
        Updatedata['occupation']=request.POST['occupation']
        DoUpdate=True
    if request.POST['car_reg_details']:
        Updatedata['car_reg_details']=request.POST['car_reg_details']
        DoUpdate=True
    if request.POST['Nationality']:
        Updatedata['Nationality']=request.POST['Nationality']
        DoUpdate=True
    if request.POST['Language']:
        Updatedata['PrimaryLanguage']=request.POST['Language']
        DoUpdate=True
    if DoUpdate:
        contactExtRec.WriteExtention('ContactExtra', Updatedata)
    # delete pupil objects in cache
    for i in FamilyChildren.objects.filter(FamilyId=FamilyNo):
        cache.delete('PupilEAL_%s' % i.Pupil.id)
        cache.delete('PupilSEN_%s' % i.Pupil.id)
        cache.delete('Pupil_%s' % i.Pupil.id)
    # end
    NewRecordFunctions.UpdatePupilCache(FamilyCont.FamilyId,AcYear)
    log.warn('Contact (%s) was modified in reference to  Family (%s) by %s' % (FamilyCont.Contact.id,FamilyNo,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def ModifyContactAddressOld(request,school,AcYear,FamilyNo,AddressNo):
    ''' Used to modify an address already connected to a Family contact. This is
    triggered from the EditFamily Page'''
    OldAddress=Address.objects.get(id=int(AddressNo))
    OldAddress.HomeSalutation=request.POST['HomeSalutation']
    OldAddress.PostalTitle=request.POST['PostalTitle']
    OldAddress.AddressLine1=request.POST['AddressLine1']
    OldAddress.AddressLine2=request.POST['AddressLine2']
    if 'AddressLine3' in request.POST:
        OldAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        OldAddress.AddressLine4=request.POST['AddressLine4']
    OldAddress.PostCode=request.POST['PostCode']
    OldAddress.Phone1=request.POST['Phone1']
    if 'Phone2' in request.POST:
        OldAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        OldAddress.Phone3=request.POST['Phone3']
    OldAddress.EmailAddress=request.POST['EmailAddress']
    if 'Note' in request.POST:
        OldAddress.Note=request.POST['Note']

    OldAddress.save()

    # deletes pupil objects stored in cache
    for i in FamilyChildren.objects.filter(FamilyId=FamilyNo):
        cache.delete('PupilEAL_%s' % i.Pupil.id)
        cache.delete('PupilSEN_%s' % i.Pupil.id)
        cache.delete('Pupil_%s' % i.Pupil.id)

    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def ModifyContactAddressNew(request,school,AcYear,FamilyNo,ContactNo):
    ''' Triggered from the EditFamily page, to add a new postal address to a
    family contact'''
    NewAddress=Address(HomeSalutation=request.POST['HomeSalutation'],PostalTitle=request.POST['PostalTitle'],
                      AddressLine1=request.POST['AddressLine1'],AddressLine2=request.POST['AddressLine2'],
                      PostCode=request.POST['PostCode'],Phone1=request.POST['Phone1'],EmailAddress=request.POST['EmailAddress'])
    NewAddress.save()
    if 'AddressLine3' in request.POST:
        NewAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        NewAddress.AddressLine4=request.POST['AddressLine4']
    if 'Phone2' in request.POST:
        NewAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        NewAddress.Phone3=request.POST['Phone3']
    if 'Note' in request.POST:
        NewAddress.Note=request.POST['Note']
    NewAddress.save()
    ContactRec=Contact.objects.get(id=int(ContactNo))
    ContactRec.Address=(NewAddress)
    ContactRec.save()

    # deletes pupil objects stored in cache
    for i in FamilyChildren.objects.filter(FamilyId=FamilyNo):
        cache.delete('PupilEAL_%s' % i.Pupil.id)
        cache.delete('PupilSEN_%s' % i.Pupil.id)
        cache.delete('Pupil_%s' % i.Pupil.id)

    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def ModifyContactAddressFromFamily(request,school,AcYear,FamilyNo,ContactNo,AddressNo):
    ''' Used to add a family postal address as a contact address. Triggered from
    EditFamily'''
    ContactRec=Contact.objects.get(id=int(ContactNo))
    ContactRec.Address=(Address.objects.get(id=int(AddressNo)))
    ContactRec.save()

    # deletes pupil objects stored in cache
    for i in FamilyChildren.objects.filter(FamilyId=FamilyNo):
        cache.delete('PupilEAL_%s' % i.Pupil.id)
        cache.delete('PupilSEN_%s' % i.Pupil.id)
        cache.delete('Pupil_%s' % i.Pupil.id)

    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def ModifyContactWorkAddressOld(request,school,AcYear,FamilyNo,AddressNo):
    ''' Used to modify an exitsing contact work address. Triggered from EditFamily'''
    OldAddress=Address.objects.get(id=int(AddressNo))
    OldAddress.HomeSalutation=request.POST['HomeSalutation']
    OldAddress.PostalTitle=request.POST['PostalTitle']
    OldAddress.AddressLine1=request.POST['AddressLine1']
    OldAddress.AddressLine2=request.POST['AddressLine2']
    if 'AddressLine3' in request.POST:
        OldAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        OldAddress.AddressLine4=request.POST['AddressLine4']
    OldAddress.PostCode=request.POST['PostCode']
    OldAddress.Phone1=request.POST['Phone1']
    if 'Phone2' in request.POST:
        OldAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        OldAddress.Phone3=request.POST['Phone3']
    OldAddress.EmailAddress=request.POST['EmailAddress']
    if 'Note' in request.POST:
        OldAddress.Note=request.POST['Note']

    OldAddress.save()

    # deletes pupil objects stored in cache
    for i in FamilyChildren.objects.filter(FamilyId=FamilyNo):
        cache.delete('PupilEAL_%s' % i.Pupil.id)
        cache.delete('PupilSEN_%s' % i.Pupil.id)
        cache.delete('Pupil_%s' % i.Pupil.id)

    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def ModifyContactWorkAddressNew(request,school,AcYear,FamilyNo,ContactNo):
    ''' Used to add a new Work address to a family contact.Triggered from EditFamily'''
    NewAddress=Address(HomeSalutation=request.POST['HomeSalutation'],PostalTitle=request.POST['PostalTitle'],
                      AddressLine1=request.POST['AddressLine1'],AddressLine2=request.POST['AddressLine2'],
                      PostCode=request.POST['PostCode'],Phone1=request.POST['Phone1'],EmailAddress=request.POST['EmailAddress'])
    NewAddress.save()
    if 'AddressLine3' in request.POST:
        NewAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        NewAddress.AddressLine4=request.POST['AddressLine4']
    if 'Phone2' in request.POST:
        NewAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        NewAddress.Phone3=request.POST['Phone3']
    if 'Note' in request.POST:
        NewAddress.Note=request.POST['Note']
    NewAddress.save()
    ContactRec=Contact.objects.get(id=int(ContactNo))
    ContactRec.WorkAddress=(NewAddress)
    ContactRec.save()

    # deletes pupil objects stored in cache
    for i in FamilyChildren.objects.filter(FamilyId=FamilyNo):
        cache.delete('PupilEAL_%s' % i.Pupil.id)
        cache.delete('PupilSEN_%s' % i.Pupil.id)
        cache.delete('Pupil_%s' % i.Pupil.id)

    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def ModifyContactWorkAddressFromFamily(request,school,AcYear,FamilyNo,ContactNo,AddressNo):
    ''' Used to add a Family Postal address as a contacts work address'''
    ContactRec=Contact.objects.get(id=int(ContactNo))
    ContactRec.WorkAddress=(Address.objects.get(id=int(AddressNo)))
    ContactRec.save()

    # deletes pupil objects stored in cache
    for i in FamilyChildren.objects.filter(FamilyId=FamilyNo):
        cache.delete('PupilEAL_%s' % i.Pupil.id)
        cache.delete('PupilSEN_%s' % i.Pupil.id)
        cache.delete('Pupil_%s' % i.Pupil.id)

    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def NewFamily(request,AcYear,school):
    ''' View to proccess the form to create a brand new family. This consists of a family name and
    a single postal address.'''
    NewAddress=Address(HomeSalutation=request.POST['HomeSalutation'],PostalTitle=request.POST['PostalTitle'],
                      AddressLine1=request.POST['AddressLine1'],AddressLine2=request.POST['AddressLine2'],
                      PostCode=request.POST['PostCode'],Phone1=request.POST['Phone1'],EmailAddress=request.POST['EmailAddress'])
    NewAddress.save()
    if 'AddressLine3' in request.POST:
        NewAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        NewAddress.AddressLine4=request.POST['AddressLine4']
    if 'Phone2' in request.POST:
        NewAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        NewAddress.Phone3=request.POST['Phone3']
    if 'Note' in request.POST:
        NewAddress.Note=request.POST['Note']
    NewAddress.save()
    FamilyRec=Family(FamilyName=NewRecordFunctions.FamilyRecordName(request.POST['FamilyName']))
    FamilyRec.save()
    FamilyRec.Address.add(NewAddress)
    FamilyRec.save()
    log.warn('New Family (%s) added by %s' % (FamilyRec.id,request.user.username))

    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyRec.id))

def NewPupilRecord(request, AcYear, school, FamilyNo):
    ''' Proccesses the form from EditFamily, to add a new pupil to the system. All
    new pupils are automatically put in Applicants holding'''
    NewPupil = Pupil(Surname=request.POST['Surname'],
                     Forename=request.POST['Forename'],
                     DateOfBirth=request.POST['DateOfBirth'],
                     Gender=request.POST['Gender'],
                     RegistrationDate=request.POST['registrationDate'])
    NewPupil.save()
    if 'Title' in request.POST:
        NewPupil.Title=request.POST['Title']
    if 'NickName' in request.POST:
        NewPupil.NickName=request.POST['NickName']
    if 'OtherNames' in request.POST:
        NewPupil.OtherNames=request.POST['OtherNames']
    if 'Picture' in request.FILES:
        NewPupil.Picture=request.FILES['Picture']
    #new email address generation...
    pForename = request.POST['Forename']
    pSurname = request.POST['Surname']
    eObj = NewRecordFunctions.generateNewEmailAddress(pForename, pSurname, school)
    NewPupil.EmailAddress = eObj.pupilEmail()
    #end
    NewPupil.save()
    AssignGroup=PupilGroup(AcademicYear='Holding', Pupil=NewPupil,Group=Group.objects.get(Name='Applicants.Holding'))
    AssignGroup.save()
    NewFamilyChild=FamilyChildren(FamilyId=Family.objects.get(id=int(FamilyNo)), Pupil=NewPupil)
    NewFamilyChild.save()
    log.warn('New Pupil (%s) added by %s' % (NewPupil.id,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def UpdateApplicantExtra(request, AcYear, school, PupilNo):
    ''' Used to add extra info to an applicants pupil Record'''
    ExtraFields=ExtendedRecord(BaseType='Pupil',BaseId=int(PupilNo))
    if 'ApplicantSource' in request.POST and request.POST['ApplicantSource'] != '':
        ExtraFields.WriteExtention('PupilExtra',{'ApplicantSource':request.POST['ApplicantSource']})
    if 'ApplicantInterviewTime' in request.POST and request.POST['ApplicantInterviewTime'] != '':
        ExtraFields.WriteExtention('PupilExtra',{'ApplicantInterviewTime':request.POST['ApplicantInterviewTime']})
    if 'ApplicantInterviewDate' in request.POST and request.POST['ApplicantInterviewDate'] != '':
        ExtraFields.WriteExtention('PupilExtra',{'ApplicantInterviewDate':request.POST['ApplicantInterviewDate']})
    if 'ApplicantStandardisedScore' in request.POST and request.POST['ApplicantStandardisedScore'] != '':
        ExtraFields.WriteExtention('PupilExtra',{'ApplicantStandardisedScore':request.POST['ApplicantStandardisedScore']})
    if 'applicantAcceptanceDeadline' in request.POST and request.POST['applicantAcceptanceDeadline'] != '':
        ExtraFields.WriteExtention('PupilExtra',{'applicantAcceptanceDeadline':request.POST['applicantAcceptanceDeadline']})
    if 'applicantPercentageScore' in request.POST and request.POST['applicantPercentageScore'] != '':
        ExtraFields.WriteExtention('PupilExtra',{'applicantPercentageScore':request.POST['applicantPercentageScore']})
    if 'applicantNotes' in request.POST and request.POST['applicantNotes'] != '':
        ExtraFields.WriteExtention('PupilExtra',{'applicantNotes':request.POST['applicantNotes']})
    PupilRecs.PupilRecord(PupilNo, AcYear)
    cache.delete('PupilEAL_%s' % str(PupilNo))
    cache.delete('PupilSEN_%s' % str(PupilNo))
    cache.delete('Pupil_%s' % str(PupilNo))
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school, AcYear, PupilNo))


def addExistingContactSearch(request, school, AcYear, FamilyId):
    ''' creates a list of existing contacts for a user to choose from when
    adding to a family'''
    contactName = request.POST['contactSearch'].split(' ')
    contactSearchObjs = []
    for i in contactName:
        temp = Contact.objects.filter(Q(Forename__icontains=i, Active=True) | Q(Surname__icontains=i, Active=True))
        for contact in temp:
            contactSearchObjs.append(copy.deepcopy(contact))
    c = {'school': school, 'AcYear': AcYear, 'FamilyId': FamilyId}
    c.update({'contactSearchObjs': contactSearchObjs})
    url = 'addExistingContactSearchResults.html'
    conIns = RequestContext(request, processors=[SetMenu])
    return render_to_response(url, c, context_instance=conIns)


def addExistingContactAdd(request, school, AcYear, familyId):
    ''' adds an existing contact to a family '''
    conList = []
    for k, v in request.POST.iteritems():
        if 'contact_id' in k:
            conList.append(v)
    for conId in conList:
        if not FamilyContact.objects.filter(FamilyId=int(familyId), Contact=int(conId)).exists():
            famConObj = FamilyContact(FamilyId=Family.objects.get(id=int(familyId)),
                                      Contact=Contact.objects.get(id=int(conId)),
                                      Relationship=FamilyRelationship.objects.get(Type='Other'))
            famConObj.save()
    url = '/WillowTree/%s/%s/EditFamily/%d/'
    return HttpResponseRedirect(url % (school, AcYear, int(familyId)))


def addExistingPupilSearch(request, school, AcYear, FamilyId):
    ''' creates a list of existing pupils for a user to choose from when
    adding to a family'''
    pupilName = request.POST['pupilSearch'].split(' ')
    pupilSearchObjs = []
    for i in pupilName:
        temp = Pupil.objects.filter(Q(Forename__icontains=i,
                                                        Active=True) | Q(Surname__icontains=i,
                                                        Active=True) | Q(NickName__icontains=i,
                                                        Active=True))
        for pupil in temp:
            pupilSearchObjs.append(copy.deepcopy(pupil))
    c = {'school': school, 'AcYear': AcYear, 'FamilyId': FamilyId}
    c.update({'pupilSearchObjs': pupilSearchObjs})
    url = 'addExistingPupilSearchResults.html'
    conIns = RequestContext(request, processors=[SetMenu])
    return render_to_response(url, c, context_instance=conIns)


def addExistingPupilAdd(request, school, AcYear, familyId):
    ''' adds an existing pupil to a family '''
    pupilList = []
    for k, v in request.POST.iteritems():
        if 'pupil_id' in k:
            pupilList.append(v)
    for pupilId in pupilList:
        if not FamilyChildren.objects.filter(FamilyId=int(familyId), Pupil=int(pupilId)).exists():
            amountOfFam = len(FamilyChildren.objects.filter(Pupil__id=int(pupilId)))
            amountOfFam += 1
            famConObj = FamilyChildren(FamilyId=Family.objects.get(id=int(familyId)),
                                      Pupil=Pupil.objects.get(id=int(pupilId)),
                                      FamilyType=str(amountOfFam))
            famConObj.save()
    url = '/WillowTree/%s/%s/EditFamily/%d/'
    return HttpResponseRedirect(url % (school, AcYear, int(familyId)))


def RemoveExistingPupilFromFamily(request, school, AcYear, familyId, PupilNo):
    ''' Removes a pupils from a family, but only if it is member of another
    family first...'''
    test = FamilyChildren.objects.filter(Pupil=int(PupilNo))
    if len(test) == 1:
        pass
    else:
        famObj = FamilyChildren.objects.get(FamilyId=int(familyId),
                                            Pupil=int(PupilNo))
        famObj.delete()
        families = FamilyChildren.objects.filter(Pupil=int(PupilNo))
        for family in families:
            if family.FamilyType == 1:
                pass
            else:
                temp = family
                temp.FamilyType = temp.FamilyType - 1
                temp.save()
    url = '/WillowTree/%s/%s/EditFamily/%d/'
    log.warn('Pupil (%s) was removed from Family (%s) by %s' % (PupilNo,familyId,request.user.username))
    return HttpResponseRedirect(url % (school, AcYear, int(familyId)))