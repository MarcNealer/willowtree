# Django middleware imports
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect

#WillowTree imports
from MIS.models import Staff
from MIS.modules.globalVariables import GlobalVariablesFunctions


def newPasswordAfterReset(request, school, StaffNo):
    ''' Sets a new Passphrase for a user if their Passphrase  has previously
    been reset '''
    userObj = User.objects.get(id=int(StaffNo))
    c = {'StaffId': StaffNo, 'school': school, 'UserName': userObj.username}
    context_instance = RequestContext(request)
    x = context_instance
    if not 'password' in request.POST:
        c.update({'errmsg': 'Please enter a new passphrase'})
        return render_to_response('ChangePassword.html', c, x)
    if not 'password2' in request.POST:
        c.update({'errmsg': 'No Repeat passphrase  specified'})
        return render_to_response('ChangePassword.html', c, x)
    if len(request.POST['password']) < 12:
        c.update({'errmsg': 'Sorry Your Passphrase is too short'})
        return render_to_response('ChangePassword.html', c, x)
    if len(request.POST['password'].split(' ')) < 3:
        c.update({'errmsg': 'Sorry, Not enough Words in your Passphrase'})
        return render_to_response('ChangePassword.html', c, x)
    PasswordError = False
    for words in request.POST['password'].split(' '):
        if len(words) < 3:
            PasswordError = True
    if PasswordError:
        errorMessage = 'Passphrase words have to be at least 3 characters long'
        c.update({'errmsg': errorMessage})
        return render_to_response('ChangePassword.html', c, x)
    if not request.POST['password'] == request.POST['password2']:
        c.update({'errmsg': 'Sorry, Your pass phrases don\'t match'})
        return render_to_response('ChangePassword.html', c, x)
    userObj.set_password(request.POST['password'])
    userObj.save()
    staffObj = Staff.objects.get(id=userObj.staff.id)
    staffObj.ResetPassword = 0
    staffObj.save()
    AcYear = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    StaffDetails = Staff.objects.get(User=request.user)
    school = StaffDetails.DefaultMenu.Name
    c = {'school': school}
    request.session['Messages'] = ['Passphrase successfully changed!']
    return HttpResponseRedirect('/WillowTree/%s/%s/Welcome/' % (school,
                                                                AcYear))