from MIS.models import Pupil
from MIS.modules.PupilRecs import PupilRecord
from MIS.modules.ExtendedRecords import *
from django.core.cache import cache


class MergeEALAssessmentHistoryWithEALGeneralNotes():
    """
    This class merges the EAL Assessment History with EAL General Notes.

    This is done by appending EAL Assessment History to EAL General Notes then deleting EAL Assessment History.

    NOTE!!! As this is only running once the ac_year variable is hard coded to "2013-2014"
    """
    def __init__(self):
        self.ac_year = "2013-2014"
        self.eal_list = self.__get_all_current_eal_pupils__()
        self.__merge_logic__()

    def __get_all_current_eal_pupils__(self):
        eal_list = []
        pupil_ids = Pupil.objects.all().values_list("id", flat=True)
        counter = 0
        number_of_records = len(pupil_ids)
        for pupil_id in pupil_ids:
            test_pupil_record = PupilRecord(pupil_id, self.ac_year)
            if bool(test_pupil_record.ReturnCurrentActiveEal()):
                eal_list.append(pupil_id)
            counter += 1
            print "Reading EAL data: %d of %d" % (counter, number_of_records)
        return eal_list

    def __merge_logic__(self):
        for eal_pupil in self.eal_list:
            try:
                extension_record = ExtendedRecord(BaseType='Pupil',
                                                  BaseId=eal_pupil)
                eal_assessment_history = extension_record.ReadExtention(ExtentionName='PupilEAL')['ealAssessmentHistory'][0]
                eal_general_notes = extension_record.ReadExtention(ExtentionName='PupilEAL')['ealGeneralNotes'][0]
                new_data = "%s<br /><br />%s" % (eal_general_notes, eal_assessment_history)
                extension_record.WriteExtention('PupilEAL', {'ealGeneralNotes': new_data})
                extension_record.WriteExtention('PupilEAL', {'ealAssessmentHistory': ""})
                print "SUCCESS: merging ealGeneralNotes with ealAssessmentHistory for %s" % eal_pupil
                cache.delete("PupilEAL_%s" % eal_pupil)
            except Exception as error:
                print "FAIL: %s" % error