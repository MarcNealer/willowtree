# Django middleware imports
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.core.cache import cache


# WillowTree system imports
from MIS.ViewExtras import SetMenu
from MIS.modules.EAL import EALFunctions
from MIS.modules.ExtendedRecords import *
from MIS.modules.GeneralFunctions import isUserInThisGroup
from MIS.modules.notes import NoteFunctions

# logging
import logging
log = logging.getLogger(__name__)


def PupilEALRecord(request, school, AcYear, PupilId):
    ''' displays a Pupils EAL page --
        staff members of the SEN group may edit '''
    PupilDetails = EALFunctions.PupilRecordWithEal(int(PupilId), AcYear)
    c = {'PupilDetails': PupilDetails,
         'school': school,
         'AcYear': AcYear,
         'inSEN': isUserInThisGroup(request, 'SEN'),
         'KWList': NoteFunctions.KeywordList('Notes'),
         'fromEalPage': True}
    c.update(NoteFunctions.GetPupilNotes(request,
                                         Pupil_Id=PupilId,
                                         allEalNotes=True))
    conInst = RequestContext(request, processors=[SetMenu])
    return render_to_response('PupilEALRecord.html', c,
                              context_instance=conInst)


def pupilModifyEALSpecialPermissions(request, school, AcYear, PupilId):
    ''' updates Pupil EAL Special Permissions '''
    senExtraTime = request.POST['ealExtraTime']
    senLaptop = request.POST['ealLaptop']
    senOther = request.POST['ealOther']
    pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=PupilId)
    pupilExtentionRecord.WriteExtention('PupilEAL',
                                        {'ealExtraTime': senExtraTime})
    pupilExtentionRecord.WriteExtention('PupilEAL',
                                        {'ealLaptop': senLaptop})
    pupilExtentionRecord.WriteExtention('PupilEAL',
                                        {'ealOther': senOther})
    EALFunctions.updateEalCache(PupilId, AcYear)
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def pupilModifyEALGeneralNotes(request, school, AcYear, PupilId):
    ''' updates Pupil EAL General Notes '''
    data = request.POST['ealGeneralNotes']
    pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=PupilId)
    pupilExtentionRecord.WriteExtention('PupilEAL',
                                        {'ealGeneralNotes': data})
    EALFunctions.updateEalCache(PupilId, AcYear)
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def pupilModifyEALAssessmentHistory(request, school, AcYear, PupilId):
    ''' updates Pupil EAL Assessment History '''
    data = request.POST['ealAssessmentHistory']
    pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=PupilId)
    pupilExtentionRecord.WriteExtention('PupilEAL',
                                        {'ealAssessmentHistory': data})
    EALFunctions.updateEalCache(PupilId, AcYear)
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def pupilModifyEalAreasOfConcern(request, school, AcYear, PupilId, delete=False):
    ''' modifies a pupils EAL Area of Concern record '''
    ealAreasOfConcernList = ['ealAreasOfConcernReadingLevel',
                             'ealAreasOfConcernWritingLevel',
                             'ealAreasOfConcernMathsLevel',
                             'ealAreasOfConcernListeningAndRespondingLevel',
                             'ealAreasOfConcernSocialLevel',
                             'ealAreasOfConcernGeneralLevel',
                             'ealAreasOfConcernReadingNotes',
                             'ealAreasOfConcernWritingNotes',
                             'ealAreasOfConcernMathsNotes',
                             'ealAreasOfConcernListeningAndRespondingNotes',
                             'ealAreasOfConcernSocialNotes',
                             'ealAreasOfConcernGeneralNotes',
                             'ealAreasOfConcernReadingTeacherNotes',
                             'ealAreasOfConcernWritingTeacherNotes',
                             'ealAreasOfConcernMathsTeacherNotes',
                             'ealAreasOfConcernListeningAndRespondingTeacherNotes',
                             'ealAreasOfConcernSocialTeacherNotes',
                             'ealAreasOfConcernGeneralTeacherNotes']
    pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=PupilId)
    if delete is True:
        for i in ealAreasOfConcernList:
            if i in request.POST:
                pupilExtentionRecord.WriteExtention('PupilEAL',
                                                    {i: ''})
    else:
        for i in ealAreasOfConcernList:
            if i in request.POST:
                pupilExtentionRecord.WriteExtention('PupilEAL',
                                                    {i: request.POST[i]})
    EALFunctions.updateEalCache(PupilId, AcYear)
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def pupilModifyEalLanguageData(request, school, AcYear, PupilId, delete=False):
    ''' modifies a pupils EAL language data '''
    ealLanguageList = ['ealLanguage1',
                       'ealLanguage2',
                       'ealLanguage3',
                       'ealLanguage4',
                       'ealLanguage5',
                       'ealLanguage6',
                       'ealLanguage1SpeakingLevel',
                       'ealLanguage2SpeakingLevel',
                       'ealLanguage3SpeakingLevel',
                       'ealLanguage4SpeakingLevel',
                       'ealLanguage5SpeakingLevel',
                       'ealLanguage6SpeakingLevel',
                       'ealLanguage1ReadingLevel',
                       'ealLanguage2ReadingLevel',
                       'ealLanguage3ReadingLevel',
                       'ealLanguage4ReadingLevel',
                       'ealLanguage5ReadingLevel',
                       'ealLanguage6ReadingLevel',
                       'ealLanguage1WritingLevel',
                       'ealLanguage2WritingLevel',
                       'ealLanguage3WritingLevel',
                       'ealLanguage4WritingLevel',
                       'ealLanguage5WritingLevel',
                       'ealLanguage6WritingLevel']
    pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=PupilId)
    if delete is True:
        for i in ealLanguageList:
            if i in request.POST:
                pupilExtentionRecord.WriteExtention('PupilEAL',
                                                    {i: ''})
    else:
        for i in ealLanguageList:
            if i in request.POST:
                pupilExtentionRecord.WriteExtention('PupilEAL',
                                                    {i: request.POST[i]})
    EALFunctions.updateEalCache(PupilId, AcYear)
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def pupilModifyEalFurtherLanguageDetails(request, school, AcYear, PupilId):
    ''' modifies a pupils EAL further language data '''
    deletePupilEalCache = False
    # update pupil Ethnicity
    if 'ealPupilEthnicity' in request.POST:
        pupilExObj = ExtendedRecord(BaseType='Pupil', BaseId=PupilId)
        pupilExObj.WriteExtention('PupilExtra',
                                  {'Ethnicity': request.POST['ealPupilEthnicity']})
        cache.delete('Pupil_%s' % PupilId)
        deletePupilEalCache = True
    # update parent/contact 1 data
    if 'ealPupilContactNationality1' in request.POST or 'ealPupilContactLanguage1' in request.POST:
        parent1ExObj = ExtendedRecord(BaseType='Contact',
                                      BaseId=request.POST['ealPupilContactId1'])
        if 'ealPupilContactNationality1' in request.POST:
            parent1ExObj.WriteExtention('ContactExtra',
                                        {'Nationality': request.POST['ealPupilContactNationality1']})
            deletePupilEalCache = True
        if 'ealPupilContactLanguage1' in request.POST:
            parent1ExObj.WriteExtention('ContactExtra',
                                        {'PrimaryLanguage': request.POST['ealPupilContactLanguage1']})
            deletePupilEalCache = True
    # update parent/contact 2 data
    if 'ealPupilContactNationality2' in request.POST or 'ealPupilContactLanguage2' in request.POST:
        parent1ExObj = ExtendedRecord(BaseType='Contact',
                                      BaseId=request.POST['ealPupilContactId2'])
        if 'ealPupilContactNationality2' in request.POST:
            parent1ExObj.WriteExtention('ContactExtra',
                                        {'Nationality': request.POST['ealPupilContactNationality2']})
            deletePupilEalCache = True
        if 'ealPupilContactLanguage2' in request.POST:
            parent1ExObj.WriteExtention('ContactExtra',
                                        {'PrimaryLanguage': request.POST['ealPupilContactLanguage2']})
            deletePupilEalCache = True
    if deletePupilEalCache:
        EALFunctions.updateEalCache(PupilId, AcYear)
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def ViewEalProvisionMap(request, school, AcYear, PupilId, mode):
    ''' View a pupils SEN provision mapping report '''
    if mode == 'singlePupil':
        pupils = [PupilId]
    if mode == 'multiplePupil':
        pupils = request.POST.getlist('PupilSelect')
    returnList = []
    for i in pupils:
        temp = []
        temp.append(EALFunctions.PupilRecordWithEal(int(i), AcYear))
        temp.append(NoteFunctions.GetPupilNotes(request,
                                             Pupil_Id=i,
                                             allEalNotes=True))
        returnList.append(temp)
    c = {'pupilList': returnList}
    conInst = RequestContext(request, processors=[SetMenu])
    return render_to_response('pupilEalMap.html', c,
                              context_instance=conInst)


def AddEalProvision(request, school, AcYear, PupilId):
    ''' Adds a new eal provision to a pupil '''
    provisionObj = PupilProvision(Pupil=Pupil.objects.get(id=int(PupilId)),
                                  Type='EAL',
                                  SENType=request.POST['senType'],
                                  StartDate=request.POST['startDate'],
                                  StopDate=request.POST['stopDate'])
    if 'provisionText' in request.POST:
        provisionObj.Details = request.POST['provisionText']
    provisionObj.save()
    log.warn('Pupil %s has been given a the new EAL Provision id:%s by %s' % (PupilId,
                                                                              provisionObj.id,
                                                                              request.user.username))
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def ModifyEalProvision(request, school, AcYear, PupilId, ProvisionId):
    ''' Modifies a Pupils Eal Provision '''
    ealProvision = PupilProvision.objects.get(id=int(ProvisionId))
    ealProvision.SENType = request.POST['senType']
    ealProvision.StartDate = request.POST['startDate']
    ealProvision.StopDate = request.POST['stopDate']
    if 'provisionText' in request.POST:
        ealProvision.Details = request.POST['provisionText']
    ealProvision.save()
    log.warn('Pupil %s\'s EAL Provision id:%s modified by %s' % (PupilId,
                                                                 ealProvision.id,
                                                                 request.user.username))
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def DeleteEalProvision(request, school, AcYear, PupilId, ProvisionId):
    ''' Deletes a Pupils Eal Provision '''
    ealProvision = PupilProvision.objects.get(id=int(ProvisionId))
    ealProvision.Active = False
    ealProvision.save()
    log.warn('Pupil %s\'s EAL Provision id:%s deleted by %s' % (PupilId,
                                                                ealProvision.id,
                                                                request.user.username))
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def AddEalTarget(request, school, AcYear, PupilId):
    ''' Adds an Eal Target to a Pupil '''
    targetObj = PupilTarget(Pupil=Pupil.objects.get(id=int(PupilId)),
                            Type='EAL',
                            ReviewDate=request.POST['reviewDate'])
    if 'targetText' in request.POST:
        targetObj.Target = request.POST['targetText']
    if 'reviewText' in request.POST:
        targetObj.Review = request.POST['reviewText']
    targetObj.save()
    log.warn('Pupil %s has been given a the new EAL Target id:%s by %s' % (PupilId,
                                                                           targetObj.id,
                                                                           request.user.username))
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def ModifyEalTarget(request, school, AcYear, PupilId, ProvisionId):
    ''' Modifies an Eal Target to a Pupil '''
    targetObj = PupilTarget.objects.get(id=int(ProvisionId))
    targetObj.ReviewDate = request.POST['reviewDate']
    if 'targetText' in request.POST:
        targetObj.Target = request.POST['targetText']
    if 'reviewText' in request.POST:
        targetObj.Review = request.POST['reviewText']
    targetObj.save()
    log.warn('Pupil %s\'s EAL Target id:%s modified by %s' % (PupilId,
                                                              targetObj.id,
                                                              request.user.username))
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)


def DeleteEalTarget(request, school, AcYear, PupilId, ProvisionId):
    ''' Deletes an Eal Target to a Pupil '''
    targetObj = PupilTarget.objects.get(id=int(ProvisionId))
    targetObj.Active = False
    targetObj.save()
    log.warn('Pupil %s\'s EAL Target id:%s deleted by %s' % (PupilId,
                                                             targetObj.id,
                                                             request.user.username))
    httpAddress = '/WillowTree/%s/%s/pupilEALRecord/%d/'
    tupleData = (school, AcYear, int(PupilId))
    return HttpResponseRedirect(httpAddress % tupleData)
