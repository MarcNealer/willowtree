# django imports
from django.core.cache import cache

# willow tree imports
from MIS.modules.PupilRecs import PupilObject
from MIS.modules.ExtendedRecords import *
from MIS.models import ExtentionPickList
from MIS.models import PupilProvision


def PupilRecordWithEal(PupilId, AcYear):
    Pupilrec = cache.get('PupilEAL_%s' % PupilId)
    if not Pupilrec:
        PupilOb = PupilObjectWithEal(PupilId, AcYear)
        cache.set('PupilEAL_%s' % PupilId, PupilOb, 900)
        return PupilOb
    else:
        return Pupilrec


def updateEalCache(PupilId, AcYear):
    ''' Quick method to update the cached PupilRecordWithEal obj '''
    cache.delete('PupilEAL_%s' % PupilId)
    PupilRecordWithEal(PupilId, AcYear)
    return


class PupilObjectWithEal(PupilObject):
    ''' A child of the pupil object with extended EAL methods '''
    def __CacheSelf__(self):
        cache.set('PupilEAL_%s' % self.PupilId, self, 900)
        return

    def ReturnPastEALs(self):
        ''' Returns a list of past EAL alerts for a Pupil '''
        pastEalList = []
        PupilAlerts = self.AllPupilAlerts()
        for i in PupilAlerts:
            for alert in i:
                if alert.Active == False and alert.AlertType.AlertGroup.Name == 'EAL':
                    pastEalList.append(alert)
        return pastEalList

    def ExtendedEal(self):
        ''' Returns a dictionary of EAL extended records '''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = ExtendedRecord(BaseType='Pupil',
                                                    BaseId=self.Pupil.id).ReadExtention(ExtentionName='PupilEAL')
            self.__CacheSelf__()
        return self.ExtendedEalRecord

    def returnEalExtraTimeSpecialPermissions(self):
        ''' returns the Extra Time eal special permission for a pupil'''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        return exRecord['ealExtraTime'][0]

    def returnEalExtraTimeSpecialPermissionsPickList(self):
        ''' returns the Extra Time eal special permission pick list for a
        pupil'''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        return exRecord['ealExtraTime'][2]

    def returnEalLaptopSpecialPermissions(self):
        ''' returns the Laptop eal special permission for a pupil'''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        return exRecord['ealLaptop'][0]

    def returnEalLaptopSpecialPermissionsPickList(self):
        ''' returns the Laptop eal special permission pick list for a pupil'''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        return exRecord['ealLaptop'][2]

    def returnEalOtherSpecialPermissions(self):
        ''' returns theOther eal special permission for a pupil '''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        return exRecord['ealOther'][0]

    def returnEalGeneralNotes(self):
        ''' returns theOther eal general notes for a pupil '''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        return exRecord['ealGeneralNotes'][0]

    def returnEalAssessmentHistory(self):
        ''' returns theOther eal assessment history for a pupil '''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        return exRecord['ealAssessmentHistory'][0]

    def returnAreasOfConcernList(self):
        ''' returns a list of areas of concern with extention data'''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        return [['Reading',
                 'Reading',
                 exRecord['ealAreasOfConcernReadingLevel'],
                 exRecord['ealAreasOfConcernReadingNotes'],
                 exRecord['ealAreasOfConcernReadingTeacherNotes']],
                ['Writing',
                 'Writing',
                 exRecord['ealAreasOfConcernWritingLevel'],
                 exRecord['ealAreasOfConcernWritingNotes'],
                 exRecord['ealAreasOfConcernWritingTeacherNotes']],
                ['Maths',
                 'Maths',
                 exRecord['ealAreasOfConcernMathsLevel'],
                 exRecord['ealAreasOfConcernMathsNotes'],
                 exRecord['ealAreasOfConcernMathsTeacherNotes']],
                ['Listening & Response',
                 'ListeningAndResponding',
                 exRecord['ealAreasOfConcernListeningAndRespondingLevel'],
                 exRecord['ealAreasOfConcernListeningAndRespondingNotes'],
                 exRecord['ealAreasOfConcernListeningAndRespondingTeacherNotes']],
                ['Social',
                 'Social',
                  exRecord['ealAreasOfConcernSocialLevel'],
                  exRecord['ealAreasOfConcernSocialNotes'],
                  exRecord['ealAreasOfConcernSocialTeacherNotes']],
                ['General',
                 'General',
                  exRecord['ealAreasOfConcernGeneralLevel'],
                  exRecord['ealAreasOfConcernGeneralNotes'],
                  exRecord['ealAreasOfConcernGeneralTeacherNotes']]]

    def returnAreasOfConcernListPickList(self):
        ''' returns the EAL Areas Concern picklist '''
        data = eval(ExtentionPickList.objects.get(Name='ealAreasOfConcern').Data)
        return data

    def returnAreasOfConcern(self):
        ''' returns a dictionary of EAL Areas of Concerns - this will return
        false if no data is available '''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        dataBool = False
        returnDict = {}
        for i in self.returnAreasOfConcernList():
            if exRecord['ealAreasOfConcern%sLevel' % i[1]][0]:
                dataBool = True
            if exRecord['ealAreasOfConcern%sLevel' % i[1]][1]:
                dataBool = True
            if exRecord['ealAreasOfConcern%sNotes' % i[1]][0]:
                dataBool = True
            if exRecord['ealAreasOfConcern%sNotes' % i[1]][1]:
                dataBool = True
            if exRecord['ealAreasOfConcern%sTeacherNotes' % i[1]][0]:
                dataBool = True
            if exRecord['ealAreasOfConcern%sTeacherNotes' % i[1]][1]:
                dataBool = True
            tempList = [exRecord['ealAreasOfConcern%sLevel' % i[1]],
                        exRecord['ealAreasOfConcern%sNotes' % i[1]],
                        exRecord['ealAreasOfConcern%sTeacherNotes' % i[1]]]
            returnDict.update({'%s' % i[1]: tempList})
        if dataBool is False:
            return False
        else:
            return returnDict

    def returnEthnicity(self):
        if not self.ExtendedRecord:
            self.ExtendedRecord = self.Extended()
        exRecord = self.ExtendedRecord
        return exRecord['Ethnicity'][1]

    def returnEalLanguageData(self):
        ''' returns a dictionary of EAL Language Data - this will return
        false if no data is available '''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        dataBool = False
        returnDict = {}
        for i in self.returnEalLanguageList():
            if exRecord['ealLanguage%s' % i[0]][0]:
                dataBool = True
            if exRecord['ealLanguage%s' % i[0]][1]:
                dataBool = True
            if exRecord['ealLanguage%sSpeakingLevel' % i[0]][0]:
                dataBool = True
            if exRecord['ealLanguage%sSpeakingLevel' % i[0]][1]:
                dataBool = True
            if exRecord['ealLanguage%sReadingLevel' % i[0]][0]:
                dataBool = True
            if exRecord['ealLanguage%sReadingLevel' % i[0]][1]:
                dataBool = True
            if exRecord['ealLanguage%sWritingLevel' % i[0]][0]:
                dataBool = True
            if exRecord['ealLanguage%sWritingLevel' % i[0]][1]:
                dataBool = True
            tempList = [exRecord['ealLanguage%s' % i[0]],
                        exRecord['ealLanguage%sSpeakingLevel' % i[0]],
                        exRecord['ealLanguage%sReadingLevel' % i[0]],
                        exRecord['ealLanguage%sWritingLevel' % i[0]]]
            returnDict.update({'%s' % i[1]: tempList})
        if dataBool is False:
            return False
        else:
            return returnDict

    def returnEalLanguageList(self):
        ''' returns a list of areas of concern with extention data'''
        if not self.ExtendedEalRecord:
            self.ExtendedEalRecord = self.ExtendedEal()
        exRecord = self.ExtendedEalRecord
        return [['1',
                 '1st',
                 exRecord['ealLanguage1'],
                 exRecord['ealLanguage1SpeakingLevel'],
                 exRecord['ealLanguage1ReadingLevel'],
                 exRecord['ealLanguage1WritingLevel']],
                ['2',
                 '2nd',
                 exRecord['ealLanguage2'],
                 exRecord['ealLanguage2SpeakingLevel'],
                 exRecord['ealLanguage2ReadingLevel'],
                 exRecord['ealLanguage2WritingLevel']],
                ['3',
                 '3rd',
                 exRecord['ealLanguage3'],
                 exRecord['ealLanguage3SpeakingLevel'],
                 exRecord['ealLanguage3ReadingLevel'],
                 exRecord['ealLanguage3WritingLevel']],
                ['4',
                 '4th',
                 exRecord['ealLanguage4'],
                 exRecord['ealLanguage4SpeakingLevel'],
                 exRecord['ealLanguage4ReadingLevel'],
                 exRecord['ealLanguage4WritingLevel']],
                ['5',
                 '5th',
                 exRecord['ealLanguage5'],
                 exRecord['ealLanguage5SpeakingLevel'],
                 exRecord['ealLanguage5ReadingLevel'],
                 exRecord['ealLanguage5WritingLevel']],
                ['6',
                 '6th',
                 exRecord['ealLanguage6'],
                 exRecord['ealLanguage6SpeakingLevel'],
                 exRecord['ealLanguage6ReadingLevel'],
                 exRecord['ealLanguage6WritingLevel']]]

    def returnLanguageLevelPickList(self):
        ''' returns the language level picklist '''
        data = eval(ExtentionPickList.objects.get(Name='ealLanguageLevel').Data)
        return data

    def returnLanguagePickList(self):
        ''' returns the language picklist '''
        data = eval(ExtentionPickList.objects.get(Name='PrimaryLanguage').Data)
        return data

    def returnProvisions(self):
        ''' Returns a dictionary holding active and inactive eal provisions for
        a child '''
        if PupilProvision.objects.filter(Pupil__id=self.Pupil.id,
                                         Type='EAL',
                                         Active=True).exists():
            activeP = PupilProvision.objects.filter(Pupil__id=self.Pupil.id,
                                                    Type='EAL',
                                                    Active=True)
        else:
            activeP = False
        if PupilProvision.objects.filter(Pupil__id=self.Pupil.id,
                                         Type='EAL',
                                         Active=False).exists():
            inActiveP = PupilProvision.objects.filter(Pupil__id=self.Pupil.id,
                                                      Type='EAL',
                                                      Active=False)
        else:
            inActiveP = False
        return {'activeProvisions': activeP, 'inActiveProvisions': inActiveP}

    def returnTargets(self):
        ''' Returns a dictionary holding active and inactive eal targets for a
        child '''
        if PupilTarget.objects.filter(Pupil__id=self.Pupil.id,
                                      Type='EAL',
                                      Active=True).exists():
            activeT = PupilTarget.objects.filter(Pupil__id=self.Pupil.id,
                                                 Type='EAL',
                                                 Active=True)
        else:
            activeT = False
        if PupilTarget.objects.filter(Pupil__id=self.Pupil.id,
                                      Type='EAL',
                                      Active=False):
            inActiveT = PupilTarget.objects.filter(Pupil__id=self.Pupil.id,
                                                   Type='EAL',
                                                   Active=False)
        else:
            inActiveT = False
        return {'activeTargets': activeT, 'inActiveTargets': inActiveT}
