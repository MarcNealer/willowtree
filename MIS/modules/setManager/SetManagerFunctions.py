from MIS.models import *
from django import template
import datetime, copy,json
from MIS.modules import GeneralFunctions

class SetManagerObject():
    """Returns a group of pupils from a specified year group with their group
    sets they are in.
    
    Example usage:
    SetManagerObject(AcYear,GroupId,GroupSets)"""
    def __init__(self,AcYear,MgroupChild,Sgroups):
        self.MgroupChild=Group.objects.get(id=int(MgroupChild))
        self.Mgroup=Group.objects.get(Name='.'.join(self.MgroupChild.Name.split('.')[0:-1]))
        self.MgroupName=self.Mgroup.Name.split('.')[-1]
        self.SgroupString=Sgroups
    
        self.Sgroup=Group.objects.filter(id__in=[int(x) for x in self.SgroupString.split('-')])
        self.GroupCount={}
        for groups in self.Sgroup:
            self.GroupCount[groups.id]=0
            
        self.PupilList=[]        
        for Pupils in GeneralFunctions.Get_PupilList(AcYear,GroupName=self.Mgroup.Name):
            Record={'PupilDetails':Pupils,'Selected':False}
            SelectList=[]
            for Groups in self.Sgroup:
                if PupilGroup.objects.filter(Pupil=Pupils,Group=Groups,AcademicYear=AcYear,Active=True).exists():
                    SelectList.append([Groups,True])
                    self.GroupCount[Groups.id]+=1
                    Record['Selected']=True
                else:
                    SelectList.append([Groups,False])
            Record['GroupList']=copy.deepcopy(SelectList)
            self.PupilList.append(copy.deepcopy(Record))
        self.GroupCounter=[]
        for Groups in self.Sgroup:
            self.GroupCounter.append(self.GroupCount[Groups.id])

def ChangeGroups(request,AcYear,MgroupChild,GroupList):
    """Changes the set a pupil might be in.
    
    Example usage:
    SetManagerObject.ChangeGroups(request,AcYear,GroupId,GroupSets)"""
    return_messages=[]
    Mgroup=Group.objects.get(id=int(request.POST['ParentGroup']))
    for Pupil in GeneralFunctions.Get_PupilList(AcYear,GroupName=Mgroup.Name):
        if str(Pupil.id) in request.POST:
            if len(request.POST[str(Pupil.id)]) > 0:
                if not PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=Pupil,Group__id=int(request.POST[str(Pupil.id)]),Active=True).exists():
                    for SetGroups in GroupList.split('-'):
                        GeneralFunctions.RemoveFromSubGroups(int(SetGroups),Pupil.id,AcYear)
                    NewGroup=PupilGroup(AcademicYear=AcYear,Group=Group.objects.get(id=int(request.POST[str(Pupil.id)])), Pupil=Pupil)
                    NewGroup.save()
                    return_messages.append('%s has been moved in %s' % (Pupil.FullName(),Group.objects.get(id=int(request.POST[str(Pupil.id)])).MenuName))    
    return return_messages
    
                    

        
        