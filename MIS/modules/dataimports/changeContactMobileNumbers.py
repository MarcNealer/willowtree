"""
Created on Wed Oct  2 10:50:16 2013

@author: roy

replace +44 with 07 for all contacts in all schools script

"""

from MIS.modules.ExtendedRecords import ExtendedRecord
from MIS.models import Contact


def run():
    for i in Contact.objects.all():
        oldNumber = False
        temp = ExtendedRecord('Contact', i.id)
        if '+44' in temp.ReadExtention('ContactExtra')['mobileNumber'][0]:
            oldNumber = temp.ReadExtention('ContactExtra')['mobileNumber'][0]
            temp.WriteExtention('ContactExtra',
                                {'mobileNumber': '0%s' % oldNumber[3:]})
            print '%s %s %s changed %s to %s' % (i.id,
                                                 i.Forename,
                                                 i.Surname,
                                                 oldNumber,
                                                 '0%s' % oldNumber[3:])
    return