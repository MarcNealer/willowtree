# -*- coding: utf-8 -*-
"""
Created on Sun Jan 12 08:48:14 2014

@author: marc
"""
from MIS.modules import GeneralFunctions, ExtendedRecordsOld,ExtendedRecords

def EYFSTransfer(schoolname,AcYear,ClassName=None):
    pupillist=GeneralFunctions.Get_PupilList(AcYear,'Current.%s.LowerSchool.Reception' % (schoolname))
    # get a list of reception children

    # extract Their EYFS Record
    for pupil in pupillist:
        oldext=ExtendedRecordsOld.ExtendedRecord('Pupil',pupil.id)
        olddata=oldext.ReadExtention('EYFS_Mich','Form_Comment')
        #Remove the Picklist and Alt Data ( List items 1 and 2)
        newdata={}
        for OldKey,OldData in olddata.items():
            newdata[OldKey]=OldData[0]
        NewExt=ExtendedRecords.ExtendedRecord('Pupil',pupil.id)
        NewExt.WriteExtention('EYFS_Mich',newdata,'Form_Comment')
        print newdata
        print pupil.FullName()


            # Save to the new Extended Record System