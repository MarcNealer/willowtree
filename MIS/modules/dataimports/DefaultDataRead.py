#-------------------------------------------------------------------------------
# Name:        DefaultData
# Purpose:      Pushes default records into the WillowTree MIS
#
# Author:      DBMgr
#
# Created:     12/02/2012
# Copyright:   (c) DBMgr 2012
# Licence:
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from WillowTree.MIS import models
from MIS.models import *
import datetime, copy, csv
from MIS.modules.GeneralFunctions import *
from MIS.modules.ExtendedRecords import *
from FamilyImport import importPupils,importContacts

def SetUpScript():
    InputData('SetUp.ini')
    BatterseaMenu('Admin')
    BatterseaMenu('Teacher')
    #ClaphamMenu('Admin')
    #ClaphamMenu('Teacher')
    #FulhamMenu('Admin')
    #FulhamMenu('Teacher')
    #KensingtonMenu('Admin')
    #KensingtonMenu('Teacher')
    importPupils('ImportData/Bat_2013.csv','Battersea','2012-2013')
    importContacts('ImportData/BatterseaContacts.csv','Battersea','2012-2013')
    #importPupils('ImportData/Bat_2012.csv','Battersea','2011-2012')    
    #importPupils('ImportData/Bat_2011.csv','Battersea','2010-2011')
    #importPupils('ImportData/Bat_2010.csv','Battersea','2009-2010')  
    #importPupils('ImportData/Bat_2009.csv','Battersea','2008-2009')  
    #importPupils('ImportData/Bat_2008.csv','Battersea','2007-2008')    
    #importPupils('ImportData/Cla-2013.csv','Clapham','2012-2013')
    #importContacts('ImportData/ClaphamContacts.csv','Clapham','2012-2013')    
    #importPupils('ImportData/Cla-2012.csv','Clapham','2011-2012')    
    #importPupils('ImportData/Cla-2011.csv','Clapham','2010-2011')
    #importPupils('ImportData/Cla-2010.csv','Clapham','2009-2010')  
    #importPupils('ImportData/Cla-2009.csv','Clapham','2008-2009')  
    #importPupils('ImportData/Cla-2008.csv','Clapham','2007-2008') 
    #importPupils('ImportData/Ken-2013.csv','Kensington','2012-2013')
    #importContacts('ImportData/KensingtonContacts.csv','Kensington','2012-2013')    
    #importPupils('ImportData/Ken-2012.csv','Kensington','2011-2012')    
    #importPupils('ImportData/Ken-2011.csv','Kensington','2010-2011')
    #importPupils('ImportData/Ken-2010.csv','Kensington','2009-2010')  
    #importPupils('ImportData/Ken-2009.csv','Kensington','2008-2009')  
    #importPupils('ImportData/Ken-2008.csv','Kensington','2007-2008') 
    #importPupils('ImportData/Ful-2013.csv','Fulham','2012-2013')
    #importContacts('ImportData/FulhamContacts.csv','Fulham','2012-2013')    
    #importPupils('ImportData/Ful-2012.csv','Fulham','2011-2012')    
    #importPupils('ImportData/Ful-2011.csv','Fulham','2010-2011')
    #importPupils('ImportData/Ful-2010.csv','Fulham','2009-2010')  
    #importPupils('ImportData/Ful-2009.csv','Fulham','2008-2009')  
    #importPupils('ImportData/Ful-2008.csv','Fulham','2007-2008')
    
    importStaff('ImportData/Battersea_Staff.csv','Battersea','2012-2013')
    #importStaff('ImportData/Clapham_Staff.csv','Clapham','2012-2013')    
    #importStaff('ImportData/Fulham_Staff.csv','Fulham','2012-2013')    
    #importStaff('ImportData/Kensington_Staff.csv','Kensington','2012-2013')
    #importDocuments('ImportData/Bat-Documents.csv','Battersea')
    #importDocuments('ImportData/Cla-Documents.csv','Clapham')
    #importDocuments('ImportData/Ful-Documents.csv','Fulham')
    #importDocuments('ImportData/Ken-Documents.csv','Kensington')


    
def InputData(FileName,TestPupils=False):
    Parms = ConfigParser(FileName)
    
    # School Namess
    print 'School Records'
    if type(Parms.items('School')).__name__ =='dict':
        for SName,SData in Parms.items('School').items():
            SchoolData=eval(SData)
            if School.objects.filter(Name=SchoolData['SchoolName']).exists()==False:
                NewAddress=Address(HomeSalutation=SchoolData['SchoolName'],PostalTitle=SchoolData['SchoolName'],AddressLine1=SchoolData['AddressLine1'],AddressLine2=SchoolData['AddressLine2'],PostCode=SchoolData['PostCode'],Phone1=SchoolData['Phone1'],Phone2=SchoolData['Phone2'],EmailAddress="%s@Thomas's.co.uk" % SchoolData['SchoolName'])
                NewAddress.save()
                NewSchool = School(Name=SchoolData['SchoolName'],HeadTeacher=SchoolData['HeadTeacher'],TemplateCSS="/LetterCSS/%s.css" % SchoolData['SchoolName'],TemplateBanner="/LetterCSS/%s.jpg" % SchoolData['SchoolName'],Address=NewAddress)
                NewSchool.save()    
    
    # Reading Family Relatipships
    print 'Family Relationships'
    if type(Parms.items('FamilyRelationships')).__name__ =='dict':
        for RelName,RelDetails in Parms.items('FamilyRelationships').items():
            print RelName
            if FamilyRelationship.objects.filter(Type=RelName).exists()==False:
                NewSubject = FamilyRelationship(Type=RelName,RelType=RelDetails)
                NewSubject.save()

    # Reading Subjects
    print 'Subjects'
    if type(Parms.items('Subjects')).__name__ =='dict':
        for SubjectName,SubjectDetails in Parms.items('Subjects').items():
            print SubjectName
            if Subject.objects.filter(Name=SubjectName).exists()==False:
                NewSubject = Subject(Name=SubjectName,Desc=SubjectDetails)
                NewSubject.save()
    # Reading Menu Actions
    print 'Menu Actions'
    if type(Parms.items('MenuActions')).__name__ =='dict':
        for ActionName,ActionDetails in Parms.items('MenuActions').items():
            print ActionName
            ActionItems=ActionDetails.split(':')
            if MenuActions.objects.filter(Name=ActionName).exists()==False:
                NewSubject = MenuActions(Name=ActionName,MenuName=ActionItems[0],MenuOrder=ActionItems[1])
                NewSubject.save()
    # Reading Sub Menus
    print 'Sub Menus'
    if type(Parms.items('SubMenus')).__name__ =='dict':
        for ActionName,ActionDetails in Parms.items('SubMenus').items():
            print ActionName
            ActionItems=ActionDetails.split(':')
            if MenuSubMenu.objects.filter(Name=ActionName).exists()==False:
                NewSubject = MenuSubMenu(Name=ActionName,MenuName=ActionItems[0],MenuOrder=ActionItems[1])
                NewSubject.save()

    # Reading Attendance codes
    print 'Attendance codes'
    if type(Parms.items('Subjects')).__name__ =='dict':
        for AttenCode,AttenDetails in Parms.items('AttendanceCodes').items():
            print AttenCode
            if AttendanceCode.objects.filter(AttendanceCode=AttenCode).exists()==False:
                AttenDesc,AttenLevel,AttenType=AttenDetails.split(':')
                NewCode = AttendanceCode(AttendanceCode=AttenCode,AttendanceDesc=AttenDesc,AttendanceAccess=AttenLevel,AttendanceCodeType=AttenType)
                NewCode.save()

    print 'PickLists'
    if type(Parms.items('PickLists')).__name__ =='dict':
        print Parms.items('PickLists')
        for PickListName,PickListData in Parms.items('PickLists').items():
            print PickListName
            if ExtentionPickList.objects.filter(Name=PickListName).exists()==False:
                NewPickList=ExtentionPickList(Name=PickListName,Data=PickListData)
                NewPickList.save()

    print 'fields'
    if type(Parms.items('Fields')).__name__ =='dict':
        for FieldName,FieldDetails in Parms.items('Fields').items():
            print FieldName
            if ExtentionFields.objects.filter(Name=FieldName).exists()==False:
                Details=FieldDetails.split(':')
                ERF=ExtentionFields(Name=FieldName,Type=Details[0],PickList=ExtentionPickList.objects.get(Name=Details[1]))
                ERF.save()
                
    print 'Extention Records'
    if type(Parms.items('ExtentionRecords')).__name__ =='dict':
        for ERName,ERDetails in Parms.items('ExtentionRecords').items():
            ExtentionDetails=ERDetails.split(':')
            if ExtentionRecords.objects.filter(Name=ERName).exists() == False:
                NewER=ExtentionRecords(Name=ERName,FullName=ExtentionDetails[0])
                NewER.save()
                if len(ExtentionDetails) > 1:
                    for ERField in ExtentionDetails[1:]:
                        if ExtentionFields.objects.filter(Name=ERField).exists():
                            NewER.Fields.add(ExtentionFields.objects.get(Name=ERField))
                    NewER.save()

    print 'MenuTypes'
    if type(Parms.items('MenuTypes')).__name__ =='dict':
        for MenuTypeName,MenuTypeDetails in Parms.items('MenuTypes').items():
            print MenuTypeName
            if MenuTypes.objects.filter(Name=MenuTypeName).exists()==False:
                MenuTypeDesc=MenuTypeDetails.split('\r')[0]
                Description,Image,Level,SchoolName=MenuTypeDesc.split(':')
                MT=MenuTypes(Name=MenuTypeName,Desc=Description,SchoolId=School.objects.get(Name=SchoolName),Logo=Image,MenuLevel=Level)
                MT.save()

    print 'Adding System Variables'
    if type(Parms.items('SystemVariables')).__name__ =='dict':
        for VarName,VarData in Parms.items('SystemVariables').items():
            WillowFunc.Write_SystemVar({VarName:VarData})
    print 'Adding Menu Items'
    if type(Parms.items('MenuItems')).__name__=='dict':
        for KWName, KWType in Parms.items('MenuItems').items():
            NewKeyword=MenuItems(Name=KWName,ItemURL=KWType)
            NewKeyword.save()
         

def importDocuments(FileName,School):
    staff=Staff.objects.get(id=2358)
    School=School[:3]
    ImportItems=csv.DictReader(open(FileName,'rb'))
    for Items in ImportItems:
        if Items['File ref'].find('facilitycmis') > -1:
            DocFile='DocumentStorage/%s' % Items['File ref'].split('\\')[-1]
            OldPupilId='%s-%s' % (School,Items['Creator'])
            DocName=Items['Name']
            print DocName
            print OldPupilId
            
            NewDoc=Document(DocumentKeywords=DocName,DocumentName=DocName,Filename=DocFile,UploadStaff=staff)
            NewDoc.save()
            if Pupil.objects.filter(Old_Id=OldPupilId).exists():
                PupilDoc=PupilDocument(PupilId=Pupil.objects.get(Old_Id=OldPupilId),DocumentId=NewDoc)
                PupilDoc.save()
        
def importStaff(FileName,School,AcYear):
    ImportItems=csv.DictReader(open(FileName,'rb'))
    for Items in ImportItems:
        print Items
        if Items['Gender']=='NotSpecified':
            if Items['Titile']=='Mr':
                Items['Gender']='Male'
            else:
                Items['Gender']='Female'
        if len(Items['Address2']) < 3:
            Items['Address2']='NotSpecified'
        if len(Items['Address3']) < 3:
            Items['Address3']='NotSpecified'            
        if len(Items['Home post code']) < 3:
            Items['Home Post Code']='NotSpecified'    
        if len(Items['Phone1']) < 3:
            Items['Phone1']='NotSpecified'            
        if len(Items['Phone2']) < 3:
            Items['Phone2']='NotSpecified' 
        if len(Items['Date of Birth']) < 3:
            Items['Date of Birth']='01-01-1971'
        if len(Items['Forename']) < 3:
            Items['Forename']='NotSpecified'
        if len(Items['Surname']) < 3:
            Items['Surname']='NotSpecified'            
            
        # save address        
        NewAddress=Address(HomeSalutation="%s %s. %s" % (Items['Title'],Items['Forename'][0],Items['Surname']), 
                           PostalTitle="%s %s. %s" % (Items['Title'],Items['Forename'][0],Items['Surname']),AddressLine1=Items['Address1'],
                           AddressLine2=Items['Address2'],AddressLine3=Items['Address3'],AddressLine4=Items['Address4'],
                           PostCode=Items['Home post code'],Phone1=Items['Phone1'],Phone2=Items['Phone2'],EmailAddress="%s%s@Thomas-s.co.uk" % (Items['Forename'][0],Items['Surname']))
        NewAddress.save()
        
        #Import staff and attach to address
        day,month,year=Items['Date of Birth'].split('-')
        NewStaff=Staff(Forename=Items['Forename'],Surname=Items['Surname'],Title=Items['Title'],
                       EmailAddress="%s%s@Thomas-s.co.uk" % (Items['Forename'][0],Items['Surname']),
DateOfBirth=datetime.datetime(int(year),int(month),int(day)),Gender=Items['Gender'][0],
DefaultMenu=MenuTypes.objects.get(Name="%sAdmin" % School),Old_Id="%s-%s" % (School[:3],Items['Id']),Address=NewAddress)
        NewStaff.save()
        
        if 'Group Catering' in Items:
            if Items['Group Catering']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Catering')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                  
        if 'Group Admin' in Items:
            if Items['Group Admin'] =='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='OfficeAdmin')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()  
        if 'Group Co Curr HoDs' in Items:
            if Items['Group Co Curr HoDs']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='CoHODS')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                  
        if 'Group CurrHoDs' in Items:
            if Items['Group CurrHoDs']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='CurrHODS')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                  
        if 'Group IT Support' in Items:
            if Items['Group IT Support']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='SysAdmin')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                  
        if 'Group Maintinance' in Items:
            if Items['Group Maintinance']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Maintinance')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                  
        if 'Group SLT' in Items:
            if Items['Group SLT']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='SLT')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save() 
        if 'Group Transport' in Items:
            if Items['Group Transport']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Transport')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                   
                

        if 'Group LS Form Teachers' in Items:
            if Items['Group LS Form Teachers']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='LowerSchool.Form')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                  

        if 'Group MS Form Teachers' in Items:
            if Items['Group MS Form Teachers']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='MiddleSchool.Form')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                 
        if 'Group PS Form Teachers' in Items:
            if Items['Group PS Form Teachers']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='PrepSchool.Form')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                  
        if 'Group US Form Teachers' in Items:
            if Items['Group US Form Teachers']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='UpperSchool.Form')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                 
        if 'Group Year Reception' in Items:
            if Items['Group Year Reception']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Reception')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                  
        if 'Group Year 1' in Items:
            if Items['Group Year 1']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Year1')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                             
        if 'Group Year 2' in Items:
            if Items['Group Year 2']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Year2')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                             
        if 'Group Year 3' in Items:
            if Items['Group Year 3']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Year3')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                             
        if 'Group Year 4' in Items:
            if Items['Group Year 4']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Year4')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                             
        if 'Group Year 5' in Items:
            if Items['Group Year 5']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Year5')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                             
        if 'Group Year 6' in Items:
            if Items['Group Year 6']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Year6')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                             
        if 'Group Year 7' in Items:
            if Items['Group Year 7']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Year7')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()                             
        if 'Group Year 8' in Items:
            if Items['Group Year 8']=='1':
                AssginedGroup=Group.objects.get(Name__istartswith='Staff.%s' % School,Name__iendswith='Year8')
                NewStaffGroup=StaffGroup(AcademicYear=AcYear,Staff=NewStaff,Group=AssginedGroup)
                NewStaffGroup.save()

def FindDuplicateParents():
    contactlist=Contact.objects.all()
    for contacts in contactlist:
        if not FamilyContact.objects.filter(Contact=contacts).exists():
            if len(Contact.objects.filter(Surname=contacts.Surname,Forename=contacts.Forename)) > 1:
                conrec=contacts
                print conrec
                conrec.delete()

def DeleteAllTermGrades(Group,AcYear):
    term=['M','L','S']
    year=['R','1','2','3','4','5','6','7','8']
    types=['Hf','Eot']
    examlist=[]
    for Terms in term:
        for Years in year:
            for Types in types:
                examlist.append('Yr%s_%s_%s' % (Years,Types,Terms))
    pupillist=PupilGroup.objects.filter(Group__Name__istartswith=Group,AcademicYear=AcYear)
    for pupils in pupillist:
        print pupils.Pupil.FullName()
        ExtentionForPupil.objects.filter(PupilId=pupils.Pupil,ExtentionRecordId__Name__in=examlist).delete()

                   