# Python imports
import csv

# WillowTree imports
from MIS.models import *
from MIS.modules import ExtendedRecords


def importContacts(csvFile="/home/login/Dropbox/livefiles/Roy/ken_contact_extract.csv"):
    ''' Import contacts inj WillowTree '''
    with open(csvFile, 'rb') as csvfile:
        spamreader = csv.reader(csvfile)
        for i in spamreader:
            if not FamilyChildren.objects.filter(Pupil__Old_Id=i[0], FamilyType=1).exists():
                print '<----- %s not in Willpow Tree ----->' % i[0]
            else:
                if not FamilyContact.objects.filter(FamilyId=FamilyChildren.objects.get(Pupil__Old_Id=i[0], FamilyType=1).FamilyId.id).count() == 2:
                    if i[7] is 'Female':
                        gender = 'F'
                    else:
                        gender = 'M'
                    if 'PAM' in i[1]:
                        relationship = FamilyRelationship.objects.get(Type="Mother")
                    else:
                        relationship = FamilyRelationship.objects.get(Type="Father")
                    if FamilyContact.objects.filter(FamilyId=FamilyChildren.objects.get(Pupil__Old_Id=i[0], FamilyType=1).FamilyId.id, Priority=1).exists():
                        priority = 2
                    else:
                        priority = 1
                    # contact data...
                    contactObj = Contact(Forename=i[4],
                                         Surname=i[5],
                                         EmailAddress=i[17],
                                         Title=i[6],
                                         Gender=gender)
                    contactObj.save()
                    # family data...
                    familyObj = FamilyContact(FamilyId=FamilyChildren.objects.get(Pupil__Old_Id=i[0], FamilyType=1).FamilyId,
                                              Contact=contactObj,
                                              Relationship=relationship,
                                              Priority=priority)
                    familyObj.save()
                    # extended records data...
                    contactExObj = ExtendedRecords.ExtendedRecord('Contact', contactObj.id)
                    contactExObj.WriteExtention('ContactExtra', {'mobileNumber': i[15]})
                    print '%s - %s %s imported' % (i[0],
                                                   Pupil.objects.get(Old_Id=i[0]).Forename,
                                                   Pupil.objects.get(Old_Id=i[0]).Surname)

def testFamilies(csvFile="ken_contact_extract.csv"):
    ''' Import contacts inj WillowTree '''
    with open(csvFile, 'rb') as csvfile:
        spamreader = csv.reader(csvfile)
        for i in spamreader:
            x = FamilyContact.objects.filter(FamilyId=FamilyChildren.objects.get(Pupil__Old_Id=i[0]).FamilyId.id)
