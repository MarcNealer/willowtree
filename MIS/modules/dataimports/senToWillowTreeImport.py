'''
HOW TO USE -->

1) Firstly run the "SEN_import_to_WillowTree.py" program on all four Facility
   servers.

2) This will save .p files to the "/home/login/Dropbox/senToWillowTree" folder.
   now open a django's ipython shell on the server you wish to import to and
   run this program for all four .p files.

3) Remeber to include a school when running this program, Or the log files will
   overwrite eachother.

4) use the individualImport option, specifying a student's name in this format:
   'Roy_Hanley' - use x.keys() if needed.
'''
# python imports
import pickle
import datetime
import re

# django imports
from django.core.cache import cache

# WillowTree imports
from MIS.models import Pupil
from MIS.models import PupilAlert
from MIS.models import Staff
from MIS.models import AlertType
from MIS.models import PupilProvision
from MIS.models import NoteDetails
from MIS.models import PupilTarget
from MIS.models import PupilNotes
from MIS.modules.ExtendedRecords import ExtendedRecord

class run():
    ''' this code is to be used in conjuction with the 'SEN_to_WillowTree'
    scripts on the Facility servers. Does what it says on the tin - roy '''
    def __init__(self, importFile,
                 school=False,
                 individualImportPupilName=False,
                 individualImportPupilId=False,
                 clearOldSenAlerts = False):
        self.clearOldSenAlerts = False
        self.individualImportPupilName = False
        self.individualImportPupilId = False
        if individualImportPupilName:
            self.individualImportPupilName = individualImportPupilName
        if individualImportPupilId:
            self.individualImportPupilId = individualImportPupilId
        self.todaysDate = self.__todaysDate__()
        self.me = Staff.objects.get(EmailAddress="RHanley@Thomas-s.co.uk")
        if school:
            self.logFile = open('senToWillowTreeImport%s.log' % school, 'w')
        else:
            self.logFile = open('senToWillowTreeImport.log', 'w')
        self.senObj = pickle.load(open(importFile, 'rb'))
        if self.individualImportPupilName:
            v = self.senObj[self.individualImportPupilName]
            test = self.__importActiveAlert__(v)
            if test != "error":
                self.__importSpecialPermissions__(v)
                self.__importAreasOfConcern__(v)
                self.__importAreasOfConcern2__(v)
                self.__importAssessmentHistory__(v)
                try:
                    self.__importProvisions__(v)
                except:
                    pass
                try:
                    self.__importTargets__(v)
                except:
                    pass
                try:
                    self.__importNotes__(v)
                except:
                    pass
            else:
                pass
        else:
            counter = 0
            lengthOfObj = len(self.senObj)
            for k, v in self.senObj.iteritems():
                test = self.__importActiveAlert__(v)
                if test != "error":
                    pass
                    self.__importSpecialPermissions__(v)
                    self.__importAreasOfConcern__(v)
                    self.__importAreasOfConcern2__(v)
#                    self.__importAssessmentHistory__(v)
#                    try:
#                        self.__importProvisions__(v)
#                    except:
#                        pass
#                    try:
#                        self.__importTargets__(v)
#                    except:
#                        pass
#                    try:
#                        self.__importNotes__(v)
#                    except:
#                        pass
                else:
                    pass
                counter += 1
                print "%s of %s completed" % (str(counter), str(lengthOfObj))
        self.logFile.close()
###############################################################################
# helper methods
###############################################################################

    def __getPupilObjOrError__(self, facilityPupilObj):
        ''' checks if there is only one pupil with the same forename, surname
        and DOB as the pupil in the facilityPupilObj in WillowTree. If there
        is more than one a dupilcate error is returned '''
        forename = facilityPupilObj['changeList']['Default']['SENdata']['Forename']
        surname = facilityPupilObj['changeList']['Default']['SENdata']['Surname']
        dob = facilityPupilObj['changeList']['Default']['SENdata']['DOB']
        test = Pupil.objects.filter(Forename=forename,
                                    Surname=surname,
                                    DateOfBirth=dob).count()
        if test == 0:
            self.tup = (forename, surname, dob)
            self.logFile.write('error: no pupil of %s %s, dob:%s exists\n' % self.tup)
            return 'error'
        elif test > 1:
            self.tup = (forename, surname, dob)
            self.logFile.write('error: duplicates of pupil %s %s, dob:%s exist\n' % self.tup)
            return 'error'
        elif test == 1:
            self.tup = (forename, surname, dob)
            self.logFile.write('Importing sen records for --> %s %s, dob:%s\n' % self.tup)
            WillowPupilObj = Pupil.objects.get(Forename=forename,
                                               Surname=surname,
                                               DateOfBirth=dob)
            cache.delete('PupilSEN_%s' % WillowPupilObj.id)
            return WillowPupilObj

    def __todaysDate__(self):
        now = datetime.datetime.now()
        return now.strftime("%Y-%m-%d")

    def __helperForAreasOfConcern__(self, data, mode):
        if mode == 'notes':
            t = data[0]
        else:
            t = data[1]
        dataString = ''
        try:
            t = unicode(t)
            if t != '':
                dataString += "<p>%s</p>" % t
        except:
            dataString = '<p><<<--- unable to import --->>></p>'
        return dataString

    def __convertDate__(self, oldDate):
        regTest = False
        try:
            if "/" in oldDate:
                oldDate = oldDate.split('/')
            elif "-" in oldDate:
                oldDate = oldDate.split('-')
            if len(oldDate[2]) == 2:
                oldDate[2] = "20%s" % oldDate[2]
            newDate = "%s-%s-%s" % (oldDate[2], oldDate[1], oldDate[0])
            regTest = re.match(newDate, "^\d{4}-\d{2}-\d{2}$")
            if regTest:
                return newDate
            else:
                return self.todaysDate
        except:
            return self.todaysDate

    def __clearAreaOfConcerns__(self):
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senLiteracy': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senNumeracy': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senBehavior': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senCommunication': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senPhysical': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senGeneral': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senLiteracyTeacherNotes': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senNumeracyTeacherNotes': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senBehaviorTeacherNotes': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senCommunicationTeacherNotes': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senPhysicalTeacherNotes': ''})
            self.pupilExtentionRecord.WriteExtention('PupilSEN', {'senGeneralTeacherNotes': ''})
        # end
###############################################################################
# getting data from picked object created from Facility_DB.py
###############################################################################

    def __getPastAlerts__(self, facilityPupilObj):
        return facilityPupilObj['changeList']['Default']['SENdata']['Precis']

    def __getActiveAlert__(self, facilityPupilObj):
        activeAlertData = {}
        startDate = self.__convertDate__(facilityPupilObj['changeList']['Default']['SENdata']['StartDate'])
        stopDate = self.__convertDate__(facilityPupilObj['changeList']['Default']['SENdata']['StopDate'])
        activeAlertData['startDate'] = startDate
        activeAlertData['stopDate'] = stopDate
        activeAlertData['provision'] = facilityPupilObj['changeList']['Default']['SENdata']['Provision']
        activeAlertData['text'] = facilityPupilObj['changeList']['Default']['SENdata']['Precis']
        return activeAlertData

    def __getSpecialPermissions__(self, facilityPupilObj):
        ''' there is no other! '''
        specialPermissions = {}
        specialPermissions['laptop'] = facilityPupilObj['changeList']['Default']['SENdata']['Laptop']
        specialPermissions['extraTime'] = facilityPupilObj['changeList']['Default']['SENdata']['ExtraTime']
        return specialPermissions

    def __getAreasOfConcern__(self, facilityPupilObj):
        if self.clearOldSenAlerts:
            self.__clearAreaOfConcerns__()
        returnValue = True
        areasOfConcern = {}
        try:
            areasOfConcern['Literacy'] = facilityPupilObj['changeList']['Default']['Literacy']
        except:
            areasOfConcern['Literacy'] = ['','']
        try:
            areasOfConcern['Numeracy'] = facilityPupilObj['changeList']['Default']['Numeracy']
        except:
            areasOfConcern['Numeracy'] = ['','']
        try:
            areasOfConcern['Behaviour'] = facilityPupilObj['changeList']['Default']['Behaviour']
        except:
            areasOfConcern['Behaviour'] = ['','']
        try:
            areasOfConcern['Communication'] = facilityPupilObj['changeList']['Default']['Communication']
        except:
            areasOfConcern['Communication'] = ['','']
        try:
            areasOfConcern['Physical'] = facilityPupilObj['changeList']['Default']['Physical']
        except:
            areasOfConcern['Physical'] = ['','']
        try:
            areasOfConcern['General'] = facilityPupilObj['changeList']['Default']['General']
        except:
            areasOfConcern['General'] = ['','']
        if returnValue:
            return areasOfConcern
        else:
            returnValue

    def __getProvisions__(self, facilityPupilObj):
        ''' stopDate and SEN Type are both missing from here and may cause
        problems '''
        provisions = []
        provs = facilityPupilObj['reports']['Default']['Provisions']
        for i in provs:
            targetDate = i['DateTime'][:10]
            targetData = "<p>%s, %s</p><p>Freq:%s</p><p>%s</p><p>%s</p>" % (i['Term'],
                                                                            i['Year'],
                                                                            i['Freq'],
                                                                            i['SenProv'],
                                                                            i['Teacher'])
            temp = {'targetData': targetData, 'targetDate': targetDate}
            provisions.append(temp)
        return provisions

    def __getTargets__(self, facilityPupilObj):
        targets = []
        for i in facilityPupilObj['reports']['Default']['Targets']:
            temp = "<p>%s, %s</p><p>%s</p><p>%s</p>" % (i['Term'],
                                                        i['TargYear'],
                                                        i['Target'],
                                                        i['Teacher'])
            targets.append(temp)
        return targets

    def __getNotes__(self, facilityPupilObj):
        notes = []
        for i in facilityPupilObj['reports']['Default']['Reviews']:
            noteData = i['Notes']
            noteStartDate = i['DateTime'][:10]
            temp = {'noteData': noteData, 'noteStartDate': noteStartDate}
            notes.append(temp)
        return notes

    def __getAssessmentHistory__(self, facilityPupilObj):
        if facilityPupilObj['changeList']['Default']['SENdata']['Assessment']:
            return facilityPupilObj['changeList']['Default']['SENdata']['Assessment']
        else:
            return None
###############################################################################
# putting data into WillowTree
###############################################################################

    def __importActiveAlert__(self, facilityPupilObj):
        ''' includes extra to test if a pupil's sen records can be imported '''
        if self.individualImportPupilId:
            self.willowPupilObj = Pupil.objects.get(id=int(self.individualImportPupilId))
        else:
            self.willowPupilObj = self.__getPupilObjOrError__(facilityPupilObj)
        try:
            if 'error' in self.willowPupilObj:
                return 'error'
        except:
            # delete old alert...
            if not self.individualImportPupilId:
                oldAlert = PupilAlert.objects.get(PupilId=self.willowPupilObj.id,
                                                  AlertType__id=3,
                                                  Active=True)
                oldAlert.Active = False
                oldAlert.save()
            # end
            alertType = AlertType.objects.get(id=3)
            alertData = self.__getActiveAlert__(facilityPupilObj)
            alert = PupilAlert(PupilId=self.willowPupilObj,
                               StartDate=alertData['startDate'],
                               AlertType=alertType,
                               AlertDetails=alertData['text'],
                               RaisedBy=self.me)
            if alertData['stopDate'] != "":
                alert.StopDate = alertData['stopDate']
                # code to determine if the alert should be Active or not...
                stopDateObj = datetime.datetime.strptime(alertData['stopDate'],
                                                         '%Y-%m-%d')
                if datetime.datetime.now().date() > stopDateObj.date():
                    alert.Active = False
            alert.save()
        return

    def __importSpecialPermissions__(self, facilityPupilObj):
        data = self.__getSpecialPermissions__(facilityPupilObj)
        self.pupilExtentionRecord = ExtendedRecord(BaseType='Pupil',
                                                   BaseId=self.willowPupilObj.id)
        self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                {'senExtraTime': data['extraTime']})
        self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                {'senLaptop': data['laptop']})
        return

    def __importAreasOfConcern__(self, facilityPupilObj):
        data = self.__getAreasOfConcern__(facilityPupilObj)
        if data:
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senLiteracy': self.__helperForAreasOfConcern__(data['Literacy'],
                                                    'notes')})
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senNumeracy': self.__helperForAreasOfConcern__(data['Numeracy'],
                                                    'notes')})
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senBehavior': self.__helperForAreasOfConcern__(data['Behaviour'],
                                                    'notes')})
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senCommunication': self.__helperForAreasOfConcern__(data['Communication'],
                                                    'notes')})
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senPhysical': self.__helperForAreasOfConcern__(data['Physical'],
                                                    'notes')})
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senGeneral': self.__helperForAreasOfConcern__(data['General'],
                                                    'notes')})
        return

    def __importAreasOfConcern2__(self, facilityPupilObj):
        data = self.__getAreasOfConcern__(facilityPupilObj)
        if data:
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senLiteracyTeacherNotes': self.__helperForAreasOfConcern__(data['Literacy'],
                                                    'teacherNotes')})
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senNumeracyTeacherNotes': self.__helperForAreasOfConcern__(data['Numeracy'],
                                                    'teacherNotes')})
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senBehaviorTeacherNotes': self.__helperForAreasOfConcern__(data['Behaviour'],
                                                    'teacherNotes')})
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senCommunicationTeacherNotes': self.__helperForAreasOfConcern__(data['Communication'],
                                                    'teacherNotes')})
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senPhysicalTeacherNotes': self.__helperForAreasOfConcern__(data['Physical'],
                                                    'teacherNotes')})
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                    {'senGeneralTeacherNotes': self.__helperForAreasOfConcern__(data['General'],
                                                    'teacherNotes')})

    def __importProvisions__(self, facilityPupilObj):
        for i in self.__getProvisions__(facilityPupilObj):
            provisionObj = PupilProvision(Pupil=self.willowPupilObj,
                                                  Type='SEN',
                                                  SENType='Tuition',
                                                  StartDate=i['targetDate'])
            try:
                t = unicode(i['targetData'])
                provisionObj.Details = t
            except:
                provisionObj.Target = '<<<--- unable to import --->>>'
            provisionObj.save()
        return

    def __importTargets__(self, facilityPupilObj):
        for i in self.__getTargets__(facilityPupilObj):
            targetObj = PupilTarget(Pupil=self.willowPupilObj,
                                    Type='SEN')
            try:
                t = unicode(i)
                targetObj.Target = t
            except:
                targetObj.Target = '<<<--- unable to import --->>>'
            targetObj.save()
        return

    def __importNotes__(self, facilityPupilObj):
        for i in self.__getNotes__(facilityPupilObj):
            note = NoteDetails(Keywords='Learning Support:SEN',
                               RaisedBy=self.me,
                               ModifiedBy=self.me,
                               RaisedDate=i['noteStartDate'],
                               ModifiedDate=self.todaysDate)
            try:
                t = unicode(i['noteData'])
                note.NoteText = t
            except:
                note.Target = '<<<--- unable to import --->>>'
            note.save()
            pupilNote = PupilNotes(PupilId=self.willowPupilObj,
                                   NoteId=note)
            pupilNote.save()
        return

    def __importAssessmentHistory__(self, facilityPupilObj):
        if self.__getAssessmentHistory__(facilityPupilObj) !=None:
            self.pupilExtentionRecord.WriteExtention('PupilSEN',
                                                     {'senAssessmentHistory': self.__getAssessmentHistory__(facilityPupilObj)})
        return
