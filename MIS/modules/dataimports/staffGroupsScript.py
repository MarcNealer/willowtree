import csv
from MIS.models import User
from MIS.models import Staff
from MIS.models import Group
from MIS.models import StaffGroup
from MIS.models import MenuTypes


def hasAdminRights():
    ''' displays a list of users who have admin rights '''
    for i in User.objects.all():
        hasAdmin = False
        for p in i.get_all_permissions():
            if 'Admin' in p:
                hasAdmin = True
        if hasAdmin:
            print i.username
    return


def setAllDefaultToMenusToTeacher():
    ''' sets all staff to the default teacher menu '''
    #ids of staff we do not wish to change...
    doNotChange = [2028, 1949, 2765, 2725, 3055, 2560, 2567,
                   2412, 2761, 2613, 2724, 3098, 2920, 2903,
                   3073, 3102]
    bat = MenuTypes.objects.get(Name='BatterseaTeacher')
    ful = MenuTypes.objects.get(Name='FulhamTeacher')
    cla = MenuTypes.objects.get(Name='ClaphamTeacher')
    ken = MenuTypes.objects.get(Name='KensingtonTeacher')
    staff = Staff.objects.all()
    for i in staff:
        if 'Battersea' in i.DefaultMenu.Name and i.id not in doNotChange:
            temp = i
            temp.DefaultMenu = bat
            temp.save()
        if 'Fulham' in i.DefaultMenu.Name and i.id not in doNotChange:
            temp = i
            temp.DefaultMenu = ful
            temp.save()
        if 'Clapham' in i.DefaultMenu.Name and i.id not in doNotChange:
            temp = i
            temp.DefaultMenu = cla
            temp.save()
        if 'Kensington' in i.DefaultMenu.Name and i.id not in doNotChange:
            temp = i
            temp.DefaultMenu = ken
            temp.save()
        tup = (i.Forename, i.Surname, i.DefaultMenu.Name)
        print '%s %s menu changed to %s' % tup
    pass


class staffGroupScript():
    '''
        puts staff members in their correct groups from CSV extracts...

        put script in WillowTree folder then...

        import with:
        from staffGroupsScript import staffGroupScript
    '''
    def __init__(self, school):
        self.staffGroupsTemp = ['Staff.<school>.Department.Catering',
                                'Staff.<school>.LowerSchool.Form',
                                'Staff.<school>.MiddleSchool.Form',
                                'Staff.<school>.UpperSchool.Form',
                                'Staff.<school>.Department.SysAdmin',
                                'Staff.<school>.Department.CoHODS',
                                'Staff.<school>.Department.CurrHODS',
                                'Staff.<school>.Department.IT',
                                'Staff.<school>.Department.Maintenance',
                                'Staff.<school>.Department.SLT',
                                'Staff.<school>.Department.Transport',
                                'Staff.<school>.LowerSchool.Year1',
                                'Staff.<school>.LowerSchool.Year2',
                                'Staff.<school>.MiddleSchool.Year3',
                                'Staff.<school>.MiddleSchool.Year4',
                                'Staff.<school>.MiddleSchool.Year5',
                                'Staff.<school>.UpperSchool.Year6',
                                'Staff.<school>.UpperSchool.Year7',
                                'Staff.<school>.UpperSchool.Year8',
                                'Staff.<school>.LowerSchool.Reception']
        self.school = school
        self.AcYear = "2012-2013"
        #path will change depending on which server imports are found...
        self.csvPathOrginal = "/home/dbmgr/DjangoCode/WillowTree/MIS/modules/dataimports/ImportData/Extracts/"
        if self.school == "Battersea":
            self.csvFile = self.csvPathOrginal + "Battersea/BatStaff.csv"
        if self.school == "Clapham":
            self.csvFile = self.csvPathOrginal + "Clapham/ClaStaff.csv"
        if self.school == "Fulham":
            self.csvFile = self.csvPathOrginal + "Fulham/FulStaff.csv"
        if self.school == "Kensington":
            self.csvFile = self.csvPathOrginal + "Kensington/KenStaff.csv"
        self.staffGroups = []
        for i in self.staffGroupsTemp:
            temp = i.replace('<school>', self.school)
            self.staffGroups.append(temp)

    def __AddToGroup__(self, GroupName, staffForname, staffSurname):
        sucessfullyGotRec = True
        try:
            StaffRec = Staff.objects.get(Forename=staffForname,
                                         Surname=staffSurname)
        except:
            print "---error getting %s %s's staff record---" % (staffForname,
                                                                staffSurname)
            sucessfullyGotRec = False
        if sucessfullyGotRec:
            if not StaffGroup.objects.filter(Staff=StaffRec,
                                             Group__Name=GroupName).exists():
                groupRec = Group.objects.get(Name=GroupName)
                NewGroup = StaffGroup(Staff=StaffRec,
                                      AcademicYear=self.AcYear,
                                      Group=groupRec)
                NewGroup.save()
                print "%s %s added to %s" % (staffForname,
                                             staffSurname,
                                             GroupName)
        return

    def assignStaffToGroups(self):
        ''' assign staff to groups from CSV file '''
        with open(self.csvFile, 'rb') as csvF:
            csvInMem = csv.reader(csvF, delimiter=',')
            for row in csvInMem:
                staffForname = row[1]
                staffSurname = row[2]
                counter = 0
                for i in row[7:26]:
                    if i == "1":
                        self.__AddToGroup__(self.staffGroups[counter],
                                            staffForname,
                                            staffSurname)
                    counter += 1
        return