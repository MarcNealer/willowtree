# -*- coding: utf-8 -*-
"""
Created on Sun May 19 07:55:00 2013

@author: dbmgr
"""
from MIS.models import *

def RRGroups():
    schools=['B','C','K','F']
    YearsBig=['R',1,2,3,4,5,6,7,8]
    YearsSmall=['R',1,2,3,4,5,6]
    Forms=['E','N','S','W']
    for sch in schools:
        for forms in Forms:
            classlist=[]
            if sch=='C' or sch=='B':
                Years=YearsBig
            else:
                Years=YearsSmall
            for years in Years:
                classlist.append('%s%s%s' % (years,sch,forms))
            RRCreator(None,classlist)
    return

def RRCreator(PastGroup,ClassList):
    if len(ClassList) > 0:
        NewGroup=Group.objects.get(Name__iendswith='Form.%s' % ClassList[0])
        newRule=RollOverRule(NewGroup=NewGroup)
        newRule.save()
        if Group.objects.filter(Name__iendswith=ClassList[0]).filter(Name__icontains='Accepted').filter(Name__istartswith='Applicants').exists():
            newRule.ApplicantGroup=Group.objects.filter(Name__iendswith=ClassList[0]).filter(Name__icontains='Accepted').filter(Name__istartswith='Applicants')[0]
            newRule.save()
        if PastGroup:
            newRule.OldGroup=PastGroup
            newRule.save()
        del ClassList[0]
        return RRCreator(NewGroup, ClassList)
    return