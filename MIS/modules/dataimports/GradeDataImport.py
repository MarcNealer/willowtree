#-------------------------------------------------------------------------------
# Name:        DefaultData
# Purpose:      Pushes default records into the WillowTree MIS
#
# Author:      DBMgr
#
# Created:     12/02/2012
# Copyright:   (c) DBMgr 2012
# Licence:
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from WillowTree.MIS import models
from MIS.models import *
import datetime, copy, ConfigParser, csv
from MIS.modules.ExtendedRecords import *
# Python imports
import threading

class AddImportedGrade(threading.Thread):
    ''' emails staff changes to ringwood '''
    def __init__(self,school,OldId, data):
        threading.Thread.__init__(self)
        self.school=school
        self.OldId=OldId
        self.data=data
                
    def run(self):
        if Pupil.objects.filter(Old_Id="%s-%s" % (self.school,self.OldId)).exists():
            PupilExt=ExtendedRecord(BaseType='Pupil',BaseId=Pupil.objects.get(Old_Id="%s-%s" % (self.school,self.OldId)).id)
            GradeData=eval(self.data.replace('\\x',''))
            if not Subject.objects.filter(Name=GradeData['SubjectName']).exists():
                NewSubject=Subject(Name=GradeData['SubjectName'],Desc=GradeData['SubjectName'])
                NewSubject.save()
            if ExtentionRecords.objects.filter(Name=GradeData['Exam']).exists():
                PupilExt.WriteExtention(ExtentionName=GradeData['Exam'],ExtentionData=GradeData['ExtentionData'],SubjectName=GradeData['SubjectName'])

        

def GradeImport(FileName,school):
    f=open(FileName,'r')
    found=False
    for lines in f.readlines():
        print lines
        if lines.find('Old_Id=') > -1:
            Old_StudentId=lines.split('=')[1].split(' ')[0]
            found=True
        elif found:
            ImportData=AddImportedGrade(school,Old_StudentId,lines)
            ImportData.start()



def GradeIndMusicTranslation(FileIn,FileOut):
    f_in=open(FileIn,'r')
    f_out=open(FileOut,'w')
    found=False
    for lines in f_in.readlines():
        if not 'Old_Id=' in lines:
            inputdata=eval(lines.replace('\\x',''))
            if 'IndividualMusic' in inputdata['SubjectName']:
                if 'EFFORT1' in inputdata['ExtentionData'] or 'COMMENT1' in inputdata['ExtentionData']:
                    inputdata['SubjectName']='IndividualMusic1'
                    for field in [['EFFORT1','Effort'],['TCH INIT1','TeacherName'],['COMMENT1','Comment'],['ATTAIN1','Attainment'],['INSTRUM1','Instrument']]:
                        if field[0] in inputdata['ExtentionData']:
                            inputdata['ExtentionData'][field[1]]=inputdata['ExtentionData'][field[0]]
                            del(inputdata['ExtentionData'][field[0]])
                if 'EFFORT2' in inputdata['ExtentionData'] or 'COMMENT2' in inputdata['ExtentionData']:
                    inputdata['SubjectName']='IndividualMusic2'
                    for field in [['EFFORT2','Effort'],['TCH INIT2','TeacherName'],['COMMENT2','Comment'],['ATTAIN2','Attainment'],['INSTRUM2','Instrument']]:
                        if field[0] in inputdata['ExtentionData']:
                            inputdata['ExtentionData'][field[1]]=inputdata['ExtentionData'][field[0]]
                            del(inputdata['ExtentionData'][field[0]])
            f_out.writelines('%s\n' % str(inputdata))
        else:
            f_out.writelines(lines)
            
def GradeEYFSTranslation(FileIn,FileOut):
    f_in=open(FileIn,'r')
    f_out=open(FileOut,'w')
    found=False
    for lines in f_in.readlines():
        print lines
        if not 'Old_Id=' in lines:
            found=True
            inputdata=eval(lines.replace('\\x',''))
            if inputdata['SubjectName'] in ['PSED','PSRN','KUW','CRD','PD','CL','UW','CLL']:
                if 'Comment' in inputdata['ExtentionData']:
                    inputdata['ExtentionData'][inputdata['SubjectName']]=inputdata['ExtentionData']['Comment']
                    del(inputdata['ExtentionData']['Comment'])
                inputdata['SubjectName']='Form_Comment'

            f_out.writelines('%s\n' % str(inputdata))
        else:
            f_out.writelines(lines)
        
def GradeKenCATTranslation(FileIn,FileOut):
    f_in=open(FileIn,'r')
    f_out=open(FileOut,'w')
    found=False
    for lines in f_in.readlines():
        if not 'Old_Id=' in lines:
            found=True
            inputdata=eval(lines.replace('\\x',''))
            if 'COMM' in inputdata['SubjectName'] or 'Form_Comment' in inputdata['SubjectName']:
                inputdata['SubjectName']='Form_Comment'
                testdata=inputdata['Exam'].split('_',1)
                if testdata[1].lower() in ['gen_nv','gen_vr','gen_ca','gen_qu','gen_sp']:
                    testdata[1]='_Cat'
                    inputdata['Exam']=''.join(testdata)

            f_out.writelines('%s\n' % str(inputdata))
        else:
            f_out.writelines(lines)

def GradeRemovePE(FileIn,FileOut):
    f_in=open(FileIn,'r')
    f_out=open(FileOut,'w')
    found=False
    for lines in f_in.readlines():
        if not 'Old_Id=' in lines:
            found=True
            inputdata=eval(lines.replace('\\x',''))
            if not 'PE' in inputdata['SubjectName']:
                f_out.writelines('%s\n' % str(inputdata))
        else:
            f_out.writelines(lines)