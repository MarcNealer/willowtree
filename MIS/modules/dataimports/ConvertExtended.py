# -*- coding: utf-8 -*-
"""
Created on Thu Dec 26 10:24:50 2013

@author: marc
"""

from MIS.models import *
from MIS.modules import ExtendedRecordsOld
import threading
import time
import gc

class convertExtended():
    def __init__(self):
        pass

    def convert(self, Type,RecordPartName=None):
        self.RecordPartName=RecordPartName
        self.Type = Type
        if Type == 'Pupil':
            self.__PupilData__()
        elif Type == 'Contact':
            print 'Contact found'
            self.__ContactData__()
        elif Type == 'Family':
            self.__FamilyData__()
        elif Type == 'Staff':
            self.__StaffData__()
        elif Type == 'School':
            self.__SchoolData__()
        else:
            return

        print 'Completed'
        return

    def __ConvertRecords__(self, Records,SubjectName=False):
        OldRec = ExtendedRecordsOld.ExtendedRecord(Records['Type'], Records['Id'])
        if SubjectName:
            OldData = OldRec.ReadExtention(Records['Record'].Name,Records['Subject'])
        else:
            OldData = OldRec.ReadExtention(Records['Record'].Name)
        for keys in OldData:
            OldData[keys] = OldData[keys][0:2]
        NewRec = ExtentionData(BaseType=Records['Type'],
                               BaseId=Records['Id'],
                               ExtentionRecord=Records['Record'],
                               Subject=Subject.objects.get(Name=Records['Subject']),
                               Data=OldData,
                               UpdatedOn=Records['UpdatedOn'],
                               UpdatedBy=Records['UpdatedBy'])
        NewRec.save()
        del OldData
        del NewRec
    def __PupilData__(self):
        RecordNames=ExtentionForPupil.objects.filter(ExtentionRecordId__Name__istartswith=self.RecordPartName).values('ExtentionRecordId').distinct()
        counter=1
        threaddict={}
        for recs in RecordNames:
            recname=ExtentionRecords.objects.get(id=recs['ExtentionRecordId'])
            print 'starting %s' % recname.Name
            threaddict['Thread%d' % counter]=ConvertPupilThread(recname.Name)
            threaddict['Thread%d' % counter].start()
            test=gc.collect()
            print 'Garbage collected %d' % test
        return
    def __ContactData__(self):
        Records = ExtentionForContact.objects.all()
        counter=len(Records)
        for recs in Records:
            Dataline = {'Type': 'Contact',
                        'Id': recs.ContactId.id,
                        'Record': recs.ExtentionRecordId,
                        'Subject': 'Form_Comment',
                        'UpdatedOn': recs.ExtentionUpdatedOn,
                        'UpdatedBy': recs.ExtentionUpdatedBy}
            self.__ConvertRecords__(Dataline)
            print counter
            counter-=1

    def __StaffData__(self):
        Records = ExtentionForStaff.objects.all()
        counter=len(Records)
        for recs in Records:
            print recs.StaffId.id
            Dataline = {'Type': 'Staff',
                        'Id': recs.StaffId.id,
                        'Record': recs.ExtentionRecordId,
                        'Subject': 'Form_Comment',
                        'UpdatedOn': recs.ExtentionUpdatedOn,
                        'UpdatedBy': recs.ExtentionUpdatedBy}
            self.__ConvertRecords__(Dataline)
            print counter
            counter-=1

    def __FamilyData__(self):
        Records = ExtentionForFamily.objects.all()
        for recs in Records:
            Dataline = {'Type': 'Family',
                        'Id': recs.FamilyId.id,
                        'Record': recs.ExtentionRecordId,
                        'Subject': 'Form_Comment',
                        'UpdatedOn': recs.ExtentionUpdatedOn,
                        'UpdatedBy': recs.ExtentionUpdatedBy}
            self.__ConvertRecords__(Dataline)

    def __SchoolData__(self):
        Records = ExtentionForSchool.objects.all()
        for recs in Records:
            Dataline = {'Type': 'School',
                        'Id': recs.SchoolId.id,
                        'Record': recs.ExtentionRecordId,
                        'Subject': 'Form_Comment',
                        'UpdatedOn': recs.ExtentionUpdatedOn,
                        'UpdatedBy': recs.ExtentionUpdatedBy}
            self.__ConvertRecords__(Dataline)

class ConvertPupilThread(threading.Thread):
    ''' This is a threading class used to issue emails to staff members
    when a Note is raised'''
    def __init__(self,RecordName):
        threading.Thread.__init__(self)
        self.RecordName=RecordName
    def run(self):
        Records=ExtentionForPupil.objects.filter(ExtentionRecordId__Name=self.RecordName)
        counter=len(Records)
        for recs in Records:
            if recs.ExtentionSubject:
                SubjectName = recs.ExtentionSubject
            else:
                SubjectName = 'PUPIL'
            if not Subject.objects.filter(Name=SubjectName).exists():
                newsub=Subject(Name=SubjectName,Desc='Created on Convert')
                newsub.save()
            Dataline = {'Type': 'Pupil',
                        'Id': recs.PupilId.id,
                        'Record': recs.ExtentionRecordId,
                        'Subject': SubjectName,
                        'UpdatedOn': recs.ExtentionUpdatedOn,
                        'UpdatedBy': recs.ExtentionUpdatedBy}
            self.__ConvertRecords__(Dataline)
            counter-=1
            print "%s %d" % (self.RecordName,counter)
        return
    def __ConvertRecords__(self,Records):
        saverecord=SingleRecordThread(Records)
        saverecord.start()
        saverecord.join()
        del saverecord
    def __ConvertRecordsOld__(self, Records):
        OldRec = ExtendedRecord(Records['Type'], Records['Id'])
        OldData = OldRec.ReadExtention(Records['Record'].Name,Records['Subject'])
        for keys in OldData:
            OldData[keys] = OldData[keys][0:2]
        NewRec = ExtentionData(BaseType=Records['Type'],
                               BaseId=Records['Id'],
                               ExtentionRecord=Records['Record'],
                               Subject=Subject.objects.get(Name=Records['Subject']),
                               Data=OldData,
                               UpdatedOn=Records['UpdatedOn'],
                               UpdatedBy=Records['UpdatedBy'])
        NewRec.save()
        del OldData
        del NewRec
        return
class SingleRecordThread(threading.Thread):
    ''' This is a threading class used to issue emails to staff members
    when a Note is raised'''
    def __init__(self,Records):
        threading.Thread.__init__(self)
        self.Records=Records
    def run(self):
        OldRec = ExtendedRecordsOld.ExtendedRecord(self.Records['Type'], self.Records['Id'])
        if 'PUPIL' in self.Records['Subject']:
            OldData = OldRec.ReadExtention(self.Records['Record'].Name)
        else:
            OldData = OldRec.ReadExtention(self.Records['Record'].Name,self.Records['Subject'])
        for keys in OldData:
            OldData[keys] = OldData[keys][0:2]
        NewRec = ExtentionData(BaseType=self.Records['Type'],
                               BaseId=self.Records['Id'],
                               ExtentionRecord=self.Records['Record'],
                               Subject=Subject.objects.get(Name=self.Records['Subject']),
                               Data=OldData,
                               UpdatedOn=self.Records['UpdatedOn'],
                               UpdatedBy=self.Records['UpdatedBy'])
        NewRec.save()
        return

def FindDuplicates(RecName):
    reclist=ExtentionForPupil.objects.filter(ExtentionRecordId__Name=RecName)
    print len(reclist)
    for records in reclist:
        try:
            test=ExtentionForPupil.objects.get(ExtentionRecordId=records.ExtentionRecordId,PupilId=records.PupilId,ExtentionSubject=records.ExtentionSubject)
        except:
            print "%s - %s -%d" % (records.ExtentionRecordId.Name, records.ExtentionSubject,records.PupilId.id)