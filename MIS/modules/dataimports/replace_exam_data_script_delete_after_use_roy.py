from MIS.modules.gradeInput.GradeFunctions import *
from MIS.modules.GeneralFunctions import Get_PupilList
from MIS.modules.ExtendedRecords import ExtendedRecord
from django.core.cache import cache

def run():
    pupils = [i.id for i in Get_PupilList('2013-2014', 'Current.Fulham.PrepSchool.Year5')]
    #pupils = [10470] # Test Pupil
    for i in pupils:
        try:
            writing = None
            form = None
            x = PupilGradeRecord(i, '2013-2014', 'Yr5_Sp_M')
            r = x.Grades['Yr5_Sp_M'].Grades()
            writing = x.Grades['Yr5_Sp_M'].Grades()['English_Writing'].Current()
            for k,v in writing.iteritems():
                writing[k] = v[0]
            form = x.Grades['Yr5_Sp_M'].Grades()['Form_Comment'].Current()
            for k,v in form.iteritems():
                form[k] = 0.0
            ExtendedRecord('Pupil', i).ReplaceExtention('Yr5_Sp_M', writing, 'Form_Comment')
            ExtendedRecord('Pupil', i).ReplaceExtention('Yr5_Sp_M', form, 'English_Writing')
            cache.delete('%s_%s_%s' % ('Yr5_Sp_M', 'Form_Comment', i))
            cache.delete('%s_%s_%s' % ('Yr5_Sp_M', 'English_Writing', i))
            print "pupil %d convert sucessful" % i
        except:
            print "ERROR on %d" % i
