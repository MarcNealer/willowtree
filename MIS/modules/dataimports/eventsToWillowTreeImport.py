'''
HOW TO USE -->

1) Firstly run the "Events_import_to_WillowTree.py" program on all four Facility
   servers.

2) This will save .p files to the "/home/login/Dropbox/eventsToWillowTree" folder.
   now open a django's ipython shell on the server you wish to import to and
   run this program for all four .p files.

3) Remeber to include a school when running this program, Or the log files will
   overwrite eachother.

4) use the individualImport option, specifying a student's name in this format:
   'Roy_Hanley' - use x.keys() if needed.
'''

# python imports
import pickle
import datetime

# WillowTree imports
from MIS.models import Pupil
from MIS.models import Staff
from MIS.models import NoteDetails
from MIS.models import PupilNotes

# Django imports
from django.core.cache import cache

class run():
    ''' this code is to be used in conjuction with the 'Events_to_WillowTree.py'
    scripts on the Facility servers - roy '''
    def __init__(self, importFile,
                 school,
                 individualImportPupilName=False,
                 individualImportPupilId=False):
        if individualImportPupilName:
            self.individualImportPupilName = individualImportPupilName
        if individualImportPupilId:
            self.individualImportPupilId = individualImportPupilId
        self.school = school
        if self.__checkSchoolFormat__():
            self.todaysDate = self.__todaysDate__()
            self.me = Staff.objects.get(EmailAddress="RHanley@Thomas-s.co.uk")
            self.logFile = open('eventsToWillowTreeImport%s.log' % self.school, 'w')
            self.senObj = pickle.load(open(importFile, 'rb'))
            if self.individualImportName:
                self.senObj = {self.individualImportName: self.senObj[self.individualImportName]}
            jobLength = len(self.senObj)
            counter = 0
            if self.individualImportPupilName:
                for k, v in self.senObj[self.individualImportName]:
                    test = self.__getPupilObjOrError__(k)
                    if test:
                        self.__import__(v)
                        cache.delete('Pupil_%s' % self.PupilObj.id)
                        counter += 1
                        print jobLength - counter
            else:
                for k, v in self.senObj.iteritems():
                    test = self.__getPupilObjOrError__(k)
                    if test:
                        self.__import__(v)
                        cache.delete('Pupil_%s' % self.PupilObj.id)
                        counter += 1
                        print jobLength - counter
            self.logFile.close()

###############################################################################
# helper methods
###############################################################################
    def __checkSchoolFormat__(self):
        if 'Bat' in self.school or 'Cla' in self.school or 'Ken' in self.school or 'Ful' in self.school:
            return True
        else:
            print "ERROR: Enter either 'Bat', 'Cla', 'Ken' or 'Ful' as school!"
            return False

    def __todaysDate__(self):
        now = datetime.datetime.now()
        return now.strftime("%Y-%m-%d")

    def __getPupilObjOrError__(self, dictKey):
        if self.individualImportPupilId:
            self.PupilObj = Pupil.objects.get(id=int(self.individualImportPupilId))
            return True
        else:
            dictKey = "%s-%s" % (self.school, dictKey)
            try:
                self.PupilObj = Pupil.objects.get(Old_Id=dictKey)
                self.logFile.write('Importing sen records for --> %s %s, dob:%s\n' % (self.PupilObj.Forename,
                                                                                      self.PupilObj.Surname,
                                                                                      self.PupilObj.DateOfBirth))
                return True
            except:
                self.logFile.write('error: no Old_Id match for pupil with old Id of %s' % dictKey)
                return False

    def __convertToUniCode__(self, string):
        for i in ['\x90', '\x91', '\x92' ,'\x93', '\x94',
                  '\x95','\x96', '\x97', '\x98', '\x99']:
            string = string.replace(i, '')
        try:
            string = unicode(string)
        except:
            string = "<p><<<--- unable to import --->>></p>"
        return string

    def __convertDictToString__(self, dictionary):
        returnString = "Imported from Facility:<br /><br />"
        for k, v in dictionary.iteritems():
            tempString = "%s: %s<br />" % (k, v)
            returnString += tempString
        return self.__convertToUniCode__(returnString)

    def __saveNote__(self, keywordString, raisedDate, noteText):
        checkKeywords = self.__checkForSpecialKeywords__(keywordString,
                                                         noteText)
        if checkKeywords:
            keywordString = checkKeywords[:-1]
        note = NoteDetails(Keywords=keywordString,
                           RaisedBy=self.me,
                           ModifiedBy=self.me,
                           RaisedDate=raisedDate,
                           ModifiedDate=self.todaysDate,
                           NoteText=noteText)
        note.save()
        pupilNote = PupilNotes(PupilId=self.PupilObj,
                               NoteId=note)
        pupilNote.save()
        return

    def __checkForSpecialKeywords__(self, keywordString, string):
        ''' returns bool if there are any of the following words in the
        string: Bullying, Racist or Sexist '''
        returnValue = ''
        if 'Bullying' in string or 'bullying' in string:
            returnValue += 'Bullying:'
        if 'Racist' in string or 'racist' in string:
            returnValue += 'Racist:'
        if 'Sexist' in string or 'sexist' in string:
            returnValue += 'Sexist:'
        return returnValue

###############################################################################
# import events/notes
###############################################################################

    def __import__(self, facilityPupilObj):
#        facilityData = [['Communication:Formal:Informal',facilityPupilObj['EventRep0']],
#                        ['Achievement:Other', facilityPupilObj['EventRep1']],
#                        ['Pastoral:Conduct:Positive1', facilityPupilObj['EventRep2']],
#                        ['Pastoral:Conduct:Negative1', facilityPupilObj['EventRep3']],
#                        ['Pastoral:Conduct:Positive1', facilityPupilObj['EventRep4']],
#                        ['Pastoral:Conduct:Negative1', facilityPupilObj['EventRep5']],
#                        ['Pastoral:Conduct:Positive1', facilityPupilObj['EventRep6']],
#                        ['Pastoral:Conduct:Negative1', facilityPupilObj['EventRep7']],
#                        ['Achievement:Other', facilityPupilObj['EventRep8']],
#                        ['Pastoral:Conduct:Positive1', facilityPupilObj['EventRep9']],
#                        ['Pastoral:Conduct:Negative1', facilityPupilObj['EventRep10']],
#                        ['Communication:Formal:Informal:Parents:Teacher', facilityPupilObj['EventRep11']],
#                        ['Learning Support:SEN', facilityPupilObj['EventRep12']],
#                        ['Learning Support:SEN', facilityPupilObj['EventRep13']],
#                        ['Communication:Parents:Teacher', facilityPupilObj['EventRep14']],
#                        ['Communication:Pastoral:Teacher', facilityPupilObj['EventRep15']],
#                        ['Communication:Parents:Teacher', facilityPupilObj['EventRep16']],
#                        ['Communication:Pupil:Teacher', facilityPupilObj['EventRep17']],
#                        ['Communication:Pupil:Teacher', facilityPupilObj['EventRep18']],
#                        ['Communication:Parents:Teacher:Future Schools', facilityPupilObj['EventRep19']],
#                        ['Achievement:Ballet', facilityPupilObj['EventRep20']],
#                        ['Achievement:Drama', facilityPupilObj['EventRep21']],
#                        ['Pastoral:Conduct:Positive1', facilityPupilObj['EventRep22']],
#                        ['Achievement:Prize', facilityPupilObj['EventRep23']],
#                        ['Future Schools', facilityPupilObj['EventRep24']]]
        # start
        # for yearly synopis only... DELETE IF NOT NEEDED!!!
        facilityData = [['Communication:Pastoral:Informal',
                         facilityPupilObj['EventRep25']]]
        # end
        for data in facilityData:
            keywordString = data[0]
            dataList = data[1]
            if dataList != 'No Data':
                for i in dataList:
                    noteText = self.__convertDictToString__(i)
                    try:
                        inputDate = i['DateTime'][:10]
                    except:
                        inputDate = self.__todaysDate__()
                    self.__saveNote__(keywordString, inputDate, noteText)
        return
