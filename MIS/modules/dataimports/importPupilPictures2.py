# python imports
from PIL import Image

# django imports
from MIS.models import Pupil
from django.core.cache import cache
from django.core.files.storage import default_storage as storage


class ImportPupilPictures2():
    """
    Mass uploading of pupils pictures.
    """
    def __init__(self, request):
        self.messages = list()
        self.pictures = request.FILES.getlist('pictures')
        self.supported_pil_formats = ['.bmp',
                                      '.dib',
                                      '.dcx',
                                      '.eps',
                                      '.ps',
                                      '.gif',
                                      '.im',
                                      '.jpg',
                                      '.jpe',
                                      '.jpeg',
                                      '.pcd',
                                      '.pcx',
                                      '.pdf',
                                      '.png',
                                      '.pbm',
                                      '.pgm',
                                      '.ppm',
                                      '.psd',
                                      '.tif',
                                      '.tiff',
                                      '.xbm',
                                      '.xpm']

    def __supported_pil_format__(self, file_name):
        """
        Tries to retrieve a pupil record from the title of the uploaded file.
        Files should be in the format of: <forename> <surname>.jpg
        """
        for supported_pil_format in self.supported_pil_formats:
            if supported_pil_format not in file_name.name.lower():
                return True
            else:
                self.messages.append([file_name.name, 'Image format not supported', 'ERROR'])
                return False

    def __get_pupil_record_from_file_name__(self, file_name):
        """
        Tries to retrieve a pupil record from the title of the uploaded file.
        Files should be in the format of: <forename> <surname>.jpg
        """
        file_name_as_list = file_name.name.split('.')[0].split(' ')
        forename = file_name_as_list[0]
        last_name = ' '.join(file_name_as_list[1:])
        pupil_record_test = Pupil.objects.filter(Forename=forename, Surname=last_name, Active=True)
        if pupil_record_test.count() is 1:
            return pupil_record_test[0]
        elif pupil_record_test.count() > 1:
            self.messages.append([file_name.name, 'More than one pupil record for this file_name returned', 'ERROR'])
            return False
        else:
            self.messages.append([file_name.name, 'No pupil for this file_name returned', 'ERROR'])
            return False

    def upload_pupil_pictures(self):
        """
        Main logic for uploading pupil pictures.
        """
        for file_name in self.pictures:
            supported_pil_format_test = self.__supported_pil_format__(file_name)
            if supported_pil_format_test:
                pupil_record = self.__get_pupil_record_from_file_name__(file_name)
                if pupil_record:
                    try:
                        pupil_record.Picture = file_name
                        pupil_record.save()
                        image = Image.open(pupil_record.Picture)
                        width = 170
                        height = 125
                        image = image.resize((width, height), Image.ANTIALIAS)
                        s3_store = storage.open(pupil_record.Picture.file.name, "w")
                        image.save(s3_store)
                        s3_store.close()
                        cache.delete('Pupil_%s' % pupil_record.id)
                        self.messages.append([file_name.name, "Image successfully updated", 'SUCCESS'])
                    except Exception as error:
                        self.messages.append([file_name.name, error, 'ERROR'])
        return self.messages