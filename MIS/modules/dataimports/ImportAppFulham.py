from WillowTree.MIS import models
from MIS.models import *
import datetime, copy, ConfigParser, csv
from MIS.modules.ExtendedRecords import *
from MIS.modules import GeneralFunctions

def importApplicantsFulham(FileName,School,AcademicYear):
    ImportItems=csv.DictReader(open(FileName,'rb'))
    for Items in ImportItems:
        day,month,year=Items['Date of birth'].split('-')

        print "%s %s" % (Items['Forename'],Items['Surname'])
        if not Pupil.objects.filter(Old_Id=Items['Id']).exists():
            newPupil=Pupil(Old_Id=Items['Id'],Forename=Items['Forename'], Surname=Items['Surname'], NickName = Items['Called name'],
            OtherNames=Items['Forename 2'],Gender=Items['Sex'][0],DateOfBirth=datetime.datetime(int(year),int(month),int(day)),
            Picture='images/StudentPhotos/%s.jpg' % (Items['Id']),EmailAddress=Items['Email'])
            newPupil.save()
            if Items['Internet Permission']=='1':
                I_Perm=True
            else:
                I_Perm=False
            if Items['Media Permission']=='1':
                M_Perm=True
            else:
                M_Perm=False
            ExtraItems=ExtendedRecord(BaseType='Pupil',BaseId=newPupil.id)
            UpdateData={'UPN':Items['Unique Pupil Number'],'Ethnicity':Items['Ethnicity'],'Religion':Items['Religion'],'Nationality':Items['Nationality'],'PrimaryLanguage':Items['Language'],'P_Internet':I_Perm,'P_Media':M_Perm}
            ExtraItems.WriteExtention('PupilExtra',UpdateData)
            if not Address.objects.filter(AddressLine1=Items['Address1'],PostCode=Items['Home post code']).exists():
                NewAddress=Address(HomeSalutation=Items['Home salutation'], PostalTitle=Items['Postal title'],
                                   AddressLine1=Items['Address1'],AddressLine2=Items['Address2'],AddressLine3=Items['Address3'],AddressLine4=Items['Address4'],
                PostCode=Items['Home post code'],Phone1=Items['Home phone number'],Phone2=Items['Mobile phone number'],EmailAddress=Items['Email'])
                NewAddress.save()
                FamilyID=Family(FamilyName='%s-%d' % (Items['Surname'],newPupil.id))
                FamilyID.save()
                FamilyID.Address.add(NewAddress)
            else:
                if Family.objects.filter(Address=Address.objects.get(AddressLine1=Items['Address1'],PostCode=Items['Home post code'])).exists():
                    FamilyID=Family.objects.get(Address=Address.objects.get(AddressLine1=Items['Address1'],PostCode=Items['Home post code']))
                else:
                    FamilyID=Family(FamilyName='%s-%d' % (Items['Surname'],newPupil.id))
                    FamilyID.save()
            NewFamilyChild=FamilyChildren(FamilyId=FamilyID,Pupil=newPupil)
            NewFamilyChild.save()
                    
            
            
        else:
            newPupil=Pupil.objects.get(Old_Id=Items['Id']) 

        EntryYear = '%d-%d' % (int(Items['Entry Year']),int(Items['Entry Year'])+1)
        if 'REC' in Items['Course'] and not 'Reserve' in Items['Class']:
            GeneralFunctions.AssignApplicantGroup(newPupil.id,'Main.Reception',School,EntryYear)
        elif 'Reserve' in Items['Class']:
            GeneralFunctions.AssignApplicantGroup(newPupil.id,'Reserve',School,EntryYear)
        elif 'KS1' in Items['Course'] or 'KS2' in Items['Course']:
            GeneralFunctions.AssignApplicantGroup(newPupil.id,'Main.Year%d' % int(Items['Year']),School,EntryYear)

