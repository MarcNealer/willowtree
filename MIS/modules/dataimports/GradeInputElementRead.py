#-------------------------------------------------------------------------------
# Name:        DefaultData
# Purpose:      Pushes default records into the WillowTree MIS
#
# Author:      DBMgr
#
# Created:     12/02/2012
# Copyright:   (c) DBMgr 2012
# Licence:
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from MIS.models import *
import datetime, copy, csv

def ImportGradeElements(FileName):
    inputdata=csv.DictReader(open(FileName,'rb'))
    for records in inputdata:
        GradeRecord=ExtentionRecords.objects.get(Name=records['ReportRecord'])
        grouprecs=Group.objects.filter(Name__iendswith=records['GroupEnd']).filter(Name__istartswith='Current')
        for groups in grouprecs:
            newSeason=ReportingSeason(GradeRecord=GradeRecord,
                                      GradeGroup=groups,
                                      GradeStartDay=records['StartDay'],
                                      GradeStopDay=records['EndDay'])
            newSeason.save()
        subjectlist=records['Subjects'].split(':')
        if records['PrevRecord']:
            PrevRecord=ExtentionRecords.objects.get(Name=records['PrevRecord'])
        for subjects in subjectlist:
            if records['PrevRecord']:
                newElement=GradeInputElement(GradeSubject=Subject.objects.get(Name=subjects),
                                             GradeRecord=GradeRecord,
                                             PrevGradeRecord=PrevRecord,
                                             InputForm='GradeInputForms/StandardForm.html',
                                             DisplayOrder=1)
            else:
                newElement=GradeInputElement(GradeSubject=Subject.objects.get(Name=subjects),
                                             GradeRecord=GradeRecord,
                                             InputForm='GradeInputForms/StandardForm.html',
                                             DisplayOrder=1)
            newElement.save()
            
        