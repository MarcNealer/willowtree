# -*- coding: utf-8 -*-
"""
Created on Mon Jan 13 09:44:23 2014

@author: marc
"""
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import *

def UpdateHouseLists(schoolname,AcYear):
    #  get a list of all pupils in the selected school
    pupilist=GeneralFunctions.Get_PupilList(AcYear,'Current.%s' % schoolname)
    # for each pupil in the list
    for pupil in pupilist:
        # get the pupilObject
        pupilrec=PupilRecord(pupil.id,AcYear)
        AcademicHouse=pupilrec.AcademicHouse()
        #check to see if they are in the current House group
        if not PupilGroup.objects.filter(Pupil=pupilrec.Pupil,Group__Name='Current.%s.House.House.%s' %(schoolname,AcademicHouse),
                                         Active=True,AcademicYear=AcYear).exists():
            newgroup=Group.objects.get(Name='Current.%s.House.House.%s' % (schoolname,AcademicHouse))
            newpupilGroup=PupilGroup(Pupil=pupilrec.Pupil,Group=newgroup,AcademicYear=AcYear)
            newpupilGroup.save()
    # if not add them