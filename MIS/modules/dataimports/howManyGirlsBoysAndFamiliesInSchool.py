from MIS.models import PupilGroup
from MIS.modules.PupilRecs import PupilRecord


def how_many_girls_boys_and_families_in_school(school, AcYear):
    '''

    returns a dictionary showing how many
    girls boys and families in a school

    '''
    total = 0
    girls = 0
    boys = 0
    pupilIdList = [i.Pupil.id for i in set(PupilGroup.objects.filter(Group__Name__icontains='Current.%s' % school,
                                                                     AcademicYear=AcYear,
                                                                     Active=True))]
    pupilIdList = set(pupilIdList)
    familyIdList = []
    lengthOfJob = len(pupilIdList)
    counter = 0
    for i in pupilIdList:
        pupilObj = PupilRecord(i, AcYear)
        total += 1
        if pupilObj.Pupil.Gender == 'M':
            boys += 1
        if pupilObj.Pupil.Gender == 'F':
            girls += 1
        familyIdList.append(pupilObj.FamilyRecord().id)
        counter += 1
        print "%s of %s" % (counter, lengthOfJob)
    noOfFamilies = len(set(familyIdList))
    return {'total': total,
            'boys': boys,
            'girls': girls,
            'families': noOfFamilies}

