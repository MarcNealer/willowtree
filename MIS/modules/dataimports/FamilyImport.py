#-------------------------------------------------------------------------------
# Name:        DefaultData
# Purpose:      Pushes default records into the WillowTree MIS
#
# Author:      DBMgr
#
# Created:     12/02/2012
# Copyright:   (c) DBMgr 2012
# Licence:
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from WillowTree.MIS import models
from MIS.models import *
import datetime, copy, ConfigParser, csv
from MIS.modules.ExtendedRecords import *
from MIS.modules import GeneralFunctions


def importPupils(FileName, School, AcademicYear):
    ImportItems = csv.DictReader(open(FileName, 'rb'))
    for Items in ImportItems:
        day, month, year = Items['Date of birth'].split('-')

        print "%s %s" % (Items['Forename'], Items['Surname'])
        if not Pupil.objects.filter(Old_Id=Items['Id']).exists():
            newPupil = Pupil(Old_Id=Items['Id'],
                           Forename=Items['Forename'],
                            Surname=Items['Surname'],
                            NickName=Items['Called name'],
                            OtherNames=Items['Forename 2'],
                            Gender=Items['Sex'][0],
                            DateOfBirth=datetime.datetime(int(year),
                                                          int(month),
                                                          int(day)),
            Picture='images/StudentPhotos/%s.jpg' % (Items['Id']),
                                            EmailAddress=Items['Email'])
            newPupil.save()
            if Items['Internet Permission'] == '1':
                I_Perm = True
            else:
                I_Perm = False
            if Items['Media Permission'] == '1':
                M_Perm = True
            else:
                M_Perm = False
            ExtraItems=ExtendedRecord(BaseType='Pupil',BaseId=newPupil.id)
            UpdateData={'UPN':Items['Unique Pupil Number'],'Ethnicity':Items['Ethnicity'],'Religion':Items['Religion'],'Nationality':Items['Nationality'],'PrimaryLanguage':Items['Language'],'P_Internet':I_Perm,'P_Media':M_Perm}
            ExtraItems.WriteExtention('PupilExtra',UpdateData)
            if not Address.objects.filter(AddressLine1=Items['Address1'],PostCode=Items['Home post code']).exists():
                NewAddress=Address(HomeSalutation=Items['Home salutation'], PostalTitle=Items['Postal title'],
                                   AddressLine1=Items['Address1'],AddressLine2=Items['Address2'],AddressLine3=Items['Address3'],AddressLine4=Items['Address4'],
                PostCode=Items['Home post code'],Phone1=Items['Home phone number'],Phone2=Items['Mobile phone number'],EmailAddress=Items['Email'])
                NewAddress.save()
                FamilyID=Family(FamilyName='%s-%d' % (Items['Surname'],newPupil.id))
                FamilyID.save()
                FamilyID.Address.add(NewAddress)
            else:
                if Family.objects.filter(Address=Address.objects.get(AddressLine1=Items['Address1'],PostCode=Items['Home post code'])).exists():
                    FamilyID=Family.objects.get(Address=Address.objects.get(AddressLine1=Items['Address1'],PostCode=Items['Home post code']))
                else:
                    FamilyID=Family(FamilyName='%s-%d' % (Items['Surname'],newPupil.id))
                    FamilyID.save()
            NewFamilyChild=FamilyChildren(FamilyId=FamilyID,Pupil=newPupil)
            NewFamilyChild.save()
                    
            
            
        else:
            newPupil=Pupil.objects.get(Old_Id=Items['Id'])

        
        GeneralFunctions.AssignGroup(newPupil.id,Items['Class'],School,AcademicYear)
        GeneralFunctions.AssignGroup(newPupil.id,Items['Ac House'],School,AcademicYear)
        
def importContacts(FileName,School,AcademicYear):
    ImportItems=csv.DictReader(open(FileName,'rb'))
    for Items in ImportItems:
        if not Contact.objects.filter(Old_Id=Items['Id']).exists():              
            print "%s %s" % (Items['Forename'],Items['Surname'])
            NewContact=Contact(Forename=Items['Forename'],Surname=Items['Surname'],Title=Items['Title'],Gender=Items['Sex'][0],EmailAddress=Items['Email'],Old_Id=Items['Id'])
            NewContact.save()
            if not Address.objects.filter(AddressLine1=Items['Address1'],PostCode=Items['Home post code']).exists():
                print len(Items['Address1'])
                if len(Items['Address1']) > 2:
                    AddressId=Address(HomeSalutation=Items['Home salutation'], PostalTitle=Items['Home postal title'],
                                      AddressLine1=Items['Address1'],AddressLine2=Items['Address2'],AddressLine3=Items['Address3'],AddressLine4=Items['Address4'],
                                      PostCode=Items['Home post code'],Phone1=Items['Home phone number'],Phone2=Items['Mobile phone number'],EmailAddress=Items['Email'])
                    AddressId.save()
            else:
                if len(Items['Address1']) > 2:
                    AddressId=Address.objects.get(AddressLine1=Items['Address1'],PostCode=Items['Home post code'])
            if len(Items['Address1']) > 2:
                NewContact.Address=AddressId
                NewContact.save()
        else:
            NewContact=Contact.objects.get(Old_Id=Items['Id'])
                    
        # find Family and attach if not already attached
        if FamilyChildren.objects.filter(Pupil__Old_Id=Items['PupilId'],FamilyType=1).exists():
            FamilyDetails=FamilyChildren.objects.get(Pupil__Old_Id=Items['PupilId'],FamilyType=1).FamilyId
            if not FamilyContact.objects.filter(FamilyId=FamilyDetails,Contact=NewContact).exists():
                if Items['Relationship'].find('PAM') > -1:
                    Rel='Mother'
                else:
                    Rel='Father'
                if not FamilyContact.objects.filter(FamilyId=FamilyDetails,Relationship=FamilyRelationship.objects.get(Type=Rel)).exists():
                    NewFamilyContact=FamilyContact(FamilyId=FamilyDetails,Contact=NewContact,Relationship=FamilyRelationship.objects.get(Type=Rel),Priority=int(Items['Priority']))
                    NewFamilyContact.save()

def importContactsMobiles(FileName):
    ImportItems=csv.DictReader(open(FileName,'rb'))  
    for Items in ImportItems:
        if Contact.objects.filter(Old_Id=Items['Id']).exists():              
            print "%s %s" % (Items['Forename'],Items['Surname'])
            conRec=Contact.objects.get(Old_Id=Items['Id'])
            ExtRec=ExtendedRecord('Contact',conRec.id)
            ExtRec.WriteExtention('ContactExtra',{'mobileNumber':Items['Mobile phone number']})
    return


def importKindie(FileName,School,AcademicYear): 
    ImportItems=csv.DictReader(open(FileName,'rb'))
    for Items in ImportItems:
        day,month,year=Items['Date of birth'].split('-')

        print "%s %s" % (Items['Forename'],Items['Surname'])
        if not Pupil.objects.filter(Old_Id=Items['Id']).exists():
            newPupil=Pupil(Old_Id=Items['Id'],Forename=Items['Forename'], Surname=Items['Surname'], NickName = Items['Called name'],
            OtherNames=Items['Forename 2'],Gender=Items['Sex'][0],DateOfBirth=datetime.datetime(int(year),int(month),int(day)),
            Picture='images/StudentPhotos/%s.jpg' % (Items['Id']),EmailAddress=Items['Email'])
            newPupil.save()
            if Items['Internet Permission']=='1':
                I_Perm=True
            else:
                I_Perm=False
            if Items['Media Permission']=='1':
                M_Perm=True
            else:
                M_Perm=False
            ExtraItems=ExtendedRecord(BaseType='Pupil',BaseId=newPupil.id)
            UpdateData={'UPN':Items['Unique Pupil Number'],'Ethnicity':Items['Ethnicity'],'Religion':Items['Religion'],'Nationality':Items['Nationality'],'PrimaryLanguage':Items['Language'],'P_Internet':I_Perm,'P_Media':M_Perm}
            ExtraItems.WriteExtention('PupilExtra',UpdateData)
            if not Address.objects.filter(AddressLine1=Items['Address1'],PostCode=Items['Home post code']).exists():
                NewAddress=Address(HomeSalutation=Items['Home salutation'], PostalTitle=Items['Postal title'],
                                   AddressLine1=Items['Address1'],AddressLine2=Items['Address2'],AddressLine3=Items['Address3'],AddressLine4=Items['Address4'],
                PostCode=Items['Home post code'],Phone1=Items['Home phone number'],Phone2=Items['Mobile phone number'],EmailAddress=Items['Email'])
                NewAddress.save()
                FamilyID=Family(FamilyName='%s-%d' % (Items['Surname'],newPupil.id))
                FamilyID.save()
                FamilyID.Address.add(NewAddress)
            else:
                if Family.objects.filter(Address=Address.objects.get(AddressLine1=Items['Address1'],PostCode=Items['Home post code'])).exists():
                    FamilyID=Family.objects.get(Address=Address.objects.get(AddressLine1=Items['Address1'],PostCode=Items['Home post code']))
                else:
                    FamilyID=Family(FamilyName='%s-%d' % (Items['Surname'],newPupil.id))
                    FamilyID.save()
            NewFamilyChild=FamilyChildren(FamilyId=FamilyID,Pupil=newPupil)
            NewFamilyChild.save()
                    
            
            
        else:
            newPupil=Pupil.objects.get(Old_Id=Items['Id'])

        
        if Items['Class']=='PLK':
            ClassGroup='Current.Kindergarten.Pimlico.Lower'
        elif Items['Class']=='PUK':
            ClassGroup='Current.Kindergarten.Pimlico.Upper'
        elif Items['Class']=='BLK':
            ClassGroup='Current.Kindergarten.Battersea.Lower'
        elif Items['Class']=='BUL':
            ClassGroup='Current.Kindergarten.Battersea.Upper'
        print ClassGroup
        print Items['Class']
            
        NewPupilGroup=PupilGroup(Pupil=newPupil,Group=Group.objects.get(Name=ClassGroup))
        NewPupilGroup.save()


class ContactExtraImport():

    def __init__(self, FileName):
        self.ImportItems = csv.DictReader(open(FileName, 'rb'))
        self.database={}
        self.WillowDB={}
    def ReadRecords(self):
        for record in self.ImportItems:
            if self.__exists__(record['Id']):
                self.__UpdateItems__(record)
            else:
                self.__AddRecord__(record)
        return
    def ConvertToWillowTree(self):
        for keys,values in self.database.items():
            if Contact.objects.filter(Surname=values['Surname'],Forename=values['Forename']).exists():
                self.WillowDB[Contact.objects.filter(Surname=values['Surname'],Forename=values['Forename'])[0].id]=values
        return
                
    def __AddRecord__(self,record):
        self.database[record['Id']]={'Surname':record['Surname'],'Forename':record['Forename']}
        self.__UpdateItems__(record)
        return
    def __UpdateItems__(self,record):
        self.__UpdateItem__('Email','EmailAddress',record)
        self.__UpdateItem__('Mobile phone number','mobileNumber',record)
        self.__UpdateItem__('Job Type','JobType',record)
        self.__UpdateItem__('Nationality','Nationality',record)
        self.__UpdateItem__('Religion','Religion',record)
        self.__UpdateItem__('Company','Company', record)
        self.__UpdateItem__('Work phone number','WorkPhone',record)
        self.__UpdateItem__('Work Email Address','WorkEmail',record)
    def __exists__(self,RecordId):
        return RecordId in self.database
    def UpdateWillowTree(self):
        for keys,values in self.WillowDB.items():
            ExtraItems=ExtendedRecord(BaseType='Contact',BaseId=keys)
            ExtraItems.WriteExtention('ContactExtra',values)
        return
    def __UpdateItem__(self,ItemName,NewName,record):
        if not NewName in self.database[record['Id']] and len(record[ItemName]) > 0:
            self.database[record['Id']][NewName]=record[ItemName]
        return