from MIS.models import *
import datetime, copy, csv


def findPupils(FileName,OutfileName,AcYear):
    ImportItems=csv.DictReader(open(FileName,'rb'))
    notfoundlist=[]
    for items in ImportItems:
        try:
            names=items['Name'].split()
            surname=names[0]
            forename=names[1]
            if not (PupilGroup.objects.filter(Group__Name__istartswith='Current', Pupil__Forename__iexact=forename,Pupil__Surname__iexact=surname,AcademicYear=AcYear,Active=True).exists() or PupilGroup.objects.filter(Group__Name__istartswith='Current', Pupil__NickName__iexact=forename,Pupil__Surname__iexact=surname,AcademicYear=AcYear,Active=True).exists() or PupilGroup.objects.filter(Group__Name__istartswith='Current', Pupil__Surname__iexact=forename,Pupil__Forename__iexact=surname,AcademicYear=AcYear,Active=True).exists() or PupilGroup.objects.filter(Group__Name__istartswith='Current', Pupil__Surname__iexact=forename,Pupil__NickName__iexact=surname,AcademicYear=AcYear,Active=True).exists()):
                print '%s not found' % surname
                notfoundlist.append([forename,surname])
        except:
            notfoundlist.append(items['Name'])
    fileobj=open(OutfileName ,'wb')
    csvObj=csv.writer(fileobj)
    csvObj.writerows(notfoundlist)
    fileobj.close()



