(dp0
S'Maximilian_Maddox'
p1
(dp2
S'changeList'
p3
(dp4
S'Default'
p5
(dp6
S'SENRepVT'
p7
I0
sS'Numeracy'
p8
(lp9
S'Hector has difficulties with mathematical reasoning.'
p10
aS'To help with his mathematical reasoning skills encourage Hector to work extra hard to visualise mathematical problems. The teacher or Hector could draw a picture to help understand the problem, and help him to take the time to look at any visual information that is provided, for example pictures, charts or graphs, etc.  When he is presented with new material make sure he is able to write down each step and talk it through until he understands it well enough to teach it to someone else.'
p11
asS'Description'
p12
S'Sen provision map for Maximilian Maddox'
p13
sS'SchoolName'
p14
S'Clapham'
p15
sS'Communication'
p16
(lp17
S'Toby has some social communication difficulties.'
p18
aS'Be very clear when giving instructions or having conversations with Toby and avoid using vague or confusing language. Asking him to verbalise the instruction will help ensure understanding. Encourage Toby to look at the person he is speaking to and support conversations where possible.'
p19
asS'CreateDate'
p20
S'2013-07-31'
p21
sS'Title'
p22
S'Special Needs Provision Map for Maximilian Maddox'
p23
sS'SENRepOT'
p24
I0
sS'General'
p25
(lp26
S''
p27
aS'To help support Callum with organisation and planning, teachers should ensure that the area around Callum is free from visual clutter. Use checklists, reminders and any other visual cues necessary to reduce auditory stimulus. Seat him in a clear space near the board to reduce possible distractions. When necessary and if appropriate close blinds to avoid bright light and monitor the noise level around him. Break tasks down into chunks and give clear step by step guidance and support throughout the task. Teachers should support Callum to help him complete tasks however maintain the focus of building independence and self regulatory behaviours. Encourage and praise independent achievement through maintaining focus and utilising well thought out processes. Additional visual supports may be required to ensure he has the necessary equipment for lessons. Pre-prepared sheets should be given where appropriate and the use of a Laptop should additionally be used to support the planning and writing process.'
p28
asS'Behaviour'
p29
(lp30
g27
aS'Always support socially appropriate behaviours.'
p31
asS'Keywords'
p32
S'SEN,Provision map,Maximilian Maddox'
p33
sS'EventNames'
p34
(lp35
S'Provisions'
p36
aS'Targets'
p37
aS'Reviews'
p38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asS'SENdata'
p39
(dp40
S'StartDate'
p41
S'01-09-2011'
p42
sS'Surname'
p43
S'Maddox'
p44
sS'Provision'
p45
S'Action'
p46
sS'DOB'
p47
S'2005-03-24'
p48
sS'Assessment'
p49
S'Observed by NB on 12.10.11\r<br>EP Annie Mitchell 07.12.2012'
p50
sS'Laptop'
p51
g27
sS'Year'
p52
S'3'
p53
sS'StopDate'
p54
S'07-12-2011'
p55
sS'Precis'
p56
S'Max has strong verbal skills in English despite being bilingual and his verbal reasoning is particularly strong. He is less able at the visual spatial matching and abstract reasoning and gets a little distracted on visual tasks. He processes visual information quickly but not always carefully. He must learn to double check before he answers. Max is showing specific difficulties with working auditory and visual memory. Max is dyslexic. His underlying phonological awareness and processing skills are weaker and because of this can become quite easily distracted. He has worked very hard in year 2 with his Learning Specialist teacher who will continue with him in year 3.'
p57
sS'EALlevel'
p58
g27
sS'Forename'
p59
S'Maximilian'
p60
sS'ExtraTime'
p61
g27
sS'Class'
p62
S'3CS'
p63
ssS'SENRepPsy'
p64
I1
sS'SENRepHR'
p65
I0
sS'Literacy'
p66
(lp67
S'Max is dyslexic. His underlying phonological awareness and processing skills are weak and because of this can become quite easily distracted.'
p68
aS"Make sure the classroom is not too visually stimulating as it can be distracting. Written work should be differentiated and ensure he sits face on to the board. He shouldn't be expected to copy a great deal from the board and perhaps not routinely write the date and title, when appropriate scribe for him to allow his imagination to develop and not be hampered by his weaker writing skills. When reading, encourage him to follow with his finger in the book. Encourage him to read a problem through and explain it to you so it is clear he knows what to do. Also he should be able to tell you what he is going to do first. Make sure instructions are explicit and very clear and cue him in using his name. Give Max some time to respond to instructions as he doesn't process auditory information accurately. Give him visual cues and prompts to help him maintain attention as the multisensory approach is most helpful. Try to reduce auditory distractions where possible. Practice him acting on a sequence of instructions in PE and other physical activities to help build working memory for instructions. Support him in class with sequencing his thoughts in a logical way to help him record. Give a short assignment so that Max can feel instant success in completing a task. Document the length of time he can focus on one task and structure the assignment so that it can be completed in that length of time. Gradually increase the time and use rewards (praise) and regular movement breaks to help re-focus. Writing frames and Mind Maps are useful strategies to help structure thinking. Give him a prompt sheet with a list of questions to answer when a task is open ended (e.g. creative writing). Ensure he knows what the 'end' product is. He can then work towards it in a systemic and structured way."
p69
asS'SENRepEP'
p70
S'07-12-2011'
p71
sS'SENRepSL'
p72
I0
sS'Physical'
p73
(lp74
S'He has reduced in-hand manipulation skills and reduced strength in the small muscles of the hand and forearm.'
p75
ag27
asssS'reports'
p76
(dp77
g5
(dp78
g38
(lp79
(dp80
S'Gender'
p81
S'M '
p82
sS'Notes'
p83
S'NB, LLK and Mrs Maddox discussion of NBs observations and assessment. '
p84
sS'RevDate'
p85
g27
sS'Teacher'
p86
g27
sS'DateTime'
p87
S'2011-10-20 17:19'
p88
sasg37
(lp89
(dp90
S'Term'
p91
S'Lent'
p92
sS'TargYear'
p93
S'2012'
p94
sS'Target'
p95
S'To complete the first instruction given by the teacher before asking for help.  '
p96
sg81
S'M '
p97
sS'TargDate'
p98
g27
sg87
S'2012-03-11 12:03'
p99
sS'TargAch'
p100
S'1'
p101
sS'Strategies'
p102
g27
sg86
g27
sa(dp103
g91
S'Mich'
p104
sg93
S'2012'
p105
sg95
S'Use mind maps to help plan his thoughs before commencing written tasks.'
p106
sg81
S'M '
p107
sg98
g27
sg87
S'2012-07-19 12:04'
p108
sg100
g101
sg102
g27
sg86
g27
sa(dp109
g91
S'Lent'
p110
sg93
S'2013'
p111
sg95
S'1. To put his hand up at least once a lesson to share an answer.                                                                                  '
p112
sg81
S'M '
p113
sg98
g27
sg87
S'2012-07-19 12:04'
p114
sg100
g101
sg102
g27
sg86
g27
sa(dp115
g91
S'Lent'
p116
sg93
S'2013'
p117
sg95
S' 2. Sound out words accurately.                                                                              '
p118
sg81
S'M '
p119
sg98
g27
sg87
S'2012-07-19 12:04'
p120
sg100
g101
sg102
g27
sg86
g27
sa(dp121
g91
S'Summer'
p122
sg93
S'2013'
p123
sg95
S'To read daily to an adult for 10 minutes.                                                                 '
p124
sg81
S'M '
p125
sg98
g27
sg87
S'2012-07-19 12:04'
p126
sg100
g101
sg102
g27
sg86
g27
sa(dp127
g91
S'Summer'
p128
sg93
S'2013'
p129
sg95
S'To word process long pieces of written work.                                                                        '
p130
sg81
S'M '
p131
sg98
g27
sg87
S'2012-07-19 12:04'
p132
sg100
g101
sg102
g27
sg86
g27
sa(dp133
g91
S'Mich'
p134
sg93
S'2013'
p135
sg95
S'To complete a task in a specified time using appropriate egg timer provided by the teacher.                                                                  '
p136
sg81
S'M '
p137
sg98
g27
sg87
S'2013-07-16 09:50'
p138
sg100
S'0'
p139
sg102
g27
sg86
g27
sasg36
(lp140
(dp141
g91
S'Lent'
p142
sg81
S'M '
p143
sg87
S'2012-03-04 12:02'
p144
sS'SenProv'
p145
S'1:1 support lessons with Sarah McKinlay.'
p146
sg52
S'2012'
p147
sS'Freq'
p148
S'weekly(x1)'
p149
sg86
g27
sS'Staff'
p150
g27
sa(dp151
g91
S'Summer'
p152
sg81
S'M '
p153
sg87
S'2012-03-04 12:02'
p154
sg145
S'1:1 support lessons with Sarah McKinlay.'
p155
sg52
S'2012'
p156
sg148
S'weekly(x1)'
p157
sg86
g27
sg150
g27
sa(dp158
g91
S'Michaelmas'
p159
sg81
S'M '
p160
sg87
S'2012-03-04 12:02'
p161
sg145
S'1:1 support lessons with GSK'
p162
sg52
S'2012'
p163
sg148
S'weekly(x1)'
p164
sg86
g27
sg150
g27
sa(dp165
g91
S'Lent'
p166
sg81
S'M '
p167
sg87
S'2012-03-04 12:02'
p168
sg145
S'1:1 support lessons with GSK'
p169
sg52
S'2013'
p170
sg148
S'weekly(x1)'
p171
sg86
g27
sg150
g27
sa(dp172
g91
S'Summer'
p173
sg81
S'M '
p174
sg87
S'2012-03-04 12:02'
p175
sg145
S'1:1 support lessons with GSK'
p176
sg52
S'2013'
p177
sg148
S'weekly(x1)'
p178
sg86
g27
sg150
g27
sassssS'Ned_Haden'
p179
(dp180
g3
(dp181
g5
(dp182
g7
I0
sg8
(lp183
S'India has difficulties with mathematical reasoning however numerical operations are a strength.'
p184
aS'See above.'
p185
asg12
S'Sen provision map for Ned Haden'
p186
sg14
g15
sg16
(lp187
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Ned Haden'
p188
sg24
I0
sg25
(lp189
S'Please utilise the strategies below to support Ned in all settings.'
p190
aS" Monitor seating arrangements on the floor and at desk to minimize distractions.  Divide long-term work tasks into segments and assign a completion goal for each segment. Where possible teachers should utilise visuals such as charts, pictures, colour coding to help support working memory and to stimulate his attention. In opening the lesson, tell Ned what he will be learning and what your expectations are. Tell and list exactly what materials they'll need for the task. To further stimulate concentration and attention, vary the pace of the lesson and include different kinds of activities. Incorporate brain gym activities. Have an unobtrusive cue set up with the Ned, such as a touch on the shoulder or placing a sticky note on his desk, to remind him to stay on task."
p191
asg29
(lp192
g27
ag31
asg32
S'SEN,Provision map,Ned Haden'
p193
sg34
(lp194
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp195
g41
S'03/03/2010'
p196
sg43
S'Haden'
p197
sg45
S'Action'
p198
sg47
S'2003-10-21'
p199
sg49
S'Dr Anis Mustafa 3/3/10. \r<br>In class observation by NB 29/6\r<br>Annie Mitchell EP Report 9/3/2011\r<br>Classroom observation completed by NB.'
p200
sg51
g27
sg52
S'4'
p201
sg54
S'09-03-2011'
p202
sg56
S'Ned finds it much harder to concentrate and focus during language based tasks especially tasks that require him to scann for information. He has strenghts in non verbal reasoning and good working memory. Ned has specific difficulties with visual scanning and visual discrimination which will affect his ability to read and record writing accuratrly. He has a slight weakness with auditory processing. He has a generally slow and meticulous working style. VCI 100 PRI 112 WMI 110 PSI 112 FSIQ 111'
p203
sg58
g27
sg59
S'Ned'
p204
sg61
g27
sg62
S'4CS'
p205
ssg64
I1
sg65
I0
sg66
(lp206
S'Ned finds it much harder to concentrate and focus during language based tasks especially tasks that require him to scann for information. He has strenghts in non verbal reasoning and good working memory. Ned has specific difficulties with visual scanning and visual discrimination which will affect his ability to read and record writing accuratrly.'
p207
aS'Give instructions one at a time and repeat as necessary having Ned complete each task as required before checking in with the teacher. To support Ned with focus and concentration chunk activities into workable sections for Ned and encourage him to check in with the teacher after completing.'
p208
asg70
S'09-03-2011'
p209
sg72
I0
sg73
(lp210
S'Freddie also has some auditory filtering difficulties and mild bilateral conductive hearing loss that is worse in the left ear.'
p211
aS'Freddie is trialling a hearing device until a more accurate assessment is completed.'
p212
asssg76
(dp213
g5
(dp214
g38
(lp215
(dp216
g81
S'M '
p217
sg83
S'KM and Mrs Haden. Concerns about learning, possible dyselxia and ADD. '
p218
sg85
S'07-10-2010'
p219
sg86
g27
sg87
S'2010-10-07 11:51'
p220
sa(dp221
g81
S'M '
p222
sg83
S'Meeting between NB and Mrs Haden to review EP Report.'
p223
sg85
S'30-03-2011'
p224
sg86
g27
sg87
S'2010-12-13 12:32'
p225
sa(dp226
g81
S'M '
p227
sg83
S'KM and Mrs Haden. Parent reporting concerns about dyslexia. '
p228
sg85
S'20-01-2011'
p229
sg86
g27
sg87
S'2011-01-20 11:51'
p230
sa(dp231
g81
S'M '
p232
sg83
S'Review meeting held between Mrs Haden, KM, NB and HSM to discuss Neds progress. Mrs Haden indicated that a further EP assessment was required. '
p233
sg85
S'27-01-2011'
p234
sg86
g27
sg87
S'2011-01-27 11:51'
p235
sa(dp236
g81
S'M '
p237
sg83
S'Meeting between NB and Mrs Haden to review EP Report. '
p238
sg85
S'30-03-2011'
p239
sg86
g27
sg87
S'2011-03-30 12:32'
p240
sa(dp241
g81
S'M '
p242
sg83
S'NB has called Neds eye doctor on 3 occassions for possible suggestions however has not received any returned calls. '
p243
sg85
g27
sg86
g27
sg87
S'2012-05-15 08:58'
p244
sasg37
(lp245
(dp246
g91
S'Lent'
p247
sg93
S'2011'
p248
sg95
S'To listen to the instruction, then repeat it back to the teacher before following the instruction.'
p249
sg81
S'M '
p250
sg98
g27
sg87
S'2010-12-13 12:32'
p251
sg100
g101
sg102
g27
sg86
g27
sa(dp252
g91
S'Lent'
p253
sg93
S'2011'
p254
sg95
S' To recall and understand the learning objective of the lesson'
p255
sg81
S'M '
p256
sg98
g27
sg87
S'2010-12-13 12:32'
p257
sg100
g101
sg102
g27
sg86
g27
sa(dp258
g91
S'Mich'
p259
sg93
S'2012'
p260
sg95
S'To remember books and equipment for lessons.To commence tasks within 2 minutes after all instructions and support have been given.'
p261
sg81
S'M '
p262
sg98
g27
sg87
S'2011-09-20 16:31'
p263
sg100
g101
sg102
S'Writing instructions down on whiteboard.Encouraging Ned to repeat instructions back.Moving Ned away from distractions.Giving Ned fixed timeframes within which to finish his work.'
p264
sg86
S'CDY'
p265
sa(dp266
g91
S'Lent'
p267
sg93
S'2012'
p268
sg95
S'To increase the accuracy of spelling word endings.'
p269
sg81
S'M '
p270
sg98
g27
sg87
S'2012-01-27 08:58'
p271
sg100
g139
sg102
g27
sg86
S'CDY'
p272
sa(dp273
g91
S'Lent'
p274
sg93
S'2012'
p275
sg95
S'To focus in lessons quickly (within 2 minutes) and complete tasks as directed by the teacher.'
p276
sg81
S'M '
p277
sg98
g27
sg87
S'2012-01-27 08:58'
p278
sg100
g101
sg102
g27
sg86
g27
sa(dp279
g91
S'Mich'
p280
sg93
S'2012'
p281
sg95
S'1. To improve accuracy of spelling word endings.'
p282
sg81
S'M '
p283
sg98
g27
sg87
S'2012-07-19 14:00'
p284
sg100
g101
sg102
g27
sg86
g27
sa(dp285
g91
S'Mich'
p286
sg93
S'2012'
p287
sg95
S'2. To improve layout of written work.'
p288
sg81
S'M '
p289
sg98
g27
sg87
S'2012-07-19 14:00'
p290
sg100
g101
sg102
g27
sg86
g27
sa(dp291
g91
S'Lent'
p292
sg93
S'2013'
p293
sg95
S' ENGLISH 1. To identify and edit at least 3 spelling mistakes from pieces of written work.                                                                                                                                              '
p294
sg81
S'M '
p295
sg98
g27
sg87
S'2012-07-19 14:00'
p296
sg100
g101
sg102
g27
sg86
g27
sa(dp297
g91
S'Lent'
p298
sg93
S'2013'
p299
sg95
S'2. Ensure ideas are written in order and are well spaced.                                                                                                                           '
p300
sg81
S'M '
p301
sg98
g27
sg87
S'2012-07-19 14:00'
p302
sg100
g101
sg102
g27
sg86
g27
sa(dp303
g91
S'Mich'
p304
sg93
S'2013'
p305
sg95
S'1. Ensure all sentences are punctuated with capital letters and full stops.                                                     '
p306
sg81
S'M '
p307
sg98
g27
sg87
S'2013-07-16 10:05'
p308
sg100
g139
sg102
g27
sg86
g27
sa(dp309
g91
S'Mich'
p310
sg93
S'2013'
p311
sg95
S'2.  Ensure story ideas are well paced and plausible.'
p312
sg81
S'M '
p313
sg98
g27
sg87
S'2013-07-16 10:05'
p314
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp315
(dp316
g91
S'Michaelmas'
p317
sg81
S'M '
p318
sg87
S'2010-12-13 12:35'
p319
sg145
S'Small group spelling support focusing on high frequency words.'
p320
sg52
S'2010'
p321
sg148
S'weekly(x1)'
p322
sg86
g27
sg150
S'KM'
p323
sa(dp324
g91
S'Lent'
p325
sg81
S'M '
p326
sg87
S'2010-12-13 12:35'
p327
sg145
S'Small group spelling support focusing on high frequency words.'
p328
sg52
S'2011'
p329
sg148
S'weekly(x1)'
p330
sg86
g27
sg150
S'KM'
p331
sa(dp332
g91
S'Lent'
p333
sg81
S'M '
p334
sg87
S'2010-12-13 12:35'
p335
sg145
S'Handwriting support group.'
p336
sg52
S'2011'
p337
sg148
S'weekly(x1)'
p338
sg86
g27
sg150
S'KM'
p339
sa(dp340
g91
S'Summer'
p341
sg81
S'M '
p342
sg87
S'2010-12-13 12:35'
p343
sg145
S'Small group spelling support focusing on high frequency words.'
p344
sg52
S'2011'
p345
sg148
S'weekly(x1)'
p346
sg86
g27
sg150
S'KM'
p347
sa(dp348
g91
S'Summer'
p349
sg81
S'M '
p350
sg87
S'2010-12-13 12:35'
p351
sg145
S'Handwriting support group.'
p352
sg52
S'2011'
p353
sg148
S'weekly(x1)'
p354
sg86
g27
sg150
S'KM'
p355
sa(dp356
g91
S'Summer'
p357
sg81
S'M '
p358
sg87
S'2011-07-12 13:36'
p359
sg145
S'Classroom observation completed by NB. '
p360
sg52
S'2010'
p361
sg148
g27
sg86
g27
sg150
S'NB'
p362
sassssS'Samuel_Green'
p363
(dp364
g3
(dp365
g5
(dp366
g7
I0
sg8
(lp367
g27
aS'Sam will require support with organisation and sequencing especially in mathematics tasks.'
p368
asg12
S'Sen provision map for Samuel Green'
p369
sg14
g15
sg16
(lp370
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Samuel Green'
p371
sg24
I1
sg25
(lp372
g27
aS'Sam may appear to lack attention and concentration however this may be due to auditory filtering. Please check for understanding and repeat instructions as required.'
p373
asg29
(lp374
g27
ag31
asg32
S'SEN,Provision map,Samuel Green'
p375
sg34
(lp376
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp377
g41
S'08-06-2010'
p378
sg43
S'Green'
p379
sg45
S'Action'
p380
sg47
S'2003-01-28'
p381
sg49
S'Paediatric Physiotherapist assessment 14/7/10 by Laura Irwin.'
p382
sg51
S'Both'
p383
sg52
S'5'
p384
sg54
S'09-09-2011'
p385
sg56
S'Sam is hyper mobile and has low postural tone. He has poor motor coordination and slightly reduced muscle power.'
p386
sg58
g27
sg59
S'Samuel'
p387
sg61
g27
sg62
S'5CS'
p388
ssg64
I0
sg65
I0
sg66
(lp389
S" Sam's handwriting will appear jerky and disorganised."
p390
aS'Teachers should not write negative comments about the presentation of his work focusing rather on his understanding of the topic. Alternative assessment to written work should be undertaken where possible, for example, providing printed work sheets in Maths to allow Sam to master understanding.'
p391
asg70
S' '
p392
sg72
I0
sg73
(lp393
S'Sam is hyper mobile and has low postural tone. He has poor motor coordination and slightly reduced muscle power.'
p394
aS"Due to Sam's poor motor coordination and slightly reduced muscle power he may fatigue more quickly. The effect is similar to someone attempting such tasks in sand. Sam has been identified as having high potential which is not showing through in his performance at the current time due to fine motor and dexterity difficulties. Sam may kneel on a chair during writing activities in an attempt to support his posture however this should be corrected."
p395
asssg76
(dp396
g5
(dp397
g38
(lp398
(dp399
g81
S'M '
p400
sg83
S'NB, CMC Mr and Mrs Green. Review of OT report. '
p401
sg85
S'23-09-2010'
p402
sg86
g27
sg87
S'2010-09-23 13:28'
p403
sa(dp404
g81
S'M '
p405
sg83
S'CD Mr and Mrs Green. Meeting following up from parents evening. '
p406
sg85
S'15-03-2011'
p407
sg86
g27
sg87
S'2011-03-15 13:28'
p408
sa(dp409
g81
S'M '
p410
sg83
S'NB, CD Mr and Mrs Green. Meeting regarding Laptop use. '
p411
sg85
S'09-06-2011'
p412
sg86
g27
sg87
S'2011-06-09 13:28'
p413
sa(dp414
g81
S'M '
p415
sg83
S'NB, AP Mr and Mrs Green. Meeting regarding Laptop use.'
p416
sg85
S'09-06-2011'
p417
sg86
g27
sg87
S'2011-09-08 13:28'
p418
sa(dp419
g81
S'M '
p420
sg83
S'NB, Mr and Mrs Green. Review mtg regarding laptop use. '
p421
sg85
g27
sg86
g27
sg87
S'2012-04-26 13:28'
p422
sa(dp423
g81
S'M '
p424
sg83
S'NB and Mr Green re; Laptop use in year 5. '
p425
sg85
g27
sg86
g27
sg87
S'2012-07-19 13:52'
p426
sasg37
(lp427
(dp428
g91
S'Lent'
p429
sg93
S'2011'
p430
sg95
S'To structure written work in an organised fashion using accurate sentence structure and punctuation.'
p431
sg81
S'M '
p432
sg98
g27
sg87
S'2010-12-13 13:28'
p433
sg100
g101
sg102
g27
sg86
g27
sa(dp434
g91
S'Lent'
p435
sg93
S'2011'
p436
sg95
S'To make sure capital letters are in the correct place in independent writing.'
p437
sg81
S'M '
p438
sg98
g27
sg87
S'2010-12-13 13:28'
p439
sg100
g101
sg102
g27
sg86
g27
sa(dp440
g91
S'Mich'
p441
sg93
S'2011'
p442
sg95
S'To bring in laptop every day and to take it to all English lessons and bring it back to 4CE.'
p443
sg81
S'M '
p444
sg98
g27
sg87
S'2011-09-19 17:16'
p445
sg100
g101
sg102
S'Sam has a reminder on his bag.AP and RGA to remind Sam also.'
p446
sg86
S'AP'
p447
sa(dp448
g91
S'Lent'
p449
sg93
S'2012'
p450
sg95
S'Focus more on presentation of his written work when tasks require written answers.'
p451
sg81
S'M '
p452
sg98
g27
sg87
S'2012-01-25 16:41'
p453
sg100
g101
sg102
S'Regular encouragement and praise for neat work.'
p454
sg86
S'AP'
p455
sa(dp456
g91
S'Lent'
p457
sg93
S'2012'
p458
sg95
S'To ensure he has only the necessary equipment for each lesson on his desk.'
p459
sg81
S'M '
p460
sg98
g27
sg87
S'2012-07-19 13:52'
p461
sg100
g139
sg102
g27
sg86
g27
sa(dp462
g91
S'Mich'
p463
sg93
S'2012'
p464
sg95
S'To ensure he has only the necessary equipment for each lesson on his desk.'
p465
sg81
S'M '
p466
sg98
g27
sg87
S'2012-07-19 13:52'
p467
sg100
g101
sg102
g27
sg86
g27
sa(dp468
g91
S'Lent'
p469
sg93
S'2013'
p470
sg95
S'To attempt work individually before asking for help (after instructions have been given). 75% of the time.'
p471
sg81
S'M '
p472
sg98
g27
sg87
S'2012-07-19 13:52'
p473
sg100
g101
sg102
g27
sg86
g27
sa(dp474
g91
S'Summer'
p475
sg93
S'2013'
p476
sg95
S'To challenge myself with my learning and be willing to take a risk with harder work options. '
p477
sg81
S'M '
p478
sg98
g27
sg87
S'2012-07-19 13:52'
p479
sg100
g101
sg102
g27
sg86
g27
sa(dp480
g91
S'Mich'
p481
sg93
S'2013'
p482
sg95
S'1. To consistently display confidence in his own abilities and be willing to share his ideas with others in lessons.                                                      '
p483
sg81
S'M '
p484
sg98
g27
sg87
S'2013-07-16 10:33'
p485
sg100
g139
sg102
g27
sg86
g27
sa(dp486
g91
S'Mich'
p487
sg93
S'2013'
p488
sg95
S'2. To make sure I have all of the equipment I need for the lesson before I leave my classroom.                                                  '
p489
sg81
S'M '
p490
sg98
g27
sg87
S'2013-07-16 10:33'
p491
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp492
(dp493
g91
S'Michaelmas'
p494
sg81
S'M '
p495
sg87
S'2010-12-13 13:22'
p496
sg145
S'Handwriting support group'
p497
sg52
S'2010'
p498
sg148
S'weekly(x1)'
p499
sg86
g27
sg150
S'KM'
p500
sa(dp501
g91
S'Lent'
p502
sg81
S'M '
p503
sg87
S'2010-12-13 13:22'
p504
sg145
S'Handwriting support group'
p505
sg52
S'2011'
p506
sg148
S'weekly(x1)'
p507
sg86
g27
sg150
S'KM'
p508
sa(dp509
g91
S'Summer'
p510
sg81
S'M '
p511
sg87
S'2010-12-13 13:22'
p512
sg145
S'Handwriting support group'
p513
sg52
S'2011'
p514
sg148
S'weekly(x1)'
p515
sg86
g27
sg150
S'AST'
p516
sa(dp517
g91
S'Michaelmas'
p518
sg81
S'M '
p519
sg87
S'2010-12-13 13:22'
p520
sg145
S'Handwriting support group'
p521
sg52
S'2011'
p522
sg148
S'weekly(x1)'
p523
sg86
g27
sg150
S'NB'
p524
sa(dp525
g91
S'Lent'
p526
sg81
S'M '
p527
sg87
S'2012-02-06 13:22'
p528
sg145
S'Handwriting support group'
p529
sg52
S'2012'
p530
sg148
S'weekly(x1)'
p531
sg86
g27
sg150
S'NB'
p532
sa(dp533
g91
S'Summer'
p534
sg81
S'M '
p535
sg87
S'2012-02-06 13:22'
p536
sg145
S'Handwriting support group'
p537
sg52
S'2012'
p538
sg148
S'weekly(x1)'
p539
sg86
g27
sg150
S'NB'
p540
sa(dp541
g91
S'Summer'
p542
sg81
S'M '
p543
sg87
S'2012-07-17 09:35'
p544
sg145
S'1:1 session working on organisation. '
p545
sg52
S'2012'
p546
sg148
g27
sg86
g27
sg150
S'NB'
p547
sassssS'Lily_Griffin'
p548
(dp549
g3
(dp550
g5
(dp551
g7
I0
sg8
(lp552
S'Her mental maths and knowledge of number bonds is weak.'
p553
aS'Please see above for strategies.'
p554
asg12
S'Sen provision map for Lily Griffin'
p555
sg14
g15
sg16
(lp556
S'Bertie received speech and language therapy most specifically for work for his lisp.'
p557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Lily Griffin'
p558
sg24
I0
sg25
(lp559
g27
aS'Utilise multi-sensory approaches that use a combination of auditory information as well as visual and kinaesthetic information. Additional time to process and respond to a question would be helpful. She will benefit from being praised for the work that she does well in order to keep her confidence high.'
p560
asg29
(lp561
S'Please stop William when you can see he is becoming upset or over excited. Speak calmly to him and remove him from the peer group to have this discussion if possible. Specifically and clearly explain to William what he has done and explain why it is inappropriate. Be very specific in discussion with him and avoid using any vague language or instructions. Keep instructions to a minimum and have him complete small steps to complete the whole task. Praise William where possible and again very clearly explain why you are happy with him and his behaviour.'
p562
ag27
asg32
S'SEN,Provision map,Lily Griffin'
p563
sg34
(lp564
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp565
g41
S'06/05/2010'
p566
sg43
S'Griffin'
p567
sg45
S'Action'
p568
sg47
S'2003-12-20'
p569
sg49
S'DST - J Nov 2010 KAM.\r<br>EP Report Annie Mitchell 17/3/2011\r<br>Dr K S Sirimanna Consultant Audiological Physician 2/6/2011                                                  \r<br>WHIS - Salendar Tay May 2011'
p570
sg51
S'Both'
p571
sg52
g201
sg54
S'17-03-2011'
p572
sg56
S'Lily has mild to moderate conductive hearing loss that is slightly worse in the right ear. This hearing loss will be affecting her ability to hear especially in poor acoustic conditions or if the speech signal is degraded. She wears hearing aids in both ears. Lily displays significant difficulties with visual spatial reasoning as well as short term visual memory, working memory (94) auditory sequencing and auditory processing. She has a very small sight vocabulary and her reaidng fluency is poor due to phonological difficulties most specifically decoding skills and poor phoneme/grapheme correspondence. Her mental maths and knowledge of number bonds is weak.  VCI 98 PRI 90 WMI 94 PSI 103 FSIQ 94'
p573
sg58
g27
sg59
S'Lily'
p574
sg61
g27
sg62
S'4CN'
p575
ssg64
I1
sg65
I1
sg66
(lp576
S'Lily displays significant difficulties with visual spatial reasoning as well as short term visual memory, working memory (94) auditory sequencing and auditory processing. She has a very small sight vocabulary and her reaidng fluency is poor due to phonological difficulties most specifically decoding skills and poor phoneme/grapheme correspondence.'
p577
aS"Lily shouldn't be expected to copy copious amounts from the board. Scribe sections if appropriate. Check understanding of vocabulary when introducing new concepts or topics. Break tasks down into their smallest components and encourage completion of each step at a time, check off each step to ensure understanding and accuracy. Support Lily with Reading Comprehension work by encouraging her to highlight key words and make notes in the margin as appropriate. Encourage Lily to mind map her ideas for longer writing pieces to support her memory. Support planning of work, where appropriate, with writing frames. Have a list of high frequency words she can use to support her spelling."
p578
asg70
S'17-03-2011'
p579
sg72
I0
sg73
(lp580
S'Lily has mild to moderate conductive hearing loss that is slightly worse in the right ear. This hearing loss will be affecting her ability to hear especially in poor acoustic conditions or if the speech signal is degraded.'
p581
aS'Please try and sit Lily as close to the front of the group or class as possible, with her left ear closest to the speaker. Lily will lip watch to clarify what she hears so face her when speakingh. Keep verbal instructions as brief as possible and write them on the board where feasible. Reduce auditory distractions where possible such as keeping the classroom door closed. Avoid talking whilst writing on the board or from a distance and do not speak to her from across the classroom. Check understanding by asking her to repeat information during task. Cue Lily in by using her name before specific instructions and repeat announcements from assemblies etc.'
p582
asssg76
(dp583
g5
(dp584
g38
(lp585
(dp586
g81
S'F '
p587
sg83
S'Review of support provided in groups by KAM. Present KAM, LH and Miss Cole.'
p588
sg85
S'15-11-2011'
p589
sg86
g27
sg87
S'2011-02-17 11:41'
p590
sa(dp591
g81
S'F '
p592
sg83
S'Miss Cole met with HSM to discuss the possibility of an EP report. Lilly is booked to have an assessment with Annie Mitchell on the 17th March 2011. '
p593
sg85
S'24-01-2011'
p594
sg86
g27
sg87
S'2011-02-17 11:43'
p595
sa(dp596
g81
S'F '
p597
sg83
S'NB. MSM, LH, Mr and Mrs Griffin. Review of EP Report completed by Annie Mitchell and suggetsed interventions. '
p598
sg85
S'16-05-2011'
p599
sg86
g27
sg87
S'2011-05-16 11:43'
p600
sa(dp601
g81
S'F '
p602
sg83
S'NB and Miss Cole via Email. Lily will be fitted with Hearning aids in both ears. '
p603
sg85
g27
sg86
g27
sg87
S'2011-10-10 12:30'
p604
sa(dp605
g81
S'F '
p606
sg83
S'Review Meeting with NB and Katie Cole. Discussion of support options and current progress. '
p607
sg85
S'29-11-2011'
p608
sg86
g27
sg87
S'2011-11-29 14:08'
p609
sa(dp610
g81
S'F '
p611
sg83
S'NB and Katie Cole. Discussion of Lilys difficulties and how we are catering for her in school. '
p612
sg85
g27
sg86
g27
sg87
S'2012-11-08 11:25'
p613
sasg37
(lp614
(dp615
g91
S'Lent'
p616
sg93
S'2011'
p617
sg95
S'Listen very carefully to instructions (sit near the teacher on the carpet) and then repeat back what is said.'
p618
sg81
S'F '
p619
sg98
g27
sg87
S'2010-12-13 12:30'
p620
sg100
g101
sg102
g27
sg86
g27
sa(dp621
g91
S'Mich'
p622
sg93
S'2011'
p623
sg95
S'To recognise common vowel phonemes when reading and spelling.'
p624
sg81
S'F '
p625
sg98
g27
sg87
S'2011-09-21 14:51'
p626
sg100
g101
sg102
S'Extra 1 to 1 support 1 x 20 minutes a week for handwriting. Extra 1 to 1 support 1 x 35 minutes a for reading and spelling. Lily to use a task planner during lessons to structure her work.'
p627
sg86
S'JHA'
p628
sa(dp629
g91
S'Mich'
p630
sg93
S'2011'
p631
sg95
S'When writing to get her letters close together and use a gap between her words.'
p632
sg81
S'F '
p633
sg98
g27
sg87
S'2011-09-21 14:51'
p634
sg100
g101
sg102
S'Extra 1 to 1 support 1 x 20 minutes a week for handwriting. Extra 1 to 1 support 1 x 35 minutes a for reading and spelling. Lily to use a task planner during lessons to structure her work.'
p635
sg86
g27
sa(dp636
g91
S'Mich'
p637
sg93
S'2011'
p638
sg95
S'To complete a given task within her ability.'
p639
sg81
S'F '
p640
sg98
g27
sg87
S'2011-09-21 14:51'
p641
sg100
g101
sg102
S'Extra 1 to 1 support 1 x 20 minutes a week for handwriting. Extra 1 to 1 support 1 x 35 minutes a for reading and spelling. Lily to use a task planner during lessons to structure her work.'
p642
sg86
g27
sa(dp643
g91
S'Lent'
p644
sg93
S'2012'
p645
sg95
S'To be able to work independently when answering comprehension questions.'
p646
sg81
S'F '
p647
sg98
g27
sg87
S'2012-02-06 11:18'
p648
sg100
g139
sg102
S'Lily to have a breakdown of the steps she needs to work through during differientiated comprehension lessons. Lily to have clearly highlighted words to use when forming her answers and to use a connectives word bank to help her.'
p649
sg86
S'JHA'
p650
sa(dp651
g91
S'Summer'
p652
sg93
S'2012'
p653
sg95
S'To be able to answer comprehension question independently. To sound out words effectively in order to spell them more accurately. To read for understanding. To have a go at starting a task before asking for help.'
p654
sg81
S'F '
p655
sg98
g27
sg87
S'2012-04-30 12:27'
p656
sg100
g101
sg102
S'Lily to work on reading a piece of text for meaning and then using words from a question to form an answer written in full sentences. lily to use high lighters to help her. Lily to use trackers when reading in order to help her souns them out. lily to have access to magnetic letters during a lesson to help with spelling. Lily to have extra spellings each week focusing on words spelt incorrectly in class work.'
p657
sg86
S'JHA'
p658
sa(dp659
g91
S'Mich'
p660
sg93
S'2012'
p661
sg95
S'1.To be able to sound out each part of a word to spell it accurately and remember phoneme patterns.'
p662
sg81
S'F '
p663
sg98
g27
sg87
S'2012-07-19 13:55'
p664
sg100
g101
sg102
g27
sg86
g27
sa(dp665
g91
S'Mich'
p666
sg93
S'2012'
p667
sg95
S'2. To read for understanding and verbally describe what she has read about.'
p668
sg81
S'F '
p669
sg98
g27
sg87
S'2012-07-19 13:55'
p670
sg100
g101
sg102
g27
sg86
g27
sa(dp671
g91
S'Lent'
p672
sg93
S'2013'
p673
sg95
S'Maths: To be confident in my abilities through attempting questions first time without needing to check with teachers if my answer is correct. (75% of the time).                                                                    '
p674
sg81
S'F '
p675
sg98
g27
sg87
S'2012-07-19 13:55'
p676
sg100
g101
sg102
g27
sg86
g27
sa(dp677
g91
S'Lent'
p678
sg93
S'2013'
p679
sg95
S'ENGLISH 1. To identify and edit at least 3 spelling mistakes from pieces of written work.                                                                                                                                              '
p680
sg81
S'F '
p681
sg98
g27
sg87
S'2013-07-16 11:38'
p682
sg100
g139
sg102
g27
sg86
g27
sa(dp683
g91
S'Lent'
p684
sg93
S'2013'
p685
sg95
S'2. Ensure ideas are written in order and are well spaced.'
p686
sg81
S'F '
p687
sg98
g27
sg87
S'2013-07-16 11:38'
p688
sg100
g139
sg102
g27
sg86
g27
sa(dp689
g91
S'Summer'
p690
sg93
S'2013'
p691
sg95
S'ENGLISH 1. To identify and edit at least 3 spelling mistakes from pieces of written work.                                                                                                                                              '
p692
sg81
S'F '
p693
sg98
g27
sg87
S'2013-07-16 11:38'
p694
sg100
g139
sg102
g27
sg86
g27
sa(dp695
g91
S'Summer'
p696
sg93
S'2013'
p697
sg95
S'2. Ensure ideas are written in order and are well spaced.'
p698
sg81
S'F '
p699
sg98
g27
sg87
S'2013-07-16 11:38'
p700
sg100
g101
sg102
g27
sg86
g27
sa(dp701
g91
S'Summer'
p702
sg93
S'2013'
p703
sg95
S'Maths: to check through answers carefully and check method. (after each task).'
p704
sg81
S'F '
p705
sg98
g27
sg87
S'2013-07-16 11:38'
p706
sg100
g101
sg102
g27
sg86
g27
sa(dp707
g91
S'Mich'
p708
sg93
S'2013'
p709
sg95
S'English: 1. To remember to use capital letters and full stops in my sentences.                                                        '
p710
sg81
S'F '
p711
sg98
g27
sg87
S'2013-07-16 11:40'
p712
sg100
g139
sg102
g27
sg86
g27
sa(dp713
g91
S'Mich'
p714
sg93
S'2013'
p715
sg95
S'2. Identify and edit 3 spellings mistakes from writing.                                                                                               '
p716
sg81
S'F '
p717
sg98
g27
sg87
S'2013-07-16 11:40'
p718
sg100
g139
sg102
g27
sg86
g27
sa(dp719
g91
S'Mich'
p720
sg93
S'2013'
p721
sg95
S'3. Ensure enough detail in comprehension answers.                                                                                    '
p722
sg81
S'F '
p723
sg98
g27
sg87
S'2013-07-16 11:40'
p724
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp725
(dp726
g91
S'Michaelmas'
p727
sg81
S'F '
p728
sg87
S'2009-05-13 16:42'
p729
sg145
S'Small group spelling support focusing on high frequency words.'
p730
sg52
S'2010'
p731
sg148
S'weekly(x1)'
p732
sg86
g27
sg150
S'KM'
p733
sa(dp734
g91
S'Michaelmas'
p735
sg81
S'F '
p736
sg87
S'2009-05-13 16:42'
p737
sg145
S'Small group maths support focusing on number.'
p738
sg52
S'2010'
p739
sg148
S'weekly(x1)'
p740
sg86
g27
sg150
S'KM'
p741
sa(dp742
g91
S'Lent'
p743
sg81
S'F '
p744
sg87
S'2009-05-13 16:42'
p745
sg145
S'Small group spelling support focusing on high frequency words.'
p746
sg52
S'2011'
p747
sg148
S'weekly(x1)'
p748
sg86
g27
sg150
S'KM'
p749
sa(dp750
g91
S'Lent'
p751
sg81
S'F '
p752
sg87
S'2009-05-13 16:42'
p753
sg145
S'Small group maths support focusing on number.'
p754
sg52
S'2011'
p755
sg148
S'weekly(x1)'
p756
sg86
g27
sg150
S'KM'
p757
sa(dp758
g91
S'Lent'
p759
sg81
S'F '
p760
sg87
S'2009-05-13 16:42'
p761
sg145
S'Handwriting support group'
p762
sg52
S'2011'
p763
sg148
S'weekly(x1)'
p764
sg86
g27
sg150
S'KM'
p765
sa(dp766
g91
S'Summer'
p767
sg81
S'F '
p768
sg87
S'2009-05-13 16:42'
p769
sg145
S'Small group spelling support focusing on high frequency words.'
p770
sg52
S'2011'
p771
sg148
S'weekly(x1)'
p772
sg86
g27
sg150
S'AST'
p773
sa(dp774
g91
S'Summer'
p775
sg81
S'F '
p776
sg87
S'2009-05-13 16:42'
p777
sg145
S'Small group maths support focusing on number.'
p778
sg52
S'2011'
p779
sg148
S'weekly(x1)'
p780
sg86
g27
sg150
S'AST'
p781
sa(dp782
g91
S'Summer'
p783
sg81
S'F '
p784
sg87
S'2009-05-13 16:42'
p785
sg145
S'Handwriting support group'
p786
sg52
S'2011'
p787
sg148
S'weekly(x1)'
p788
sg86
g27
sg150
S'AST'
p789
sa(dp790
g91
S'Michaelmas'
p791
sg81
S'F '
p792
sg87
S'2009-05-13 16:42'
p793
sg145
S'Spelling support group'
p794
sg52
S'2011'
p795
sg148
S'weekly(x1)'
p796
sg86
g27
sg150
S'NB'
p797
sa(dp798
g91
S'Summer'
p799
sg81
S'F '
p800
sg87
S'2011-07-12 09:48'
p801
sg145
S'Hearing device borrowed from Wandsworth Hearing Impaired Services. '
p802
sg52
S'2011'
p803
sg148
S'Daily'
p804
sg86
g27
sg150
g27
sa(dp805
g91
S'Summer'
p806
sg81
S'F '
p807
sg87
S'2011-07-12 16:42'
p808
sg145
S'1:1 lesson with Isabel Clarke. '
p809
sg52
S'2011'
p810
sg148
S'weekly(x1)'
p811
sg86
g27
sg150
g27
sa(dp812
g91
S'Michaelmas'
p813
sg81
S'F '
p814
sg87
S'2011-11-29 16:42'
p815
sg145
S'1:1 Literacy Support George Scott Kerr'
p816
sg52
S'2011'
p817
sg148
S'Monthly(x1)'
p818
sg86
g27
sg150
g27
sa(dp819
g91
S'Michaelmas'
p820
sg81
S'F '
p821
sg87
S'2011-11-29 16:42'
p822
sg145
S'1:1 Literacy Support Eleanor Barker'
p823
sg52
S'2011'
p824
sg148
S'Monthly(x1)'
p825
sg86
g27
sg150
g27
sa(dp826
g91
S'Lent'
p827
sg81
S'F '
p828
sg87
S'2011-11-29 16:42'
p829
sg145
S'1:1 Literacy Support Eleanor Barker'
p830
sg52
S'2012'
p831
sg148
S'Monthly(x1)'
p832
sg86
g27
sg150
g27
sa(dp833
g91
S'Lent'
p834
sg81
S'F '
p835
sg87
S'2011-11-29 16:42'
p836
sg145
S'1:1 Literacy Support George Scott Kerr'
p837
sg52
S'2012'
p838
sg148
S'Monthly(x1)'
p839
sg86
g27
sg150
g27
sa(dp840
g91
S'Lent'
p841
sg81
S'F '
p842
sg87
S'2012-02-06 16:42'
p843
sg145
S'Spelling support group'
p844
sg52
S'2012'
p845
sg148
S'weekly(x1)'
p846
sg86
g27
sg150
S'NB'
p847
sa(dp848
g91
S'Summer'
p849
sg81
S'F '
p850
sg87
S'2012-05-16 16:42'
p851
sg145
S'1:1 Literacy Support Eleanor Barker'
p852
sg52
S'2012'
p853
sg148
S'Monthly(x1)'
p854
sg86
g27
sg150
g27
sa(dp855
g91
S'Summer'
p856
sg81
S'F '
p857
sg87
S'2012-05-16 16:42'
p858
sg145
S'1:1 Literacy Support Sarah McKinlay.'
p859
sg52
S'2012'
p860
sg148
S'Monthly(x1)'
p861
sg86
g27
sg150
g27
sa(dp862
g91
S'Summer'
p863
sg81
S'F '
p864
sg87
S'2012-06-06 16:42'
p865
sg145
S'Spelling support group'
p866
sg52
S'2012'
p867
sg148
S'weekly(x1)'
p868
sg86
g27
sg150
S'NB'
p869
sa(dp870
g91
S'Michaelmas'
p871
sg81
S'F '
p872
sg87
S'2013-07-08 16:04'
p873
sg145
S'In class literacy support spelling and reading comprehension'
p874
sg52
S'2012'
p875
sg148
S'weekly(x2)'
p876
sg86
g27
sg150
S'NB'
p877
sa(dp878
g91
S'Lent'
p879
sg81
S'F '
p880
sg87
S'2013-07-08 16:04'
p881
sg145
S'In class literacy support spelling and reading comprehension'
p882
sg52
S'2013'
p883
sg148
S'weekly(x2)'
p884
sg86
g27
sg150
S'NB'
p885
sa(dp886
g91
S'Summer'
p887
sg81
S'F '
p888
sg87
S'2013-07-08 16:04'
p889
sg145
S'In class literacy support spelling and reading comprehension'
p890
sg52
S'2013'
p891
sg148
S'weekly(x2)'
p892
sg86
g27
sg150
S'NB'
p893
sa(dp894
g91
S'Michaelmas'
p895
sg81
S'F '
p896
sg87
S'2013-07-08 16:09'
p897
sg145
S'1:1 Literacy support with EB and GSK'
p898
sg52
S'2012'
p899
sg148
S'weekly(x2)'
p900
sg86
g27
sg150
g27
sa(dp901
g91
S'Lent'
p902
sg81
S'F '
p903
sg87
S'2013-07-08 16:09'
p904
sg145
S'1:1 Literacy support with EB and GSK'
p905
sg52
S'2013'
p906
sg148
S'weekly(x2)'
p907
sg86
g27
sg150
g27
sa(dp908
g91
S'Summer'
p909
sg81
S'F '
p910
sg87
S'2013-07-08 16:09'
p911
sg145
S'1:1 Literacy support with EB and GSK'
p912
sg52
S'2013'
p913
sg148
S'weekly(x2)'
p914
sg86
g27
sg150
g27
sassssS'Charlie_Edwards'
p915
(dp916
g3
(dp917
g5
(dp918
g7
I0
sg8
(lp919
g27
ag368
asg12
S'Sen provision map for Charlie Edwards'
p920
sg14
g15
sg16
(lp921
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Charlie Edwards'
p922
sg24
I0
sg25
(lp923
g27
aS'Charlie would benefit from sitting near the teacher, this will assist with maintaining concentration and allowing the teacher to check in with him. Presented information should be chunked into workable amounts and frequent checking to make sure Charlie has understood what is expected of him in tasks before moving onto the next part. Encourage Charlie to think about the question before answering. Provide him with take up time when information is presented and if appropriate additional time on tasks would be benificial. Where possible provide templates and structured support to commence tasks and organise his thoughts. It would be helpful to ensure that the information load through language is not too great. Repetition is helpful particularly where vocabulary may be distractingly complex.  Visual support is helpful.  He needs an approach that will give him as many cues as possible, for example, handouts in class, visual cues, and clear instructions.'
p924
asg29
(lp925
g27
ag31
asg32
S'SEN,Provision map,Charlie Edwards'
p926
sg34
(lp927
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp928
g41
S'01/12/2009'
p929
sg43
S'Edwards'
p930
sg45
S'Action'
p931
sg47
S'2000-03-22'
p932
sg49
S'EP Report Nina Elliott\r<br>NEALE AS 10.09.09'
p933
sg51
S'Both'
p934
sg52
S'8'
p935
sg54
g27
sg56
S"There is a significant difference between Charlie's verbal and non-verbal reasoning ability, with his verbal abilities being much stronger.  Charlie's reading accuracy and spelling are in the High Average range; however his vocabulary is weak and he has difficulty learning from and generalising from previously taught knowledge.  His listening comprehension skills are weak.  Charlie's concentration is poor, he has some difficulties with his working memory (88) and his speed of processing (88) visual information is slow.  Charlie's visual-verbal memory is very weak. VCI 96 PRI 77 WMI 88 PSI 88"
p936
sg58
g27
sg59
S'Charlie'
p937
sg61
S'25%'
p938
sg62
S'8CN'
p939
ssg64
I1
sg65
I0
sg66
(lp940
g27
aS'Charlie will need support to develop his comprehension and thinking skills and to help him with his understanding of what is expected of him, and to retain this information.'
p941
asg70
S'21-12-2009'
p942
sg72
I0
sg73
(lp943
g394
ag395
asssg76
(dp944
g5
(dp945
g38
(lp946
(dp947
g81
S'M '
p948
sg83
S'Telephone conversation between Mrs Edwards and NB. NB discussed the need to update EP report. TBC following next set of exams in Summer term. '
p949
sg85
g27
sg86
g27
sg87
S'2012-01-11 15:44'
p950
sa(dp951
g81
S'M '
p952
sg83
S'Telephone conversation . Phone message left with Mrs Edwards regarding updating EP report and what the family had decided. '
p953
sg85
g27
sg86
g27
sg87
S'2012-07-12 15:44'
p954
sa(dp955
g81
S'M '
p956
sg83
S'Telephone conversation between NB and Mrs Edwards. Mrs Edwards will not be having Charlie re assessed as she does not think it is necessary. Mrs Edwards also made comment that Charlie will only not do well if he does not like his teachers or they are not good teachers.'
p957
sg85
g27
sg86
g27
sg87
S'2012-07-17 15:44'
p958
sasg37
(lp959
(dp960
g91
S'Lent'
p961
sg93
S'2011'
p962
sg95
S'To develop independence as a learner and focus on the task at hand. '
p963
sg81
S'M '
p964
sg98
g27
sg87
S'2010-12-14 15:42'
p965
sg100
g139
sg102
g27
sg86
g27
sa(dp966
g91
S'Lent'
p967
sg93
S'2012'
p968
sg95
S'To be more proactive in asking for support if required. '
p969
sg81
S'M '
p970
sg98
g27
sg87
S'2012-03-04 09:06'
p971
sg100
g101
sg102
g27
sg86
g27
sa(dp972
g91
S'Mich'
p973
sg93
S'2012'
p974
sg95
S'Utilise mind maps to help orgainse his thoughts relating to written work.'
p975
sg81
S'M '
p976
sg98
g27
sg87
S'2012-07-19 13:45'
p977
sg100
g101
sg102
g27
sg86
g27
sassssS'Freddie_Pape'
p978
(dp979
g3
(dp980
g5
(dp981
g7
I0
sg8
(lp982
S'Freddie will display some confusion with letters and will have difficulty with worded problems.'
p983
ag27
asg12
S'Sen provision map for Freddie Pape'
p984
sg14
g15
sg16
(lp985
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Freddie Pape'
p986
sg24
I0
sg25
(lp987
g27
aS'If necessary, take the time to read through the social story with David. Store this in his tray and let him know this is where it will be. Explain to him that when he has forgotten about his personal space of that of others, he will need to read/look at his story to help him remember. After providing him with reminders (2), tell him you think he has forgotten about personal space bubbles and ask him to go and have a quick look at his book. Print and laminate the last page of this booklet and have it near the teachers sit. When David is not displaying the behaviours which are listed, you can simply put it in front of him as a visual reminder. When possible get him to say which one he was not doing properly.\r<br>Saturate him in this language and really go over the top with it so it becomes something he thinks about. Praise him when you notice he is taking sensible actions to help himself and again go over the top for a week with it to reinforce. Use other students as well for rewards so it does not always revolve around him and also everyone benefits from it.'
p988
asg29
(lp989
g562
ag27
asg32
S'SEN,Provision map,Freddie Pape'
p990
sg34
(lp991
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp992
g41
S'07/05/2010'
p993
sg43
S'Pape'
p994
sg45
S'Action'
p995
sg47
S'2004-05-17'
p996
sg49
S'DST 7/5/10 completed by NB\r<br>Memory Battery 7/10/10 completed by NB\r<br>EP Report Annie Mitchell 7/5/2011'
p997
sg51
g27
sg52
g201
sg54
S'07-05-2011'
p998
sg56
S'Freddie has specific difficulties with visual spatial problem solving. He processes visual information quickly but not always accurately and finds tasks that require him to visually sequence letters and numbers difficult. He has some difficulties with auditory discrimination and auditory processing. He has a small sight vocabulary and as such does not recognise all sight words automatically. He displays visual discrimination errors. These visual and auditory difficulties indicate dyslexia. VCI 112 PRI 100 WMI 102 PSI 118 FSIQ 110'
p999
sg58
g27
sg59
S'Freddie'
p1000
sg61
g27
sg62
S'4CE'
p1001
ssg64
I1
sg65
I0
sg66
(lp1002
S'Freddie has specific difficulties with visual spatial problem solving. He processes visual information quickly but not always accurately and finds tasks that require him to visually sequence letters and numbers difficult. He has some difficulties with auditory discrimination and auditory processing. He has a small sight vocabulary and as such does not recognise all sight words automatically. He displays visual discrimination errors. These visual and auditory difficulties indicate dyslexia.'
p1003
aS'It is important that tasks are chunked into small sections for him to maintain concentration and focus. Regular checking in with Freddie will also ensure he remains on task and focused on achieving the lesson objective. Very explicit instructions need to be given to Freddie and possibly supported by visuals and reminders. Encouraging Freddie to discuss his answers may additionally build meaning and deeper thought. Obviously correcting inappropriate answers and explaining why will additionally help build an awareness of meaningful and appropriate thinking. Learning tasks should be as multisensory as possible. Provide Freddie with a word bank of common sight words that he can refer to when writing. Allow Freddie to write his draft work and correct spelling after as to not prevent fluency in writing.'
p1004
asg70
S'07-05-2011'
p1005
sg72
I0
sg73
(lp1006
g581
ag582
asssg76
(dp1007
g5
(dp1008
g38
(lp1009
(dp1010
g81
S'M '
p1011
sg83
S'Email recieved from Tanya Marks informing the school that Freddie would be getting a full EP Report. '
p1012
sg85
S'17-02-2011'
p1013
sg86
g27
sg87
S'2011-02-17 11:20'
p1014
sa(dp1015
g81
S'M '
p1016
sg83
S'Meeting to discuss the EP report completed by Annie Mitchell. Present NB, Tanya Marks, HF and HSM. '
p1017
sg85
S'08-06-2011'
p1018
sg86
g27
sg87
S'2011-06-07 15:50'
p1019
sa(dp1020
g81
S'M '
p1021
sg83
S'CMC, MR and MRs Pape. Review of progress. '
p1022
sg85
g27
sg86
g27
sg87
S'2012-05-23 08:59'
p1023
sa(dp1024
g81
S'M '
p1025
sg83
S'CAE, CMC,NB,  MR and MRs Pape. Review of progress and discussion about Freddies lack of progress and attainment. All staff mentioned he must work much harder in year 4 and he must complete his homework. NB reported he is now a target student and an IEP will be created for him. Parents asked for daily support lessons which NB commented are not possible with our level of staff. Half termly review meetings will be established. '
p1026
sg85
g27
sg86
g27
sg87
S'2012-07-09 08:59'
p1027
sa(dp1028
g81
S'M '
p1029
sg83
S'NB, CMC, CPJ, Mr Pape. Review of IEP and progress. Discussion of support and how Freddie is coping at school. '
p1030
sg85
g27
sg86
g27
sg87
S'2012-11-19 16:30'
p1031
sasg37
(lp1032
(dp1033
g91
S'Lent'
p1034
sg93
S'2011'
p1035
sg95
S'To focus on learning 5 tricky words by sight per week.'
p1036
sg81
S'M '
p1037
sg98
g27
sg87
S'2009-05-13 16:45'
p1038
sg100
g101
sg102
g27
sg86
g27
sa(dp1039
g91
S'Summer'
p1040
sg93
S'2011'
p1041
sg95
S'To ensure I am remembering to write the final sound of a word. '
p1042
sg81
S'M '
p1043
sg98
g27
sg87
S'2009-05-13 16:45'
p1044
sg100
g101
sg102
g27
sg86
g27
sa(dp1045
g91
S'Mich'
p1046
sg93
S'2012'
p1047
sg95
S'To listen carefully to instructions. ONGOING'
p1048
sg81
S'M '
p1049
sg98
g27
sg87
S'2011-09-20 16:36'
p1050
sg100
g101
sg102
S'A sign for Freddie to check he is focussed.Check he has understood each part of activity.Freddie to check in with teacher after first few answers/questions to check he has understood correctly.'
p1051
sg86
g27
sa(dp1052
g91
S'Mich'
p1053
sg93
S'2012'
p1054
sg95
S'To ask if unsure about an activity. ONGOING'
p1055
sg81
S'M '
p1056
sg98
g27
sg87
S'2011-09-20 16:36'
p1057
sg100
g101
sg102
S'A sign for Freddie to check he is focussed.Check he has understood each part of activity.Freddie to check in with teacher after first few answers/questions to check he has understood correctly.'
p1058
sg86
g27
sa(dp1059
g91
S'Lent'
p1060
sg93
S'2012'
p1061
sg95
S'To listen carefully to instructions and ask if unsure about an activity.'
p1062
sg81
S'M '
p1063
sg98
g27
sg87
S'2012-01-27 08:59'
p1064
sg100
g101
sg102
g27
sg86
S'CDY'
p1065
sa(dp1066
g91
S'Lent'
p1067
sg93
S'2012'
p1068
sg95
S'Maths: Sit at the front or the class and always look at the teacher when instructions are being given.'
p1069
sg81
S'M '
p1070
sg98
g27
sg87
S'2012-01-27 08:59'
p1071
sg100
g139
sg102
g27
sg86
g27
sa(dp1072
g91
S'Mich'
p1073
sg93
S'2012'
p1074
sg95
S'1. To improve his reading accuracy and fluency.'
p1075
sg81
S'M '
p1076
sg98
g27
sg87
S'2012-07-19 12:34'
p1077
sg100
g101
sg102
g27
sg86
g27
sa(dp1078
g91
S'Mich'
p1079
sg93
S'2012'
p1080
sg95
S'2. Attempt and complete all homework and hand it in as necessary.'
p1081
sg81
S'M '
p1082
sg98
g27
sg87
S'2012-07-19 12:34'
p1083
sg100
g101
sg102
g27
sg86
g27
sa(dp1084
g91
S'Mich'
p1085
sg93
S'2012'
p1086
sg95
S'3. To increase focus and attention in lessons and complete a greater percentage of set tasks.'
p1087
sg81
S'M '
p1088
sg98
g27
sg87
S'2012-07-19 12:34'
p1089
sg100
g101
sg102
g27
sg86
g27
sa(dp1090
g91
S'Lent'
p1091
sg93
S'2013'
p1092
sg95
S'Maths: To stay focussed in lessons and not fiddle 75% of the time.                                                                                              '
p1093
sg81
S'M '
p1094
sg98
g27
sg87
S'2012-07-19 12:34'
p1095
sg100
g101
sg102
g27
sg86
g27
sa(dp1096
g91
S'Lent'
p1097
sg93
S'2013'
p1098
sg95
S'ENGLISH 1. To identify and edit at least 3 spelling mistakes from pieces of written work.                                                                                            '
p1099
sg81
S'M '
p1100
sg98
g27
sg87
S'2013-07-16 10:11'
p1101
sg100
g139
sg102
g27
sg86
g27
sa(dp1102
g91
S'Summer'
p1103
sg93
S'2013'
p1104
sg95
S'ENGLISH 1. To identify and edit at least 3 spelling mistakes from pieces of written work.                                                                                            '
p1105
sg81
S'M '
p1106
sg98
g27
sg87
S'2013-07-16 10:11'
p1107
sg100
g139
sg102
g27
sg86
g27
sa(dp1108
g91
S'Lent'
p1109
sg93
S'2013'
p1110
sg95
S' 2. Ensure all sentences have full stops and capital letters and are not too long.                                                                                    '
p1111
sg81
S'M '
p1112
sg98
g27
sg87
S'2013-07-16 10:11'
p1113
sg100
g139
sg102
g27
sg86
g27
sa(dp1114
g91
S'Summer'
p1115
sg93
S'2013'
p1116
sg95
S' 2. Ensure all sentences have full stops and capital letters and are not too long.                                                                                    '
p1117
sg81
S'M '
p1118
sg98
g27
sg87
S'2013-07-16 10:11'
p1119
sg100
g139
sg102
g27
sg86
g27
sa(dp1120
g91
S'Summer'
p1121
sg93
S'2013'
p1122
sg95
S'Maths: To stay focused in independent work and check with teacher before rushing ahead.    '
p1123
sg81
S'M '
p1124
sg98
g27
sg87
S'2013-07-16 10:12'
p1125
sg100
g101
sg102
g27
sg86
g27
sa(dp1126
g91
S'Mich'
p1127
sg93
S'2013'
p1128
sg95
S'Maths 1. To ask for help if needed.                            '
p1129
sg81
S'M '
p1130
sg98
g27
sg87
S'2013-07-16 10:12'
p1131
sg100
g139
sg102
g27
sg86
g27
sa(dp1132
g91
S'Mich'
p1133
sg93
S'2013'
p1134
sg95
S'2. To ensure that he has chosen the correct number operation for word problems.                  '
p1135
sg81
S'M '
p1136
sg98
g27
sg87
S'2013-07-16 10:12'
p1137
sg100
g139
sg102
g27
sg86
g27
sa(dp1138
g91
S'Mich'
p1139
sg93
S'2013'
p1140
sg95
S'ENGLISH 1. To identify and edit at least 3 spelling mistakes from pieces of written work.                                                                                            '
p1141
sg81
S'M '
p1142
sg98
g27
sg87
S'2013-07-16 10:12'
p1143
sg100
g139
sg102
g27
sg86
g27
sa(dp1144
g91
S'Mich'
p1145
sg93
S'2013'
p1146
sg95
S' 2. Ensure all sentences have full stops and capital letters and are not too long.                                                                                                       '
p1147
sg81
S'M '
p1148
sg98
g27
sg87
S'2013-07-16 10:12'
p1149
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp1150
(dp1151
g91
S'Michaelmas'
p1152
sg81
S'M '
p1153
sg87
S'2010-12-13 11:40'
p1154
sg145
S'Small group spelling support group concentating on high frequency words. '
p1155
sg52
S'2010'
p1156
sg148
S'weekly(x1)'
p1157
sg86
g27
sg150
S'KM'
p1158
sa(dp1159
g91
S'Lent'
p1160
sg81
S'M '
p1161
sg87
S'2010-12-13 11:40'
p1162
sg145
S'Small group spelling support group concentating on high frequency words.'
p1163
sg52
S'2011'
p1164
sg148
S'weekly(x1)'
p1165
sg86
g27
sg150
S'KM'
p1166
sa(dp1167
g91
S'Summer'
p1168
sg81
S'M '
p1169
sg87
S'2010-12-13 11:40'
p1170
sg145
S'Small group spelling support group concentating on high frequency words.'
p1171
sg52
S'2011'
p1172
sg148
S'weekly(x1)'
p1173
sg86
g27
sg150
S'AST'
p1174
sa(dp1175
g91
S'Summer'
p1176
sg81
S'M '
p1177
sg87
S'2011-07-13 13:25'
p1178
sg145
S'1:1 literacy support. '
p1179
sg52
S'2011'
p1180
sg148
S'weekly(x1)'
p1181
sg86
g27
sg150
S'HF'
p1182
sa(dp1183
g91
S'Michaelmas'
p1184
sg81
S'M '
p1185
sg87
S'2011-09-01 11:40'
p1186
sg145
S'Small group spelling support group concentating on high frequency words.'
p1187
sg52
S'2011'
p1188
sg148
S'weekly(x1)'
p1189
sg86
g27
sg150
S'NB'
p1190
sa(dp1191
g91
S'Lent'
p1192
sg81
S'M '
p1193
sg87
S'2011-10-10 11:40'
p1194
sg145
S'1:1 Literacy support with George Scott Kerr.'
p1195
sg52
S'2012'
p1196
sg148
S'weekly(x2)'
p1197
sg86
g27
sg150
g27
sa(dp1198
g91
S'Michaelmas'
p1199
sg81
S'M '
p1200
sg87
S'2011-10-10 11:40'
p1201
sg145
S'1:1 Literacy support with George Scott Kerr.'
p1202
sg52
S'2011'
p1203
sg148
S'weekly(x2)'
p1204
sg86
g27
sg150
g27
sa(dp1205
g91
S'Lent'
p1206
sg81
S'M '
p1207
sg87
S'2012-01-01 11:40'
p1208
sg145
S'Small group spelling support group concentating on high frequency words.'
p1209
sg52
S'2012'
p1210
sg148
S'weekly(x1)'
p1211
sg86
g27
sg150
S'NB'
p1212
sa(dp1213
g91
S'Summer'
p1214
sg81
S'M '
p1215
sg87
S'2012-03-11 11:40'
p1216
sg145
S'1:1 Literacy support with Sarah McKinlay.'
p1217
sg52
S'2012'
p1218
sg148
S'weekly(x2)'
p1219
sg86
g27
sg150
g27
sa(dp1220
g91
S'Summer'
p1221
sg81
S'M '
p1222
sg87
S'2012-05-03 11:40'
p1223
sg145
S'Small group spelling support group concentating on high frequency words.'
p1224
sg52
S'2012'
p1225
sg148
S'weekly(x1)'
p1226
sg86
g27
sg150
S'NB'
p1227
sa(dp1228
g91
S'Michaelmas'
p1229
sg81
S'M '
p1230
sg87
S'2012-05-03 11:40'
p1231
sg145
S'In class literacy support spelling and reading comprehension'
p1232
sg52
S'2012'
p1233
sg148
S'weekly(x1)'
p1234
sg86
g27
sg150
S'NB'
p1235
sa(dp1236
g91
S'Lent'
p1237
sg81
S'M '
p1238
sg87
S'2012-05-03 11:40'
p1239
sg145
S'In class literacy support spelling and reading comprehension'
p1240
sg52
S'2013'
p1241
sg148
S'weekly(x1)'
p1242
sg86
g27
sg150
S'NB'
p1243
sa(dp1244
g91
S'Summer'
p1245
sg81
S'M '
p1246
sg87
S'2012-05-03 11:40'
p1247
sg145
S'In class literacy support spelling and reading comprehension'
p1248
sg52
S'2013'
p1249
sg148
S'weekly(x1)'
p1250
sg86
g27
sg150
S'NB'
p1251
sa(dp1252
g91
S'Summer'
p1253
sg81
S'M '
p1254
sg87
S'2013-07-08 16:13'
p1255
sg145
S'1:1 Literacy support with EB'
p1256
sg52
S'2013'
p1257
sg148
S'weekly(x1)'
p1258
sg86
g27
sg150
g27
sa(dp1259
g91
S'Summer'
p1260
sg81
S'M '
p1261
sg87
S'2013-07-08 16:13'
p1262
sg145
S'1:1 Literacy support with GSK'
p1263
sg52
S'2013'
p1264
sg148
S'weekly(x1)'
p1265
sg86
g27
sg150
g27
sa(dp1266
g91
S'Lent'
p1267
sg81
S'M '
p1268
sg87
S'2013-07-08 16:13'
p1269
sg145
S'1:1 Literacy support with GSK'
p1270
sg52
S'2013'
p1271
sg148
S'weekly(x1)'
p1272
sg86
g27
sg150
g27
sa(dp1273
g91
S'Michaelmas'
p1274
sg81
S'M '
p1275
sg87
S'2013-07-08 16:13'
p1276
sg145
S'1:1 Literacy support with GSK'
p1277
sg52
S'2012'
p1278
sg148
S'weekly(x1)'
p1279
sg86
g27
sg150
g27
sassssS'Evelyn_Pilcher'
p1280
(dp1281
g3
(dp1282
g5
(dp1283
g7
I0
sg8
(lp1284
S'Jamie displays specific strenghts in numerical operations (126).'
p1285
ag27
asg12
S'Sen provision map for Evelyn Pilcher'
p1286
sg14
g15
sg16
(lp1287
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Evelyn Pilcher'
p1288
sg24
I0
sg25
(lp1289
S'Jaime is awarded 25% extra time in exams as a result of his slow speed of writing.  Word processing has been suggested to support his slow speed of writing however Jamie is not currently using a laptop in class or exams. This is a skill he should develop in the coming years.'
p1290
aS"Simplifying multi-step directions and using poster checklists may help Jamie learn without overwhelming his working memory.  Jamie would benefit from praise for remaining on task, and for task completion.  Have a non-verbal 'secret signal' with Jamie to help him realise when he is off task, to help him regain his focus. Specific seating plans, near the blackboard or front of class. Use visual timers where appropriate."
p1291
asg29
(lp1292
g27
ag31
asg32
S'SEN,Provision map,Evelyn Pilcher'
p1293
sg34
(lp1294
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp1295
g41
S'11-07-2012'
p1296
sg43
S'Pilcher'
p1297
sg45
S'Action'
p1298
sg47
S'2004-09-21'
p1299
sg49
S'DST completed by KAM 26.06.2012'
p1300
sg51
g27
sg52
g53
sg54
g27
sg56
S'Evelyn was referred to the LE Dept due to concerns with her literacy skills. She has specific difficulties with spelling and reading compared to a very good vocabulary and use of language in stories. Assessment completed by KAM indicated a few signs of dyslexia however this is not a formal diagnosis. Parents are happy to work with Evelyn on her areas of difficulty however please do not mention formal assessment of dyslexia to the family or Evelyn. A review meeting will be held before half term Michaelmas 2012.'
p1301
sg58
g27
sg59
S'Evelyn'
p1302
sg61
g27
sg62
S'3CW'
p1303
ssg64
I0
sg65
I0
sg66
(lp1304
S"Jaime has some difficulties with his literacy skills in particular his writing speed is very slow. His core literacy skills are age appropriate however discrepant in relation to his verbal and non verbal abilities. In the past 2 years he has made good progress with his reading, spelling and writing. Jamie's verbal comprehension abilities are in the superior range (124) and his perceptual reasoning very superior range (131)."
p1305
aS'Adopt a structured approach to literacy using small steps and much over-learning using a multi-sensory approach.  Ensure that the information load through language is not too great.  Clear and concise instructions will help.  Repetition is helpful particularly where vocabulary may be distractingly complex.  Helping to define the space available may make a noticeable difference to presentation. It would help if Jamie were to have a template for the setting out of a task in advance, showing, for example, lines where the heading and date go, a box in which to draw apparatus or a picture, and guidelines for the writing. Provide key spellings to support new topics.'
p1306
asg70
g392
sg72
I0
sg73
(lp1307
S'Jamie has food allergies, fish/egg/nuts.'
p1308
ag27
asssg76
(dp1309
g5
(dp1310
g36
(lp1311
(dp1312
g91
S'Michaelmas'
p1313
sg81
S'F '
p1314
sg87
S'2013-07-08 16:01'
p1315
sg145
S'Guided reading group'
p1316
sg52
S'2012'
p1317
sg148
S'weekly(x1)'
p1318
sg86
g27
sg150
S'NB'
p1319
sa(dp1320
g91
S'Lent'
p1321
sg81
S'F '
p1322
sg87
S'2013-07-08 16:01'
p1323
sg145
S'Guided reading group'
p1324
sg52
S'2013'
p1325
sg148
S'weekly(x1)'
p1326
sg86
g27
sg150
S'NB'
p1327
sa(dp1328
g91
S'Summer'
p1329
sg81
S'F '
p1330
sg87
S'2013-07-08 16:01'
p1331
sg145
S'Guided reading group'
p1332
sg52
S'2013'
p1333
sg148
S'weekly(x1)'
p1334
sg86
g27
sg150
S'NB'
p1335
sassssS'Eimear_Griffin'
p1336
(dp1337
g3
(dp1338
g5
(dp1339
g7
I0
sg8
(lp1340
g10
ag11
asg12
S'Sen provision map for Eimear Griffin'
p1341
sg14
g15
sg16
(lp1342
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Eimear Griffin'
p1343
sg24
I0
sg25
(lp1344
g27
aS'Breaking tasks down into smaller chunks, for which Eimear has to attend for short periods of time and which can then be followed by a change of activity. The class teacher should check for understanding of concepts through having Eimear repeat back what is required. Utilise brain gym type activities to allow for movement and re-focusing if required. When introducing a new topic or concept choose a small vocabulary of important words and check that they are understood and used appropriately. Encourage Eimear to elaborate on information and to extend it. Rather than accepting only the basic information from her ask her to tell you more about part of it, or even to find out more to tell you. Encourage Eimear to ask for help and to be specific about what is not understood, e.g., if she says "I don\'t know what I have to do", get her to say what she thinks she has to do and ask what part she does not understand.'
p1345
asg29
(lp1346
g27
ag31
asg32
S'SEN,Provision map,Eimear Griffin'
p1347
sg34
(lp1348
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp1349
g41
S'29/01/2010'
p1350
sg43
S'Griffin'
p1351
sg45
S'Action +'
p1352
sg47
S'2000-10-27'
p1353
sg49
S'EP Nina Elliott'
p1354
sg51
S'Both'
p1355
sg52
S'7'
p1356
sg54
g27
sg56
S"Eimear's nonverbal reasoning abilities are much better developed than her verbal reasoning abilities. Eimear's reasoning abilities on verbal tasks are generally in the Average range (VCI = 93), while her nonverbal reasoning abilities are significantly higher and in the High Average range (PRI = 110). Eimear's general working memory abilities are in the High Average range (WMI = 113), and general processing speed abilities in the Average range (PSI = 109).  Eimear exhibites some visual difficulties with near vision tasks, and spectacles were prescribed for reading. Her speed of writing is slow and she has difficulties with her reading and spelling skills, but has some strong maths skills.  Eimear has some particular difficulties with aspects of her phonological awareness skills. Eimear is presenting with dyslexia, or Specific Learning Difficulties which are impacting all aspects of her literacy skills."
p1357
sg58
g27
sg59
S'Eimear'
p1358
sg61
S'25%'
p1359
sg62
S'7CS'
p1360
ssg64
I1
sg65
I0
sg66
(lp1361
S'Eimear is presenting with dyslexia, or Specific Learning Difficulties which are impacting all aspects of her literacy skills.'
p1362
aS'Utilise a mulit-sensory approach when working on reading, spelling and writing skills, work to develop phonological awareness skills, use of content and meaning, structural analysis of word parts, flexible and appropriate use of strategies and reading for pleasure and meaning. Structured discussion to develop language and confidence, and fluency with language and literacy. Encourage Eimear to consider what is expected of her for a few moments before she starts writing or answering a question.'
p1363
asg70
S'26-01-2010'
p1364
sg72
I0
sg73
(lp1365
S'Eimear had a full eye assessment completed at John F. Rose in April 2009.  Eimear exhibited some visual difficulties with near vision tasks, and spectacles were prescribed for reading.'
p1366
ag27
asssg76
(dp1367
g5
(dp1368
g38
(lp1369
(dp1370
g81
S'F '
p1371
sg83
S'NB and Miss Cole. Review EP report. '
p1372
sg85
g27
sg86
g27
sg87
S'2011-02-09 11:42'
p1373
sa(dp1374
g81
S'F '
p1375
sg83
S'Review Meeting with NB and Katie Cole. Discussion of support options and current progress. '
p1376
sg85
S'29-11-2011'
p1377
sg86
g27
sg87
S'2011-11-29 11:42'
p1378
sasg37
(lp1379
(dp1380
g91
S'Lent'
p1381
sg93
S'2011'
p1382
sg95
S'To self edit and review all written work to ensure careless errors are indentified and corrected. '
p1383
sg81
S'F '
p1384
sg98
g27
sg87
S'2010-12-14 11:42'
p1385
sg100
g101
sg102
g27
sg86
g27
sa(dp1386
g91
S'Summer'
p1387
sg93
S'2011'
p1388
sg95
S'To develop the confidence to contribute more to class discussions.'
p1389
sg81
S'F '
p1390
sg98
g27
sg87
S'2010-12-14 11:42'
p1391
sg100
g101
sg102
g27
sg86
g27
sa(dp1392
g91
S'Mich'
p1393
sg93
S'2011'
p1394
sg95
S'To display a greater level of confidence in her own abilities.'
p1395
sg81
S'F '
p1396
sg98
g27
sg87
S'2011-09-30 11:42'
p1397
sg100
g101
sg102
g27
sg86
g27
sa(dp1398
g91
S'Mich'
p1399
sg93
S'2012'
p1400
sg95
S'To actively share her ideas and thoughts in lessons without being prompted by the teacher.'
p1401
sg81
S'F '
p1402
sg98
g27
sg87
S'2012-07-19 13:57'
p1403
sg100
g101
sg102
g27
sg86
g27
sa(dp1404
g91
S'Lent'
p1405
sg93
S'2013'
p1406
sg95
S'To focus and concentrate for 10 minute blocks in lessons. '
p1407
sg81
S'F '
p1408
sg98
g27
sg87
S'2012-07-19 13:57'
p1409
sg100
g101
sg102
g27
sg86
g27
sa(dp1410
g91
S'Summer'
p1411
sg93
S'2013'
p1412
sg95
S'To self assess my work and see if I can add more detail to my responses. '
p1413
sg81
S'F '
p1414
sg98
g27
sg87
S'2012-07-19 13:57'
p1415
sg100
g101
sg102
g27
sg86
g27
sa(dp1416
g91
S'Mich'
p1417
sg93
S'2013'
p1418
sg95
S'To extend my self and challenge my own thinking in activities I feel comfortable with. '
p1419
sg81
S'F '
p1420
sg98
g27
sg87
S'2013-07-16 11:22'
p1421
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp1422
(dp1423
g91
S'Michaelmas'
p1424
sg81
S'F '
p1425
sg87
S'2010-12-14 11:42'
p1426
sg145
S'1:1 Learning Specialist Isabel Clarke.'
p1427
sg52
S'2010'
p1428
sg148
S'weekly(x1)'
p1429
sg86
g27
sg150
g27
sa(dp1430
g91
S'Lent'
p1431
sg81
S'F '
p1432
sg87
S'2010-12-14 11:42'
p1433
sg145
S'1:1 Learning Specialist Isabel Clarke.'
p1434
sg52
S'2011'
p1435
sg148
S'weekly(x1)'
p1436
sg86
g27
sg150
g27
sa(dp1437
g91
S'Summer'
p1438
sg81
S'F '
p1439
sg87
S'2010-12-14 11:42'
p1440
sg145
S'1:1 Learning Specialist Isabel Clarke.'
p1441
sg52
S'2011'
p1442
sg148
S'weekly(x1)'
p1443
sg86
g27
sg150
g27
sa(dp1444
g91
S'Michaelmas'
p1445
sg81
S'F '
p1446
sg87
S'2010-12-14 11:42'
p1447
sg145
S'1:1 Learning Specialist Caroline Leahy.'
p1448
sg52
S'2011'
p1449
sg148
S'weekly(x1)'
p1450
sg86
g27
sg150
g27
sassssS'William_Freebairn'
p1451
(dp1452
g3
(dp1453
g5
(dp1454
g7
I0
sg8
(lp1455
S'Her Numerical operation and mathematical reasoning are in the average range and are an area of strength.'
p1456
ag27
asg12
S'Sen provision map for William Freebairn'
p1457
sg14
g15
sg16
(lp1458
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for William Freebairn'
p1459
sg24
I0
sg25
(lp1460
S'Luca may be hampered by trying to perfect all of his work as he does not want to have work samples with errors.'
p1461
ag27
asg29
(lp1462
g562
ag27
asg32
S'SEN,Provision map,William Freebairn'
p1463
sg34
(lp1464
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp1465
g41
S'24-01-2011'
p1466
sg43
S'Freebairn'
p1467
sg45
S'Action'
p1468
sg47
S'2005-12-29'
p1469
sg49
g27
sg51
g27
sg52
S'2'
p1470
sg54
g27
sg56
S'William can have difficulties with social interactions and peers. He is very literal and finds it difficult to understand reasoning when he is in the wrong. He does some socially inappropriate things such as interupting conversations and calling out across a large group. William has commenced social skilling sessions with NB looking specifically at sharing and communicating with peers and teachers when he was a problem.'
p1471
sg58
g27
sg59
S'William'
p1472
sg61
g27
sg62
S'2CS'
p1473
ssg64
I0
sg65
I0
sg70
g392
sg72
I0
sg66
(lp1474
S'Luca has some specific difficulties with viual motor integration that is affecting his ability to produce accurate writing and spelling at speed. He over relies on the phonic strategy. His slight weakness with visual memory means he often knows the word is spelt incorrectly however cannot remember the correct sequence of letters which is very frustrating for Luca.'
p1475
aS'To help Luca with his visual attention, use a line guide or highlighters to help him read key information. He will need support with spelling activities and the composition of his writing. Use mind maps and writing frames where possible. When writing, ensure Luca has a clear understanding of the lesson objective, i.e. just focus on punctuation. Encourage him to write a draft version where spelling can be checked later. Focus on qulaity over quantity and provide him with explicit targets that he can achieve to help break down longer tasks and keep him focused on the task at hand. Provide movement breaks within a long activity to help focus and concentration. Ensure he sits facing the board and limit what he copies when possible. If possible, scribe some information to allow him to share his thoughts without becoming frustrated. Encourage him to use his finger when reading to help visually track.'
p1476
asssg76
(dp1477
g5
(dp1478
g38
(lp1479
(dp1480
g81
S'M '
p1481
sg83
S'RA and Mrs Freebairn. Discussion of concerning behaviours and social skills. '
p1482
sg85
g27
sg86
g27
sg87
S'2011-01-24 09:29'
p1483
sa(dp1484
g81
S'M '
p1485
sg83
S'NB, HSM, RA Mr and Mrs Freebairn. Review of progress and social skilling sessions. '
p1486
sg85
g27
sg86
g27
sg87
S'2011-05-23 09:31'
p1487
sasg37
(lp1488
(dp1489
g91
S'Mich'
p1490
sg93
S'2011'
p1491
sg95
S'To greet adults with a hand shake and look them in the eye.'
p1492
sg81
S'M '
p1493
sg98
g27
sg87
S'2011-09-20 19:58'
p1494
sg100
g101
sg102
g27
sg86
S'LGA'
p1495
sa(dp1496
g91
S'Lent'
p1497
sg93
S'2012'
p1498
sg95
S'Put my hand up and wait for an adult to answer my question.'
p1499
sg81
S'M '
p1500
sg98
g27
sg87
S'2012-02-06 19:58'
p1501
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp1502
(dp1503
g91
S'Michaelmas'
p1504
sg81
S'M '
p1505
sg87
S'2011-09-20 19:58'
p1506
sg145
S'Small group social skilling support. '
p1507
sg52
S'2011'
p1508
sg148
S'weekly(x1)'
p1509
sg86
g27
sg150
S'NB'
p1510
sa(dp1511
g91
S'Summer'
p1512
sg81
S'M '
p1513
sg87
S'2011-09-20 19:58'
p1514
sg145
S'Small group social skilling support.'
p1515
sg52
S'2011'
p1516
sg148
S'weekly(x1)'
p1517
sg86
g27
sg150
S'NB'
p1518
sa(dp1519
g91
S'Michaelmas'
p1520
sg81
S'M '
p1521
sg87
S'2013-07-08 15:37'
p1522
sg145
S'Lower school extension group'
p1523
sg52
S'2012'
p1524
sg148
S'weekly(x1)'
p1525
sg86
g27
sg150
S'KM'
p1526
sa(dp1527
g91
S'Lent'
p1528
sg81
S'M '
p1529
sg87
S'2013-07-08 15:37'
p1530
sg145
S'Lower school extension group'
p1531
sg52
S'2013'
p1532
sg148
S'weekly(x1)'
p1533
sg86
g27
sg150
S'KM'
p1534
sa(dp1535
g91
S'Summer'
p1536
sg81
S'M '
p1537
sg87
S'2013-07-08 15:37'
p1538
sg145
S'Lower school extension group'
p1539
sg52
S'2013'
p1540
sg148
S'weekly(x1)'
p1541
sg86
g27
sg150
S'KM'
p1542
sassssS'Olivia_Mermagen'
p1543
(dp1544
g3
(dp1545
g5
(dp1546
g7
I0
sg8
(lp1547
S'Tilda will have similar difficulties when processing visual information in mathematics. She may read numerical operations incorrectly.'
p1548
aS'Please refer to suggestions above as they are universal.'
p1549
asg12
S'Sen provision map for Olivia Mermagen'
p1550
sg14
g15
sg16
(lp1551
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Olivia Mermagen'
p1552
sg24
I0
sg25
(lp1553
g27
aS' Ensure all instructions are kept precise and check understanding and retention by asking for repetition and clarification during task. Use any appropriate devices to support her memory " flashcards, diagrams, visual prompts, lists of tasks to complete. Break tasks down into their smallest components and encourage completion of each step at a time. Praise sustained attention and active participation in class. Encourage Olivia to reflect on the task before commencing. Ensure information load through language is not too great and provide clear and concise instructions. Repeat as necessary and check for understanding regularly.'
p1554
asg29
(lp1555
g562
ag27
asg32
S'SEN,Provision map,Olivia Mermagen'
p1556
sg34
(lp1557
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp1558
g41
S'01/06/2010'
p1559
sg43
S'Mermagen'
p1560
sg45
S'Action +'
p1561
sg47
S'2002-04-08'
p1562
sg49
S'EP Assessment Nina Elliott\r<br>DST 2.7.10 by NB'
p1563
sg51
g27
sg52
S'6'
p1564
sg54
S'12-07-2011'
p1565
sg56
S"Olivia has some mild dyslexic difficulties. Olivia's abilities to sustain attention and concentrate are a weakness relative to her nonverbal and verbal reasoning abilities which are superior. Olivia has some difficulties with her working memory and speed of phonological production. Olivia has a relatively weak verbal memory. VCI 128 PRi 127 WMI 94 PSI 88"
p1566
sg58
g27
sg59
S'Olivia'
p1567
sg61
S'25%'
p1568
sg62
S'6CN'
p1569
ssg64
I1
sg65
I0
sg66
(lp1570
S'Olivia has some mild dyslexic difficulties.'
p1571
aS"Provide a word list of high frequency words to support writing. Encourage the use of a trying book with new spellings or words she is not familiar with. This will then be a record that she can refer back to in the future. Encourage Olivia to look closely and try new words. Praise close attempts at spelling, and tell her why it was a good attempt. Continue with the additional phonic based spelling activities. Check understanding of vocabulary when introducing new concepts or topics. Check Olivia's pencil grip when writing."
p1572
asg70
S'25-09-2010'
p1573
sg72
I0
sg73
(lp1574
g581
ag582
asssg76
(dp1575
g5
(dp1576
g38
(lp1577
(dp1578
g81
S'F '
p1579
sg83
S'NB and Mrs Mermagen. Discussing EP Report. '
p1580
sg85
g27
sg86
g27
sg87
S'2010-10-20 14:47'
p1581
sa(dp1582
g81
S'F '
p1583
sg83
S'GSK, AP and Mrs Mermagen. Review of 1:1 lessons and support suggestions. '
p1584
sg85
g27
sg86
g27
sg87
S'2011-02-08 14:46'
p1585
sa(dp1586
g81
S'F '
p1587
sg83
S'Email to Mrs Mermagen from NB. Email regarding touch typing teachers for Isabel. '
p1588
sg85
g27
sg86
g27
sg87
S'2011-07-06 14:48'
p1589
sa(dp1590
g81
S'F '
p1591
sg83
S'Summer Report - George Scott Kerr. '
p1592
sg85
g27
sg86
g27
sg87
S'2011-07-12 14:46'
p1593
sasg37
(lp1594
(dp1595
g91
S'Lent'
p1596
sg93
S'2011'
p1597
sg95
S'To use the spelling strategies from spelling group when attempting unknown words. '
p1598
sg81
S'F '
p1599
sg98
g27
sg87
S'2010-12-14 10:06'
p1600
sg100
g101
sg102
g27
sg86
g27
sa(dp1601
g91
S'Mich'
p1602
sg93
S'2011'
p1603
sg95
S'To increase independence as a learner especially through reducing the occurrence of questions which she knows the answer to.'
p1604
sg81
S'F '
p1605
sg98
g27
sg87
S'2011-10-10 10:06'
p1606
sg100
g101
sg102
g27
sg86
g27
sa(dp1607
g91
S'Lent'
p1608
sg93
S'2012'
p1609
sg95
S'To ensure peer discussions remain relevant to the lesson objective.'
p1610
sg81
S'F '
p1611
sg98
g27
sg87
S'2012-02-27 10:06'
p1612
sg100
g101
sg102
g27
sg86
g27
sa(dp1613
g91
S'Mich'
p1614
sg93
S'2012'
p1615
sg95
S'To display greater focus and concentration in all lessons.'
p1616
sg81
S'F '
p1617
sg98
g27
sg87
S'2012-07-19 12:25'
p1618
sg100
g101
sg102
g27
sg86
g27
sa(dp1619
g91
S'Lent'
p1620
sg93
S'2013'
p1621
sg95
S'To not let spelling stop me from attempting work and answering questions. (75% of the time).'
p1622
sg81
S'F '
p1623
sg98
g27
sg87
S'2012-07-19 12:25'
p1624
sg100
g101
sg102
g27
sg86
g27
sasg36
(lp1625
(dp1626
g91
S'Michaelmas'
p1627
sg81
S'F '
p1628
sg87
S'2010-12-14 10:06'
p1629
sg145
S'Small group spelling support focusing on spelling strategies.'
p1630
sg52
S'2010'
p1631
sg148
S'weekly(x1)'
p1632
sg86
g27
sg150
S'NB'
p1633
sa(dp1634
g91
S'Lent'
p1635
sg81
S'F '
p1636
sg87
S'2010-12-14 10:06'
p1637
sg145
S'Small group spelling support focusing on spelling strategies.'
p1638
sg52
S'2011'
p1639
sg148
S'weekly(x1)'
p1640
sg86
g27
sg150
S'NB'
p1641
sa(dp1642
g91
S'Lent'
p1643
sg81
S'F '
p1644
sg87
S'2010-12-14 10:06'
p1645
sg145
S'1:1 Learning Support George Scott Kerr. '
p1646
sg52
S'2011'
p1647
sg148
S'weekly(x1)'
p1648
sg86
g27
sg150
g27
sa(dp1649
g91
S'Summer'
p1650
sg81
S'F '
p1651
sg87
S'2010-12-14 10:06'
p1652
sg145
S'Small group spelling support focusing on spelling strategies.'
p1653
sg52
S'2011'
p1654
sg148
S'weekly(x1)'
p1655
sg86
g27
sg150
S'NB'
p1656
sa(dp1657
g91
S'Summer'
p1658
sg81
S'F '
p1659
sg87
S'2010-12-14 10:06'
p1660
sg145
S'1:1 Learning Support George Scott Kerr.'
p1661
sg52
S'2011'
p1662
sg148
S'weekly(x1)'
p1663
sg86
g27
sg150
g27
sa(dp1664
g91
S'Michaelmas'
p1665
sg81
S'F '
p1666
sg87
S'2010-12-14 10:06'
p1667
sg145
S'1:1 Learning Support George Scott Kerr.'
p1668
sg52
S'2011'
p1669
sg148
S'weekly(x1)'
p1670
sg86
g27
sg150
g27
sa(dp1671
g91
S'Lent'
p1672
sg81
S'F '
p1673
sg87
S'2013-07-08 16:20'
p1674
sg145
S'1:1 Specialist Teaching Lindsey Copeman'
p1675
sg52
S'2013'
p1676
sg148
S'weekly(x1)'
p1677
sg86
g27
sg150
g27
sa(dp1678
g91
S'Summer'
p1679
sg81
S'F '
p1680
sg87
S'2013-07-08 16:20'
p1681
sg145
S'1:1 Specialist Teaching Lindsey Copeman'
p1682
sg52
S'2013'
p1683
sg148
S'weekly(x1)'
p1684
sg86
g27
sg150
g27
sassssS'Callum_Graham'
p1685
(dp1686
g3
(dp1687
g5
(dp1688
g7
I0
sg8
(lp1689
g10
ag11
asg12
S'Sen provision map for Callum Graham'
p1690
sg14
g15
sg16
(lp1691
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Callum Graham'
p1692
sg24
I1
sg25
(lp1693
g27
ag28
asg29
(lp1694
g27
ag31
asg32
S'SEN,Provision map,Callum Graham'
p1695
sg34
(lp1696
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp1697
g41
S'16/02/2008'
p1698
sg43
S'Graham'
p1699
sg45
S'Action'
p1700
sg47
S'2001-06-17'
p1701
sg49
S'H Poulter (OT) Jan 2008\r<br>M.Loubascher (Ed Psych)\r<br>Dr Cavanagh (Neurologist) 16/12/08\r<br>Laura Irwin (Physio) 14/01/09'
p1702
sg51
S'Both'
p1703
sg52
g1356
sg54
g27
sg56
S'Callum has difficulties with sensory processing and self regulation. He has reduced in-hand manipulation skills and reduced strength in the small muscles of the hand and forearm.  Callum is verbally gifted.  He has spatial delay and displays Dyspraxic type tendencies. He has very weak auditory filtering skills therefore in a noisy environment he will find it difficult to focus and extract relevant information.'
p1704
sg58
g27
sg59
S'Callum'
p1705
sg61
g27
sg62
S'7CS'
p1706
ssg64
I1
sg65
I0
sg66
(lp1707
g1362
ag1363
asg70
S'11-11-2008'
p1708
sg72
I0
sg73
(lp1709
g75
ag27
asssg76
(dp1710
g5
(dp1711
g38
(lp1712
(dp1713
g81
S'M '
p1714
sg83
S'KH and Mrs Graham. Science set discussion and laptop use. '
p1715
sg85
S'05-11-2010'
p1716
sg86
g27
sg87
S'2010-11-05 10:57'
p1717
sa(dp1718
g81
S'M '
p1719
sg83
S'NB and Mrs Graham. Meeting to discuss exam results and revision techniques. '
p1720
sg85
S'30-11-2011'
p1721
sg86
g27
sg87
S'2011-11-30 17:07'
p1722
sasg37
(lp1723
(dp1724
g91
S'Lent'
p1725
sg93
S'2011'
p1726
sg95
S'To focus and concentration for 10 minute periods in class.'
p1727
sg81
S'M '
p1728
sg98
g27
sg87
S'2010-12-14 11:01'
p1729
sg100
g101
sg102
g27
sg86
g27
sa(dp1730
g91
S'Lent'
p1731
sg93
S'2011'
p1732
sg95
S'To use his laptop during all longer written tasks at school. '
p1733
sg81
S'M '
p1734
sg98
g27
sg87
S'2010-12-14 11:01'
p1735
sg100
g101
sg102
g27
sg86
g27
sa(dp1736
g91
S'Mich'
p1737
sg93
S'2011'
p1738
sg95
S'To use his laptop during all longer written tasks at school.  '
p1739
sg81
S'M '
p1740
sg98
g27
sg87
S'2011-09-30 16:35'
p1741
sg100
g139
sg102
g27
sg86
g27
sa(dp1742
g91
S'Mich'
p1743
sg93
S'2011'
p1744
sg95
S'To focus and concentration for 10 minute periods in class.'
p1745
sg81
S'M '
p1746
sg98
g27
sg87
S'2011-09-30 16:35'
p1747
sg100
g101
sg102
g27
sg86
g27
sa(dp1748
g91
S'Lent'
p1749
sg93
S'2012'
p1750
sg95
S'To use his laptop during all longer written tasks at school.       '
p1751
sg81
S'M '
p1752
sg98
g27
sg87
S'2011-09-30 16:35'
p1753
sg100
g101
sg102
g27
sg86
g27
sa(dp1754
g91
S'Mich'
p1755
sg93
S'2012'
p1756
sg95
S'To be ready to start each lesson within 2 minutes of entering the room.'
p1757
sg81
S'M '
p1758
sg98
g27
sg87
S'2012-07-19 13:57'
p1759
sg100
g101
sg102
g27
sg86
g27
sa(dp1760
g91
S'Lent'
p1761
sg93
S'2013'
p1762
sg95
S'1. Only take out of my bag the equipment I need for the lesson.                                                                                                                '
p1763
sg81
S'M '
p1764
sg98
g27
sg87
S'2012-07-19 13:57'
p1765
sg100
g139
sg102
g27
sg86
g27
sa(dp1766
g91
S'Lent'
p1767
sg93
S'2013'
p1768
sg95
S'2. Only ask questions relevant to the lesson unless otherwise directed by the teacher. (75% of the time).                                                                                                              '
p1769
sg81
S'M '
p1770
sg98
g27
sg87
S'2012-07-19 13:57'
p1771
sg100
g139
sg102
g27
sg86
g27
sa(dp1772
g91
S'Summer'
p1773
sg93
S'2013'
p1774
sg95
S'1. Only take out of my bag the equipment I need for the lesson.                                                                                                                '
p1775
sg81
S'M '
p1776
sg98
g27
sg87
S'2012-07-19 13:57'
p1777
sg100
g101
sg102
g27
sg86
g27
sa(dp1778
g91
S'Summer'
p1779
sg93
S'2013'
p1780
sg95
S'2. Only ask questions relevant to the lesson unless otherwise directed by the teacher. (75% of the time).                                                                                                              '
p1781
sg81
S'M '
p1782
sg98
g27
sg87
S'2012-07-19 13:57'
p1783
sg100
g101
sg102
g27
sg86
g27
sa(dp1784
g91
S'Mich'
p1785
sg93
S'2013'
p1786
sg95
S'To take more care with all of my work and make sure my handwriting is neat and my work is presented to a high standard.     '
p1787
sg81
S'M '
p1788
sg98
g27
sg87
S'2013-07-16 11:15'
p1789
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp1790
(dp1791
g91
S'Michaelmas'
p1792
sg81
S'M '
p1793
sg87
S'2010-12-14 10:57'
p1794
sg145
S'Handwriting Support group. '
p1795
sg52
S'2010'
p1796
sg148
S'weekly(x1)'
p1797
sg86
g27
sg150
S'NB'
p1798
sa(dp1799
g91
S'Lent'
p1800
sg81
S'M '
p1801
sg87
S'2010-12-14 10:57'
p1802
sg145
S'Handwriting Support group.'
p1803
sg52
S'2011'
p1804
sg148
S'weekly(x1)'
p1805
sg86
g27
sg150
S'NB'
p1806
sa(dp1807
g91
S'Lent'
p1808
sg81
S'M '
p1809
sg87
S'2010-12-14 10:57'
p1810
sg145
S'1:1 Study Skills and Mind Mapping. '
p1811
sg52
S'2011'
p1812
sg148
S'weekly(x1)'
p1813
sg86
g27
sg150
S'NB'
p1814
sa(dp1815
g91
S'Summer'
p1816
sg81
S'M '
p1817
sg87
S'2010-12-14 10:57'
p1818
sg145
S'Handwriting Support group.'
p1819
sg52
S'2011'
p1820
sg148
S'weekly(x1)'
p1821
sg86
g27
sg150
S'NB'
p1822
sassssS'Toby_Davis-Rowley'
p1823
(dp1824
g3
(dp1825
g5
(dp1826
g7
I0
sg8
(lp1827
S'Elke has some specific diffioculties with recall from long term memory, short term working memory (visual and auditory) and recalling the correct sequence of information specifically numbers. She has difficulties with place value and her basic knowledge of maths may at times be weak.'
p1828
aS"Provide Elke with visual and concrete demonstrations during multistep mathematics questions. Elke must be shown how to set out a question and learn these processes to prevent confusion and loosing information that is stored short term. Templates will help her follow the process and focus on the question itself.  Elke must rote learn many simple mathematical concepts such as number bonds of 10 and 20 regularly. This needs to be automatic for Elke so that she does not have to rely on calculating additional steps in questions. 100's charts can be used in most mathematics questions and I would suggest she had one with her when completing her work. Additional work should be completed on place value. Elke must develop a greater understanding of mathematical vocabulary to help her with the automaticity of her thinking and answers. Display this in the room and create a small sheet for her to assist with written questions."
p1829
asg12
S'Sen provision map for Toby Davis-Rowley'
p1830
sg14
g15
sg16
(lp1831
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Toby Davis-Rowley'
p1832
sg24
I0
sg25
(lp1833
g27
aS'It is important that teachers ensure all instructions are kept precise and check understanding and retention by asking for repetition and clarification during tasks.  As Ella has a weak working memory, teachers should use any appropriate devices to support her memory such as flashcards, diagrams and visual prompts. To ensure Ella remains focused and on task develop a signal that can be used if Ella feels unsure " ask \'what she thinks she needs to do?\' Break tasks down into their smallest components and encourage completion of each step at a time.'
p1834
asg29
(lp1835
g27
ag31
asg32
S'SEN,Provision map,Toby Davis-Rowley'
p1836
sg34
(lp1837
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp1838
g41
S'10/03/2010'
p1839
sg43
S'Davis-Rowley'
p1840
sg45
S'Action +'
p1841
sg47
S'2003-11-07'
p1842
sg49
S'Observation completed by NB 2/3/10.\r<br>EP report completed by Annie Mitchell. \r<br>Speech and Language with Lorna Davis from Michaelmas 2010.'
p1843
sg51
S'Both'
p1844
sg52
g201
sg54
g27
sg56
S'Toby has some social communication and behavioural difficulties.  VCI 136 PSI 115 WMI 91 PSI 97'
p1845
sg58
g27
sg59
S'Toby'
p1846
sg61
g27
sg62
S'4CN'
p1847
ssg64
I1
sg65
I0
sg66
(lp1848
g27
aS'Provide Toby with 1 or 2 step instructions only.  As he has strong reading skills this can be used as a reference point for him.  Encourage hands on learning activities.'
p1849
asg70
S'18-03-2010'
p1850
sg72
I1
sg73
(lp1851
S'Alex has been diagnosed with Encopresis. This affects his ability to regulate his bowels and he often has toileting accidents.'
p1852
aS"Please make your self familiar with Alex's management plan which clearly sets out the process for supporting him with this issue."
p1853
asssg76
(dp1854
g5
(dp1855
g38
(lp1856
(dp1857
g81
S'M '
p1858
sg83
S'Mrs DR and HSM. Discussion of in school behaviour and suggestion of possible EP assessment. '
p1859
sg85
g27
sg86
g27
sg87
S'2010-01-19 11:57'
p1860
sa(dp1861
g81
S'M '
p1862
sg83
S'NB, HSM, AS, SM, Mr and Mrs DR. Provided feedback from in school assessment. EP assessment suggested. '
p1863
sg85
g27
sg86
g27
sg87
S'2010-03-18 11:57'
p1864
sa(dp1865
g81
S'M '
p1866
sg83
S'HSM, Annie Mitchell, SM, Mrs DR. Annie Mitchell providing feedback from assessment and observation. '
p1867
sg85
g27
sg86
g27
sg87
S'2010-04-30 11:57'
p1868
sa(dp1869
g81
S'M '
p1870
sg83
S'NB and Mrs DR. NB requesting a copy of EP report.  '
p1871
sg85
g27
sg86
g27
sg87
S'2010-06-20 11:57'
p1872
sa(dp1873
g81
S'M '
p1874
sg83
S'HSM, HF, NB, Mr and Mrs DR. Review Sp. and Lan. support and interaction with peers.  '
p1875
sg85
g27
sg86
g27
sg87
S'2011-05-04 11:57'
p1876
sa(dp1877
g81
S'M '
p1878
sg83
S'HF and Ruth Samur. Discuss progress and specific areas to focus on.  '
p1879
sg85
g27
sg86
g27
sg87
S'2011-05-13 11:57'
p1880
sa(dp1881
g81
S'M '
p1882
sg83
S'NB, CMC, KH and Mr and Mrs DR. Discussion regarding year 3. '
p1883
sg85
g27
sg86
g27
sg87
S'2011-10-03 11:57'
p1884
sasg37
(lp1885
(dp1886
g91
S'Lent'
p1887
sg93
S'2011'
p1888
sg95
S'To develop a greater awareness of conversational skills through giving eye contact and allowing others to speak during a discussion. '
p1889
sg81
S'M '
p1890
sg98
g27
sg87
S'2010-12-13 11:57'
p1891
sg100
g101
sg102
g27
sg86
g27
sa(dp1892
g91
S'Summer'
p1893
sg93
S'2011'
p1894
sg95
S'To avoid getting distracted and to focus on the task at hand.'
p1895
sg81
S'M '
p1896
sg98
g27
sg87
S'2010-12-13 11:57'
p1897
sg100
g101
sg102
g27
sg86
g27
sa(dp1898
g91
S'Mich'
p1899
sg93
S'2011'
p1900
sg95
S'To be able to not call out and always put his hand up and wait to be asked to speak.'
p1901
sg81
S'M '
p1902
sg98
g27
sg87
S'2011-09-20 08:26'
p1903
sg100
g101
sg102
S'Remind Toby to put his hand up and not call out.  Give Toby clear guidelines and boundaries and a clear sanctioning system.'
p1904
sg86
S'KH'
p1905
sa(dp1906
g91
S'Lent'
p1907
sg93
S'2012'
p1908
sg95
S'To follow instructions immediately without question'
p1909
sg81
S'M '
p1910
sg98
g27
sg87
S'2012-02-06 08:26'
p1911
sg100
g139
sg102
g27
sg86
g27
sa(dp1912
g91
S'Mich'
p1913
sg93
S'2012'
p1914
sg95
S'1. To follow instructions immediately without questioning.'
p1915
sg81
S'M '
p1916
sg98
g27
sg87
S'2012-07-19 13:39'
p1917
sg100
g101
sg102
g27
sg86
g27
sa(dp1918
g91
S'Mich'
p1919
sg93
S'2012'
p1920
sg95
S'2. To not talk back inappriopriately to teachers.'
p1921
sg81
S'M '
p1922
sg98
g27
sg87
S'2012-07-19 13:39'
p1923
sg100
g101
sg102
g27
sg86
g27
sa(dp1924
g91
S'Lent'
p1925
sg93
S'2013'
p1926
sg95
S'1. To remember to take his belongings to lessons 75% of the time.                        '
p1927
sg81
S'M '
p1928
sg98
g27
sg87
S'2012-07-19 13:39'
p1929
sg100
g101
sg102
g27
sg86
g27
sa(dp1930
g91
S'Lent'
p1931
sg93
S'2013'
p1932
sg95
S'2. To listen and act upon teachers advice on how to improve his work. (To be seen on 3 occasions per week).                                                             '
p1933
sg81
S'M '
p1934
sg98
g27
sg87
S'2012-07-19 13:39'
p1935
sg100
g101
sg102
g27
sg86
g27
sa(dp1936
g91
S'Summer'
p1937
sg93
S'2013'
p1938
sg95
S'1. To listen to instructions, not answer back and accept that the teacher is trying to help him.        '
p1939
sg81
S'M '
p1940
sg98
g27
sg87
S'2012-07-19 13:39'
p1941
sg100
g101
sg102
g27
sg86
g27
sa(dp1942
g91
S'Summer'
p1943
sg93
S'2013'
p1944
sg95
S' 2. To take more responsibility for his own actions.                                           '
p1945
sg81
S'M '
p1946
sg98
g27
sg87
S'2012-07-19 13:39'
p1947
sg100
g139
sg102
g27
sg86
g27
sa(dp1948
g91
S'Mich'
p1949
sg93
S'2013'
p1950
sg95
S'1. To continue to add detail in his comprehension answers and check that his answers are relevant.                                                   '
p1951
sg81
S'M '
p1952
sg98
g27
sg87
S'2013-07-16 10:17'
p1953
sg100
g139
sg102
g27
sg86
g27
sa(dp1954
g91
S'Mich'
p1955
sg93
S'2013'
p1956
sg95
S'2. To take more responsibility for his own actions.                                           '
p1957
sg81
S'M '
p1958
sg98
g27
sg87
S'2013-07-16 10:17'
p1959
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp1960
(dp1961
g91
S'Michaelmas'
p1962
sg81
S'M '
p1963
sg87
S'2010-12-13 11:57'
p1964
sg145
S'1:1 Speech and Language Therapy - Lorna Davis'
p1965
sg52
S'2010'
p1966
sg148
S'weekly(x1)'
p1967
sg86
g27
sg150
g27
sa(dp1968
g91
S'Lent'
p1969
sg81
S'M '
p1970
sg87
S'2010-12-13 11:57'
p1971
sg145
S'1:1 Speech and Language Therapy - Lorna Davis'
p1972
sg52
S'2011'
p1973
sg148
S'weekly(x1)'
p1974
sg86
g27
sg150
g27
sa(dp1975
g91
S'Lent'
p1976
sg81
S'M '
p1977
sg87
S'2010-12-13 11:57'
p1978
sg145
S'1:1 Speech and Language Therapy - Ruth Samur'
p1979
sg52
S'2011'
p1980
sg148
S'weekly(x1)'
p1981
sg86
g27
sg150
g27
sa(dp1982
g91
S'Summer'
p1983
sg81
S'M '
p1984
sg87
S'2010-12-13 11:57'
p1985
sg145
S'1:1 Speech and Language Therapy - Ruth Samur'
p1986
sg52
S'2011'
p1987
sg148
S'weekly(x1)'
p1988
sg86
g27
sg150
g27
sa(dp1989
g91
S'Michaelmas'
p1990
sg81
S'M '
p1991
sg87
S'2010-12-13 11:57'
p1992
sg145
S'1:1 Speech and Language Therapy - Lorna Davis'
p1993
sg52
S'2011'
p1994
sg148
S'weekly(x1)'
p1995
sg86
g27
sg150
g27
sa(dp1996
g91
S'Lent'
p1997
sg81
S'M '
p1998
sg87
S'2010-12-13 11:57'
p1999
sg145
S'1:1 Speech and Language Therapy - Lorna Davis'
p2000
sg52
S'2012'
p2001
sg148
S'weekly(x1)'
p2002
sg86
g27
sg150
g27
sa(dp2003
g91
S'Lent'
p2004
sg81
S'M '
p2005
sg87
S'2012-01-02 11:57'
p2006
sg145
S'1:1 Speech and Language Therapy - Lorna Davis'
p2007
sg52
S'2012'
p2008
sg148
S'weekly(x1)'
p2009
sg86
g27
sg150
g27
sa(dp2010
g91
S'Summer'
p2011
sg81
S'M '
p2012
sg87
S'2012-01-02 11:57'
p2013
sg145
S'1:1 Speech and Language Therapy - Lorna Davis'
p2014
sg52
S'2012'
p2015
sg148
S'weekly(x1)'
p2016
sg86
g27
sg150
g27
sa(dp2017
g91
S'Summer'
p2018
sg81
S'M '
p2019
sg87
S'2013-07-08 15:48'
p2020
sg145
S'Reading comprehension 1:1'
p2021
sg52
S'2013'
p2022
sg148
S'Monthly(x2)'
p2023
sg86
g27
sg150
S'KM'
p2024
sassssS'Joseph_Amenta'
p2025
(dp2026
g3
(dp2027
g5
(dp2028
g7
I0
sg8
(lp2029
g1548
ag1549
asg12
S'Sen provision map for Joseph Amenta'
p2030
sg14
g15
sg16
(lp2031
g27
aS"Teachers should be aware of Sophie's hearing loss and ensure careful positioning within class sitting with her aided ear (left) towards the teacher and enabling her to see the speaker. Ensure eye contact and make sure Sophie is looking at speaker before communication commences. Encourage Sophie to repeat instructions back to you to check that she has heard and knows what is required. Name others before they speak so Sophie can turn and face them, additionally repeat what other pupils say."
p2032
asg20
g21
sg22
S'Special Needs Provision Map for Joseph Amenta'
p2033
sg24
I0
sg25
(lp2034
g27
ag1834
asg29
(lp2035
S'Alex requires support to ensure he speaks and acts appropriately to peers and adults.'
p2036
aS"Use very clear and concise words when communicating with Alex. It is important to slow Alex down and get him to operate at our level when we are trying to communicate something with him, otherwise he seems to say 'yes, yes, yes ' whilst looking where he wants to be and moving towards this locations. Support Alex with peer interactions and model as necessary any behaviours you think he requires. Praise Alex for communicating his needs and for displaying appropriate social skills."
p2037
asg32
S'SEN,Provision map,Joseph Amenta'
p2038
sg34
(lp2039
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp2040
g41
g27
sg43
S'Amenta'
p2041
sg45
g27
sg47
S'2006-05-23'
p2042
sg49
g27
sg51
g27
sg52
g1470
sg54
g27
sg56
g27
sg58
g27
sg59
S'Joseph'
p2043
sg61
g27
sg62
S'2CS'
p2044
ssg64
I0
sg65
I0
sg66
(lp2045
S"Matthew's ability to understand concepts presented in language is considerably better developed than his ability to demonstrate his thoughts and ideas in words. He has a limited vocabulary and his word knowledge in weaker than his verbal reasoning. He has a slight weakness with his receptive language skills and this will impact on his ability to store language in the short term and sustain attention and focus. Matthew is a very slow reader and has a slow writing speed. This will greatly impact on his ability to complete longer written tasks. He has mild dyslexia."
p2046
aS'Provide Matthew with extra time in class where possible. Reduce copying from the board as this will take a great deal of time and effort for Matt. Please be aware of his very slow pace of reading and writing. Supply him in advance with topic vocabulary with simple meanings which he can learn. Encourage note taking in lessons if possible and always have him complete a mind map or plan before longer written tasks. Break tasks into manable sections and have him complete then check in with the teacher. Utilise targeted questioning to ensure he is clear of what is expected. Matt will benefit from discussion and explaination.'
p2047
asg70
g392
sg72
I0
sg73
(lp2048
g1852
ag1853
asssg76
(dp2049
g5
(dp2050
g38
(lp2051
(dp2052
g81
S'M '
p2053
sg83
S'Meeting with Mrs Amenta, KAM, LG and HSM to discuss findings of assessment and observations. '
p2054
sg85
S'25-06-2012'
p2055
sg86
g27
sg87
S'2012-06-25 14:11'
p2056
sasg37
(lp2057
(dp2058
g91
S'Mich'
p2059
sg93
S'2012'
p2060
sg95
S'To sit appropriately during carpet time and put his hand up to share his ideas.'
p2061
sg81
S'M '
p2062
sg98
g27
sg87
S'2012-07-19 13:28'
p2063
sg100
g101
sg102
g27
sg86
g27
sa(dp2064
g91
S'Lent'
p2065
sg93
S'2013'
p2066
sg95
S'To volunteer answers during class discussions. '
p2067
sg81
S'M '
p2068
sg98
g27
sg87
S'2012-07-19 13:28'
p2069
sg100
g101
sg102
g27
sg86
g27
sa(dp2070
g91
S'Summer'
p2071
sg93
S'2013'
p2072
sg95
S'To focus on a task for at least 5 minutes at a time.'
p2073
sg81
S'M '
p2074
sg98
g27
sg87
S'2012-07-19 13:28'
p2075
sg100
g101
sg102
g27
sg86
g27
sa(dp2076
g91
S'Mich'
p2077
sg93
S'2013'
p2078
sg95
S'To develop self-checking skills to spot errors in written work, '
p2079
sg81
S'M '
p2080
sg98
g27
sg87
S'2013-07-16 09:41'
p2081
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp2082
(dp2083
g91
S'Summer'
p2084
sg81
S'M '
p2085
sg87
S'2012-07-16 14:08'
p2086
sg145
S'Numeracy support group. '
p2087
sg52
S'2012'
p2088
sg148
S'weekly(x1)'
p2089
sg86
g27
sg150
S'KM'
p2090
sa(dp2091
g91
S'Summer'
p2092
sg81
S'M '
p2093
sg87
S'2012-07-16 14:08'
p2094
sg145
S'Guided reading group. '
p2095
sg52
S'2012'
p2096
sg148
S'weekly(x1)'
p2097
sg86
g27
sg150
S'KM'
p2098
sa(dp2099
g91
S'Michaelmas'
p2100
sg81
S'M '
p2101
sg87
S'2013-07-08 10:09'
p2102
sg145
S'Maths support group'
p2103
sg52
S'2012'
p2104
sg148
S'weekly(x1)'
p2105
sg86
g27
sg150
S'KM'
p2106
sa(dp2107
g91
S'Lent'
p2108
sg81
S'M '
p2109
sg87
S'2013-07-08 10:09'
p2110
sg145
S'Maths support group'
p2111
sg52
S'2013'
p2112
sg148
S'weekly(x1)'
p2113
sg86
g27
sg150
S'KM'
p2114
sa(dp2115
g91
S'Summer'
p2116
sg81
S'M '
p2117
sg87
S'2013-07-08 10:09'
p2118
sg145
S'Maths support group'
p2119
sg52
S'2013'
p2120
sg148
S'weekly(x1)'
p2121
sg86
g27
sg150
S'KM'
p2122
sa(dp2123
g91
S'Michaelmas'
p2124
sg81
S'M '
p2125
sg87
S'2013-07-08 10:09'
p2126
sg145
S'Weekly handwriting support'
p2127
sg52
S'2012'
p2128
sg148
S'weekly(x1)'
p2129
sg86
g27
sg150
S'KM'
p2130
sa(dp2131
g91
S'Lent'
p2132
sg81
S'M '
p2133
sg87
S'2013-07-08 10:09'
p2134
sg145
S'Weekly handwriting support'
p2135
sg52
S'2013'
p2136
sg148
S'weekly(x1)'
p2137
sg86
g27
sg150
S'KM'
p2138
sa(dp2139
g91
S'Summer'
p2140
sg81
S'M '
p2141
sg87
S'2013-07-08 10:09'
p2142
sg145
S'Weekly handwriting support'
p2143
sg52
S'2013'
p2144
sg148
S'weekly(x1)'
p2145
sg86
g27
sg150
S'KM'
p2146
sassssS'Emily_George'
p2147
(dp2148
g3
(dp2149
g5
(dp2150
g7
I0
sg8
(lp2151
g184
ag185
asg12
S'Sen provision map for Emily George'
p2152
sg14
g15
sg16
(lp2153
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Emily George'
p2154
sg24
I0
sg25
(lp2155
g27
aS"Try to reduce distractions in the classroom by seating Emily at the front. Give her instructions one/two at a time and cue her in before issuing class instructions. Encourage her to ask for clarification.  Make sure she knows the schedule of the day to reduce the 'what's next' questions. When you give her a task, ensure she knows what is expected as she may rush and miss vital instruction."
p2156
asg29
(lp2157
g27
ag31
asg32
S'SEN,Provision map,Emily George'
p2158
sg34
(lp2159
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp2160
g41
S'17/3/2010'
p2161
sg43
S'George'
p2162
sg45
S'Action +'
p2163
sg47
S'2002-03-16'
p2164
sg49
S'EP Assessment Annie Mitchell. \r<br>NEALE 23.2.10 Anita Sawyer.'
p2165
sg51
g27
sg52
g1564
sg54
g27
sg56
S'Emily is a girl of average intelligence with specific auditory processing difficulties. She also has difficulties with working memory most noticabely auditory memory. At times Emily may find it challenging to concentrate and maintain attention. Emily may find it difficult to think and plan ahead, organise and control impulses and complete tasks. Emily will require support with phonic decoding as she has difficulties with her spelling. VCI 99 PRI 110 WMI 88 PSI 106'
p2166
sg58
g27
sg59
S'Emily'
p2167
sg61
S'25%'
p2168
sg62
S'6CS'
p2169
ssg64
I1
sg65
I0
sg66
(lp2170
S'Emily may find it difficult to think and plan ahead, organise and control impulses and complete tasks. Emily will require support with phonic decoding as she has difficulties with her spelling.'
p2171
aS'Try not to overload Emily with verbal instructions. Write the steps of the assignment or task on the board or a prompt sheet to help her work independently.  She would benefit from a task given in small chunks. With Emily, go for quality over quantity. Using strategies such as timers work if done in conjunction with a clear and concise target. Emily will require regular monitoring of what is required and prompting to do the next part of the task. She would benefit from Mind Maps to help her plan and organise her work. Aim to make learning meaningful for her and related if and when possible to her interests.  Utilise brain gym type activities to allow for movement and re-focusing if required.'
p2172
asg70
S'01-06-2010'
p2173
sg72
I0
sg73
(lp2174
S'Matty has some difficulties with fine motor skills, hypermobility, postural stability and hand eye coordination. To support his fine and gross motor skills, encourage general physical activities to support gross and fine motor skills such as PE games, playdoh, lego, colouring, cutting, ripping etc. Use a slope cushion to help with posture when sitting on a seat and encourage correct sitting at all times. Let him lye on his stomach when he is reading.'
p2175
ag27
asssg76
(dp2176
g5
(dp2177
g38
(lp2178
(dp2179
g81
S'F '
p2180
sg83
S'CMC, NB, RO and Mr George. Discussion of EP Report. '
p2181
sg85
S'01-11-2010'
p2182
sg86
g27
sg87
S'2010-11-01 09:27'
p2183
sa(dp2184
g81
S'F '
p2185
sg83
S'CMC, NB, FS, Mr and Mrs George. Discussion of settling in and target setting for Michaelmas Term. '
p2186
sg85
S'01-11-2010'
p2187
sg86
g27
sg87
S'2011-09-22 09:27'
p2188
sa(dp2189
g81
S'F '
p2190
sg83
S'NB, SAC and Mr George. Meeting to discuss progress and future intervention. '
p2191
sg85
S'04-07-2013'
p2192
sg86
g27
sg87
S'2013-07-04 13:56'
p2193
sasg37
(lp2194
(dp2195
g91
S'Lent'
p2196
sg93
S'2011'
p2197
sg95
S'To maintain focus and concentration for 10 minute periods in lessons.'
p2198
sg81
S'F '
p2199
sg98
g27
sg87
S'2010-12-14 09:29'
p2200
sg100
g101
sg102
g27
sg86
g27
sa(dp2201
g91
S'Mich'
p2202
sg93
S'2011'
p2203
sg95
S'To commence tasks without questioning their relevance.'
p2204
sg81
S'F '
p2205
sg98
g27
sg87
S'2011-10-10 09:29'
p2206
sg100
g101
sg102
g27
sg86
g27
sa(dp2207
g91
S'Lent'
p2208
sg93
S'2012'
p2209
sg95
S'To commence tasks confidently as instructed by teachers without questioning their relevance.'
p2210
sg81
S'F '
p2211
sg98
g27
sg87
S'2012-02-27 09:29'
p2212
sg100
g101
sg102
g27
sg86
g27
sa(dp2213
g91
S'Mich'
p2214
sg93
S'2012'
p2215
sg95
S'To display a more mature and independent approach to learning tasks through commencing without requiring additional teacher support.'
p2216
sg81
S'F '
p2217
sg98
g27
sg87
S'2012-07-19 13:56'
p2218
sg100
g101
sg102
g27
sg86
g27
sa(dp2219
g91
S'Lent'
p2220
sg93
S'2013'
p2221
sg95
S'To make sure my handwriting is neat and my work is presented to a high standard. '
p2222
sg81
S'F '
p2223
sg98
g27
sg87
S'2012-07-19 13:56'
p2224
sg100
g139
sg102
g27
sg86
g27
sa(dp2225
g91
S'Summer'
p2226
sg93
S'2013'
p2227
sg95
S'To make sure my handwriting is neat and my work is presented to a high standard. '
p2228
sg81
S'F '
p2229
sg98
g27
sg87
S'2012-07-19 13:56'
p2230
sg100
g101
sg102
g27
sg86
g27
sa(dp2231
g91
S'Mich'
p2232
sg93
S'2013'
p2233
sg95
S'1. To self regualte and edit my work through carefully checking my work before handing it in.                                                       '
p2234
sg81
S'F '
p2235
sg98
g27
sg87
S'2013-07-16 11:00'
p2236
sg100
g139
sg102
g27
sg86
g27
sa(dp2237
g91
S'Mich'
p2238
sg93
S'2013'
p2239
sg95
S'2. To make sure I have all of the equipment I need for the lesson before I leave my classroom.                                                    '
p2240
sg81
S'F '
p2241
sg98
g27
sg87
S'2013-07-16 11:00'
p2242
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp2243
(dp2244
g91
S'Michaelmas'
p2245
sg81
S'F '
p2246
sg87
S'2010-12-14 09:27'
p2247
sg145
S'Small group spelling support. '
p2248
sg52
S'2010'
p2249
sg148
S'weekly(x1)'
p2250
sg86
g27
sg150
S'NB'
p2251
sa(dp2252
g91
S'Lent'
p2253
sg81
S'F '
p2254
sg87
S'2010-12-14 09:27'
p2255
sg145
S'Small group spelling support. '
p2256
sg52
S'2011'
p2257
sg148
S'weekly(x1)'
p2258
sg86
g27
sg150
S'NB'
p2259
sa(dp2260
g91
S'Lent'
p2261
sg81
S'F '
p2262
sg87
S'2010-12-14 09:27'
p2263
sg145
S'Handwriting support. '
p2264
sg52
S'2011'
p2265
sg148
S'weekly(x1)'
p2266
sg86
g27
sg150
S'NB'
p2267
sa(dp2268
g91
S'Summer'
p2269
sg81
S'F '
p2270
sg87
S'2010-12-14 09:27'
p2271
sg145
S'Small group spelling support.'
p2272
sg52
S'2011'
p2273
sg148
S'weekly(x1)'
p2274
sg86
g27
sg150
S'NB'
p2275
sa(dp2276
g91
S'Summer'
p2277
sg81
S'F '
p2278
sg87
S'2010-12-14 09:27'
p2279
sg145
S'Handwriting support.'
p2280
sg52
S'2011'
p2281
sg148
S'weekly(x1)'
p2282
sg86
g27
sg150
S'NB'
p2283
sa(dp2284
g91
S'Lent'
p2285
sg81
S'F '
p2286
sg87
S'2011-02-18 14:01'
p2287
sg145
S'Learning Specialist lesson with Eleanor Barker. '
p2288
sg52
S'2011'
p2289
sg148
S'Monthly(x1)'
p2290
sg86
g27
sg150
g27
sa(dp2291
g91
S'Summer'
p2292
sg81
S'F '
p2293
sg87
S'2011-02-18 14:01'
p2294
sg145
S'Learning Specialist lesson with Eleanor Barker.'
p2295
sg52
S'2011'
p2296
sg148
S'weekly(x1)'
p2297
sg86
g27
sg150
g27
sa(dp2298
g91
S'Michaelmas'
p2299
sg81
S'F '
p2300
sg87
S'2011-02-18 14:01'
p2301
sg145
S'Learning Specialist lesson with Eleanor Barker.'
p2302
sg52
S'2011'
p2303
sg148
S'weekly(x1)'
p2304
sg86
g27
sg150
g27
sa(dp2305
g91
S'Lent'
p2306
sg81
S'F '
p2307
sg87
S'2012-01-04 14:01'
p2308
sg145
S'Learning Specialist lesson with Eleanor Barker.'
p2309
sg52
S'2012'
p2310
sg148
S'weekly(x1)'
p2311
sg86
g27
sg150
g27
sa(dp2312
g91
S'Summer'
p2313
sg81
S'F '
p2314
sg87
S'2012-04-12 14:01'
p2315
sg145
S'Learning Specialist lesson with Eleanor Barker.'
p2316
sg52
S'2012'
p2317
sg148
S'weekly(x1)'
p2318
sg86
g27
sg150
g27
sa(dp2319
g91
S'Summer'
p2320
sg81
S'F '
p2321
sg87
S'2012-05-11 14:01'
p2322
sg145
S'Handwriting support. '
p2323
sg52
S'2012'
p2324
sg148
S'weekly(x1)'
p2325
sg86
g27
sg150
g27
sa(dp2326
g91
S'Michaelmas'
p2327
sg81
S'F '
p2328
sg87
S'2013-07-08 16:22'
p2329
sg145
S'1:1 Specialist Teaching Clare Robinson'
p2330
sg52
S'2012'
p2331
sg148
S'weekly(x1)'
p2332
sg86
g27
sg150
g27
sa(dp2333
g91
S'Lent'
p2334
sg81
S'F '
p2335
sg87
S'2013-07-08 16:22'
p2336
sg145
S'1:1 Specialist Teaching Clare Robinson'
p2337
sg52
S'2013'
p2338
sg148
S'weekly(x1)'
p2339
sg86
g27
sg150
g27
sa(dp2340
g91
S'Summer'
p2341
sg81
S'F '
p2342
sg87
S'2013-07-08 16:22'
p2343
sg145
S'1:1 Specialist Teaching Clare Robinson'
p2344
sg52
S'2013'
p2345
sg148
S'weekly(x1)'
p2346
sg86
g27
sg150
g27
sassssS'William_Cook'
p2347
(dp2348
g3
(dp2349
g5
(dp2350
g7
I0
sg8
(lp2351
g184
ag185
asg12
S'Sen provision map for William Cook'
p2352
sg14
g15
sg16
(lp2353
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for William Cook'
p2354
sg24
I0
sg25
(lp2355
S'William may be tentative with his ideas and lack confidence in his skills.'
p2356
aS'William should be encouraged to have confidence in his abilities. He is a student with foundation skills which need to be refined to help him work at a more accurate level.'
p2357
asg29
(lp2358
g27
ag31
asg32
S'SEN,Provision map,William Cook'
p2359
sg34
(lp2360
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp2361
g41
S'19-01-2011'
p2362
sg43
S'Cook'
p2363
sg45
S'Action'
p2364
sg47
S'2001-04-15'
p2365
sg49
S'Series of assessments carried out by NB - 19/01/2011\r<br>Nina Elliott EP 15.09.2011'
p2366
sg51
g27
sg52
g1356
sg54
S'07-09-2011'
p2367
sg56
S'William has difficulties of a Dyslexic nature which impact on his spelling and comprehension skills. He has difficulties with listening comprehension and his reading speed is slow. Overall William has low average writting skills. William may be tentative with his ideas and lack confidence in his skills. He has a weak working memory especially when when he is required to mentally sequence and manipulate information. VR 96 NVR 98 WMI 77 PSI 100'
p2368
sg58
g27
sg59
S'William'
p2369
sg61
S'25%'
p2370
sg62
S'7CE'
p2371
ssg64
I1
sg65
I0
sg66
(lp2372
S'William has difficulties of a Dyslexic nature which impact on his spelling and comprehension skills. He has difficulties with listening comprehension and his reading speed is slow. Overall William has low average writting skills.'
p2373
aS"William's sight vocabulary will naturally increase through reading due and this must be encouraged daily. Additionally, provide William with key vocabulary. William will benefit from discussion when reading a comprehension passage. Where possible, put his learning in context and check for meaning and understanding through specific questioning. To avoid confusion, use concise language and be precise when explaining new concepts to him. William may tend to rush his work, please encourage careful proof reading. Sample pieces of work can be selected and any spelling errors should be identified, William can then look them up in the dictionary and correct his own work. This develops his visual store of words and increases self regulation and editing."
p2374
asg70
S'15-09-2011'
p2375
sg72
I0
sg73
(lp2376
g2175
ag27
asssg76
(dp2377
g5
(dp2378
g38
(lp2379
(dp2380
g81
S'M '
p2381
sg83
S'FC and Mrs Cook. Discussion of intial screening with NB due to assessment results. '
p2382
sg85
g27
sg86
g27
sg87
S'2010-11-29 10:53'
p2383
sa(dp2384
g81
S'M '
p2385
sg83
S'NB, Mr and Mrs Cook. Discussion of in school screening tests. Suggestion that William should be assessed by an EP. Parents agreed. '
p2386
sg85
g27
sg86
g27
sg87
S'2011-01-31 10:53'
p2387
sa(dp2388
g81
S'M '
p2389
sg83
S'Summer report LC. '
p2390
sg85
g27
sg86
g27
sg87
S'2011-07-11 10:53'
p2391
sa(dp2392
g81
S'M '
p2393
sg83
S'NB, Mr and Mrs Cook. Discussion of EP report. '
p2394
sg85
S'11-11-2011'
p2395
sg86
g27
sg87
S'2011-11-11 10:53'
p2396
sasg37
(lp2397
(dp2398
g91
S'Mich'
p2399
sg93
S'2011'
p2400
sg95
S'To be proactive in asking questions in class especially when he is not sure of something.                                                                                  '
p2401
sg81
S'M '
p2402
sg98
g27
sg87
S'2011-09-30 16:48'
p2403
sg100
g139
sg102
g27
sg86
g27
sa(dp2404
g91
S'Mich'
p2405
sg93
S'2011'
p2406
sg95
S'To display confidence in his own abilities and be willing to share his ideas with others in lessons.'
p2407
sg81
S'M '
p2408
sg98
g27
sg87
S'2011-09-30 16:48'
p2409
sg100
g101
sg102
g27
sg86
g27
sa(dp2410
g91
S'Lent'
p2411
sg93
S'2012'
p2412
sg95
S'To be proactive in asking questions in class especially when he is not sure of something.                                                                               '
p2413
sg81
S'M '
p2414
sg98
g27
sg87
S'2012-01-01 16:48'
p2415
sg100
g101
sg102
g27
sg86
g27
sa(dp2416
g91
S'Lent'
p2417
sg93
S'2012'
p2418
sg95
S' To display confidence in his own abilities and be willing to share his ideas with others in lessons.                                                                     '
p2419
sg81
S'M '
p2420
sg98
g27
sg87
S'2012-01-01 16:48'
p2421
sg100
g101
sg102
g27
sg86
g27
sa(dp2422
g91
S'Mich'
p2423
sg93
S'2012'
p2424
sg95
S'To organise myself each morning and only carry what I need for morning lessons.'
p2425
sg81
S'M '
p2426
sg98
g27
sg87
S'2012-07-19 13:27'
p2427
sg100
g101
sg102
g27
sg86
g27
sa(dp2428
g91
S'Lent'
p2429
sg93
S'2013'
p2430
sg95
S'To put my hand up in class and ask for help when I am not sure of something. '
p2431
sg81
S'M '
p2432
sg98
g27
sg87
S'2012-07-19 13:27'
p2433
sg100
g101
sg102
g27
sg86
g27
sa(dp2434
g91
S'Summer'
p2435
sg93
S'2013'
p2436
sg95
S'To challenge myself in lessons by constructing more sophisticated written and spoken answers. '
p2437
sg81
S'M '
p2438
sg98
g27
sg87
S'2012-07-19 13:27'
p2439
sg100
g139
sg102
g27
sg86
g27
sa(dp2440
g91
S'Mich'
p2441
sg93
S'2013'
p2442
sg95
S'To challenge myself in lessons by constructing more sophisticated written and spoken answers. '
p2443
sg81
S'M '
p2444
sg98
g27
sg87
S'2012-07-19 13:27'
p2445
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp2446
(dp2447
g91
S'Michaelmas'
p2448
sg81
S'M '
p2449
sg87
S'2011-01-31 10:53'
p2450
sg145
S'1:1 Literacy support with Lindsey Copeman. '
p2451
sg52
S'2011'
p2452
sg148
S'weekly(x1)'
p2453
sg86
g27
sg150
g27
sa(dp2454
g91
S'Lent'
p2455
sg81
S'M '
p2456
sg87
S'2012-01-10 10:53'
p2457
sg145
S'1:1 Literacy support with Lindsey Copeman.'
p2458
sg52
S'2012'
p2459
sg148
S'weekly(x1)'
p2460
sg86
g27
sg150
g27
sa(dp2461
g91
S'Summer'
p2462
sg81
S'M '
p2463
sg87
S'2012-01-10 10:53'
p2464
sg145
S'1:1 Literacy support with Lindsey Copeman.'
p2465
sg52
S'2012'
p2466
sg148
S'weekly(x1)'
p2467
sg86
g27
sg150
g27
sa(dp2468
g91
S'Michaelmas'
p2469
sg81
S'M '
p2470
sg87
S'2013-07-08 16:17'
p2471
sg145
S'1:1 Literacy support with LC'
p2472
sg52
S'2012'
p2473
sg148
S'weekly(x1)'
p2474
sg86
g27
sg150
g27
sa(dp2475
g91
S'Lent'
p2476
sg81
S'M '
p2477
sg87
S'2013-07-08 16:17'
p2478
sg145
S'1:1 Literacy support with LC'
p2479
sg52
S'2013'
p2480
sg148
S'weekly(x1)'
p2481
sg86
g27
sg150
g27
sa(dp2482
g91
S'Summer'
p2483
sg81
S'M '
p2484
sg87
S'2013-07-08 16:17'
p2485
sg145
S'1:1 Literacy support with LC'
p2486
sg52
S'2013'
p2487
sg148
S'weekly(x1)'
p2488
sg86
g27
sg150
g27
sassssS'Jessica_Warne'
p2489
(dp2490
g3
(dp2491
g5
(dp2492
g7
I0
sg8
(lp2493
g1828
ag1829
asg12
S'Sen provision map for Jessica Warne'
p2494
sg14
g15
sg16
(lp2495
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Jessica Warne'
p2496
sg24
I0
sg25
(lp2497
g27
ag1834
asg29
(lp2498
g27
ag31
asg32
S'SEN,Provision map,Jessica Warne'
p2499
sg34
(lp2500
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp2501
g41
S'01/12/2008'
p2502
sg43
S'Warne'
p2503
sg45
S'Action +'
p2504
sg47
S'2000-02-12'
p2505
sg49
S'EP Report K Sharkey\r<br>EP Nicola Bishop 20.03.12'
p2506
sg51
g27
sg52
g935
sg54
g27
sg56
S'Jessica has a specific learning difficulty. She has particular difficulty with auditory processing, spatial skills and immediate recall. These difficulties will affect listening attention and understanding. Her difficulties in processing information, will impact on her ability to express her ideas fluently. Her reading speed is very slow and her writing speed is below average. Gaining meaning from text will be a challenge for Jessica. VR 94  NVR 90 WMI 97 PSI 91 FSIQ 92'
p2507
sg58
g27
sg59
S'Jessica'
p2508
sg61
S'25%'
p2509
sg62
S'8CN'
p2510
ssg64
I1
sg65
I0
sg66
(lp2511
S'Jessica has a specific learning difficulty. She has particular difficulty with auditory processing, spatial skills and immediate recall. These difficulties will affect listening attention and understanding. Her difficulties in processing information, will impact on her ability to express her ideas fluently. Her reading speed is very slow and her writing speed is below average. Gaining meaning from text will be a challenge for Jessica.'
p2512
aS'Class teachers and support staff should read vital information to her. Additionally, Jessica should be encouraged to seek help from peers if she feels comfortable to do so. Staff should ensure that copying is kept to a minimum and keep instructions short and repeat often. She may ask for repetition and clarification of questions and instructions. Jessica should be close to the teacher and be given extra time for reading and processing tasks. All staff should aim to boost self-esteem and encourage gentle risk taking. When marking work samples, teacher should adopt a targeted marking approach, focusing specifically on a particular skill, do not overwhelm. Multi-sensory teaching methods will benefit her memory and learning. Do not presume Jessica has understood information or instructions and especially reinforce any specific details you feel she should take from the lesson. Pre-prepared notes can be make in dot form and glued into her book.'
p2513
asg70
S'20-03-2012'
p2514
sg72
I0
sg73
(lp2515
g1852
ag1853
asssg76
(dp2516
g5
(dp2517
g38
(lp2518
(dp2519
g81
S'F '
p2520
sg83
S'To display confidence in her own abilities and be willing to share her ideas with others in lessons. this target. '
p2521
sg85
S'Lent'
p2522
sg86
g27
sg87
S'2010-12-14 16:03'
p2523
sa(dp2524
g81
S'F '
p2525
sg83
S'Telephone conversation between Mrs Warne and NB. NB discussed the need to update EP report. Mrs Warne will be doing this as advised. '
p2526
sg85
g27
sg86
g27
sg87
S'2012-01-11 15:42'
p2527
sa(dp2528
g81
S'F '
p2529
sg83
S'CAE, NB, MR and MRs Warne. Discussion regaring EP report and possible schooling options. Monmoth boarding was agreed. '
p2530
sg85
g27
sg86
g27
sg87
S'2012-05-02 15:42'
p2531
sasg37
(lp2532
(dp2533
g91
S'Lent'
p2534
sg93
S'2011'
p2535
sg95
S'To display confidence in her own abilities and be willing to share her ideas with others in lessons. this target. '
p2536
sg81
S'F '
p2537
sg98
g27
sg87
S'2010-12-14 16:03'
p2538
sg100
g139
sg102
g27
sg86
g27
sa(dp2539
g91
S'Lent'
p2540
sg93
S'2012'
p2541
sg95
S'To be more proactive in asking for support if required. '
p2542
sg81
S'F '
p2543
sg98
g27
sg87
S'2012-03-04 13:12'
p2544
sg100
g101
sg102
g27
sg86
g27
sa(dp2545
g91
S'Mich'
p2546
sg93
S'2012'
p2547
sg95
S'Display a greater level of independence and more belief in her own abilities.'
p2548
sg81
S'F '
p2549
sg98
g27
sg87
S'2012-07-19 13:11'
p2550
sg100
g101
sg102
g27
sg86
g27
sa(dp2551
g91
S'Lent'
p2552
sg93
S'2013'
p2553
sg95
S'1. To read the question 2 times before beginning my answer.                                                                                   '
p2554
sg81
S'F '
p2555
sg98
g27
sg87
S'2012-07-19 13:11'
p2556
sg100
g101
sg102
g27
sg86
g27
sa(dp2557
g91
S'Lent'
p2558
sg93
S'2013'
p2559
sg95
S'2. To read and check my answer before submitting to the teacher or asking for help.                                                                          '
p2560
sg81
S'F '
p2561
sg98
g27
sg87
S'2012-07-19 13:11'
p2562
sg100
g101
sg102
g27
sg86
g27
sa(dp2563
g91
S'Summer'
p2564
sg93
S'2013'
p2565
sg95
S'To put my hand up at least 2 times in every lesson to either ask a question or share my ideas. '
p2566
sg81
S'F '
p2567
sg98
g27
sg87
S'2012-07-19 13:11'
p2568
sg100
g101
sg102
g27
sg86
g27
sasg36
(lp2569
(dp2570
g91
S'Lent'
p2571
sg81
S'F '
p2572
sg87
S'2012-02-01 16:03'
p2573
sg145
S'1:1 literacy lesson with Lindsey Copeman.'
p2574
sg52
S'2012'
p2575
sg148
S'weekly(x1)'
p2576
sg86
g27
sg150
g27
sa(dp2577
g91
S'Summer'
p2578
sg81
S'F '
p2579
sg87
S'2012-02-01 16:03'
p2580
sg145
S'1:1 literacy lesson with Lindsey Copeman.'
p2581
sg52
S'2012'
p2582
sg148
S'weekly(x1)'
p2583
sg86
g27
sg150
g27
sa(dp2584
g91
S'Summer'
p2585
sg81
S'F '
p2586
sg87
S'2012-04-14 16:03'
p2587
sg145
S'1:1 literacy lesson with Lindsey Copeman.'
p2588
sg52
S'2012'
p2589
sg148
S'weekly(x1)'
p2590
sg86
g27
sg150
g27
sassssS'Hugo_Flower'
p2591
(dp2592
g3
(dp2593
g5
(dp2594
g7
I0
sg8
(lp2595
S'Max has some difficulty with mathematical reasoning most specifically worded problems.'
p2596
aS' In numeracy, provide a mathematics dictionary which explains the language in written questions such as all together = addition. Always encourage Max to write down his thinking and not rely on his memory.'
p2597
asg12
S'Sen provision map for Hugo Flower'
p2598
sg14
g15
sg16
(lp2599
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Hugo Flower'
p2600
sg24
I0
sg25
(lp2601
g27
aS'Max has good visual memory skills and as such this should be utilised in class where possible to support his learning, such as visual timetables, visual cues and visual prompts such as templates to help him follow what is required. Mind mapping and planning sheets will also help his organisation of thoughts.  Provide short and clear instructions and always cue his name when a specific instruction in needed in the classroom.  Praise and rewards will help Max stay focused and on task.'
p2602
asg29
(lp2603
g27
ag31
asg32
S'SEN,Provision map,Hugo Flower'
p2604
sg34
(lp2605
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp2606
g41
S'07-09-2010'
p2607
sg43
S'Flower'
p2608
sg45
S'Action'
p2609
sg47
S'1999-10-30'
p2610
sg49
S'EP Beverley Steffert'
p2611
sg51
S'Both'
p2612
sg52
g935
sg54
g27
sg56
S"Hugo is a student of high average verbal reasoning ability. His working memory is average (99) and his speed of processing is low average (91). Hugo's weak working memory will affect his ability to sequence and plan his thoughts. His spelling and written expression is significantly below his intellectual level. Hugo has a specific spelling difficulty and fits the dyslexic/dysgraphic pattern. VCI 116 PRI 119 WMI 99 PSI 91"
p2613
sg58
g27
sg59
S'Hugo'
p2614
sg61
S'25%'
p2615
sg62
S'8CN'
p2616
ssg64
I1
sg65
I0
sg66
(lp2617
S'Hugo has a specific spelling difficulty and fits the dyslexic/dysgraphic pattern.'
p2618
aS"Hugo's ability to note take and record important information is weak therefore teachers should where possible check that he has recorded necessary information. Hugo will need support orgainising his thoughts, graphic organisers are a good way to do this as are diagrams, pictures, flow charts etc.Hugo should be seated near the front of the class and away from possible distractions. Wrok should be presented in small chunks and he should be given the opportunity to have breaks to ensure he can remain focused throughout the lesson. Hugo requires a positive teaching style with encourgement and praise. Teacher's should mark Hugo's work carefully to ensure his mistakes are due to conceptualisation not simple sequencing and organisational errors. When spelling, Hugo should be encouraged to think about the correct spelling of the word rather than just phonetically sounding and spelling as he hears the sounds."
p2619
asg70
S'07-09-2010'
p2620
sg72
I0
sg73
(lp2621
g211
ag212
asssg76
(dp2622
g5
(dp2623
g38
(lp2624
(dp2625
g81
S'M '
p2626
sg83
S'NB and Mrs Flower. Extra time discussion in examinations. '
p2627
sg85
S'19-05-2011'
p2628
sg86
g27
sg87
S'2011-05-19 16:19'
p2629
sa(dp2630
g81
S'M '
p2631
sg83
S'Phone conversation between NB and Mrs Flower regarding re assessment. NB to send through contact details of EPs. NB commented that Hugo may not get extra time based on new requirements. Mrs Flower to decide and arrange as required.'
p2632
sg85
g27
sg86
g27
sg87
S'2012-07-16 16:19'
p2633
sasg37
(lp2634
(dp2635
g91
S'Lent'
p2636
sg93
S'2011'
p2637
sg95
S'To adopt a more optomistic thinking style to ensure he does not develop a pessimistic attitude. '
p2638
sg81
S'M '
p2639
sg98
g27
sg87
S'2010-12-14 16:19'
p2640
sg100
g101
sg102
g27
sg86
g27
sa(dp2641
g91
S'Mich'
p2642
sg93
S'2011'
p2643
sg95
S'To be more proactive in asking for support if required.'
p2644
sg81
S'M '
p2645
sg98
g27
sg87
S'2011-09-30 16:19'
p2646
sg100
g101
sg102
g27
sg86
g27
sa(dp2647
g91
S'Lent'
p2648
sg93
S'2012'
p2649
sg95
S'To be proactive in asking questions in class and in sharing his knowledge of subjects. '
p2650
sg81
S'M '
p2651
sg98
g27
sg87
S'2011-09-30 16:19'
p2652
sg100
g101
sg102
g27
sg86
g27
sa(dp2653
g91
S'Mich'
p2654
sg93
S'2012'
p2655
sg95
S'To concentrate and focus more consistently in all lessons.'
p2656
sg81
S'M '
p2657
sg98
g27
sg87
S'2012-07-19 13:47'
p2658
sg100
g101
sg102
g27
sg86
g27
sa(dp2659
g91
S'Lent'
p2660
sg93
S'2013'
p2661
sg95
S'To focus and concentrate for 5-10 minute blocks in lessons. '
p2662
sg81
S'M '
p2663
sg98
g27
sg87
S'2012-07-19 13:47'
p2664
sg100
g101
sg102
g27
sg86
g27
sa(dp2665
g91
S'Summer'
p2666
sg93
S'2013'
p2667
sg95
S'To focus on my learning and ensure I complete all tasks to the best of my ability.  '
p2668
sg81
S'M '
p2669
sg98
g27
sg87
S'2012-07-19 13:47'
p2670
sg100
g101
sg102
g27
sg86
g27
sassssS'Hector_Christie-Miller'
p2671
(dp2672
g3
(dp2673
g5
(dp2674
g7
I0
sg8
(lp2675
g10
ag11
asg12
S'Sen provision map for Hector Christie-Miller'
p2676
sg14
g15
sg16
(lp2677
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Hector Christie-Miller'
p2678
sg24
I1
sg25
(lp2679
g27
aS'Hector would benefit from developing his word-processing and laptop skills.'
p2680
asg29
(lp2681
g27
ag31
asg32
S'SEN,Provision map,Hector Christie-Miller'
p2682
sg34
(lp2683
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp2684
g41
S'29/04/2010'
p2685
sg43
S'Christie-Miller'
p2686
sg45
S'Action'
p2687
sg47
S'2002-06-11'
p2688
sg49
S'EP Report Nina Elliott\r<br>Julia Terteryan OT 3.9.10\r<br>Touch Typing - Simone Shaw - Michaelmas 2011'
p2689
sg51
g27
sg52
g1564
sg54
g27
sg56
S'Hector is left handed. Hector has very superior verbal comprehension (99.8%) and Superior nonverbal comprehension (92%). Hector is relatively slow to process visual information.  His presentation skills are to be poor.  Hector struggles with his organisation skills and has difficulties with his handwriting skills. He has difficulties with perceptual organisation and fine motor coordination. His abilities to sustain concentration and focus are a weakness relative to his verbal and non verbal skills. Hector has mild specific learning difficulties. He has mild motor planning difficulties. VCI 144 PRI 121 WMI 113 PSI 112 FSIQ 131'
p2690
sg58
g27
sg59
S'Hector'
p2691
sg61
S'15%'
p2692
sg62
S'6CN'
p2693
ssg64
I1
sg65
I0
sg66
(lp2694
S'Hector is relatively slow to process visual information.  His presentation skills are to be poor.  Hector struggles with his organisation skills and has difficulties with his handwriting skills. He has difficulties with perceptual organisation and fine motor coordination.'
p2695
aS'Use sequencing and task-analysis, which entails breaking the task down into parts and then making the parts into the whole, while at the same time providing step-by-step prompts. Use rhythm or music to help memorise. Provide a template for the setting out of a task in advance. Hector would benefit from being given handouts in lessons. When handwriting, ensure Hector tilts his paper a quarter turn so he can see what he is writing. Please allow Hector to use a writing slope.'
p2696
asg70
S'28-04-2010'
p2697
sg72
I0
sg73
(lp2698
S'Ellie suffers from hyper mobility, very bendy joints. Due to this Ellie will fatigue more easily. Ellie has a nut allergy and an Epipen should be available as per school policy.'
p2699
aS"Class teachers should be aware of Ellie's difficulties and that she is prone to fatigue easily. Protein bars are kept in the classroom and should be given as required. Please be aware that physical activities will make Ellie more fatigued. She may also become more fatigued as the day progresses."
p2700
asssg76
(dp2701
g5
(dp2702
g38
(lp2703
(dp2704
g81
S'M '
p2705
sg83
S'Hector has completed a touch typing course and now uses a laptop. '
p2706
sg85
g27
sg86
g27
sg87
S'2010-11-12 14:46'
p2707
sasg37
(lp2708
(dp2709
g91
S'Lent'
p2710
sg93
S'2011'
p2711
sg95
S'To commence activities independently within 2 minutes once instruction and support has been given.'
p2712
sg81
S'M '
p2713
sg98
g27
sg87
S'2010-11-12 14:46'
p2714
sg100
g101
sg102
g27
sg86
g27
sa(dp2715
g91
S'Mich'
p2716
sg93
S'2011'
p2717
sg95
S'To use his laptop for all appropriate pieces of work.'
p2718
sg81
S'M '
p2719
sg98
g27
sg87
S'2011-10-10 14:46'
p2720
sg100
g101
sg102
g27
sg86
g27
sa(dp2721
g91
S'Lent'
p2722
sg93
S'2012'
p2723
sg95
S'To develop a greater level of independence as a learner especially in reducing the amount of times teachers must reming him to focus and concentrate on his work.'
p2724
sg81
S'M '
p2725
sg98
g27
sg87
S'2012-02-27 14:46'
p2726
sg100
g101
sg102
g27
sg86
g27
sa(dp2727
g91
S'Mich'
p2728
sg93
S'2012'
p2729
sg95
S'To increase focus and concentration in lessons through ensuring peer discussions remain relevant to the lesson objective.'
p2730
sg81
S'M '
p2731
sg98
g27
sg87
S'2012-07-19 13:35'
p2732
sg100
g139
sg102
g27
sg86
g27
sa(dp2733
g91
S'Lent'
p2734
sg93
S'2013'
p2735
sg95
S'To increase focus and concentration in lessons through ensuring peer discussions remain relevant to the lesson objective.'
p2736
sg81
S'M '
p2737
sg98
g27
sg87
S'2012-07-19 13:35'
p2738
sg100
g101
sg102
g27
sg86
g27
sa(dp2739
g91
S'Lent'
p2740
sg93
S'2013'
p2741
sg95
S'2. To put my hand up to ask a question or share an idea. (75% of the time). '
p2742
sg81
S'M '
p2743
sg98
g27
sg87
S'2012-07-19 13:35'
p2744
sg100
g139
sg102
g27
sg86
g27
sa(dp2745
g91
S'Summer'
p2746
sg93
S'2013'
p2747
sg95
S'2. To put my hand up to ask a question or share an idea. (75% of the time). '
p2748
sg81
S'M '
p2749
sg98
g27
sg87
S'2012-07-19 13:35'
p2750
sg100
g139
sg102
g27
sg86
g27
sa(dp2751
g91
S'Summer'
p2752
sg93
S'2013'
p2753
sg95
S'2. To listen respectfully to the views of others in class discussions. '
p2754
sg81
S'M '
p2755
sg98
g27
sg87
S'2012-07-19 13:35'
p2756
sg100
g139
sg102
g27
sg86
g27
sa(dp2757
g91
S'Mich'
p2758
sg93
S'2013'
p2759
sg95
S'1. To put my hand up to ask a question or share an idea. (75% of the time).          '
p2760
sg81
S'M '
p2761
sg98
g27
sg87
S'2013-07-16 10:57'
p2762
sg100
g139
sg102
g27
sg86
g27
sa(dp2763
g91
S'Mich'
p2764
sg93
S'2013'
p2765
sg95
S'2. To listen respectfully to the views of others in class discussions. '
p2766
sg81
S'M '
p2767
sg98
g27
sg87
S'2013-07-16 10:57'
p2768
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp2769
(dp2770
g91
S'Michaelmas'
p2771
sg81
S'M '
p2772
sg87
S'2010-12-14 09:44'
p2773
sg145
S'Handwriting group'
p2774
sg52
S'2010'
p2775
sg148
S'weekly(x1)'
p2776
sg86
g27
sg150
S'NB'
p2777
sa(dp2778
g91
S'Lent'
p2779
sg81
S'M '
p2780
sg87
S'2010-12-14 09:44'
p2781
sg145
S'Handwriting group'
p2782
sg52
S'2011'
p2783
sg148
S'weekly(x1)'
p2784
sg86
g27
sg150
S'NB'
p2785
sa(dp2786
g91
S'Summer'
p2787
sg81
S'M '
p2788
sg87
S'2010-12-14 09:44'
p2789
sg145
S'Handwriting group'
p2790
sg52
S'2011'
p2791
sg148
S'weekly(x1)'
p2792
sg86
g27
sg150
S'NB'
p2793
sassssS'Josie_Gordon'
p2794
(dp2795
g3
(dp2796
g5
(dp2797
g7
I0
sg8
(lp2798
g1548
ag1549
asg12
S'Sen provision map for Josie Gordon'
p2799
sg14
g15
sg16
(lp2800
g27
ag2032
asg20
g21
sg22
S'Special Needs Provision Map for Josie Gordon'
p2801
sg24
I0
sg25
(lp2802
g27
aS'Always support Sophie with social interaction and help her through any process which appears to be difficult. An extensive list of suggestions to support Sophie socially can be found on her EP report.'
p2803
asg29
(lp2804
S'Sophie at times is reluctant to take on the advice of staff.'
p2805
ag27
asg32
S'SEN,Provision map,Josie Gordon'
p2806
sg34
(lp2807
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp2808
g41
g27
sg43
S'Gordon'
p2809
sg45
g27
sg47
S'2005-09-04'
p2810
sg49
g27
sg51
g27
sg52
g1470
sg54
g27
sg56
g27
sg58
g27
sg59
S'Josie'
p2811
sg61
g27
sg62
S'2CS'
p2812
ssg64
I0
sg65
I0
sg66
(lp2813
S"Sophie has some very mild specific learning difficulties which impact her coordination and organisation skills. Her ability to process visual information quickly is weak in comparison to her VR and NVR abilities. Sophie's difficulties with organisation of work may contribute to her work being crossed out and presented in an untidy manner."
p2814
aS'Any opportunity to help Sophie structure her work is beneficial throught the use of templates, mind maps, story lines etc. Encourage Sophie to think about her work before commencing especially written tasks. Provide opportunity to discuss her ideas and prompt her with why? how? style questions if required. Check in regularly to ensure she is focused and on task. Set targets with Sophie and provide detailed positive praise when appropriate.'
p2815
asg70
g392
sg72
I0
sg73
(lp2816
S'Sophie has profound hearing loss in her right ear and a predominately low frequency, mild to moderate hearing loss on the left. This is a permanent deadness caused by damage to the auditory nerve or cochlea. Sophie was issued with hearing aids in 2008. Sophie only wears hearing aids in her left ear.'
p2817
aS'Additionally being aware of background noise will also support Sophie. Present short segments of information interspersed with tasks to maintain her attention.  Avoid talking whilst writing on the board, walking around or when in a shadow. Indicate change of topic in discussion or if something new is introduced incidentally.'
p2818
asssg76
(dp2819
g5
(dp2820
g38
(lp2821
(dp2822
g81
S'F '
p2823
sg83
S'Meeting with KAM, HSM, LG and Mrs Gordon. Discussion of observation and assessment. '
p2824
sg85
g27
sg86
g27
sg87
S'2012-05-21 09:19'
p2825
sasg37
(lp2826
(dp2827
g91
S'Lent'
p2828
sg93
S'2012'
p2829
sg95
S'To write a simple sentence including a capital letter and a full stop.'
p2830
sg81
S'F '
p2831
sg98
g27
sg87
S'2011-09-21 19:19'
p2832
sg100
g139
sg102
g27
sg86
S'LGA'
p2833
sa(dp2834
g91
g27
sg93
g27
sg95
S'To recognise numbers to 20'
p2835
sg81
S'F '
p2836
sg98
g27
sg87
S'2011-09-23 07:26'
p2837
sg100
g139
sg102
g27
sg86
S'LGA'
p2838
sa(dp2839
g91
S'Mich'
p2840
sg93
S'2012'
p2841
sg95
S'To write a simple sentence including a capital letter and a full stop.'
p2842
sg81
S'F '
p2843
sg98
g27
sg87
S'2012-07-19 13:48'
p2844
sg100
g101
sg102
g27
sg86
g27
sa(dp2845
g91
S'Mich'
p2846
sg93
S'2012'
p2847
sg95
S'Numeracy: Complete simple addition and subtraction questions using a number line.'
p2848
sg81
S'F '
p2849
sg98
g27
sg87
S'2012-07-19 13:48'
p2850
sg100
g101
sg102
g27
sg86
g27
sa(dp2851
g91
S'Lent'
p2852
sg93
S'2013'
p2853
sg95
S'Literacy: to use a word bank to help with spelling.                                                                     '
p2854
sg81
S'F '
p2855
sg98
g27
sg87
S'2012-07-19 13:48'
p2856
sg100
g101
sg102
g27
sg86
g27
sa(dp2857
g91
S'Lent'
p2858
sg93
S'2013'
p2859
sg95
S'Numeracy: to complete addition and subtraction questions without an aid.'
p2860
sg81
S'F '
p2861
sg98
g27
sg87
S'2012-07-19 13:48'
p2862
sg100
g101
sg102
g27
sg86
g27
sa(dp2863
g91
S'Summer'
p2864
sg93
S'2013'
p2865
sg95
S'Literacy: to use a word bank to help with spelling.                                                                                 '
p2866
sg81
S'F '
p2867
sg98
g27
sg87
S'2012-07-19 13:48'
p2868
sg100
g101
sg102
g27
sg86
g27
sa(dp2869
g91
S'Summer'
p2870
sg93
S'2013'
p2871
sg95
S' Numeracy: to complete addition and subtraction questions independently (TU +TU)                                                           '
p2872
sg81
S'F '
p2873
sg98
g27
sg87
S'2012-07-19 13:48'
p2874
sg100
g101
sg102
g27
sg86
g27
sa(dp2875
g91
S'Mich'
p2876
sg93
S'2013'
p2877
sg95
S'Literacy: to use greater story language and description in her writing. '
p2878
sg81
S'F '
p2879
sg98
g27
sg87
S'2012-07-19 13:48'
p2880
sg100
g139
sg102
g27
sg86
g27
sa(dp2881
g91
S'Mich'
p2882
sg93
S'2013'
p2883
sg95
S'Numeracy: to recall times tables facts at random'
p2884
sg81
S'F '
p2885
sg98
g27
sg87
S'2012-07-19 13:48'
p2886
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp2887
(dp2888
g91
S'Michaelmas'
p2889
sg81
S'F '
p2890
sg87
S'2011-09-11 09:19'
p2891
sg145
S'Spelling support group. '
p2892
sg52
S'2011'
p2893
sg148
S'weekly(x1)'
p2894
sg86
g27
sg150
S'AST'
p2895
sa(dp2896
g91
S'Lent'
p2897
sg81
S'F '
p2898
sg87
S'2012-01-01 09:19'
p2899
sg145
S'Spelling support group. '
p2900
sg52
S'2012'
p2901
sg148
S'weekly(x1)'
p2902
sg86
g27
sg150
S'AST'
p2903
sa(dp2904
g91
S'Lent'
p2905
sg81
S'F '
p2906
sg87
S'2012-01-01 09:19'
p2907
sg145
S'In class numeracy support. '
p2908
sg52
S'2012'
p2909
sg148
S'weekly(x1)'
p2910
sg86
g27
sg150
S'AST'
p2911
sa(dp2912
g91
S'Lent'
p2913
sg81
S'F '
p2914
sg87
S'2012-01-01 09:19'
p2915
sg145
S'Handwriting support group. '
p2916
sg52
S'2012'
p2917
sg148
S'Monthly(x3)'
p2918
sg86
g27
sg150
S'NB'
p2919
sa(dp2920
g91
S'Summer'
p2921
sg81
S'F '
p2922
sg87
S'2012-04-04 09:19'
p2923
sg145
S'Handwriting support group. '
p2924
sg52
S'2012'
p2925
sg148
S'Monthly(x3)'
p2926
sg86
g27
sg150
S'NB'
p2927
sa(dp2928
g91
S'Summer'
p2929
sg81
S'F '
p2930
sg87
S'2012-05-12 09:19'
p2931
sg145
S'Spelling support group. '
p2932
sg52
S'2012'
p2933
sg148
S'weekly(x1)'
p2934
sg86
g27
sg150
S'KM'
p2935
sa(dp2936
g91
S'Summer'
p2937
sg81
S'F '
p2938
sg87
S'2012-05-12 09:19'
p2939
sg145
S'In class numeracy support. '
p2940
sg52
S'2012'
p2941
sg148
S'weekly(x1)'
p2942
sg86
g27
sg150
S'KM'
p2943
sa(dp2944
g91
S'Michaelmas'
p2945
sg81
S'F '
p2946
sg87
S'2013-07-08 10:06'
p2947
sg145
S'Maths support group'
p2948
sg52
S'2012'
p2949
sg148
S'weekly(x1)'
p2950
sg86
g27
sg150
S'KM'
p2951
sa(dp2952
g91
S'Lent'
p2953
sg81
S'F '
p2954
sg87
S'2013-07-08 10:06'
p2955
sg145
S'Maths support group'
p2956
sg52
S'2013'
p2957
sg148
S'weekly(x1)'
p2958
sg86
g27
sg150
S'KM'
p2959
sa(dp2960
g91
S'Summer'
p2961
sg81
S'F '
p2962
sg87
S'2013-07-08 10:06'
p2963
sg145
S'Maths support group'
p2964
sg52
S'2013'
p2965
sg148
S'weekly(x1)'
p2966
sg86
g27
sg150
S'KM'
p2967
sa(dp2968
g91
S'Michaelmas'
p2969
sg81
S'F '
p2970
sg87
S'2013-07-08 10:06'
p2971
sg145
S'Weekly handwriting support'
p2972
sg52
S'2012'
p2973
sg148
S'weekly(x1)'
p2974
sg86
g27
sg150
S'KM'
p2975
sa(dp2976
g91
S'Lent'
p2977
sg81
S'F '
p2978
sg87
S'2013-07-08 10:06'
p2979
sg145
S'Weekly handwriting support'
p2980
sg52
S'2013'
p2981
sg148
S'weekly(x1)'
p2982
sg86
g27
sg150
S'KM'
p2983
sa(dp2984
g91
S'Summer'
p2985
sg81
S'F '
p2986
sg87
S'2013-07-08 10:06'
p2987
sg145
S'Weekly handwriting support'
p2988
sg52
S'2013'
p2989
sg148
S'weekly(x1)'
p2990
sg86
g27
sg150
S'KM'
p2991
sa(dp2992
g91
S'Michaelmas'
p2993
sg81
S'F '
p2994
sg87
S'2013-07-08 10:06'
p2995
sg145
S'1:1 Toe by Toe'
p2996
sg52
S'2012'
p2997
sg148
S'weekly(x1)'
p2998
sg86
g27
sg150
S'KM'
p2999
sa(dp3000
g91
S'Lent'
p3001
sg81
S'F '
p3002
sg87
S'2013-07-08 10:06'
p3003
sg145
S'1:1 Toe by Toe'
p3004
sg52
S'2013'
p3005
sg148
S'weekly(x1)'
p3006
sg86
g27
sg150
S'KM'
p3007
sa(dp3008
g91
S'Summer'
p3009
sg81
S'F '
p3010
sg87
S'2013-07-08 10:06'
p3011
sg145
S'1:1 Toe by Toe'
p3012
sg52
S'2013'
p3013
sg148
S'weekly(x1)'
p3014
sg86
g27
sg150
S'KM'
p3015
sa(dp3016
g91
S'Summer'
p3017
sg81
S'F '
p3018
sg87
S'2013-07-08 10:08'
p3019
sg145
S'1:1 Literacy support with GSK'
p3020
sg52
S'2013'
p3021
sg148
S'weekly(x1)'
p3022
sg86
g27
sg150
g27
sassssS'Jamie_Lamb'
p3023
(dp3024
g3
(dp3025
g5
(dp3026
g7
I0
sg8
(lp3027
g1285
ag27
asg12
S'Sen provision map for Jamie Lamb'
p3028
sg14
g15
sg16
(lp3029
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Jamie Lamb'
p3030
sg24
I0
sg25
(lp3031
g1290
ag1291
asg29
(lp3032
g27
ag31
asg32
S'SEN,Provision map,Jamie Lamb'
p3033
sg34
(lp3034
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp3035
g41
S'06-05-2010'
p3036
sg43
S'Lamb'
p3037
sg45
S'Action +'
p3038
sg47
S'2002-08-23'
p3039
sg49
S'EP Report Nina Elliott 06.05.10\r<br>EP Report Nina Elliott 19.09.12'
p3040
sg51
g27
sg52
g1564
sg54
g27
sg56
S"Jaime is a very intelligent boy (GAI 132) who has some difficulties with his literacy skills in particular his writing speed is very slow. His core literacy skills are age appropriate however discrepant in relation to his verbal and non verbal abilities. In the past 2 years he has made good progress with his reading, spelling and writing. Jamie's verbal comprehension abilities are in the superior range (124) and his perceptual reasoning very superior range (131). His general working memory and processing speeds abilities are in the average range (WMI = 94) (PSI = 94).  Jamie's ability to process visual material quickly is a weakness relative to his verbal and nonverbal reasoning ability. His visuo - spatial memory is weak.  He is presenting with some mild specific learning difficulties of a dyslexic nature. Jamie has food allergies, fish/egg/nuts."
p3041
sg58
g27
sg59
S'Jamie'
p3042
sg61
S'25%'
p3043
sg62
S'6CS'
p3044
ssg64
I1
sg65
I0
sg66
(lp3045
g1305
ag1306
asg70
S'19-09-2012'
p3046
sg72
I0
sg73
(lp3047
g1308
ag27
asssg76
(dp3048
g5
(dp3049
g38
(lp3050
(dp3051
g81
S'M '
p3052
sg83
S'Michaelmas Report - Eleanor Barker.'
p3053
sg85
g27
sg86
g27
sg87
S'2010-12-19 14:36'
p3054
sa(dp3055
g81
S'M '
p3056
sg83
S'NB, SAC and Mrs Lamb. Discussion of Year 5 and Jamies Year 4 results. '
p3057
sg85
g27
sg86
g27
sg87
S'2011-09-14 14:36'
p3058
sasg37
(lp3059
(dp3060
g91
S'Lent'
p3061
sg93
S'2011'
p3062
sg95
S'To take time to organise his thoughts before commencing written tasks and plan using a mind map or template.'
p3063
sg81
S'M '
p3064
sg98
g27
sg87
S'2010-12-14 09:54'
p3065
sg100
g139
sg102
g27
sg86
g27
sa(dp3066
g91
S'Mich'
p3067
sg93
S'2011'
p3068
sg95
S'To show greater confidence in himself as a learner and produce a sustained effort in all learning activities.'
p3069
sg81
S'M '
p3070
sg98
g27
sg87
S'2011-10-10 16:29'
p3071
sg100
g101
sg102
g27
sg86
g27
sa(dp3072
g91
S'Lent'
p3073
sg93
S'2012'
p3074
sg95
S'To be more proactive in lessons such as putting his hand up more often to answer questions and share his thoughts.'
p3075
sg81
S'M '
p3076
sg98
g27
sg87
S'2012-02-27 16:29'
p3077
sg100
g101
sg102
g27
sg86
g27
sa(dp3078
g91
S'Mich'
p3079
sg93
S'2012'
p3080
sg95
S'To increase independence as a learner through communicating with his teachers and asking for help if required.'
p3081
sg81
S'M '
p3082
sg98
g27
sg87
S'2012-10-10 12:01'
p3083
sg100
g101
sg102
g27
sg86
g27
sa(dp3084
g91
S'Mich'
p3085
sg93
S'2012'
p3086
sg95
S'To complete the tasks given by teachers in the time allocated. (75% of the time). '
p3087
sg81
S'M '
p3088
sg98
g27
sg87
S'2012-10-11 12:01'
p3089
sg100
g139
sg102
g27
sg86
g27
sa(dp3090
g91
S'Lent'
p3091
sg93
S'2013'
p3092
sg95
S'To complete the tasks given by teachers in the time allocated. (75% of the time).'
p3093
sg81
S'M '
p3094
sg98
g27
sg87
S'2012-10-11 12:01'
p3095
sg100
g101
sg102
g27
sg86
g27
sa(dp3096
g91
S'Mich'
p3097
sg93
S'2013'
p3098
sg95
S'To build on recent success and increase my level of interaction in class discussion. '
p3099
sg81
S'M '
p3100
sg98
g27
sg87
S'2012-10-11 12:01'
p3101
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp3102
(dp3103
g91
S'Michaelmas'
p3104
sg81
S'M '
p3105
sg87
S'2010-12-14 09:46'
p3106
sg145
S'Handwriting Support group. '
p3107
sg52
S'2010'
p3108
sg148
S'weekly(x1)'
p3109
sg86
g27
sg150
S'CC'
p3110
sa(dp3111
g91
S'Michaelmas'
p3112
sg81
S'M '
p3113
sg87
S'2010-12-14 09:46'
p3114
sg145
S'1:1 Learning Specialist Eleanor Barker.'
p3115
sg52
S'2010'
p3116
sg148
S'weekly(x1)'
p3117
sg86
g27
sg150
g27
sa(dp3118
g91
S'Michaelmas'
p3119
sg81
S'M '
p3120
sg87
S'2010-12-14 09:46'
p3121
sg145
S'1:1 Learning Specialist Eleanor Barker.'
p3122
sg52
S'2011'
p3123
sg148
S'weekly(x1)'
p3124
sg86
g27
sg150
g27
sa(dp3125
g91
S'Summer'
p3126
sg81
S'M '
p3127
sg87
S'2010-12-14 09:46'
p3128
sg145
S'1:1 Learning Specialist Eleanor Barker.'
p3129
sg52
S'2011'
p3130
sg148
S'weekly(x1)'
p3131
sg86
g27
sg150
g27
sa(dp3132
g91
S'Michaelmas'
p3133
sg81
S'M '
p3134
sg87
S'2010-12-14 09:46'
p3135
sg145
S'1:1 Learning Specialist Eleanor Barker.'
p3136
sg52
S'2011'
p3137
sg148
S'weekly(x1)'
p3138
sg86
g27
sg150
g27
sa(dp3139
g91
S'Lent'
p3140
sg81
S'M '
p3141
sg87
S'2012-02-09 09:46'
p3142
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p3143
sg52
S'2012'
p3144
sg148
S'weekly(x1)'
p3145
sg86
g27
sg150
g27
sa(dp3146
g91
S'Summer'
p3147
sg81
S'M '
p3148
sg87
S'2012-05-08 09:46'
p3149
sg145
S'Handwriting support class. '
p3150
sg52
S'2012'
p3151
sg148
S'weekly(x1)'
p3152
sg86
g27
sg150
g27
sa(dp3153
g91
S'Summer'
p3154
sg81
S'M '
p3155
sg87
S'2012-05-14 09:46'
p3156
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p3157
sg52
S'2012'
p3158
sg148
S'weekly(x1)'
p3159
sg86
g27
sg150
g27
sa(dp3160
g91
S'Michaelmas'
p3161
sg81
S'M '
p3162
sg87
S'2012-09-07 09:46'
p3163
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p3164
sg52
S'2012'
p3165
sg148
S'weekly(x1)'
p3166
sg86
g27
sg150
g27
sassssS'Eliza_Antelme'
p3167
(dp3168
g3
(dp3169
g5
(dp3170
g7
I0
sg8
(lp3171
g184
ag185
asg12
S'Sen provision map for Eliza Antelme'
p3172
sg14
g15
sg16
(lp3173
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Eliza Antelme'
p3174
sg24
I0
sg25
(lp3175
g27
aS' Encourage Eliza to consider what is expected of her for a few moments before she starts writing or answering a question. Breaking tasks down into smaller chunks, for which Eliza has to attend for short periods of time and which can then be followed by a change of activity.'
p3176
asg29
(lp3177
g27
ag31
asg32
S'SEN,Provision map,Eliza Antelme'
p3178
sg34
(lp3179
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp3180
g41
S'12/12/09'
p3181
sg43
S'Antelme'
p3182
sg45
S'Action +'
p3183
sg47
S'2002-06-30'
p3184
sg49
S'EP Report Nina Elliott'
p3185
sg51
S'Both'
p3186
sg52
g1564
sg54
S'07-09-2011'
p3187
sg56
S'Eliza has good verbal comprehension and reasoning abilities however has weak spelling, reading and written expression. Eliza has Dyslexic type difficulties. VCI 116 PRI 119 WMI 107 PSI 106 FSIQ 118'
p3188
sg58
g27
sg59
S'Eliza'
p3189
sg61
S'25%'
p3190
sg62
S'6CE'
p3191
ssg64
I1
sg65
I0
sg66
(lp3192
S'Eliza has good verbal comprehension and reasoning abilities however has weak spelling, reading and written expression. Eliza has Dyslexic type difficulties.'
p3193
aS"Utilise a mulit-sensory approach when working on reading, spelling and writing skills, work to develop phonological awareness skills. Structured discussion to develop language and confidence, and fluency with language and literacy.  Eliza's verbal comprehension and reasoning are relative areas of strength, these relative strengths should be incorporated in learning strategies such as discussing passages with peers, questioning and justification prior to written work.   Discuss in advance work that will require much reading and writing so that she can gain interest and knowledge of the text before she attempts to read it. Support Eliza in with templates and specific planning or mapping of thoughts before she commences formal writing pieces to prevent disorganised and patchy writing samples."
p3194
asg70
S'21-12-2009'
p3195
sg72
I0
sg73
(lp3196
g211
ag212
asssg76
(dp3197
g5
(dp3198
g38
(lp3199
(dp3200
g81
S'F '
p3201
sg83
S'Michaelmas report from GSK. '
p3202
sg85
S'11-07-2011'
p3203
sg86
g27
sg87
S'2010-12-13 15:10'
p3204
sa(dp3205
g81
S'F '
p3206
sg83
S'Summer report from GSK. '
p3207
sg85
S'11-07-2011'
p3208
sg86
g27
sg87
S'2011-07-11 15:10'
p3209
sasg37
(lp3210
(dp3211
g91
S'Lent'
p3212
sg93
S'2011'
p3213
sg95
S'To share ideas more readily in class discussions.'
p3214
sg81
S'F '
p3215
sg98
g27
sg87
S'2010-12-13 15:10'
p3216
sg100
g101
sg102
g27
sg86
g27
sa(dp3217
g91
S'Mich'
p3218
sg93
S'2011'
p3219
sg95
S'To develop a greater level of independence as a learner.'
p3220
sg81
S'F '
p3221
sg98
g27
sg87
S'2011-10-10 15:10'
p3222
sg100
g101
sg102
g27
sg86
g27
sa(dp3223
g91
S'Lent'
p3224
sg93
S'2012'
p3225
sg95
S'To build on recent success as a learner and begin to challenge herself in lessons. '
p3226
sg81
S'F '
p3227
sg98
g27
sg87
S'2012-02-27 15:10'
p3228
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp3229
(dp3230
g91
S'Lent'
p3231
sg81
S'F '
p3232
sg87
S'2010-01-10 15:00'
p3233
sg145
S'1:1 Specialist Teaching Claire Cooper'
p3234
sg52
S'2010'
p3235
sg148
S'weekly(x1)'
p3236
sg86
g27
sg150
g27
sa(dp3237
g91
S'Summer'
p3238
sg81
S'F '
p3239
sg87
S'2010-04-15 15:05'
p3240
sg145
S'1:1 Specialist Teaching Claire Cooper'
p3241
sg52
S'2010'
p3242
sg148
S'weekly(x1)'
p3243
sg86
g27
sg150
g27
sa(dp3244
g91
S'Michaelmas'
p3245
sg81
S'F '
p3246
sg87
S'2010-04-15 15:05'
p3247
sg145
S'1:1 Specialist Teaching George Scott Kerr'
p3248
sg52
S'2010'
p3249
sg148
S'weekly(x1)'
p3250
sg86
g27
sg150
g27
sa(dp3251
g91
S'Lent'
p3252
sg81
S'F '
p3253
sg87
S'2010-04-15 15:05'
p3254
sg145
S'1:1 Specialist Teaching George Scott Kerr'
p3255
sg52
S'2011'
p3256
sg148
S'weekly(x1)'
p3257
sg86
g27
sg150
g27
sa(dp3258
g91
S'Lent'
p3259
sg81
S'F '
p3260
sg87
S'2010-04-15 15:05'
p3261
sg145
S'1:1 Specialist Teaching George Scott Kerr'
p3262
sg52
S'2011'
p3263
sg148
S'weekly(x1)'
p3264
sg86
g27
sg150
g27
sa(dp3265
g91
S'Summer'
p3266
sg81
S'F '
p3267
sg87
S'2010-04-15 15:05'
p3268
sg145
S'1:1 Specialist Teaching George Scott Kerr'
p3269
sg52
S'2011'
p3270
sg148
S'weekly(x1)'
p3271
sg86
g27
sg150
g27
sassssS'Albie_Redding'
p3272
(dp3273
g3
(dp3274
g5
(dp3275
g7
I0
sg8
(lp3276
S'Albie will also find worded problems difficult in mathematics.'
p3277
ag27
asg12
S'Sen provision map for Albie Redding'
p3278
sg14
g15
sg16
(lp3279
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Albie Redding'
p3280
sg24
I0
sg25
(lp3281
S"Ensure all instructions are kept precise and check understanding and retention by asking for repetition and clarification during task. Cue Albie in by using his name before specific instructions. Break tasks down into their smallest components and encourage completion of each step at a time, check off each step to ensure understanding and accuracy. Ensure he can 'see' what the end product will be. Use movement breaks to help concentration and focus."
p3282
ag27
asg29
(lp3283
g562
ag27
asg32
S'SEN,Provision map,Albie Redding'
p3284
sg34
(lp3285
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp3286
g41
S'17/5/10'
p3287
sg43
S'Redding'
p3288
sg45
S'Action'
p3289
sg47
S'2004-11-28'
p3290
sg49
S'17/5/2010 DEST completed by Nathan Boller\r<br>EP Report Annie Mitchell 17/3/2011'
p3291
sg51
g27
sg52
g53
sg54
S'17-03-2011'
p3292
sg56
S'Albie has specific weaknesses in working memory (88) and auditory processing. His verbal comprehension (112) and non verbal comprehension  (110) are high average and his processing speed is average (106). Albie has worked very hard on improving his reading skills which he is now more confident with. His written work is often very creative however he may need support with structure and organisation of thoughts. Albie may find worded problems difficult in mathematics. Albie has some visual processing difficulties. Albie may have dyslexia.'
p3293
sg58
g27
sg59
S'Albie'
p3294
sg61
g27
sg62
S'3CN'
p3295
ssg64
I1
sg65
I1
sg70
S'17-03-2011'
p3296
sg72
I0
sg66
(lp3297
S'Albie has worked very hard on improving his reading skills which he is now more confident with. His written work is often very creative however he may need support with structure and organisation of thoughts. Albie may find worded problems difficult in mathematics. Albie has some visual processing difficulties. Albie may have dyslexia.'
p3298
aS"Ensure Albie sits facing the board so he can see clearly. He shouldn't be expected to copy copious amounts from the board. Scribe sections if appropriate. Provide Albie with templates for written tasks is possible such as mind maps and prompt sheets. If Albie makes an error when writing letters, discuss with him why it is. Have him refer to an example and ask him if it looks the same. Provide a letter list which has visuals. A apple, B banana etc. Provide Albie with visual cues where possible. Showing Albie the 'bed' trick may help with his b and d reversals. Check understanding of vocabulary when introducing new concepts or topics. Reduce auditory distractions where possible."
p3299
asssg76
(dp3300
g5
(dp3301
g38
(lp3302
(dp3303
g81
S'M '
p3304
sg83
S'Meeting held between Mrs Redding, NB, EJ and HSM regarding having a formal EP assessment completed for Albie. '
p3305
sg85
S'01-02-2011'
p3306
sg86
g27
sg87
S'2010-12-13 10:20'
p3307
sasg37
(lp3308
(dp3309
g91
S'Lent'
p3310
sg93
S'2011'
p3311
sg95
S'To try and sound out words independently when reading and writing.'
p3312
sg81
S'M '
p3313
sg98
g27
sg87
S'2010-12-13 10:19'
p3314
sg100
g101
sg102
g27
sg86
g27
sa(dp3315
g91
S'Lent'
p3316
sg93
S'2011'
p3317
sg95
S'To listen to instructions carefully and repeat them back to the teacher.'
p3318
sg81
S'M '
p3319
sg98
g27
sg87
S'2010-12-13 10:20'
p3320
sg100
g101
sg102
g27
sg86
g27
sa(dp3321
g91
S'Lent'
p3322
sg93
S'2012'
p3323
sg95
S'To read for 10 minutes daily to increase his accuracy and fluency when reading. '
p3324
sg81
S'M '
p3325
sg98
g27
sg87
S'2011-03-14 17:22'
p3326
sg100
g101
sg102
g27
sg86
g27
sa(dp3327
g91
S'Mich'
p3328
sg93
S'2011'
p3329
sg95
S'To listen to instructions first time around and act on them straight away.'
p3330
sg81
S'M '
p3331
sg98
g27
sg87
S'2011-09-27 17:22'
p3332
sg100
g101
sg102
g27
sg86
S'LLA'
p3333
sa(dp3334
g91
S'Mich'
p3335
sg93
S'2012'
p3336
sg95
S'1. Complete the instruction given by a teacher without delay.'
p3337
sg81
S'M '
p3338
sg98
g27
sg87
S'2012-07-19 12:44'
p3339
sg100
g101
sg102
g27
sg86
g27
sa(dp3340
g91
S'Mich'
p3341
sg93
S'2012'
p3342
sg95
S'2. Attempt taks before asking for support from the teacher.                            '
p3343
sg81
S'M '
p3344
sg98
g27
sg87
S'2012-07-19 12:44'
p3345
sg100
g139
sg102
g27
sg86
g27
sa(dp3346
g91
S'Lent'
p3347
sg93
S'2012'
p3348
sg95
S'2. Attempt taks before asking for support from the teacher. (75%)'
p3349
sg81
S'M '
p3350
sg98
g27
sg87
S'2012-07-19 12:44'
p3351
sg100
g101
sg102
g27
sg86
g27
sa(dp3352
g91
S'Summer'
p3353
sg93
S'2013'
p3354
sg95
S'Complete the allocated amount of work in the given time as agreed before the task commences. '
p3355
sg81
S'M '
p3356
sg98
g27
sg87
S'2013-07-16 09:48'
p3357
sg100
g101
sg102
g27
sg86
g27
sa(dp3358
g91
S'Mich'
p3359
sg93
S'2013'
p3360
sg95
S'Attempt to complete more than the minimum amount of work assigned. Complete this without prompting and reminding from the teacher.'
p3361
sg81
S'M '
p3362
sg98
g27
sg87
S'2013-07-16 09:48'
p3363
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp3364
(dp3365
g91
S'Michaelmas'
p3366
sg81
S'M '
p3367
sg87
S'2010-12-13 10:19'
p3368
sg145
S'Small group maths session focusing on number facts'
p3369
sg52
S'2010'
p3370
sg148
S'weekly(x1)'
p3371
sg86
g27
sg150
S'KM'
p3372
sa(dp3373
g91
S'Lent'
p3374
sg81
S'M '
p3375
sg87
S'2010-12-13 10:19'
p3376
sg145
S'Small group maths session focusing on number facts'
p3377
sg52
S'2011'
p3378
sg148
S'weekly(x1)'
p3379
sg86
g27
sg150
S'KM'
p3380
sa(dp3381
g91
S'Summer'
p3382
sg81
S'M '
p3383
sg87
S'2011-05-13 10:19'
p3384
sg145
S'1:1 Support sessions - literacy.'
p3385
sg52
S'2011'
p3386
sg148
S'weekly(x2)'
p3387
sg86
g27
sg150
S'NB'
p3388
sa(dp3389
g91
S'Michaelmas'
p3390
sg81
S'M '
p3391
sg87
S'2011-08-14 10:19'
p3392
sg145
S'1:1 Support sessions - literacy.'
p3393
sg52
S'2011'
p3394
sg148
S'weekly(x2)'
p3395
sg86
g27
sg150
S'NB'
p3396
sa(dp3397
g91
S'Lent'
p3398
sg81
S'M '
p3399
sg87
S'2012-02-06 10:19'
p3400
sg145
S'1:1 Support sessions - literacy.'
p3401
sg52
S'2012'
p3402
sg148
S'weekly(x2)'
p3403
sg86
g27
sg150
S'NB'
p3404
sa(dp3405
g91
S'Summer'
p3406
sg81
S'M '
p3407
sg87
S'2012-06-06 10:19'
p3408
sg145
S'1:1 Support sessions - literacy.'
p3409
sg52
S'2012'
p3410
sg148
S'weekly(x2)'
p3411
sg86
g27
sg150
S'NB'
p3412
sa(dp3413
g91
S'Michaelmas'
p3414
sg81
S'M '
p3415
sg87
S'2013-07-08 10:22'
p3416
sg145
S'Spelling support group'
p3417
sg52
S'2012'
p3418
sg148
S'weekly(x1)'
p3419
sg86
g27
sg150
S'KM'
p3420
sa(dp3421
g91
S'Michaelmas'
p3422
sg81
S'M '
p3423
sg87
S'2013-07-08 10:22'
p3424
sg145
S'Reading comprehension group'
p3425
sg52
S'2012'
p3426
sg148
S'weekly(x1)'
p3427
sg86
g27
sg150
S'NB'
p3428
sa(dp3429
g91
S'Lent'
p3430
sg81
S'M '
p3431
sg87
S'2013-07-08 10:22'
p3432
sg145
S'Reading comprehension group'
p3433
sg52
S'2013'
p3434
sg148
S'weekly(x1)'
p3435
sg86
g27
sg150
S'NB'
p3436
sa(dp3437
g91
S'Summer'
p3438
sg81
S'M '
p3439
sg87
S'2013-07-08 10:22'
p3440
sg145
S'Reading comprehension group'
p3441
sg52
S'2013'
p3442
sg148
S'weekly(x1)'
p3443
sg86
g27
sg150
S'NB'
p3444
sa(dp3445
g91
S'Michaelmas'
p3446
sg81
S'M '
p3447
sg87
S'2013-07-08 10:22'
p3448
sg145
S'1:1 Literacy support with EB'
p3449
sg52
S'2012'
p3450
sg148
S'weekly(x1)'
p3451
sg86
g27
sg150
g27
sa(dp3452
g91
S'Lent'
p3453
sg81
S'M '
p3454
sg87
S'2013-07-08 10:22'
p3455
sg145
S'1:1 Literacy support with EB'
p3456
sg52
S'2013'
p3457
sg148
S'weekly(x1)'
p3458
sg86
g27
sg150
g27
sa(dp3459
g91
S'Summer'
p3460
sg81
S'M '
p3461
sg87
S'2013-07-08 10:22'
p3462
sg145
S'1:1 Literacy support with EB'
p3463
sg52
S'2013'
p3464
sg148
S'weekly(x1)'
p3465
sg86
g27
sg150
g27
sassssS'India_Alderman'
p3466
(dp3467
g3
(dp3468
g5
(dp3469
g7
I0
sg8
(lp3470
g184
ag185
asg12
S'Sen provision map for India Alderman'
p3471
sg14
g15
sg16
(lp3472
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for India Alderman'
p3473
sg24
I0
sg25
(lp3474
g27
aS"Building India's confidence is paramount in ensuring success so please take all opportunities to praise and congratulate India."
p3475
asg29
(lp3476
g27
ag31
asg32
S'SEN,Provision map,India Alderman'
p3477
sg34
(lp3478
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp3479
g41
S'24/06/2010'
p3480
sg43
S'Alderman'
p3481
sg45
S'Action'
p3482
sg47
S'2001-10-30'
p3483
sg49
S'EP Report Nina Elliott. 18.09.12 and 13.09.10\r<br>Assessed by AS on 24.6.10. Neale Test of Reading'
p3484
sg51
S'Both'
p3485
sg52
g1564
sg54
S'13-09-2010'
p3486
sg56
S"India has a specific learning difficulty that particularly impacts on her written and oral expression. She finds it difficult to express herself and has difficulties getting her thoughts down on paper. She is under confident as a learner. India's writing speed is slow and her ability to comprehend written and spoken information is weak. Her fundamental literacy and numeracy skills are in the 'average' range and she must be encouraged to attempt work and take risks with her learning."
p3487
sg58
g27
sg59
S'India'
p3488
sg61
S'25%'
p3489
sg62
S'6CS'
p3490
ssg64
I1
sg65
I0
sg66
(lp3491
S"India has a specific learning difficulty that particularly impacts on her written and oral expression. She finds it difficult to express herself and has difficulties getting her thoughts down on paper. She is under confident as a learner. India's writing speed is slow and her ability to comprehend written and spoken information is weak. Her fundamental literacy and numeracy skills are in the 'average' range and she must be encouraged to attempt work and take risks with her learning."
p3492
aS"Keep verbal instructions as brief as possible and reinforce them with visuals or written prompts. Ensure that verbal information is delivered in small chunks so that India has the chance to digest the information. Encourage discussion of thoughts and break tasks down into workable chunks. India may find it difficult to understand complex instructions and you will be required to check that she understands tasks. Additional time to process and respond to a question would be helpful. Support India with Reading Comprehension work by encouraging her to highlight key words and make notes in the margin as appropriate and be willing to repeat instructions and information to support processing. Encourage India to mind map her ideas for essay writing to support her memory and encourage her to 'see' the problem or answer through graphs, charts, tables etc. Support planning of work, where appropriate, with writing frames. Provide templates for written tasks and printouts to prevent copying from the board."
p3493
asg70
S'18-09-2012'
p3494
sg72
I0
sg73
(lp3495
g75
ag27
asssg76
(dp3496
g5
(dp3497
g38
(lp3498
(dp3499
g81
S'F '
p3500
sg83
S'Termly report by LC.'
p3501
sg85
S'04-07-2011'
p3502
sg86
g27
sg87
S'2011-07-11 09:40'
p3503
sa(dp3504
g81
S'F '
p3505
sg83
S'NB and Mrs Alderman. Review meeting. India to join the reading comprehension group in YR 5. '
p3506
sg85
S'27-06-2011'
p3507
sg86
g27
sg87
S'2011-07-11 09:40'
p3508
sa(dp3509
g81
S'F '
p3510
sg83
S'AP and Mrs Alderman. Review meeting to discuss progress.'
p3511
sg85
S'03-03-2011'
p3512
sg86
g27
sg87
S'2011-07-11 09:40'
p3513
sa(dp3514
g81
S'F '
p3515
sg83
S'CH and Mrs Alderman. Review meeting to discuss Nina Elliotts report. '
p3516
sg85
S'03-11-2010'
p3517
sg86
g27
sg87
S'2011-07-11 09:40'
p3518
sa(dp3519
g81
S'F '
p3520
sg83
S'NB and Mrs Alderman. Review meeting to discuss Nina Elliotts report. '
p3521
sg85
S'21-10-2010'
p3522
sg86
g27
sg87
S'2011-07-11 09:40'
p3523
sa(dp3524
g81
S'F '
p3525
sg83
S'NB and Mrs Alderman. Discussion of progress. '
p3526
sg85
g27
sg86
g27
sg87
S'2012-04-26 15:40'
p3527
sa(dp3528
g81
S'F '
p3529
sg83
S'Meeting between NB and Mrs Alderman to discuss EP report. '
p3530
sg85
g27
sg86
g27
sg87
S'2012-10-08 13:29'
p3531
sasg37
(lp3532
(dp3533
g91
S'Lent'
p3534
sg93
S'2011'
p3535
sg95
S'To have the confidence to attempt tasks.'
p3536
sg81
S'F '
p3537
sg98
g27
sg87
S'2010-12-13 14:55'
p3538
sg100
g101
sg102
g27
sg86
g27
sa(dp3539
g91
S'Mich'
p3540
sg93
S'2011'
p3541
sg95
S'To attempt written work with a greater level of confidence and independence.'
p3542
sg81
S'F '
p3543
sg98
g27
sg87
S'2010-12-13 14:55'
p3544
sg100
g101
sg102
g27
sg86
g27
sa(dp3545
g91
S'Lent'
p3546
sg93
S'2012'
p3547
sg95
S'To build on recent success and increase her level of interaction in class discussion.'
p3548
sg81
S'F '
p3549
sg98
g27
sg87
S'2012-02-27 14:55'
p3550
sg100
g139
sg102
g27
sg86
g27
sa(dp3551
g91
S'Summer'
p3552
sg93
S'2012'
p3553
sg95
S'To build on recent success and increase her level of interaction in class discussion.'
p3554
sg81
S'F '
p3555
sg98
g27
sg87
S'2012-02-27 14:55'
p3556
sg100
g101
sg102
g27
sg86
g27
sa(dp3557
g91
S'Mich'
p3558
sg93
S'2012'
p3559
sg95
S'To build on recent success and increase her level of interaction in class discussion.'
p3560
sg81
S'F '
p3561
sg98
g27
sg87
S'2012-07-19 13:29'
p3562
sg100
g101
sg102
g27
sg86
g27
sa(dp3563
g91
S'Lent'
p3564
sg93
S'2013'
p3565
sg95
S'To read the question twice before attempting to write my answer. '
p3566
sg81
S'F '
p3567
sg98
g27
sg87
S'2012-07-19 13:29'
p3568
sg100
g139
sg102
g27
sg86
g27
sa(dp3569
g91
S'Summer'
p3570
sg93
S'2013'
p3571
sg95
S'To read the question twice before attempting to write my answer. '
p3572
sg81
S'F '
p3573
sg98
g27
sg87
S'2012-07-19 13:29'
p3574
sg100
g101
sg102
g27
sg86
g27
sa(dp3575
g91
S'Mich'
p3576
sg93
S'2013'
p3577
sg95
S'To put my hand up in class and ask for help when I dont know what to do. '
p3578
sg81
S'F '
p3579
sg98
g27
sg87
S'2013-07-16 10:48'
p3580
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp3581
(dp3582
g91
S'Summer'
p3583
sg81
S'F '
p3584
sg87
S'2010-12-13 14:49'
p3585
sg145
S'Individual reading support.  '
p3586
sg52
S'2009'
p3587
sg148
S'weekly(x1)'
p3588
sg86
g27
sg150
g27
sa(dp3589
g91
S'Lent'
p3590
sg81
S'F '
p3591
sg87
S'2010-12-13 14:49'
p3592
sg145
S'1:1 Learning Specialist Lindsey Copeman. '
p3593
sg52
S'2011'
p3594
sg148
S'weekly(x1)'
p3595
sg86
g27
sg150
g27
sa(dp3596
g91
S'Summer'
p3597
sg81
S'F '
p3598
sg87
S'2010-12-13 14:49'
p3599
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p3600
sg52
S'2011'
p3601
sg148
S'weekly(x1)'
p3602
sg86
g27
sg150
g27
sa(dp3603
g91
S'Michaelmas'
p3604
sg81
S'F '
p3605
sg87
S'2010-12-13 14:49'
p3606
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p3607
sg52
S'2011'
p3608
sg148
S'weekly(x1)'
p3609
sg86
g27
sg150
g27
sa(dp3610
g91
S'Lent'
p3611
sg81
S'F '
p3612
sg87
S'2010-12-13 14:49'
p3613
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p3614
sg52
S'2012'
p3615
sg148
S'weekly(x1)'
p3616
sg86
g27
sg150
g27
sa(dp3617
g91
S'Summer'
p3618
sg81
S'F '
p3619
sg87
S'2010-12-13 14:49'
p3620
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p3621
sg52
S'2012'
p3622
sg148
S'weekly(x1)'
p3623
sg86
g27
sg150
g27
sa(dp3624
g91
S'Summer'
p3625
sg81
S'F '
p3626
sg87
S'2010-12-13 14:49'
p3627
sg145
S'Handwriting support. '
p3628
sg52
S'2012'
p3629
sg148
S'weekly(x1)'
p3630
sg86
g27
sg150
S'NB'
p3631
sa(dp3632
g91
S'Michaelmas'
p3633
sg81
S'F '
p3634
sg87
S'2013-07-08 16:16'
p3635
sg145
S'1:1 Literacy support with LC'
p3636
sg52
S'2012'
p3637
sg148
S'weekly(x1)'
p3638
sg86
g27
sg150
g27
sa(dp3639
g91
S'Lent'
p3640
sg81
S'F '
p3641
sg87
S'2013-07-08 16:16'
p3642
sg145
S'1:1 Literacy support with LC'
p3643
sg52
S'2013'
p3644
sg148
S'weekly(x1)'
p3645
sg86
g27
sg150
g27
sa(dp3646
g91
S'Summer'
p3647
sg81
S'F '
p3648
sg87
S'2013-07-08 16:16'
p3649
sg145
S'1:1 Literacy support with LC'
p3650
sg52
S'2013'
p3651
sg148
S'weekly(x1)'
p3652
sg86
g27
sg150
g27
sassssS'Matthew_Tyson'
p3653
(dp3654
g3
(dp3655
g5
(dp3656
g7
I0
sg8
(lp3657
g1548
ag1549
asg12
S'Sen provision map for Matthew Tyson'
p3658
sg14
g15
sg16
(lp3659
g27
ag2032
asg20
g21
sg22
S'Special Needs Provision Map for Matthew Tyson'
p3660
sg24
I0
sg25
(lp3661
g27
ag1834
asg29
(lp3662
g2805
ag27
asg32
S'SEN,Provision map,Matthew Tyson'
p3663
sg34
(lp3664
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp3665
g41
S'21-11-2011'
p3666
sg43
S'Tyson'
p3667
sg45
S'Action'
p3668
sg47
S'2000-03-23'
p3669
sg49
S'EP Report V Hero 12.05.2012'
p3670
sg51
S'Both'
p3671
sg52
g935
sg54
g27
sg56
S"Matthew's ability to understand concepts presented in language is considerably better developed than his ability to demonstrate his thoughts and ideas in words. He has a limited vocabulary and his word knowledge in weaker than his verbal reasoning. He has a slight weakness with his receptive language skills and this will impact on his ability to store language in the short term and sustain attention and focus. Matthew is a very slow reader and has a slow writing speed. This will greatly impact on his ability to complete longer written tasks. He has mild dyslexia."
p3672
sg58
g27
sg59
S'Matthew'
p3673
sg61
S'25%'
p3674
sg62
S'8CE'
p3675
ssg64
I0
sg65
I0
sg66
(lp3676
g2046
ag2047
asg70
S'12-05-2012'
p3677
sg72
I0
sg73
(lp3678
g2817
ag2818
asssg76
(dp3679
g5
(dp3680
g38
(lp3681
(dp3682
g81
S'M '
p3683
sg83
S'NB and Mrs Tyson. Meeting to discuss concerns regarding possible learning difficulties. '
p3684
sg85
S'02-12-2011'
p3685
sg86
g27
sg87
S'2011-12-02 17:04'
p3686
sa(dp3687
g81
S'M '
p3688
sg83
S'NB and Mrs Tyson. Meeting to discuss Matts EP report and the implications of this.'
p3689
sg85
S'01-10-2012'
p3690
sg86
g27
sg87
S'2012-10-01 17:04'
p3691
sasg37
(lp3692
(dp3693
g91
S'Mich'
p3694
sg93
S'2012'
p3695
sg95
S'To extend his analysis in higher mark reading comprehension questions.'
p3696
sg81
S'M '
p3697
sg98
g27
sg87
S'2012-07-19 12:55'
p3698
sg100
g101
sg102
g27
sg86
g27
sa(dp3699
g91
S'Lent'
p3700
sg93
S'2013'
p3701
sg95
S'To focus and concentrate for 10 minute blocks in lessons. '
p3702
sg81
S'M '
p3703
sg98
g27
sg87
S'2012-07-19 12:55'
p3704
sg100
g101
sg102
g27
sg86
g27
sa(dp3705
g91
S'Summer'
p3706
sg93
S'2013'
p3707
sg95
S'To challenge myself in lessons by constructing more sophisticated written and spoken answers. '
p3708
sg81
S'M '
p3709
sg98
g27
sg87
S'2012-07-19 12:55'
p3710
sg100
g101
sg102
g27
sg86
g27
sassssS'Ted_Pilcher'
p3711
(dp3712
g3
(dp3713
g5
(dp3714
g7
I0
sg8
(lp3715
g2596
ag2597
asg12
S'Sen provision map for Ted Pilcher'
p3716
sg14
g15
sg16
(lp3717
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Ted Pilcher'
p3718
sg24
I0
sg25
(lp3719
g27
ag2602
asg29
(lp3720
g27
ag31
asg32
S'SEN,Provision map,Ted Pilcher'
p3721
sg34
(lp3722
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp3723
g41
S'21-09-2010'
p3724
sg43
S'Pilcher'
p3725
sg45
S'No Provision'
p3726
sg47
S'2003-01-22'
p3727
sg49
S'Classroom observation KAM 21.09.2010 and 28.09.2010\r<br>DST- J 28.09.2010'
p3728
sg51
g27
sg52
g384
sg54
g27
sg56
g27
sg58
g27
sg59
S'Ted'
p3729
sg61
g27
sg62
S'5CN'
p3730
ssg64
I0
sg65
I0
sg66
(lp3731
g2618
ag2619
asg70
g392
sg72
I0
sg73
(lp3732
g211
ag212
asssg76
(dp3733
g5
(dp3734
g38
(lp3735
(dp3736
g81
S'M '
p3737
sg83
S'Mrs Pilcher, KAM and GS. Discussion regarding assessment. '
p3738
sg85
g27
sg86
g27
sg87
S'2010-10-13 13:27'
p3739
sasg37
(lp3740
(dp3741
g91
S'Mich'
p3742
sg93
S'2011'
p3743
sg95
S'Check/edit work carefully before handing in.'
p3744
sg81
S'M '
p3745
sg98
g27
sg87
S'2011-09-21 12:32'
p3746
sg100
g139
sg102
S'Allow time for Ted to edit work.Tick lists of what to check for.'
p3747
sg86
S'AP'
p3748
sa(dp3749
g91
S'Lent'
p3750
sg93
S'2012'
p3751
sg95
S'To take more time and care over work in all subjects.'
p3752
sg81
S'M '
p3753
sg98
g27
sg87
S'2012-01-25 16:43'
p3754
sg100
g139
sg102
S'Regular encouragement and praise for good work.'
p3755
sg86
S'AP'
p3756
sasg36
(lp3757
(dp3758
g91
S'Lent'
p3759
sg81
S'M '
p3760
sg87
S'2011-07-13 13:29'
p3761
sg145
S'Small group spelling support. '
p3762
sg52
S'2011'
p3763
sg148
S'weekly(x1)'
p3764
sg86
g27
sg150
S'NB'
p3765
sa(dp3766
g91
S'Summer'
p3767
sg81
S'M '
p3768
sg87
S'2011-07-13 13:29'
p3769
sg145
S'Small group spelling support. '
p3770
sg52
S'2011'
p3771
sg148
S'weekly(x1)'
p3772
sg86
g27
sg150
S'NB'
p3773
sassssS'Leo_Leach'
p3774
(dp3775
g3
(dp3776
g5
(dp3777
g7
I0
sg8
(lp3778
g1456
ag27
asg12
S'Sen provision map for Leo Leach'
p3779
sg14
g15
sg16
(lp3780
S'Leo may find it difficult to communicate his needs and/or concerns with peers and adults. He has some receptive language difficulties and receives speech and language support for this.'
p3781
aS'Leo must be encouraged to communicate with peers and adults to help express his feelings and requirements. Please support Leo with his communication. Praise at any opportunity if he does communicate with a teacher or peer.  Scaffold discussion as this will hopefully develop a better understanding of daily routine and expectations.'
p3782
asg20
g21
sg22
S'Special Needs Provision Map for Leo Leach'
p3783
sg24
I0
sg25
(lp3784
S'Leo has commenced social skilling sessions with NB looking specifically at risk taking behaviours, sharing, communicating and following the instructions of staff when directed.'
p3785
aS'Implement a rewards program. In the early stages, rewards should be relatively easy to achieve and occur throughout the day. As his behaviour improves, increase the amount of time he needs to wait/achieve before providing a reward. Always tell him specifically what he has done to gain the reward. Continue to use visuals where possible to support his understanding of structure and daily routine.'
p3786
asg29
(lp3787
S"Leo's main area of concern in his risk taking behaviour and his ability to interact appropriately with peers. Leo is developing the ability to self regulate his behaviour. Leo completes social skilling sessions with NB looking specifically at risk taking behaviours, social communication and understanding emotions."
p3788
aS'The use of specific seating is very important as it allows the teacher to direct him and his concentration when on the floor. Please break tasks into manageable challenges for Leo to maintain his interest and do not have him sitting for long periods of time. Where possible, incorporate movement into the lessons such as brain gym. Using timers for tasks and speaking to him through this time is a very useful and will reinforce his responsibility in completing tasks as required. Use his name with very specific instructions such as \x93Leo stop\x94 and \x93Leo no\x94 before longer strings of instructions. Use visuals where possible to support his understanding of structure and daily routine. Scaffolding this with discussion will develop a better understanding of daily routine and expectations. Implement a rewards program. In the early stages, rewards should be relatively easy to achieve and occur throughout the day. As his behaviour improves, increase the amount of time he needs to wait/achieve before providing a reward but always make it achievable. Always tell him specifically what he has done to gain the reward and PRAISE, PRAISE, PRAISE.'
p3789
asg32
S'SEN,Provision map,Leo Leach'
p3790
sg34
(lp3791
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp3792
g41
S'01-10-2010'
p3793
sg43
S'Leach'
p3794
sg45
S'Early years Action +'
p3795
sg47
S'2005-09-17'
p3796
sg49
S'Observation completed on 5/10/2010 by NB\r<br>Speech and Language assessment Sept 2011 - Ruth Jacobs.\r<br>EP Assessment Jeremy Monson 7/07/2012'
p3797
sg51
g27
sg52
g1470
sg54
S'12-07-2011'
p3798
sg56
S'Leo can display challenging behaviours. He finds some social interactions with peers challenging and may engage in unsafe behaviours. Leo may find it difficult to communicate his needs and/or concerns with peers and adults. He has some receptive language difficulties. Leo is a very energetic young boy who needs to be challenged in his learning. He enjoys working on the computer in general and as a reward for appropriate behaviour.'
p3799
sg58
g27
sg59
S'Leo'
p3800
sg61
g27
sg62
S'2CS'
p3801
ssg64
I1
sg65
I0
sg70
S'07-07-2012'
p3802
sg72
I1
sg66
(lp3803
S'Lara has dyslexia. She has difficulties with her spelling, reading and writing skills. Her comprehension and reasoning abilities are good. Her punctuation can be erratic and she has difficulties organising and arranging her thoughts.'
p3804
aS'Lara will benefit from discussion of her thoughts and the opportunity to question content. She requires additional time to think about and process information. Class teachers should ensure Lara has a clear understanding of written instructions and what she is required to do. Ask her questions about the task and generally check in with Lara throughout tasks. Break tasks down so that Lara can focus and complete one/two sections well. This will also allow her to experience success. Where possible, supply Lara with key vocabulary that she can learn as part of her homework. Always look for opportunities to praise Lara and encourage her to communicate with you if she requires support.'
p3805
asssg76
(dp3806
g5
(dp3807
g38
(lp3808
(dp3809
g81
S'M '
p3810
sg83
S'Meeting held between Mr and Mrs Leach, RA and NB. Discussion was held regarding risk taking behaviours, NBs observations and suggested interventions through 1:1 social skilling sessions.'
p3811
sg85
S'18-11-2010'
p3812
sg86
g27
sg87
S'2010-11-18 10:14'
p3813
sa(dp3814
g81
S'M '
p3815
sg83
S'Meeting held between Mr and Mrs Leach, RA, NB and HSM. Discussion was held regarding progress made on risk taking behaviours and the new focus of social skilling sessions.'
p3816
sg85
S'24-01-2011'
p3817
sg86
g27
sg87
S'2010-12-13 10:14'
p3818
sa(dp3819
g81
S'M '
p3820
sg83
S'NB, HSM, RA, Mr and Mrs LEach. Transition to year 1 and review or Reception progress. '
p3821
sg85
g27
sg86
g27
sg87
S'2011-06-24 10:14'
p3822
sa(dp3823
g81
S'M '
p3824
sg83
S'Meeting held between Mr and Mrs Leach, HSM, LG and NB. Discussion of transition. '
p3825
sg85
S'21-09-2011'
p3826
sg86
g27
sg87
S'2011-09-21 10:14'
p3827
sa(dp3828
g81
S'M '
p3829
sg83
S'NB, HSM, LG and Mr and Mrs Leach. Meeting to discuss behaviour and beginning to the term. '
p3830
sg85
g27
sg86
g27
sg87
S'2011-09-21 10:18'
p3831
sa(dp3832
g81
S'M '
p3833
sg83
S'NB, HSM, LG and Mr Leach. Meeting to discuss behaviour and beginning to the term. '
p3834
sg85
g27
sg86
g27
sg87
S'2012-03-05 10:18'
p3835
sa(dp3836
g81
S'M '
p3837
sg83
S'NB, HSM, HF , Mr and Mrs Leach. Review of EP and discussion of progress. '
p3838
sg85
g27
sg86
g27
sg87
S'2012-11-06 10:14'
p3839
sasg37
(lp3840
(dp3841
g91
S'Lent'
p3842
sg93
S'2011'
p3843
sg95
S'To tell the teacher when he has a problem with a peer that he cannot control.'
p3844
sg81
S'M '
p3845
sg98
g27
sg87
S'2010-12-13 10:18'
p3846
sg100
g101
sg102
g27
sg86
g27
sa(dp3847
g91
S'Summer'
p3848
sg93
S'2011'
p3849
sg95
S'Follow teachers instructions within 2 minutes. '
p3850
sg81
S'M '
p3851
sg98
g27
sg87
S'2010-12-13 10:18'
p3852
sg100
g139
sg102
g27
sg86
g27
sa(dp3853
g91
g27
sg93
g27
sg95
S'To tell an adult about how he is feeling before he acts in an inappropriate way.'
p3854
sg81
S'M '
p3855
sg98
g27
sg87
S'2011-09-20 20:01'
p3856
sg100
g101
sg102
g27
sg86
S'LGA'
p3857
sa(dp3858
g91
S'Lent'
p3859
sg93
S'2012'
p3860
sg95
S'Listen to an instruction an respond within 30 seconds.'
p3861
sg81
S'M '
p3862
sg98
g27
sg87
S'2011-11-15 16:15'
p3863
sg100
g139
sg102
g27
sg86
g27
sa(dp3864
g91
S'Lent'
p3865
sg93
S'2011'
p3866
sg95
S'To tell the teacher when he has a problem with a peer that he cannot control.'
p3867
sg81
S'M '
p3868
sg98
g27
sg87
S'2012-04-13 10:18'
p3869
sg100
g101
sg102
g27
sg86
g27
sa(dp3870
g91
S'Mich'
p3871
sg93
S'2012'
p3872
sg95
S'Listen to an instruction an respond within 10 seconds.'
p3873
sg81
S'M '
p3874
sg98
g27
sg87
S'2012-07-19 11:57'
p3875
sg100
g101
sg102
g27
sg86
g27
sa(dp3876
g91
S'Lent'
p3877
sg93
S'2013'
p3878
sg95
S'To speak to a teacher when upset or angry, instead of responding with a physical action towards peers.'
p3879
sg81
S'M '
p3880
sg98
g27
sg87
S'2012-07-19 11:57'
p3881
sg100
g101
sg102
g27
sg86
g27
sa(dp3882
g91
S'Summer'
p3883
sg93
S'2013'
p3884
sg95
S'To listen to an instruction and follow it carefully within 3 seconds.'
p3885
sg81
S'M '
p3886
sg98
g27
sg87
S'2012-07-19 11:57'
p3887
sg100
g101
sg102
g27
sg86
g27
sa(dp3888
g91
S'Mich'
p3889
sg93
S'2013'
p3890
sg95
S'To understand that behaviour should be consistent in all lessons, whether a specialist or with class teacher.'
p3891
sg81
S'M '
p3892
sg98
g27
sg87
S'2012-07-19 11:57'
p3893
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp3894
(dp3895
g91
S'Michaelmas'
p3896
sg81
S'M '
p3897
sg87
S'2010-12-13 10:14'
p3898
sg145
S'1:1 sessions on social skills and safe behaviours'
p3899
sg52
S'2010'
p3900
sg148
S'weekly(x1)'
p3901
sg86
g27
sg150
S'NB'
p3902
sa(dp3903
g91
S'Lent'
p3904
sg81
S'M '
p3905
sg87
S'2010-12-13 10:14'
p3906
sg145
S'1:1 and small group sessions on social skills and safe behaviours'
p3907
sg52
S'2011'
p3908
sg148
S'weekly(x2)'
p3909
sg86
g27
sg150
S'NB'
p3910
sa(dp3911
g91
S'Michaelmas'
p3912
sg81
S'M '
p3913
sg87
S'2010-12-13 10:14'
p3914
sg145
S'1:1 Weekly Speech and Language sessions - Lorna Davis'
p3915
sg52
S'2010'
p3916
sg148
S'weekly(x1)'
p3917
sg86
g27
sg150
g27
sa(dp3918
g91
S'Lent'
p3919
sg81
S'M '
p3920
sg87
S'2010-12-13 10:14'
p3921
sg145
S'1:1 Weekly Speech and Language sessions - Lorna Davis'
p3922
sg52
S'2011'
p3923
sg148
S'weekly(x1)'
p3924
sg86
g27
sg150
g27
sa(dp3925
g91
S'Summer'
p3926
sg81
S'M '
p3927
sg87
S'2010-12-13 10:14'
p3928
sg145
S'1:1 and small group sessions on social skills and safe behaviours'
p3929
sg52
S'2011'
p3930
sg148
S'weekly(x2)'
p3931
sg86
g27
sg150
S'NB'
p3932
sa(dp3933
g91
S'Summer'
p3934
sg81
S'M '
p3935
sg87
S'2010-12-13 10:14'
p3936
sg145
S'1:1 Weekly Speech and Language sessions - Lorna Davis'
p3937
sg52
S'2011'
p3938
sg148
S'weekly(x1)'
p3939
sg86
g27
sg150
g27
sa(dp3940
g91
S'Michaelmas'
p3941
sg81
S'M '
p3942
sg87
S'2010-12-13 10:14'
p3943
sg145
S'1:1 Weekly Speech and Language sessions - Lorna Davis'
p3944
sg52
S'2011'
p3945
sg148
S'weekly(x1)'
p3946
sg86
g27
sg150
g27
sa(dp3947
g91
S'Michaelmas'
p3948
sg81
S'M '
p3949
sg87
S'2010-12-13 10:14'
p3950
sg145
S'1:1 and small group sessions on social skills and safe behaviours'
p3951
sg52
S'2011'
p3952
sg148
S'weekly(x2)'
p3953
sg86
g27
sg150
S'NB'
p3954
sa(dp3955
g91
S'Lent'
p3956
sg81
S'M '
p3957
sg87
S'2010-12-13 10:14'
p3958
sg145
S'1:1 and small group sessions on social skills and safe behaviours'
p3959
sg52
S'2012'
p3960
sg148
S'Monthly(x2)'
p3961
sg86
g27
sg150
S'NB'
p3962
sa(dp3963
g91
S'Michaelmas'
p3964
sg81
S'M '
p3965
sg87
S'2011-09-01 10:14'
p3966
sg145
S'1:1 and small group sessions on social skills and safe behaviours'
p3967
sg52
S'2011'
p3968
sg148
S'weekly(x2)'
p3969
sg86
g27
sg150
S'NB'
p3970
sa(dp3971
g91
S'Lent'
p3972
sg81
S'M '
p3973
sg87
S'2012-02-08 10:14'
p3974
sg145
S'1:1 and small group sessions on social skills and safe behaviours'
p3975
sg52
S'2012'
p3976
sg148
S'Monthly(x2)'
p3977
sg86
g27
sg150
S'NB'
p3978
sa(dp3979
g91
S'Summer'
p3980
sg81
S'M '
p3981
sg87
S'2012-05-08 10:14'
p3982
sg145
S'1:1 and small group sessions on social skills and safe behaviours'
p3983
sg52
S'2012'
p3984
sg148
S'Monthly(x2)'
p3985
sg86
g27
sg150
S'NB'
p3986
sa(dp3987
g91
S'Michaelmas'
p3988
sg81
S'M '
p3989
sg87
S'2013-07-08 15:38'
p3990
sg145
S'Lower school extension group'
p3991
sg52
S'2012'
p3992
sg148
S'weekly(x1)'
p3993
sg86
g27
sg150
S'KM'
p3994
sa(dp3995
g91
S'Lent'
p3996
sg81
S'M '
p3997
sg87
S'2013-07-08 15:38'
p3998
sg145
S'Lower school extension group'
p3999
sg52
S'2013'
p4000
sg148
S'weekly(x1)'
p4001
sg86
g27
sg150
S'KM'
p4002
sa(dp4003
g91
S'Summer'
p4004
sg81
S'M '
p4005
sg87
S'2013-07-08 15:38'
p4006
sg145
S'Lower school extension group'
p4007
sg52
S'2013'
p4008
sg148
S'weekly(x1)'
p4009
sg86
g27
sg150
S'KM'
p4010
sa(dp4011
g91
S'Michaelmas'
p4012
sg81
S'M '
p4013
sg87
S'2013-07-08 15:39'
p4014
sg145
S'In class support/small group social skills'
p4015
sg52
S'2012'
p4016
sg148
S'Monthly(x3)'
p4017
sg86
g27
sg150
S'NB'
p4018
sa(dp4019
g91
S'Lent'
p4020
sg81
S'M '
p4021
sg87
S'2013-07-08 15:39'
p4022
sg145
S'In class support/small group social skills'
p4023
sg52
S'2013'
p4024
sg148
S'Monthly(x3)'
p4025
sg86
g27
sg150
S'NB'
p4026
sa(dp4027
g91
S'Summer'
p4028
sg81
S'M '
p4029
sg87
S'2013-07-08 15:39'
p4030
sg145
S'In class support/small group social skills'
p4031
sg52
S'2013'
p4032
sg148
S'Monthly(x3)'
p4033
sg86
g27
sg150
S'NB'
p4034
sassssS'Lara_Osborn'
p4035
(dp4036
g3
(dp4037
g5
(dp4038
g7
I0
sg8
(lp4039
S'Many of the difficulties Lara has in Numeracy are similar to literacy.'
p4040
ag27
asg12
S'Sen provision map for Lara Osborn'
p4041
sg14
g15
sg16
(lp4042
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Lara Osborn'
p4043
sg24
I0
sg25
(lp4044
S'Although Lara has greater difficulty in Numeracy, these strategies are universal at this stage of support. Please apply and modify to all lessons.'
p4045
aS"Use talk partners, selected questioning, and Lara's name before question and instruction to make sure she is engaged. Lara will need help planning and organising her thoughts. Templates and graphic organisers will help her. She should have information chunked for her into small steps. This is to allow her to process information and not get confused with content. Lara should be encouraged to work for 3-5 minute sections and then check in with the teacher. Teachers must check in with Lara to ensure she understands the task. Approximately 3 minute blocks. A timer can be used to help Lara concentrate and work independently during these blocks of time. Staff must encourage Lara to ask for help if she is not sure of what to do. Teachers should get an example answer from Lara to test her level of understanding."
p4046
asg29
(lp4047
g27
ag31
asg32
S'SEN,Provision map,Lara Osborn'
p4048
sg34
(lp4049
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp4050
g41
S'20-01-2012'
p4051
sg43
S'Osborn'
p4052
sg45
S'Action'
p4053
sg47
S'2006-06-20'
p4054
sg49
S'Classroom observations by NB 27.01.2012\r<br>Annie Mitchell EP 16.5.2013'
p4055
sg51
g27
sg52
g1470
sg54
S'15-05-2013'
p4056
sg56
S'Lara is dyslexic. She has a specific weakness with literacy and phonological production at a word level. Lara has good verbal reasoning skills and will rely on this to help her solve problems. Her non verbal abilities are weaker. She has visual processing difficulties. Lara has a weak working memory which will impact on any activity that requires her to transform, manipulate of hold information. Auditory processing is an additional area of weakness for Lara. Lara struggles to maintain concentration and focus.  She lacks confidence in her abilities and at times, employs avoidance tactics.'
p4057
sg58
g27
sg59
S'Lara'
p4058
sg61
g27
sg62
S'2CW'
p4059
ssg64
I1
sg65
I0
sg66
(lp4060
S'Lara is dyslexic. She has a specific weakness with literacy and phonological production at a word level. Lara has good verbal reasoning skills and will rely on this to help her solve problems. Her non verbal abilities are weaker. She has visual processing difficulties. Lara has a weak working memory which will impact on any activity that requires her to transform, manipulate of hold information. Auditory processing is an additional area of weakness for Lara. Lara struggles to maintain concentration and focus.  She lacks confidence in her abilities and at times, employs avoidance tactics.'
p4061
aS'At times she will adopt a trial and error approach to learning rather than apply the correct procedure. This leads to errors and inaccuracy, always emphasise the process she should follow. Ensure instructions are written where possible on a small whiteboard or check list next to her and avoid long, complex verbal instructions. Break tasks down into smaller activities and use timers to clearly define how long is required. Check in with her often however aim not to answer questions for her.  Encourage discussion with peers and have her talk you through her thinking before she commences longer written work. Look for opportunity to praise Lara especially for independent work.'
p4062
asg70
S'16-05-2013'
p4063
sg72
I0
sg73
(lp4064
g211
ag212
asssg76
(dp4065
g5
(dp4066
g38
(lp4067
(dp4068
g81
S'F '
p4069
sg83
S'NB, HSM, JS, Sir and Lady Osborn. Discussion of observations and actions to support Lara. '
p4070
sg85
g27
sg86
g27
sg87
S'2012-01-30 10:15'
p4071
sa(dp4072
g81
S'F '
p4073
sg83
S'NB, HSM, LLO, Sir and Lady Osborn. Meeting held to discuss EP report and future intervention and support. '
p4074
sg85
g27
sg86
g27
sg87
S'2013-07-04 06:29'
p4075
sasg37
(lp4076
(dp4077
g91
S'Lent'
p4078
sg93
S'2012'
p4079
sg95
S'To repeat instructions to the teacher before carrying out an activity.'
p4080
sg81
S'F '
p4081
sg98
g27
sg87
S'2012-02-06 10:14'
p4082
sg100
g101
sg102
g27
sg86
g27
sa(dp4083
g91
S'Lent'
p4084
sg93
S'2012'
p4085
sg95
S'To work independently for 3 minutes after instruction before requesting support.'
p4086
sg81
S'F '
p4087
sg98
g27
sg87
S'2012-02-06 10:14'
p4088
sg100
g101
sg102
g27
sg86
g27
sa(dp4089
g91
S'Mich'
p4090
sg93
S'2012'
p4091
sg95
S'1. To repeat instructions to the teacher before carrying out an activity.'
p4092
sg81
S'F '
p4093
sg98
g27
sg87
S'2012-07-19 12:26'
p4094
sg100
g101
sg102
g27
sg86
g27
sa(dp4095
g91
S'Mich'
p4096
sg93
S'2012'
p4097
sg95
S'2. To work independently for 7 minutes after instruction before requesting support.'
p4098
sg81
S'F '
p4099
sg98
g27
sg87
S'2012-07-19 12:26'
p4100
sg100
g101
sg102
g27
sg86
g27
sa(dp4101
g91
S'Lent'
p4102
sg93
S'2013'
p4103
sg95
S'1. To listen and be able to repeat the views of her talk partner when discussing topics. '
p4104
sg81
S'F '
p4105
sg98
g27
sg87
S'2012-07-19 12:26'
p4106
sg100
g101
sg102
g27
sg86
g27
sa(dp4107
g91
S'Lent'
p4108
sg93
S'2013'
p4109
sg95
S'2. To work independently for 5 minutes after instruction before requesting support.'
p4110
sg81
S'F '
p4111
sg98
g27
sg87
S'2012-07-19 12:26'
p4112
sg100
g101
sg102
g27
sg86
g27
sa(dp4113
g91
S'Summer'
p4114
sg93
S'2013'
p4115
sg95
S'1. To listen and be able to repeat instructions back to the teacher before commencing a written activity.    '
p4116
sg81
S'F '
p4117
sg98
g27
sg87
S'2013-07-16 09:45'
p4118
sg100
g101
sg102
g27
sg86
g27
sa(dp4119
g91
S'Summer'
p4120
sg93
S'2013'
p4121
sg95
S'2.  To think carefully about a question before giving an answer.    '
p4122
sg81
S'F '
p4123
sg98
g27
sg87
S'2013-07-16 09:45'
p4124
sg100
g101
sg102
g27
sg86
g27
sa(dp4125
g91
S'Mich'
p4126
sg93
S'2013'
p4127
sg95
S'To take responsibility for his personal organisation for lessons. '
p4128
sg81
S'F '
p4129
sg98
g27
sg87
S'2013-07-16 09:46'
p4130
sg100
g139
sg102
g27
sg86
g27
sassssS'Verity_Carman'
p4131
(dp4132
g3
(dp4133
g5
(dp4134
g7
I0
sg8
(lp4135
g27
aS"Skills such as number bonds should be learnt often. Focus Verity on accuracy rather than speed of response.Make learning multisensory where possible especially in maths where repetition of number facts such as tables is required. Where possible, discuss Verity's answers with her and discuss the process she used."
p4136
asg12
S'Sen provision map for Verity Carman'
p4137
sg14
g15
sg16
(lp4138
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Verity Carman'
p4139
sg24
I0
sg25
(lp4140
g27
aS'Verity may take additional time to respond to test information. Verity will benefit from structured small steps with opportunity to rehearse new skills.'
p4141
asg29
(lp4142
g562
ag27
asg32
S'SEN,Provision map,Verity Carman'
p4143
sg34
(lp4144
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp4145
g41
S'17/03/2010'
p4146
sg43
S'Carman'
p4147
sg45
S'Action +'
p4148
sg47
S'2000-10-08'
p4149
sg49
S'EP Report P Rowse'
p4150
sg51
g27
sg52
g1356
sg54
g27
sg56
S'Verity has good verbal comprehension and perceptual reasoning skills but her auditory and working memory are much less well developed. Verity has mild dyslexia. VCI 105 WCI 94 PSI 108 PSI 103'
p4151
sg58
g27
sg59
S'Verity'
p4152
sg61
g27
sg62
S'7CN'
p4153
ssg64
I1
sg65
I0
sg70
S'17-03-2010'
p4154
sg72
I0
sg66
(lp4155
S'Verity has good verbal comprehension and perceptual reasoning skills but her auditory and working memory are much less well developed. Verity has mild dyslexia.'
p4156
aS'Skills such as spelling and should be learnt often. Focus Verity on accuracy rather than speed of response. Paired and shared reading should occur on a daily basis. When reading, allow Verity to attempt a word before offering a prompt and positive comment. Break all tasks especially writing tasks down into manageable chunks and utilise mind mapping to help organise her thoughts before writing.'
p4157
asssg76
(dp4158
g5
(dp4159
g38
(lp4160
(dp4161
g81
S'F '
p4162
sg83
S'Michaelmas term report. Eleanor Barker. '
p4163
sg85
g27
sg86
g27
sg87
S'2010-12-14 14:16'
p4164
sasg37
(lp4165
(dp4166
g91
S'Lent'
p4167
sg93
S'2011'
p4168
sg95
S'To develop more confidence and independence as a learner. '
p4169
sg81
S'F '
p4170
sg98
g27
sg87
S'2010-12-14 14:16'
p4171
sg100
g101
sg102
g27
sg86
g27
sa(dp4172
g91
S'Summer'
p4173
sg93
S'2011'
p4174
sg95
S'To continue to develop relationships with peers. '
p4175
sg81
S'F '
p4176
sg98
g27
sg87
S'2010-12-14 14:16'
p4177
sg100
g101
sg102
g27
sg86
g27
sa(dp4178
g91
S'Mich'
p4179
sg93
S'2011'
p4180
sg95
S'To focus on the task at hand and complete each step completely before commencing the next task. '
p4181
sg81
S'F '
p4182
sg98
g27
sg87
S'2011-09-30 14:16'
p4183
sg100
g139
sg102
g27
sg86
g27
sa(dp4184
g91
S'Lent'
p4185
sg93
S'2012'
p4186
sg95
S'To focus on the task at hand and complete each step completely before commencing the next task. '
p4187
sg81
S'F '
p4188
sg98
g27
sg87
S'2012-07-16 14:16'
p4189
sg100
g101
sg102
g27
sg86
g27
sa(dp4190
g91
S'Mich'
p4191
sg93
S'2012'
p4192
sg95
S'To concentrate and focus for 10 minute blocks in lessons.'
p4193
sg81
S'F '
p4194
sg98
g27
sg87
S'2012-07-19 13:37'
p4195
sg100
g101
sg102
g27
sg86
g27
sa(dp4196
g91
S'Lent'
p4197
sg93
S'2013'
p4198
sg95
S'To ask at least one question per lesson to make sure I understand the information. '
p4199
sg81
S'F '
p4200
sg98
g27
sg87
S'2012-07-19 13:37'
p4201
sg100
g139
sg102
g27
sg86
g27
sa(dp4202
g91
S'Summer'
p4203
sg93
S'2013'
p4204
sg95
S'To ask at least one question per lesson to make sure I understand the information. '
p4205
sg81
S'F '
p4206
sg98
g27
sg87
S'2012-07-19 13:37'
p4207
sg100
g101
sg102
g27
sg86
g27
sa(dp4208
g91
S'Mich'
p4209
sg93
S'2013'
p4210
sg95
S'To self evaulate her performance in lessons and communicate with staff when she needs support. '
p4211
sg81
S'F '
p4212
sg98
g27
sg87
S'2013-07-16 11:25'
p4213
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp4214
(dp4215
g91
S'Michaelmas'
p4216
sg81
S'F '
p4217
sg87
S'2010-12-14 14:15'
p4218
sg145
S'1:1 Learning Specialist Eleanor Barker.'
p4219
sg52
S'2010'
p4220
sg148
S'weekly(x1)'
p4221
sg86
g27
sg150
g27
sa(dp4222
g91
S'Lent'
p4223
sg81
S'F '
p4224
sg87
S'2010-12-14 14:15'
p4225
sg145
S'1:1 Learning Specialist Eleanor Barker.'
p4226
sg52
S'2011'
p4227
sg148
S'weekly(x1)'
p4228
sg86
g27
sg150
g27
sa(dp4229
g91
S'Summer'
p4230
sg81
S'F '
p4231
sg87
S'2010-12-14 14:15'
p4232
sg145
S'1:1 Learning Specialist Eleanor Barker.'
p4233
sg52
S'2011'
p4234
sg148
S'weekly(x1)'
p4235
sg86
g27
sg150
g27
sa(dp4236
g91
S'Michaelmas'
p4237
sg81
S'F '
p4238
sg87
S'2011-11-07 14:15'
p4239
sg145
S'1:1 Learning Specialist Caroline Leahy.'
p4240
sg52
S'2011'
p4241
sg148
S'weekly(x1)'
p4242
sg86
g27
sg150
g27
sassssS'Frederick_Deal'
p4243
(dp4244
g3
(dp4245
g5
(dp4246
g7
I0
sg8
(lp4247
g184
ag185
asg12
S'Sen provision map for Frederick Deal'
p4248
sg14
g15
sg16
(lp4249
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Frederick Deal'
p4250
sg24
I0
sg25
(lp4251
S'Freddie has developed low self esteem. Freddie also has some auditory filtering difficulties and mild bilateral conductive hearing loss that is worse in the left ear. He has difficulty with focus and attention and has some sensory processing and regulation difficulties.'
p4252
aS'Where possible use visuals to support instructions and make sure you have his attention before giving an instruction. Ensure instructions are repeated to Freddie and do not presume he understands what has been said. To support Freddie with focus and concentration chunk activities into workable sections and encourage him to check in with the teacher after completing.'
p4253
asg29
(lp4254
g27
ag31
asg32
S'SEN,Provision map,Frederick Deal'
p4255
sg34
(lp4256
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp4257
g41
S'09-01-2012'
p4258
sg43
S'Deal'
p4259
sg45
S'Action +'
p4260
sg47
S'2006-04-19'
p4261
sg49
S'Cognitive Assessment Dr A Kuczynski 7.11.2011\r<br>L Irwin OT 7.11.2011\r<br>Dr K S Sirimanna Audiological report 20.12.2011\r<br>Prof Ricky Richardson Medical Reportogical report 7.12.2011'
p4262
sg51
g27
sg52
g1470
sg54
g27
sg56
S'Freddie has mild dyspraxia. Freddie has a good vocabulary and is a strong verbal learner. Freddie has some difficulties with spatial reasoning and visual motor integration. He has slightly reduced muscle power in his hands and this will affect his pencip grip and control. He has great difficulty creating consistent size and space when writing. His written output may not match what he intended to say as his fine motor dexterity and accuracy is well below his intellectual ability. He will have difficulty with precision and speed in fine motor activities. Due to these difficulties, Freddie has developed low self esteem. Freddie also has some auditory filtering difficulties and mild bilateral conductive hearing loss that is worse in the left ear. He has difficulty with focus and attention and has some sensory processing and regulation difficulties.'
p4263
sg58
g27
sg59
S'Frederick'
p4264
sg61
g27
sg62
S'2CS'
p4265
ssg64
I1
sg65
I1
sg66
(lp4266
S'He has slightly reduced muscle power in his hands and this will affect his pencip grip and control. He has great difficulty creating consistent size and space when writing. His written output may not match what he intended to say as his fine motor dexterity and accuracy is well below his intellectual ability. He will have difficulty with precision and speed in fine motor activities.'
p4267
aS'Regarding difficulties with handwriting, focus on the lesson objective. If the main lesson objective is not writing etc, then scaffold this part of his lesson to ensure he can engage and succeed as would another child. Some examples may be having the writing dotted on his work sample, having the transcript written on a small whiteboard or piece of paper on his desk, having an alphabet strip on his desk to help with memory and letter formation. If the lesson objective is independent writing, have him do as much as possible and then scribe. It is helpful to rotate throughout as to maintain concentration and confidence. i.e. Word/word, sentence/sentence, paragraph/paragraph. Always praise Freddie for attempting handwritten work and discuss with him how his work is presented. Provide templates and visual prompts to help him organise his work and thoughts especially with direction and spacing of writing.'
p4268
asg70
S'07-11-2011'
p4269
sg72
I0
sg73
(lp4270
g211
ag212
asssg76
(dp4271
g5
(dp4272
g38
(lp4273
(dp4274
g81
S'M '
p4275
sg83
S'NB. HSM, LG and Mrs Deal. Meeting to review assessments and discuss support. '
p4276
sg85
g27
sg86
g27
sg87
S'2012-01-11 19:56'
p4277
sa(dp4278
g81
S'M '
p4279
sg83
S'Telephone conversation between NB and Mrs D'
p4280
sg85
g27
sg86
g27
sg87
S'2012-03-01 19:56'
p4281
sasg37
(lp4282
(dp4283
g91
S'Mich'
p4284
sg93
S'2011'
p4285
sg95
S'To listen carefully to instructions'
p4286
sg81
S'M '
p4287
sg98
g27
sg87
S'2012-01-06 19:56'
p4288
sg100
g101
sg102
g27
sg86
S'LGA'
p4289
sa(dp4290
g91
S'Lent'
p4291
sg93
S'2012'
p4292
sg95
S'Follow an instruction within 30 seconds of it being given.'
p4293
sg81
S'M '
p4294
sg98
g27
sg87
S'2012-02-06 19:56'
p4295
sg100
g101
sg102
g27
sg86
g27
sa(dp4296
g91
S'Mich'
p4297
sg93
S'2012'
p4298
sg95
S'To sit appropriately during carpet time and put his hand up to share his ideas.'
p4299
sg81
S'M '
p4300
sg98
g27
sg87
S'2012-07-19 13:39'
p4301
sg100
g101
sg102
g27
sg86
g27
sa(dp4302
g91
S'Lent'
p4303
sg93
S'2013'
p4304
sg95
S'To give teachers clear and truthful answers. '
p4305
sg81
S'M '
p4306
sg98
g27
sg87
S'2012-07-19 13:39'
p4307
sg100
g101
sg102
g27
sg86
g27
sa(dp4308
g91
S'Summer'
p4309
sg93
S'2013'
p4310
sg95
S'To follow an instruction within 3 seconds without being distracted.'
p4311
sg81
S'M '
p4312
sg98
g27
sg87
S'2012-07-19 13:39'
p4313
sg100
g101
sg102
g27
sg86
g27
sa(dp4314
g91
S'Mich'
p4315
sg93
S'2013'
p4316
sg95
S'To listen well during carpet time and be an active participant in lessons.'
p4317
sg81
S'M '
p4318
sg98
g27
sg87
S'2013-07-16 09:35'
p4319
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp4320
(dp4321
g91
S'Lent'
p4322
sg81
S'M '
p4323
sg87
S'2012-01-01 10:10'
p4324
sg145
S'Guided reading group. '
p4325
sg52
S'2012'
p4326
sg148
S'weekly(x1)'
p4327
sg86
g27
sg150
S'AST'
p4328
sa(dp4329
g91
S'Lent'
p4330
sg81
S'M '
p4331
sg87
S'2012-01-01 10:10'
p4332
sg145
S'Spelling support.'
p4333
sg52
S'2012'
p4334
sg148
S'weekly(x1)'
p4335
sg86
g27
sg150
S'AST'
p4336
sa(dp4337
g91
S'Lent'
p4338
sg81
S'M '
p4339
sg87
S'2012-01-01 10:10'
p4340
sg145
S'Numeracy support.'
p4341
sg52
S'2012'
p4342
sg148
S'weekly(x1)'
p4343
sg86
g27
sg150
S'AST'
p4344
sa(dp4345
g91
S'Lent'
p4346
sg81
S'M '
p4347
sg87
S'2012-01-11 15:39'
p4348
sg145
S'1:1 support focusing on self esteem and confidence. '
p4349
sg52
S'2012'
p4350
sg148
S'weekly(x2)'
p4351
sg86
g27
sg150
S'NB'
p4352
sa(dp4353
g91
S'Lent'
p4354
sg81
S'M '
p4355
sg87
S'2012-01-11 15:39'
p4356
sg145
S'1:1 OT with Laura Irwin. '
p4357
sg52
S'2012'
p4358
sg148
S'weekly(x1)'
p4359
sg86
g27
sg150
g27
sa(dp4360
g91
S'Lent'
p4361
sg81
S'M '
p4362
sg87
S'2012-01-11 19:56'
p4363
sg145
S'Wandswroth Hearing Support. '
p4364
sg52
S'2012'
p4365
sg148
S'Monthly(x1)'
p4366
sg86
g27
sg150
g27
sa(dp4367
g91
S'Lent'
p4368
sg81
S'M '
p4369
sg87
S'2012-02-06 10:10'
p4370
sg145
S'Freddie is trialling a hearing device provided by Wandsworth Hearing. '
p4371
sg52
S'2012'
p4372
sg148
S'Daily'
p4373
sg86
g27
sg150
g27
sa(dp4374
g91
S'Summer'
p4375
sg81
S'M '
p4376
sg87
S'2012-05-02 10:10'
p4377
sg145
S'Guided reading group. '
p4378
sg52
S'2012'
p4379
sg148
S'weekly(x1)'
p4380
sg86
g27
sg150
S'KM'
p4381
sa(dp4382
g91
S'Summer'
p4383
sg81
S'M '
p4384
sg87
S'2012-05-02 10:10'
p4385
sg145
S'Spelling support. '
p4386
sg52
S'2012'
p4387
sg148
S'weekly(x1)'
p4388
sg86
g27
sg150
S'KM'
p4389
sa(dp4390
g91
S'Summer'
p4391
sg81
S'M '
p4392
sg87
S'2012-05-02 10:10'
p4393
sg145
S'Numeracy support.'
p4394
sg52
S'2012'
p4395
sg148
S'weekly(x1)'
p4396
sg86
g27
sg150
S'KM'
p4397
sassssS'Luca_Freeman'
p4398
(dp4399
g3
(dp4400
g5
(dp4401
g7
I0
sg8
(lp4402
g1456
ag27
asg12
S'Sen provision map for Luca Freeman'
p4403
sg14
g15
sg16
(lp4404
g3781
ag3782
asg20
g21
sg22
S'Special Needs Provision Map for Luca Freeman'
p4405
sg24
I0
sg25
(lp4406
g1461
ag27
asg29
(lp4407
g3788
ag3789
asg32
S'SEN,Provision map,Luca Freeman'
p4408
sg34
(lp4409
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp4410
g41
S'07-09-2011'
p4411
sg43
S'Freeman'
p4412
sg45
S'Action +'
p4413
sg47
S'2003-11-08'
p4414
sg49
S'EP Report Annie Mitchell - 10.08.2011'
p4415
sg51
g27
sg52
g201
sg54
g27
sg56
S'Luca has a full scale IQ of 131 which places him in the superior range. He has extremely high verbal skills (144) and superior non verbal skills (125). Luca has some specific difficulties with viual motor integration that is affecting his ability to produce accurate writing and spelling at speed. He over relies on the phonic strategy. His slight weakness with visual memory means he often knows the word is spelt incorrectly however cannot remember the correct sequence of letters which is very frustrating for Luca. Luca may be hampered by trying to perfect all of his work as he does not want to have work samples with errors. His numeracy, reading, comprehension are all high average.'
p4416
sg58
g27
sg59
S'Luca'
p4417
sg61
g27
sg62
S'4CN'
p4418
ssg64
I1
sg65
I0
sg70
S'10-08-2011'
p4419
sg72
I0
sg66
(lp4420
g1475
ag1476
asssg76
(dp4421
g5
(dp4422
g38
(lp4423
(dp4424
g81
S'M '
p4425
sg83
S'NB and Mrs Freeman. Discussion of laptop use in year 4. '
p4426
sg85
g27
sg86
g27
sg87
S'2012-04-30 15:40'
p4427
sasg37
(lp4428
(dp4429
g91
S'Mich'
p4430
sg93
S'2012'
p4431
sg95
S'To increase the accuracy of spelling word endings.'
p4432
sg81
S'M '
p4433
sg98
g27
sg87
S'2011-09-20 16:34'
p4434
sg100
g101
sg102
S'Positive praise.Taking focus away from handwriting and spelling in other lessons to encourgage creatvity through scaffolded activities and ICT work.'
p4435
sg86
S'CDY'
p4436
sa(dp4437
g91
S'Mich'
p4438
sg93
S'2012'
p4439
sg95
S'To display greater confidence in his own ability.'
p4440
sg81
S'M '
p4441
sg98
g27
sg87
S'2011-09-20 16:34'
p4442
sg100
g101
sg102
S'Positive praise.Taking focus away from handwriting and spelling in other lessons to encourgage creatvity through scaffolded activities and ICT work.'
p4443
sg86
g27
sa(dp4444
g91
S'Lent'
p4445
sg93
S'2012'
p4446
sg95
S'To increase the accuracy of spelling word endings.'
p4447
sg81
S'M '
p4448
sg98
g27
sg87
S'2012-01-27 08:57'
p4449
sg100
g139
sg102
g27
sg86
S'CDY'
p4450
sa(dp4451
g91
S'Lent'
p4452
sg93
S'2012'
p4453
sg95
S'To focus in lessons quickly (within 2 minutes) and complete tasks as directed by the teacher.'
p4454
sg81
S'M '
p4455
sg98
g27
sg87
S'2012-01-27 08:57'
p4456
sg100
g101
sg102
g27
sg86
g27
sa(dp4457
g91
S'Mich'
p4458
sg93
S'2012'
p4459
sg95
S'1. To improve accuracy of spelling word endings.'
p4460
sg81
S'M '
p4461
sg98
g27
sg87
S'2012-07-19 13:46'
p4462
sg100
g101
sg102
g27
sg86
g27
sa(dp4463
g91
S'Mich'
p4464
sg93
S'2012'
p4465
sg95
S'2. To maintain focus throughout lessons.'
p4466
sg81
S'M '
p4467
sg98
g27
sg87
S'2012-07-19 13:46'
p4468
sg100
g101
sg102
g27
sg86
g27
sa(dp4469
g91
S'Lent'
p4470
sg93
S'2013'
p4471
sg95
S'To begin to use laptop in school effectively. '
p4472
sg81
S'M '
p4473
sg98
g27
sg87
S'2012-07-19 13:46'
p4474
sg100
g101
sg102
g27
sg86
g27
sa(dp4475
g91
S'Summer'
p4476
sg93
S'2013'
p4477
sg95
S'General: to remember to keep his laptop charged.                                                                                   '
p4478
sg81
S'M '
p4479
sg98
g27
sg87
S'2012-07-19 13:46'
p4480
sg100
g101
sg102
g27
sg86
g27
sa(dp4481
g91
S'Summer'
p4482
sg93
S'2013'
p4483
sg95
S' English - to practise my high frequency spellings every week                                                                               '
p4484
sg81
S'M '
p4485
sg98
g27
sg87
S'2012-07-19 13:46'
p4486
sg100
g101
sg102
g27
sg86
g27
sa(dp4487
g91
S'Mich'
p4488
sg93
S'2013'
p4489
sg95
S'English: To remember to use capital letters and full stops in my sentences.'
p4490
sg81
S'M '
p4491
sg98
g27
sg87
S'2013-07-16 10:07'
p4492
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp4493
(dp4494
g91
S'Michaelmas'
p4495
sg81
S'M '
p4496
sg87
S'2011-09-20 16:34'
p4497
sg145
S'1:1 Literacy Support lesson with Eleanor Barker. '
p4498
sg52
S'2011'
p4499
sg148
S'weekly(x1)'
p4500
sg86
g27
sg150
g27
sa(dp4501
g91
S'Lent'
p4502
sg81
S'M '
p4503
sg87
S'2011-09-20 16:34'
p4504
sg145
S'1:1 Literacy Support lesson with Eleanor Barker.'
p4505
sg52
S'2012'
p4506
sg148
S'weekly(x1)'
p4507
sg86
g27
sg150
g27
sa(dp4508
g91
S'Summer'
p4509
sg81
S'M '
p4510
sg87
S'2012-02-01 10:35'
p4511
sg145
S'Spelling support class.'
p4512
sg52
S'2012'
p4513
sg148
S'weekly(x1)'
p4514
sg86
g27
sg150
S'NB'
p4515
sa(dp4516
g91
S'Michaelmas'
p4517
sg81
S'M '
p4518
sg87
S'2012-02-06 10:35'
p4519
sg145
S'Spelling support class. '
p4520
sg52
S'2011'
p4521
sg148
S'weekly(x1)'
p4522
sg86
g27
sg150
S'NB'
p4523
sa(dp4524
g91
S'Lent'
p4525
sg81
S'M '
p4526
sg87
S'2012-02-06 10:35'
p4527
sg145
S'Spelling support class. '
p4528
sg52
S'2012'
p4529
sg148
S'weekly(x1)'
p4530
sg86
g27
sg150
S'NB'
p4531
sa(dp4532
g91
S'Summer'
p4533
sg81
S'M '
p4534
sg87
S'2012-04-09 16:34'
p4535
sg145
S'1:1 Literacy Support lesson with Eleanor Barker.'
p4536
sg52
S'2012'
p4537
sg148
S'weekly(x1)'
p4538
sg86
g27
sg150
g27
sa(dp4539
g91
S'Michaelmas'
p4540
sg81
S'M '
p4541
sg87
S'2013-07-08 16:10'
p4542
sg145
S'1:1 Literacy support with EB'
p4543
sg52
S'2012'
p4544
sg148
S'weekly(x1)'
p4545
sg86
g27
sg150
g27
sa(dp4546
g91
S'Lent'
p4547
sg81
S'M '
p4548
sg87
S'2013-07-08 16:10'
p4549
sg145
S'1:1 Literacy support with EB'
p4550
sg52
S'2013'
p4551
sg148
S'weekly(x1)'
p4552
sg86
g27
sg150
g27
sa(dp4553
g91
S'Summer'
p4554
sg81
S'M '
p4555
sg87
S'2013-07-08 16:10'
p4556
sg145
S'1:1 Literacy support with EB'
p4557
sg52
S'2013'
p4558
sg148
S'weekly(x1)'
p4559
sg86
g27
sg150
g27
sassssS'Sebastian_Harper'
p4560
(dp4561
g3
(dp4562
g5
(dp4563
g7
I0
sg12
S'Sen provision map for Sebastian Harper'
p4564
sg22
S'Special Needs Provision Map for Sebastian Harper'
p4565
sg20
g21
sg14
g15
sg24
I0
sg25
(lp4566
g27
aS'Multi-sensory approaches that use a combination of auditory information as well as visual and kinaesthetic information support learners like Sebastian most effectively. Seat Sebastian at the front of the class or somewhere he can clearly see the teacher and the board, remind him to stay focused, possibly using a reward system if necessary.'
p4567
asg32
S'SEN,Provision map,Sebastian Harper'
p4568
sg34
(lp4569
g36
ag37
ag38
asg39
(dp4570
g41
S'17/06/2010'
p4571
sg43
S'Harper'
p4572
sg45
S'Action'
p4573
sg47
S'2002-09-21'
p4574
sg49
S'Assessed by AS on 17.6.10. DST-J\r<br>Assessed by KAM Jan 2011\r<br>EP Report Annie Mitchell 05.05.2011\r<br>Dr Sue Fowler 28.08.2012'
p4575
sg51
g27
sg52
g384
sg54
S'05-05-2011'
p4576
sg56
S"Sebastian was referred following concerns raised by the classroom teacher regarding his progress in all areas. Seb's reading is behind his chronological age. His attention and concentration varies dramatically. He may have an auditory processing problem. Seb reads so quickly that he makes substitutions and does not fully understand text."
p4577
sg58
g27
sg59
S'Sebastian'
p4578
sg61
S'25%'
p4579
sg62
S'5CE'
p4580
ssg64
I1
sg65
I0
sg70
S'05-05-2011'
p4581
sg72
I0
sg66
(lp4582
S'Sebastian has specific difficulties with spelling and reading comprehension.'
p4583
aS'Keep verbal instructions as brief as possible and write them on the board where feasible, frequently check Sebastian has understood. Ensure that verbal information is delivered in small chunks so that Sebastian has the chance to digest the information.   Support Sebastian with Reading Comprehension work by encouraging him to highlight key words and make notes in the margin as appropriate. Encourage Sebastian to mind map his ideas for essay writing to support his memory. Support planning of work, where appropriate, with writing frames. Bear in mind that Sebastian may need longer than his peers to complete tasks.Encourage Sebastian to slow down when reading and not guess at words. Help him to sound out words and break them into syllables.'
p4584
asssg76
(dp4585
g5
(dp4586
g38
(lp4587
(dp4588
g81
S'M '
p4589
sg83
S'Review meeting held between NB, CAE, FS and Mrs Harper regarding Sebs mid term assessment in literacy and numeracy. Both scores were very low. EP assessment will be completed as previously suggested. '
p4590
sg85
S'15-02-2011'
p4591
sg86
g27
sg87
S'2010-12-13 13:48'
p4592
sa(dp4593
g81
S'M '
p4594
sg83
S'KAM, FS and Mrs Harper. Review of assessment completed by KAM. '
p4595
sg85
g27
sg86
g27
sg87
S'2011-01-26 14:02'
p4596
sa(dp4597
g81
S'M '
p4598
sg83
S'NB, CAE, FS and Mrs Harper. Review of in school assessment and suggestion for EP report. '
p4599
sg85
g27
sg86
g27
sg87
S'2011-02-15 14:02'
p4600
sa(dp4601
g81
S'M '
p4602
sg83
S'Mrs Harper and FS. Summary of progress in English Comprehension. '
p4603
sg85
g27
sg86
g27
sg87
S'2011-03-04 14:02'
p4604
sa(dp4605
g81
S'M '
p4606
sg83
S'Mrs Harper and GS. Discussion of Maths assessments. '
p4607
sg85
g27
sg86
g27
sg87
S'2011-07-01 14:01'
p4608
sa(dp4609
g81
S'M '
p4610
sg83
S'FS and Mrs Harper. Discussion of English results. '
p4611
sg85
g27
sg86
g27
sg87
S'2011-07-05 14:01'
p4612
sa(dp4613
g81
S'M '
p4614
sg83
S'NB and Mrs Harper. Suggestions for future support. '
p4615
sg85
g27
sg86
g27
sg87
S'2011-07-07 14:04'
p4616
sa(dp4617
g81
S'M '
p4618
sg83
S'NB and Mrs Harper. Review of support and progress. '
p4619
sg85
g27
sg86
g27
sg87
S'2011-09-12 14:04'
p4620
sa(dp4621
g81
S'M '
p4622
sg83
S'NB and Mrs Harper. Review of support and current situation'
p4623
sg85
g27
sg86
g27
sg87
S'2012-09-21 16:36'
p4624
sasg37
(lp4625
(dp4626
g91
S'Mich'
p4627
sg93
S'2011'
p4628
sg95
S'To discuss his understanding of a task with a talk partner fmember. ONGOINGrom the extension group.'
p4629
sg81
S'M '
p4630
sg98
g27
sg87
S'2010-12-13 13:48'
p4631
sg100
g101
sg102
g27
sg86
g27
sa(dp4632
g91
S'Mich'
p4633
sg93
S'2011'
p4634
sg95
S'Discuss understanding of a task with a teacher. ONGOING'
p4635
sg81
S'M '
p4636
sg98
g27
sg87
S'2011-09-21 12:27'
p4637
sg100
g101
sg102
S'Question understanding.Allow time to process information.Encourage Seb to take his time.'
p4638
sg86
S'AP'
p4639
sa(dp4640
g91
S'Lent'
p4641
sg93
S'2012'
p4642
sg95
S'Maths: Work slowly through a calculation and check his answer is a sensible one and ask for help if unsure.'
p4643
sg81
S'M '
p4644
sg98
g27
sg87
S'2012-01-25 16:48'
p4645
sg100
g139
sg102
g27
sg86
S'AP'
p4646
sa(dp4647
g91
S'Lent'
p4648
sg93
S'2012'
p4649
sg95
S'Literacy: Re read and check his work carefully to make sure that his sentences make sense.'
p4650
sg81
S'M '
p4651
sg98
g27
sg87
S'2012-01-25 16:48'
p4652
sg100
g139
sg102
g27
sg86
g27
sa(dp4653
g91
S'Mich'
p4654
sg93
S'2012'
p4655
sg95
S'Maths: Work slowly through a calculation and check his answer is a sensible one and ask for help if unsure.'
p4656
sg81
S'M '
p4657
sg98
g27
sg87
S'2012-07-19 14:02'
p4658
sg100
g139
sg102
g27
sg86
g27
sa(dp4659
g91
S'Mich'
p4660
sg93
S'2012'
p4661
sg95
S'Literacy: Re read and check his work carefully to make sure that his sentences make sense.'
p4662
sg81
S'M '
p4663
sg98
g27
sg87
S'2012-07-19 14:02'
p4664
sg100
g139
sg102
g27
sg86
g27
sa(dp4665
g91
S'Lent'
p4666
sg93
S'2013'
p4667
sg95
S'Maths: Work slowly through a calculation and check his answer is a sensible one and ask for help if unsure.'
p4668
sg81
S'M '
p4669
sg98
g27
sg87
S'2012-07-19 14:02'
p4670
sg100
g139
sg102
g27
sg86
g27
sa(dp4671
g91
S'Summer'
p4672
sg93
S'2013'
p4673
sg95
S'Maths: Work slowly through a calculation and check his answer is a sensible one and ask for help if unsure.'
p4674
sg81
S'M '
p4675
sg98
g27
sg87
S'2012-07-19 14:02'
p4676
sg100
g139
sg102
g27
sg86
g27
sa(dp4677
g91
S'Lent'
p4678
sg93
S'2013'
p4679
sg95
S'Literacy: To accurately read the question twice to make sure I apply the correct processes when answering questions. '
p4680
sg81
S'M '
p4681
sg98
g27
sg87
S'2012-07-19 14:02'
p4682
sg100
g139
sg102
g27
sg86
g27
sa(dp4683
g91
S'Lent'
p4684
sg93
S'2013'
p4685
sg95
S'2. To accurately read the question twice to make sure I apply the correct processes when answering questions. '
p4686
sg81
S'M '
p4687
sg98
g27
sg87
S'2012-07-19 14:02'
p4688
sg100
g139
sg102
g27
sg86
g27
sa(dp4689
g91
S'Summer'
p4690
sg93
S'2013'
p4691
sg95
S'Literacy: To accurately read the question twice to make sure I apply the correct processes when answering questions. '
p4692
sg81
S'M '
p4693
sg98
g27
sg87
S'2012-07-19 14:02'
p4694
sg100
g139
sg102
g27
sg86
g27
sa(dp4695
g91
S'Summer'
p4696
sg93
S'2013'
p4697
sg95
S'2. To accurately read the question twice to make sure I apply the correct processes when answering questions. '
p4698
sg81
S'M '
p4699
sg98
g27
sg87
S'2012-07-19 14:02'
p4700
sg100
g139
sg102
g27
sg86
g27
sa(dp4701
g91
S'Mich'
p4702
sg93
S'2013'
p4703
sg95
S'1. To check my answer at least one time before handing it in to make sure it makes sense and answers the question.                                                                '
p4704
sg81
S'M '
p4705
sg98
g27
sg87
S'2013-07-16 10:38'
p4706
sg100
g139
sg102
g27
sg86
g27
sa(dp4707
g91
S'Mich'
p4708
sg93
S'2013'
p4709
sg95
S' 2. To accurately read the question twice to make sure I apply the correct processes when answering questions.                                                                                           '
p4710
sg81
S'M '
p4711
sg98
g27
sg87
S'2013-07-16 10:38'
p4712
sg100
g139
sg102
g27
sg86
g27
sa(dp4713
g91
S'Mich'
p4714
sg93
S'2013'
p4715
sg95
S'3. To independently apply the learning skills needed after instruction has been given by a teacher. (60%)                                                                                      '
p4716
sg81
S'M '
p4717
sg98
g27
sg87
S'2013-07-16 10:38'
p4718
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp4719
(dp4720
g91
S'Michaelmas'
p4721
sg81
S'M '
p4722
sg87
S'2010-12-13 13:43'
p4723
sg145
S'Spelling support sessions focusing on spelling strategies. '
p4724
sg52
S'2010'
p4725
sg148
S'weekly(x1)'
p4726
sg86
g27
sg150
S'NB'
p4727
sa(dp4728
g91
S'Michaelmas'
p4729
sg81
S'M '
p4730
sg87
S'2010-12-13 13:43'
p4731
sg145
S'Reading comprehension group '
p4732
sg52
S'2010'
p4733
sg148
S'weekly(x1)'
p4734
sg86
g27
sg150
S'KM'
p4735
sa(dp4736
g91
S'Lent'
p4737
sg81
S'M '
p4738
sg87
S'2010-12-13 13:43'
p4739
sg145
S'Spelling support sessions focusing on spelling strategies.'
p4740
sg52
S'2011'
p4741
sg148
S'weekly(x1)'
p4742
sg86
g27
sg150
S'NB'
p4743
sa(dp4744
g91
S'Lent'
p4745
sg81
S'M '
p4746
sg87
S'2010-12-13 13:43'
p4747
sg145
S'Reading comprehension group'
p4748
sg52
S'2011'
p4749
sg148
S'weekly(x1)'
p4750
sg86
g27
sg150
S'KM'
p4751
sa(dp4752
g91
S'Summer'
p4753
sg81
S'M '
p4754
sg87
S'2010-12-13 13:43'
p4755
sg145
S'Reading comprehension group'
p4756
sg52
S'2011'
p4757
sg148
S'weekly(x1)'
p4758
sg86
g27
sg150
S'AST'
p4759
sa(dp4760
g91
S'Michaelmas'
p4761
sg81
S'M '
p4762
sg87
S'2010-12-13 13:43'
p4763
sg145
S'Reading comprehension group'
p4764
sg52
S'2011'
p4765
sg148
S'weekly(x1)'
p4766
sg86
g27
sg150
S'NB'
p4767
sa(dp4768
g91
S'Lent'
p4769
sg81
S'M '
p4770
sg87
S'2012-02-11 10:18'
p4771
sg145
S'Reading comprehension support.'
p4772
sg52
S'2012'
p4773
sg148
S'weekly(x1)'
p4774
sg86
g27
sg150
S'NB'
p4775
sa(dp4776
g91
S'Summer'
p4777
sg81
S'M '
p4778
sg87
S'2012-02-11 10:18'
p4779
sg145
S'Reading comprehension support.'
p4780
sg52
S'2012'
p4781
sg148
S'weekly(x1)'
p4782
sg86
g27
sg150
S'NB'
p4783
sa(dp4784
g91
S'Summer'
p4785
sg81
S'M '
p4786
sg87
S'2012-02-11 10:18'
p4787
sg145
S'1:1 Lindsey Copeman.'
p4788
sg52
S'2012'
p4789
sg148
S'weekly(x1)'
p4790
sg86
g27
sg150
g27
sassssS'Lachlan_Clunie'
p4791
(dp4792
g3
(dp4793
g5
(dp4794
g7
I0
sg8
(lp4795
S"Cameron's mental arithmetic is in the high average range however his written arithmetic abilities are lower."
p4796
aS"To support Cameron's difficulties with written arithmetic, provide squared paper for organisation and colour code where appropriate i.e.. different colours for the different operations."
p4797
asg12
S'Sen provision map for Lachlan Clunie'
p4798
sg14
g15
sg16
(lp4799
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Lachlan Clunie'
p4800
sg24
I0
sg25
(lp4801
g27
aS' Cameron will benefit from using visual organisers to help with his working memory. Diagrams, pictures, images, flow charts etc should be used where possible. As Cameron has some auditory processing difficulties, it is suggested that if possible he sit near the front of the room and away from possible distractions. When communicating be sure to speak at a steady pace.  New and lengthy information should be to be chunked. Additionally if he is provided with a specific set of tasks (2 or 3) it may relieve any anxiety associated with trying to remember additional tasks.'
p4802
asg29
(lp4803
g27
ag31
asg32
S'SEN,Provision map,Lachlan Clunie'
p4804
sg34
(lp4805
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp4806
g41
g27
sg43
S'Clunie'
p4807
sg45
g27
sg47
S'2005-01-16'
p4808
sg49
g27
sg51
g27
sg52
g53
sg54
g27
sg56
g27
sg58
g27
sg59
S'Lachlan'
p4809
sg61
g27
sg62
S'3CS'
p4810
ssg64
I0
sg65
I0
sg66
(lp4811
S'He has specific learning difficulties of a dyslexic nature.'
p4812
aS'Cameron has some word retrieval problems therefore he may not be able to use the word/s which accurately communicate his thoughts. Lists of synonyms and use of a thesaurus will help. To support punctuation, he could record information in dot point and then transform this into longer sentences. Additionally, he should be encouraged to read aloud what he has written to hear the natural breaks.'
p4813
asg70
g392
sg72
I0
sg73
(lp4814
g75
ag27
asssg76
(dp4815
g5
(dp4816
g38
(lp4817
(dp4818
g81
S'M '
p4819
sg83
S'NB, HSM, HF and Mrs Clunie. Discussion of EP report. '
p4820
sg85
g27
sg86
g27
sg87
S'2012-06-22 10:23'
p4821
sasg37
(lp4822
(dp4823
g91
S'Mich'
p4824
sg93
S'2012'
p4825
sg95
S'To learn 5 tricky words a week (set by the class teacher)'
p4826
sg81
S'M '
p4827
sg98
g27
sg87
S'2012-07-19 13:35'
p4828
sg100
g101
sg102
g27
sg86
g27
sa(dp4829
g91
S'Lent'
p4830
sg93
S'2013'
p4831
sg95
S'1. To put his hand up before answering a question.                                                                                '
p4832
sg81
S'M '
p4833
sg98
g27
sg87
S'2012-07-19 13:35'
p4834
sg100
g101
sg102
g27
sg86
g27
sa(dp4835
g91
S'Lent'
p4836
sg93
S'2013'
p4837
sg95
S'2. Sound out words accurately.                                                                              '
p4838
sg81
S'M '
p4839
sg98
g27
sg87
S'2012-07-19 13:35'
p4840
sg100
g101
sg102
g27
sg86
g27
sassssS'Alexander_Hoskins'
p4841
(dp4842
g3
(dp4843
g5
(dp4844
g7
I0
sg8
(lp4845
g1548
ag1549
asg12
S'Sen provision map for Alexander Hoskins'
p4846
sg14
g15
sg16
(lp4847
g27
ag2032
asg20
g21
sg22
S'Special Needs Provision Map for Alexander Hoskins'
p4848
sg24
I0
sg25
(lp4849
g27
ag1834
asg29
(lp4850
g2036
ag2037
asg32
S'SEN,Provision map,Alexander Hoskins'
p4851
sg34
(lp4852
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp4853
g41
S'17-05-2010'
p4854
sg43
S'Hoskins'
p4855
sg45
S'Action'
p4856
sg47
S'2004-10-27'
p4857
sg49
S"Dirk Flower -No formal report provided to Thomas's. This was completed in Michaelmas term 2010.\r<br>Dr Zoe Graham 11.03.2011 Review Report."
p4858
sg51
g27
sg52
g53
sg54
g27
sg56
S"Alexander has displayed some challenging behaviours whilst at school. At times he is very distracting in the classroom and fails to follow the teacher's instructions. Alexander can become very angry and defiant. He has difficulty forming relationships with peers and communicating his emotions and feelings. His ability to maintain attention can waiver. Alex has been diagnosed with Encopresis. This affects his ability to regulate his bowels and he often has toileting accidents."
p4859
sg58
g27
sg59
S'Alexander'
p4860
sg61
g27
sg62
S'3CE'
p4861
ssg64
I1
sg65
I0
sg66
(lp4862
g2046
ag2047
asg70
g392
sg72
I0
sg73
(lp4863
g1852
ag1853
asssg76
(dp4864
g5
(dp4865
g38
(lp4866
(dp4867
g81
S'M '
p4868
sg83
S'Mrs Hoskins and Mrs MacInnis. Toileting concerns. '
p4869
sg85
g27
sg86
g27
sg87
S'2010-03-17 15:53'
p4870
sa(dp4871
g81
S'M '
p4872
sg83
S'PB and Mrs Hoskins. Social skilling concerns from Mum. '
p4873
sg85
g27
sg86
g27
sg87
S'2010-03-25 15:52'
p4874
sa(dp4875
g81
S'M '
p4876
sg83
S'Mrs Hoskins and SD. Discussion regarding Alex and social skilling/friendships. '
p4877
sg85
g27
sg86
g27
sg87
S'2010-04-23 15:51'
p4878
sa(dp4879
g81
S'M '
p4880
sg83
S'Mrs Dennis and Mrs Hoskins. Informing re progress and concerns. '
p4881
sg85
g27
sg86
g27
sg87
S'2010-06-07 10:49'
p4882
sa(dp4883
g81
S'M '
p4884
sg83
S'Meeting with RM, HSM, NB and Dr Zoe Graham. Initial meeting regarding Alexs fear of the toilet. '
p4885
sg85
S'11-02-2011'
p4886
sg86
g27
sg87
S'2010-06-10 10:49'
p4887
sa(dp4888
g81
S'M '
p4889
sg83
S'Meeting with RM, HSM, NB and Dr Zoe Graham. 2nd meeting to discuss progress and support which will be provided by Dr Zoe Graham. '
p4890
sg85
S'08-03-2011'
p4891
sg86
g27
sg87
S'2010-06-10 10:49'
p4892
sa(dp4893
g81
S'M '
p4894
sg83
S'HSM and Mrs Hoskins. Information regaridng encoprecis. '
p4895
sg85
g27
sg86
g27
sg87
S'2010-09-10 15:54'
p4896
sa(dp4897
g81
S'M '
p4898
sg83
S'Dirk Flower, CAE, NB and HSM. Review of observations. '
p4899
sg85
g27
sg86
g27
sg87
S'2010-10-13 10:49'
p4900
sa(dp4901
g81
S'M '
p4902
sg83
S'NB, Dirk Flower, RM, Mrs Hoskins. Review of observations and findings. '
p4903
sg85
g27
sg86
g27
sg87
S'2010-11-08 15:47'
p4904
sa(dp4905
g81
S'M '
p4906
sg83
S'Dirk Flower, Mrs Hoskins and HSM. Review of observations. '
p4907
sg85
g27
sg86
g27
sg87
S'2010-12-07 10:49'
p4908
sa(dp4909
g81
S'M '
p4910
sg83
S'Dr ZG, NB, RM, HSM. Review of support and progress. '
p4911
sg85
g27
sg86
g27
sg87
S'2011-05-17 10:49'
p4912
sa(dp4913
g81
S'M '
p4914
sg83
S'Meeting with RM, HSM, NB and Dr Zoe Graham. Initial meeting regarding Alexs fear of the toilet. '
p4915
sg85
S'11-02-2011'
p4916
sg86
g27
sg87
S'2011-06-10 10:49'
p4917
sa(dp4918
g81
S'M '
p4919
sg83
S'S Henderson, NB, RM, HSM. Review of support provided to date. '
p4920
sg85
g27
sg86
g27
sg87
S'2011-06-24 10:49'
p4921
sa(dp4922
g81
S'M '
p4923
sg83
S'NB, CMC and Mrs Hoskins. Review of progress and support for Alex. '
p4924
sg85
S'20-11-2012'
p4925
sg86
g27
sg87
S'2012-11-20 14:01'
p4926
sasg37
(lp4927
(dp4928
g91
S'Lent'
p4929
sg93
S'2011'
p4930
sg95
S'To appropriately communicate with an adult when he has an emotional, behavioural or academic need. '
p4931
sg81
S'M '
p4932
sg98
g27
sg87
S'2010-12-13 10:51'
p4933
sg100
g139
sg102
g27
sg86
g27
sa(dp4934
g91
S'Summer'
p4935
sg93
S'2011'
p4936
sg95
S'To appropriately communicate with an adult when he has an emotional, behavioural or academic need. ONGOING'
p4937
sg81
S'M '
p4938
sg98
g27
sg87
S'2011-06-10 10:49'
p4939
sg100
g101
sg102
g27
sg86
g27
sa(dp4940
g91
S'Mich'
p4941
sg93
S'2011'
p4942
sg95
S'To appropriately communicate with an adult and his peers when he has an emotional, behavioural or academic need.'
p4943
sg81
S'M '
p4944
sg98
g27
sg87
S'2011-10-12 09:49'
p4945
sg100
g101
sg102
g27
sg86
S'CEB'
p4946
sa(dp4947
g91
S'Lent'
p4948
sg93
S'2012'
p4949
sg95
S'To communicate appropriately with his peers during paired/ group work.'
p4950
sg81
S'M '
p4951
sg98
g27
sg87
S'2012-02-01 09:49'
p4952
sg100
g101
sg102
g27
sg86
g27
sa(dp4953
g91
S'Mich'
p4954
sg93
S'2012'
p4955
sg95
S'To attempt new tasks before deciding that he does not want to complete them.'
p4956
sg81
S'M '
p4957
sg98
g27
sg87
S'2012-07-19 14:01'
p4958
sg100
g101
sg102
g27
sg86
g27
sa(dp4959
g91
S'Lent'
p4960
sg93
S'2012'
p4961
sg95
S'To follow classroom proceedures for writing in homework diaries, putting bags or activities away and getting changed at the same time as the rest of the class. This should progress to being done 90% of the time.'
p4962
sg81
S'M '
p4963
sg98
g27
sg87
S'2012-07-19 14:01'
p4964
sg100
g101
sg102
g27
sg86
g27
sa(dp4965
g91
S'Summer'
p4966
sg93
S'2012'
p4967
sg95
S'To ensure that a teacher is informed immediately when there is a problem and that Alex should never react aggressively.'
p4968
sg81
S'M '
p4969
sg98
g27
sg87
S'2012-07-19 14:01'
p4970
sg100
g101
sg102
g27
sg86
g27
sa(dp4971
g91
S'Mich'
p4972
sg93
S'2013'
p4973
sg95
S'To aim to work co operatively in group situations and to speak to a teacher if he starts to feel frustrated.'
p4974
sg81
S'M '
p4975
sg98
g27
sg87
S'2013-07-16 10:01'
p4976
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp4977
(dp4978
g91
S'Summer'
p4979
sg81
S'M '
p4980
sg87
S'2010-06-10 10:49'
p4981
sg145
S'1:1 and small group sessions on social skilling and friendship'
p4982
sg52
S'2010'
p4983
sg148
S'weekly(x1)'
p4984
sg86
g27
sg150
S'NB'
p4985
sa(dp4986
g91
S'Lent'
p4987
sg81
S'M '
p4988
sg87
S'2010-06-10 10:49'
p4989
sg145
S'1:1 and small group sessions on social skilling and friendship'
p4990
sg52
S'2011'
p4991
sg148
S'weekly(x1)'
p4992
sg86
g27
sg150
S'NB'
p4993
sa(dp4994
g91
S'Summer'
p4995
sg81
S'M '
p4996
sg87
S'2010-06-10 10:49'
p4997
sg145
S'1:1 and small group sessions on social skilling and friendship'
p4998
sg52
S'2011'
p4999
sg148
S'Monthly(x2)'
p5000
sg86
g27
sg150
S'NB'
p5001
sa(dp5002
g91
S'Michaelmas'
p5003
sg81
S'M '
p5004
sg87
S'2010-12-13 10:50'
p5005
sg145
S'1:1 and small group work on social skilling, emotions and his fear of the toilet'
p5006
sg52
S'2010'
p5007
sg148
S'weekly(x3)'
p5008
sg86
g27
sg150
S'NB'
p5009
sa(dp5010
g91
S'Summer'
p5011
sg81
S'M '
p5012
sg87
S'2012-02-01 09:49'
p5013
sg145
S'1:1 support lessons focussing on social skills. '
p5014
sg52
S'2012'
p5015
sg148
S'Monthly(x3)'
p5016
sg86
g27
sg150
S'NB'
p5017
sa(dp5018
g91
S'Michaelmas'
p5019
sg81
S'M '
p5020
sg87
S'2013-07-08 15:59'
p5021
sg145
S'1:1 social skills support'
p5022
sg52
S'2012'
p5023
sg148
S'weekly(x1)'
p5024
sg86
g27
sg150
S'NB'
p5025
sa(dp5026
g91
S'Lent'
p5027
sg81
S'M '
p5028
sg87
S'2013-07-08 15:59'
p5029
sg145
S'1:1 social skills support'
p5030
sg52
S'2013'
p5031
sg148
S'weekly(x1)'
p5032
sg86
g27
sg150
S'NB'
p5033
sa(dp5034
g91
S'Summer'
p5035
sg81
S'M '
p5036
sg87
S'2013-07-08 15:59'
p5037
sg145
S'1:1 social skills support'
p5038
sg52
S'2013'
p5039
sg148
S'weekly(x1)'
p5040
sg86
g27
sg150
S'NB'
p5041
sassssS'Emma_Woodcock'
p5042
(dp5043
g3
(dp5044
g5
(dp5045
g7
I0
sg8
(lp5046
g27
aS"Make learning multisensory where possible especially in maths where repetition of number facts such as tables is required. Where possible, discuss Emma's answers with her and discuss the process she used. When asking questions of Emma ensure they are specific and expect specific answers."
p5047
asg12
S'Sen provision map for Emma Woodcock'
p5048
sg14
g15
sg16
(lp5049
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Emma Woodcock'
p5050
sg24
I0
sg25
(lp5051
g27
aS'Emma may take additional time to respond to test information. Emma will benefit from structured small steps with opportunity to rehearse new skills. Do not put her on the spot with questions and provide her with thinking time before answering. Emma may require instructions to be repeated 2-3 times to ensure she has adequate understanding.'
p5052
asg29
(lp5053
g562
ag27
asg32
S'SEN,Provision map,Emma Woodcock'
p5054
sg34
(lp5055
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp5056
g41
S'12/05/2010'
p5057
sg43
S'Woodcock'
p5058
sg45
S'Action +'
p5059
sg47
S'2000-11-22'
p5060
sg49
S'EP Patricia Rouse.'
p5061
sg51
g27
sg52
g1356
sg54
g27
sg56
S"Emma's general levol of cognitive ability is average for perceptual reasoning and towards the lower end of average for more word based tasks. Emma has a specific learning difficulty of a dyslexic nature. She will find it difficult to process information presented visually and auditory. She is in the early stages of working out how the letters sound, how they look and how they blend together. VCI 87 WMI 110 PRI 94 PSI 94"
p5062
sg58
g27
sg59
S'Emma'
p5063
sg61
S'25%'
p5064
sg62
S'7CN'
p5065
ssg64
I1
sg65
I0
sg66
(lp5066
S'Emma has a specific learning difficulty of a dyslexic nature.'
p5067
aS'Skills such as spelling and number bonds should be learnt often. Focus Emma on accuracy rather than speed of response. Paired and shared reading should occur on a daily basis. When reading, allow Emma to attempt a word before offering a prompt and positive comment. Break all tasks especially writing tasks down into manageable chunks and utilise mind mapping to help organise her thoughts before writing.'
p5068
asg70
S'12-05-2010'
p5069
sg72
I0
sg73
(lp5070
g581
ag582
asssg76
(dp5071
g5
(dp5072
g38
(lp5073
(dp5074
g81
S'F '
p5075
sg83
S'Michaelmas Report - George Scott Kerr. '
p5076
sg85
g27
sg86
g27
sg87
S'2010-12-19 15:13'
p5077
sa(dp5078
g81
S'F '
p5079
sg83
S'Nb, GSK and Mrs Woodcock. Review of 1:1 sessions and changing learning specialists. '
p5080
sg85
g27
sg86
g27
sg87
S'2011-06-21 15:13'
p5081
sa(dp5082
g81
S'F '
p5083
sg83
S'Summer Report - George Scott Kerr. '
p5084
sg85
g27
sg86
g27
sg87
S'2011-07-13 15:13'
p5085
sasg37
(lp5086
(dp5087
g91
S'Lent'
p5088
sg93
S'2011'
p5089
sg95
S'To contribute to class discussion without being prompted by the teacher. '
p5090
sg81
S'F '
p5091
sg98
g27
sg87
S'2010-12-14 11:11'
p5092
sg100
g101
sg102
g27
sg86
g27
sa(dp5093
g91
S'Mich'
p5094
sg93
S'2011'
p5095
sg95
S'To display confidence in her own abilities and be willing to share her ideas with others in lessons. '
p5096
sg81
S'F '
p5097
sg98
g27
sg87
S'2011-09-30 11:11'
p5098
sg100
g139
sg102
g27
sg86
g27
sa(dp5099
g91
S'Mich'
p5100
sg93
S'2012'
p5101
sg95
S'To contribute more of her ideas in lessons. '
p5102
sg81
S'F '
p5103
sg98
g27
sg87
S'2013-07-16 11:12'
p5104
sg100
g101
sg102
g27
sg86
g27
sa(dp5105
g91
S'Lent'
p5106
sg93
S'2013'
p5107
sg95
S'To put my hand up at least 2 times in every lesson to either ask a question or share my ideas. '
p5108
sg81
S'F '
p5109
sg98
g27
sg87
S'2013-07-16 11:12'
p5110
sg100
g101
sg102
g27
sg86
g27
sa(dp5111
g91
S'Summer'
p5112
sg93
S'2013'
p5113
sg95
S'Have greater confidence in my abilities and continue to share my ideas and answers with others. '
p5114
sg81
S'F '
p5115
sg98
g27
sg87
S'2013-07-16 11:12'
p5116
sg100
g101
sg102
g27
sg86
g27
sa(dp5117
g91
S'Mich'
p5118
sg93
S'2013'
p5119
sg95
S'To ask teachers if there is something I am not sure of in my learning. '
p5120
sg81
S'F '
p5121
sg98
g27
sg87
S'2013-07-16 11:13'
p5122
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp5123
(dp5124
g91
S'Michaelmas'
p5125
sg81
S'F '
p5126
sg87
S'2010-12-14 11:07'
p5127
sg145
S'1:1 Learning Specialist George Scott Kerr.'
p5128
sg52
S'2010'
p5129
sg148
S'weekly(x1)'
p5130
sg86
g27
sg150
g27
sa(dp5131
g91
S'Lent'
p5132
sg81
S'F '
p5133
sg87
S'2010-12-14 11:07'
p5134
sg145
S'1:1 Learning Specialist George Scott Kerr.'
p5135
sg52
S'2011'
p5136
sg148
S'weekly(x1)'
p5137
sg86
g27
sg150
g27
sa(dp5138
g91
S'Summer'
p5139
sg81
S'F '
p5140
sg87
S'2010-12-14 11:07'
p5141
sg145
S'1:1 Learning Specialist George Scott Kerr.'
p5142
sg52
S'2011'
p5143
sg148
S'weekly(x1)'
p5144
sg86
g27
sg150
g27
sa(dp5145
g91
S'Michaelmas'
p5146
sg81
S'F '
p5147
sg87
S'2011-11-07 11:07'
p5148
sg145
S'1:1 Learning Specialist Clare Robinson.'
p5149
sg52
S'2011'
p5150
sg148
S'weekly(x1)'
p5151
sg86
g27
sg150
g27
sassssS'Jessica_Chapman'
p5152
(dp5153
g3
(dp5154
g5
(dp5155
g7
I0
sg8
(lp5156
g27
aS"In mathematics, longer tasks and instructions should be broken down into workable chunks and have her repeat back what is expected and required.Jessica should visualise and draw the 'problems' where possible to support her working memory."
p5157
asg12
S'Sen provision map for Jessica Chapman'
p5158
sg14
g15
sg16
(lp5159
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Jessica Chapman'
p5160
sg24
I0
sg25
(lp5161
S'She lacks confidence in her own abilities and at times requires support and repetition of questions to make sure she attempts her work.'
p5162
aS' Encourage Ellie at all times and ensure she attempts her work with confidence.'
p5163
asg29
(lp5164
g27
ag31
asg32
S'SEN,Provision map,Jessica Chapman'
p5165
sg34
(lp5166
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp5167
g41
S'30-03-2011'
p5168
sg43
S'Chapman'
p5169
sg45
S'Action'
p5170
sg47
S'2001-10-28'
p5171
sg49
S'Series of assessments carried out 30/03/2011 by NB. \r<br>EP report 26/05/2011'
p5172
sg51
S'Both'
p5173
sg52
g1564
sg54
S'26-05-2011'
p5174
sg56
S"Jessica has a mild specific learning difficulty. Her written expression skills are weak and her speed of phonological production is slow. Jessica's working memory is relatively weak (91) and her speed of processing information can be slow (94). VCI 104 PRI 94 WMI 91 PSI 94."
p5175
sg58
g27
sg59
S'Jessica'
p5176
sg61
S'15%'
p5177
sg62
S'6CS'
p5178
ssg64
I1
sg65
I0
sg66
(lp5179
S"Jessica has a mild specific learning difficulty. Her written expression skills are weak and her speed of phonological production is slow. Jessica's working memory is relatively weak (91) and her speed of processing information can be slow (94)."
p5180
aS"As Jessica has difficulties with written expression, support her where possible with templates and planning sheets. Encourage the use of mind maps and planning sheets to ensure she includes all relevant information. Jessica has a stronger verbal reasoning score and as such ensure you provide her opportunities to discuss her thoughts and answers, additionally have her record these as she speaks. To help with building a written vocabulary, Jessica must read daily. She should practice key word spellings for topics to help with her written expression.  Please ensure at all times Jessica is praised for attempting work with a positive attitude and 'having a go' at the work independently. Jessica does take time to process her thoughts so please give her the time she requires to think."
p5181
asg70
S'26-05-2011'
p5182
sg72
I0
sg73
(lp5183
g2699
ag2700
asssg76
(dp5184
g5
(dp5185
g38
(lp5186
(dp5187
g81
S'F '
p5188
sg83
S'Mr and Mrs Chapman. Review of in school assessment and suggestion of EP report. Parents agreed. '
p5189
sg85
g27
sg86
g27
sg87
S'2011-05-11 14:47'
p5190
sa(dp5191
g81
S'F '
p5192
sg83
S'SAC, CAE, NB, Mr and Mrs Chapman. Discussion of recent assessment results. '
p5193
sg85
g27
sg86
g27
sg87
S'2011-06-27 14:47'
p5194
sa(dp5195
g81
S'F '
p5196
sg83
S'NB, Mr and Mrs Chapman. Discussion of EP report. '
p5197
sg85
g27
sg86
g27
sg87
S'2011-07-11 14:47'
p5198
sa(dp5199
g81
S'F '
p5200
sg83
S'NB, Mr and Mrs Chapman. Review of progress and start of YR 5. '
p5201
sg85
g27
sg86
g27
sg87
S'2011-09-29 14:47'
p5202
sa(dp5203
g81
S'F '
p5204
sg83
S'NB, Mr and Mrs Chapman. Review of progress and exam results.'
p5205
sg85
g27
sg86
g27
sg87
S'2011-12-06 14:47'
p5206
sasg37
(lp5207
(dp5208
g91
S'Mich'
p5209
sg93
S'2011'
p5210
sg95
S'To be more proactive in sharing her ideas and thoughts in class.'
p5211
sg81
S'F '
p5212
sg98
g27
sg87
S'2011-10-10 16:25'
p5213
sg100
g101
sg102
g27
sg86
g27
sa(dp5214
g91
S'Lent'
p5215
sg93
S'2012'
p5216
sg95
S'To increase the accuracy of her written work especially key spellings.'
p5217
sg81
S'F '
p5218
sg98
g27
sg87
S'2012-02-27 16:25'
p5219
sg100
g101
sg102
g27
sg86
g27
sa(dp5220
g91
S'Mich'
p5221
sg93
S'2012'
p5222
sg95
S'To share her ideas with others in lessons and ask for help if she is not sure of something.'
p5223
sg81
S'F '
p5224
sg98
g27
sg87
S'2012-07-19 13:38'
p5225
sg100
g101
sg102
g27
sg86
g27
sa(dp5226
g91
S'Lent'
p5227
sg93
S'2013'
p5228
sg95
S'To put her hand up 2 or more times per lesson to answer a question or share an idea. '
p5229
sg81
S'F '
p5230
sg98
g27
sg87
S'2012-07-19 13:38'
p5231
sg100
g101
sg102
g27
sg86
g27
sa(dp5232
g91
S'Summer'
p5233
sg93
S'2013'
p5234
sg95
S'To include more detailed vocabulary in written answers to extend my responses. '
p5235
sg81
S'F '
p5236
sg98
g27
sg87
S'2012-07-19 13:38'
p5237
sg100
g101
sg102
g27
sg86
g27
sa(dp5238
g91
S'Mich'
p5239
sg93
S'2013'
p5240
sg95
S'To build on recent success and increase further my level of interaction in class discussion. '
p5241
sg81
S'F '
p5242
sg98
g27
sg87
S'2013-07-16 10:54'
p5243
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp5244
(dp5245
g91
S'Michaelmas'
p5246
sg81
S'F '
p5247
sg87
S'2011-05-11 14:47'
p5248
sg145
S'1:1 Support lessons with Clare Robinson. '
p5249
sg52
S'2011'
p5250
sg148
S'weekly(x1)'
p5251
sg86
g27
sg150
g27
sa(dp5252
g91
S'Lent'
p5253
sg81
S'F '
p5254
sg87
S'2011-07-11 14:47'
p5255
sg145
S'Spelling support group. '
p5256
sg52
S'2011'
p5257
sg148
S'weekly(x1)'
p5258
sg86
g27
sg150
S'NB'
p5259
sa(dp5260
g91
S'Summer'
p5261
sg81
S'F '
p5262
sg87
S'2011-07-11 14:47'
p5263
sg145
S'Spelling support group. '
p5264
sg52
S'2011'
p5265
sg148
S'weekly(x1)'
p5266
sg86
g27
sg150
S'NB'
p5267
sa(dp5268
g91
S'Michaelmas'
p5269
sg81
S'F '
p5270
sg87
S'2011-09-02 14:47'
p5271
sg145
S'1:1 Claire Robinson'
p5272
sg52
S'2011'
p5273
sg148
S'weekly(x1)'
p5274
sg86
g27
sg150
g27
sa(dp5275
g91
S'Lent'
p5276
sg81
S'F '
p5277
sg87
S'2012-01-01 14:47'
p5278
sg145
S'1:1 Claire Robinson'
p5279
sg52
S'2012'
p5280
sg148
S'weekly(x1)'
p5281
sg86
g27
sg150
g27
sa(dp5282
g91
S'Summer'
p5283
sg81
S'F '
p5284
sg87
S'2012-06-06 14:47'
p5285
sg145
S'1:1 Claire Robinson'
p5286
sg52
S'2012'
p5287
sg148
S'weekly(x1)'
p5288
sg86
g27
sg150
g27
sassssS'David_Kembery'
p5289
(dp5290
g3
(dp5291
g5
(dp5292
g7
I0
sg8
(lp5293
g27
ag5047
asg12
S'Sen provision map for David Kembery'
p5294
sg14
g15
sg16
(lp5295
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for David Kembery'
p5296
sg24
I0
sg25
(lp5297
g27
ag988
asg29
(lp5298
g562
ag27
asg32
S'SEN,Provision map,David Kembery'
p5299
sg34
(lp5300
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp5301
g41
S'04-10-2011'
p5302
sg43
S'Kembery'
p5303
sg45
S'Action'
p5304
sg47
S'2006-09-17'
p5305
sg49
g27
sg51
g27
sg52
g101
sg54
g27
sg56
S"Concerns were raised in reception about David's concentration, focus, understanding of personal space and general social skills. NB completed some 1:1 sessions with David and created a social story addressing personal space in particular."
p5306
sg58
g27
sg59
S'David'
p5307
sg61
g27
sg62
S'1CW'
p5308
ssg64
I0
sg65
I0
sg66
(lp5309
g5067
ag5068
asg70
g392
sg72
I0
sg73
(lp5310
g581
ag582
asssg76
(dp5311
g5
(dp5312
g37
(lp5313
(dp5314
g91
S'Mich'
p5315
sg93
S'2012'
p5316
sg95
S'To sit on the carpet as required and not touch others or things around him.'
p5317
sg81
S'M '
p5318
sg98
g27
sg87
S'2012-07-19 11:41'
p5319
sg100
g101
sg102
g27
sg86
g27
sa(dp5320
g91
S'Lent'
p5321
sg93
S'2013'
p5322
sg95
S'To keep to his personal space bubble for 2 class carpet sessions each day. '
p5323
sg81
S'M '
p5324
sg98
g27
sg87
S'2012-07-19 11:41'
p5325
sg100
g101
sg102
g27
sg86
g27
sa(dp5326
g91
S'Summer'
p5327
sg93
S'2013'
p5328
sg95
S'To keep his thoughts in his head and raise his hand to share ideas with the class. Non-verbal cue to remind David to be used no more than 2 times each session. '
p5329
sg81
S'M '
p5330
sg98
g27
sg87
S'2013-07-16 09:27'
p5331
sg100
g139
sg102
g27
sg86
g27
sa(dp5332
g91
S'Mich'
p5333
sg93
S'2013'
p5334
sg95
S'Ongoing: To keep his thoughts in his head and raise his hand to share ideas with the class. Non-verbal cue to remind David to be used no more than 2 times each session. Introduce an additional cue (e.g. pegs) so David can see how many reminders he has had. '
p5335
sg81
S'M '
p5336
sg98
g27
sg87
S'2013-07-16 09:27'
p5337
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp5338
(dp5339
g91
S'Michaelmas'
p5340
sg81
S'M '
p5341
sg87
S'2011-12-03 11:40'
p5342
sg145
S'1:1 Sessions supporting social skilling. '
p5343
sg52
S'2012'
p5344
sg148
S'Monthly(x3)'
p5345
sg86
g27
sg150
S'NB'
p5346
sassssS'Cameron_Flower'
p5347
(dp5348
g3
(dp5349
g5
(dp5350
g7
I0
sg8
(lp5351
g4796
ag4797
asg12
S'Sen provision map for Cameron Flower'
p5352
sg14
g15
sg16
(lp5353
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Cameron Flower'
p5354
sg24
I0
sg25
(lp5355
g27
ag4802
asg29
(lp5356
g27
ag31
asg32
S'SEN,Provision map,Cameron Flower'
p5357
sg34
(lp5358
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp5359
g41
S'21/5/2010'
p5360
sg43
S'Flower'
p5361
sg45
S'Action +'
p5362
sg47
S'2001-06-22'
p5363
sg49
S'EP Assessment Beverley Steffert\r<br>DST 21.5.10 NB'
p5364
sg51
g27
sg52
g1356
sg54
g27
sg56
S"Cameron is a child of high average verbal intellectual reasoning ability and perceptual organisation. Cameron has specific difficulties with verbal working memory. His writing speed is very slow and he has some difficulties with punctuation and written expression. Cameron's mental arithmetic is in the high average range however written arithmetic is lower. He struggles to grasp and remember certain concepts in mathematics. He has specific learning difficulties of a dyslexic nature. VCI 111 PRI 111 WMI 90 PSI 117"
p5365
sg58
g27
sg59
S'Cameron'
p5366
sg61
S'25%'
p5367
sg62
S'7CE'
p5368
ssg64
I1
sg65
I0
sg66
(lp5369
g4812
ag4813
asg70
S'10-09-2010'
p5370
sg72
I0
sg73
(lp5371
g75
ag27
asssg76
(dp5372
g5
(dp5373
g38
(lp5374
(dp5375
g81
S'M '
p5376
sg83
S'Summer Report - George Scott Kerr. '
p5377
sg85
S'12-07-2011'
p5378
sg86
g27
sg87
S'2011-07-12 09:39'
p5379
sa(dp5380
g81
S'M '
p5381
sg83
S'NB and Mrs Flower - Discussion of EP report. '
p5382
sg85
g27
sg86
g27
sg87
S'2012-10-30 13:48'
p5383
sasg37
(lp5384
(dp5385
g91
S'Lent'
p5386
sg93
S'2011'
p5387
sg95
S'To be more proactive in asking for support if required.'
p5388
sg81
S'M '
p5389
sg98
g27
sg87
S'2010-12-14 10:11'
p5390
sg100
g101
sg102
g27
sg86
g27
sa(dp5391
g91
S'Mich'
p5392
sg93
S'2012'
p5393
sg95
S'To put his hand up more in class to share my ideas.'
p5394
sg81
S'M '
p5395
sg98
g27
sg87
S'2012-07-19 13:48'
p5396
sg100
g101
sg102
g27
sg86
g27
sa(dp5397
g91
S'Lent'
p5398
sg93
S'2013'
p5399
sg95
S'To put my hand up at least once per lesson to share my ideas and thoughts with the class. '
p5400
sg81
S'M '
p5401
sg98
g27
sg87
S'2012-07-19 13:48'
p5402
sg100
g101
sg102
g27
sg86
g27
sa(dp5403
g91
S'Summer'
p5404
sg93
S'2013'
p5405
sg95
S'To focus on my learning and ensure I complete all tasks to the best of my ability. '
p5406
sg81
S'M '
p5407
sg98
g27
sg87
S'2012-07-19 13:48'
p5408
sg100
g101
sg102
g27
sg86
g27
sa(dp5409
g91
S'Mich'
p5410
sg93
S'2013'
p5411
sg95
S'To focus on specific areas suggested by teachers to fill in any gaps in my knowledge. '
p5412
sg81
S'M '
p5413
sg98
g27
sg87
S'2013-07-16 11:05'
p5414
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp5415
(dp5416
g91
S'Lent'
p5417
sg81
S'M '
p5418
sg87
S'2010-12-14 10:11'
p5419
sg145
S'1:1 Learning Support George Scott Kerr. '
p5420
sg52
S'2011'
p5421
sg148
S'weekly(x1)'
p5422
sg86
g27
sg150
g27
sa(dp5423
g91
S'Summer'
p5424
sg81
S'M '
p5425
sg87
S'2010-12-14 10:11'
p5426
sg145
S'1:1 Learning Support George Scott Kerr.'
p5427
sg52
S'2011'
p5428
sg148
S'weekly(x1)'
p5429
sg86
g27
sg150
g27
sa(dp5430
g91
S'Lent'
p5431
sg81
S'M '
p5432
sg87
S'2012-01-11 10:11'
p5433
sg145
S'1:1 Learning Support Lindsey Copeman.'
p5434
sg52
S'2012'
p5435
sg148
S'weekly(x1)'
p5436
sg86
g27
sg150
g27
sa(dp5437
g91
S'Summer'
p5438
sg81
S'M '
p5439
sg87
S'2012-05-10 10:11'
p5440
sg145
S'1:1 Learning Support Lindsey Copeman.'
p5441
sg52
S'2012'
p5442
sg148
S'weekly(x1)'
p5443
sg86
g27
sg150
g27
sa(dp5444
g91
S'Michaelmas'
p5445
sg81
S'M '
p5446
sg87
S'2012-12-16 10:11'
p5447
sg145
S'1:1 Learning Support Lindsey Copeman.'
p5448
sg52
S'2011'
p5449
sg148
S'weekly(x1)'
p5450
sg86
g27
sg150
g27
sa(dp5451
g91
S'Michaelmas'
p5452
sg81
S'M '
p5453
sg87
S'2013-07-08 16:19'
p5454
sg145
S'1:1 Literacy support with LC'
p5455
sg52
S'2012'
p5456
sg148
S'weekly(x1)'
p5457
sg86
g27
sg150
g27
sa(dp5458
g91
S'Lent'
p5459
sg81
S'M '
p5460
sg87
S'2013-07-08 16:19'
p5461
sg145
S'1:1 Literacy support with LC'
p5462
sg52
S'2013'
p5463
sg148
S'weekly(x1)'
p5464
sg86
g27
sg150
g27
sa(dp5465
g91
S'Summer'
p5466
sg81
S'M '
p5467
sg87
S'2013-07-08 16:19'
p5468
sg145
S'1:1 Literacy support with LC'
p5469
sg52
S'2013'
p5470
sg148
S'weekly(x1)'
p5471
sg86
g27
sg150
g27
sassssS'Max_Doyle'
p5472
(dp5473
g3
(dp5474
g5
(dp5475
g7
I0
sg8
(lp5476
g2596
ag2597
asg12
S'Sen provision map for Max Doyle'
p5477
sg14
g15
sg16
(lp5478
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Max Doyle'
p5479
sg24
I0
sg25
(lp5480
g27
ag2602
asg29
(lp5481
g27
ag31
asg32
S'SEN,Provision map,Max Doyle'
p5482
sg34
(lp5483
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp5484
g41
S'15/06/2010'
p5485
sg43
S'Doyle'
p5486
sg45
S'Action'
p5487
sg47
S'2002-12-12'
p5488
sg49
S'Assessed by AS on 15.6.10. Neale and rapid naming and backward digit span from DST\r<br>EP Report - Annie Mitchell 14.01.2011'
p5489
sg51
g27
sg52
g384
sg54
g27
sg56
S"Max has a slight weakness in auditory memory and processing. His spelling is slightly weak and he is over relying on phonic strategies to help him spell unknown and common words. Max's working memory (97) and processing speed (103) are in the average range and his FS IQ is 105. VCI 104 PRI 108 WMI 97 PSI 103 FSIQ 105"
p5490
sg58
g27
sg59
S'Max'
p5491
sg61
g27
sg62
S'5CE'
p5492
ssg64
I1
sg65
I0
sg66
(lp5493
S'Max has a slight weakness in auditory memory and processing. His spelling is slightly weak and he is over relying on phonic strategies to help him spell unknown and common words.'
p5494
aS'When reading comprehension is required, encourage Max to re read sections of text to ensure he has a clear understanding of what is required. Highligh key words and phrases if appropriate. Encourage reading when possible to help build his sight vocabulary.'
p5495
asg70
S'14-01-2011'
p5496
sg72
I0
sg73
(lp5497
g211
ag212
asssg76
(dp5498
g5
(dp5499
g38
(lp5500
(dp5501
g81
S'M '
p5502
sg83
S'CMC, NB, Mr and Mrs Doyle. Review of observations. EP report suggested. '
p5503
sg85
S'18-02-2011'
p5504
sg86
g27
sg87
S'2010-11-12 08:40'
p5505
sa(dp5506
g81
S'M '
p5507
sg83
S'NB, Annie Mitchell and Mr and Mrs Doyle. Discussion of EP report and possible interventions to support Max. '
p5508
sg85
S'18-02-2011'
p5509
sg86
g27
sg87
S'2011-02-18 08:40'
p5510
sa(dp5511
g81
S'M '
p5512
sg83
S'NB and Mrs Doyle. Review of previous meeting notes. '
p5513
sg85
S'18-02-2011'
p5514
sg86
g27
sg87
S'2011-05-18 08:40'
p5515
sa(dp5516
g81
S'M '
p5517
sg83
S'NB emailed Mrs Doyle with feedback from the start of the year. '
p5518
sg85
S'05-10-2011'
p5519
sg86
g27
sg87
S'2011-10-05 08:40'
p5520
sa(dp5521
g81
S'M '
p5522
sg83
S'NB, Mr and Mrs Doyle. Meeting to discuss progress and attainment. '
p5523
sg85
g27
sg86
g27
sg87
S'2011-11-10 13:21'
p5524
sasg37
(lp5525
(dp5526
g91
S'Lent'
p5527
sg93
S'2011'
p5528
sg95
S'To write down and record his thoughts via planning sheets to help with accuracy in literacy and numeracy.'
p5529
sg81
S'M '
p5530
sg98
g27
sg87
S'2010-12-13 13:21'
p5531
sg100
g101
sg102
g27
sg86
g27
sa(dp5532
g91
S'Mich'
p5533
sg93
S'2011'
p5534
sg95
S'To check each piece of work carefully before handing in. ONGOING'
p5535
sg81
S'M '
p5536
sg98
g27
sg87
S'2011-09-21 12:10'
p5537
sg100
g139
sg102
S'Regular reminders of what Max needs to check for.WILF lists.'
p5538
sg86
S'AP'
p5539
sa(dp5540
g91
S'Lent'
p5541
sg93
S'2012'
p5542
sg95
S'To check work carefully before handing in.'
p5543
sg81
S'M '
p5544
sg98
g27
sg87
S'2012-01-25 16:25'
p5545
sg100
g101
sg102
S'Positive reinforcement and praise'
p5546
sg86
S'AP'
p5547
sa(dp5548
g91
S'Lent'
p5549
sg93
S'2012'
p5550
sg95
S'To fully focus on the task in hand.'
p5551
sg81
S'M '
p5552
sg98
g27
sg87
S'2012-01-25 16:25'
p5553
sg100
g139
sg102
S'Positive reinforcement and praise'
p5554
sg86
g27
sa(dp5555
g91
S'Mich'
p5556
sg93
S'2012'
p5557
sg95
S'1. To check each piece of work carefully before handing in.'
p5558
sg81
S'M '
p5559
sg98
g27
sg87
S'2012-07-19 13:41'
p5560
sg100
g139
sg102
g27
sg86
g27
sa(dp5561
g91
S'Mich'
p5562
sg93
S'2012'
p5563
sg95
S'2. To fully focus on the task in hand.'
p5564
sg81
S'M '
p5565
sg98
g27
sg87
S'2012-07-19 13:41'
p5566
sg100
g101
sg102
g27
sg86
g27
sa(dp5567
g91
S'Lent'
p5568
sg93
S'2013'
p5569
sg95
S'1. To check each piece of work carefully before handing in.'
p5570
sg81
S'M '
p5571
sg98
g27
sg87
S'2012-07-19 13:41'
p5572
sg100
g101
sg102
g27
sg86
g27
sa(dp5573
g91
S'Summer'
p5574
sg93
S'2013'
p5575
sg95
S'1. To make sure my handwriting is neat and my work is presented to a high standard.                                                                          .  '
p5576
sg81
S'M '
p5577
sg98
g27
sg87
S'2012-07-19 13:41'
p5578
sg100
g101
sg102
g27
sg86
g27
sa(dp5579
g91
S'Summer'
p5580
sg93
S'2013'
p5581
sg95
S' 2. To make sure I have all of the equipment I need for the lesson before I leave my classroom                                                                .  '
p5582
sg81
S'M '
p5583
sg98
g27
sg87
S'2012-07-19 13:41'
p5584
sg100
g101
sg102
g27
sg86
g27
sa(dp5585
g91
S'Mich'
p5586
sg93
S'2013'
p5587
sg95
S'To listen carefully to teachers instructions in lessons so that all learning requirements are completed.  '
p5588
sg81
S'M '
p5589
sg98
g27
sg87
S'2013-07-16 10:32'
p5590
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp5591
(dp5592
g91
S'Michaelmas'
p5593
sg81
S'M '
p5594
sg87
S'2010-12-13 13:17'
p5595
sg145
S'Handwriting group '
p5596
sg52
S'2010'
p5597
sg148
S'weekly(x1)'
p5598
sg86
g27
sg150
S'KM'
p5599
sa(dp5600
g91
S'Lent'
p5601
sg81
S'M '
p5602
sg87
S'2010-12-13 13:17'
p5603
sg145
S'Handwriting group'
p5604
sg52
S'2011'
p5605
sg148
S'weekly(x1)'
p5606
sg86
g27
sg150
S'KM'
p5607
sa(dp5608
g91
S'Lent'
p5609
sg81
S'M '
p5610
sg87
S'2010-12-13 13:17'
p5611
sg145
S'Small group spelling support. '
p5612
sg52
S'2011'
p5613
sg148
S'weekly(x1)'
p5614
sg86
g27
sg150
S'NB'
p5615
sa(dp5616
g91
S'Michaelmas'
p5617
sg81
S'M '
p5618
sg87
S'2010-12-13 13:17'
p5619
sg145
S'Small group spelling support. '
p5620
sg52
S'2010'
p5621
sg148
S'weekly(x1)'
p5622
sg86
g27
sg150
S'NB'
p5623
sa(dp5624
g91
S'Summer'
p5625
sg81
S'M '
p5626
sg87
S'2010-12-13 13:17'
p5627
sg145
S'Handwriting group.'
p5628
sg52
S'2011'
p5629
sg148
S'weekly(x1)'
p5630
sg86
g27
sg150
S'AST'
p5631
sa(dp5632
g91
S'Summer'
p5633
sg81
S'M '
p5634
sg87
S'2010-12-13 13:17'
p5635
sg145
S'Small group spelling support.'
p5636
sg52
S'2011'
p5637
sg148
S'weekly(x1)'
p5638
sg86
g27
sg150
S'NB'
p5639
sa(dp5640
g91
S'Lent'
p5641
sg81
S'M '
p5642
sg87
S'2010-12-13 13:17'
p5643
sg145
S'Reading Comprehension support group. '
p5644
sg52
S'2011'
p5645
sg148
S'weekly(x1)'
p5646
sg86
g27
sg150
S'KM'
p5647
sa(dp5648
g91
S'Summer'
p5649
sg81
S'M '
p5650
sg87
S'2010-12-13 13:17'
p5651
sg145
S'Reading Comprehension support group. '
p5652
sg52
S'2011'
p5653
sg148
S'weekly(x1)'
p5654
sg86
g27
sg150
S'AST'
p5655
sa(dp5656
g91
S'Michaelmas'
p5657
sg81
S'M '
p5658
sg87
S'2010-12-13 13:17'
p5659
sg145
S'Reading Comprehension support. '
p5660
sg52
S'2011'
p5661
sg148
S'weekly(x1)'
p5662
sg86
g27
sg150
S'NB'
p5663
sa(dp5664
g91
S'Lent'
p5665
sg81
S'M '
p5666
sg87
S'2012-01-01 13:17'
p5667
sg145
S'Reading Comprehension support.'
p5668
sg52
S'2012'
p5669
sg148
S'weekly(x1)'
p5670
sg86
g27
sg150
S'NB'
p5671
sa(dp5672
g91
S'Summer'
p5673
sg81
S'M '
p5674
sg87
S'2012-01-01 13:17'
p5675
sg145
S'Reading Comprehension support.'
p5676
sg52
S'2012'
p5677
sg148
S'weekly(x1)'
p5678
sg86
g27
sg150
S'NB'
p5679
sassssS'Matthew_Corkum'
p5680
(dp5681
g3
(dp5682
g5
(dp5683
g7
I0
sg8
(lp5684
g184
ag185
asg12
S'Sen provision map for Matthew Corkum'
p5685
sg14
g15
sg16
(lp5686
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Matthew Corkum'
p5687
sg24
I1
sg25
(lp5688
S"Matty's ability to interpret visually presented perceptual information is very high, however his visuo motor integration is average. He has some auditory filtering difficulties. Matty will require instructions to be repeated and will be much more distractable when there is noise."
p5689
ag27
asg29
(lp5690
g27
ag31
asg32
S'SEN,Provision map,Matthew Corkum'
p5691
sg34
(lp5692
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp5693
g41
S'15-10-2010'
p5694
sg43
S'Corkum'
p5695
sg45
S'Early years Action +'
p5696
sg47
S'2006-06-29'
p5697
sg49
S'Classroom Observation Nathan Boller 15.10.10\r<br>Laura Irwin 05.01.2011\r<br>EP Dirk Flower 12.1.12'
p5698
sg51
g27
sg52
g1470
sg54
S'14-07-2011'
p5699
sg56
S"Matty has superior verbal and non verbal reasoning skills however there is a discrepancy between these scores and his processing speeds. Matty has some difficulties with fine motor skills, hyper mobility, postural stability and hand eye coordination. His difficulties with handwriting due to the strength of the muscles in his hand affect his ability to print accurately. He additionally has some hesitancy with the direction of letter formation and start position. Matty's ability to interpret visually presented perceptual information is very high, however his visuo motor integration is average. He has some auditory filtering difficulties. VR 129 NVR 127 PSI 100"
p5700
sg58
g27
sg59
S'Matthew'
p5701
sg61
g27
sg62
S'2CW'
p5702
ssg64
I1
sg65
I0
sg66
(lp5703
S'His difficulties with handwriting due to the strenght of the muscles in his hand affect his ability to print accurately. He additionally has some hesitancy with the direction of letter formation and start position.'
p5704
aS"Due to the discrepancy between Matty's visual perception and visuo motor integration skills, he will know what he wants to do however his work produced may not be of the same standard. This may create frustration, avoidance and a lack of confidence. Additionally staff should be aware that his written work may not match his verbal ability.  Provide as much visual support as possible with motor integration activities, such as writing letters, have templates and visuals which he can trace and refer to to help with his grapho motor skills. Always check his pencil grip and use a grip if necessary."
p5705
asg70
S'12-01-2012'
p5706
sg72
I0
sg73
(lp5707
g2175
ag27
asssg76
(dp5708
g5
(dp5709
g38
(lp5710
(dp5711
g81
S'M '
p5712
sg83
S'Mr and Mrs Corkum, NB, IH and HSM. Meeting to discuss concerns and suggestion for OT assessment. '
p5713
sg85
S'17-11-2010'
p5714
sg86
g27
sg87
S'2011-02-18 12:13'
p5715
sa(dp5716
g81
S'M '
p5717
sg83
S'Mr and Mrs Corkum, NB, IH and HSM. Meeting to discuss possible 1:1 support options with OT for Matty as fine motor skills continue to be a concern.'
p5718
sg85
S'09-03-2011'
p5719
sg86
g27
sg87
S'2011-02-18 12:13'
p5720
sa(dp5721
g81
S'M '
p5722
sg83
S'Mr and Mrs Corkum, NB, IH and Laura Irwin. Review of OT sessions, meeting called by parents. '
p5723
sg85
S'09-03-2011'
p5724
sg86
g27
sg87
S'2011-07-04 12:13'
p5725
sa(dp5726
g81
S'M '
p5727
sg83
S'Mr and Mrs Corkum, NB, JS and HSM. Review of support in school and progress/transition. '
p5728
sg85
S'03-10-2011'
p5729
sg86
g27
sg87
S'2011-10-03 12:13'
p5730
sa(dp5731
g81
S'M '
p5732
sg83
S'NB, JS, HSM, Mr and MRs Corkum. Review of progress meeting. '
p5733
sg85
S'05-12-2011'
p5734
sg86
g27
sg87
S'2011-12-05 14:52'
p5735
sa(dp5736
g81
S'M '
p5737
sg83
S'NB, JS, HSM, Mr Corkum. Review of progress meeting.'
p5738
sg85
S'06-02-2012'
p5739
sg86
g27
sg87
S'2012-02-06 14:52'
p5740
sa(dp5741
g81
S'M '
p5742
sg83
S'NB, JS, HSM, Mr Corkum and Laura Irwin. Review of LIs assessment and plans for Summer holidays.'
p5743
sg85
S'25-06-2012'
p5744
sg86
g27
sg87
S'2012-06-25 14:52'
p5745
sa(dp5746
g81
S'M '
p5747
sg83
S'NB, HSM, LLO, Mr and Mrs Corkum. Review of progress and support. '
p5748
sg85
g27
sg86
g27
sg87
S'2012-11-14 16:32'
p5749
sasg37
(lp5750
(dp5751
g91
S'Lent'
p5752
sg93
S'2011'
p5753
sg95
S'To follow instructions given by a teacher within 2 minutes. '
p5754
sg81
S'M '
p5755
sg98
g27
sg87
S'2011-02-18 12:13'
p5756
sg100
g139
sg102
g27
sg86
g27
sa(dp5757
g91
S'Summer'
p5758
sg93
S'2011'
p5759
sg95
S'To follow instructions given by a teacher within 2 minutes.'
p5760
sg81
S'M '
p5761
sg98
g27
sg87
S'2011-07-11 10:45'
p5762
sg100
g101
sg102
g27
sg86
g27
sa(dp5763
g91
S'Mich'
p5764
sg93
S'2011'
p5765
sg95
S'To stay focused on an activity for 5 minutes.'
p5766
sg81
S'M '
p5767
sg98
g27
sg87
S'2011-09-26 09:13'
p5768
sg100
g101
sg102
g27
sg86
S'JS'
p5769
sa(dp5770
g91
S'Lent'
p5771
sg93
S'2012'
p5772
sg95
S'To ask the teacher for support if he has an social, emotional or academic need.'
p5773
sg81
S'M '
p5774
sg98
g27
sg87
S'2012-02-06 09:13'
p5775
sg100
g101
sg102
g27
sg86
g27
sa(dp5776
g91
S'Mich'
p5777
sg93
S'2012'
p5778
sg95
S'1. To appropriately communicate if he has an social, emotional or academic need.'
p5779
sg81
S'M '
p5780
sg98
g27
sg87
S'2012-07-19 13:34'
p5781
sg100
g101
sg102
g27
sg86
g27
sa(dp5782
g91
S'Mich'
p5783
sg93
S'2012'
p5784
sg95
S'2. To complete the first task given in a multistep activity.'
p5785
sg81
S'M '
p5786
sg98
g27
sg87
S'2012-07-19 13:34'
p5787
sg100
g101
sg102
g27
sg86
g27
sa(dp5788
g91
S'Lent'
p5789
sg93
S'2013'
p5790
sg95
S'1.To follow step by step tasks as part of a multi step challenge. After each step completed know to check in with teacher.'
p5791
sg81
S'M '
p5792
sg98
g27
sg87
S'2012-07-19 13:34'
p5793
sg100
g101
sg102
g27
sg86
g27
sa(dp5794
g91
S'Lent'
p5795
sg93
S'2013'
p5796
sg95
S'2. To sit with his chair tucked in and back supported when engaging in writing activities at his desk.'
p5797
sg81
S'M '
p5798
sg98
g27
sg87
S'2012-07-19 13:34'
p5799
sg100
g101
sg102
g27
sg86
g27
sa(dp5800
g91
S'Summer'
p5801
sg93
S'2013'
p5802
sg95
S'To listen to an instruction and follow it within 5 sceonds.'
p5803
sg81
S'M '
p5804
sg98
g27
sg87
S'2012-07-19 13:34'
p5805
sg100
g101
sg102
g27
sg86
g27
sa(dp5806
g91
S'Mich'
p5807
sg93
S'2013'
p5808
sg95
S'1.To undertake tasks in managable and precise chunks.                                        '
p5809
sg81
S'M '
p5810
sg98
g27
sg87
S'2013-07-16 09:43'
p5811
sg100
g139
sg102
g27
sg86
g27
sa(dp5812
g91
S'Mich'
p5813
sg93
S'2013'
p5814
sg95
S'2. To take responsibility for her personal belongings.                               '
p5815
sg81
S'M '
p5816
sg98
g27
sg87
S'2013-07-16 09:43'
p5817
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp5818
(dp5819
g91
S'Michaelmas'
p5820
sg81
S'M '
p5821
sg87
S'2011-03-14 14:52'
p5822
sg145
S'1:1 Weekly Speech and Language sessions - Lorna Davis'
p5823
sg52
S'2010'
p5824
sg148
S'weekly(x1)'
p5825
sg86
g27
sg150
g27
sa(dp5826
g91
S'Lent'
p5827
sg81
S'M '
p5828
sg87
S'2011-03-14 14:52'
p5829
sg145
S'1:1 Weekly Speech and Language sessions - Lorna Davis'
p5830
sg52
S'2011'
p5831
sg148
S'weekly(x1)'
p5832
sg86
g27
sg150
g27
sa(dp5833
g91
S'Summer'
p5834
sg81
S'M '
p5835
sg87
S'2011-03-14 14:52'
p5836
sg145
S'1:1 Weekly Speech and Language sessions - Lorna Davis'
p5837
sg52
S'2011'
p5838
sg148
S'weekly(x1)'
p5839
sg86
g27
sg150
g27
sa(dp5840
g91
S'Summer'
p5841
sg81
S'M '
p5842
sg87
S'2011-03-14 14:52'
p5843
sg145
S'1:1 Occupational Therapy session - Laura Irwin'
p5844
sg52
S'2011'
p5845
sg148
S'weekly(x1)'
p5846
sg86
g27
sg150
g27
sa(dp5847
g91
S'Michaelmas'
p5848
sg81
S'M '
p5849
sg87
S'2011-03-14 14:52'
p5850
sg145
S'1:1 Weekly Speech and Language sessions - Lorna Davis'
p5851
sg52
S'2011'
p5852
sg148
S'weekly(x1)'
p5853
sg86
g27
sg150
g27
sa(dp5854
g91
S'Michaelmas'
p5855
sg81
S'M '
p5856
sg87
S'2011-03-14 14:52'
p5857
sg145
S'1:1 Occupational Therapy session - Laura Irwin'
p5858
sg52
S'2011'
p5859
sg148
S'weekly(x1)'
p5860
sg86
g27
sg150
g27
sa(dp5861
g91
S'Michaelmas'
p5862
sg81
S'M '
p5863
sg87
S'2011-03-14 14:52'
p5864
sg145
S'1:1 support for fine motor and social skills. '
p5865
sg52
S'2011'
p5866
sg148
S'weekly(x1)'
p5867
sg86
g27
sg150
S'NB'
p5868
sa(dp5869
g91
S'Michaelmas'
p5870
sg81
S'M '
p5871
sg87
S'2011-09-04 14:32'
p5872
sg145
S'Spelling group'
p5873
sg52
S'2011'
p5874
sg148
S'weekly(x1)'
p5875
sg86
g27
sg150
S'AST'
p5876
sa(dp5877
g91
S'Lent'
p5878
sg81
S'M '
p5879
sg87
S'2012-01-04 14:32'
p5880
sg145
S'Spelling group'
p5881
sg52
S'2012'
p5882
sg148
S'weekly(x1)'
p5883
sg86
g27
sg150
S'AST'
p5884
sa(dp5885
g91
S'Lent'
p5886
sg81
S'M '
p5887
sg87
S'2012-01-05 14:32'
p5888
sg145
S'In class literacy support. '
p5889
sg52
S'2012'
p5890
sg148
S'weekly(x1)'
p5891
sg86
g27
sg150
S'AST'
p5892
sa(dp5893
g91
S'Lent'
p5894
sg81
S'M '
p5895
sg87
S'2012-01-09 14:52'
p5896
sg145
S'1:1 support for fine motor and social skills.'
p5897
sg52
S'2012'
p5898
sg148
S'weekly(x1)'
p5899
sg86
g27
sg150
S'NB'
p5900
sa(dp5901
g91
S'Summer'
p5902
sg81
S'M '
p5903
sg87
S'2012-01-09 14:52'
p5904
sg145
S'1:1 Occupational Therapy session - Laura Irwin'
p5905
sg52
S'2012'
p5906
sg148
S'Monthly(x3)'
p5907
sg86
g27
sg150
g27
sa(dp5908
g91
S'Lent'
p5909
sg81
S'M '
p5910
sg87
S'2012-01-09 14:52'
p5911
sg145
S'1:1 Occupational Therapy session - Laura Irwin'
p5912
sg52
S'2012'
p5913
sg148
S'Monthly(x3)'
p5914
sg86
g27
sg150
g27
sa(dp5915
g91
S'Summer'
p5916
sg81
S'M '
p5917
sg87
S'2012-04-10 14:52'
p5918
sg145
S'1:1 support for fine motor and social skills.'
p5919
sg52
S'2012'
p5920
sg148
S'weekly(x1)'
p5921
sg86
g27
sg150
S'NB'
p5922
sa(dp5923
g91
S'Summer'
p5924
sg81
S'M '
p5925
sg87
S'2012-05-08 14:32'
p5926
sg145
S'Spelling group'
p5927
sg52
S'2012'
p5928
sg148
S'weekly(x1)'
p5929
sg86
g27
sg150
S'KM'
p5930
sa(dp5931
g91
S'Summer'
p5932
sg81
S'M '
p5933
sg87
S'2012-05-08 14:32'
p5934
sg145
S'In class literacy support. '
p5935
sg52
S'2012'
p5936
sg148
S'weekly(x1)'
p5937
sg86
g27
sg150
S'KM'
p5938
sa(dp5939
g91
S'Michaelmas'
p5940
sg81
S'M '
p5941
sg87
S'2013-07-08 10:11'
p5942
sg145
S'Weekly handwriting support'
p5943
sg52
S'2012'
p5944
sg148
S'weekly(x1)'
p5945
sg86
g27
sg150
S'KM'
p5946
sa(dp5947
g91
S'Lent'
p5948
sg81
S'M '
p5949
sg87
S'2013-07-08 10:11'
p5950
sg145
S'Weekly handwriting support'
p5951
sg52
S'2013'
p5952
sg148
S'weekly(x1)'
p5953
sg86
g27
sg150
S'KM'
p5954
sa(dp5955
g91
S'Summer'
p5956
sg81
S'M '
p5957
sg87
S'2013-07-08 10:11'
p5958
sg145
S'Weekly handwriting support'
p5959
sg52
S'2013'
p5960
sg148
S'weekly(x1)'
p5961
sg86
g27
sg150
S'KM'
p5962
sa(dp5963
g91
S'Michaelmas'
p5964
sg81
S'M '
p5965
sg87
S'2013-07-08 10:11'
p5966
sg145
S'Learning support session'
p5967
sg52
S'2012'
p5968
sg148
S'weekly(x1)'
p5969
sg86
g27
sg150
S'NB'
p5970
sa(dp5971
g91
S'Lent'
p5972
sg81
S'M '
p5973
sg87
S'2013-07-08 10:11'
p5974
sg145
S'Learning support session'
p5975
sg52
S'2013'
p5976
sg148
S'weekly(x1)'
p5977
sg86
g27
sg150
S'NB'
p5978
sa(dp5979
g91
S'Summer'
p5980
sg81
S'M '
p5981
sg87
S'2013-07-08 10:11'
p5982
sg145
S'Learning support session'
p5983
sg52
S'2013'
p5984
sg148
S'Monthly(x1)'
p5985
sg86
g27
sg150
S'NB'
p5986
sassssS'Safia_Dovell'
p5987
(dp5988
g3
(dp5989
g5
(dp5990
g7
I0
sg8
(lp5991
g1828
ag1829
asg12
S'Sen provision map for Safia Dovell'
p5992
sg14
g15
sg16
(lp5993
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Safia Dovell'
p5994
sg24
I0
sg25
(lp5995
g27
aS"Safia's attitiude and motivation towards work will be an important contributory factor to her success. Safia needs to be empowered to think she can succeed and should be set achievable targets. She will need clearly defined instructions with explicit expectations relating to the task. Please ensure Safia is seated in clear view of the teacher and the board, preferably towards the front of the room."
p5996
asg29
(lp5997
g27
ag31
asg32
S'SEN,Provision map,Safia Dovell'
p5998
sg34
(lp5999
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp6000
g41
S'01-09-2011'
p6001
sg43
S'Dovell'
p6002
sg45
S'Action'
p6003
sg47
S'2002-11-04'
p6004
sg49
S'DST-J completed by NB Michaelmas 2011.\r<br>EP Report Nina Elliott 07.07.2012'
p6005
sg51
g27
sg52
g384
sg54
g27
sg56
S"Safia has a mild specific learning difficulty which particularly impacts on her spelling and writing skills, her ability to focus and her application. Her ability to apply phonetic decoding skills are weak and as a result her spelling is poor. Her handwriting is immature and poorly formed. Safia's written language skills are weak and her punctuation can be erratic. She lacks self belief and does not always see herself as a successful learner. Safia will require your full support and attention in class to ensure she engages in learning and also completes work as expected. Safia is short sighted and colour blind."
p6006
sg58
g27
sg59
S'Safia'
p6007
sg61
g27
sg62
S'5CN'
p6008
ssg64
I1
sg65
I0
sg66
(lp6009
S"Safia has a mild specific learning difficulty which particularly impacts on her spelling and writing skills, her ability to focus and her application. Her ability to apply phonetic decoding skills are weak and as a result her spelling is poor. Her handwriting is immature and poorly formed. Safia's written language skills are weak and her punctuation can be erratic."
p6010
aS"Safia will require support to assist with her planning and thinking skills. Utilse templates and plans where possible. Additionally break tasks down into 2-3 steps presenting information in parts which make up the whole. Visuals and manipulatives must be used to help Safia see the problem or question. Charts, spelling cards, key words etc will be very beneficial. Exposure to reading text will help improve her sight vocabulary. Safia should be heard reading regularly to ensure she is reading texts at a level equal to her ability. Safia should practice key word spellings. Days of the week, months etc and common sight words must be spelt correctly. A spelling chart will be given to help her check her spelling when working independently. Please ensure at all times Safia is praised for attempting work with a positive attitude and 'having a go' at the work independently."
p6011
asg70
S'07-07-2012'
p6012
sg72
I0
sg73
(lp6013
S'Safia is short sighted and colour blind.'
p6014
ag27
asssg76
(dp6015
g5
(dp6016
g38
(lp6017
(dp6018
g81
S'F '
p6019
sg83
S'NB, CMC and Mrs Dovell met to discuss the results of the assesment. '
p6020
sg85
g27
sg86
g27
sg87
S'2011-10-19 16:59'
p6021
sa(dp6022
g81
S'F '
p6023
sg83
S'NB, CMC and Mr and Mrs Dovell met to discuss Safia getting an EP assessment completed. '
p6024
sg85
g27
sg86
g27
sg87
S'2012-05-21 16:59'
p6025
sa(dp6026
g81
S'F '
p6027
sg83
S'NB, Mr and Mrs Dovell. Discussion of EP report and possible support. '
p6028
sg85
g27
sg86
g27
sg87
S'2012-09-28 11:52'
p6029
sasg37
(lp6030
(dp6031
g91
S'Mich'
p6032
sg93
S'2011'
p6033
sg95
S'To work independently for a minimum 5 minute period when all instructions have been provided. '
p6034
sg81
S'F '
p6035
sg98
g27
sg87
S'2011-10-03 16:59'
p6036
sg100
g139
sg102
g27
sg86
g27
sa(dp6037
g91
S'Lent'
p6038
sg93
S'2012'
p6039
sg95
S'To work independently for a minimum 5 minute period when all instructions have been provided.'
p6040
sg81
S'F '
p6041
sg98
g27
sg87
S'2012-02-06 16:59'
p6042
sg100
g101
sg102
g27
sg86
g27
sa(dp6043
g91
S'Mich'
p6044
sg93
S'2012'
p6045
sg95
S'Take her time over her writing and to check carefully before handing it in.'
p6046
sg81
S'F '
p6047
sg98
g27
sg87
S'2012-07-19 13:40'
p6048
sg100
g139
sg102
g27
sg86
g27
sa(dp6049
g91
S'Lent'
p6050
sg93
S'2013'
p6051
sg95
S'Take her time over her writing and to check carefully before handing it in.'
p6052
sg81
S'F '
p6053
sg98
g27
sg87
S'2012-07-19 13:40'
p6054
sg100
g101
sg102
g27
sg86
g27
sa(dp6055
g91
S'Summer'
p6056
sg93
S'2013'
p6057
sg95
S'To accurately read the question twice to make sure I apply the correct processes when answering questions. '
p6058
sg81
S'F '
p6059
sg98
g27
sg87
S'2012-07-19 13:40'
p6060
sg100
g101
sg102
g27
sg86
g27
sa(dp6061
g91
S'Mich'
p6062
sg93
S'2013'
p6063
sg95
S'To make sure my handwriting is neat and my work is presented to a high standard.     '
p6064
sg81
S'F '
p6065
sg98
g27
sg87
S'2013-07-16 10:35'
p6066
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp6067
(dp6068
g91
S'Michaelmas'
p6069
sg81
S'F '
p6070
sg87
S'2011-10-05 15:11'
p6071
sg145
S'Reading Comprehension Support. '
p6072
sg52
S'2011'
p6073
sg148
S'weekly(x1)'
p6074
sg86
g27
sg150
S'NB'
p6075
sa(dp6076
g91
S'Michaelmas'
p6077
sg81
S'F '
p6078
sg87
S'2011-10-05 15:11'
p6079
sg145
S'Spelling support group. '
p6080
sg52
S'2011'
p6081
sg148
S'weekly(x1)'
p6082
sg86
g27
sg150
S'NB'
p6083
sa(dp6084
g91
S'Summer'
p6085
sg81
S'F '
p6086
sg87
S'2011-10-05 15:11'
p6087
sg145
S'Spelling support group. '
p6088
sg52
S'2011'
p6089
sg148
S'weekly(x1)'
p6090
sg86
g27
sg150
S'NB'
p6091
sa(dp6092
g91
S'Lent'
p6093
sg81
S'F '
p6094
sg87
S'2012-01-01 15:11'
p6095
sg145
S'Spelling support group.'
p6096
sg52
S'2012'
p6097
sg148
S'weekly(x1)'
p6098
sg86
g27
sg150
S'NB'
p6099
sa(dp6100
g91
S'Summer'
p6101
sg81
S'F '
p6102
sg87
S'2012-01-01 15:11'
p6103
sg145
S'Spelling support group.'
p6104
sg52
S'2012'
p6105
sg148
S'weekly(x1)'
p6106
sg86
g27
sg150
S'NB'
p6107
sa(dp6108
g91
S'Lent'
p6109
sg81
S'F '
p6110
sg87
S'2012-01-05 15:11'
p6111
sg145
S'Reading Comprehension Support.'
p6112
sg52
S'2012'
p6113
sg148
S'weekly(x1)'
p6114
sg86
g27
sg150
S'NB'
p6115
sa(dp6116
g91
S'Summer'
p6117
sg81
S'F '
p6118
sg87
S'2012-01-05 15:11'
p6119
sg145
S'Reading Comprehension Support.'
p6120
sg52
S'2012'
p6121
sg148
S'weekly(x1)'
p6122
sg86
g27
sg150
S'NB'
p6123
sa(dp6124
g91
S'Michaelmas'
p6125
sg81
S'F '
p6126
sg87
S'2013-07-08 16:21'
p6127
sg145
S'1:1 Specialist Teaching Clare Robinson'
p6128
sg52
S'2012'
p6129
sg148
S'weekly(x1)'
p6130
sg86
g27
sg150
g27
sa(dp6131
g91
S'Lent'
p6132
sg81
S'F '
p6133
sg87
S'2013-07-08 16:21'
p6134
sg145
S'1:1 Specialist Teaching Clare Robinson'
p6135
sg52
S'2013'
p6136
sg148
S'weekly(x1)'
p6137
sg86
g27
sg150
g27
sa(dp6138
g91
S'Summer'
p6139
sg81
S'F '
p6140
sg87
S'2013-07-08 16:21'
p6141
sg145
S'1:1 Specialist Teaching Clare Robinson'
p6142
sg52
S'2013'
p6143
sg148
S'weekly(x1)'
p6144
sg86
g27
sg150
g27
sassssS'Amy_McWey'
p6145
(dp6146
g3
(dp6147
g5
(dp6148
g7
I0
sg8
(lp6149
g27
aS'Amy will need help planning and organising her thoughts. Templates and graphic organisers will help her. Amy should have information chunked for her into small steps. This is not for memory reasons, more so to allow her to process information and not get confused with content. Amy should be encouraged to work for 5 minute sections and then check in with the teacher. Teachers must check in with Amy to ensure she understands the task. Approximately 5 minute blocks. A timer can be used to help Amy concentrate and work independently during these blocks of time. Staff must encourage Amy to ask for help if she is not sure of what to do. Teachers should get an example answer from Amy to test her level of understanding. Amy will need longer to process instructions and thoughts. Please provide Amy with extra time and discussion where possible to ensure she displays her true level of understanding.'
p6150
asg12
S'Sen provision map for Amy McWey'
p6151
sg14
g15
sg16
(lp6152
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Amy McWey'
p6153
sg24
I0
sg25
(lp6154
g1290
ag1291
asg29
(lp6155
g27
ag31
asg32
S'SEN,Provision map,Amy McWey'
p6156
sg34
(lp6157
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp6158
g41
S'23-11-2011'
p6159
sg43
S'McWey'
p6160
sg45
S'Action'
p6161
sg47
S'2004-07-30'
p6162
sg49
S'DST and Classroom observation completed by NB on 23.11.2011'
p6163
sg51
g27
sg52
g201
sg54
g27
sg56
S'Amy was referred to the LE Department due to possible concerns with attention, concentration and focus. Her behaviours and mannerisms are quiet misleading and may appear that she does not know what to do. Amy can complete tasks however may take some extra time to understand the tasks and may need careful explaination.'
p6164
sg58
g27
sg59
S'Amy'
p6165
sg61
g27
sg62
S'4CE'
p6166
ssg64
I0
sg65
I0
sg66
(lp6167
g27
aS'Amy will need help planning and organising her thoughts. Templates and graphic organisers will help her. Amy should have information chunked for her into small steps. This is not for memory reasons, more so to allow her to process information and not get confused with content. Amy should be encouraged to work for 5 minute sections and then check in with the teacher. Teachers must check in with Amy to ensure she understands the task. Approximately 5 minute blocks. A timer can be used to help Amy concentrate and work independently during these blocks of time. Staff must encourage Amy to ask for help if she is not sure of what to do. Teachers should get an example answer from Amy to test her level of understanding. Amy will need longer to process instructions and thoughts. Please provide Amy with extra time and discussion where possible to ensure she displays her true level of understanding.'
p6168
asg70
g392
sg72
I0
sg73
(lp6169
g1308
ag27
asssg76
(dp6170
g5
(dp6171
g38
(lp6172
(dp6173
g81
S'F '
p6174
sg83
S'NB, CD, Mrs McWey. Discussion of concerns by Mrs McWey and CD. NB to observe and assess. '
p6175
sg85
g27
sg86
g27
sg87
S'2011-11-03 09:00'
p6176
sa(dp6177
g81
S'F '
p6178
sg83
S'NB, CD, Mrs McWey. Discussion of progress following observations and suggestions from NB. '
p6179
sg85
g27
sg86
S'CDY'
p6180
sg87
S'2012-03-09 09:00'
p6181
sasg37
(lp6182
(dp6183
g91
S'Lent'
p6184
sg93
S'2012'
p6185
sg95
S'English: To focus quickly in lessons (2 minutes), and to complete the task at hand.'
p6186
sg81
S'F '
p6187
sg98
g27
sg87
S'2012-01-27 09:00'
p6188
sg100
g101
sg102
g27
sg86
g27
sa(dp6189
g91
S'Mich'
p6190
sg93
S'2012'
p6191
sg95
S'English - To concentrate on adding detail to work in English'
p6192
sg81
S'F '
p6193
sg98
g27
sg87
S'2012-07-19 12:15'
p6194
sg100
g101
sg102
g27
sg86
g27
sa(dp6195
g91
S'Mich'
p6196
sg93
S'2012'
p6197
sg95
S'Maths:  Stay focused and on task and commence answering the question within 2 minutes.'
p6198
sg81
S'F '
p6199
sg98
g27
sg87
S'2012-07-19 12:15'
p6200
sg100
g101
sg102
g27
sg86
g27
sa(dp6201
g91
S'Lent'
p6202
sg93
S'2013'
p6203
sg95
S'Maths: To be confident in my abilities through attempting questions first time without needing to check with teachers if my answer is correct. (75% of the time).                                                                                                                 '
p6204
sg81
S'F '
p6205
sg98
g27
sg87
S'2012-07-19 12:15'
p6206
sg100
g101
sg102
g27
sg86
g27
sa(dp6207
g91
S'Lent'
p6208
sg93
S'2013'
p6209
sg95
S'English - To use at least 3 adjectives in a piece of work.                                                                                                       '
p6210
sg81
S'F '
p6211
sg98
g27
sg87
S'2012-07-19 12:15'
p6212
sg100
g101
sg102
g27
sg86
g27
sa(dp6213
g91
S'Summer'
p6214
sg93
S'2013'
p6215
sg95
S'Maths - to work faster and complete the work in a set amount of time.                                                                                                                                                            '
p6216
sg81
S'F '
p6217
sg98
g27
sg87
S'2012-07-19 12:15'
p6218
sg100
g101
sg102
g27
sg86
g27
sa(dp6219
g91
S'Summer'
p6220
sg93
S'2013'
p6221
sg95
S'English - to check work through to spot 3 spellingor punctuation errors to correct independently.                                                                                                                                                    '
p6222
sg81
S'F '
p6223
sg98
g27
sg87
S'2012-07-19 12:15'
p6224
sg100
g101
sg102
g27
sg86
g27
sassssS'Albert_Nuttall'
p6225
(dp6226
g3
(dp6227
g5
(dp6228
g7
I0
sg8
(lp6229
g1456
ag27
asg12
S'Sen provision map for Albert Nuttall'
p6230
sg14
g15
sg16
(lp6231
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Albert Nuttall'
p6232
sg24
I0
sg25
(lp6233
g1461
ag27
asg29
(lp6234
g3788
ag3789
asg32
S'SEN,Provision map,Albert Nuttall'
p6235
sg34
(lp6236
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp6237
g41
S'12/01/2010'
p6238
sg43
S'Nuttall'
p6239
sg45
S'Action +'
p6240
sg47
S'2005-03-11'
p6241
sg49
g27
sg51
g27
sg52
g53
sg54
g27
sg56
g27
sg58
g27
sg59
S'Albert'
p6242
sg61
g27
sg62
S'3CS'
p6243
ssg64
I0
sg65
I0
sg70
g392
sg72
I0
sg66
(lp6244
g1475
ag1476
asssg76
(dp6245
g5
(dp6246
sssS'Samuel_Hanson'
p6247
(dp6248
g3
(dp6249
g5
(dp6250
g7
I0
sg8
(lp6251
g27
ag368
asg12
S'Sen provision map for Samuel Hanson'
p6252
sg14
g15
sg16
(lp6253
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Samuel Hanson'
p6254
sg24
I0
sg25
(lp6255
g27
aS'Although Sam\'s working memory is well developed it is below his intellectual ability therefore the use of graphic organisers will help him quickly record his ideas for expansion at a later time. Additionally pictures, mind maps and other visual means of remembering verbally presented information will benefit. Sam will be slow to handwrite and should word process if appropriate.  To ensure Sam remains focused and on task develop a signal that can be used if Sam feels unsure " ask \'what he thinks he needs to do?\' Break tasks down into their smallest components and encourage completion of each step at a time.'
p6256
asg29
(lp6257
g27
ag31
asg32
S'SEN,Provision map,Samuel Hanson'
p6258
sg34
(lp6259
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp6260
g41
S'02/01/2010'
p6261
sg43
S'Hanson'
p6262
sg45
S'Action +'
p6263
sg47
S'2002-03-25'
p6264
sg49
S'EP Report Beverley Steffert\r<br>NEALE 10.12.09 - Anita Sawyer\r<br>08.05.2008 Speech and Language\r<br>Speech and Language Michaelmas - Lorna Davis'
p6265
sg51
g27
sg52
g1564
sg54
g27
sg56
S"Samuel has superior verbal intellectual ability, (126) and very superior non-verbal intellectual capacity (135). This is very discrepant compared to his weak speed of processing (85) and attention level, these are significantly below Sam's intellectual level. Sam is able to maintain attention with visually presented information rather than verbal. Sam has some difficulties with friendships and peers and should be supported at all opportunities. VCI 126 PRI 135 WMi 113 PSI 85"
p6266
sg58
g27
sg59
S'Samuel'
p6267
sg61
g27
sg62
S'6CE'
p6268
ssg64
I1
sg65
I0
sg66
(lp6269
g27
aS"Allow sufficient time and incentives for Sam to proof read his written work and read it aloud. Staff should be aware that Sam's written work will not always reflect his understanding of concept knowledge. Staff should always look for alternative methods of assessment."
p6270
asg70
S'02-01-2010'
p6271
sg72
I1
sg73
(lp6272
S'Joe has some difficulties with the physical act of handwriting.'
p6273
aS"Encourage Joe to have neat handwriting however please be aware that he does have difficulties with this. Positive praise and reinforcement will also be beneficial. Joe's handwriting although messy at times is legible."
p6274
asssg76
(dp6275
g5
(dp6276
g38
(lp6277
(dp6278
g81
S'M '
p6279
sg83
S'NB, CMC, AP, RO, Mr and Mrs Hanson. Discussion re supporting Sam at home and school. '
p6280
sg85
g27
sg86
g27
sg87
S'2010-09-30 14:09'
p6281
sa(dp6282
g81
S'M '
p6283
sg83
S'NB and Mrs Hanson. Social skilling discussion/support. Phone conversation. '
p6284
sg85
g27
sg86
g27
sg87
S'2011-02-11 14:07'
p6285
sa(dp6286
g81
S'M '
p6287
sg83
S'Summer report - Lindsey Copeman'
p6288
sg85
g27
sg86
g27
sg87
S'2011-07-01 14:07'
p6289
sa(dp6290
g81
S'M '
p6291
sg83
S'AP and Mrs Hanson. Social skilling discussion. Phone conversation. '
p6292
sg85
g27
sg86
g27
sg87
S'2011-07-12 14:07'
p6293
sa(dp6294
g81
S'M '
p6295
sg83
S'NB and Mrs Hanson telephone conversation. NB suggesting additional emotional support for Sam. '
p6296
sg85
S'17-01-2012'
p6297
sg86
g27
sg87
S'2012-01-17 09:12'
p6298
sasg37
(lp6299
(dp6300
g91
S'sVG'
p6301
sg93
S'2011'
p6302
sg95
S'To speak to an adult if he has a problem with peers.'
p6303
sg81
S'M '
p6304
sg98
g27
sg87
S'2010-12-14 09:12'
p6305
sg100
g139
sg102
g27
sg86
g27
sa(dp6306
g91
S'Lent'
p6307
sg93
S'2011'
p6308
sg95
S'To develop his focus and concentration during lessons.'
p6309
sg81
S'M '
p6310
sg98
g27
sg87
S'2010-12-14 09:12'
p6311
sg100
g101
sg102
g27
sg86
g27
sa(dp6312
g91
S'Mich'
p6313
sg93
S'2011'
p6314
sg95
S'To improve his ability to work as a member of a group through appropriate communication and interaction.'
p6315
sg81
S'M '
p6316
sg98
g27
sg87
S'2011-10-10 09:12'
p6317
sg100
g101
sg102
g27
sg86
g27
sa(dp6318
g91
S'Lent'
p6319
sg93
S'2012'
p6320
sg95
S'To improve his ability to work as a member of a group through appropriate communication and interaction. ONGOING'
p6321
sg81
S'M '
p6322
sg98
g27
sg87
S'2012-02-27 09:12'
p6323
sg100
g139
sg102
g27
sg86
g27
sa(dp6324
g91
S'Mich'
p6325
sg93
S'2012'
p6326
sg95
S'To improve his ability to work as a member of a group through appropriate communication and interaction.'
p6327
sg81
S'M '
p6328
sg98
g27
sg87
S'2012-07-19 14:04'
p6329
sg100
g139
sg102
g27
sg86
g27
sa(dp6330
g91
S'Lent'
p6331
sg93
S'2013'
p6332
sg95
S'To improve his ability to work as a member of a group through appropriate communication and interaction.'
p6333
sg81
S'M '
p6334
sg98
g27
sg87
S'2012-07-19 14:04'
p6335
sg100
g101
sg102
g27
sg86
g27
sa(dp6336
g91
S'Lent'
p6337
sg93
S'2013'
p6338
sg95
S'2. Use my timetable in form time to make sure I have all of the equipment I need for the day. '
p6339
sg81
S'M '
p6340
sg98
g27
sg87
S'2012-07-19 14:04'
p6341
sg100
g101
sg102
g27
sg86
g27
sa(dp6342
g91
S'Summer'
p6343
sg93
S'2013'
p6344
sg95
S'1. To commence work withing 2 minutes of having the instruction given by teachers.                                  '
p6345
sg81
S'M '
p6346
sg98
g27
sg87
S'2013-07-16 10:49'
p6347
sg100
g139
sg102
g27
sg86
g27
sa(dp6348
g91
S'Summer'
p6349
sg93
S'2013'
p6350
sg95
S'2. To manage my pack up and arrival times so I do not miss the beginning of lessons.                      '
p6351
sg81
S'M '
p6352
sg98
g27
sg87
S'2013-07-16 10:49'
p6353
sg100
g139
sg102
g27
sg86
g27
sa(dp6354
g91
S'Mich'
p6355
sg93
S'2013'
p6356
sg95
S'1. To commence work withing 2 minutes of having the instruction given by teachers.                                  '
p6357
sg81
S'M '
p6358
sg98
g27
sg87
S'2013-07-16 10:49'
p6359
sg100
g139
sg102
g27
sg86
g27
sa(dp6360
g91
S'Mich'
p6361
sg93
S'2013'
p6362
sg95
S'2. To manage my pack up and arrival times so I do not miss the beginning of lessons.                      '
p6363
sg81
S'M '
p6364
sg98
g27
sg87
S'2013-07-16 10:49'
p6365
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp6366
(dp6367
g91
S'Michaelmas'
p6368
sg81
S'M '
p6369
sg87
S'2010-12-14 09:12'
p6370
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p6371
sg52
S'2010'
p6372
sg148
S'weekly(x1)'
p6373
sg86
g27
sg150
g27
sa(dp6374
g91
S'Lent'
p6375
sg81
S'M '
p6376
sg87
S'2010-12-14 09:12'
p6377
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p6378
sg52
S'2011'
p6379
sg148
S'weekly(x1)'
p6380
sg86
g27
sg150
g27
sa(dp6381
g91
S'Summer'
p6382
sg81
S'M '
p6383
sg87
S'2010-12-14 09:12'
p6384
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p6385
sg52
S'2011'
p6386
sg148
S'weekly(x1)'
p6387
sg86
g27
sg150
g27
sa(dp6388
g91
S'Michaelmas'
p6389
sg81
S'M '
p6390
sg87
S'2010-12-14 09:12'
p6391
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p6392
sg52
S'2011'
p6393
sg148
S'weekly(x1)'
p6394
sg86
g27
sg150
g27
sa(dp6395
g91
S'Lent'
p6396
sg81
S'M '
p6397
sg87
S'2011-02-18 12:35'
p6398
sg145
S'1:1 social skilling sessions. '
p6399
sg52
S'2011'
p6400
sg148
g27
sg86
g27
sg150
S'NB'
p6401
sa(dp6402
g91
S'Summer'
p6403
sg81
S'M '
p6404
sg87
S'2011-02-18 12:35'
p6405
sg145
S'1:1 social skilling sessions.'
p6406
sg52
S'2011'
p6407
sg148
g27
sg86
g27
sg150
S'NB'
p6408
sa(dp6409
g91
S'Michaelmas'
p6410
sg81
S'M '
p6411
sg87
S'2011-07-12 14:07'
p6412
sg145
S'Speech and Language Support - Lorna Davies'
p6413
sg52
S'2010'
p6414
sg148
S'weekly(x1)'
p6415
sg86
g27
sg150
g27
sassssS'Ines_Verhoosel'
p6416
(dp6417
g3
(dp6418
g5
(dp6419
g7
I0
sg8
(lp6420
g10
ag11
asg12
S'Sen provision map for Ines Verhoosel'
p6421
sg14
g15
sg16
(lp6422
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Ines Verhoosel'
p6423
sg24
I1
sg25
(lp6424
S'All assessments indicated that she does not has any specific difficulties however teachers should be aware of her sitting position when writing. Ines additionally has some mild short term auditory memory and word retrieval difficulties. Ines may find it difficult to interpret visually represented information and additionally hold information during multistep questions. She has a weakness in phonological awareness affecting her spelling. Her vocabulary is also a little weak however she has superior verbal reasoning abilities'
p6425
aS'Where possible provide Ines with a list of the key vocabulary before reading a piece of text. Decode these words and explain their meaning to her. This will allow her to read the piece of text with greater fluency and maintain a greater level of comprehension. Provide Ines with templates during multistep mathematics questions. Ines must be shown how to set out a question and learn these processes to prevent confusion and loosing information that is stored short term. Templates will help her follow the process and focus on the question itself.  Where possible, break tasks down into 2-3 steps. Ines showed in this assessment that 4 items of information was the maximum she could hold accurately. Presenting information in parts which make up the whole will allow her to focus on the task with greater accuracy. Ensure you explain what the completed task will be before commencing so she can see how all smaller parts contribute to achieving the outcome. Talking her way through multistep tasks will help.'
p6426
asg29
(lp6427
g27
ag31
asg32
S'SEN,Provision map,Ines Verhoosel'
p6428
sg34
(lp6429
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp6430
g41
S'05-10-2011'
p6431
sg43
S'Verhoosel'
p6432
sg45
S'Action'
p6433
sg47
S'2004-08-14'
p6434
sg49
g27
sg51
g27
sg52
g201
sg54
g27
sg56
S'Ines was assessed by NB on 5.10.2011. She had further assessments completed on 23.4.2012 at Fairly House for SP and L, OT and and EP assessment. All assessments indicated that she does not has any specific difficulties however teachers should be aware of her sitting position when writing. Ines additionally has some mild short term auditory memory and word retrieval difficulties. Ines may find it difficult to interpret visually represented information and additionally hold information during multistep questions. She has a weakness in phonological awareness affecting her spelling. Her vocabulary is also a little weak however she has superior verbal reasoning abilities'
p6435
sg58
g27
sg59
S'Ines'
p6436
sg61
g27
sg62
S'4CE'
p6437
ssg64
I1
sg65
I0
sg66
(lp6438
g1362
ag1363
asg70
S'23-04-2012'
p6439
sg72
I1
sg73
(lp6440
g1366
ag27
asssg76
(dp6441
g5
(dp6442
g38
(lp6443
(dp6444
g81
S'F '
p6445
sg83
S'NB, CMC and Mrs and Mr Verhoosel. Meeting to discuss possible concerns with mathematics. '
p6446
sg85
g27
sg86
g27
sg87
S'2011-09-23 10:36'
p6447
sa(dp6448
g81
S'F '
p6449
sg83
S'NB, CMC and Mrs and Mr Verhoosel. Meeting to discuss results of assessment. '
p6450
sg85
g27
sg86
g27
sg87
S'2011-10-19 10:36'
p6451
sa(dp6452
g81
S'F '
p6453
sg83
S'NB and Mrs Verhoosel. Mr V via Phone. Review of progress. '
p6454
sg85
g27
sg86
g27
sg87
S'2012-01-25 10:36'
p6455
sa(dp6456
g81
S'F '
p6457
sg83
S'NB, CMC, MR and MRS Verhoosel. Review of assessments from Fairly House. '
p6458
sg85
g27
sg86
g27
sg87
S'2012-05-21 12:00'
p6459
sasg37
(lp6460
(dp6461
g91
S'Lent'
p6462
sg93
S'2012'
p6463
sg95
S'Maths - Always remember to show her workings and the unit of measure.'
p6464
sg81
S'F '
p6465
sg98
g27
sg87
S'2012-02-06 12:00'
p6466
sg100
g101
sg102
g27
sg86
g27
sa(dp6467
g91
S'Mich'
p6468
sg93
S'2012'
p6469
sg95
S'Maths: Always show her workings and have a go at a question before asking for help.'
p6470
sg81
S'F '
p6471
sg98
g27
sg87
S'2012-07-19 13:00'
p6472
sg100
g101
sg102
g27
sg86
g27
sa(dp6473
g91
S'Lent'
p6474
sg93
S'2013'
p6475
sg95
S'Maths: to be confident in her own abilities and only check with teachers once she has had a go.'
p6476
sg81
S'F '
p6477
sg98
g27
sg87
S'2012-07-19 13:00'
p6478
sg100
g101
sg102
g27
sg86
g27
sa(dp6479
g91
S'Summer'
p6480
sg93
S'2013'
p6481
sg95
S'Maths: to check that her answer is a sensible one by referring back to the question.'
p6482
sg81
S'F '
p6483
sg98
g27
sg87
S'2012-07-19 13:00'
p6484
sg100
g101
sg102
g27
sg86
g27
sa(dp6485
g91
S'Mich'
p6486
sg93
S'2013'
p6487
sg95
S'Maths: 1. To ensure that she has chosen the correct number operation for the word problem and check her answer fits the question.'
p6488
sg81
S'F '
p6489
sg98
g27
sg87
S'2012-07-19 13:00'
p6490
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp6491
(dp6492
g91
S'Michaelmas'
p6493
sg81
S'F '
p6494
sg87
S'2012-02-06 12:00'
p6495
sg145
S'Spelling support group. '
p6496
sg52
S'2011'
p6497
sg148
S'weekly(x1)'
p6498
sg86
g27
sg150
S'NB'
p6499
sa(dp6500
g91
S'Lent'
p6501
sg81
S'F '
p6502
sg87
S'2012-02-06 12:00'
p6503
sg145
S'Spelling support group. '
p6504
sg52
S'2012'
p6505
sg148
S'weekly(x1)'
p6506
sg86
g27
sg150
S'NB'
p6507
sa(dp6508
g91
S'Summer'
p6509
sg81
S'F '
p6510
sg87
S'2012-07-19 13:00'
p6511
sg145
S'Self confidence lessons. '
p6512
sg52
S'2012'
p6513
sg148
S'Monthly(x2)'
p6514
sg86
g27
sg150
S'NB'
p6515
sassssS'Lewis_Whinney'
p6516
(dp6517
g3
(dp6518
g5
(dp6519
g7
I0
sg8
(lp6520
g27
ag368
asg12
S'Sen provision map for Lewis Whinney'
p6521
sg14
g15
sg16
(lp6522
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Lewis Whinney'
p6523
sg24
I1
sg25
(lp6524
g27
ag373
asg29
(lp6525
g27
ag31
asg32
S'SEN,Provision map,Lewis Whinney'
p6526
sg34
(lp6527
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp6528
g41
S'05/03/2007'
p6529
sg43
S'Whinney'
p6530
sg45
S'Action'
p6531
sg47
S'2000-01-24'
p6532
sg49
S'EP Report Nina Elliott'
p6533
sg51
g27
sg52
g935
sg54
g27
sg56
S'Lewis has some specific learning difficulties. His speed of processing visual information is relatively slow as is his speed of writing. Lewis has symptoms of ADHD including impulsivity, attention deficit and concentration. His behaviour difficulties stem from his attention deficits where he struggles to listen and follow directions as well as having difficulties getting organised. Lewis is medicated at home. He has previously been diagnosed as having poor trunk control and posture which affected his handwriting speed. VCI 112 PRI 121 WMI 97 PSI 100 FSIQ 112'
p6534
sg58
g27
sg59
S'Lewis'
p6535
sg61
S'25%'
p6536
sg62
S'8CE'
p6537
ssg64
I1
sg65
I0
sg66
(lp6538
S'Lewis has some specific learning difficulties which seem to include some dyslexic and co-ordination difficulties.'
p6539
aS'Position Lewis near the teacher and assist where possible by setting tasks with minimal distractions. Use a timer to focus his mind on the length of the task and provide check in points in the lesson or task where the teacher and Lewis can check progress. Set clear boundaries and achievable expectations and make these explicit. Teachers should continually refocus his attention to the lesson objective. Praise him wherever possible and reward with responsibility. Provide visuals which will support his working memory and allow him to develop the ability to self regulate his learning. Ensure he has correct posture when sitting and remind him of this.'
p6540
asg70
S'05-02-2007'
p6541
sg72
I0
sg73
(lp6542
g394
ag395
asssg76
(dp6543
g5
(dp6544
g38
(lp6545
(dp6546
g81
S'M '
p6547
sg83
S'NB and Mrs Whinney. Extra time in exams and general review. '
p6548
sg85
g27
sg86
g27
sg87
S'2010-11-09 15:52'
p6549
sa(dp6550
g81
S'M '
p6551
sg83
S'Teachers have indicated that although Lewis has improved with his target, it is not fully achieved. '
p6552
sg85
g27
sg86
g27
sg87
S'2010-12-14 15:52'
p6553
sa(dp6554
g81
S'M '
p6555
sg83
S'NB, SW, SM Mr and Mrs Whinney. General review of support and suggestions for future. '
p6556
sg85
g27
sg86
g27
sg87
S'2011-05-26 15:52'
p6557
sa(dp6558
g81
S'M '
p6559
sg83
S'NB, SAC Mr and Mrs Whinney. Meeting to discuss progress and support options for Lewis. '
p6560
sg85
g27
sg86
g27
sg87
S'2011-11-10 18:32'
p6561
sasg37
(lp6562
(dp6563
g91
S'Lent'
p6564
sg93
S'2011'
p6565
sg95
S'To have all of the required equipment ready within 5 minutes of the start of lessons.'
p6566
sg81
S'M '
p6567
sg98
g27
sg87
S'2010-12-14 15:52'
p6568
sg100
g139
sg102
g27
sg86
g27
sa(dp6569
g91
S'Lent'
p6570
sg93
S'2012'
p6571
sg95
S'To improve his organisation at the beginning of a lesson to ensure he can commence as required.                                                                                                                        '
p6572
sg81
S'M '
p6573
sg98
g27
sg87
S'2012-02-14 15:52'
p6574
sg100
g101
sg102
g27
sg86
g27
sa(dp6575
g91
S'Lent'
p6576
sg93
S'2012'
p6577
sg95
S'Use his laptop for all extended pieces of writing.    '
p6578
sg81
S'M '
p6579
sg98
g27
sg87
S'2012-02-14 15:52'
p6580
sg100
g101
sg102
g27
sg86
g27
sa(dp6581
g91
S'Mich'
p6582
sg93
S'2012'
p6583
sg95
S'To focus and concentration for 10 minute periods in class.'
p6584
sg81
S'M '
p6585
sg98
g27
sg87
S'2012-07-19 13:08'
p6586
sg100
g101
sg102
g27
sg86
g27
sa(dp6587
g91
S'Lent'
p6588
sg93
S'2013'
p6589
sg95
S'To focus and concentration for 10 minute periods in class. (85%)'
p6590
sg81
S'M '
p6591
sg98
g27
sg87
S'2012-07-19 13:08'
p6592
sg100
g101
sg102
g27
sg86
g27
sa(dp6593
g91
S'Summer'
p6594
sg93
S'2013'
p6595
sg95
S'To focus and concentration for 10 minute periods in class. (100%)'
p6596
sg81
S'M '
p6597
sg98
g27
sg87
S'2012-07-19 13:08'
p6598
sg100
g101
sg102
g27
sg86
g27
sasg36
(lp6599
(dp6600
g91
S'Lent'
p6601
sg81
S'M '
p6602
sg87
S'2012-07-19 13:07'
p6603
sg145
S'1:1 support lessons with Lindsey Copeman.'
p6604
sg52
S'2012'
p6605
sg148
S'weekly(x1)'
p6606
sg86
g27
sg150
g27
sa(dp6607
g91
S'Summer'
p6608
sg81
S'M '
p6609
sg87
S'2012-07-19 13:07'
p6610
sg145
S'1:1 support lessons with Lindsey Copeman.'
p6611
sg52
S'2012'
p6612
sg148
S'weekly(x1)'
p6613
sg86
g27
sg150
g27
sassssS'Lottie_Hodges'
p6614
(dp6615
g3
(dp6616
g5
(dp6617
g7
I0
sg8
(lp6618
g27
ag6150
asg12
S'Sen provision map for Lottie Hodges'
p6619
sg14
g15
sg16
(lp6620
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Lottie Hodges'
p6621
sg24
I0
sg25
(lp6622
S"Clear differentiation of tasks. Select questions to enage Lottie and assess her learning/understanding. Use Lottie's name to enage her before asking her a question or giving her an instruction. Lottie also needs instructions repeated to her and for her to tell you what the instruction is before doing it. Lottie benefits from use of manipulative equipment, visual templates, kinaesthetic and multi-sensory teaching strategies to assist understanding when completing a task. Lottie also works with a 5 minute sand timer, however we are encouraging her to become more of an independent learner."
p6623
ag27
asg29
(lp6624
g27
ag31
asg32
S'SEN,Provision map,Lottie Hodges'
p6625
sg34
(lp6626
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp6627
g41
S'01-03-2012'
p6628
sg43
S'Hodges'
p6629
sg45
S'Early years Action'
p6630
sg47
S'2007-06-28'
p6631
sg49
S'Classroom observations completed by NB on 7.3.12 and 2.5.12'
p6632
sg51
g27
sg52
g101
sg54
g27
sg56
S'Lottie appears to have difficulties working independently, She is very happy to engage in school however requires a great deal of differentiation and support from staff.'
p6633
sg58
g27
sg59
S'Lottie'
p6634
sg61
g27
sg62
S'1CE'
p6635
ssg64
I0
sg65
I0
sg66
(lp6636
g27
ag6168
asg70
g392
sg72
I0
sg73
(lp6637
g1308
ag27
asssg76
(dp6638
g5
(dp6639
g37
(lp6640
(dp6641
g91
S'Mich'
p6642
sg93
S'2012'
p6643
sg95
S'1. To follow a one or two-step instruction as soon as it is given.'
p6644
sg81
S'F '
p6645
sg98
g27
sg87
S'2012-07-19 13:58'
p6646
sg100
g101
sg102
g27
sg86
g27
sa(dp6647
g91
S'Mich'
p6648
sg93
S'2012'
p6649
sg95
S'2. To begin an adult directed activity and work for 3 minutes independently.'
p6650
sg81
S'F '
p6651
sg98
g27
sg87
S'2012-07-19 13:58'
p6652
sg100
g101
sg102
g27
sg86
g27
sa(dp6653
g91
S'Lent'
p6654
sg93
S'2013'
p6655
sg95
S'1. To participate and focus during the plenary of a lesson                                  '
p6656
sg81
S'F '
p6657
sg98
g27
sg87
S'2012-07-19 13:58'
p6658
sg100
g139
sg102
g27
sg86
g27
sa(dp6659
g91
S'Summer'
p6660
sg93
S'2013'
p6661
sg95
S'1. To participate and focus during the plenary of a lesson                                  '
p6662
sg81
S'F '
p6663
sg98
g27
sg87
S'2012-07-19 13:58'
p6664
sg100
g101
sg102
g27
sg86
g27
sa(dp6665
g91
S'Mich'
p6666
sg93
S'2013'
p6667
sg95
S'1. To use phonic knowledge to have a go at spelling and reading words without being reminded. '
p6668
sg81
S'F '
p6669
sg98
g27
sg87
S'2012-07-19 13:58'
p6670
sg100
g139
sg102
g27
sg86
g27
sa(dp6671
g91
S'Mich'
p6672
sg93
S'2013'
p6673
sg95
S'2.To begin an adult directed activity and work for 5 minutes independently.                                                                                      '
p6674
sg81
S'F '
p6675
sg98
g27
sg87
S'2012-07-19 13:58'
p6676
sg100
g139
sg102
g27
sg86
g27
sa(dp6677
g91
S'Mich'
p6678
sg93
S'2013'
p6679
sg95
S'3.To be confident of what she is able to do independently during a task and voice that to an adult.                                                                                    '
p6680
sg81
S'F '
p6681
sg98
g27
sg87
S'2012-07-19 13:58'
p6682
sg100
g139
sg102
g27
sg86
g27
sa(dp6683
g91
S'Lent'
p6684
sg93
S'2013'
p6685
sg95
S'2. To begin an adult directed activity and work for 5 minutes independently.         '
p6686
sg81
S'F '
p6687
sg98
g27
sg87
S'2013-07-16 09:22'
p6688
sg100
g139
sg102
g27
sg86
g27
sa(dp6689
g91
S'Summer'
p6690
sg93
S'2013'
p6691
sg95
S'2. To begin an adult directed activity and work for 5 minutes independently.         '
p6692
sg81
S'F '
p6693
sg98
g27
sg87
S'2013-07-16 09:22'
p6694
sg100
g101
sg102
g27
sg86
g27
sa(dp6695
g91
S'Lent'
p6696
sg93
S'2013'
p6697
sg95
S'3.  To follow a one or two-step instruction as soon as it is given.      '
p6698
sg81
S'F '
p6699
sg98
g27
sg87
S'2013-07-16 09:22'
p6700
sg100
g139
sg102
g27
sg86
g27
sa(dp6701
g91
S'Summer'
p6702
sg93
S'2013'
p6703
sg95
S'3.  To follow a one or two-step instruction as soon as it is given.      '
p6704
sg81
S'F '
p6705
sg98
g27
sg87
S'2013-07-16 09:22'
p6706
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp6707
(dp6708
g91
S'Lent'
p6709
sg81
S'F '
p6710
sg87
S'2012-07-17 10:10'
p6711
sg145
S'Learning support in class and 1:1.'
p6712
sg52
S'2012'
p6713
sg148
S'weekly(x1)'
p6714
sg86
g27
sg150
S'AST'
p6715
sa(dp6716
g91
S'Summer'
p6717
sg81
S'F '
p6718
sg87
S'2012-07-17 10:10'
p6719
sg145
S'Learning support in class and 1:1.'
p6720
sg52
S'2012'
p6721
sg148
S'weekly(x1)'
p6722
sg86
g27
sg150
S'KM'
p6723
sa(dp6724
g91
S'Michaelmas'
p6725
sg81
S'F '
p6726
sg87
S'2013-07-08 09:49'
p6727
sg145
S'Maths support group'
p6728
sg52
S'2012'
p6729
sg148
S'weekly(x1)'
p6730
sg86
g27
sg150
S'KM'
p6731
sa(dp6732
g91
S'Lent'
p6733
sg81
S'F '
p6734
sg87
S'2013-07-08 09:49'
p6735
sg145
S'Maths support group'
p6736
sg52
S'2013'
p6737
sg148
S'weekly(x1)'
p6738
sg86
g27
sg150
S'KM'
p6739
sa(dp6740
g91
S'Summer'
p6741
sg81
S'F '
p6742
sg87
S'2013-07-08 09:49'
p6743
sg145
S'Maths support group'
p6744
sg52
S'2013'
p6745
sg148
S'weekly(x1)'
p6746
sg86
g27
sg150
S'KM'
p6747
sa(dp6748
g91
S'Lent'
p6749
sg81
S'F '
p6750
sg87
S'2013-07-08 09:49'
p6751
sg145
S'1:1 Hornet'
p6752
sg52
S'2013'
p6753
sg148
S'weekly(x1)'
p6754
sg86
g27
sg150
S'KM'
p6755
sa(dp6756
g91
S'Summer'
p6757
sg81
S'F '
p6758
sg87
S'2013-07-08 09:49'
p6759
sg145
S'1:1 Hornet'
p6760
sg52
S'2013'
p6761
sg148
S'weekly(x1)'
p6762
sg86
g27
sg150
S'KM'
p6763
sa(dp6764
g91
S'Michaelmas'
p6765
sg81
S'F '
p6766
sg87
S'2013-07-08 09:49'
p6767
sg145
S'Literacy/phonics support'
p6768
sg52
S'2012'
p6769
sg148
S'weekly(x4)'
p6770
sg86
g27
sg150
S'EJ'
p6771
sa(dp6772
g91
S'Lent'
p6773
sg81
S'F '
p6774
sg87
S'2013-07-08 09:49'
p6775
sg145
S'Literacy/phonics support'
p6776
sg52
S'2013'
p6777
sg148
S'weekly(x4)'
p6778
sg86
g27
sg150
S'EJ'
p6779
sa(dp6780
g91
S'Summer'
p6781
sg81
S'F '
p6782
sg87
S'2013-07-08 09:49'
p6783
sg145
S'Literacy/phonics support'
p6784
sg52
S'2013'
p6785
sg148
S'weekly(x4)'
p6786
sg86
g27
sg150
S'EJ'
p6787
sassssS'Alexander_Khandke'
p6788
(dp6789
g3
(dp6790
g5
(dp6791
g7
I0
sg8
(lp6792
g27
ag368
asg12
S'Sen provision map for Alexander Khandke'
p6793
sg14
g15
sg16
(lp6794
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Alexander Khandke'
p6795
sg24
I0
sg25
(lp6796
S'When marking work be selective when highlighting spelling and follow the marking policy to ensure the feedback to Alex supports his learning rather than discouraging him.'
p6797
ag27
asg29
(lp6798
g27
ag31
asg32
S'SEN,Provision map,Alexander Khandke'
p6799
sg34
(lp6800
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp6801
g41
S'17/06/2010'
p6802
sg43
S'Khandke'
p6803
sg45
S'Action'
p6804
sg47
S'2002-10-07'
p6805
sg49
S'DST-J completed by AS 17.6.10'
p6806
sg51
g27
sg52
g384
sg54
g27
sg56
S'Alex was referred to LE department for assessment due to poor spelling skills and due to weak Incas results. Word recognition, decoding and comprehension were all ahead of his chronological age.'
p6807
sg58
g27
sg59
S'Alexander'
p6808
sg61
g27
sg62
S'5CE'
p6809
ssg64
I0
sg65
I0
sg66
(lp6810
S"An error analysis of Alex's spellings in the assessment test indicates that there may be an underlying processing difficulty related to auditory discrimination. There may be times when it seems Alex has misunderstood verbal information. Be aware of this and check back with him as appropriate. Be aware that there may be a difficulty affecting spelling and ensure that Alex feels encourage and supported in expressing himself in written assignments despite his relative weakness with spelling."
p6811
aS'Provide a list of high frequency words which Alex can use to support his writing. introduce a have a go book where Alex can record attempts at spelling. This will then be a record that he can refer back to in the future. Encourage Alex to look closely and try new words. Praise close attempts at spelling, and tell him why it was a good attempt. Continue with the additional phonic based spelling activities. Ensure all instructions are kept precise and check understanding and retention by asking for repetition and clarification during task. Break tasks down into their smallest components and encourage completion of each step at a time. Check understanding of vocabulary when introducing new concepts or topics. Praise sustained attention and active participation in class. Use visual techniques to support memory for spelling such as word shape activites. During written tasks be aware that Alex may become very frustrated due to his difficulties expressing himself in writing, if this is observed allow him to discuss his knowledge and be sure to use this in assessment.'
p6812
asg70
g392
sg72
I0
sg73
(lp6813
g394
ag395
asssg76
(dp6814
g5
(dp6815
g38
(lp6816
(dp6817
g81
S'M '
p6818
sg83
S'NB and Miss Fidler. Discussion of school assessment and suggestions for support. '
p6819
sg85
S'21-10-2010'
p6820
sg86
g27
sg87
S'2010-10-21 14:19'
p6821
sa(dp6822
g81
S'M '
p6823
sg83
S'NB and Miss Fidler. Progress meeting and review of support and interventions.'
p6824
sg85
S'19-01-2011'
p6825
sg86
g27
sg87
S'2010-12-13 14:19'
p6826
sa(dp6827
g81
S'M '
p6828
sg83
S'NB and Miss Fidler. Discussion of school assessment and suggestions for support.'
p6829
sg85
S'21-10-2010'
p6830
sg86
g27
sg87
S'2010-12-13 14:19'
p6831
sa(dp6832
g81
S'M '
p6833
sg83
S'NB and Miss Fidler. Progress meeting and review of support and interventions.'
p6834
sg85
S'19-01-2011'
p6835
sg86
g27
sg87
S'2011-01-19 14:19'
p6836
sa(dp6837
g81
S'M '
p6838
sg83
S'NB and Miss Fidler. Review of progress and support. '
p6839
sg85
g27
sg86
g27
sg87
S'2011-01-19 14:19'
p6840
sa(dp6841
g81
S'M '
p6842
sg83
S'CMC, NB, MR Khandke and Miss Fidler. Review of EP report and discussion of support for year 5. '
p6843
sg85
g27
sg86
g27
sg87
S'2012-05-23 14:19'
p6844
sasg37
(lp6845
(dp6846
g91
S'Lent'
p6847
sg93
S'2011'
p6848
sg95
S'To use the spelling strategies from spelling group when attempting unknown words.'
p6849
sg81
S'M '
p6850
sg98
g27
sg87
S'2010-12-13 14:19'
p6851
sg100
g101
sg102
g27
sg86
g27
sa(dp6852
g91
S'Mich'
p6853
sg93
S'2011'
p6854
sg95
S'To edit work carefully to make sure spellings are correct. ONGOING'
p6855
sg81
S'M '
p6856
sg98
g27
sg87
S'2011-09-21 12:12'
p6857
sg100
g101
sg102
S'Provide word lists for Alex.Allow checking time.'
p6858
sg86
S'AP'
p6859
sa(dp6860
g91
S'Lent'
p6861
sg93
S'2012'
p6862
sg95
S'To edit work to check carefully for demon spellings.'
p6863
sg81
S'M '
p6864
sg98
g27
sg87
S'2012-01-25 17:01'
p6865
sg100
g101
sg102
S'Have a go book.Ensure Alex has extra time to self edit work.'
p6866
sg86
S'AP'
p6867
sa(dp6868
g91
S'Mich'
p6869
sg93
S'2012'
p6870
sg95
S'To self edit work focusing on checking spelling before submitting for marking.'
p6871
sg81
S'M '
p6872
sg98
g27
sg87
S'2012-07-19 11:44'
p6873
sg100
g101
sg102
g27
sg86
g27
sa(dp6874
g91
S'Lent'
p6875
sg93
S'2013'
p6876
sg95
S'To check my answer at least one time before handing it in to make sure it makes sense and answers the question. '
p6877
sg81
S'M '
p6878
sg98
g27
sg87
S'2012-07-19 11:44'
p6879
sg100
g101
sg102
g27
sg86
g27
sasg36
(lp6880
(dp6881
g91
S'Michaelmas'
p6882
sg81
S'M '
p6883
sg87
S'2010-12-13 14:18'
p6884
sg145
S'Small group spelling support focusing on spelling strategies.'
p6885
sg52
S'2010'
p6886
sg148
S'weekly(x1)'
p6887
sg86
g27
sg150
S'NB'
p6888
sa(dp6889
g91
S'Lent'
p6890
sg81
S'M '
p6891
sg87
S'2010-12-13 14:18'
p6892
sg145
S'Small group spelling support focusing on spelling strategies.'
p6893
sg52
S'2011'
p6894
sg148
S'weekly(x1)'
p6895
sg86
g27
sg150
S'NB'
p6896
sa(dp6897
g91
S'Summer'
p6898
sg81
S'M '
p6899
sg87
S'2010-12-13 14:18'
p6900
sg145
S'Small group spelling support focusing on spelling strategies.'
p6901
sg52
S'2011'
p6902
sg148
S'weekly(x1)'
p6903
sg86
g27
sg150
S'NB'
p6904
sassssS'Matilda_Howarth'
p6905
(dp6906
g3
(dp6907
g5
(dp6908
g7
I0
sg8
(lp6909
g1828
ag1829
asg12
S'Sen provision map for Matilda Howarth'
p6910
sg14
g15
sg16
(lp6911
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Matilda Howarth'
p6912
sg24
I0
sg25
(lp6913
g27
ag1834
asg29
(lp6914
g27
ag31
asg32
S'SEN,Provision map,Matilda Howarth'
p6915
sg34
(lp6916
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp6917
g41
S'01/04/2010'
p6918
sg43
S'Howarth'
p6919
sg45
S'Action'
p6920
sg47
S'2002-10-02'
p6921
sg49
S'EP Report completed by Beverley Steffert. (14-06-2010)'
p6922
sg51
g27
sg52
g384
sg54
g27
sg56
S'Tilly has very superior verbal intellectual ability and superior non verbal intellectual ability. Her speed of processing is extremely rapid. Tilly has a specific learning difficulty of a Dyslexic nature. Despite her high level of intelligence, Tilly has a low level of understanding of the phonological code on which reading and spelling are based. Tilly will often use alternative words in written work as she cannot recall more complex spellings impacting on her communication of understanding.\r<br>VR 132\r<br>NVR 129\r<br>WM 113\r<br>PR 133'
p6923
sg58
g27
sg59
S'Matilda'
p6924
sg61
g27
sg62
S'5CN'
p6925
ssg64
I1
sg65
I0
sg66
(lp6926
S'Tilly has a specific learning difficulty of a Dyslexic nature.'
p6927
aS'Teachers should explain the overall concept before discussing specific details allowing Tilly to see the whole concept being taught. Provide a word list of high frequency words to support writing. Encourage the use of a trying book with new spellings or words she is not familiar with. Encourage Tilly to look closely and try new words. Praise close attempts at spelling, and tell her why it was a good attempt. Ensure all instructions are kept precise and check understanding and retention by asking for repetition and clarification during task. Break tasks down into their smallest components and encourage completion of each step at a time. Use visual techniques to support memory for spelling such as word shape activites. During written tasks be aware that Tilly may become very frustrated due to her difficulties expressing herself in writing, if this is observed allow her to discuss her knowledge and be sure to use this in assessment.'
p6928
asg70
S'14-06-2010'
p6929
sg72
I0
sg73
(lp6930
g1852
ag1853
asssg76
(dp6931
g5
(dp6932
g38
(lp6933
(dp6934
g81
S'F '
p6935
sg83
S'Meeting held to discuss observations of NB.Full EP report suggested. Present NB, IG, HSM, Mr and Mrs Howarth. '
p6936
sg85
S'07-05-2010'
p6937
sg86
g27
sg87
S'2010-05-07 14:31'
p6938
sa(dp6939
g81
S'F '
p6940
sg83
S'Meeting held to discuss progress and targets for school based support. Present NB, CMC, JW, Mr and Mrs Howarth. More detailed noted contained in SEND student file. '
p6941
sg85
S'22-09-2010'
p6942
sg86
g27
sg87
S'2010-09-22 14:31'
p6943
sa(dp6944
g81
S'F '
p6945
sg83
S'Mr and Mrs Howarth, CAE, NB (JW absent). Review meeting, confidence and future plans. '
p6946
sg85
g27
sg86
g27
sg87
S'2011-06-30 14:31'
p6947
sa(dp6948
g81
S'F '
p6949
sg83
S'NB, CD, Mr and Mrs Howarth. Meeting to discuss progress and support. '
p6950
sg85
S'21-11-2011'
p6951
sg86
g27
sg87
S'2011-11-21 17:10'
p6952
sasg37
(lp6953
(dp6954
g91
S'Lent'
p6955
sg93
S'2011'
p6956
sg95
S'To always highlight key words in written questions to improve the ability to understand what a question is asking and answer with greater accuracy.'
p6957
sg81
S'F '
p6958
sg98
g27
sg87
S'2010-12-13 14:35'
p6959
sg100
g101
sg102
g27
sg86
g27
sa(dp6960
g91
S'Lent'
p6961
sg93
S'2011'
p6962
sg95
S'To actively seek support from the teacher in any lesson when unsure of what is required.'
p6963
sg81
S'F '
p6964
sg98
g27
sg87
S'2010-12-13 14:35'
p6965
sg100
g101
sg102
g27
sg86
g27
sa(dp6966
g91
S'Mich'
p6967
sg93
S'2011'
p6968
sg95
S'Discuss ideas with teacher before writing them down to support fluency in writing.'
p6969
sg81
S'F '
p6970
sg98
g27
sg87
S'2011-09-21 12:19'
p6971
sg100
g101
sg102
S'Provide HF word lists to support spelling.Regular praise and encouragement.'
p6972
sg86
S'AP'
p6973
sa(dp6974
g91
S'Lent'
p6975
sg93
S'2012'
p6976
sg95
S'Checking spellings in everyday writing using spelling card across all subjects.'
p6977
sg81
S'F '
p6978
sg98
g27
sg87
S'2012-01-25 16:56'
p6979
sg100
g101
sg102
S'Spelling card provided.'
p6980
sg86
S'AP'
p6981
sa(dp6982
g91
S'Mich'
p6983
sg93
S'2012'
p6984
sg95
S'To try and use the time she is given to finish a task and not to rush her work.'
p6985
sg81
S'F '
p6986
sg98
g27
sg87
S'2012-07-19 14:03'
p6987
sg100
g101
sg102
g27
sg86
g27
sa(dp6988
g91
S'Lent'
p6989
sg93
S'2013'
p6990
sg95
S'To check my answer at least one time before handing it in to make sure it makes sense and answers the question. '
p6991
sg81
S'F '
p6992
sg98
g27
sg87
S'2012-07-19 14:03'
p6993
sg100
g139
sg102
g27
sg86
g27
sa(dp6994
g91
S'Summer'
p6995
sg93
S'2013'
p6996
sg95
S'To check my answer at least one time before handing it in to make sure it makes sense and answers the question. '
p6997
sg81
S'F '
p6998
sg98
g27
sg87
S'2012-07-19 14:03'
p6999
sg100
g101
sg102
g27
sg86
g27
sa(dp7000
g91
S'Summer'
p7001
sg93
S'2013'
p7002
sg95
S'2. To take a risk with my learning and to trust by ability before asking for teacher approval. '
p7003
sg81
S'F '
p7004
sg98
g27
sg87
S'2012-07-19 14:03'
p7005
sg100
g101
sg102
g27
sg86
g27
sa(dp7006
g91
S'Mich'
p7007
sg93
S'2013'
p7008
sg95
S'To self evaulate her performance in lessons and communicate with staff when she needs support. '
p7009
sg81
S'F '
p7010
sg98
g27
sg87
S'2013-07-16 10:47'
p7011
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp7012
(dp7013
g91
S'Michaelmas'
p7014
sg81
S'F '
p7015
sg87
S'2010-12-13 14:31'
p7016
sg145
S'Small group spelling support focusing on spelling strategies.'
p7017
sg52
S'2010'
p7018
sg148
S'weekly(x1)'
p7019
sg86
g27
sg150
S'NB'
p7020
sa(dp7021
g91
S'Lent'
p7022
sg81
S'F '
p7023
sg87
S'2010-12-13 14:31'
p7024
sg145
S'Small group spelling support focusing on spelling strategies.'
p7025
sg52
S'2011'
p7026
sg148
S'weekly(x1)'
p7027
sg86
g27
sg150
S'NB'
p7028
sa(dp7029
g91
S'Summer'
p7030
sg81
S'F '
p7031
sg87
S'2010-12-13 14:31'
p7032
sg145
S'Small group spelling support focusing on spelling strategies.'
p7033
sg52
S'2011'
p7034
sg148
S'weekly(x1)'
p7035
sg86
g27
sg150
S'NB'
p7036
sa(dp7037
g91
S'Michaelmas'
p7038
sg81
S'F '
p7039
sg87
S'2010-12-13 14:31'
p7040
sg145
S'Small group spelling support focusing on spelling strategies.'
p7041
sg52
S'2011'
p7042
sg148
S'weekly(x1)'
p7043
sg86
g27
sg150
S'NB'
p7044
sa(dp7045
g91
S'Lent'
p7046
sg81
S'F '
p7047
sg87
S'2012-02-06 14:31'
p7048
sg145
S'Small group spelling support focusing on spelling strategies.'
p7049
sg52
S'2012'
p7050
sg148
S'weekly(x1)'
p7051
sg86
g27
sg150
S'NB'
p7052
sa(dp7053
g91
S'Lent'
p7054
sg81
S'F '
p7055
sg87
S'2012-02-28 14:31'
p7056
sg145
S'Small group spelling support focusing on spelling strategies.'
p7057
sg52
S'2012'
p7058
sg148
S'weekly(x1)'
p7059
sg86
g27
sg150
S'NB'
p7060
sa(dp7061
g91
S'Summer'
p7062
sg81
S'F '
p7063
sg87
S'2012-02-28 14:31'
p7064
sg145
S'Small group spelling support focusing on spelling strategies.'
p7065
sg52
S'2012'
p7066
sg148
S'weekly(x1)'
p7067
sg86
g27
sg150
S'NB'
p7068
sassssS'Connie_Williams-Thomas'
p7069
(dp7070
g3
(dp7071
g5
(dp7072
g7
I0
sg8
(lp7073
g1548
ag1549
asg12
S'Sen provision map for Connie Williams-Thomas'
p7074
sg14
g15
sg16
(lp7075
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Connie Williams-Thomas'
p7076
sg24
I0
sg25
(lp7077
g27
aS'Ensure all instructions are kept precise and check understanding and retention by asking for repetition and clarification during task. Use any appropriate devices to support her memory " flashcards, diagrams, visual prompts, lists of tasks to complete. Break tasks down into their smallest components and encourage completion of each step at a time. Praise sustained attention and active participation in class.'
p7078
asg29
(lp7079
g562
ag27
asg32
S'SEN,Provision map,Connie Williams-Thomas'
p7080
sg34
(lp7081
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp7082
g41
S'11/11/2009'
p7083
sg43
S'Williams-Thomas'
p7084
sg45
S'Action +'
p7085
sg47
S'2002-04-15'
p7086
sg49
S'EP Report Nina Elliott'
p7087
sg51
S'Both'
p7088
sg52
g1564
sg54
g27
sg56
S'Connie has strong verbal comprehension and her perceptual reasoning skills are good. She has weak visual processing, speed of processing, memory, phonological skills, fine motor skills and concentration which impacts on her spelling, writing, reading and written expression.  Connie lacks confidence and needs extra time to process information. Connie has Dyslexia. VCI 116 PRI 121 WMI 102 PSI 100 FSIQ 115'
p7089
sg58
g27
sg59
S'Connie'
p7090
sg61
S'25%'
p7091
sg62
S'6CN'
p7092
ssg64
I1
sg65
I0
sg66
(lp7093
S'Connie has Dyslexia.'
p7094
aS'Allow extra time for Connie to respond to questions and understand what is being asked of her. Providing visual cues and help in organising her writing will support her difficulties with visual processing and memory. Provide a word list of high frequency words to support writing. Encourage the use of a trying book with new spellings or words she is not familiar with. This will then be a record that she can refer back to in the future. Encourage Connie to look closely and try new words. Praise close attempts at spelling, and tell her why it was a good attempt. Check understanding of vocabulary when introducing new concepts or topics.  Scaffold copying if necessary. As speed of processing is a minor problem for Connie, have prepared worksheets that she can copy from her desk. This will assist in maintaining attention on the activity and reduce losing her place in between the board and her book.'
p7095
asg70
S'11-11-2009'
p7096
sg72
I0
sg73
(lp7097
g581
ag582
asssg76
(dp7098
g5
(dp7099
g38
(lp7100
(dp7101
g81
S'F '
p7102
sg83
S'Summer Report - Clare Cooper. '
p7103
sg85
g27
sg86
g27
sg87
S'2010-07-07 15:07'
p7104
sa(dp7105
g81
S'F '
p7106
sg83
S'Michaelmas 2010 Report- Isabel Clarke. '
p7107
sg85
g27
sg86
g27
sg87
S'2010-12-19 15:08'
p7108
sa(dp7109
g81
S'F '
p7110
sg83
S'NB, CAE, AP, Isabel Clarke, Mr and Mrs WT. Assessment results discussion and interventions for future support. '
p7111
sg85
g27
sg86
g27
sg87
S'2011-02-15 15:06'
p7112
sa(dp7113
g81
S'F '
p7114
sg83
S'Connie visited a behavioural optometrist. '
p7115
sg85
g27
sg86
g27
sg87
S'2011-03-16 15:06'
p7116
sa(dp7117
g81
S'F '
p7118
sg83
S'NB, AP, ET, Mr and Mrs Thomas. Progress meeting to discuss year 4 results. '
p7119
sg85
g27
sg86
g27
sg87
S'2011-07-04 15:19'
p7120
sasg37
(lp7121
(dp7122
g91
S'Lent'
p7123
sg93
S'2011'
p7124
sg95
S'To use the spelling strategies from spelling group when attempting unknown words. '
p7125
sg81
S'F '
p7126
sg98
g27
sg87
S'2010-12-13 15:17'
p7127
sg100
g101
sg102
g27
sg86
g27
sa(dp7128
g91
S'Mich'
p7129
sg93
S'2011'
p7130
sg95
S'To consistently display focus and concentration in all lessons.'
p7131
sg81
S'F '
p7132
sg98
g27
sg87
S'2011-10-10 15:17'
p7133
sg100
g101
sg102
g27
sg86
g27
sa(dp7134
g91
S'Lent'
p7135
sg93
S'2012'
p7136
sg95
S'To consistently display focus and concentration in all lessons. '
p7137
sg81
S'F '
p7138
sg98
g27
sg87
S'2012-02-27 15:17'
p7139
sg100
g101
sg102
g27
sg86
g27
sa(dp7140
g91
S'Lent'
p7141
sg93
S'2012'
p7142
sg95
S' To wear her glasses in all necessary lessons.'
p7143
sg81
S'F '
p7144
sg98
g27
sg87
S'2012-02-27 15:17'
p7145
sg100
g101
sg102
g27
sg86
g27
sa(dp7146
g91
S'Mich'
p7147
sg93
S'2012'
p7148
sg95
S'To ask more questions and communicate with staff if she does not understand lesson content.'
p7149
sg81
S'F '
p7150
sg98
g27
sg87
S'2012-07-19 13:23'
p7151
sg100
g101
sg102
g27
sg86
g27
sa(dp7152
g91
S'Lent'
p7153
sg93
S'2013'
p7154
sg95
S'To be confident in my abilities through attempting questions first time without needing to check with teachers if my answer is correct. (75% of the time). '
p7155
sg81
S'F '
p7156
sg98
g27
sg87
S'2012-07-19 13:23'
p7157
sg100
g101
sg102
g27
sg86
g27
sa(dp7158
g91
S'Summer'
p7159
sg93
S'2013'
p7160
sg95
S'To be confident in my abilities through attempting questions first time without needing to check with teachers if my answer is correct. (90% of the time). '
p7161
sg81
S'F '
p7162
sg98
g27
sg87
S'2012-07-19 13:23'
p7163
sg100
g101
sg102
g27
sg86
g27
sa(dp7164
g91
S'Mich'
p7165
sg93
S'2013'
p7166
sg95
S'To believe in my own abilities and be confident that I can manage and compete in higher sets. '
p7167
sg81
S'F '
p7168
sg98
g27
sg87
S'2013-07-16 11:01'
p7169
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp7170
(dp7171
g91
S'Michaelmas'
p7172
sg81
S'F '
p7173
sg87
S'2010-02-16 15:19'
p7174
sg145
S'Small group spelling support focusing on spelling strategies.'
p7175
sg52
S'2010'
p7176
sg148
S'weekly(x1)'
p7177
sg86
g27
sg150
S'NB'
p7178
sa(dp7179
g91
S'Lent'
p7180
sg81
S'F '
p7181
sg87
S'2010-02-16 15:19'
p7182
sg145
S'1:1 Specialist Teaching Claire Cooper'
p7183
sg52
S'2010'
p7184
sg148
S'weekly(x1)'
p7185
sg86
g27
sg150
g27
sa(dp7186
g91
S'Michaelmas'
p7187
sg81
S'F '
p7188
sg87
S'2010-02-16 15:19'
p7189
sg145
S'1:1 Specialist Teaching Isabel Clarke'
p7190
sg52
S'2010'
p7191
sg148
S'weekly(x1)'
p7192
sg86
g27
sg150
g27
sa(dp7193
g91
S'Summer'
p7194
sg81
S'F '
p7195
sg87
S'2010-02-16 15:19'
p7196
sg145
S'1:1 Specialist Teaching Claire Cooper'
p7197
sg52
S'2010'
p7198
sg148
S'weekly(x1)'
p7199
sg86
g27
sg150
g27
sa(dp7200
g91
S'Michaelmas'
p7201
sg81
S'F '
p7202
sg87
S'2010-02-16 15:19'
p7203
sg145
S'Reading comprehension support group.'
p7204
sg52
S'2010'
p7205
sg148
S'weekly(x1)'
p7206
sg86
g27
sg150
S'KM'
p7207
sa(dp7208
g91
S'Lent'
p7209
sg81
S'F '
p7210
sg87
S'2010-02-16 15:19'
p7211
sg145
S'Small group spelling support focusing on spelling strategies.'
p7212
sg52
S'2011'
p7213
sg148
S'weekly(x1)'
p7214
sg86
g27
sg150
S'NB'
p7215
sa(dp7216
g91
S'Lent'
p7217
sg81
S'F '
p7218
sg87
S'2010-02-16 15:19'
p7219
sg145
S'1:1 Specialist Teaching Isabel Clarke'
p7220
sg52
S'2011'
p7221
sg148
S'weekly(x1)'
p7222
sg86
g27
sg150
g27
sa(dp7223
g91
S'Lent'
p7224
sg81
S'F '
p7225
sg87
S'2010-02-16 15:19'
p7226
sg145
S'Reading comprehension support group.'
p7227
sg52
S'2011'
p7228
sg148
S'weekly(x1)'
p7229
sg86
g27
sg150
S'KM'
p7230
sa(dp7231
g91
S'Summer'
p7232
sg81
S'F '
p7233
sg87
S'2010-02-16 15:19'
p7234
sg145
S'Reading comprehension support group.'
p7235
sg52
S'2011'
p7236
sg148
S'weekly(x1)'
p7237
sg86
g27
sg150
S'AST'
p7238
sa(dp7239
g91
S'Summer'
p7240
sg81
S'F '
p7241
sg87
S'2010-02-16 15:19'
p7242
sg145
S'1:1 Specialist Teaching Isabel Clarke'
p7243
sg52
S'2011'
p7244
sg148
S'weekly(x1)'
p7245
sg86
g27
sg150
g27
sa(dp7246
g91
S'Summer'
p7247
sg81
S'F '
p7248
sg87
S'2010-02-16 15:19'
p7249
sg145
S'Small group spelling support focusing on spelling strategies.'
p7250
sg52
S'2011'
p7251
sg148
S'weekly(x1)'
p7252
sg86
g27
sg150
S'NB'
p7253
sa(dp7254
g91
S'Michaelmas'
p7255
sg81
S'F '
p7256
sg87
S'2010-02-16 15:19'
p7257
sg145
S'1:1 Specialist Teaching Isabel Clarke'
p7258
sg52
S'2011'
p7259
sg148
S'weekly(x1)'
p7260
sg86
g27
sg150
g27
sa(dp7261
g91
S'Lent'
p7262
sg81
S'F '
p7263
sg87
S'2012-02-03 15:19'
p7264
sg145
S'1:1 Specialist Teaching Lindsey Copmean'
p7265
sg52
S'2012'
p7266
sg148
S'weekly(x1)'
p7267
sg86
g27
sg150
g27
sa(dp7268
g91
S'Summer'
p7269
sg81
S'F '
p7270
sg87
S'2012-05-03 15:19'
p7271
sg145
S'1:1 Specialist Teaching Lindsey Copmean'
p7272
sg52
S'2012'
p7273
sg148
S'weekly(x1)'
p7274
sg86
g27
sg150
g27
sa(dp7275
g91
S'Michaelmas'
p7276
sg81
S'F '
p7277
sg87
S'2012-05-03 15:19'
p7278
sg145
S'1:1 Specialist Teaching Lindsey Copmean'
p7279
sg52
S'2012'
p7280
sg148
S'weekly(x1)'
p7281
sg86
g27
sg150
g27
sa(dp7282
g91
S'Lent'
p7283
sg81
S'F '
p7284
sg87
S'2012-05-03 15:19'
p7285
sg145
S'1:1 Specialist Teaching Lindsey Copmean'
p7286
sg52
S'2013'
p7287
sg148
S'weekly(x1)'
p7288
sg86
g27
sg150
g27
sa(dp7289
g91
S'Summer'
p7290
sg81
S'F '
p7291
sg87
S'2012-05-03 15:19'
p7292
sg145
S'1:1 Specialist Teaching Lindsey Copmean'
p7293
sg52
S'2013'
p7294
sg148
S'weekly(x1)'
p7295
sg86
g27
sg150
g27
sassssS'Felix_Warne'
p7296
(dp7297
g3
(dp7298
g5
(dp7299
g7
I0
sg8
(lp7300
g27
ag5047
asg12
S'Sen provision map for Felix Warne'
p7301
sg14
g15
sg16
(lp7302
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Felix Warne'
p7303
sg24
I0
sg25
(lp7304
g27
ag988
asg29
(lp7305
g562
ag27
asg32
S'SEN,Provision map,Felix Warne'
p7306
sg34
(lp7307
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp7308
g41
S'14/6/2010'
p7309
sg43
S'Warne'
p7310
sg45
S'Action'
p7311
sg47
S'2002-05-23'
p7312
sg49
S'DST-J, by AS on the 14/6/10\r<br>NEALE by AS on 1.3.10'
p7313
sg51
g27
sg52
g1564
sg54
g27
sg56
S'Felix was referred following concerns raised regarding aspects of literacy and handwriting. No formal assessment has been completed on Felix due to family decision. Felix is often chatty and his focus and concentration at times prevents him from completing the lesson tasks to his potential. His writing speed is slow and often untidy. He requires support to help arrange his thoughts.'
p7314
sg58
g27
sg59
S'Felix'
p7315
sg61
g27
sg62
S'6CN'
p7316
ssg64
I0
sg65
I0
sg66
(lp7317
S'Felix has general difficulties in literacy that have not been officially diagnosed by an EP.'
p7318
aS'Provide Felix with time to arrange his thoughts and where appropriate. He should be given extra time with tasks that require reading. As appropriate encourage Felix to break unknown words into constituent parts and sound out loud, scaffold this process. At home, encourage audio books to maintain his knowledge of English structures. Be aware that Felix has very strong verbal abilities and will be finding it frustrating with written tasks. Praise and acknowledge verbal contributions. Provide explicit instructions with handwriting and help him with spacing and joins using visuals where possible.'
p7319
asg70
g392
sg72
I0
sg73
(lp7320
g581
ag582
asssg76
(dp7321
g5
(dp7322
g38
(lp7323
(dp7324
g81
S'M '
p7325
sg83
S'Phone conversation with Mrs Warne and NB regarding having Felix assessed by an EP. NB suggested this happen ASAP so he can utilise any access arrangements leading into his 11+ year. NB emailed contact details to Mrs Warne. '
p7326
sg85
g27
sg86
g27
sg87
S'2012-07-17 13:04'
p7327
sasg37
(lp7328
(dp7329
g91
S'Lent'
p7330
sg93
S'2011'
p7331
sg95
S'To commence activities independently within 2 minutes once instruction and support has been given. '
p7332
sg81
S'M '
p7333
sg98
g27
sg87
S'2010-12-14 09:34'
p7334
sg100
g101
sg102
g27
sg86
g27
sa(dp7335
g91
S'Mich'
p7336
sg93
S'2011'
p7337
sg95
S'To increase independence as a learner especially through reducing the occurrence of questions which he knows the answer to.'
p7338
sg81
S'M '
p7339
sg98
g27
sg87
S'2011-10-10 09:34'
p7340
sg100
g139
sg102
g27
sg86
g27
sa(dp7341
g91
S'Mich'
p7342
sg93
S'2012'
p7343
sg95
S'To increase independence as a learner through asking for help only if required.'
p7344
sg81
S'M '
p7345
sg98
g27
sg87
S'2012-07-19 13:03'
p7346
sg100
g139
sg102
g27
sg86
g27
sa(dp7347
g91
S'Lent'
p7348
sg93
S'2013'
p7349
sg95
S'To increase independence as a learner through asking for help only if required.'
p7350
sg81
S'M '
p7351
sg98
g27
sg87
S'2012-07-19 13:03'
p7352
sg100
g101
sg102
g27
sg86
g27
sa(dp7353
g91
S'Lent'
p7354
sg93
S'2013'
p7355
sg95
S'2. To complete the tasks given by teachers in the time allocated. (75% of the time). '
p7356
sg81
S'M '
p7357
sg98
g27
sg87
S'2012-07-19 13:03'
p7358
sg100
g101
sg102
g27
sg86
g27
sasg36
(lp7359
(dp7360
g91
S'Michaelmas'
p7361
sg81
S'M '
p7362
sg87
S'2010-12-14 09:31'
p7363
sg145
S'Handwriting Support group. '
p7364
sg52
S'2010'
p7365
sg148
S'weekly(x1)'
p7366
sg86
g27
sg150
S'NB'
p7367
sa(dp7368
g91
S'Michaelmas'
p7369
sg81
S'M '
p7370
sg87
S'2010-12-14 09:31'
p7371
sg145
S'Small group spelling support focusing on spelling strategies.'
p7372
sg52
S'2010'
p7373
sg148
S'weekly(x1)'
p7374
sg86
g27
sg150
S'NB'
p7375
sa(dp7376
g91
S'Lent'
p7377
sg81
S'M '
p7378
sg87
S'2010-12-14 09:31'
p7379
sg145
S'Handwriting Support group.'
p7380
sg52
S'2011'
p7381
sg148
S'weekly(x1)'
p7382
sg86
g27
sg150
S'NB'
p7383
sa(dp7384
g91
S'Lent'
p7385
sg81
S'M '
p7386
sg87
S'2010-12-14 09:31'
p7387
sg145
S'Small group spelling support focusing on spelling strategies.'
p7388
sg52
S'2011'
p7389
sg148
S'weekly(x1)'
p7390
sg86
g27
sg150
S'NB'
p7391
sa(dp7392
g91
S'Lent'
p7393
sg81
S'M '
p7394
sg87
S'2010-12-14 09:31'
p7395
sg145
S'Reading comprehension support. '
p7396
sg52
S'2011'
p7397
sg148
S'weekly(x1)'
p7398
sg86
g27
sg150
S'KM'
p7399
sa(dp7400
g91
S'Michaelmas'
p7401
sg81
S'M '
p7402
sg87
S'2010-12-14 09:31'
p7403
sg145
S'Reading comprehension support. '
p7404
sg52
S'2010'
p7405
sg148
S'weekly(x1)'
p7406
sg86
g27
sg150
S'KM'
p7407
sa(dp7408
g91
S'Summer'
p7409
sg81
S'M '
p7410
sg87
S'2010-12-14 09:31'
p7411
sg145
S'Reading comprehension support.'
p7412
sg52
S'2011'
p7413
sg148
S'weekly(x1)'
p7414
sg86
g27
sg150
S'AST'
p7415
sa(dp7416
g91
S'Summer'
p7417
sg81
S'M '
p7418
sg87
S'2010-12-14 09:31'
p7419
sg145
S'Small group spelling support focusing on spelling strategies.'
p7420
sg52
S'2011'
p7421
sg148
S'weekly(x1)'
p7422
sg86
g27
sg150
S'NB'
p7423
sa(dp7424
g91
S'Summer'
p7425
sg81
S'M '
p7426
sg87
S'2010-12-14 09:31'
p7427
sg145
S'Handwriting Support group.'
p7428
sg52
S'2011'
p7429
sg148
S'weekly(x1)'
p7430
sg86
g27
sg150
S'NB'
p7431
sa(dp7432
g91
S'Summer'
p7433
sg81
S'M '
p7434
sg87
S'2012-05-14 09:31'
p7435
sg145
S'Handwriting Support group.'
p7436
sg52
S'2012'
p7437
sg148
S'weekly(x1)'
p7438
sg86
g27
sg150
S'NB'
p7439
sa(dp7440
g91
S'Summer'
p7441
sg81
S'M '
p7442
sg87
S'2012-07-19 13:05'
p7443
sg145
S'Weekly review meetings with Felix and emails composed and sent to parents. '
p7444
sg52
S'2012'
p7445
sg148
S'weekly(x1)'
p7446
sg86
g27
sg150
S'NB'
p7447
sassssS'Sophie_Marshall'
p7448
(dp7449
g3
(dp7450
g5
(dp7451
g7
I0
sg8
(lp7452
g1548
ag1549
asg12
S'Sen provision map for Sophie Marshall'
p7453
sg14
g15
sg16
(lp7454
g27
ag2032
asg20
g21
sg22
S'Special Needs Provision Map for Sophie Marshall'
p7455
sg24
I1
sg25
(lp7456
g27
ag2803
asg29
(lp7457
g2805
ag27
asg32
S'SEN,Provision map,Sophie Marshall'
p7458
sg34
(lp7459
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp7460
g41
S'01/06/2008'
p7461
sg43
S'Marshall'
p7462
sg45
S'Action +'
p7463
sg47
S'2002-04-08'
p7464
sg49
S'Termly observation by Wandsworth Service for Children with Hearing Impairment. Salander Tay.\r<br>EP Report Nina Elliott 10.03.2011\r<br>OT Report 01.05.2011 Julia Terteryan'
p7465
sg51
S'Both'
p7466
sg52
g1564
sg54
g27
sg56
S"Sophie has profound hearing loss in her right ear and a predominately low frequency, mild to moderate hearing loss on the left. This is a permanent deadness caused by damage to the auditory nerve or cochlea. Sophie was issued with hearing aids in 2008. Sophie only wears hearing aids in her left ear. Sophie has some very mild specific learning difficulties which impact her coordination and organisation skills. Her ability to process visual information quickly is weak in comparison to her VR and NVR abilities. Sophie's difficulties with organisation of work may contribute to her work being crossed out and presented in an untidy manner. Sophie additionally has some difficulties with social interaction. VCI 126 VRI 117 WMI 99 PSI 100"
p7467
sg58
g27
sg59
S'Sophie'
p7468
sg61
S'15%'
p7469
sg62
S'6CN'
p7470
ssg64
I1
sg65
I1
sg66
(lp7471
g2814
ag2815
asg70
S'10-03-2011'
p7472
sg72
I0
sg73
(lp7473
g2817
ag2818
asssg76
(dp7474
g5
(dp7475
g38
(lp7476
(dp7477
g81
S'F '
p7478
sg83
S'CAE, AP, Mr and Mrs Marshall. Discussion re concerns. '
p7479
sg85
g27
sg86
g27
sg87
S'2011-02-07 14:51'
p7480
sa(dp7481
g81
S'F '
p7482
sg83
S'NB, CAE, AP, Mr and Mrs Marshall. Review of EP report. '
p7483
sg85
g27
sg86
g27
sg87
S'2011-05-17 14:53'
p7484
sa(dp7485
g81
S'F '
p7486
sg83
S'NB, Mr and Mrs Marshall. Review of OT report. '
p7487
sg85
g27
sg86
g27
sg87
S'2011-07-05 14:51'
p7488
sasg37
(lp7489
(dp7490
g91
S'Lent'
p7491
sg93
S'2011'
p7492
sg95
S'To wear her hearning aid at all times and ensure it is fully charged. '
p7493
sg81
S'F '
p7494
sg98
g27
sg87
S'2010-12-14 09:21'
p7495
sg100
g101
sg102
g27
sg86
g27
sa(dp7496
g91
S'Mich'
p7497
sg93
S'2011'
p7498
sg95
S'To not speak over teachers or peers when they are speaking.'
p7499
sg81
S'F '
p7500
sg98
g27
sg87
S'2011-10-10 16:22'
p7501
sg100
g101
sg102
g27
sg86
g27
sa(dp7502
g91
S'Mich'
p7503
sg93
S'2011'
p7504
sg95
S'To complete a given tasks in the time frame provided by the teacher.'
p7505
sg81
S'F '
p7506
sg98
g27
sg87
S'2011-10-10 16:22'
p7507
sg100
g101
sg102
g27
sg86
g27
sa(dp7508
g91
S'Lent'
p7509
sg93
S'2012'
p7510
sg95
S'To put her hand up and wait for a teacher to say her name before speaking.'
p7511
sg81
S'F '
p7512
sg98
g27
sg87
S'2012-02-27 16:22'
p7513
sg100
g101
sg102
g27
sg86
g27
sa(dp7514
g91
S'Lent'
p7515
sg93
S'2012'
p7516
sg95
S'To ensure she has all of the equipment she requires before leaving 5CE.'
p7517
sg81
S'F '
p7518
sg98
g27
sg87
S'2012-02-27 16:22'
p7519
sg100
g101
sg102
g27
sg86
g27
sa(dp7520
g91
S'Mich'
p7521
sg93
S'2012'
p7522
sg95
S'Improve her ability to work in pairs and small groups by taking turns to speak to and listen.'
p7523
sg81
S'F '
p7524
sg98
g27
sg87
S'2012-07-19 12:22'
p7525
sg100
g101
sg102
g27
sg86
g27
sa(dp7526
g91
S'Lent'
p7527
sg93
S'2013'
p7528
sg95
S'Improve my ability to work in pairs and small groups by taking turns to speak to and listen. (75% of the time). '
p7529
sg81
S'F '
p7530
sg98
g27
sg87
S'2012-07-19 12:22'
p7531
sg100
g101
sg102
g27
sg86
g27
sasg36
(lp7532
(dp7533
g91
S'Michaelmas'
p7534
sg81
S'F '
p7535
sg87
S'2010-12-14 09:21'
p7536
sg145
S'Termly observation by Wandsworth Service for Children with Hearing Impairment. '
p7537
sg52
S'2010'
p7538
sg148
g27
sg86
g27
sg150
g27
sa(dp7539
g91
S'Lent'
p7540
sg81
S'F '
p7541
sg87
S'2010-12-14 09:21'
p7542
sg145
S'Termly observation by Wandsworth Service for Children with Hearing Impairment.'
p7543
sg52
S'2011'
p7544
sg148
g27
sg86
g27
sg150
g27
sa(dp7545
g91
S'Summer'
p7546
sg81
S'F '
p7547
sg87
S'2010-12-14 09:21'
p7548
sg145
S'Termly observation by Wandsworth Service for Children with Hearing Impairment.'
p7549
sg52
S'2011'
p7550
sg148
g27
sg86
g27
sg150
g27
sa(dp7551
g91
S'Michaelmas'
p7552
sg81
S'F '
p7553
sg87
S'2010-12-14 09:21'
p7554
sg145
S'Termly observation by Wandsworth Service for Children with Hearing Impairment.'
p7555
sg52
S'2011'
p7556
sg148
g27
sg86
g27
sg150
g27
sa(dp7557
g91
S'Summer'
p7558
sg81
S'F '
p7559
sg87
S'2011-07-12 14:55'
p7560
sg145
S'Sophie has completed a touch typing course. '
p7561
sg52
S'2011'
p7562
sg148
g27
sg86
g27
sg150
g27
sa(dp7563
g91
S'Lent'
p7564
sg81
S'F '
p7565
sg87
S'2012-01-01 09:21'
p7566
sg145
S'Termly observation by Wandsworth Service for Children with Hearing Impairment.'
p7567
sg52
S'2012'
p7568
sg148
g27
sg86
g27
sg150
g27
sa(dp7569
g91
S'Summer'
p7570
sg81
S'F '
p7571
sg87
S'2012-05-04 09:21'
p7572
sg145
S'Termly observation by Wandsworth Service for Children with Hearing Impairment.'
p7573
sg52
S'2012'
p7574
sg148
g27
sg86
g27
sg150
g27
sassssS'Esme_Jacques'
p7575
(dp7576
g3
(dp7577
g5
(dp7578
g7
I0
sg8
(lp7579
g1548
ag1549
asg12
S'Sen provision map for Esme Jacques'
p7580
sg14
g15
sg16
(lp7581
g27
ag2032
asg20
g21
sg22
S'Special Needs Provision Map for Esme Jacques'
p7582
sg24
I0
sg25
(lp7583
g27
ag2803
asg29
(lp7584
g2805
ag27
asg32
S'SEN,Provision map,Esme Jacques'
p7585
sg34
(lp7586
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp7587
g41
g27
sg43
S'Jacques'
p7588
sg45
g27
sg47
S'2005-04-24'
p7589
sg49
g27
sg51
g27
sg52
g53
sg54
g27
sg56
g27
sg58
g27
sg59
S'Esme'
p7590
sg61
g27
sg62
S'3CW'
p7591
ssg64
I0
sg65
I0
sg66
(lp7592
g2814
ag2815
asg70
g392
sg72
I0
sg73
(lp7593
g2817
ag2818
asssg76
(dp7594
g5
(dp7595
g37
(lp7596
(dp7597
g91
S'Mich'
p7598
sg93
S'2012'
p7599
sg95
S'1. To listen carefully to a question and think about what is being asked.'
p7600
sg81
S'F '
p7601
sg98
g27
sg87
S'2012-07-19 14:05'
p7602
sg100
g101
sg102
g27
sg86
g27
sa(dp7603
g91
S'Mich'
p7604
sg93
S'2012'
p7605
sg95
S'2. To give a careful answer, showing you have thought about what has been asked.'
p7606
sg81
S'F '
p7607
sg98
g27
sg87
S'2012-07-19 14:05'
p7608
sg100
g101
sg102
g27
sg86
g27
sa(dp7609
g91
S'Lent'
p7610
sg93
S'2013'
p7611
sg95
S'1. To think carefully about what the teacher has asked and have an answer or question ready within 1 minute.                                    '
p7612
sg81
S'F '
p7613
sg98
g27
sg87
S'2012-07-19 14:05'
p7614
sg100
g101
sg102
g27
sg86
g27
sa(dp7615
g91
S'Summer'
p7616
sg93
S'2013'
p7617
sg95
S'To offer an answer by putting up hand twice in every lesson.              '
p7618
sg81
S'F '
p7619
sg98
g27
sg87
S'2012-07-19 14:05'
p7620
sg100
g101
sg102
g27
sg86
g27
sasg36
(lp7621
(dp7622
g91
S'Summer'
p7623
sg81
S'F '
p7624
sg87
S'2012-07-17 10:29'
p7625
sg145
S'In class numeracy support. '
p7626
sg52
S'2012'
p7627
sg148
S'weekly(x1)'
p7628
sg86
g27
sg150
S'KM'
p7629
sa(dp7630
g91
S'Summer'
p7631
sg81
S'F '
p7632
sg87
S'2012-07-17 10:29'
p7633
sg145
S'Spelling support group. '
p7634
sg52
S'2012'
p7635
sg148
S'weekly(x1)'
p7636
sg86
g27
sg150
S'KM'
p7637
sa(dp7638
g91
S'Summer'
p7639
sg81
S'F '
p7640
sg87
S'2012-07-17 10:29'
p7641
sg145
S'Handwriting support. '
p7642
sg52
S'2012'
p7643
sg148
S'weekly(x1)'
p7644
sg86
g27
sg150
S'KM'
p7645
sa(dp7646
g91
S'Lent'
p7647
sg81
S'F '
p7648
sg87
S'2012-07-17 10:29'
p7649
sg145
S'Spelling support group. '
p7650
sg52
S'2012'
p7651
sg148
S'weekly(x1)'
p7652
sg86
g27
sg150
S'AST'
p7653
sa(dp7654
g91
S'Lent'
p7655
sg81
S'F '
p7656
sg87
S'2012-07-17 10:29'
p7657
sg145
S'Handwriting support. '
p7658
sg52
S'2012'
p7659
sg148
S'weekly(x1)'
p7660
sg86
g27
sg150
S'AST'
p7661
sa(dp7662
g91
S'Michaelmas'
p7663
sg81
S'F '
p7664
sg87
S'2013-07-08 10:28'
p7665
sg145
S'Spelling support group'
p7666
sg52
S'2012'
p7667
sg148
S'weekly(x1)'
p7668
sg86
g27
sg150
S'KM'
p7669
sa(dp7670
g91
S'Lent'
p7671
sg81
S'F '
p7672
sg87
S'2013-07-08 10:28'
p7673
sg145
S'Spelling support group'
p7674
sg52
S'2013'
p7675
sg148
S'weekly(x1)'
p7676
sg86
g27
sg150
S'KM'
p7677
sa(dp7678
g91
S'Summer'
p7679
sg81
S'F '
p7680
sg87
S'2013-07-08 10:28'
p7681
sg145
S'Spelling support group'
p7682
sg52
S'2013'
p7683
sg148
S'weekly(x1)'
p7684
sg86
g27
sg150
S'KM'
p7685
sa(dp7686
g91
S'Summer'
p7687
sg81
S'F '
p7688
sg87
S'2013-07-08 10:28'
p7689
sg145
S'Reading comprehension group'
p7690
sg52
S'2013'
p7691
sg148
S'weekly(x1)'
p7692
sg86
g27
sg150
S'NB'
p7693
sa(dp7694
g91
S'Lent'
p7695
sg81
S'F '
p7696
sg87
S'2013-07-08 10:28'
p7697
sg145
S'1:1 Word Wasp'
p7698
sg52
S'2013'
p7699
sg148
S'weekly(x1)'
p7700
sg86
g27
sg150
S'KM'
p7701
sa(dp7702
g91
S'Summer'
p7703
sg81
S'F '
p7704
sg87
S'2013-07-08 10:28'
p7705
sg145
S'1:1 Word Wasp'
p7706
sg52
S'2013'
p7707
sg148
S'weekly(x1)'
p7708
sg86
g27
sg150
S'KM'
p7709
sa(dp7710
g91
S'Lent'
p7711
sg81
S'F '
p7712
sg87
S'2013-07-08 10:28'
p7713
sg145
S'1:1 Literacy support with SM'
p7714
sg52
S'2013'
p7715
sg148
S'weekly(x1)'
p7716
sg86
g27
sg150
g27
sa(dp7717
g91
S'Summer'
p7718
sg81
S'F '
p7719
sg87
S'2013-07-08 10:28'
p7720
sg145
S'1:1 Literacy support with GSK'
p7721
sg52
S'2013'
p7722
sg148
S'weekly(x1)'
p7723
sg86
g27
sg150
g27
sassssS'Tilda_Gray'
p7724
(dp7725
g3
(dp7726
g5
(dp7727
g7
I0
sg8
(lp7728
g1548
ag1549
asg12
S'Sen provision map for Tilda Gray'
p7729
sg14
g15
sg16
(lp7730
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Tilda Gray'
p7731
sg24
I0
sg25
(lp7732
g27
aS"Regularly check in with Tilda whilst on the carpet and check understanding by asking relevant and frequent questions. Regular discussion and targeted questioning is essential to reinforce the lesson objective and to check that she has an accurate understanding of the task. Investigate seating arrangements especially when seated on the floor. Provide her with extra time to think and process where possible. Always ensure you have her attention after using attention getters 'clapping patterns' etc. Encourage Tilda to share her answers even when others have shared theirs. Scaffold organisation and provide support when packing up etc. Reading should be encouraged as often as possible with careful notice paid to pronunciation of words and letters. If Tilda happens to say a word incorrectly, this should be discussed.  Structured and shared reading will provide Tilda with exposure to words in context. In conjunction with specific spelling lessons (letters and sounds) Tilda will develop greater accuracy with her letters and sounds. Discuss good attempts at unknown words and explain why. If instructions are given orally, try to supplement this with written or other visual cues. Keep the area of difficulty in mind. Simplifying verbal directions, slowing the rate of speech, and minimalising distractions."
p7733
asg29
(lp7734
g562
ag27
asg32
S'SEN,Provision map,Tilda Gray'
p7735
sg34
(lp7736
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp7737
g41
S'25-11-2010'
p7738
sg43
S'Gray'
p7739
sg45
S'Action'
p7740
sg47
S'2005-04-15'
p7741
sg49
S'DEST completed on 25/11/2010 by NB\r<br>EP Annie Mitchell 14.3.2012'
p7742
sg51
g27
sg52
g53
sg54
S'13-03-2012'
p7743
sg56
S'Tilda has some visual processing difficulties and this is most evident when she is trying to read and spell. She makes careless visual errors in both literacy and numeracy. Tilda may require support to understand instructions and her concentration can often wander. When she reads she often skims through and as such looses the meaning of the text. She has a small sight vocabulary and her word attack skills are poorly developed. Her reading is not fluent which will affect her comprehension.'
p7744
sg58
g27
sg59
S'Tilda'
p7745
sg61
g27
sg62
S'3CE'
p7746
ssg64
I1
sg65
I0
sg66
(lp7747
S'Tilda has some visual processing difficulties and this is most evident when she is trying to read and spell. She makes careless visual errors in both literacy and numeracy. Tilda may require support to understand instructions and her concentration can often wander. When she reads she often skims through and as such looses the meaning of the text. She has a small sight vocabulary and her word attack skills are poorly developed. Her reading is not fluent which will affect her comprehension.'
p7748
aS'Tilda needs activites which will expand her sight vocabulary. List spellings she commonly struggles with and learn using Look, Cover, Write, Check. Ensure her reading book is accessible to her. She needs instructions repeated and broken down so she can remember them due to poor attention. Ensure that she isn\'t expected to copy a lot from the board as she will take a long time and find it hard to keep focused. She would benefit from prepared worksheets but ensure they\'re not visually complicated or overwhelming.  She needs work given to her in small chunks so she can understand what she has to do but she also needs to know the end point so she knows what she working towards. She may benefit from a small prompt sheet on her desk when her reading skills become more proficient. She needs to know that task is attainable and thus will feel satisfied when she has managed it. Talk-back\' technique may be helpful " Tilda repeats back the instruction AND says: what/how they are going to do it, what they need, where they are going to work.'
p7749
asg70
S'14-03-2012'
p7750
sg72
I0
sg73
(lp7751
g581
ag582
asssg76
(dp7752
g5
(dp7753
g38
(lp7754
(dp7755
g81
S'F '
p7756
sg83
S'Meeting held between NB, JS, HSm and Mr and Mrs Gray. Discussion of observation by NB and suggested strategies for Christmas break and Lent term. '
p7757
sg85
S'13-12-2010'
p7758
sg86
g27
sg87
S'2010-12-13 10:40'
p7759
sa(dp7760
g81
S'F '
p7761
sg83
S'Meeting held between NB, JS, HSM and Mr and Mrs Gray. Discussion of progress and follow up. '
p7762
sg85
S'13-05-2011'
p7763
sg86
g27
sg87
S'2011-05-13 10:40'
p7764
sa(dp7765
g81
S'F '
p7766
sg83
S'NB and Mrs Gray. Discussion regarding re assessment. NB to complete re assessment in September. '
p7767
sg85
S'21-06-2011'
p7768
sg86
g27
sg87
S'2011-06-21 10:40'
p7769
sa(dp7770
g81
S'F '
p7771
sg83
S'Meeting held between JS and Mrs Gray. Discussion of progress and follow up - concentration and focus highlighted. '
p7772
sg85
S'29-09-2011'
p7773
sg86
g27
sg87
S'2011-09-29 10:40'
p7774
sa(dp7775
g81
S'F '
p7776
sg83
S'Telephone conversation between NB and Mrs Gray. Tilda will be having an assessment. NB has sent contact details to Mrs Gray. '
p7777
sg85
g27
sg86
g27
sg87
S'2012-01-25 16:36'
p7778
sa(dp7779
g81
S'F '
p7780
sg83
S'Meeting between NB, HSM, LH and Mr and Mrs Gray to discuss the findings of the EP report completed by Annie Mitchell. Additional support suggestions will be implemented in class. 1:1 support lessons were agreed upon for the remainder of this term. '
p7781
sg85
S'17-05-2012'
p7782
sg86
g27
sg87
S'2012-05-17 10:40'
p7783
sa(dp7784
g81
S'F '
p7785
sg83
S'NB, JW, RO and Mr and Mrs Gray. Review of progress. All happy with how she is progressing. '
p7786
sg85
g27
sg86
g27
sg87
S'2013-05-16 15:21'
p7787
sasg37
(lp7788
(dp7789
g91
S'Lent'
p7790
sg93
S'2011'
p7791
sg95
S'To sit still and focus for a 5 minute period during whole class carpet time.'
p7792
sg81
S'F '
p7793
sg98
g27
sg87
S'2010-12-13 10:40'
p7794
sg100
g101
sg102
g27
sg86
g27
sa(dp7795
g91
S'Summer'
p7796
sg93
S'2011'
p7797
sg95
S'To sit still and focus for a 10 minute period during whole class carpet time.'
p7798
sg81
S'F '
p7799
sg98
g27
sg87
S'2010-12-13 10:40'
p7800
sg100
g101
sg102
g27
sg86
g27
sa(dp7801
g91
S'Mich'
p7802
sg93
S'2011'
p7803
sg95
S'To sit still and concentrate during whole class sessions and repeat any instructions given to ensure understanding. ONGOING'
p7804
sg81
S'F '
p7805
sg98
g27
sg87
S'2011-09-20 16:41'
p7806
sg100
g101
sg102
g27
sg86
S'LH'
p7807
sa(dp7808
g91
S'Lent'
p7809
sg93
S'2012'
p7810
sg95
S'Using her timer, concentrate and attempt tasks independently for 5 minute sections after instruction has been given and understanding has been established.'
p7811
sg81
S'F '
p7812
sg98
g27
sg87
S'2012-02-08 16:41'
p7813
sg100
g101
sg102
g27
sg86
g27
sa(dp7814
g91
S'Mich'
p7815
sg93
S'2012'
p7816
sg95
S'To practise, consolidate and continually use her key spelling/reading words, for example said, what, make, were etc.'
p7817
sg81
S'F '
p7818
sg98
g27
sg87
S'2012-07-19 13:49'
p7819
sg100
g101
sg102
g27
sg86
g27
sa(dp7820
g91
S'Lent'
p7821
sg93
S'2013'
p7822
sg95
S'To use and refer to her try it out book for common spelling words.'
p7823
sg81
S'F '
p7824
sg98
g27
sg87
S'2012-07-19 13:49'
p7825
sg100
g101
sg102
g27
sg86
g27
sa(dp7826
g91
S'Summer'
p7827
sg93
S'2013'
p7828
sg95
S'To use the words in her try it out book to edit a completed piece or written work.  '
p7829
sg81
S'F '
p7830
sg98
g27
sg87
S'2012-07-19 13:49'
p7831
sg100
g101
sg102
g27
sg86
g27
sa(dp7832
g91
S'Mich'
p7833
sg93
S'2013'
p7834
sg95
S'1. To proritise organising her bag in the morning and her homework at the end of the day.                                                            '
p7835
sg81
S'F '
p7836
sg98
g27
sg87
S'2013-07-16 10:00'
p7837
sg100
g139
sg102
g27
sg86
g27
sa(dp7838
g91
S'Mich'
p7839
sg93
S'2013'
p7840
sg95
S'2. To try not to be the last person in the class ready 50% of the time.                         '
p7841
sg81
S'F '
p7842
sg98
g27
sg87
S'2013-07-16 10:00'
p7843
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp7844
(dp7845
g91
S'Michaelmas'
p7846
sg81
S'F '
p7847
sg87
S'2011-02-17 11:31'
p7848
sg145
S'Small group numeracy support sessions. '
p7849
sg52
S'2010'
p7850
sg148
S'Monthly(x2)'
p7851
sg86
g27
sg150
S'KM'
p7852
sa(dp7853
g91
S'Lent'
p7854
sg81
S'F '
p7855
sg87
S'2011-02-17 11:31'
p7856
sg145
S'Small group numeracy support sessions. '
p7857
sg52
S'2011'
p7858
sg148
S'Monthly(x2)'
p7859
sg86
g27
sg150
S'KM'
p7860
sa(dp7861
g91
S'Michaelmas'
p7862
sg81
S'F '
p7863
sg87
S'2011-09-17 09:27'
p7864
sg145
S'Spelling support group. '
p7865
sg52
S'2011'
p7866
sg148
S'weekly(x1)'
p7867
sg86
g27
sg150
S'AST'
p7868
sa(dp7869
g91
S'Michaelmas'
p7870
sg81
S'F '
p7871
sg87
S'2011-09-17 09:27'
p7872
sg145
S'Handwriting support. '
p7873
sg52
S'2011'
p7874
sg148
S'weekly(x1)'
p7875
sg86
g27
sg150
S'AST'
p7876
sa(dp7877
g91
S'Lent'
p7878
sg81
S'F '
p7879
sg87
S'2012-01-11 09:27'
p7880
sg145
S'Spelling support group. '
p7881
sg52
S'2012'
p7882
sg148
S'weekly(x1)'
p7883
sg86
g27
sg150
S'AST'
p7884
sa(dp7885
g91
S'Lent'
p7886
sg81
S'F '
p7887
sg87
S'2012-01-11 09:27'
p7888
sg145
S'Handwriting support.'
p7889
sg52
S'2012'
p7890
sg148
S'weekly(x1)'
p7891
sg86
g27
sg150
S'AST'
p7892
sa(dp7893
g91
S'Summer'
p7894
sg81
S'F '
p7895
sg87
S'2012-05-04 09:27'
p7896
sg145
S'Spelling support group. '
p7897
sg52
S'2012'
p7898
sg148
S'weekly(x1)'
p7899
sg86
g27
sg150
S'KM'
p7900
sa(dp7901
g91
S'Summer'
p7902
sg81
S'F '
p7903
sg87
S'2012-05-17 10:24'
p7904
sg145
S'1:1 Support lessions with Eleanor Barker.'
p7905
sg52
S'2012'
p7906
sg148
S'weekly(x1)'
p7907
sg86
g27
sg150
g27
sa(dp7908
g91
S'Summer'
p7909
sg81
S'F '
p7910
sg87
S'2012-05-17 10:24'
p7911
sg145
S'In class numeracy support.'
p7912
sg52
S'2012'
p7913
sg148
S'weekly(x1)'
p7914
sg86
g27
sg150
S'KM'
p7915
sa(dp7916
g91
S'Summer'
p7917
sg81
S'F '
p7918
sg87
S'2012-05-17 10:24'
p7919
sg145
S'Handwriting support.'
p7920
sg52
S'2012'
p7921
sg148
S'weekly(x1)'
p7922
sg86
g27
sg150
S'KM'
p7923
sa(dp7924
g91
S'Michaelmas'
p7925
sg81
S'F '
p7926
sg87
S'2013-07-08 15:58'
p7927
sg145
S'Guided reading group'
p7928
sg52
S'2012'
p7929
sg148
S'weekly(x1)'
p7930
sg86
g27
sg150
S'NB'
p7931
sa(dp7932
g91
S'Lent'
p7933
sg81
S'F '
p7934
sg87
S'2013-07-08 15:58'
p7935
sg145
S'Guided reading group'
p7936
sg52
S'2013'
p7937
sg148
S'weekly(x1)'
p7938
sg86
g27
sg150
S'NB'
p7939
sa(dp7940
g91
S'Summer'
p7941
sg81
S'F '
p7942
sg87
S'2013-07-08 15:58'
p7943
sg145
S'Guided reading group'
p7944
sg52
S'2013'
p7945
sg148
S'weekly(x1)'
p7946
sg86
g27
sg150
S'NB'
p7947
sa(dp7948
g91
S'Michaelmas'
p7949
sg81
S'F '
p7950
sg87
S'2013-07-08 16:11'
p7951
sg145
S'1:1 Literacy support with EB'
p7952
sg52
S'2012'
p7953
sg148
S'weekly(x1)'
p7954
sg86
g27
sg150
g27
sa(dp7955
g91
S'Lent'
p7956
sg81
S'F '
p7957
sg87
S'2013-07-08 16:11'
p7958
sg145
S'1:1 Literacy support with EB'
p7959
sg52
S'2013'
p7960
sg148
S'weekly(x1)'
p7961
sg86
g27
sg150
g27
sa(dp7962
g91
S'Summer'
p7963
sg81
S'F '
p7964
sg87
S'2013-07-08 16:11'
p7965
sg145
S'1:1 Literacy support with EB'
p7966
sg52
S'2013'
p7967
sg148
S'weekly(x1)'
p7968
sg86
g27
sg150
g27
sassssS'Lara_Maher'
p7969
(dp7970
g3
(dp7971
g5
(dp7972
g7
I0
sg8
(lp7973
g1456
ag27
asg12
S'Sen provision map for Lara Maher'
p7974
sg22
S'Special Needs Provision Map for Lara Maher'
p7975
sg20
g21
sg14
g15
sg24
I0
sg25
(lp7976
S'She can be underconfident and at times has a low opinion of herself as a learner. Lara has some difficulties with verbal working memory.'
p7977
aS'Always look for opportunities to praise Lara and encourage her to communicate with you if she requires support.'
p7978
asg32
S'SEN,Provision map,Lara Maher'
p7979
sg34
(lp7980
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp7981
g41
S'05/05/2010'
p7982
sg43
S'Maher'
p7983
sg45
S'Action'
p7984
sg47
S'2002-10-07'
p7985
sg49
S'DST-J completed by NB on 5/5/10.\r<br>EP Report Nina Elliott 18.09.12'
p7986
sg51
g27
sg52
g384
sg54
S'18-09-2012'
p7987
sg56
S'Lara has dyslexia. She has difficulties with her spelling, reading and writing skills. Her comprehension and reasoning abilities are good. Her punctuation can be erratic and she has difficulties organising and arranging her thoughts. She can be underconfident and at times has a low opinion of herself as a learner. Lara has some difficulties with verbal working memory. Her Numerical operation and mathematical reasoning are in the average range and are an area of strength.  VR 106 NVR 102 WM 94 PSI 97 GAI 105'
p7988
sg58
g27
sg59
S'Lara'
p7989
sg61
S'25%'
p7990
sg62
S'5CS'
p7991
ssg64
I1
sg65
I0
sg70
S'18-09-2012'
p7992
sg72
I0
sg66
(lp7993
g3804
ag3805
asssg76
(dp7994
g5
(dp7995
g38
(lp7996
(dp7997
g81
S'F '
p7998
sg83
S'NB, AS, SW adn Mrs Maher. Review of DST-J assessment. '
p7999
sg85
g27
sg86
g27
sg87
S'2010-07-05 13:54'
p8000
sa(dp8001
g81
S'F '
p8002
sg83
S'Meeting between NB and Mrs Maher. Both reported that progress is being seen and interventions were appropriate. '
p8003
sg85
S'19-01-2011'
p8004
sg86
g27
sg87
S'2010-12-13 13:54'
p8005
sa(dp8006
g81
S'F '
p8007
sg83
S'FS, NB and Mrs Maher. Review of progress and planning for future support. '
p8008
sg85
g27
sg86
g27
sg87
S'2011-06-24 13:54'
p8009
sa(dp8010
g81
S'F '
p8011
sg83
S'NB and Mrs Maher. Review of progress. No EP to be completed at this stage. '
p8012
sg85
S'07-12-2011'
p8013
sg86
g27
sg87
S'2011-12-07 13:54'
p8014
sa(dp8015
g81
S'F '
p8016
sg83
S'NB and Mrs Maher. Telephone conversation to discuss Lara and her progress. NB suggested to have an EP report completed and Mrs Maher agreed. '
p8017
sg85
S'17-07-2012'
p8018
sg86
g27
sg87
S'2012-07-17 13:54'
p8019
sa(dp8020
g81
S'F '
p8021
sg83
S'NB and Mrs Maher reviewed EP report and discussed support options. '
p8022
sg85
g27
sg86
g27
sg87
S'2012-09-28 13:54'
p8023
sa(dp8024
g81
S'F '
p8025
sg83
S'NB and Mrs Maher. Review of progress and discussion of changing support lesson. '
p8026
sg85
g27
sg86
g27
sg87
S'2013-02-07 15:12'
p8027
sasg37
(lp8028
(dp8029
g91
S'Summer'
p8030
sg93
S'2011'
p8031
sg95
S'To self edit written work especially remembering capital letters and full stops.ters and full stops. ONGOING'
p8032
sg81
S'F '
p8033
sg98
g27
sg87
S'2010-09-15 13:54'
p8034
sg100
g101
sg102
g27
sg86
g27
sa(dp8035
g91
S'Mich'
p8036
sg93
S'2011'
p8037
sg95
S'To self edit written work especially remembering capital letters and full stops.'
p8038
sg81
S'F '
p8039
sg98
g27
sg87
S'2012-01-13 13:54'
p8040
sg100
g101
sg102
g27
sg86
g27
sa(dp8041
g91
S'Lent'
p8042
sg93
S'2012'
p8043
sg95
S'To use word list and have a go book for spellings.'
p8044
sg81
S'F '
p8045
sg98
g27
sg87
S'2012-01-25 16:50'
p8046
sg100
g139
sg102
S'Provide word list and have a go book.'
p8047
sg86
S'AP'
p8048
sa(dp8049
g91
S'Mich'
p8050
sg93
S'2012'
p8051
sg95
S'To increase the accuracy of common words using a have a go book and words lists. '
p8052
sg81
S'F '
p8053
sg98
g27
sg87
S'2012-07-19 12:09'
p8054
sg100
g139
sg102
g27
sg86
g27
sa(dp8055
g91
S'Mich'
p8056
sg93
S'2012'
p8057
sg95
S'To increase the accuracy of common words using a have a go book and words lists.'
p8058
sg81
S'F '
p8059
sg98
g27
sg87
S'2012-07-19 12:09'
p8060
sg100
g101
sg102
g27
sg86
g27
sa(dp8061
g91
S'Lent'
p8062
sg93
S'2013'
p8063
sg95
S'To increase the amount of powerful and descriptive words used in my written answers. '
p8064
sg81
S'F '
p8065
sg98
g27
sg87
S'2013-07-16 10:39'
p8066
sg100
g139
sg102
g27
sg86
g27
sa(dp8067
g91
S'Summer'
p8068
sg93
S'2013'
p8069
sg95
S'To increase the amount of powerful and descriptive words used in my written answers. '
p8070
sg81
S'F '
p8071
sg98
g27
sg87
S'2013-07-16 10:39'
p8072
sg100
g101
sg102
g27
sg86
g27
sa(dp8073
g91
S'Summer'
p8074
sg93
S'2013'
p8075
sg95
S'2. To challenge myself with my learning and be willing to take a risk with harder work options. '
p8076
sg81
S'F '
p8077
sg98
g27
sg87
S'2013-07-16 10:39'
p8078
sg100
g139
sg102
g27
sg86
g27
sa(dp8079
g91
S'Mich'
p8080
sg93
S'2013'
p8081
sg95
S'1. To challenge myself with my learning and be willing to take a risk with harder work options.                                                 '
p8082
sg81
S'F '
p8083
sg98
g27
sg87
S'2013-07-16 10:40'
p8084
sg100
g139
sg102
g27
sg86
g27
sa(dp8085
g91
S'Mich'
p8086
sg93
S'2013'
p8087
sg95
S'2. To put my hand up at least 2 times in every lesson to either ask a question or share my ideas. '
p8088
sg81
S'F '
p8089
sg98
g27
sg87
S'2013-07-16 10:40'
p8090
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp8091
(dp8092
g91
S'Michaelmas'
p8093
sg81
S'F '
p8094
sg87
S'2010-12-13 13:50'
p8095
sg145
S'Reading comprehension support group. '
p8096
sg52
S'2010'
p8097
sg148
S'weekly(x1)'
p8098
sg86
g27
sg150
S'KM'
p8099
sa(dp8100
g91
S'Michaelmas'
p8101
sg81
S'F '
p8102
sg87
S'2010-12-13 13:50'
p8103
sg145
S'Small group spelling support focusing on spelling strategies.'
p8104
sg52
S'2010'
p8105
sg148
S'weekly(x1)'
p8106
sg86
g27
sg150
S'NB'
p8107
sa(dp8108
g91
S'Lent'
p8109
sg81
S'F '
p8110
sg87
S'2010-12-13 13:50'
p8111
sg145
S'Reading comprehension support group.'
p8112
sg52
S'2011'
p8113
sg148
S'weekly(x1)'
p8114
sg86
g27
sg150
S'KM'
p8115
sa(dp8116
g91
S'Lent'
p8117
sg81
S'F '
p8118
sg87
S'2010-12-13 13:50'
p8119
sg145
S'Small group spelling support focusing on spelling strategies.'
p8120
sg52
S'2011'
p8121
sg148
S'weekly(x1)'
p8122
sg86
g27
sg150
S'NB'
p8123
sa(dp8124
g91
S'Summer'
p8125
sg81
S'F '
p8126
sg87
S'2010-12-13 13:50'
p8127
sg145
S'Reading comprehension support group.'
p8128
sg52
S'2011'
p8129
sg148
S'weekly(x1)'
p8130
sg86
g27
sg150
S'AST'
p8131
sa(dp8132
g91
S'Summer'
p8133
sg81
S'F '
p8134
sg87
S'2010-12-13 13:50'
p8135
sg145
S'Small group spelling support focusing on spelling strategies.'
p8136
sg52
S'2011'
p8137
sg148
S'weekly(x1)'
p8138
sg86
g27
sg150
S'NB'
p8139
sa(dp8140
g91
S'Michaelmas'
p8141
sg81
S'F '
p8142
sg87
S'2010-12-13 13:50'
p8143
sg145
S'Small group spelling support focusing on spelling strategies.'
p8144
sg52
S'2011'
p8145
sg148
S'weekly(x1)'
p8146
sg86
g27
sg150
S'NB'
p8147
sa(dp8148
g91
S'Michaelmas'
p8149
sg81
S'F '
p8150
sg87
S'2011-09-15 13:50'
p8151
sg145
S'Reading comprehension support group.'
p8152
sg52
S'2011'
p8153
sg148
S'weekly(x1)'
p8154
sg86
g27
sg150
S'NB'
p8155
sa(dp8156
g91
S'Lent'
p8157
sg81
S'F '
p8158
sg87
S'2012-03-13 13:50'
p8159
sg145
S'Small group spelling support focusing on spelling strategies.'
p8160
sg52
S'2012'
p8161
sg148
S'weekly(x1)'
p8162
sg86
g27
sg150
S'NB'
p8163
sa(dp8164
g91
S'Lent'
p8165
sg81
S'F '
p8166
sg87
S'2012-03-13 13:50'
p8167
sg145
S'Reading comprehension support. '
p8168
sg52
S'2012'
p8169
sg148
S'weekly(x1)'
p8170
sg86
g27
sg150
S'NB'
p8171
sa(dp8172
g91
S'Summer'
p8173
sg81
S'F '
p8174
sg87
S'2012-03-13 13:50'
p8175
sg145
S'Reading comprehension support. '
p8176
sg52
S'2012'
p8177
sg148
S'weekly(x1)'
p8178
sg86
g27
sg150
S'NB'
p8179
sa(dp8180
g91
S'Summer'
p8181
sg81
S'F '
p8182
sg87
S'2012-03-13 13:50'
p8183
sg145
S'Spelling support group.'
p8184
sg52
S'2012'
p8185
sg148
S'weekly(x1)'
p8186
sg86
g27
sg150
S'NB'
p8187
sa(dp8188
g91
S'Michaelmas'
p8189
sg81
S'F '
p8190
sg87
S'2012-10-09 10:07'
p8191
sg145
S'1:1 Literacy support lessons with Sarah McKinlay.'
p8192
sg52
S'2012'
p8193
sg148
S'weekly(x1)'
p8194
sg86
g27
sg150
g27
sassssS'Angus_Strachan'
p8195
(dp8196
g3
(dp8197
g5
(dp8198
g7
I0
sg8
(lp8199
g1456
ag27
asg12
S'Sen provision map for Angus Strachan'
p8200
sg14
g15
sg16
(lp8201
g3781
ag3782
asg20
g21
sg22
S'Special Needs Provision Map for Angus Strachan'
p8202
sg24
I0
sg25
(lp8203
S"Angus's concentration and memory are a weakness."
p8204
aS"Before information is shared with Angus it is essential that you have his attention. Repeat his name until he focuses on you before providing instructions. Assign him a specific place on the floor and room where distractions can be limited. Ensure that very regular checking in occurs as he is most likely to engage in discussions and activities that are not associated with the lesson objective. Chunk information for Angus having him complete small steps of the tasks before having to check in with you. Use a timer when possible. Alternatively you can provide Angus with a 'to do list' which has 3-4 key points/words that he can check in with to ensure he remains on task. Praise Angus when he is on task and settled."
p8205
asg29
(lp8206
g3788
ag3789
asg32
S'SEN,Provision map,Angus Strachan'
p8207
sg34
(lp8208
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp8209
g41
S'20/05/2010'
p8210
sg43
S'Strachan'
p8211
sg45
S'Action +'
p8212
sg47
S'2003-05-30'
p8213
sg49
S'DST-J completed by NB on 20/5/10.\r<br>Annie Mitchell 4/2/2011'
p8214
sg51
g27
sg52
g384
sg54
g27
sg56
S"Angus has mild to moderate Dyslexia. Angus has good verbal reasoning skills and an well developed vocabulary. Angus additionally has very good non verbal reasoning abilities. His ability to record written information at speed is a weakness as he difficulties with visual processing, visual memory and fine motor skills. Angus's concentration and memory are a weakness. Angus has weak phonic skills , he does not segment words and makes visual errors when reading. Angus's reading comprehension will be weak as he is investing effort and concentration into decoding and reading words. He has worked extremely hard on spelling, reading comprehension and handwriting in year 4 and is beginning to read a great deal more. Angus will begin using a laptop in year 5. VCI 119 PRI 121 WMI 99 PSI 97 FSIQ 114"
p8215
sg58
g27
sg59
S'Angus'
p8216
sg61
g27
sg62
S'5CN'
p8217
ssg64
I1
sg65
I0
sg70
S'04-02-2011'
p8218
sg72
I0
sg66
(lp8219
S"Angus has mild to moderate Dyslexia. Angus has good verbal reasoning skills and an well developed vocabulary. Angus additionally has very good non verbal reasoning abilities. His ability to record written information at speed is a weakness as he difficulties with visual processing, visual memory and fine motor skills. Angus's concentration and memory are a weakness. Angus has weak phonic skills , he does not segment words and makes visual errors when reading. Angus's reading comprehension will be weak as he is investing effort and concentration into decoding and reading words. He has worked extremely hard on spelling, reading comprehension and handwriting in year 4 and is beginning to read a great deal more. Angus will begin using a laptop in year 5."
p8220
aS'Angus will require support with the organisation of his written work. Provide templates where possible.  Encourage Angus to read problems through and explain verbally his thoughts before writing. Provide a list of high frequency words which Angus can use to support his writing. introduce a have a go book where Angus can record attempts at spelling. This will then be a record that he can refer back to in the future. Encourage Angus to look closely and try new words. Praise close attempts at spelling, and tell him why it was a good attempt. Continue with the additional phonic based spelling activities. Ensure all instructions are kept precise and check understanding and retention by asking for repetition and clarification during task. Break tasks down into their smallest components and encourage completion of each step at a time. Check understanding of vocabulary when introducing new concepts or topics. Use visual techniques to support memory for spelling such as word shape activites. During written tasks be aware that Angus may become very frustrated due to his difficulties expressing himself in writing, if this is observed allow him to discuss his knowledge and be sure to use this in assessment.'
p8221
asssg76
(dp8222
g5
(dp8223
g38
(lp8224
(dp8225
g81
S'M '
p8226
sg83
S'NB, CMC, FS, Mr and Mrs Strachan. Meeting to discuss Angus progress and continuing concerns. EP assessment suggested.'
p8227
sg85
S'09-12-2010'
p8228
sg86
g27
sg87
S'2010-12-09 14:03'
p8229
sa(dp8230
g81
S'M '
p8231
sg83
S'Review meeting NB and Mr S discussing laptop use. '
p8232
sg85
S'04-05-2012'
p8233
sg86
g27
sg87
S'2012-05-04 14:03'
p8234
sasg37
(lp8235
(dp8236
g91
S'Lent'
p8237
sg93
S'2011'
p8238
sg95
S'To improve his ability to focus and concentration in lessons.'
p8239
sg81
S'M '
p8240
sg98
g27
sg87
S'2010-12-13 14:03'
p8241
sg100
g101
sg102
g27
sg86
g27
sa(dp8242
g91
S'Mich'
p8243
sg93
S'2011'
p8244
sg95
S'To speed up and improve the quality handwriting.'
p8245
sg81
S'M '
p8246
sg98
g27
sg87
S'2012-01-13 12:21'
p8247
sg100
g101
sg102
S'Regular praise, reward and encouragement for good presentation of work.'
p8248
sg86
S'AP'
p8249
sa(dp8250
g91
S'Lent'
p8251
sg93
S'2012'
p8252
sg95
S'Correctly form and join letters in cursive script in all subjects.'
p8253
sg81
S'M '
p8254
sg98
g27
sg87
S'2012-01-25 16:52'
p8255
sg100
g101
sg102
S'Regular encouragement and praise.'
p8256
sg86
S'AP'
p8257
sa(dp8258
g91
S'Mich'
p8259
sg93
S'2012'
p8260
sg95
S'To include more than one event in his stories.'
p8261
sg81
S'M '
p8262
sg98
g27
sg87
S'2012-07-19 12:48'
p8263
sg100
g101
sg102
g27
sg86
g27
sa(dp8264
g91
S'Lent'
p8265
sg93
S'2013'
p8266
sg95
S'To be ready to start the lesson with 3 minutes of entering the room including having the LO and date written (or typed). '
p8267
sg81
S'M '
p8268
sg98
g27
sg87
S'2012-07-19 12:48'
p8269
sg100
g101
sg102
g27
sg86
g27
sa(dp8270
g91
S'Summer'
p8271
sg93
S'2013'
p8272
sg95
S' To make sure I have all of the equipment I need for the lesson before I leave my classroom.     '
p8273
sg81
S'M '
p8274
sg98
g27
sg87
S'2012-07-19 12:48'
p8275
sg100
g101
sg102
g27
sg86
g27
sa(dp8276
g91
S'Mich'
p8277
sg93
S'2013'
p8278
sg95
S'To consistently focus and concentrate on my learning. '
p8279
sg81
S'M '
p8280
sg98
g27
sg87
S'2013-07-16 10:42'
p8281
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp8282
(dp8283
g91
S'Michaelmas'
p8284
sg81
S'M '
p8285
sg87
S'2010-12-13 13:58'
p8286
sg145
S'Small group spelling support focusing on spelling strategies.'
p8287
sg52
S'2010'
p8288
sg148
S'weekly(x1)'
p8289
sg86
g27
sg150
S'NB'
p8290
sa(dp8291
g91
S'Lent'
p8292
sg81
S'M '
p8293
sg87
S'2010-12-13 13:58'
p8294
sg145
S'Small group spelling support focusing on spelling strategies.'
p8295
sg52
S'2011'
p8296
sg148
S'weekly(x1)'
p8297
sg86
g27
sg150
S'NB'
p8298
sa(dp8299
g91
S'Lent'
p8300
sg81
S'M '
p8301
sg87
S'2010-12-13 14:03'
p8302
sg145
S'Commenced 1:1 support with George Scott Kerr. '
p8303
sg52
S'2011'
p8304
sg148
S'weekly(x1)'
p8305
sg86
g27
sg150
g27
sa(dp8306
g91
S'Michaelmas'
p8307
sg81
S'M '
p8308
sg87
S'2010-12-13 14:03'
p8309
sg145
S'Commenced 1:1 support with George Scott Kerr.'
p8310
sg52
S'2011'
p8311
sg148
S'weekly(x1)'
p8312
sg86
g27
sg150
g27
sa(dp8313
g91
S'Michaelmas'
p8314
sg81
S'M '
p8315
sg87
S'2010-12-13 14:03'
p8316
sg145
S'Spelling support group. '
p8317
sg52
S'2011'
p8318
sg148
S'weekly(x1)'
p8319
sg86
g27
sg150
S'NB'
p8320
sa(dp8321
g91
S'Lent'
p8322
sg81
S'M '
p8323
sg87
S'2010-12-13 14:03'
p8324
sg145
S'Spelling support group. '
p8325
sg52
S'2011'
p8326
sg148
S'weekly(x1)'
p8327
sg86
g27
sg150
S'NB'
p8328
sa(dp8329
g91
S'Lent'
p8330
sg81
S'M '
p8331
sg87
S'2010-12-13 14:03'
p8332
sg145
S'1:1 support with George Scott Kerr.'
p8333
sg52
S'2012'
p8334
sg148
S'weekly(x1)'
p8335
sg86
g27
sg150
g27
sa(dp8336
g91
S'Lent'
p8337
sg81
S'M '
p8338
sg87
S'2012-02-06 14:03'
p8339
sg145
S'Spelling support group.'
p8340
sg52
S'2012'
p8341
sg148
S'weekly(x1)'
p8342
sg86
g27
sg150
S'NB'
p8343
sa(dp8344
g91
S'Lent'
p8345
sg81
S'M '
p8346
sg87
S'2012-02-06 14:03'
p8347
sg145
S'Handwriting support group. '
p8348
sg52
S'2012'
p8349
sg148
S'weekly(x1)'
p8350
sg86
g27
sg150
S'NB'
p8351
sa(dp8352
g91
S'Summer'
p8353
sg81
S'M '
p8354
sg87
S'2012-05-06 14:03'
p8355
sg145
S'Handwriting support group. '
p8356
sg52
S'2012'
p8357
sg148
S'weekly(x1)'
p8358
sg86
g27
sg150
S'NB'
p8359
sa(dp8360
g91
S'Summer'
p8361
sg81
S'M '
p8362
sg87
S'2012-06-06 14:03'
p8363
sg145
S'Spelling support group.'
p8364
sg52
S'2012'
p8365
sg148
S'weekly(x1)'
p8366
sg86
g27
sg150
S'NB'
p8367
sa(dp8368
g91
S'Michaelmas'
p8369
sg81
S'M '
p8370
sg87
S'2013-07-08 16:13'
p8371
sg145
S'1:1 Literacy support with GSK'
p8372
sg52
S'2012'
p8373
sg148
S'weekly(x1)'
p8374
sg86
g27
sg150
g27
sa(dp8375
g91
S'Lent'
p8376
sg81
S'M '
p8377
sg87
S'2013-07-08 16:13'
p8378
sg145
S'1:1 Literacy support with GSK'
p8379
sg52
S'2013'
p8380
sg148
S'weekly(x1)'
p8381
sg86
g27
sg150
g27
sa(dp8382
g91
S'Summer'
p8383
sg81
S'M '
p8384
sg87
S'2013-07-08 16:13'
p8385
sg145
S'1:1 Literacy support with GSK'
p8386
sg52
S'2013'
p8387
sg148
S'weekly(x1)'
p8388
sg86
g27
sg150
g27
sassssS'William_Knottenbelt'
p8389
(dp8390
g3
(dp8391
g5
(dp8392
g7
I0
sg8
(lp8393
g184
ag185
asg12
S'Sen provision map for William Knottenbelt'
p8394
sg14
g15
sg16
(lp8395
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for William Knottenbelt'
p8396
sg24
I0
sg25
(lp8397
g27
aS'Encourage William to talk himself through processes and visual information. To help William focus on the task at hand, give simple instructions that are repeated and if required seat away from distractions.'
p8398
asg29
(lp8399
g27
ag31
asg32
S'SEN,Provision map,William Knottenbelt'
p8400
sg34
(lp8401
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp8402
g41
S'04/06/2009'
p8403
sg43
S'Knottenbelt'
p8404
sg45
S'Action'
p8405
sg47
S'2001-04-15'
p8406
sg49
S'EP Assessment Nina Elliott'
p8407
sg51
S'Both'
p8408
sg52
g1356
sg54
S'19-07-2012'
p8409
sg56
S"William's verbal reasoning abilities are better developed than his nonverbal reasoning abilities. Making sense of complex verbal information and using verbal abilities to solve novel problems are a strength for William. Processing complex visual information by forming spatial images of part-whole relationships and/or by manipulating the parts to solve novel problems without using words is a less well-developed ability.  William has general difficulties with literacy most specifically with phonological processing and handwriting. At times William will fail to complete a given task in the allotted time due to lack of organisation of thoughts. He additionally displays difficulty with coordination. William is dyslexic. VCI 121 PRI 106 WMi 113 PSI 106 FSIQ 116"
p8410
sg58
g27
sg59
S'William'
p8411
sg61
S'25%'
p8412
sg62
S'7CN'
p8413
ssg64
I1
sg65
I0
sg66
(lp8414
S'William is dyslexic.'
p8415
aS'William requires specific support with phonics and his ability to process sounds in words. It is important that teachers aim to boost his confidence and encourage independence. Divide tasks into clear, logical steps to a given goal and praise achievement of each step. Teachers should allow extra time and support to complete copying tasks when unavoidable however if possible provide pre prepared sheets for him. Provide a template for the setting out of a task in advance and support the organisation of his thoughts through using graphic organisers which can be transferred. It is essential that teacher check understanding of written text and of instructions. William will require help with sequencing, planning and general reading and lots of over-learning will be needed to secure facts for recall.'
p8416
asg70
S'04-06-2009'
p8417
sg72
I0
sg73
(lp8418
g75
ag27
asssg76
(dp8419
g5
(dp8420
g38
(lp8421
(dp8422
g81
S'M '
p8423
sg83
S'NB and Mrs Knottenbelt. Review of progress and future support. '
p8424
sg85
g27
sg86
g27
sg87
S'2011-05-13 10:16'
p8425
sa(dp8426
g81
S'M '
p8427
sg83
S'NB and Mrs Knottenbelt. Discussion regarding support lessons for William in year 7. NB passed on contact details to Mrs K. '
p8428
sg85
g27
sg86
g27
sg87
S'2012-06-12 10:16'
p8429
sasg37
(lp8430
(dp8431
g91
S'Lent'
p8432
sg93
S'2011'
p8433
sg95
S'To focus on the task at hand to ensure his time is used productively. '
p8434
sg81
S'M '
p8435
sg98
g27
sg87
S'2010-12-14 10:16'
p8436
sg100
g101
sg102
g27
sg86
g27
sa(dp8437
g91
S'Mich'
p8438
sg93
S'2011'
p8439
sg95
S'To increase the level of involvement in lessons.'
p8440
sg81
S'M '
p8441
sg98
g27
sg87
S'2011-09-30 10:16'
p8442
sg100
g101
sg102
g27
sg86
g27
sa(dp8443
g91
S'Mich'
p8444
sg93
S'2011'
p8445
sg95
S'To attempt lesson activities with a positive and focused attitude.'
p8446
sg81
S'M '
p8447
sg98
g27
sg87
S'2011-09-30 10:16'
p8448
sg100
g101
sg102
g27
sg86
g27
sa(dp8449
g91
S'Lent'
p8450
sg93
S'2013'
p8451
sg95
S'Include more detail in my english comprehension responses through refering to the marking scheme and following PEA.'
p8452
sg81
S'M '
p8453
sg98
g27
sg87
S'2012-07-19 11:47'
p8454
sg100
g101
sg102
g27
sg86
g27
sa(dp8455
g91
S'Mich'
p8456
sg93
S'2012'
p8457
sg95
S'To be ready to start each lesson within 2 minutes of entering the room.    '
p8458
sg81
S'M '
p8459
sg98
g27
sg87
S'2013-07-16 11:17'
p8460
sg100
g101
sg102
g27
sg86
g27
sa(dp8461
g91
S'Summer'
p8462
sg93
S'2013'
p8463
sg95
S'To increase my participation in lessons by putting my hand up at least 2 times in every lesson to either ask a question or share my ideas. '
p8464
sg81
S'M '
p8465
sg98
g27
sg87
S'2013-07-16 11:17'
p8466
sg100
g139
sg102
g27
sg86
g27
sa(dp8467
g91
S'Mich'
p8468
sg93
S'2013'
p8469
sg95
S'To increase my participation in lessons by putting my hand up at least 2 times in every lesson to either ask a question or share my ideas. '
p8470
sg81
S'M '
p8471
sg98
g27
sg87
S'2013-07-16 11:17'
p8472
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp8473
(dp8474
g91
S'Michaelmas'
p8475
sg81
S'M '
p8476
sg87
S'2013-07-08 16:17'
p8477
sg145
S'1:1 Literacy support with LC'
p8478
sg52
S'2012'
p8479
sg148
S'weekly(x1)'
p8480
sg86
g27
sg150
g27
sa(dp8481
g91
S'Lent'
p8482
sg81
S'M '
p8483
sg87
S'2013-07-08 16:17'
p8484
sg145
S'1:1 Literacy support with LC'
p8485
sg52
S'2013'
p8486
sg148
S'weekly(x1)'
p8487
sg86
g27
sg150
g27
sa(dp8488
g91
S'Summer'
p8489
sg81
S'M '
p8490
sg87
S'2013-07-08 16:17'
p8491
sg145
S'1:1 Literacy support with LC'
p8492
sg52
S'2013'
p8493
sg148
S'weekly(x1)'
p8494
sg86
g27
sg150
g27
sassssS'Henry_Corkum'
p8495
(dp8496
g3
(dp8497
g5
(dp8498
g7
I0
sg12
S'Sen provision map for Henry Corkum'
p8499
sg22
S'Special Needs Provision Map for Henry Corkum'
p8500
sg20
g21
sg14
g15
sg24
I0
sg25
(lp8501
g27
aS'Differentiated homework which focuses on words being broken into syllables, and syllables into phonemes. Glossaries and word lists/word banks are very supportive and if he could have this on his desk he could refer to it as required. Encourage Harry to think carefully about spelling before attempting words. Focus on quality and accuracy on small chunks of words and written tasks.'
p8502
asg32
S'SEN,Provision map,Henry Corkum'
p8503
sg34
(lp8504
g36
ag37
ag38
ag36
ag37
ag38
asg39
(dp8505
g41
S'17/06/2010'
p8506
sg43
S'Corkum'
p8507
sg45
S'Action'
p8508
sg47
S'2004-05-06'
p8509
sg49
S'Assessed by AS on 17.6.10 using the DEST. \r<br>Observation completed by NB 7/10/10.'
p8510
sg51
g27
sg52
g201
sg54
g27
sg56
S'Harry was referred to the LE Department as he was not making a great deal of progress in literacy and he seemed to be becoming frustrated at times with the learning in class. Harry may have some underlying processing difficulties related to the retrieval of information and words. Harry does not always use his knowledge of letters and sounds when working independently.'
p8511
sg58
g27
sg59
S'Henry'
p8512
sg61
g27
sg62
S'4CS'
p8513
ssg64
I0
sg65
I0
sg70
g392
sg72
I0
sg66
(lp8514
S'Harry may have some underlying processing difficulties related to the retrieval of information and words. Harry does not always use his knowledge of letters and sounds when working independently. Harry requires specific support to ensure he spells accurately.'
p8515
aS'The main focus of support should be on improving his understanding of spelling through syllables and phonemes.  Utilise a multi sensory approach to such tasks such as writing words, clapping the syllables, saying and singing them, cutting the word up into phonemes etc. Mnemonics. Identifying the tricky part of words and working on them. Go back to root word and build up, e.g., cord, record, recording.  Word families: go from a word Harry knows, e.g., right, tight, sight, might etc. Use common word sheets to work from the known. Teach suffixes "ing, s, ed, er, etc as he often spelt the ending of the word incorrectly.   Re read passages to him and let him hear what the word he has written sounds like.   Praise close attempts at spelling, and tell Harry why it was a good attempt. Focus on common vowel sounds and provide practice with these.'
p8516
asssg76
(dp8517
g5
(dp8518
g38
(lp8519
(dp8520
g81
S'M '
p8521
sg83
S'HF and Mr Corkum. Review of progress. '
p8522
sg85
g27
sg86
g27
sg87
S'2010-09-30 11:45'
p8523
sasg37
(lp8524
(dp8525
g91
S'Lent'
p8526
sg93
S'2011'
p8527
sg95
S'To focus on learning 5 tricky words by sight per week.'
p8528
sg81
S'M '
p8529
sg98
g27
sg87
S'2010-12-13 11:51'
p8530
sg100
g101
sg102
g27
sg86
g27
sa(dp8531
g91
S'Summer'
p8532
sg93
S'2011'
p8533
sg95
S'To remember and use spelling rules when writing independently.'
p8534
sg81
S'M '
p8535
sg98
g27
sg87
S'2010-12-13 11:51'
p8536
sg100
g101
sg102
g27
sg86
g27
sa(dp8537
g91
S'Mich'
p8538
sg93
S'2011'
p8539
sg95
S'To commence tasks within 2 minutes after all instructions and support have been given and remain focused for 5 minute periods.'
p8540
sg81
S'M '
p8541
sg98
g27
sg87
S'2011-09-21 11:33'
p8542
sg100
g101
sg102
S'-check in with Harry that he understands a task-model and structure written work when possible-teacher support-additional time to allow him to retrieve his thoughts-to build in activities in which the students have time to discuss and write down their ideas before discussing at a whole class level as this will support Harrys word retrieval difficulties- Break longer tasks down into chunks and encourage him to complete each step and check in with the teacher if required'
p8543
sg86
S'TR'
p8544
sa(dp8545
g91
S'Lent'
p8546
sg93
S'2012'
p8547
sg95
S'To use spelling and word banks on desk to help edit work.'
p8548
sg81
S'M '
p8549
sg98
g27
sg87
S'2012-01-24 13:50'
p8550
sg100
g101
sg102
g27
sg86
S'TR'
p8551
sa(dp8552
g91
S'Mich'
p8553
sg93
S'2012'
p8554
sg95
S'Increase accuracy of spelling and punctuation by checking through completed work carefully.'
p8555
sg81
S'M '
p8556
sg98
g27
sg87
S'2012-07-19 13:36'
p8557
sg100
g101
sg102
g27
sg86
g27
sa(dp8558
g91
S'Lent'
p8559
sg93
S'2013'
p8560
sg95
S'English - To identify and edit at least 3 spelling mistakes from pieces of written work. '
p8561
sg81
S'M '
p8562
sg98
g27
sg87
S'2012-07-19 13:36'
p8563
sg100
g139
sg102
g27
sg86
g27
sa(dp8564
g91
S'Summer'
p8565
sg93
S'2013'
p8566
sg95
S'English - To identify and edit at least 3 spelling mistakes from pieces of written work. '
p8567
sg81
S'M '
p8568
sg98
g27
sg87
S'2012-07-19 13:36'
p8569
sg100
g101
sg102
g27
sg86
g27
sa(dp8570
g91
S'Lent'
p8571
sg93
S'2013'
p8572
sg95
S'2. Ensure all sentences have full stops and capital letters'
p8573
sg81
S'M '
p8574
sg98
g27
sg87
S'2012-07-19 13:36'
p8575
sg100
g101
sg102
g27
sg86
g27
sa(dp8576
g91
S'Summer'
p8577
sg93
S'2013'
p8578
sg95
S'English/General - to be more responsible for remembering books and belongings and homework independently'
p8579
sg81
S'M '
p8580
sg98
g27
sg87
S'2012-07-19 13:36'
p8581
sg100
g101
sg102
g27
sg86
g27
sa(dp8582
g91
S'Mich'
p8583
sg93
S'2013'
p8584
sg95
S'English - 1. To reread his witing to ensure that it makes sense                                                                                        '
p8585
sg81
S'M '
p8586
sg98
g27
sg87
S'2013-07-16 10:20'
p8587
sg100
g139
sg102
g27
sg86
g27
sa(dp8588
g91
S'Mich'
p8589
sg93
S'2013'
p8590
sg95
S'General 2. Remember to bring homeowrk and belongings to lessons 75% of the time                                                                                 '
p8591
sg81
S'M '
p8592
sg98
g27
sg87
S'2013-07-16 10:20'
p8593
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp8594
(dp8595
g91
S'Michaelmas'
p8596
sg81
S'M '
p8597
sg87
S'2010-12-13 11:45'
p8598
sg145
S'Small group spelling support focusing on high frequency words. '
p8599
sg52
S'2010'
p8600
sg148
S'weekly(x1)'
p8601
sg86
g27
sg150
S'KM'
p8602
sa(dp8603
g91
S'Lent'
p8604
sg81
S'M '
p8605
sg87
S'2010-12-13 11:45'
p8606
sg145
S'Small group spelling support focusing on high frequency words.'
p8607
sg52
S'2011'
p8608
sg148
S'weekly(x1)'
p8609
sg86
g27
sg150
S'KM'
p8610
sa(dp8611
g91
S'Summer'
p8612
sg81
S'M '
p8613
sg87
S'2010-12-13 11:45'
p8614
sg145
S'Small group spelling support focusing on high frequency words.'
p8615
sg52
S'2011'
p8616
sg148
S'weekly(x1)'
p8617
sg86
g27
sg150
S'AST'
p8618
sa(dp8619
g91
S'Michaelmas'
p8620
sg81
S'M '
p8621
sg87
S'2011-09-27 12:12'
p8622
sg145
S'Handwriting with Mrs Wood.'
p8623
sg52
S'2011'
p8624
sg148
S'weekly(x1)'
p8625
sg86
S'TR'
p8626
sg150
S'JWO'
p8627
sa(dp8628
g91
S'Michaelmas'
p8629
sg81
S'M '
p8630
sg87
S'2011-09-27 12:12'
p8631
sg145
S'Spelling support class. '
p8632
sg52
S'2011'
p8633
sg148
S'weekly(x1)'
p8634
sg86
g27
sg150
S'NB'
p8635
sa(dp8636
g91
S'Lent'
p8637
sg81
S'M '
p8638
sg87
S'2011-09-27 12:12'
p8639
sg145
S'Spelling support class.'
p8640
sg52
S'2012'
p8641
sg148
S'weekly(x1)'
p8642
sg86
g27
sg150
S'NB'
p8643
sa(dp8644
g91
S'Summer'
p8645
sg81
S'M '
p8646
sg87
S'2012-05-01 12:12'
p8647
sg145
S'Spelling support class.'
p8648
sg52
S'2012'
p8649
sg148
S'weekly(x1)'
p8650
sg86
g27
sg150
S'NB'
p8651
sassssS'Ethan_Graham'
p8652
(dp8653
g3
(dp8654
g5
(dp8655
g7
I0
sg8
(lp8656
g184
ag185
asg12
S'Sen provision map for Ethan Graham'
p8657
sg14
g15
sg16
(lp8658
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Ethan Graham'
p8659
sg24
I0
sg25
(lp8660
g27
ag3176
asg29
(lp8661
g27
ag31
asg32
S'SEN,Provision map,Ethan Graham'
p8662
sg34
(lp8663
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp8664
g41
S'16/06/2010'
p8665
sg43
S'Graham'
p8666
sg45
S'Action'
p8667
sg47
S'2003-01-15'
p8668
sg49
S'Assessed by AS on 16.6.10. Neale and the 5 minute writing test, mazes memory'
p8669
sg51
g27
sg52
g384
sg54
g27
sg56
S"Ethan was referred for screening as his class teacher was concerned that he didn't seem to be making as much progress as she would have expected. Earlier in the year, Ethan's InCAS scores had shown that word recognition was 11 months behind chronological age and there was a concern that perhaps this was due to weak visual memory. Ethan may have difficulties with aspects of visual perceptual processing.  Visual perception is the ability to interpret information and surroundings from the effects of visible light reaching the eye. This will affect both reading and writing and as such he will require support to ensure he can focus on the text, word or page space as required."
p8670
sg58
g27
sg59
S'Ethan'
p8671
sg61
g27
sg62
S'5CN'
p8672
ssg64
I0
sg65
I0
sg66
(lp8673
S'Ethan may have difficulties with aspects of visual perceptual processing.  Visual perception is the ability to interpret information and surroundings from the effects of visible light reaching the eye. This will affect both reading and writing and as such he will require support to ensure he can focus on the text, word or page space as required.'
p8674
aS'Teachers should provide Ethan with templates to help organise his work on pages such as starting points, spacers, pages with darker lines, windows when reading, and possibly colour overlays. Ethan will require assistance and support to help with handwriting especially with his joins in writing. Providing Ethan with very specific examples that he can practice from close proximity will help him develop this ability. Making learning multi sensory will also help Ethan connect with the information as he will be able to see, feel, hear etc the information.'
p8675
asg70
g392
sg72
I0
sg73
(lp8676
g211
ag212
asssg76
(dp8677
g5
(dp8678
g38
(lp8679
(dp8680
g81
S'M '
p8681
sg83
S'NB, FS, CMC and Mrs Graham. Review of current progress and communication of continuing concerns with focus and concentration. '
p8682
sg85
S'10-12-2010'
p8683
sg86
g27
sg87
S'2010-12-10 13:57'
p8684
sasg37
(lp8685
(dp8686
g91
S'Lent'
p8687
sg93
S'2011'
p8688
sg95
S'To complete a pre-specified amount of written work independently during lessons.'
p8689
sg81
S'M '
p8690
sg98
g27
sg87
S'2010-12-13 13:57'
p8691
sg100
g101
sg102
g27
sg86
g27
sa(dp8692
g91
S'Mich'
p8693
sg93
S'2011'
p8694
sg95
S'To sit appropriately when at his desk and on the floor to support focus and concentration.'
p8695
sg81
S'M '
p8696
sg98
g27
sg87
S'2011-09-21 12:29'
p8697
sg100
g101
sg102
S'Praise and encouragement.Regular reminders.'
p8698
sg86
S'AP'
p8699
sa(dp8700
g91
S'Lent'
p8701
sg93
S'2012'
p8702
sg95
S'Commence work within 2 minutes and display focus and concentration to ensure tasks are completed. '
p8703
sg81
S'M '
p8704
sg98
g27
sg87
S'2012-01-25 16:45'
p8705
sg100
g139
sg102
S'Praise for success.'
p8706
sg86
S'AP'
p8707
sasg36
(lp8708
(dp8709
g91
S'Michaelmas'
p8710
sg81
S'M '
p8711
sg87
S'2010-05-20 08:21'
p8712
sg145
S'Reading comprehension support group. '
p8713
sg52
S'2010'
p8714
sg148
S'weekly(x1)'
p8715
sg86
g27
sg150
S'KM'
p8716
sa(dp8717
g91
S'Michaelmas'
p8718
sg81
S'M '
p8719
sg87
S'2010-05-20 08:21'
p8720
sg145
S'Handwriting group. '
p8721
sg52
S'2010'
p8722
sg148
S'weekly(x1)'
p8723
sg86
g27
sg150
S'KM'
p8724
sa(dp8725
g91
S'Michaelmas'
p8726
sg81
S'M '
p8727
sg87
S'2010-05-20 08:21'
p8728
sg145
S'Small group spelling support focusing on spelling strategies.'
p8729
sg52
S'2010'
p8730
sg148
S'weekly(x1)'
p8731
sg86
g27
sg150
S'NB'
p8732
sa(dp8733
g91
S'Lent'
p8734
sg81
S'M '
p8735
sg87
S'2010-05-20 08:21'
p8736
sg145
S'Reading comprehension support group.'
p8737
sg52
S'2011'
p8738
sg148
S'weekly(x1)'
p8739
sg86
g27
sg150
S'KM'
p8740
sa(dp8741
g91
S'Lent'
p8742
sg81
S'M '
p8743
sg87
S'2010-05-20 08:21'
p8744
sg145
S'Handwriting group.'
p8745
sg52
S'2011'
p8746
sg148
S'weekly(x1)'
p8747
sg86
g27
sg150
S'KM'
p8748
sa(dp8749
g91
S'Lent'
p8750
sg81
S'M '
p8751
sg87
S'2010-05-20 08:21'
p8752
sg145
S'Small group spelling support focusing on spelling strategies.'
p8753
sg52
S'2011'
p8754
sg148
S'weekly(x1)'
p8755
sg86
g27
sg150
S'NB'
p8756
sa(dp8757
g91
S'Summer'
p8758
sg81
S'M '
p8759
sg87
S'2010-05-20 08:21'
p8760
sg145
S'Reading comprehension support group.'
p8761
sg52
S'2011'
p8762
sg148
S'weekly(x1)'
p8763
sg86
g27
sg150
S'AST'
p8764
sa(dp8765
g91
S'Summer'
p8766
sg81
S'M '
p8767
sg87
S'2010-05-20 08:21'
p8768
sg145
S'Handwriting group.'
p8769
sg52
S'2011'
p8770
sg148
S'weekly(x1)'
p8771
sg86
g27
sg150
S'AST'
p8772
sa(dp8773
g91
S'Summer'
p8774
sg81
S'M '
p8775
sg87
S'2010-05-20 08:21'
p8776
sg145
S'Small group spelling support focusing on spelling strategies.'
p8777
sg52
S'2011'
p8778
sg148
S'weekly(x1)'
p8779
sg86
g27
sg150
S'NB'
p8780
sassssS'Elleneta_Armitage'
p8781
(dp8782
g3
(dp8783
g5
(dp8784
g7
I0
sg8
(lp8785
S'Her weak memory will make it hard for her to recall and retain key mathematical facts however her mathematical reasoning and understanding of numerial operations is good.'
p8786
aS'Ellie will benefit from all strategies possible to support her memory. Displaying and explaining key vocabulary for maths will help her comprehension. Provide Ellie with templates during multistep mathematics questions and use visuals where possible to help teach new concepts. Ellie must be shown how to set out a question and learn these processes to prevent confusion and loosing information that is stored short term memory. Templates will help her follow the process and focus on the question itself.'
p8787
asg12
S'Sen provision map for Elleneta Armitage'
p8788
sg14
g15
sg16
(lp8789
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Elleneta Armitage'
p8790
sg24
I0
sg25
(lp8791
g5162
ag5163
asg29
(lp8792
g27
ag31
asg32
S'SEN,Provision map,Elleneta Armitage'
p8793
sg34
(lp8794
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp8795
g41
S'20/04/2009'
p8796
sg43
S'Armitage'
p8797
sg45
S'Action'
p8798
sg47
S'2003-08-12'
p8799
sg49
S'Working Memory Battery - NB 08/06/2011\r<br>DST - KAM 04/05/2011\r<br>EP Assessment - Annie Mitchell 12/10/2011'
p8800
sg51
g27
sg52
g384
sg54
g27
sg56
S"Ellie has some difficulties with her short term memory and recalling information from her long term memory. Her IQ is in the average range with strenghts in verbal and non verbal reasoning and some weaknesses in auditory and visual working memory. Ellie's basic literacy skills are fine and her reading and comprehension is also good. She may at times struggle with recalling key vocabulary and recalling what has just been read. Her weak memory will make it hard for her to recall and retain key mathematical facts however her mathematical reasoning and understanding of numerial operations is good. She lacks confidence in her own abilities and at times requires support and repetition of questions to make sure she attempts her work. Ellie suffers from hyper mobility, very bendy joints. Due to this Ellie will fatigue more easily. Ellie has a nut allergy and an Epipen should be available as per school policy."
p8801
sg58
g27
sg59
S'Elleneta'
p8802
sg61
g27
sg62
S'5CS'
p8803
ssg64
I1
sg65
I0
sg66
(lp8804
S"Ellie's basic literacy skills are fine and her reading and comprehension is also good. She may at times struggle with recalling key vocabulary and recalling what has just been read."
p8805
aS'Ellie will benefit from all strategies possible to support her memory. Displaying and explaining key vocabulary before reading a piece of text will help her comprehension.  Where possible, break tasks down into 2-3 steps. Presenting information in parts which make up the whole will allow her to focus on the task with greater accuracy. Ensure you explain what the completed task will be before commencing so she can see how all smaller parts contribute to achieving the outcome. Talking her way through multistep tasks will help Ellie.'
p8806
asg70
S'12-10-2011'
p8807
sg72
I0
sg73
(lp8808
g2699
ag2700
asssg76
(dp8809
g5
(dp8810
g38
(lp8811
(dp8812
g81
S'F '
p8813
sg83
S'KAM, GS,  Mr and Mrs Armitage. Review of assessment completed by KAM. Actions suggested. '
p8814
sg85
g27
sg86
g27
sg87
S'2011-05-18 14:29'
p8815
sa(dp8816
g81
S'F '
p8817
sg83
S'NB and Mrs Armitage. Discussion of MBT and suggestions for future support given. '
p8818
sg85
g27
sg86
g27
sg87
S'2011-06-26 14:29'
p8819
sa(dp8820
g81
S'F '
p8821
sg83
S'NB and Mrs Armitage regarding tutoring, support and EP assessment. '
p8822
sg85
g27
sg86
g27
sg87
S'2011-09-12 14:29'
p8823
sa(dp8824
g81
S'F '
p8825
sg83
S'Review Meeting with NB and Mrs Armitage. Discussion of EP Report, support options and current progress. '
p8826
sg85
S'29-11-2011'
p8827
sg86
g27
sg87
S'2011-11-29 14:10'
p8828
sasg37
(lp8829
(dp8830
g91
S'Lent'
p8831
sg93
S'2011'
p8832
sg95
S'To ensure that she eats her protein snacks when required (i.e if she is feeling tired).'
p8833
sg81
S'F '
p8834
sg98
g27
sg87
S'2010-12-13 14:29'
p8835
sg100
g101
sg102
g27
sg86
g27
sa(dp8836
g91
S'Mich'
p8837
sg93
S'2011'
p8838
sg95
S'To speak to a teacher and ask for help when needed.'
p8839
sg81
S'F '
p8840
sg98
g27
sg87
S'2011-09-09 12:17'
p8841
sg100
g101
sg102
S'Ellie to use her traffic lights to show teacher when she is unsure of a concept.'
p8842
sg86
S'AP'
p8843
sa(dp8844
g91
S'Lent'
p8845
sg93
S'2012'
p8846
sg95
S'To display confidence in attempting tasks without fear of failure.'
p8847
sg81
S'F '
p8848
sg98
g27
sg87
S'2012-01-25 17:02'
p8849
sg100
g101
sg102
S'Regular encouragement and praise.'
p8850
sg86
S'AP'
p8851
sa(dp8852
g91
S'Mich'
p8853
sg93
S'2012'
p8854
sg95
S'1. To use imaginative vocabulary in her stories and descriptions.'
p8855
sg81
S'F '
p8856
sg98
g27
sg87
S'2012-07-19 13:29'
p8857
sg100
g101
sg102
g27
sg86
g27
sa(dp8858
g91
S'Mich'
p8859
sg93
S'2012'
p8860
sg95
S' 2. Bring her diary and homework every day.'
p8861
sg81
S'F '
p8862
sg98
g27
sg87
S'2012-07-19 13:29'
p8863
sg100
g101
sg102
g27
sg86
g27
sa(dp8864
g91
S'Mich'
p8865
sg93
S'2012'
p8866
sg95
S'3. Making she sure has the correct equipment for lessons'
p8867
sg81
S'F '
p8868
sg98
g27
sg87
S'2012-07-19 13:29'
p8869
sg100
g101
sg102
g27
sg86
g27
sa(dp8870
g91
S'Lent'
p8871
sg93
S'2013'
p8872
sg95
S'Concentrate and focus on instructions to make sure I can attempt work independently before asking for help. '
p8873
sg81
S'F '
p8874
sg98
g27
sg87
S'2012-07-19 13:29'
p8875
sg100
g139
sg102
g27
sg86
g27
sa(dp8876
g91
S'Summer'
p8877
sg93
S'2013'
p8878
sg95
S'Concentrate and focus on instructions to make sure I can attempt work independently before asking for help. '
p8879
sg81
S'F '
p8880
sg98
g27
sg87
S'2012-07-19 13:29'
p8881
sg100
g101
sg102
g27
sg86
g27
sa(dp8882
g91
S'Mich'
p8883
sg93
S'2013'
p8884
sg95
S'To consistently produce amounts of work independently following teacher instruction and support.'
p8885
sg81
S'F '
p8886
sg98
g27
sg87
S'2012-07-19 13:29'
p8887
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp8888
(dp8889
g91
S'Michaelmas'
p8890
sg81
S'F '
p8891
sg87
S'2011-10-05 15:23'
p8892
sg145
S'Reading comprehension support group. '
p8893
sg52
S'2011'
p8894
sg148
S'weekly(x1)'
p8895
sg86
g27
sg150
S'NB'
p8896
sa(dp8897
g91
S'Michaelmas'
p8898
sg81
S'F '
p8899
sg87
S'2011-10-05 15:23'
p8900
sg145
S'1:1 working memory support. '
p8901
sg52
S'2011'
p8902
sg148
S'weekly(x1)'
p8903
sg86
g27
sg150
S'NB'
p8904
sa(dp8905
g91
S'Lent'
p8906
sg81
S'F '
p8907
sg87
S'2011-10-05 15:23'
p8908
sg145
S'Reading comprehension support group.'
p8909
sg52
S'2012'
p8910
sg148
S'weekly(x1)'
p8911
sg86
g27
sg150
S'NB'
p8912
sa(dp8913
g91
S'Summer'
p8914
sg81
S'F '
p8915
sg87
S'2011-10-05 15:23'
p8916
sg145
S'Reading comprehension support group.'
p8917
sg52
S'2012'
p8918
sg148
S'weekly(x1)'
p8919
sg86
g27
sg150
S'NB'
p8920
sassssS'Jack_Shabi'
p8921
(dp8922
g3
(dp8923
g5
(dp8924
g7
I0
sg8
(lp8925
g1548
ag1549
asg12
S'Sen provision map for Jack Shabi'
p8926
sg14
g15
sg16
(lp8927
g557
ag27
asg20
g21
sg22
S'Special Needs Provision Map for Jack Shabi'
p8928
sg24
I1
sg25
(lp8929
g27
ag7078
asg29
(lp8930
g562
ag27
asg32
S'SEN,Provision map,Jack Shabi'
p8931
sg34
(lp8932
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp8933
g41
S'06-09-2010'
p8934
sg43
S'Shabi'
p8935
sg45
S'Action'
p8936
sg47
S'2001-03-05'
p8937
sg49
S'EP Report Nina Elliott\r<br>Julia Terteryan 16.11.10\r<br>Julia Terteryan 15.02.2011'
p8938
sg51
S'Both'
p8939
sg52
g1356
sg54
S'19-02-2012'
p8940
sg56
S"Jack has a history of conductive hearing loss. Jack's abilities to sustain concentration are a weakness relative to his verbal reasoning skills. His ability to process visual information quickly is also a weakness compared to his verbal reasoning. Jack's writing speed is slow and the presentation of his work is not of a high standard due to awkward pencil grip which he has previously recieved OT for. VCI 126 PRI 121 WMI 107 PSI 103 FSIQ 120"
p8941
sg58
g27
sg59
S'Jack'
p8942
sg61
S'25%'
p8943
sg62
S'7CS'
p8944
ssg64
I1
sg65
I0
sg66
(lp8945
S"Jack's visual memory is relatively weak therefore he will require multisensory teaching and verbal instructions to support written work."
p8946
aS'Teachers should be aware of issue and not give him large amounts of work to copy out etc. Give p/c notes and sections for him to annotate etc. Teachers should still maintain high expectations in regards to written work however be aware of his difficulties when marking and giving feedback. Check for understanding and discuss instrucutions and tasks with him.'
p8947
asg70
S'06-09-2010'
p8948
sg72
I0
sg73
(lp8949
S"Jack's writing speed is slow and the presentation of his work is not of a high standard due to awkward pencil grip."
p8950
aS"Please do not insist Jack joins his letters at present as he is progressing to a new writing style. Teachers should focus on content of work rather than 'secretarial comments' (unless this is the learning Intention). Jack's work should be scaffolded to allow him to develop his writing.  It has been suggested by an OT that Jack go back to writing with a pencil and pencil grip for the time being to help his pencil grip develop."
p8951
asssg76
(dp8952
g5
(dp8953
g38
(lp8954
(dp8955
g81
S'M '
p8956
sg83
S'NB and Mrs Shabi. Discussion hearning difficulties and EP report. '
p8957
sg85
g27
sg86
g27
sg87
S'2010-10-15 14:05'
p8958
sa(dp8959
g81
S'M '
p8960
sg83
S'EMail from mrs Shabi. Information regarding hearning difficulties with approximate loss of 50%. Surgery in November 2010 unsuccessful and future operations will be required in coming years. '
p8961
sg85
g27
sg86
g27
sg87
S'2011-07-05 14:05'
p8962
sa(dp8963
g81
S'M '
p8964
sg83
S'Summer 2011 Report - Lindsey Copeman'
p8965
sg85
g27
sg86
g27
sg87
S'2011-07-13 14:55'
p8966
sasg37
(lp8967
(dp8968
g91
S'Lent'
p8969
sg93
S'2011'
p8970
sg95
S'To work on the size, fluency and spacing of his handwriting. '
p8971
sg81
S'M '
p8972
sg98
g27
sg87
S'2010-12-14 14:05'
p8973
sg100
g101
sg102
g27
sg86
g27
sa(dp8974
g91
S'Lent'
p8975
sg93
S'2012'
p8976
sg95
S'To be proactive in asking questions in class especially when he is not sure of something.'
p8977
sg81
S'M '
p8978
sg98
g27
sg87
S'2012-03-08 14:05'
p8979
sg100
g101
sg102
g27
sg86
g27
sa(dp8980
g91
S'Mich'
p8981
sg93
S'2012'
p8982
sg95
S'To continue to display confidence in his own abilities and be willing to share his ideas with others in lessons.'
p8983
sg81
S'M '
p8984
sg98
g27
sg87
S'2012-07-19 12:51'
p8985
sg100
g101
sg102
g27
sg86
g27
sa(dp8986
g91
S'Lent'
p8987
sg93
S'2013'
p8988
sg95
S'To put my hand up at least 2 times in every lesson to either ask a question or share my ideas. '
p8989
sg81
S'M '
p8990
sg98
g27
sg87
S'2012-07-19 12:51'
p8991
sg100
g139
sg102
g27
sg86
g27
sa(dp8992
g91
S'Summer'
p8993
sg93
S'2013'
p8994
sg95
S'To put my hand up at least 2 times in every lesson to either ask a question or share my ideas. '
p8995
sg81
S'M '
p8996
sg98
g27
sg87
S'2012-07-19 12:51'
p8997
sg100
g101
sg102
g27
sg86
g27
sa(dp8998
g91
S'Mich'
p8999
sg93
S'2013'
p9000
sg95
S'To self evaulate his performance in lessons and communicate with staff when he needs support. '
p9001
sg81
S'M '
p9002
sg98
g27
sg87
S'2013-07-16 11:23'
p9003
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp9004
(dp9005
g91
S'Michaelmas'
p9006
sg81
S'M '
p9007
sg87
S'2010-12-14 14:05'
p9008
sg145
S'Handwriting Support Group'
p9009
sg52
S'2010'
p9010
sg148
S'weekly(x1)'
p9011
sg86
g27
sg150
S'NB'
p9012
sa(dp9013
g91
S'Lent'
p9014
sg81
S'M '
p9015
sg87
S'2010-12-14 14:05'
p9016
sg145
S'Handwriting Support Group'
p9017
sg52
S'2011'
p9018
sg148
S'weekly(x1)'
p9019
sg86
g27
sg150
S'NB'
p9020
sa(dp9021
g91
S'Summer'
p9022
sg81
S'M '
p9023
sg87
S'2010-12-14 14:05'
p9024
sg145
S'Handwriting Support Group'
p9025
sg52
S'2011'
p9026
sg148
S'weekly(x1)'
p9027
sg86
g27
sg150
S'NB'
p9028
sa(dp9029
g91
S'Summer'
p9030
sg81
S'M '
p9031
sg87
S'2011-07-13 14:56'
p9032
sg145
S'1:1 Support Lindsey Copeman - Literacy. '
p9033
sg52
S'2011'
p9034
sg148
S'weekly(x1)'
p9035
sg86
g27
sg150
g27
sa(dp9036
g91
S'Michaelmas'
p9037
sg81
S'M '
p9038
sg87
S'2011-09-13 14:56'
p9039
sg145
S'1:1 Support Lindsey Copeman - Literacy.'
p9040
sg52
S'2011'
p9041
sg148
S'weekly(x1)'
p9042
sg86
g27
sg150
g27
sa(dp9043
g91
S'Summer'
p9044
sg81
S'M '
p9045
sg87
S'2012-04-13 14:56'
p9046
sg145
S'1:1 Support Jane Hanson.'
p9047
sg52
S'2012'
p9048
sg148
S'weekly(x1)'
p9049
sg86
g27
sg150
g27
sassssS'Harry_Jacques'
p9050
(dp9051
g3
(dp9052
g5
(dp9053
g7
I1
sg8
(lp9054
g10
ag11
asg12
S'Sen provision map for Harry Jacques'
p9055
sg14
g15
sg16
(lp9056
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Harry Jacques'
p9057
sg24
I1
sg25
(lp9058
g27
aS'It is important that staff ensure they have his complete attention when they are speaking. Check his understandings through having him repeat what has been asked or shared in his own words. Staff should aim to keep verbal instructions to 2-3 at a time and scaffold this process through additionally providing a written or visual example to support the verbal instruction. Break tasks down into their smallest components and encourage completion of each step at a time. All lessons should be made as multi-sensory as possible as this will aid the transfer of knowledge to long term memory and provide an opportunity to make a connection to the content beyond verbal or verbal instruction. Use checklists on his desk at the beginning and end of each day with use of an egg timer so he has to complete certain tasks in a certain time. Ask if he has everything BEFORE he heads off to a lesson.'
p9059
asg29
(lp9060
g27
ag31
asg32
S'SEN,Provision map,Harry Jacques'
p9061
sg34
(lp9062
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp9063
g41
S'01/03/2007'
p9064
sg43
S'Jacques'
p9065
sg45
S'Action +'
p9066
sg47
S'2001-02-04'
p9067
sg49
S'EP S Brooks\r<br>Anne Wilson - optometrist \r<br>Laura Miller- OT (2007)'
p9068
sg51
g27
sg52
g1356
sg54
g27
sg56
S"Harry may show some immature letter formation and he writes slowly. His difficulties appear to be linked mainly to the physical act of writing although aspects of Harry's phonological processing skills are mildly delayed as is his working knowledge of phoneme-grapheme links. Harry has some difficulties with remaining focused on the task at hand. VCI 114 PRI 110 WMI 104 PSI 97 FSIQ 110"
p9069
sg58
g27
sg59
S'Harry'
p9070
sg61
g27
sg62
S'7CN'
p9071
ssg64
I1
sg65
I0
sg66
(lp9072
S"Harry's phonological processing skills are mildly delayed as is his working knowledge of phoneme-grapheme links."
p9073
aS'Praising close attempts at unknown words in spelling and reading is essential and through discussion, Harry will make stronger connections with the content.'
p9074
asg70
S'02-07-2008'
p9075
sg72
I0
sg73
(lp9076
S'Harry may show some immature letter formation and he writes slowly. His difficulties appear to be linked mainly to the physical act of writing.'
p9077
aS'Encourage Harry to tilt his work book slightly when writing and refrain from colouring in errors in his book as this makes it very hard to read back. A simple line through his error is appropriate.'
p9078
asssg76
(dp9079
g5
(dp9080
g38
(lp9081
(dp9082
g81
S'M '
p9083
sg83
S'NB and Mrs Jacques. Review of learning support. '
p9084
sg85
g27
sg86
g27
sg87
S'2011-06-21 11:35'
p9085
sa(dp9086
g81
S'M '
p9087
sg83
S'NB, SAC and Mr and Mrs Jacques. Review of learning support and progress. '
p9088
sg85
S'27-09-2011'
p9089
sg86
g27
sg87
S'2011-09-27 11:35'
p9090
sasg37
(lp9091
(dp9092
g91
S'Lent'
p9093
sg93
S'2011'
p9094
sg95
S'To have all required equipment before leaving the classroom for a lesson.'
p9095
sg81
S'M '
p9096
sg98
g27
sg87
S'2010-12-14 11:37'
p9097
sg100
g101
sg102
g27
sg86
g27
sa(dp9098
g91
S'Mich'
p9099
sg93
S'2012'
p9100
sg95
S'To have the LO and the date written without being prompted.'
p9101
sg81
S'M '
p9102
sg98
g27
sg87
S'2012-07-19 14:06'
p9103
sg100
g139
sg102
g27
sg86
g27
sa(dp9104
g91
S'Mich'
p9105
sg93
S'2012'
p9106
sg95
S'To increase the amount of written work that is completed in lessons.'
p9107
sg81
S'M '
p9108
sg98
g27
sg87
S'2012-07-19 14:06'
p9109
sg100
g139
sg102
g27
sg86
g27
sa(dp9110
g91
S'Lent'
p9111
sg93
S'2013'
p9112
sg95
S'To have the LO and the date written without being prompted.'
p9113
sg81
S'M '
p9114
sg98
g27
sg87
S'2012-07-19 14:06'
p9115
sg100
g101
sg102
g27
sg86
g27
sa(dp9116
g91
S'Lent'
p9117
sg93
S'2013'
p9118
sg95
S'To increase the amount of written work that is completed in lessons.'
p9119
sg81
S'M '
p9120
sg98
g27
sg87
S'2012-07-19 14:06'
p9121
sg100
g139
sg102
g27
sg86
g27
sa(dp9122
g91
S'Summer'
p9123
sg93
S'2013'
p9124
sg95
S'To increase the amount of written work that is completed in lessons.'
p9125
sg81
S'M '
p9126
sg98
g27
sg87
S'2012-07-19 14:06'
p9127
sg100
g101
sg102
g27
sg86
g27
sa(dp9128
g91
S'Summer'
p9129
sg93
S'2013'
p9130
sg95
S'2. To hand in homework completed and on time (75% of the time).     '
p9131
sg81
S'M '
p9132
sg98
g27
sg87
S'2012-07-19 14:06'
p9133
sg100
g101
sg102
g27
sg86
g27
sa(dp9134
g91
S'Lent'
p9135
sg93
S'2012'
p9136
sg95
S'To have the LO and the date written without being prompted.                                                                                  '
p9137
sg81
S'M '
p9138
sg98
g27
sg87
S'2012-12-14 11:36'
p9139
sg100
g101
sg102
g27
sg86
g27
sa(dp9140
g91
S'Lent'
p9141
sg93
S'2012'
p9142
sg95
S' To display more independence as a learner.                                                                              '
p9143
sg81
S'M '
p9144
sg98
g27
sg87
S'2012-12-14 11:36'
p9145
sg100
g101
sg102
g27
sg86
g27
sasg36
(lp9146
(dp9147
g91
S'Michaelmas'
p9148
sg81
S'M '
p9149
sg87
S'2010-12-14 11:35'
p9150
sg145
S'1:1 Learning Specialist Isabel Clarke.'
p9151
sg52
S'2010'
p9152
sg148
S'weekly(x1)'
p9153
sg86
g27
sg150
g27
sa(dp9154
g91
S'Lent'
p9155
sg81
S'M '
p9156
sg87
S'2010-12-14 11:35'
p9157
sg145
S'1:1 Learning Specialist Isabel Clarke.'
p9158
sg52
S'2011'
p9159
sg148
S'weekly(x1)'
p9160
sg86
g27
sg150
g27
sa(dp9161
g91
S'Summer'
p9162
sg81
S'M '
p9163
sg87
S'2010-12-14 11:35'
p9164
sg145
S'1:1 Learning Specialist Isabel Clarke.'
p9165
sg52
S'2011'
p9166
sg148
S'weekly(x1)'
p9167
sg86
g27
sg150
g27
sa(dp9168
g91
S'Michaelmas'
p9169
sg81
S'M '
p9170
sg87
S'2010-12-14 11:36'
p9171
sg145
S'Handwriting support group. '
p9172
sg52
S'2010'
p9173
sg148
S'weekly(x1)'
p9174
sg86
g27
sg150
S'NB'
p9175
sa(dp9176
g91
S'Lent'
p9177
sg81
S'M '
p9178
sg87
S'2010-12-14 11:36'
p9179
sg145
S'Handwriting support group.'
p9180
sg52
S'2011'
p9181
sg148
S'weekly(x1)'
p9182
sg86
g27
sg150
S'NB'
p9183
sassssS'Elke_Arnott'
p9184
(dp9185
g3
(dp9186
g5
(dp9187
g7
I0
sg8
(lp9188
g1828
ag1829
asg12
S'Sen provision map for Elke Arnott'
p9189
sg14
g15
sg16
(lp9190
g27
ag2032
asg20
g21
sg22
S'Special Needs Provision Map for Elke Arnott'
p9191
sg24
I0
sg25
(lp9192
g27
ag1834
asg29
(lp9193
g2036
ag2037
asg32
S'SEN,Provision map,Elke Arnott'
p9194
sg34
(lp9195
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp9196
g41
S'28-09-2011'
p9197
sg43
S'Arnott'
p9198
sg45
S'Action'
p9199
sg47
S'2004-05-12'
p9200
sg49
S'Assessed by NB 28.09.2011\r<br>EP Report A Mitchell 12.10.2011'
p9201
sg51
g27
sg52
g201
sg54
S'11-09-2011'
p9202
sg56
S'Elke has some specific diffioculties with recall from long term memory, short term working memory (visual and auditory) and recalling the correct sequence of information specifically numbers. She has difficulties with place value and her basic knowledge of maths may at times be weak. She tends to read words quickly and not break down unfamiliar words. She is over reliant on phonic spelling strategies and she sometiimes makes careless mistakes when reading. \r<br>VCI 110 PRI 119 WMI 97 PSI 97 FSIQ 109'
p9203
sg58
g27
sg59
S'Elke'
p9204
sg61
g27
sg62
S'4CS'
p9205
ssg64
I1
sg65
I0
sg66
(lp9206
S'Elke has some specific diffioculties with recall from long term memory, short term working memory (visual and auditory) and recalling the correct sequence of information. She tends to read words quickly and not break down unfamiliar words. She is over reliant on phonic spelling strategies and she sometiimes makes careless mistakes when reading.'
p9207
aS' Where possible, break tasks down into 2-3 steps. Presenting information in parts which make up the whole will allow her to focus on the task with greater accuracy. Ensure you explain what the completed task will be before commencing so she can see how all smaller parts contribute to achieving the outcome. Encourage her to slow down when reading and chunk unknown words to help decode.'
p9208
asg70
S'12-10-2011'
p9209
sg72
I0
sg73
(lp9210
g1852
ag1853
asssg76
(dp9211
g5
(dp9212
g37
(lp9213
(dp9214
g91
S'Lent'
p9215
sg93
S'2012'
p9216
sg95
S'Maths: To consolidate work in class by completing homework each week and handing it in. '
p9217
sg81
S'F '
p9218
sg98
g27
sg87
S'2012-02-06 11:28'
p9219
sg100
g139
sg102
g27
sg86
g27
sa(dp9220
g91
S'Mich'
p9221
sg93
S'2012'
p9222
sg95
S'Maths: Remain focused and complete the work in the alloted time.'
p9223
sg81
S'F '
p9224
sg98
g27
sg87
S'2012-07-19 13:31'
p9225
sg100
g101
sg102
g27
sg86
g27
sa(dp9226
g91
S'Mich'
p9227
sg93
S'2012'
p9228
sg95
S'Literacy: Increase accuracy of punctuation by re-reading written work aloud.'
p9229
sg81
S'F '
p9230
sg98
g27
sg87
S'2012-07-19 13:31'
p9231
sg100
g101
sg102
g27
sg86
g27
sa(dp9232
g91
S'Lent'
p9233
sg93
S'2013'
p9234
sg95
S'1. To identify and edit at least 3 spelling mistakes from pieces of written work.                                                               '
p9235
sg81
S'F '
p9236
sg98
g27
sg87
S'2012-07-19 13:31'
p9237
sg100
g101
sg102
g27
sg86
g27
sa(dp9238
g91
S'Lent'
p9239
sg93
S'2013'
p9240
sg95
S'2. Ensure all sentences have full stops and capital letters and are not too long.                  '
p9241
sg81
S'F '
p9242
sg98
g27
sg87
S'2012-07-19 13:31'
p9243
sg100
g101
sg102
g27
sg86
g27
sa(dp9244
g91
S'Summer'
p9245
sg93
S'2013'
p9246
sg95
S'Maths: To check that her answer fits the question and is a sensible one.                  '
p9247
sg81
S'F '
p9248
sg98
g27
sg87
S'2012-07-19 13:31'
p9249
sg100
g101
sg102
g27
sg86
g27
sa(dp9250
g91
S'Mich'
p9251
sg93
S'2013'
p9252
sg95
S'1.To use a range of connectives in writing.                                                                 '
p9253
sg81
S'F '
p9254
sg98
g27
sg87
S'2013-07-16 10:22'
p9255
sg100
g139
sg102
g27
sg86
g27
sa(dp9256
g91
S'Mich'
p9257
sg93
S'2013'
p9258
sg95
S'2. Ensure story ideas are plausible and flow                                                            '
p9259
sg81
S'F '
p9260
sg98
g27
sg87
S'2013-07-16 10:22'
p9261
sg100
g139
sg102
g27
sg86
g27
sa(dp9262
g91
S'Mich'
p9263
sg93
S'2013'
p9264
sg95
S'3. To ensure story endings are well paced.                                                                        '
p9265
sg81
S'F '
p9266
sg98
g27
sg87
S'2013-07-16 10:22'
p9267
sg100
g139
sg102
g27
sg86
g27
sa(dp9268
g91
S'Mich'
p9269
sg93
S'2013'
p9270
sg95
S' Maths: To work through a problem slowly and check method.                                                                                                     '
p9271
sg81
S'F '
p9272
sg98
g27
sg87
S'2013-07-16 10:22'
p9273
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp9274
(dp9275
g91
S'Lent'
p9276
sg81
S'F '
p9277
sg87
S'2012-07-16 14:23'
p9278
sg145
S'1:1 Eleanor Barker. '
p9279
sg52
S'2012'
p9280
sg148
S'weekly(x1)'
p9281
sg86
g27
sg150
g27
sa(dp9282
g91
S'Summer'
p9283
sg81
S'F '
p9284
sg87
S'2012-07-16 14:23'
p9285
sg145
S'1:1 Eleanor Barker. '
p9286
sg52
S'2012'
p9287
sg148
S'weekly(x1)'
p9288
sg86
g27
sg150
g27
sa(dp9289
g91
S'Michaelmas'
p9290
sg81
S'F '
p9291
sg87
S'2012-07-16 14:23'
p9292
sg145
S'1:1 Eleanor Barker. '
p9293
sg52
S'2012'
p9294
sg148
S'weekly(x1)'
p9295
sg86
g27
sg150
g27
sa(dp9296
g91
S'Michaelmas'
p9297
sg81
S'F '
p9298
sg87
S'2013-07-08 16:11'
p9299
sg145
S'1:1 Literacy/numeracy support with EB'
p9300
sg52
S'2012'
p9301
sg148
S'weekly(x1)'
p9302
sg86
g27
sg150
g27
sa(dp9303
g91
S'Lent'
p9304
sg81
S'F '
p9305
sg87
S'2013-07-08 16:11'
p9306
sg145
S'1:1 Literacy/numeracy support with EB'
p9307
sg52
S'2013'
p9308
sg148
S'weekly(x1)'
p9309
sg86
g27
sg150
g27
sa(dp9310
g91
S'Summer'
p9311
sg81
S'F '
p9312
sg87
S'2013-07-08 16:11'
p9313
sg145
S'1:1 Literacy/numeracy support with EB'
p9314
sg52
S'2013'
p9315
sg148
S'weekly(x1)'
p9316
sg86
g27
sg150
g27
sassssS'Joe_Drury'
p9317
(dp9318
g3
(dp9319
g5
(dp9320
g7
I0
sg8
(lp9321
g1828
ag1829
asg12
S'Sen provision map for Joe Drury'
p9322
sg14
g15
sg16
(lp9323
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Joe Drury'
p9324
sg24
I1
sg25
(lp9325
g27
aS'With a diagnosis of ADHD, many of the difficulties Joe has in other subjects may be a direct result of his ADHD therefore all teachers must ensure that they have a clear understanding of the function of his behaviour to avoid interpreting it as non compliance, avoidance or disruptive. Support Joe through monitoring seating arrangements to minimize distractions, give instructions one at a time and repeat as necessary, using visuals such as charts, pictures, colour coding. Divide long-term work tasks into segments and assign a completion goal for each segment and where possible list the activities of the lesson on the board.In opening the lesson, tell Joe what the he is going to learn and what your expectations are. Tell Joe exactly what materials he will need.Vary the pace and include different kinds of activities. Have an unobtrusive cue set up with Joe, such as a touch on the shoulder or placing a sticky note on his desk, to remind him to stay on task.'
p9326
asg29
(lp9327
g27
ag31
asg32
S'SEN,Provision map,Joe Drury'
p9328
sg34
(lp9329
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp9330
g41
S'10/03/2007'
p9331
sg43
S'Drury'
p9332
sg45
S'Action +'
p9333
sg47
S'2000-04-09'
p9334
sg49
S'EP Report Jeremy Monson \r<br>EP Nina Elliott 04.07.2011\r<br>EP Gabby Newman 10.05.2012\r<br>Amanda Evans (OT) 27.3.06, \r<br>Paediatrician Dr G D Kewley 10.03.2010 and 13.04.2007.'
p9335
sg51
g27
sg52
g935
sg54
g27
sg56
S'Joe has been diagnosed with ADHD and is medicated (Concerta and Ritalin) in the morning before school. He has difficulties with planning and structuring ideas in literacy and some difficulties with numerical reasoning. Joe has some difficulties with the physical act of handwriting.'
p9336
sg58
g27
sg59
S'Joe'
p9337
sg61
g27
sg62
S'8CE'
p9338
ssg64
I1
sg65
I0
sg66
(lp9339
g6010
ag6011
asg70
S'10-05-2012'
p9340
sg72
I0
sg73
(lp9341
g6273
ag6274
asssg76
(dp9342
g5
(dp9343
g38
(lp9344
(dp9345
g81
S'M '
p9346
sg83
S'NB and Mrs Horton. Extra time allocation confirmed at 25%. Mrs Horton to arrange updated EP report. '
p9347
sg85
g27
sg86
g27
sg87
S'2011-05-24 16:08'
p9348
sa(dp9349
g81
S'M '
p9350
sg83
S'NB, SM, RH, SAC Mr Drury and Miss Horton. Summary of assessment and discussion of progress. '
p9351
sg85
g27
sg86
g27
sg87
S'2011-07-07 16:08'
p9352
sa(dp9353
g81
S'M '
p9354
sg83
S'NB, CAE, Mr and Mrs Drury. Meeting to discuss schools options and to discuss exam results. '
p9355
sg85
S'05-12-2011'
p9356
sg86
g27
sg87
S'2011-12-05 17:00'
p9357
sa(dp9358
g81
S'M '
p9359
sg83
S'NB and Mrs Horton. Extra time discussion as Joe was not given ET due to an expired report and staff observations. '
p9360
sg85
g27
sg86
g27
sg87
S'2012-12-07 16:08'
p9361
sasg37
(lp9362
(dp9363
g91
S'Lent'
p9364
sg93
S'2011'
p9365
sg95
S'To develop a greater ability to self regulate his behaviour. '
p9366
sg81
S'M '
p9367
sg98
g27
sg87
S'2010-12-14 16:08'
p9368
sg100
g139
sg102
g27
sg86
g27
sa(dp9369
g91
S'Lent'
p9370
sg93
S'2011'
p9371
sg95
S'To complete and submit prep for all subjects each week as required. '
p9372
sg81
S'M '
p9373
sg98
g27
sg87
S'2012-04-04 16:08'
p9374
sg100
g101
sg102
g27
sg86
g27
sa(dp9375
g91
S'Mich'
p9376
sg93
S'2012'
p9377
sg95
S'To organise himslef during morning form class to ensure he has all of the necessary equipment for lessons.'
p9378
sg81
S'M '
p9379
sg98
g27
sg87
S'2012-07-19 13:42'
p9380
sg100
g101
sg102
g27
sg86
g27
sa(dp9381
g91
S'Lent'
p9382
sg93
S'2013'
p9383
sg95
S'To increase the amount of work produced in lessons and complete tasks in the time given by teachers. (85%). '
p9384
sg81
S'M '
p9385
sg98
g27
sg87
S'2012-07-19 13:42'
p9386
sg100
g101
sg102
g27
sg86
g27
sa(dp9387
g91
S'Summer'
p9388
sg93
S'2013'
p9389
sg95
S'To focus on my learning and ensure I complete all tasks to the best of my ability. '
p9390
sg81
S'M '
p9391
sg98
g27
sg87
S'2012-07-19 13:42'
p9392
sg100
g101
sg102
g27
sg86
g27
sasg36
(lp9393
(dp9394
g91
S'Michaelmas'
p9395
sg81
S'M '
p9396
sg87
S'2010-12-14 16:08'
p9397
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p9398
sg52
S'2010'
p9399
sg148
S'weekly(x1)'
p9400
sg86
g27
sg150
g27
sa(dp9401
g91
S'Lent'
p9402
sg81
S'M '
p9403
sg87
S'2010-12-14 16:08'
p9404
sg145
S'1:1 Learning Specialist Lindsey Copeman.'
p9405
sg52
S'2011'
p9406
sg148
S'weekly(x1)'
p9407
sg86
g27
sg150
g27
sassssS'Hughie_Haden'
p9408
(dp9409
g3
(dp9410
g5
(dp9411
g7
I0
sg8
(lp9412
g184
ag185
asg12
S'Sen provision map for Hughie Haden'
p9413
sg14
g15
sg16
(lp9414
g18
ag19
asg20
g21
sg22
S'Special Needs Provision Map for Hughie Haden'
p9415
sg24
I0
sg25
(lp9416
g190
ag191
asg29
(lp9417
g27
ag31
asg32
S'SEN,Provision map,Hughie Haden'
p9418
sg34
(lp9419
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp9420
g41
S'16-02-2011'
p9421
sg43
S'Haden'
p9422
sg45
S'No Provision'
p9423
sg47
S'2005-06-20'
p9424
sg49
S'Observations completed by KAM 16/02/2011\r<br>Observation completed by NB 14/03/2011'
p9425
sg51
g27
sg52
g53
sg54
g27
sg56
g27
sg58
g27
sg59
S'Hughie'
p9426
sg61
g27
sg62
S'3CS'
p9427
ssg64
I0
sg65
I0
sg66
(lp9428
g207
ag208
asg70
g392
sg72
I0
sg73
(lp9429
g211
ag212
asssg76
(dp9430
g5
(dp9431
g37
(lp9432
(dp9433
g91
S'Lent'
p9434
sg93
S'2013'
p9435
sg95
S'1. To ask a teacher to help him when he has lots write.                                                                                  '
p9436
sg81
S'M '
p9437
sg98
g27
sg87
S'2013-07-16 09:52'
p9438
sg100
g101
sg102
g27
sg86
g27
sa(dp9439
g91
S'Lent'
p9440
sg93
S'2013'
p9441
sg95
S'2.To sound out word accurately.                                                                         '
p9442
sg81
S'M '
p9443
sg98
g27
sg87
S'2013-07-16 09:52'
p9444
sg100
g101
sg102
g27
sg86
g27
sa(dp9445
g91
S'Summer'
p9446
sg93
S'2013'
p9447
sg95
S'1. To continue to use high frequency word list and have a go book .                                                      '
p9448
sg81
S'M '
p9449
sg98
g27
sg87
S'2013-07-16 09:53'
p9450
sg100
g101
sg102
g27
sg86
g27
sa(dp9451
g91
S'Summer'
p9452
sg93
S'2013'
p9453
sg95
S'2. To complete a task in a specified time using appropriate egg timer provided by JHA.                                                            '
p9454
sg81
S'M '
p9455
sg98
g27
sg87
S'2013-07-16 09:53'
p9456
sg100
g101
sg102
g27
sg86
g27
sa(dp9457
g91
S'Mich'
p9458
sg93
S'2013'
p9459
sg95
S'English - To identify and edit at least 3 spelling mistakes from pieces of written work.                                                                         '
p9460
sg81
S'M '
p9461
sg98
g27
sg87
S'2013-07-16 09:54'
p9462
sg100
g139
sg102
g27
sg86
g27
sa(dp9463
g91
S'Mich'
p9464
sg93
S'2013'
p9465
sg95
S'2. Ensure all sentences have full stops and capital letters'
p9466
sg81
S'M '
p9467
sg98
g27
sg87
S'2013-07-16 09:54'
p9468
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp9469
(dp9470
g91
S'Michaelmas'
p9471
sg81
S'M '
p9472
sg87
S'2013-07-08 10:16'
p9473
sg145
S'Spelling support group'
p9474
sg52
S'2012'
p9475
sg148
S'weekly(x1)'
p9476
sg86
g27
sg150
S'KM'
p9477
sa(dp9478
g91
S'Lent'
p9479
sg81
S'M '
p9480
sg87
S'2013-07-08 10:16'
p9481
sg145
S'Spelling support group'
p9482
sg52
S'2013'
p9483
sg148
S'weekly(x1)'
p9484
sg86
g27
sg150
S'KM'
p9485
sa(dp9486
g91
S'Summer'
p9487
sg81
S'M '
p9488
sg87
S'2013-07-08 10:16'
p9489
sg145
S'Spelling support group'
p9490
sg52
S'2013'
p9491
sg148
S'weekly(x1)'
p9492
sg86
g27
sg150
S'KM'
p9493
sa(dp9494
g91
S'Michaelmas'
p9495
sg81
S'M '
p9496
sg87
S'2013-07-08 10:16'
p9497
sg145
S'Weekly handwriting support'
p9498
sg52
S'2012'
p9499
sg148
S'weekly(x1)'
p9500
sg86
g27
sg150
S'KM'
p9501
sa(dp9502
g91
S'Lent'
p9503
sg81
S'M '
p9504
sg87
S'2013-07-08 10:16'
p9505
sg145
S'Weekly handwriting support'
p9506
sg52
S'2013'
p9507
sg148
S'weekly(x1)'
p9508
sg86
g27
sg150
S'KM'
p9509
sa(dp9510
g91
S'Summer'
p9511
sg81
S'M '
p9512
sg87
S'2013-07-08 10:16'
p9513
sg145
S'Weekly handwriting support'
p9514
sg52
S'2013'
p9515
sg148
S'weekly(x1)'
p9516
sg86
g27
sg150
S'KM'
p9517
sa(dp9518
g91
S'Summer'
p9519
sg81
S'M '
p9520
sg87
S'2013-07-08 10:16'
p9521
sg145
S'Guided reading group'
p9522
sg52
S'2013'
p9523
sg148
S'weekly(x1)'
p9524
sg86
g27
sg150
S'NB'
p9525
sa(dp9526
g91
S'Lent'
p9527
sg81
S'M '
p9528
sg87
S'2013-07-08 10:16'
p9529
sg145
S'Guided reading group'
p9530
sg52
S'2013'
p9531
sg148
S'weekly(x1)'
p9532
sg86
g27
sg150
S'NB'
p9533
sa(dp9534
g91
S'Michaelmas'
p9535
sg81
S'M '
p9536
sg87
S'2013-07-08 10:16'
p9537
sg145
S'Guided reading group'
p9538
sg52
S'2012'
p9539
sg148
S'weekly(x1)'
p9540
sg86
g27
sg150
S'NB'
p9541
sassssS'Ella_Wilkinson'
p9542
(dp9543
g3
(dp9544
g5
(dp9545
g7
I0
sg8
(lp9546
g1548
ag1549
asg12
S'Sen provision map for Ella Wilkinson'
p9547
sg14
g15
sg16
(lp9548
g27
ag2032
asg20
g21
sg22
S'Special Needs Provision Map for Ella Wilkinson'
p9549
sg24
I0
sg25
(lp9550
g27
ag1834
asg29
(lp9551
g2805
ag27
asg32
S'SEN,Provision map,Ella Wilkinson'
p9552
sg34
(lp9553
g36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
ag36
ag37
ag38
asg39
(dp9554
g41
S'12/06/2009'
p9555
sg43
S'Wilkinson'
p9556
sg45
S'Action +'
p9557
sg47
S'2002-03-07'
p9558
sg49
S'EP Report Nina Elliott'
p9559
sg51
g27
sg52
g1564
sg54
g27
sg56
S'Ella has difficulty with speed of processing and her working memory is very weak. At times Ella may display poor concentration and attention.  When writing she has directional confusion. Ella is Dyslexic with some co-ordination difficulties which are impacting on both literacy and numeracy.  VCI 108 PRI 102 WMI 71 PSI 78'
p9560
sg58
g27
sg59
S'Ella'
p9561
sg61
S'25%'
p9562
sg62
S'6CS'
p9563
ssg64
I0
sg65
I0
sg66
(lp9564
S'Ella is Dyslexic'
p9565
aS'Ella should be heard to read every day.  Check understanding of vocabulary when introducing new concepts or topic and praise sustained attention and active participation in class. Where possible minimise copying by giving pre-prepared worksheets. Be aware that Ella will not process information as quickly as others and may not be able to retain lengthy amounts of information, therefore limit the amount of information presented and allow additional time to complete tasks.'
p9566
asg70
S'12-06-2009'
p9567
sg72
I0
sg73
(lp9568
g2817
ag2818
asssg76
(dp9569
g5
(dp9570
g38
(lp9571
(dp9572
g81
S'F '
p9573
sg83
S'Michaelmas Report - George Scott Kerr. '
p9574
sg85
g27
sg86
g27
sg87
S'2010-12-19 15:10'
p9575
sa(dp9576
g81
S'F '
p9577
sg83
S'NB, AP, ET and Mrs Wilkinson. Review meeting. '
p9578
sg85
g27
sg86
g27
sg87
S'2011-07-11 15:10'
p9579
sa(dp9580
g81
S'F '
p9581
sg83
S'Summer Report - George Scott Kerr. '
p9582
sg85
g27
sg86
g27
sg87
S'2011-07-13 15:10'
p9583
sa(dp9584
g81
S'F '
p9585
sg83
S'NB and Mr and Mrs Wilkinson meeting to discuss start to Year 5 and future options for Ella. '
p9586
sg85
S'02-11-2011'
p9587
sg86
g27
sg87
S'2011-11-02 15:25'
p9588
sa(dp9589
g81
S'F '
p9590
sg83
S'NB, SAC Mrs Wilkinson. Review of progress. '
p9591
sg85
g27
sg86
g27
sg87
S'2012-05-21 13:19'
p9592
sa(dp9593
g81
S'F '
p9594
sg83
S'NB, FC and Mrs Wilkinson. Review of progress. '
p9595
sg85
g27
sg86
g27
sg87
S'2012-06-01 13:18'
p9596
sa(dp9597
g81
S'F '
p9598
sg83
S'NB, SAC, Mr and Mrs Wilkinson. Review of progress. '
p9599
sg85
g27
sg86
g27
sg87
S'2012-06-27 13:20'
p9600
sa(dp9601
g81
S'F '
p9602
sg83
S'Review of progress. NB, Mr and Mrs Wilkinson.'
p9603
sg85
g27
sg86
g27
sg87
S'2012-11-02 13:17'
p9604
sasg37
(lp9605
(dp9606
g91
S'Lent'
p9607
sg93
S'2011'
p9608
sg95
S'To focus on the task at hand and think about what is required to achieve the lesson objective.'
p9609
sg81
S'F '
p9610
sg98
g27
sg87
S'2010-12-13 15:25'
p9611
sg100
g101
sg102
g27
sg86
g27
sa(dp9612
g91
S'Mich'
p9613
sg93
S'2011'
p9614
sg95
S'To focus on the task at hand and think about what is required to achieve the lesson objective.'
p9615
sg81
S'F '
p9616
sg98
g27
sg87
S'2011-10-10 15:25'
p9617
sg100
g101
sg102
g27
sg86
g27
sa(dp9618
g91
S'Lent'
p9619
sg93
S'2012'
p9620
sg95
S'To increase the amount of work she completes independently following teacher instruction and support.'
p9621
sg81
S'F '
p9622
sg98
g27
sg87
S'2012-02-27 15:25'
p9623
sg100
g101
sg102
g27
sg86
g27
sa(dp9624
g91
S'Mich'
p9625
sg93
S'2012'
p9626
sg95
S'To consistently produce amounts of work independently following teacher instruction and support.'
p9627
sg81
S'F '
p9628
sg98
g27
sg87
S'2012-07-19 13:21'
p9629
sg100
g101
sg102
g27
sg86
g27
sa(dp9630
g91
S'Lent'
p9631
sg93
S'2013'
p9632
sg95
S'To focus and concentrate for 10 minute blocks in lessons. '
p9633
sg81
S'F '
p9634
sg98
g27
sg87
S'2012-07-19 13:21'
p9635
sg100
g101
sg102
g27
sg86
g27
sa(dp9636
g91
S'Summer'
p9637
sg93
S'2013'
p9638
sg95
S'To be willing to take risks with my learning and attempt tasks independently once I understand the instructions. (50% of the time)'
p9639
sg81
S'F '
p9640
sg98
g27
sg87
S'2012-07-19 13:21'
p9641
sg100
g101
sg102
g27
sg86
g27
sa(dp9642
g91
S'Mich'
p9643
sg93
S'2013'
p9644
sg95
S'To increase the depth and sophistication of my answers in written and spoken form. '
p9645
sg81
S'F '
p9646
sg98
g27
sg87
S'2013-07-16 11:04'
p9647
sg100
g139
sg102
g27
sg86
g27
sasg36
(lp9648
(dp9649
g91
S'Michaelmas'
p9650
sg81
S'F '
p9651
sg87
S'2009-05-13 16:43'
p9652
sg145
S'Small group spelling support focusing on spelling strategies.'
p9653
sg52
S'2010'
p9654
sg148
S'weekly(x1)'
p9655
sg86
g27
sg150
S'NB'
p9656
sa(dp9657
g91
S'Michaelmas'
p9658
sg81
S'F '
p9659
sg87
S'2009-05-13 16:43'
p9660
sg145
S'Reading comprehension support group.'
p9661
sg52
S'2010'
p9662
sg148
S'weekly(x1)'
p9663
sg86
g27
sg150
S'KM'
p9664
sa(dp9665
g91
S'Lent'
p9666
sg81
S'F '
p9667
sg87
S'2009-05-13 16:43'
p9668
sg145
S'Small group spelling support focusing on spelling strategies.'
p9669
sg52
S'2011'
p9670
sg148
S'weekly(x1)'
p9671
sg86
g27
sg150
S'NB'
p9672
sa(dp9673
g91
S'Lent'
p9674
sg81
S'F '
p9675
sg87
S'2009-05-13 16:43'
p9676
sg145
S'Reading comprehension support group.'
p9677
sg52
S'2011'
p9678
sg148
S'weekly(x1)'
p9679
sg86
g27
sg150
S'KM'
p9680
sa(dp9681
g91
S'Michaelmas'
p9682
sg81
S'F '
p9683
sg87
S'2010-12-13 15:22'
p9684
sg145
S'1:1 Learning Specialist George Scott Kerr. '
p9685
sg52
S'2010'
p9686
sg148
S'weekly(x1)'
p9687
sg86
g27
sg150
g27
sa(dp9688
g91
S'Summer'
p9689
sg81
S'F '
p9690
sg87
S'2010-12-13 15:22'
p9691
sg145
S'1:1 Learning Specialist Claire Cooper. '
p9692
sg52
S'2010'
p9693
sg148
S'weekly(x1)'
p9694
sg86
g27
sg150
g27
sa(dp9695
g91
S'Lent'
p9696
sg81
S'F '
p9697
sg87
S'2010-12-13 15:22'
p9698
sg145
S'1:1 Learning Specialist Claire Cooper. '
p9699
sg52
S'2010'
p9700
sg148
S'weekly(x1)'
p9701
sg86
g27
sg150
g27
sa(dp9702
g91
S'Lent'
p9703
sg81
S'F '
p9704
sg87
S'2010-12-13 15:22'
p9705
sg145
S'1:1 Learning Specialist George Scott Kerr.'
p9706
sg52
S'2011'
p9707
sg148
S'weekly(x1)'
p9708
sg86
g27
sg150
g27
sa(dp9709
g91
S'Summer'
p9710
sg81
S'F '
p9711
sg87
S'2010-12-13 15:23'
p9712
sg145
S'Individual reading with Jodie Wood. '
p9713
sg52
S'2010'
p9714
sg148
S'weekly(x1)'
p9715
sg86
g27
sg150
g27
sa(dp9716
g91
S'Summer'
p9717
sg81
S'F '
p9718
sg87
S'2011-05-12 16:43'
p9719
sg145
S'Small group spelling support focusing on spelling strategies.'
p9720
sg52
S'2011'
p9721
sg148
S'weekly(x1)'
p9722
sg86
g27
sg150
S'NB'
p9723
sa(dp9724
g91
S'Summer'
p9725
sg81
S'F '
p9726
sg87
S'2011-05-13 15:22'
p9727
sg145
S'1:1 Learning Specialist George Scott Kerr.'
p9728
sg52
S'2011'
p9729
sg148
S'weekly(x1)'
p9730
sg86
g27
sg150
g27
sa(dp9731
g91
S'Summer'
p9732
sg81
S'F '
p9733
sg87
S'2011-05-13 16:43'
p9734
sg145
S'Reading comprehension support group.'
p9735
sg52
S'2011'
p9736
sg148
S'weekly(x1)'
p9737
sg86
g27
sg150
S'AST'
p9738
sa(dp9739
g91
S'Michaelmas'
p9740
sg81
S'F '
p9741
sg87
S'2011-09-01 15:22'
p9742
sg145
S'1:1 Learning Specialist George Scott Kerr.'
p9743
sg52
S'2011'
p9744
sg148
S'weekly(x1)'
p9745
sg86
g27
sg150
g27
sa(dp9746
g91
S'Lent'
p9747
sg81
S'F '
p9748
sg87
S'2012-01-05 15:22'
p9749
sg145
S'1:1 Learning Specialist George Scott Kerr.'
p9750
sg52
S'2012'
p9751
sg148
S'weekly(x1)'
p9752
sg86
g27
sg150
g27
sa(dp9753
g91
S'Summer'
p9754
sg81
S'F '
p9755
sg87
S'2012-04-05 15:22'
p9756
sg145
S'1:1 Learning Specialist Sarah McKinlay.'
p9757
sg52
S'2012'
p9758
sg148
S'weekly(x1)'
p9759
sg86
g27
sg150
g27
sa(dp9760
g91
S'Michaelmas'
p9761
sg81
S'F '
p9762
sg87
S'2012-04-05 15:22'
p9763
sg145
S'1:1 Learning Specialist Sarah McKinlay.'
p9764
sg52
S'2012'
p9765
sg148
S'weekly(x1)'
p9766
sg86
g27
sg150
g27
sa(dp9767
g91
S'Lent'
p9768
sg81
S'F '
p9769
sg87
S'2012-04-05 15:22'
p9770
sg145
S'1:1 Learning Specialist LC'
p9771
sg52
S'2013'
p9772
sg148
S'weekly(x1)'
p9773
sg86
g27
sg150
g27
sa(dp9774
g91
S'Summer'
p9775
sg81
S'F '
p9776
sg87
S'2012-04-05 15:22'
p9777
sg145
S'1:1 Learning Specialist LC'
p9778
sg52
S'2013'
p9779
sg148
S'weekly(x1)'
p9780
sg86
g27
sg150
g27
sassss.