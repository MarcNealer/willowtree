from MIS.models import Pupil
from MIS.models import PupilGroup
from MIS.models import Family
import csv

class duplicatePupils():
    ''' combines duplicate pupils into one pupil from the academic year
    2014-2015 upwards '''
    def __init__(self):
        self.logFile = open('duplicatePupils.log', 'w')
        self.notThisYear = "2013-2014"
        self.dupList = self.__makeDupList__(verbose=True)
        for i in self.dupList:
            self.__mergePupils__(i, verbose=True, notATest=True)
        self.logFile.write('<------------- deleting familes ------------->\n')
        self.deleteSpareFamilies()
        self.logFile.close()
        return

    def deleteSpareFamilies(self):
        ''' deletes spare families '''
        logFile = open('deleteSpareFamiles.log', 'w')
        for i in Family.objects.all():
            if bool(i.familychildren_set.filter()) is not True:
                logFile.write("family %s with an Id of %s deleted" % (i.FamilyName,
                                                                      i.Id))
                temp = i
                self.logFile.write('deleting family: %s with an id of %s') % (i.FamilyName,
                                                                              i.id)
                temp.delete()
                logFile.close()
        return

    def __makeDupList__(self, verbose=False):
        ''' creates main duplication list '''
        mainWriteList = set()
        pupilObjs = Pupil.objects.filter(Active=True)
        #pupilObjs = Pupil.objects.filter(Forename="Alice", Active=True)
        if verbose is True:
             print "getting duplicated pupil records..."
        for pupil in pupilObjs:
            dup = 0
            for i in Pupil.objects.filter(Active=True):
                if (pupil.Forename == i.Forename) and (pupil.Surname == i.Surname) and (pupil.DateOfBirth == i.DateOfBirth):
                    dup += 1
            if dup > 1:
                groups = PupilGroup.objects.filter(Pupil__id=pupil.id,
                                                   Active=True)
                groupList = ""
                addToList = False
                tempWriteList = ""
                for group in groups:
                    if (group.AcademicYear != self.notThisYear and "Current" not in group.Group.Name):
                        addToList = True
                        groupList += "%s--(%s)," % (group.Group.Name,
                                                    group.AcademicYear)
                if addToList == True:
                    tempWriteList = '%s,%s,%s,%s,%s' % (pupil.id,
                                                       pupil.Forename,
                                                       pupil.Surname,
                                                       pupil.DateOfBirth.isoformat(),
                                                       str(groupList))
                mainWriteList.add(tempWriteList)
        return mainWriteList

    def __mergePupils__(self, pupilString, verbose=False, notATest=False):
        ''' merges Kindergarten pupils to their Applicant duplicates '''
        temp = pupilString.split(',')
        dups = []
        try:
            if "Kindergarten" not in temp[4]:
                for i in self.dupList:
                    if ((temp[1] in i) and (temp[2] in i) and (temp[3] in i) and temp[0] not in i):
                        dups.append(i)
        except:
            print "Problem with string: %s" % temp
        for i in dups:
            temp2 = i.split(',')
            if verbose is True:
                if "Kindergarten" in temp2[4]:
                    tup = (temp2[0], temp2[1], temp2[2], temp2[4], temp[0], temp[1], temp[2], temp[4])
                    self.logFile.write("MERGING ---> %s:%s %s with the groups %s to %s:%s %s with the groups %s\n" % tup)
                    print "MERGING ---> %s:%s %s with the groups %s to %s:%s %s with the groups %s" % tup
            if notATest is True:
                try:
                    if "Kindergarten" in temp2[4]:
                        pupilObj = Pupil.objects.get(id=int(temp[0]))
                        pupilGroupsObjs = PupilGroup.objects.filter(Pupil__id=int(temp2[0]),Active=True)
                        for group in pupilGroupsObjs:
                            tempGroup = group
                            tempGroup.Pupil = pupilObj
                            tempGroup.save()
                        deletePupil = Pupil.objects.get(id=int(temp2[0]))
                        deletePupil.delete()
                except:
                    print "problem getting pupil record %s, perhaps has already been deleted by process..." % temp[0]
        return
