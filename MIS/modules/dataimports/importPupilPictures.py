'''
example code...

In [3]: exit
login@ba-willowtree:~/Dropbox/Projects/WillowTree/MIS/modules/dataimports$ python ~/Dropbox/Projects/WillowTree/manage.py shell

In [1]: from MIS.modules.dataimports.importPupilPictures import importPicturesIntoWillowTree

In [2]: importPicturesIntoWillowTree('Clapham', '2013-2014', '/home/login/Dropbox/Cla_one', '/home/media/WillowTree/images/StudentPhotos')

'''




"""

Created on Thu Aug  1 13:12:18 2013
@author: roy

import pictures into WillowTree scripts

"""
#Django imports
from django.core.cache import cache
from django.core.files.storage import default_storage as storage

# WillowTree Imports
from MIS.models import Pupil
from MIS.modules.PupilRecs import PupilRecord

#Python Imports
import os


class prepPupilPictures():
    '''
    Prep pupil pictures to be imported into willowTree,
    1) Get them into one directory
    2) resize them to correct size
    '''
    def __init__(self, rootDir, targetDir):
        self.__getPictureFiles__(rootDir)
        self.__movePicturesToNewDir__(targetDir)
        self.__resizePicturesForWillowTree__(targetDir)

    def __getPictureFiles__(self, rootDir):
        ''' this gets all the pupil pictures in a root dir recursively'''
        pictureExtention = '.jpg'
        self.files = []
        for i in os.listdir(rootDir):
            if os.path.isdir('%s/%s' % (rootDir, i)):
                for picture in os.listdir('%s/%s' % (rootDir, i)):
                    if os.path.isfile('%s/%s/%s' % (rootDir, i, picture)) and pictureExtention in picture:
                        self.files.append('%s/%s/%s' % (rootDir, i, picture))
        return self.files

    def __movePicturesToNewDir__(self, targetDir):
        ''' move pictures in the self.files list to a new dir '''
        s3_store = storage.open(self.PupilRec.Picture.file.name, "w")
        #image_format = self.PupilRec.Picture.file.name
        image.save(s3_store)
        s3_store.close()

        #os.system('mkdir %s' % targetDir)
        #for picture in self.files:
        #    os.system('cp "%s" "%s"' % (picture, targetDir))
        #return

    def __resizePicturesForWillowTree__(self, targetDir):
        ''' resizes images for WillowTree (135*180) '''
        try:
            for picture in os.listdir(targetDir):
                pathToPicture = '%s/%s' % (targetDir, picture)
                os.system('convert "%s" -resize 135x180 "%s"' % (pathToPicture,
                                                                 pathToPicture))
        except Exception as error:
            print '''problem convert pictures: please run "sudo apt-get install
                imagemagick" on this machine'''
        return

###############################################################################


class importPicturesIntoWillowTree():
    '''

    for importing preped pictures into WillowTree

    NOTE: singleImport is the full path of the picture in the inception dir

    targetDir on live server = "/home/media/WillowTree/media/images/StudentPhotos"

    '''
    def __init__(self, school, acYear, inceptionDir, targetDir,
                 singleImport=False):
        self.pathForDjango = 'images/StudentPhotos/'
        self.singleImport = singleImport
        self.school = school
        self.acYear = acYear
        self.inceptionDir = inceptionDir
        self.targetDir = targetDir
        self.school = school
        self.logFile = open('importingPupilPicturesFor%s.log' % school, 'w')
        self.__mainImportScript__()


    def __getPupilId__(self, filePath):
        ''' get pupil record from file path '''
        error = False
        try:
            foreName = filePath.split('/')[-1].split(' ')[0]
            foreName = foreName.strip(',')
        except:
            error = True
            errorCode = 'error 1'
        try:
            # standard
            #surName = filePath.split('/')[-1].split(' ')[1:]
            #surName = ' '.join(surName)
            #surName = surName.strip('.jpg')
            #surName = surName.strip('.JPG')
            #surName = surName.strip(',')

            # one off
            surName = filePath.split('/')[-1].split(' ')[1]
        except:
            error = True
            errorCode = 'error 2'
        if not error:
            try:
                #print foreName, surName

                # CHECK FOR FIRST NAME MATCHING
                if Pupil.objects.filter(Forename=foreName.replace('.jpg', ''),
                                        Surname=surName.replace('.jpg', ''),
                                        Active=True).exists():
                    pupils = Pupil.objects.filter(Forename=foreName.replace('.jpg', ''),
                                                  Surname=surName.replace('.jpg', ''),
                                                  Active=True)

                # CHECK FOR NICK NAME MATCHING
                elif Pupil.objects.filter(NickName=foreName.replace('.jpg', ''),
                                          Surname=surName.replace('.jpg', ''),
                                          Active=True).exists():
                    pupils = Pupil.objects.filter(NickName=foreName.replace('.jpg', ''),
                                                  Surname=surName.replace('.jpg', ''),
                                                  Active=True)

                #print pupils
                for pupil in pupils:
                    currentPupilTemp = PupilRecord(pupil.id, self.acYear)
                    if self.school[0].upper() in currentPupilTemp.Form() and currentPupilTemp.PupilStatus() != 'Alumni':
                        self.currentPupil = currentPupilTemp
                        #print self.currentPupil.Pupil.id
            except:
                    error = True
                    errorCode = 'error 4'
                    #print 'ERRRROR'
        if error:
            self.logFile.write('''Problem with: %s --> error code: %s\n''' % (filePath.split('/')[-1],
                                                                              errorCode))
        return

    def __movePictureToTargetDir__(self, filePath):
        ''' moves a picture file to the target dir '''
        self.newTargetFilePath = '%s/%s' % (self.targetDir,
                                            filePath.split('/')[-1])
        os.system('cp "%s" "%s"' % (filePath, self.newTargetFilePath))
        return

    def __changePicture__(self, filePath):
        ''' change the picture of a pupil to new file '''
        self.currentPupil.Pupil.Picture.name = '%s%s' % (self.pathForDjango,
                                                         filePath.split('/')[-1])
        #print '%s%s' % (self.pathForDjango, filePath.split('/')[-1])
        self.currentPupil.Pupil.save()
        cache.delete('Pupil_%s' % self.currentPupil.Pupil.id)
        return

    def __mainImportScript__(self):
        ''' main loop to import pupil pupil pictures '''
        if self.singleImport:
            self.__getPupilId__(self.singleImport)
            self.__movePictureToTargetDir__(self.singleImport)
            self.__changePicture__(self.newTargetFilePath)
            self.logFile.write('Pupil %s picture changed to %s\n' % (self.currentPupil.Pupil.id,
                                                                     self.newTargetFilePath))
        else:
            counter = 0
            lengthOfJob = len(os.listdir(self.inceptionDir))
            for picturePath in os.listdir(self.inceptionDir):
                self.newTargetFilePath = False
                self.currentPupil = False
                self.__getPupilId__(picturePath)
                self.__movePictureToTargetDir__('%s/%s' % (self.inceptionDir,
                                                           picturePath))
                if self.newTargetFilePath and self.currentPupil:
                    self.__changePicture__(self.newTargetFilePath)
                    self.logFile.write('Pupil %s picture changed to %s\n' % (self.currentPupil.Pupil.id,
                                                                             self.newTargetFilePath))
                else:
                    self.logFile.write('ERROR: Problem importing picture %s\n' % picturePath)
                    #print 'error on __mainImportScript__()'
                    #print self.newTargetFilePath
                    #print self.currentPupil
                    #print '\r'
                counter += 1
                print 'working --> %s of %s' % (counter, lengthOfJob)
        self.logFile.close()
        return
