# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 15:55:49 2013

@author: marc
"""

import csv,datetime
from MIS.models import *
from MIS.modules.PupilRecs import *

def importFutureSchools(SchoolList, RecordList):
    mystaff=Staff.objects.get(id=2358)
    mykeywords='Communication:Future Schools'
    schooldetails={}
    schoolitems = csv.DictReader(open(SchoolList, 'rb'))
    for items in schoolitems:
        schooldetails[items['SchoolCode']]=items['Type']
        
    schoolitems = csv.DictReader(open(RecordList, 'rb'))
    for items in schoolitems:
        if items['Code']:
            if 'Secondary' in schooldetails[items['Code']]:
                pupilrec=Pupil.objects.filter(Old_Id=items['Id'])
                if pupilrec.exists():
                    
                    notetext=''
                    notetext='Principal %s <br>' % items['Principal']
                    notetext+=items['Name']
                    notetext+='<br>'
                    notetext+=items['Full address']
                    notetext+='<br>'
                    notetext+=items['Postcode']
                    notetext+='<br>'
                    notetext+=items['Email']
                    if items['Senior Registered'] == 1:
                        notetext+='Registered:True <br>'
                    if items['Senior Scholarship'] == 1:
                        notetext+='Scholarship:True <br>'            
                    notetext+='Entry Date: %s <br>' % items['EntryDate']
                    notetext+='School Test Date: %s <br>' % items['School Test Date']
                    notetext+='Startdate: %s' % items['Startdate']
                    
                    newnote=NoteDetails(Keywords=mykeywords,
                                        NoteText=notetext,
                                        RaisedBy=mystaff,
                                        RaisedDate=datetime.datetime.today(),
                                        ModifiedBy=mystaff,
                                        ModifiedDate=datetime.datetime.today())
                    newnote.save()
                
            
                    newpupilnote=PupilNotes(PupilId=pupilrec[0],NoteId=newnote)
                    newpupilnote.save()