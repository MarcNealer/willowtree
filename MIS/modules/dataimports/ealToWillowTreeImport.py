'''
HOW TO USE -->

1) Firstly run the "EAL_import_to_WillowTree.py" program on all four Facility
   servers.

2) This will save .p files to the "/home/login/Dropbox/ealToWillowTree" folder.
   now open a django's ipython shell on the server you wish to import to and
   run this program for all four .p files.

3) Remeber to include a school when running this program, Or the log files will
   overwrite eachother.

4) use the individualImport option, specifying a student's name in this format:
   'Roy_Hanley' - use x.keys() if needed.
'''

# python imports
import pickle
import datetime
import re

# WillowTree imports
from MIS.models import Pupil
from MIS.models import PupilAlert
from MIS.models import Staff
from MIS.models import AlertType
from MIS.models import PupilProvision
from MIS.models import NoteDetails
from MIS.models import FamilyContact
from MIS.models import PupilNotes
from MIS.models import FamilyChildren
from MIS.modules.ExtendedRecords import ExtendedRecord

# Django imports
from django.core.cache import cache

class run():
    ''' this code is to be used in conjuction with the 'EAL_to_WillowTree'
    scripts on the Facility servers. Does what it says on the tin - roy '''
    def __init__(self,
                 importFile,
                 school=False,
                 individualImportPupilName=False,
                 individualImportPupilId=False):
        if individualImportPupilName:
            self.individualImportPupilName = individualImportPupilName
        if individualImportPupilId:
            self.individualImportPupilId = individualImportPupilId
        self.todaysDate = self.__todaysDate__()
        self.me = Staff.objects.get(EmailAddress="RHanley@Thomas-s.co.uk")
        if school:
            self.logFile = open('ealToWillowTreeImport%s.log' % school, 'w')
        else:
            self.logFile = open('ealToWillowTreeImport.log', 'w')
        self.senObj = pickle.load(open(importFile, 'rb'))
        jobLength = len(self.senObj)
        counter = 0
        if self.individualImportPupilId:
            for k, v in self.senObj[self.individualImportPupilName]:
                test = self.__getPupilObjOrError__(v)
                if test != "error":
                    pass
                    self.pupilExtentionRecord = self.__extRecord__()
                    self.__importActiveAlert__(v)
                    self.__importGeneral__(v)
                    self.__importAssessmentHistory__(v)
                    self.__importLanguageDetails__(v)
                    self.__importFurtherLanguageDetails__(v)
                    self.__importProvisions__(v)
                    self.__importAreasOfConcern__(v)
                    self.__importNotes__(v)
                    cache.delete('PupilEAL_%s' % self.willowPupilObj.id)
                    counter += 1
                    print jobLength - counter
        else:
            for k, v in self.senObj.iteritems():
                test = self.__getPupilObjOrError__(v)
                if test != "error":
                    pass
                    self.pupilExtentionRecord = self.__extRecord__()
                    self.__importActiveAlert__(v)
                    self.__importGeneral__(v)
                    self.__importAssessmentHistory__(v)
                    self.__importLanguageDetails__(v)
                    self.__importFurtherLanguageDetails__(v)
                    self.__importProvisions__(v)
                    self.__importAreasOfConcern__(v)
                    self.__importNotes__(v)
                    cache.delete('PupilEAL_%s' % self.willowPupilObj.id)
                    counter += 1
                    print jobLength - counter
        self.logFile.close()

###############################################################################
# helper methods
###############################################################################
    def __todaysDate__(self):
        now = datetime.datetime.now()
        return now.strftime("%Y-%m-%d")

    def __extRecord__(self):
        return ExtendedRecord(BaseType='Pupil',
                              BaseId=self.willowPupilObj.id)

    def __getPupilObjOrError__(self, facilityPupilObj):
        ''' checks if there is only one pupil with the same forename, surname
        and DOB as the pupil in the facilityPupilObj in WillowTree. If there
        is more than one a dupilcate error is returned '''
        if self.individualImportPupilId:
            self.willowPupilObj = Pupil.objects.get(id=int(self.individualImportPupilId))
        else:
            forename = facilityPupilObj['changeList']['Default']['SENdata']['Forename']
            surname = facilityPupilObj['changeList']['Default']['SENdata']['Surname']
            dob = facilityPupilObj['changeList']['Default']['SENdata']['DOB']
            test = Pupil.objects.filter(Forename=forename,
                                        Surname=surname,
                                        DateOfBirth=dob).count()
            if test == 0:
                self.tup = (forename, surname, dob)
                self.logFile.write('error: no pupil of %s %s, dob:%s exists\n' % self.tup)
                return 'error'
            elif test > 1:
                self.tup = (forename, surname, dob)
                self.logFile.write('error: duplicates of pupil %s %s, dob:%s exist\n' % self.tup)
                return 'error'
            elif test == 1:
                self.tup = (forename, surname, dob)
                self.logFile.write('Importing sen records for --> %s %s, dob:%s\n' % self.tup)
                self.willowPupilObj = Pupil.objects.get(Forename=forename,
                                                        Surname=surname,
                                                        DateOfBirth=dob)
                return self.willowPupilObj

    def __helperForAreasOfConcern__(self, data, mode):
        if mode == 'notes':
            t = data[0]
        else:
            t = data[1]
        dataString = ''
        try:
            t = unicode(t)
            if t != '':
                dataString += "<p>%s</p>" % t
        except:
            dataString = '<p><<<--- unable to import --->>></p>'
        return dataString

    def __convertDate__(self, oldDate):
        regTest = False
        try:
            if "/" in oldDate:
                oldDate = oldDate.split('/')
            elif "-" in oldDate:
                oldDate = oldDate.split('-')
            if len(oldDate[2]) == 2:
                oldDate[2] = "20%s" % oldDate[2]
            newDate = "%s-%s-%s" % (oldDate[2], oldDate[1], oldDate[0])
            regTest = re.match(newDate, "^\d{4}-\d{2}-\d{2}$")
            if regTest:
                return newDate
            else:
                return self.todaysDate
        except:
            return self.todaysDate

    def __convertToUniCode__(self, string):
        for i in ['\x90', '\x91', '\x92' ,'\x93', '\x94',
                  '\x95','\x96', '\x97', '\x98', '\x99']:
            string = string.replace(i, '')
        try:
            string = unicode(string)
        except:
            string = "<p><<<--- unable to import --->>></p>"
        return string

###############################################################################
# getting data from picked object created from Facility_DB.py
###############################################################################
    def __getActiveAlert__(self, facilityPupilObj):
        activeAlertData = {}
        activeAlertData['startDate'] = datetime.datetime.now()
        activeAlertData['provision'] = ''
        activeAlertData['text'] = ''
        activeAlertData['EalLevel'] = facilityPupilObj['changeList']['Default']['SENdata']['EALlevel']
        return activeAlertData

    def __getSpecialPermissions__(self, facilityPupilObj):
        # cant find data
        return

    def __getGeneral__(self, facilityPupilObj):
        return facilityPupilObj['changeList']['Default']['General'][0]

    def __getHistory__(self, facilityPupilObj):
        return facilityPupilObj['changeList']['Default']['SENdata']['Assessment']

    def __getLanguageDetails__(self, facilityPupilObj):
        returndict = {}
        returndict['lan1'] = ''
        returndict['lan1 reading level'] = facilityPupilObj['changeList']['Default']['SENdata']['lan1 reading level']
        returndict['lan1 writing level'] = facilityPupilObj['changeList']['Default']['SENdata']['lan1 writing level']
        returndict['lan1 speaking level'] = facilityPupilObj['changeList']['Default']['SENdata']['lan1 speaking level']
        returndict['lan2'] = facilityPupilObj['changeList']['Default']['SENdata']['lan2'][:2].lower()
        returndict['lan2 reading level'] = facilityPupilObj['changeList']['Default']['SENdata']['lan2 reading level']
        returndict['lan2 writing level'] = facilityPupilObj['changeList']['Default']['SENdata']['lan2 writing level']
        returndict['lan2 speaking level'] = facilityPupilObj['changeList']['Default']['SENdata']['lan2 speaking level']
        returndict['lan3'] = facilityPupilObj['changeList']['Default']['SENdata']['lan3'][:2].lower()
        returndict['lan3 reading level'] = facilityPupilObj['changeList']['Default']['SENdata']['lan3 reading level']
        returndict['lan3 writing level'] = facilityPupilObj['changeList']['Default']['SENdata']['lan3 writing level']
        returndict['lan3 speaking level'] = facilityPupilObj['changeList']['Default']['SENdata']['lan3 speaking level']
        return returndict

    def __getFurtherLanguageDetails__(self, facilityPupilObj):
        returnDict = {}
        returnDict['Mother Nationality'] = facilityPupilObj['changeList']['Default']['SENdata']['Mothers Nationality']
        returnDict['Mother Language'] = facilityPupilObj['changeList']['Default']['SENdata']['Mothers Language'][:2].lower()
        returnDict['Father Nationality'] = facilityPupilObj['changeList']['Default']['SENdata']['Fathers Nationality']
        returnDict['Father Language'] = '' # not found in facility
        return returnDict

    def __getProvisions__(self, facilityPupilObj):
        returnList = []
        try:
            provisions = facilityPupilObj['reports']['Default']['Provisions']
        except:
            return False
        for i in provisions:
            targetDate = i['DateTime'][:10]
            targetData = "<p>%s, %s</p><p>Freq:%s</p><p>%s</p><p>%s</p>" % (i['Term'],
                                                                            i['Year'],
                                                                            i['Freq'],
                                                                            self.__convertToUniCode__(i['SenProv']),
                                                                            i['Teacher'])
            temp = {'targetData': targetData, 'targetDate': targetDate}
            returnList.append(temp)
        return returnList

    def __getTargets__(self, facilityPupilObj):
        # cant find data
        return

    def __getAreasOfConcern__(self, facilityPupilObj):
        returnDict = {}
        returnDict['Reading Level'] = facilityPupilObj['changeList']['Default']['SENdata']['Reading Level']
        returnDict['Reading Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['Reading Notes']
        returnDict['Reading Teacher Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['Reading Teacher Notes']
        returnDict['Writing Level'] = facilityPupilObj['changeList']['Default']['SENdata']['Writing Level']
        returnDict['Writing Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['Writing Notes']
        returnDict['Writing Teacher Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['Writing Teacher Notes']
        returnDict['Maths Level'] = facilityPupilObj['changeList']['Default']['SENdata']['Maths Level']
        returnDict['Maths Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['Maths Notes']
        returnDict['Maths Teacher Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['Maths Teacher Notes']
        returnDict['Speaking Level'] = facilityPupilObj['changeList']['Default']['SENdata']['Speaking Level']
        returnDict['Speaking Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['Speaking Notes']
        returnDict['Speaking Teacher Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['Speaking Teacher Notes']
        returnDict['Social Level'] = facilityPupilObj['changeList']['Default']['SENdata']['Social Level']
        returnDict['Social Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['Social Notes']
        returnDict['Social Teacher Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['Social Teacher Notes']
        returnDict['General Level'] = facilityPupilObj['changeList']['Default']['SENdata']['General Level']
        returnDict['General Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['General Notes']
        returnDict['General Teacher Notes'] = facilityPupilObj['changeList']['Default']['SENdata']['General Teacher Notes']
        return returnDict

    def __getNotes__(self, facilityPupilObj):
        returnList = []
        for i in facilityPupilObj['reports']['Default']['Reviews']:
            tempDict = {}
            for k,v in i.iteritems():
                tempDict[k] = v
            returnList.append(tempDict)
        return returnList

###############################################################################
# putting data into WillowTree
###############################################################################

    def __importActiveAlert__(self, facilityPupilObj):
        alertType = AlertType.objects.get(id=7)
        alertData = self.__getActiveAlert__(facilityPupilObj)
        alert = PupilAlert(PupilId=self.willowPupilObj,
                           StartDate=alertData['startDate'],
                           AlertType=alertType,
                           AlertDetails=alertData['text'],
                           RaisedBy=self.me)
        alert.save()
        return

    def __importGeneral__(self, facilityPupilObj):
        try:
            data = self.__getGeneral__(facilityPupilObj)
        except:
            return
        data = self.__convertToUniCode__(self.__getGeneral__(facilityPupilObj))
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealGeneralNotes': data})
        return

    def __importAssessmentHistory__(self, facilityPupilObj):
        try:
            data = self.__getHistory__(facilityPupilObj)
        except:
            return
        data = self.__convertToUniCode__(self.__getHistory__(facilityPupilObj))
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealAssessmentHistory': data})
        return

    def __importLanguageDetails__(self, facilityPupilObj):
        data = self.__getLanguageDetails__(facilityPupilObj)
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguageSpeakingLevel': data['lan1 speaking level']})
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguageWritingLevel': data['lan1 writing level']})
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguageReadingLevel': data['lan1 reading level']})
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguage2': data['lan2']})
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguage2SpeakingLevel': data['lan2 speaking level']})
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguage2WritingLevel': data['lan2 writing level']})
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguage2ReadingLevel': data['lan2 reading level']})
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguage3': data['lan3']})
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguage3SpeakingLevel': data['lan3 speaking level']})
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguage3WritingLevel': data['lan3 writing level']})
        self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                {'ealLanguage3ReadingLevel': data['lan3 reading level']})
        return

    def __importFurtherLanguageDetails__(self, facilityPupilObj):
        data = self.__getFurtherLanguageDetails__(facilityPupilObj)
        # getting contacts
        for i in FamilyContact.objects.filter(FamilyId__in=[x.FamilyId for x in FamilyChildren.objects.filter(Pupil__id=self.willowPupilObj.id)]):
            if i.Relationship.Type == 'Mother':
                motherObj = i
            if i.Relationship.Type == 'Father':
                fatherObj = i
        try:
            # mother details
            motherExtRecord = ExtendedRecord(BaseType='Contact',
                                             BaseId=motherObj.id)
            motherExtRecord.WriteExtention('ContactExtra',
                                           {'Nationality':data['Mother Nationality']})
            motherExtRecord.WriteExtention('ContactExtra',
                                           {'PrimaryLanguage':data['Mother Language'][:2]})
        except:
            pass
        try:
            # father details
            fatherExtRecord = ExtendedRecord(BaseType='Contact',
                                             BaseId=fatherObj.id)
            fatherExtRecord.WriteExtention('ContactExtra',
                                           {'Nationality':data['Father Nationality']})
            fatherExtRecord.WriteExtention('ContactExtra',
                                           {'PrimaryLanguage':data['Father Language'][:2]})
        except:
            pass
        return

    def __importProvisions__(self, facilityPupilObj):
        if self.__getProvisions__(facilityPupilObj):
            for i in self.__getProvisions__(facilityPupilObj):
                provisionObj = PupilProvision(Pupil=self.willowPupilObj,
                                                      Type='EAL',
                                                      SENType='Tuition',
                                                      StartDate=i['targetDate'],
                                                      Details=i['targetData'])
                provisionObj.save()
            return
        else:
            return

    def __importAreasOfConcern__(self, facilityPupilObj):
        data = self.__getAreasOfConcern__(facilityPupilObj)
        if data['Reading Level']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                    {'ealAreasOfConcernReadingLevel': data['Reading Level']})
        if data['Reading Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                    {'ealAreasOfConcernReadingNotes': '<p>%s</p>' % self.__convertToUniCode__(data['Reading Notes'])})
        if data['Reading Teacher Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernReadingTeacherNotes': '<p>%s</p>' % self.__convertToUniCode__(data['Reading Teacher Notes'])})
        if data['Writing Level']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernWritingLevel': data['Writing Level']})
        if data['Writing Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernWritingNotes': '<p>%s</p>' % self.__convertToUniCode__(data['Writing Notes'])})
        if data['Writing Teacher Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernWritingTeacherNotes': '<p>%s</p>' % self.__convertToUniCode__(data['Writing Teacher Notes'])})
        if data['Maths Level']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernMathsLevel': data['Maths Level']})
        if data['Maths Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernMathsNotes': '<p>%s</p>' % self.__convertToUniCode__(data['Maths Notes'])})
        if data['Maths Teacher Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernTeacherNotes': '<p>%s</p>' % self.__convertToUniCode__(data['Maths Teacher Notes'])})
        if data['Speaking Level']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernListeningAndRespondingLevel': data['Speaking Level']})
        if data['Speaking Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernListeningAndRespondingNotes': '<p>%s</p>' % self.__convertToUniCode__(data['Speaking Notes'])})
        if data['Speaking Teacher Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernListeningAndRespondingTeacherNotes': '<p>%s</p>' % self.__convertToUniCode__(data['Speaking Teacher Notes'])})
        if data['Social Level']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernSocialLevel': data['Social Level']})
        if data['Social Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernSocialNotes': '<p>%s</p>' % self.__convertToUniCode__(data['Social Notes'])})
        if data['Social Teacher Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernSocialTeacherNotes': '<p>%s</p>' % self.__convertToUniCode__(data['Social Teacher Notes'])})
        if data['General Level']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernGeneralLevel': data['General Level']})
        if data['General Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernGeneralNotes': '<p>%s</p>' % self.__convertToUniCode__(data['General Notes'])})
        if data['General Teacher Notes']:
            self.pupilExtentionRecord.WriteExtention('PupilEAL',
                                                     {'ealAreasOfConcernGeneralTeacherNotes': '<p>%s</p>' % self.__convertToUniCode__(data['General Teacher Notes'])})
        return

    def __importNotes__(self, facilityPupilObj):
        try:
            notes = self.__getNotes__(facilityPupilObj)
        except:
            notes = False
        if notes:
            for i in self.__getNotes__(facilityPupilObj):
                note = NoteDetails(Keywords='Learning Support:EAL',
                       RaisedBy=self.me,
                       ModifiedBy=self.me,
                       RaisedDate=i['DateTime'][:10],
                       ModifiedDate=self.todaysDate,
                       NoteText=self.__convertToUniCode__(i['Notes']))
                note.save()
                pupilNote = PupilNotes(PupilId=self.willowPupilObj,
                                       NoteId=note)
                pupilNote.save()
        return


class importEalLevelsOnly(run):
    def __init__(self):
        self.ealLevelsList = {'Aaron_Kivinen': '2',
         'Aemil_Sobhan': '3',
         'Aiden_Forusz': '0',
         'Akira_Schroeder': '3',
         'Alba_Walter': '3',
         'Albert_Shishkhanov': '1',
         'Alec_Baxendale': '1',
         'Alejandra_Gardner': '3',
         'Alessandro_Gallo': '4',
         'Alexander_Griffin': '4',
         'Alexandra_Prakke': '4',
         'Alice_Theys': '3',
         'Alice_Wiwen-Nilsson': '4',
         'Aliki_Frangoulis': '3',
         'Allegra_Frontini': '4',
         'Allegra_Hamilton': '4',
         'Allegra_Negri': '3',
         'Amanda_Lourie': '3',
         'Amelia_Zekrya': '3',
         'Amina_Kotb': '4',
         'Andrea_Micheli': '2',
         'Andrea_Schapira': '3',
         'Annika_Golder': '3',
         'Annika_Trobman': '0',
         'Anton_De Groot van Embden': '2',
         'Ariane_Bardonnet': '4',
         'Artur_Serrat Ramada': '4',
         'Ashleigh_Adnams': '0',
         'Atto_Allas': '4',
         'Augustin_Lefevre': '2',
         'Basil_Geczy': '4',
         'Beatrice_Baldi': '3',
         'Beatrice_Daniel': '4',
         'Benjamin_Skinner': '4',
         'Bianca_Schapira': '2',
         'Brando_Tome': '3',
         'Cara_Vogels': '4',
         'Carlo_Revelli': '4',
         'Carlotta_Galperin': '2',
         'Carolina_Mavroleon': '3',
         'Caterina_Mammola': '3',
         'Cem_Demirel': '2',
         'Charlotte_Condacci Reis': '2',
         'Charlotte_Marshall-Lockyer': '4',
         'Christian_Oestergaard': '3',
         'Cosima_Aslangul': '4',
         'David_Kroll': '3',
         'David_Zaoui': '4',
         'Demi_Halil': '4',
         'Denise_Shishkhanova': '1',
         'Dirk_Hart Blanco-Ulribe': '3',
         'Dmitry_Zimin': '1',
         'Edoardo_Colao': '4',
         'Edoardo_Fitzwilliam-Lay': '3',
         'Edouard_Barrier': '3',
         'Eduardo-Ernesto_Galperin': '2',
         'Elisa_Velez': '3',
         'Elya_Renom': '3',
         'Erik_Haraldson': '4',
         'Eytan_Koen': '2',
         'Felix_Dennison': '4',
         'Felix_Hamilton': '4',
         'Francesca_Pucci': '3',
         'Frida_Douek': '4',
         'Gabriel_Henrion': '4',
         'Giacomo_Revelli': '4',
         'Giacomo_Ward-Jackson': '1',
         'Hailey_McNally': '4',
         'Hannah_Mies': '4',
         'Hannah_Wilkinson': '4',
         'Heba_Ahmed': '4',
         'Hinano_Schad': '3',
         'Hugo_Hamilton': '4',
         'Ines_Sawiris': '3',
         'Ines_Theys': '3',
         'Isabel_Ritchotte': '4',
         'Isabella_Pucci': '2',
         'Isabella_Ward-Jackson': '3',
         'Isabelle_Ondruch': '3',
         'Jad_Farrell': '2',
         'Jahan_de Bellaigue': '3',
         'Jaia_Frontini': '4',
         'Jamie_Ginsberg': '4',
         'Jan-Luca_Ko': '3',
         'Johannes_Ondruch': '2',
         'Julia_Dylik': '1',
         'Kai_Nieuwenburg': '4',
         'Kareem_Obeidat': '3',
         'Kenji_Schroeder': '3',
         'Kiana_Kamann': '2',
         'Kiana_de Bellaigue': '4',
         'Knut_Wiwen-Nilsson': '4',
         'Kumayl_Riza': '3',
         'Kyle_Newman': '3',
         'Kyra_Khlat': '3',
         'Larissa_Gaunt': '4',
         'Laura_Braun': '3',
         'Layla_Nieuwenburg': '4',
         'Lea_Cornelis': '4',
         'Lea_George': '4',
         'Leo_Baxendale': '1',
         'Leo_Guyard': '3',
         'Leo_Scully': '3',
         'Leonardo_Fitzwilliam-Lay': '3',
         'Leopold_Ducomble': '3',
         'Leya_Himani': '3',
         'Linus_Pfander': '3',
         'Livia_Hilhorst': '2',
         'Lorenzo_Chiari-Gaggia': '4',
         'Lorenzo_Frescobaldi': '4',
         'Lorenzo_Warrack': '4',
         'Luc_de Fougerolles': '4',
         'Luca_Frontini': '4',
         'Luca_Smith': '3',
         'Luca_Walter': '3',
         'Lucas_Pabst': '4',
         'Ludovica_Enrico': '1',
         'Ludovico_Baldi': '2',
         'Luke_Jeffries': '3',
         'Mafalda_Groos': '3',
         'Magali_Pashigian': '0',
         'Maia_Lough': '4',
         'Marcel_Baquiche': '2',
         'Marianna_Scopelliti': '1',
         'Marina_Santis': '4',
         'Maryam_Mirjan': '4',
         'Matias_Kerschen': '3',
         'Matilde_Monteforte': '4',
         'Matteo_Dreesmann': '3',
         'Matthias_Monarchi': '4',
         'Maxim_Lyashenko': '3',
         'Maximilien_Bonnefoy': '0',
         'Maya_Mulholland': '0',
         'Mert_Demirel': '1',
         'Mia_George': '3',
         'Michael_Lourie': '4',
         'Milla_Nieuwenburg': '3',
         'Moyo_Lawani': '4',
         'Musashi_Schad': '3',
         'Naseem_Moumene': '4',
         'Niccolo_Smith': '4',
         'Nicholas_Dennison': '4',
         'Nicolas_Ciraolo': '4',
         'Nicolas_Lefevre': '2',
         'Nicolo_Fitzwilliam-Lay': '4',
         'Niklas_Ondruch': '3',
         'Noah_Mies': '4',
         'Oliver_Nishihara': '2',
         'Oliver_Sibley': '4',
         'Olivia_Daniel': '4',
         'Olivia_Kerschen': '4',
         'Olivia_Negri': '1',
         'Orlando_Mallinson': '4',
         'Oscar_Warneryd': '4',
         'Philip_Yanakov': '1',
         'Pietro_Enrico': '2',
         'Pietro_Pignatti Morano': '3',
         'Quinten_Dreesmann': '3',
         'Ramsey_Lababidi': '3',
         'Raphael_Henrion': '4',
         'Rocco_Brignone': '1',
         'Rohan_Goyal': '3',
         'Roxanna_Fahid': '4',
         'Ryan_Benjelloun': '3',
         'Santiago_Bluhm': '1',
         'Sara_Tononi': '3',
         'Sasha_Ginsberg': '4',
         'Sebastian_Pabst': '3',
         'Sienna_Baggioli': '4',
         'Silvia_Sicheri': '3',
         'Skye_Wong': '4',
         'Sofia_Committeri': '4',
         'Sofia_Pecce': '3',
         'Sophia_de Reyes Mezbur': '4',
         'Suzzana_Dylik': '3',
         'Taddeo_Brignone': '1',
         'Tammy_Himani': '4',
         'Tess_Vogels': '4',
         'Theodore_Baturin': '1',
         'Theodore_Griffin': '2',
         'Thomas_Lefranc': '3',
         'Tomaso_Enrico': '1',
         'Tommaso_Committeri': '3',
         'Valentina_Pucci': '2',
         'Virginia_Baldan': '3',
         'Wiktoria_Maj': '2',
         'Xander_Tome': '3',
         'Xavier_Guerlain-Desai': '4',
         'Yasmin_Koen': '2',
         'Yiannis_Frangoulis': '1',
         'Zac_de Fougerolles': '4',
         'Zena_Seah': '4',
         'Zoe_Lockwood': '4',
         'Zuheyr_Riza': '4'}
        for k, v in self.ealLevelsList.iteritems():
            test = self.getPupilObj(k)
            test2 = ''
            if test != "error":
                test2 = self.setEalLevel(v)
                if test2 == 'error':
                    print '***ERROR %s --> %s ERROR***' % (k, v)
                else:
                    print '%s --> %s' % (k, v)
            else:
                print '***ERROR %s --> %s ERROR***' % (k, v)
            cache.delete('PupilEAL_%s' % self.pupilObj.id)

    def getPupilObj(self, pupilName):
        pupilName1 = pupilName.split('_')
        if Pupil.objects.filter(Forename=pupilName1[0],
                                Surname=pupilName1[1]).count() == 1:
            self.pupilObj = Pupil.objects.get(Forename=pupilName1[0],
                                              Surname=pupilName1[1])
        else:
            return 'error'
        return

    def setEalLevel(self, ealLevel):
        try:
            pupilAlert = PupilAlert.objects.get(PupilId=int(self.pupilObj.id),
                                                AlertType__Name__icontains='EAL',
                                                Active=True)
            pupilAlert.AlertType = AlertType.objects.get(Name='EAL Level %s' % ealLevel)
            pupilAlert.save()
            return pupilAlert.AlertType.Name
        except:
            return 'error2'
