from MIS.models import Pupil
from MIS.models import PupilGroup
import csv


def run():
    print "working..."
    counter = 0
    mainWriteList = set()
    for pupil in Pupil.objects.filter(Active=True):
    #for pupil in Pupil.objects.filter(Forename="Daisy", Active=True):
        dup = 0
        for i in Pupil.objects.filter(Active=True):
            if (pupil.Forename == i.Forename) and (pupil.Surname == i.Surname) and (pupil.DateOfBirth == i.DateOfBirth):
                dup += 1
        if dup > 1:
            groups = PupilGroup.objects.filter(Pupil__id=pupil.id, Active=True)
            groupList = ""
            for group in groups:
                    groupList += group.Group.Name
            tempWriteList = '%s,%s,%s' % (pupil.Forename, pupil.Surname, str(groupList))
            mainWriteList.add(tempWriteList)
            counter += 1
            print "found %d duplications..." % counter
#csv write
    mainWriteList = list(mainWriteList)
    with open('duplicatePupils_.csv', 'wb') as csvFile:
        csvWriter = csv.writer(csvFile, delimiter=',')
        csvWriter.writerow(['Forename','Surname','Groups'])
        for i in mainWriteList:
            csvWriter.writerow(i.split(','))