# willow tree imports
from MIS.models import SchoolList
from MIS.models import Address
# python imports
import csv


def printCSV():
    with open('List of previous schools6.csv', 'rb') as csvfile:
        schoolsFile = csv.reader(csvfile, delimiter=';')
        for row in schoolsFile:
            try:
                addressTemp = row[2].split(',')
                if addressTemp[0] == "" or addressTemp[0] == " ":
                    addressTemp[0] == "Not Given"
                if addressTemp[1] == "" or addressTemp[1] == " ":
                    addressTemp[1] == "Not Given"
                if addressTemp[3] == "" or addressTemp[3] == " ":
                    addressTemp[3] == "Not Given"
                schoolAddress = Address(HomeSalutation="Not Given",
                                        PostalTitle="Not Given",
                                        AddressLine1=addressTemp[0].lstrip(),
                                        AddressLine2=addressTemp[1].lstrip(),
                                        PostCode=addressTemp[3].lstrip(),
                                        Phone1=row[3])
                schoolAddress.save()
                schoolSchool = SchoolList(Name=row[0].lstrip(),
                                          HeadTeacher=row[1].lstrip(),
                                          Address=schoolAddress)
                schoolSchool.save()
                print "school %s saved" % row[0]
            except:
                print "<<<---   school %s FAILED   --->>>" % row[0]