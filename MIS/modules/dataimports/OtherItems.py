#-------------------------------------------------------------------------------
# Name:        DefaultData
# Purpose:      Pushes default records into the WillowTree MIS
#
# Author:      DBMgr
#
# Created:     12/02/2012
# Copyright:   (c) DBMgr 2012
# Licence:
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from WillowTree.MIS import models
from MIS.models import *
import datetime, copy, csv
from MIS.modules.GeneralFunctions import *
from MIS.modules.ExtendedRecords import *
from FamilyImport import importPupils,importContacts

PreviousGradeDic={'YrR_Eot_S':'YrR_Eot_M',
                  'Yr1_Eot_L':'Yr1_Eot_M',
                  'Yr1_Eot_S':'Yr1_Eot_M',
                  'Yr2_Eot_L':'Yr2_Eot_M',
                  'Yr2_Eot_S':'Yr2_Eot_M',
                  'Yr3_Eot_L':'Yr3_Eot_M',
                  'Yr3_Eot_S':'Yr3_Eot_M',
                  'Yr4_Eot_L':'Yr4_Eot_M',
                  'Yr4_Eot_S':'Yr4_Eot_M',
                  'Yr5_Eot_L':'Yr5_Eot_M',
                  'Yr5_Eot_S':'Yr5_Eot_M',
                  'Yr6_Eot_L':'Yr6_Eot_M',
                  'Yr6_Eot_S':'Yr6_Eot_M',
                  'Yr7_Eot_L':'Yr7_Eot_M',
                  'Yr7_Eot_S':'Yr7_Eot_M',
                  'Yr8_Eot_L':'Yr8_Eot_M',
                  'Yr8_Eot_S':'Yr8_Eot_M',
                  }
PreviousHalfGradeDic={'Yr3_Hf_L':'Yr3_Hf_M',
                  'Yr3_Hf_S':'Yr3_Hf_L',
                  'Yr4_Hf_L':'Yr4_Hf_M',
                  'Yr4_Hf_S':'Yr4_Hf_L',
                  'Yr5_Hf_L':'Yr5_Hf_M',
                  'Yr5_Hf_S':'Yr5_Hf_L',
                  'Yr6_Hf_L':'Yr6_Hf_M',
                  'Yr6_Hf_S':'Yr6_Hf_L',
                  'Yr7_Hf_L':'Yr7_Hf_M',
                  'Yr7_Hf_S':'Yr7_Hf_L',
                  'Yr8_Hf_L':'Yr8_Hf_M',
                  'Yr8_Hf_S':'Yr8_Hf_L',
                  }
def PopulateGradeInput():
    Recs=GradeInputElement.objects.filter(GradeRecord__Name__iendswith='_Eot_M')
    for records in Recs:
        print records
        NewLentGradeName ='%s_Eot_L' % records.GradeRecord.Name.split('_')[0]
        NewSummerGradeName = '%s_Eot_S' % records.GradeRecord.Name.split('_')[0]
        if ExtentionRecords.objects.filter(Name=NewLentGradeName).exists():
            if NewLentGradeName in PreviousGradeDic:
                NewLentInput=GradeInputElement(GradeRecord=ExtentionRecords.objects.get(Name=NewLentGradeName),
                                               PrevGradeRecord=ExtentionRecords.objects.get(Name=PreviousGradeDic[NewLentGradeName]),
                                                GradeSubject=records.GradeSubject,
                                                InputForm=records.InputForm,
                                                DisplayOrder=records.DisplayOrder)
                NewLentInput.save()
            else:
                NewLentInput=GradeInputElement(GradeRecord=ExtentionRecords.objects.get(Name=NewLentGradeName),
                                               GradeSubject=records.GradeSubject,
                                               InputForm=records.InputForm,
                                               DisplayOrder=records.DisplayOrder)
                NewLentInput.save()
        if ExtentionRecords.objects.filter(Name=NewSummerGradeName).exists():
            if NewSummerGradeName in PreviousGradeDic:
                NewSummerInput=GradeInputElement(GradeRecord=ExtentionRecords.objects.get(Name=NewSummerGradeName),
                                                 PrevGradeRecord=ExtentionRecords.objects.get(Name=PreviousGradeDic[NewSummerGradeName]),
                                                 GradeSubject=records.GradeSubject,
                                                 InputForm=records.InputForm,
                                                 DisplayOrder=records.DisplayOrder)
                NewSummerInput.save()
            else:
                NewSummerInput=GradeInputElement(GradeRecord=ExtentionRecords.objects.get(Name=NewSummerGradeName),
                                                 GradeSubject=records.GradeSubject,
                                                 InputForm=records.InputForm,
                                                 DisplayOrder=records.DisplayOrder)
                NewSummerInput.save()

def PopulateHalfGradeInput():
    Recs=GradeInputElement.objects.filter(GradeRecord__Name__iendswith='_Hf_M')
    for records in Recs:
        print records
        NewLentGradeName ='%s_Hf_L' % records.GradeRecord.Name.split('_')[0]
        NewSummerGradeName = '%s_Hf_S' % records.GradeRecord.Name.split('_')[0]
        if ExtentionRecords.objects.filter(Name=NewLentGradeName).exists():
            if NewLentGradeName in PreviousGradeDic:
                NewLentInput=GradeInputElement(GradeRecord=ExtentionRecords.objects.get(Name=NewLentGradeName),
                                               PrevGradeRecord=ExtentionRecords.objects.get(Name=PreviousGradeDic[NewLentGradeName]),
                                                GradeSubject=records.GradeSubject,
                                                InputForm=records.InputForm,
                                                DisplayOrder=records.DisplayOrder)
                NewLentInput.save()
            else:
                NewLentInput=GradeInputElement(GradeRecord=ExtentionRecords.objects.get(Name=NewLentGradeName),
                                               GradeSubject=records.GradeSubject,
                                               InputForm=records.InputForm,
                                               DisplayOrder=records.DisplayOrder)
                NewLentInput.save()
        if ExtentionRecords.objects.filter(Name=NewSummerGradeName).exists():
            if NewSummerGradeName in PreviousGradeDic:
                NewSummerInput=GradeInputElement(GradeRecord=ExtentionRecords.objects.get(Name=NewSummerGradeName),
                                                 PrevGradeRecord=ExtentionRecords.objects.get(Name=PreviousGradeDic[NewSummerGradeName]),
                                                 GradeSubject=records.GradeSubject,
                                                 InputForm=records.InputForm,
                                                 DisplayOrder=records.DisplayOrder)
                NewSummerInput.save()
            else:
                NewSummerInput=GradeInputElement(GradeRecord=ExtentionRecords.objects.get(Name=NewSummerGradeName),
                                                 GradeSubject=records.GradeSubject,
                                                 InputForm=records.InputForm,
                                                 DisplayOrder=records.DisplayOrder)
                NewSummerInput.save()