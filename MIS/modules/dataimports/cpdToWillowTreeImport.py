'''
HOW TO USE -->

1) Firstly run the "CPD_import_to_WillowTree.py" program on all four Facility
   servers.

2) This will save .p files to the "/home/login/Dropbox/cpdToWillowTree" folder.
   now open a django's ipython shell on the server you wish to import to and
   run this program for all four .p files.

3) Remeber to include a school when running this program.
'''

# python imports
import pickle
import datetime

# WillowTree imports
from MIS.models import Staff
from MIS.models import StaffCpdRecords


class run():
    ''' this code is to be used in conjuction with the 'CPD_to_WillowTree'
    scripts on the Facility servers - roy '''
    def __init__(self, importFile, school, individualImport=False):
        self.todaysDate = self.__todaysDate__()
        self.school = school
        self.logFile = open('cpdToWillowTreeImport%s.log' % school, 'w')
        self.cpdObj = pickle.load(open(importFile, 'rb'))
        if individualImport:
            self.cpdObj = self.cpdObj[0] #change index to test subject needed
        else:
            test = False
            counter = 0
            endCounter = len(self.cpdObj)
            for i in self.cpdObj:
                try:
                    test = self.__getTeacher__(i)
                except:
                    self.logFile.write('Import FAIL! for --> %s %s,\n' % (i['Forename'],
                                       i['Surname']))
                if test:
                    self.__importToWillowTree__(i)
                    self.logFile.write('Import success for --> %s %s,\n' % (self.staff.Forename,
                                       self.staff.Surname))
                counter += 1
                print '%s' % str(endCounter - counter)
        self.logFile.close()

###############################################################################
# helper methods
###############################################################################

    def __getTeacher__(self, facilityObj):
        if Staff.objects.filter(Old_Id='%s-%s' % (self.school,
                                                  facilityObj['TeacherId'])).exists():
            self.staff = Staff.objects.get(Old_Id='%s-%s' % (self.school,
                                                             facilityObj['TeacherId']))
        else:
            self.staff = Staff.objects.get(Forename=facilityObj['Forename'],
                                           Surname=facilityObj['Surname'])
        return True

    def __todaysDate__(self):
        now = datetime.datetime.now()
        return now.strftime("%Y-%m-%d")

    def __convertToUniCode__(self, string):
        for i in ['\x90', '\x91', '\x92' ,'\x93', '\x94',
                  '\x95','\x96', '\x97', '\x98', '\x99']:
            string = string.replace(i, '')
        try:
            string = unicode(string)
        except:
            string = "<p><<<--- unable to import --->>></p>"
        return string

###############################################################################
# getting data
###############################################################################

    def __getCourseTitle__(self, facilityObj):
        data = ''
        if facilityObj['CrsNameN']:
            data += facilityObj['CrsNameN']
        if facilityObj['CrsName'] and facilityObj['CrsNameN']:
            data += ', %s' % facilityObj['CrsName']
        else:
            if facilityObj['CrsName']:
                data += facilityObj['CrsName']
        if not data:
            data = 'No Course title'
        return data

    def __getNameOfProv__(self, facilityObj):
        if facilityObj['CrsProv']:
            data = facilityObj['CrsProv']
        else:
            data = 'No name of provider given'
        return data

    def __getDateTaken__(self, facilityObj):
        try:
            temp = facilityObj['CrsDate']
            if len(temp) > 11:
                data = facilityObj['DateTime'][:10]
            else:
                temp  = temp.split('-')
                if len(temp[2]) == 4:
                    data = '%s-%s-%s' %(temp[2], temp[1], temp[0])
                else:
                    data = '20%s-%s-%s' %(temp[2], temp[1], temp[0])
        except:
            data = facilityObj['DateTime'][:10]
        return data

    def __getCertIssued__(self, facilityObj):
        if 'CrsCert' in facilityObj:
            if facilityObj['CrsCert'] == 'Y':
                data = True
            else:
                data = False
        return data

    def __getExpiryDate__(self, facilityObj):
        try:
            temp = facilityObj['CrsExp']
            if len(temp) > 11:
                data = False
            else:
                temp  = temp.split('-')
                if len(temp[2]) == 4:
                    data = '%s-%s-%s' %(temp[2], temp[1], temp[0])
                else:
                    data = '20%s-%s-%s' %(temp[2], temp[1], temp[0])
        except:
            data = False
        return data

    def __getIntExt__(self, facilityObj):
        if 'CrsPlc' in facilityObj:
            if facilityObj['CrsPlc']:
                data = 'Internal'
            else:
                data = 'External'
        else:
            data = 'External'
        return data

    def __getdetailsNotes__(self, facilityObj):
        if 'CrsNotes' in facilityObj:
            if facilityObj['CrsNotes']:
                data = self.__convertToUniCode__(facilityObj['CrsNotes'])
            else:
                data = ''
        else:
            data = ''
        return data

###############################################################################
# importing data
###############################################################################

    def __importToWillowTree__(self, facilityObj):
        cpdObj = StaffCpdRecords(Staff=self.staff,
                                 Name=self.__getCourseTitle__(facilityObj),
                                 Provider=self.__getNameOfProv__(facilityObj),
                                 DateTaken=self.__getDateTaken__(facilityObj),
                                 InternalExternal=self.__getIntExt__(facilityObj),
                                 Details=self.__getdetailsNotes__(facilityObj))
        if self.__getCertIssued__(facilityObj):
            cpdObj.CertIssued = self.__getCertIssued__(facilityObj)
        if self.__getExpiryDate__(facilityObj):
            cpdObj.DateExpires = self.__getExpiryDate__(facilityObj)
        cpdObj.save()
        return
