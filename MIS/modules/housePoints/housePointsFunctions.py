# Python imports...
from MIS.models import PupilNotes
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules.GeneralFunctions import Get_PupilList
from MIS.modules.attendance.AttendanceFunctions import PupilAttendanceRec


# WillowTree imports...
import datetime
import copy

class housePointsReport():
    ''' creates the termly Battersea house point report '''
    def __init__(self, groupName, AcYear, term=False):
        self.groupName = groupName
        self.AcYear = AcYear
        self.pupils = Get_PupilList(AcYear, groupName)
        self.todaysDate = datetime.datetime.today()
        self.yearDates = GlobalVariablesFunctions.YearDates(AcYear)
        if term:
            self.__returnTermBasedOnTodaysDate__(term)
        else:
            self.__returnTermBasedOnTodaysDate__()
        self.__getDatesGrid__()
        self.__getHousePointsData__()
        self.pupilNames = self.__pupilNames__()

    def __pupilNames__(self):
        ''' list of pupil names '''
        temp = []
        for i in self.pupils:
            temp.append('%s %s' % (i.Forename, i.Surname))
        return temp

    def __returnTermBasedOnTodaysDate__(self, term):
        ''' returns the term + term start and end dates based on term variable '''
        if term == 'Michaelmas':
            self.datesData = {'Term': 'Michaelmas',
                              'startDate': self.yearDates.MichStart(),
                              'stopDate': self.yearDates.MichStop()}
        if term == 'Lent':
            self.datesData = {'Term': 'Michaelmas',
                              'startDate': self.yearDates.LentStart(),
                              'stopDate': self.yearDates.LentStop()}
        if term == 'Summer':
            self.datesData = {'Term': 'Michaelmas',
                              'startDate': self.yearDates.SummerStart(),
                              'stopDate': self.yearDates.SummerStop()}
        return

    def __returnTermBasedOnTodaysDate__(self):
        ''' returns the term + term start and end dates based on todays date '''
        # Mich term test:
        michStart = self.yearDates.MichStart()
        michEnd = self.yearDates.MichStop()
        if self.todaysDate >= michStart and self.todaysDate <= michEnd:
            self.datesData = {'Term': 'Michaelmas',
                              'startDate': michStart,
                              'stopDate': michEnd}
        # Lent term test:
        lentStart = self.yearDates.LentStart()
        lentEnd = self.yearDates.LentStop()
        if self.todaysDate >= lentStart and self.todaysDate <= lentEnd:
            self.datesData = {'Term': 'Lent',
                              'startDate': lentStart,
                              'stopDate': lentEnd}
        # Summer term test:
        summerStart = self.yearDates.SummerStart()
        summerEnd = self.yearDates.SummerStop()
        if self.todaysDate >= summerStart and self.todaysDate <= summerEnd:
            self.datesData = {'Term': 'Summer',
                              'startDate': summerStart,
                              'stopDate': summerEnd}
        return

    def __getDatesGrid__(self):
        ''' returns a list of mondays in the term used in the reports '''
        attObj = PupilAttendanceRec(self.pupils[0].id, self.AcYear)
        dates = attObj.__BuildAttendanceGrid__(self.datesData['startDate'],
                                               self.datesData['stopDate'])
        self.dateGrid = sorted([i for i in dates.keys()])
        self.dateGridStrings = [str(i).split(' ')[0] for i in self.dateGrid]

    def __getHousePointsData__(self):
        ''' gets house points data for each pupil in the grp '''
        self.listOfPupilObjs = []
        for pupil in self.pupils:
            dataDic = {}
            dataDic2 = []
            for date in self.dateGrid:
                workPostive = 0
                workNegative = 0
                behaviourPositive = 0
                behaviourNegative = 0
                workPostiveCounter = 0
                workNegativeCounter = 0
                behaviourPositiveCounter = 0
                behaviourNegativeCounter = 0
                dateForDic = str(date).split(' ')[0]
                if len(PupilNotes.objects.filter(PupilId=int(pupil.id), Active=True)) == 0:
                    pass
                else:
                    for note in PupilNotes.objects.filter(PupilId=int(pupil.id), Active=True):
                        if note.NoteId.RaisedDate >= date and note.NoteId.RaisedDate <= date + datetime.timedelta(days=6):
                            # work positive...
                            if 'Work' in note.NoteId.Keywords and 'Positive' in note.NoteId.Keywords:
                                keywords = note.NoteId.Keywords.split(':')
                                for keyword in keywords:
                                    if 'Positive' in keyword:
                                        workPostiveCounter += int(keyword.split(' ')[1])
                                workPostive += workPostiveCounter
                                workPostiveCounter = 0
                            # work negative...
                            if 'Work' in note.NoteId.Keywords and 'Negative' in note.NoteId.Keywords:
                                keywords = note.NoteId.Keywords.split(':')
                                for keyword in keywords:
                                    if 'Negative' in keyword:
                                        workNegativeCounter += int(keyword.split(' ')[1])
                                workNegative += workNegativeCounter
                                workNegativeCounter = 0
                            # behaviour positive...
                            if 'Merit-Behaviour' in note.NoteId.Keywords and 'Positive' in note.NoteId.Keywords:
                                keywords = note.NoteId.Keywords.split(':')
                                for keyword in keywords:
                                    if 'Positive' in keyword:
                                        behaviourPositiveCounter += int(keyword.split(' ')[1])
                                behaviourPositive += behaviourPositiveCounter
                                behaviourPositiveCounter = 0
                            # behaviour negative...
                            if 'Merit-Behaviour' in note.NoteId.Keywords and 'Negative' in note.NoteId.Keywords:
                                keywords = note.NoteId.Keywords.split(':')
                                for keyword in keywords:
                                    if 'Negative' in keyword:
                                        behaviourNegativeCounter += int(keyword.split(' ')[1])
                                behaviourNegative += behaviourNegativeCounter
                                behaviourNegativeCounter = 0
                        dataDic = {dateForDic: {'workPostive': copy.deepcopy(workPostive),
                                                'workNegative': copy.deepcopy(workNegative),
                                                'behaviourPositive': copy.deepcopy(behaviourPositive),
                                                'behaviourNegative': copy.deepcopy(behaviourNegative)}}
                    dataDic2.append(dataDic)
            self.listOfPupilObjs.append(pupilHousePointclass('%s %s' % (pupil.Forename, pupil.Surname),
                                                             dataDic2))
        return


class pupilHousePointclass():
    ''' pupil house point class for use with housePointsReport class '''
    def __init__(self, name, data):
        self.name = name
        self.data = data
        self.__totals__()

    def __totals__(self):
        ''' works out the totals for Work and Behavior '''
        self.totalWork = 0
        self.totalBehavior = 0
        for i in self.data:
            for key, value in i.iteritems():
                if 'behaviourNegative' in value:
                    self.totalBehavior = self.totalBehavior - value['behaviourNegative']
                if 'behaviourPositive' in value:
                    self.totalBehavior += value['behaviourPositive']
                if 'workNegative' in value:
                    self.totalWork = self.totalWork - value['workNegative']
                if 'workPostive' in value:
                    self.totalWork += value['workPostive']
