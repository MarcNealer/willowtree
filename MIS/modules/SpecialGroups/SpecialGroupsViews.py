# Django imports
from django.shortcuts import HttpResponse
#from django.http import HttpResponseBadRequest
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

# WillowTree imports
from MIS.ViewExtras import SetMenu
from SpecialGroupsFunctions import CreateSpecialGroups
from SpecialGroupsFunctions import GetSpecialGroups
from SpecialGroupsFunctions import ManageSpecialGroups
from MIS.models import Group


def special_group_admin_page(request, school, ac_year):
    """
    Displays the administer special groups page
    """
    c = {'groups': GetSpecialGroups(school, ac_year).get_special_groups()}
    return render_to_response('special_group_admin_page.html', c,
                              context_instance=RequestContext(request, processors=[SetMenu]))


def create_new_special_group(request, school, ac_year):
    """
    Tries to create a new special group
    """
    c = CreateSpecialGroups(request).create_group()
    c.update({'groups': GetSpecialGroups(school, ac_year).get_special_groups()})
    return render_to_response('special_group_admin_page.html', c,
                              context_instance=RequestContext(request, processors=[SetMenu]))


def add_pupils_to_special_group(request, school, ac_year, group_name):
    """
    Adds pupils to a special group
    """
    ManageSpecialGroups(request).add_pupils()
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageAll/%s/' % (school, ac_year, group_name))


def remove_pupils_to_special_group(request, school, ac_year, group_name):
    """
    Removes pupils to a special group
    """
    ManageSpecialGroups(request).remove_pupils()
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageAll/%s/' % (school, ac_year, group_name))


def deactivate_special_group(request, school, ac_year):
    """
    Removes pupils to a special group
    """
    ManageSpecialGroups(request).removes_all_members_from_a_group()
    return HttpResponseRedirect('/WillowTree/%s/%s/special_group_admin_page/' % (school, ac_year))


def view_special_groups(request, school, AcYear):
    """
    Views the teachers special group select page
    """
    c = {'groups': GetSpecialGroups(school, AcYear).get_special_groups()}
    return render_to_response('view_special_groups.html', c,
                              context_instance=RequestContext(request, processors=[SetMenu]))


def special_groups_delete_group(request, school, ac_year):
    """
    Deletes a special group, also deleting linked pupil group records as well
    """
    ManageSpecialGroups(request).delete_special_group()
    return HttpResponseRedirect('/WillowTree/%s/%s/special_group_admin_page/' % (school, ac_year))