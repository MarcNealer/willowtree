# Django imports
from django.core.cache import cache
from django.db.models import Q
from MIS.models import Group
from MIS.models import PupilGroup
from MIS.models import Pupil
from MIS.models import Staff
from MIS.models import SystemVarData
from MIS.models import MenuTypes


class GetSpecialGroups():
    """
    Gets all the specials groups for a school in a dictionary indicating which groups are active.
    """
    def __init__(self, school, ac_year):
        self.school = MenuTypes.objects.get(Name=school).SchoolId.Name
        self.ac_year = ac_year

    def get_special_groups(self):
        return_list = []
        for group in Group.objects.filter(Name__icontains='Special.%s.' % self.school):
            active_test = False
            if len(PupilGroup.objects.filter(Group__Name=group.Name, Active=True, AcademicYear=self.ac_year)) > 0:
                active_test = True
                return_list.append([group.Name, active_test, group.id])
            else:
                return_list.append([group.Name, active_test, group.id])
        return return_list


class CreateSpecialGroups():
    """
    Create special groups.

    form data into this class:
        self.request['special_group_name']
        self.request['school']
    """
    def __init__(self, request):
        self.special_group_name = request.POST['special_group_name'].replace(' ', '_')
        self.school = MenuTypes.objects.get(Name=request.POST['school']).SchoolId.Name

    def create_group(self):
        """
        Main create group logic
        """
        if "/" in self.special_group_name:
            error_message = "Forward slashes are not allowed in Special Group names"
            return {"error_message": error_message, 'operation_success': False}
        if "." in self.special_group_name:
            error_message = "Dots are not allowed in Special Group names"
            return {"error_message": error_message, 'operation_success': False}
        test_group_name = 'Special.%s.%s' % (self.school, self.special_group_name)
        if Group.objects.filter(Name__icontains=test_group_name).exists():
            error_message = "This group already exists. Please add pupils to this group to activate it."
            return {"error_message": error_message, 'operation_success': False}
        if self.__check_amount_of_special_groups__() > 100:
            error_message = "has reached it's limit of special groups. To add more please de-activate some groups."
            return {"error_message": "%s school %s" % (self.school, error_message),
                    'operation_success': False}
        check_group_name_message = self.__check_group_name__()
        if check_group_name_message['operation_success']:
            self.__create_group__()
            return check_group_name_message
        else:
            return check_group_name_message

    def __check_amount_of_special_groups__(self):
        """
        Checks if there are less than 100 groups in the school
        """
        return len(Group.objects.filter(Name__icontains='Special.%s.' % self.school))

    def __get_all_special_group_names__(self):
        """
        Gets all existing special group names
        """
        names = list(Group.objects.filter(Q(Name__icontains=self.school) | Q(Name__icontains='Special')).values_list('Name',
                                                                                                                     flat=True))
        return names

    def __get_all_teacher_names(self):
        """
        Gets all teacher forenames, surnames and nicknames and returns them as a list
        """
        forenames = [i.lower() for i in Staff.objects.all().values_list('Forename',
                                                                        flat=True).order_by('Forename').distinct('Forename')]
        surnames = [i.lower() for i in Staff.objects.all().values_list('Surname',
                                                                       flat=True).order_by('Surname').distinct('Surname')]
        nicknames = [i.lower() for i in Staff.objects.all().values_list('NickName',
                                                                        flat=True).order_by('NickName').distinct('NickName')]
        all_names = forenames+surnames+nicknames
        return all_names[1:]

    def __check_group_name__(self):
        """
        Checks the group name adheres to the following rules:

        groups names cannot contain:
            teacher names,
            "year",
            "my",
            academic year (part off),
            all months
        """
        error_message = str()
        check_group_name_test = True
        check_list = list()
        for group_name in self.__get_all_special_group_names__():
            check_list.append(group_name.split('.')[-1].lower())
        check_list.append(self.__get_all_teacher_names())
        ac_year = SystemVarData.objects.get(Variable__Name='CurrentYear').Data.split('-')
        check_list.append(ac_year[0])
        check_list.append(ac_year[1])
        check_list += self.__get_all_teacher_names()
        check_list += ['year',
                       'my',
                       'january',
                       'february',
                       'march',
                       'april',
                       'may',
                       'june',
                       'july',
                       'august',
                       'september',
                       'october',
                       'november',
                       'december']
        for part_of_new_name in self.special_group_name.split('_'):
            if part_of_new_name.lower() in check_list:
                check_group_name_test = False
                error_message = "error: %s is not allowed in special group name" % part_of_new_name
        return {'error_message': error_message, 'operation_success': check_group_name_test}

    def __create_group__(self):
        """
        Creates the group
        """
        new_group = Group(Name="Special.%s.%s" % (self.school, self.special_group_name),
                          MenuName="Special.%s.%s" % (self.school, self.special_group_name))
        new_group.save()
        return


class ManageSpecialGroups():
    """
    Manage existing special groups

    form data into this class:
        self.request['special_group_name']
        self.request['PupilSelect']
    """
    def __init__(self, request):
        self.special_group_name = request.POST["special_group_name"]
        self.ac_year = request.POST["ac_year"]
        if "PupilSelect" in request.POST:
            self.pupils_list = request.POST.getlist("PupilSelect")
        else:
            self.pupils_list = []

    def add_pupils(self):
        """
        Adds pupils to special group
        """
        for pupil_id in self.pupils_list:
            if len(PupilGroup.objects.filter(Group__Name=self.special_group_name,
                                             Pupil__id=pupil_id,
                                             AcademicYear=self.ac_year,
                                             Active=True)) == 0:
                new_pupil_group = PupilGroup(AcademicYear=self.ac_year,
                                             Pupil=Pupil.objects.get(id=pupil_id),
                                             Group=Group.objects.get(Name=self.special_group_name))
                new_pupil_group.save()
                cache.delete('Pupil_%s' % pupil_id)
        return

    def remove_pupils(self):
        """
        Deactivates pupil group records for a special group
        """
        for pupil_id in self.pupils_list:
            deactivating_pupil_groups = PupilGroup.objects.filter(Group__Name=self.special_group_name,
                                                                  Pupil__id=pupil_id,
                                                                  AcademicYear=self.ac_year,
                                                                  Active=True)
            for pupil_group in deactivating_pupil_groups:
                temp_pupil_group = pupil_group
                temp_pupil_group.Active = False
                temp_pupil_group.save()
                cache.delete('Pupil_%s' % pupil_id)
        return

    def removes_all_members_from_a_group(self, delete_records=False):
        """
        Deactivates pupil group records for a special group
        """
        for pupil_group in PupilGroup.objects.filter(Group__Name=self.special_group_name):
            temp_pupil_group = pupil_group
            if delete_records:
                temp_pupil_group.delete()
                cache.delete('Pupil_%s' % pupil_group.Pupil.id)
            else:
                temp_pupil_group.Active = False
                temp_pupil_group.save()
                cache.delete('Pupil_%s' % pupil_group.Pupil.id)
        return

    def delete_special_group(self):
        """
        Deletes a group
        """
        self.removes_all_members_from_a_group(delete_records=True)
        special_group = Group.objects.get(Name=self.special_group_name)
        special_group.delete()
        return