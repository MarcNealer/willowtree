# Django middleware imports
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect


# WillowTree system imports
from MIS.ViewExtras import SetMenu
from MIS.models import Staff
from MIS.models import StaffCpdRecords


def viewStaffCpdRecords(request, school, AcYear, StaffId):
    ''' displays a Staff members CPD page '''
    c = {'Staff': Staff.objects.get(id=int(StaffId)),
         'CpdData': StaffCpdRecords.objects.filter(Staff__id=int(StaffId),
                                                   Active=True),
         'school': school,
         'AcYear': AcYear}
    conInst = RequestContext(request, processors=[SetMenu])
    return render_to_response('StaffCpd.html', c,
                              context_instance=conInst)


def addStaffCpdRecord(request, school, AcYear, StaffId, modify=False):
    ''' adds or modifies a Staff members CPD record '''
    if modify:
        StaffCpdRecordsObj = StaffCpdRecords.objects.get(id=request.POST['cpdID'])
        if 'cpdName' in request.POST:
            StaffCpdRecordsObj.Name = request.POST['cpdName']
        if 'cpdProvider' in request.POST:
            StaffCpdRecordsObj.Provider = request.POST['cpdProvider']
    else:
        staffObj = Staff.objects.get(id=int(StaffId))
        StaffCpdRecordsObj = StaffCpdRecords(Staff=staffObj,
                                             Name=request.POST['cpdName'],
                                             Provider=request.POST['cpdProvider'])
    if 'cpdTimeTaken' in request.POST:
        StaffCpdRecordsObj.TimeTaken = request.POST['cpdTimeTaken']
    if 'cpdDateTaken' in request.POST:
        if request.POST['cpdDateTaken'] != '':
            StaffCpdRecordsObj.DateTaken = request.POST['cpdDateTaken']
    if 'cpdDuration' in request.POST:
        StaffCpdRecordsObj.Duration = request.POST['cpdDuration']
    if 'cpdExpiryDate' in request.POST:
        if request.POST['cpdExpiryDate'] != '':
            StaffCpdRecordsObj.DateExpires = request.POST['cpdExpiryDate']
        else:
            StaffCpdRecordsObj.DateExpires = None
    if 'cpdCertificateIssued' in request.POST:
        if 'yes' in request.POST['cpdCertificateIssued']:
            StaffCpdRecordsObj.CertIssued = True
        else:
            StaffCpdRecordsObj.CertIssued = False
    if 'cpdIntExt' in request.POST:
        StaffCpdRecordsObj.InternalExternal = request.POST['cpdIntExt']
    if 'cpdDetailsNotes' in request.POST:
        StaffCpdRecordsObj.Details = request.POST['cpdDetailsNotes']
    StaffCpdRecordsObj.save()
    httpAddress = '/WillowTree/%s/%s/staffCpd/%d/'
    tupleData = (school, AcYear, int(StaffId))
    return HttpResponseRedirect(httpAddress % tupleData)


def deleteStaffCpdRecord(request, school, AcYear, StaffId):
    ''' deletes a Staff members CPD record '''
    StaffCpdRecordsObj = StaffCpdRecords.objects.get(id=request.POST['cpdID'])
    StaffCpdRecordsObj.Active = False
    StaffCpdRecordsObj.save()
    httpAddress = '/WillowTree/%s/%s/staffCpd/%d/'
    tupleData = (school, AcYear, int(StaffId))
    return HttpResponseRedirect(httpAddress % tupleData)
