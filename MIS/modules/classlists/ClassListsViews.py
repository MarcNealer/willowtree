from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response
from django.template import loader, RequestContext

from MIS.models import *
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import *
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules.classlists import ClassListFunctions
from MIS.modules.GeneralFunctions import Get_PupilList


def PupilList(request,school,AcYear,GroupName,ListType):
    ''' This view is used to generate pupil based class lists.
    The type of list and the groupname are passed as arguments and
    the list type is used to define the html template file that is used to
    produce the class list.

    The data fed into these html templates will be the same for all lists. This
    includes the general data (Groupname, AcYear, school), plus a list collection
    of PupilRecord objects.'''
    PupilList=GeneralFunctions.Get_PupilList(AcYear,GroupName)
    ClassList=[]
    if GroupName.find('Applicant') > -1:
        AcYear='Applicants'
    for pupil in PupilList:
        ClassList.append(PupilRecord(pupil.id,AcYear))
    HTMLfile = '%sList.html' %ListType
    return render_to_response(HTMLfile,{'school':school,
                                        'AcYear':AcYear,
                                        'ClassList':sorted(ClassList, key=lambda pupil: pupil.Pupil.Surname.lower()),
                                        'GroupName':GroupName,
                                        'School':school,
                                        'AcYear':AcYear,
                                        'todaysDate': ClassListFunctions.todaysDate()},
                                        context_instance=RequestContext(request,processors=[SetMenu]))

def StaffList(request,school,AcYear,GroupName,ListType):
    ''' This view is used to generate Staff based class lists.
    The type of list and the groupname are passed as arguments and
    the list type is used to define the html template file that is used to
    produce the class list.

    The data fed into these html templates will be the same for all lists. This
    includes the general data (Groupname, AcYear, school), plus a list collection
    of StaffRecord objects.'''
    StaffList=GroupFunctions.Get_StaffList(AcYear,GroupName)
    ClassList=[]
    for Staff in StaffList:
        ClassList.append(StaffRecord(request,Staff.id,AcYear))
    HTMLfile = '%sList.html' % ListType
    return render_to_response(HTMLfile,{'school':school,
                                        'AcYear':AcYear,
                                        'StaffList':StaffList,
                                        'ClassList':sorted(ClassList, key=lambda Staff: Staff.Staff.Surname.lower()),
                                        'GroupName':GroupName,
                                        'School':school,
                                        'AcYear':AcYear,
                                        'todaysDate': ClassListFunctions.todaysDate()},
                                        context_instance=RequestContext(request,processors=[SetMenu]))

def GenerateClassList(request,school,AcYear,GroupName):
    c={'class': ClassListFunctions.GroupClassList(request.POST,GroupName,school,AcYear),
       'todaysDate': ClassListFunctions.todaysDate()}
    return render_to_response('ClassLists/BaseClassList.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def MultipleClassLists(request,school,AcYear):
    # trip list reports
    if 'TripsList' in request.POST['ClassList']:
        groups = {}
        data = ClassListFunctions.MultipleClassLists(request.POST,
                                                     school,
                                                     AcYear)
        for groupNameShort in data:
            groupName = Group.objects.filter(Name__iendswith=groupNameShort).filter(Name__icontains='Current')
            pupilList = Get_PupilList(AcYear,
                                      GroupName=groupNameShort)
            pupilDetails = []
            for pupil in pupilList:
                pupilDetails.append(PupilRecord(pupil.id, AcYear))
            groups[groupNameShort] = pupilDetails
        c = {'class': 'test', 'groups': groups, 'data': data}
        if 'medical' in request.POST['ClassList']:
            return render_to_response('tripsFormWithMedicalDetails_multi.html',c,
                                      context_instance=RequestContext(request,
                                                                      processors=[SetMenu]))
        else:
            return render_to_response('tripsForm_multi.html',c,
                                      context_instance=RequestContext(request,
                                                                      processors=[SetMenu]))
    # other reports
    else:
        c={'GroupList': ClassListFunctions.MultipleClassLists(request.POST,school,AcYear),
           'todaysDate': ClassListFunctions.todaysDate()}
        return render_to_response('ClassLists/MultipleClassListBase.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def SelectClassLists(request,school,AcYear):
    c={'formList':GeneralFunctions.GetForms(school),
       'School':school,
       'AcYear':AcYear,
       'todaysDate': ClassListFunctions.todaysDate()}
    return render_to_response('ClassListSelect.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def GenerateEmailList(request,school,AcYear,GroupName):
    c={'class': ClassListFunctions.EmailList(request.POST,GroupName,school,AcYear),
       'todaysDate': ClassListFunctions.todaysDate()}
    return render_to_response('ClassLists/EmailList.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

