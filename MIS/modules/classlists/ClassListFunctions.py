from MIS.models import *
from collections import OrderedDict
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.globalVariables.GlobalVariablesFunctions import *

def MultipleClassLists(PostData,school,AcYear):
    formlist=GeneralFunctions.GetForms(school)
    grouplist=OrderedDict()
    for forms in formlist:
        if forms.MenuName in PostData:
            grouplist[forms.MenuName]=GroupClassList(PostData,forms.Name,school,AcYear)
    return grouplist


def todaysDate():
    '''  returns todays date in UK format '''
    today = datetime.datetime.now()
    return '%s-%s-%s' % (today.day, today.month, today.year)


class GroupClassList():
    def __init__(self,PostData,GroupName,school,AcYear):
        self.GroupName=GroupName
        self.PostData=PostData
        self.SchoolName=GeneralFunctions.GetSchoolName(school)
        self.Teachers=TeacherNames(GroupName,AcYear).AllTeachers()
        self.AcYear=AcYear
        self.NewAcYear=AcYear
        if 'PupilSelect' in self.PostData:
            self.__setClassList__()
        else:
            self.__setGroupClassList__()
        if 'ClassList' in self.PostData:
            self.__setClassListDetails__()
        self.__setClassDetails__()

    def __setClassDetails__(self):
        groupsplit=self.GroupName.split('.')
        try:
            self.Name = groupsplit[-1]
        except:
            self.Name = 'None'
        try:
            self.Year = groupsplit[-3]
        except:
            self.Year = 'None'
        if 'Year' in self.Year:
            self.Year=self.Year.split('Year')[1]
        return

    def NumberOfPupils(self):
        return len(self.ClassList)

    def NumberOfGirls(self):
        Counter=0
        for items in self.ClassList:
            if items.Pupil.Gender =='F':
                Counter += 1
        return Counter

    def HouseNumbers(self):
        data={'More':0,'Lawrence':0,'Becket':0,'Hardy':0}
        for items in self.ClassList:
            for house in data:
                if PupilGroup.objects.filter(Pupil=items.Pupil,Group__Name__icontains=house,Active=True).exists():
                    data[house]=data[house]+1
        return data

    def NumberOfBoys(self):
        Counter=0
        for items in self.ClassList:
            if items.Pupil.Gender =='M':
                Counter += 1
        return Counter

    def __setClassListDetails__(self):
        self.ClassListFile="ClassLists/%s.html" % self.PostData['ClassList']
        self.ClassListName="%s List" % self.PostData['ClassList']
        return

    def __setClassList__(self):
        self.ClassList=[]
        for Pupil_Id in self.PostData.getlist('PupilSelect'):
            self.ClassList.append(PupilRecord(Pupil_Id,self.AcYear))
        self.ClassList=sorted(self.ClassList, key=lambda pupil: pupil.Pupil.Surname.lower())
        return

    def __setGroupClassList__(self):
        self.ClassList=[]
        PupilList=PupilGroup.objects.filter(Group__Name=self.GroupName,AcademicYear=self.AcYear,Active=True)
        for Pupil_Id in PupilList:
            self.ClassList.append(PupilRecord(Pupil_Id.Pupil.id,self.AcYear))
        self.ClassList=sorted(self.ClassList, key=lambda pupil: pupil.Pupil.Surname.lower())
        return

    def BirthdayList(self):
        testdate=datetime.datetime.today().month
        returnlist=[]
        for recs in self.ClassList:
            if recs.Pupil.DateOfBirth.month == testdate:
                returnlist.append(recs)
        return sorted(returnlist,key = lambda x:(x.Pupil.DateOfBirth.month,x.Pupil.DateOfBirth.day))

    def BirthdayListNext(self):
        testdate=datetime.datetime.today().month + 1
        if testdate > 12:
            testdate = 1
        returnlist=[]
        for recs in self.ClassList:
            if recs.Pupil.DateOfBirth.month == testdate:
                returnlist.append(recs)
        return sorted(returnlist,key = lambda x:(x.Pupil.DateOfBirth.month,x.Pupil.DateOfBirth.day))

    def BirthdayListAll(self):
        return sorted(self.ClassList,key = lambda x:(x.Pupil.DateOfBirth.month,x.Pupil.DateOfBirth.day))

    def ThisMonthImage(self):
        testdate=datetime.datetime.today().month
        return self.__getImage__(testdate)

    def NextMonthImage(self):
        testdate=datetime.datetime.today().month+1
        if testdate > 12:
            testdate=1
        return self.__getImage__(testdate)

    def __getImage__(self,testdate):
        if testdate==1:
            return "images/January.jpg"
        elif testdate==2:
            return "images/February.jpg"
        elif testdate==3:
            return "images/March.jpg"
        elif testdate==4:
            return "images/April.jpg"
        elif testdate==5:
            return "images/May.jpg"
        elif testdate==6:
            return "images/June.jpg"
        elif testdate==7:
            return "images/July.jpg"
        elif testdate==8:
            return "images/July.jpg"
        elif testdate==9:
            return "images/September.jpg"
        elif testdate==10:
            return "images/October.jpg"
        elif testdate==11:
            return "images/November.jpg"
        else:
            return "images/December.jpg"

class GroupClassLists():
    def __init__(self,school,AcYear,NewAcYear,PostData):
        self.GroupList={}
        self.FormList=GeneralFunctions.GetForms(school)
        self.PostData=PostData
        self.school=school
        self.AcYear=AcYear
        self.NewAcYear=NewAcYear
        self.__generate__()
        self.ClassListFile="ClassLists/%s.html" % self.PostData['ClassList']

    def __generate__(self):
        for forms in self.FormList:
            if forms.MenuName in self.PostData:
                self.GroupList[forms.MenuName]=PotentialGroup(self.school,forms.Name,self.AcYear,self.NewAcYear)
    def getGroupList():
        return self.GroupList


class EmailList(GroupClassList):
    def __init__(self,PostData,GroupName,school,AcYear):
        self.GroupName=GroupName
        self.PostData=PostData
        self.SchoolName=GeneralFunctions.GetSchoolName(school)
        self.AcYear=AcYear
        self.NewAcYear=AcYear
        self.__setClassList__()
