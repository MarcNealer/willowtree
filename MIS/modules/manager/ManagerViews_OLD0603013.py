# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q
from django.conf import settings
from django.core.servers.basehttp import FileWrapper
import csv

# WillowTree system imports

from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.notes import NoteFunctions
from MIS.modules.LettersLabels import LetterFunctions
from MIS.modules.gradeInput import GradeFunctions
from MIS.modules.manager import ManagerFunctions


def Manage(request,school,AcYear,GroupName,ManageType):
    ''' General Manager views. This view is the main one used by teacher
    to work with a class. It shows the list of pupils, some selected
    data, plus a series of actions from a dropdown.'''
    PupilYear=AcYear
    if GroupName.find('Alumni') > -1:
        PupilYear='Alumni'
    elif GroupName.find('Applicants.Holding') > -1 or GroupName.find('Applicants.Transfer') > -1:
        PupilYear='Holding'
    if ManageType=='This':
        PupilList=GeneralFunctions.Get_PupilListExact(PupilYear,GroupName)
        Type='This'
    else:
        PupilList=GeneralFunctions.Get_PupilList(PupilYear,GroupName)
        Type='All'
    if GroupName.find('Current') > -1:
        ManagerTemplate='CurrentManager.html'
    elif GroupName.find('Applicants.Holding') > -1:
        ManagerTemplate='HoldingManager.html'
    elif GroupName.find('Applicants') > -1:
        ManagerTemplate='ApplicantsManager.html'
    elif GroupName.find('Alumni') > -1:
        ManagerTemplate='AlumniManager.html'
    elif GroupName.find('Transfer') > -1:
        ManagerTemplate='TransferManager.html'        
    #gets a list of year classes only...
    years = ['1','2','3','4','5','6','7','8','9','10']
    counter = 0
    listOfYearClasses = ['Applicants.%s.Accepted.Reception' % MenuTypes.objects.get(Name=school).SchoolId.Name]
    x = Group.objects.filter(Name__icontains="Applicants.%s.Accepted." % MenuTypes.objects.get(Name=school).SchoolId.Name)
    for i in range(len(x)):
        try:
		 x = Group.objects.get(Name="Applicants.%s.Accepted.Year%s" % (MenuTypes.objects.get(Name=school).SchoolId.Name,years[counter]))
		 listOfYearClasses.append(x)
        except:
            pass
        counter += 1      
    ClassList=[]        
    for pupil in PupilList: 
        ClassList.append(PupilRecord(pupil.id,AcYear))
    ClassListLength = str(len(ClassList))
    mainGroupLists=ManagerFunctions.ApplicantSubGroups(school,GroupName)        
    gradeObj = GradeFunctions.SelectGradeReports(GroupName)
    currentGradeRecord = str(gradeObj.GetCurrentSeason())
    ClassList = sorted(ClassList, key=lambda pupil: pupil.Pupil.Surname)
    c={'school':school,'AcYear':AcYear,'ClassList':ClassList,'LetterList':LetterFunctions.LetterManager.GetlistOfLetters(GroupName),
       'GroupName':GroupName,'School':school,'AcYear':AcYear,'KWList':NoteFunctions.KeywordList('Note'),'GroupList':GeneralFunctions.SchoolGroups(school)}
    c.update({'MainGroups':mainGroupLists})
    c.update(mainGroupLists.ReturnDictionary())
    c.update({'ListOfYearClasses':listOfYearClasses})
    c.update({'schoolName':mainGroupLists.School})
    c.update({'gradeObj': gradeObj, 'currentGradeRecord': currentGradeRecord})
    c.update({'ClassListLength': ClassListLength})
    if "Applicants.%s.Accepted." % MenuTypes.objects.get(Name=school).SchoolId.Name in GroupName:
        listOfClasses = []
        listOfClassesQuerySet = Group.objects.filter(Name__icontains='%s.' % GroupName)
        for i in listOfClassesQuerySet:
            listOfClasses.append(i.Name) 
        c.update({'ListOfClasses':listOfClasses})    
    return render_to_response(ManagerTemplate,c,context_instance=RequestContext(request,processors=[SetMenu]))

def ReturnCSV(request,school,AcYear,GroupName,FileName,ManageType):
    ''' This View is used to generate a basic CSV file for teachers to download
    and use in '''
    PupilYear=AcYear
    if GroupName.find('Alumni') > -1:
        PupilYear='Alumni'
    elif GroupName.find('Applicants.Holding') > -1 or GroupName.find('Applicants.Transfer') > -1:
        PupilYear='Holding'
    if ManageType=='This':
        PupilList=GeneralFunctions.Get_PupilListExact(PupilYear,GroupName)
        Type='This'
    else:
        PupilList=GeneralFunctions.Get_PupilList(PupilYear,GroupName)
        Type='All'
    ClassList=[]
    for pupil in PupilList:
        ClassList.append(PupilRecord(pupil.id,AcYear))        
    response= HttpResponse(mimetype="text/csv")
    response_writer= csv.writer(response)
    response_writer.writerow(['id','FirstName','Surname','Gender','DOB','House','Form'])
    for Records in ClassList:
        response_writer.writerow([Records.Pupil.id,Records.Pupil.FirstName(),Records.Pupil.Surname,Records.Pupil.Gender,Records.Pupil.DateOfBirth.strftime('%d/%m/%Y'),Records.AcHouse,Records.Form()])
    response['Content-Disposition']='attachment; filename=pupillist.csv'
    return response
        


def CopyAPupil(request,school,AcYear,GroupName):
    ''' This is a proccesses requests from manager views to copy a pupil into 
    a group. It just requires the new group name, the pupil and the Academic year'''
    GroupRec=Group.objects.get(Name=request.POST['NewGroup'])
    Counter = 0
    AlreadyExists=0
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        if GroupName.find('Applicants') > -1:
            if not PupilGroup.objects.filter(Pupil=Pupil.objects.get(id=int(Pupil_Id)),AcademicYear='Applicants',Group=GroupRec).exists():
                NewPupilGroup=PupilGroup(Pupil=Pupil.objects.get(id=int(Pupil_Id)),AcademicYear='Applicants',Group=GroupRec)
                NewPupilGroup.save()
                cache.delete("Pupil_%s" % Pupil_Id)
                Counter +=1
            else:
                AlreadyExists+=1
            
    if Counter > 0:
        request.session['Messages']=['%d Pupils copied into %s' % (Counter, GroupName)]
    else:
        request.session['Messages']=['Error : No pupils were copied']
    if AlreadyExists > 0:
        request.session['Messages'].append('%d Pupils already exit is the target Group' % AlreadyExists)
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear, GroupName))
    
def MoveAPupil(request,school,AcYear,GroupName):
    ''' This view proccesses forms from the manager views to move a pupil
    from the current group into a new group. The groupname passed
    in the URL is the old group name and the new group is passed in the
    request form object. After proccessing, this returns to the manager 
    screen'''
    GroupRec=Group.objects.get(Name=request.POST['NewGroup'],) 
    OldGroup=Group.objects.get(Name=GroupName)    
    Counter = 0
    AlreadyExists=0
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        OldPupilGroup=PupilGroup.objects.get(Pupil=Pupil.objects.get(id=int(Pupil_Id)),AcademicYear=AcYear,Group=OldGroup,Active=True)
        OldPupilGroup.Active=False
        OldPupilGroup.save()
        cache.delete("Pupil_%s" % Pupil_Id)
        if not PupilGroup.objects.filter(Pupil=Pupil.objects.get(id=int(Pupil_Id)),AcademicYear=AcYear,Group=GroupRec,Active=True).exists():
            if str(GroupRec) == 'Applicants.Holding':  
                if PupilGroup.objects.filter(Pupil__id=int(Pupil_Id),Group__Name='Applicants.Holding',Active=True).exists():  
                    pass
                else:
                    NewPupilGroup=PupilGroup(Pupil=Pupil.objects.get(id=int(Pupil_Id)),AcademicYear='Holding',Group=GroupRec)
                    NewPupilGroup.save() 
                    cache.delete("Pupil_%s" % Pupil_Id)
            else:
                NewPupilGroup=PupilGroup(Pupil=Pupil.objects.get(id=int(Pupil_Id)),AcademicYear=AcYear,Group=GroupRec)
                NewPupilGroup.save()
                cache.delete("Pupil_%s" % Pupil_Id)
            Counter +=1  
            if 'ApplicantInterviewTime' in request.POST or 'ApplicantInterviewDate' in request.POST: 
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)          
                if 'ApplicantInterviewTime' in request.POST:
                    ExtraFields.WriteExtention('PupilExtra',{'ApplicantInterviewTime':request.POST['ApplicantInterviewTime'].split('T')[1]})
                if 'ApplicantInterviewDate' in request.POST:
                    ExtraFields.WriteExtention('PupilExtra',{'ApplicantInterviewDate':request.POST['ApplicantInterviewDate']})
            if 'declinedReason' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)        
                ExtraFields.WriteExtention('PupilExtra',{'declinedReason':request.POST['declinedReason']}) 
            if 'withdrawnReason' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)        
                ExtraFields.WriteExtention('PupilExtra',{'withdrawnReason':request.POST['withdrawnReason']})
            if 'declinedPlaceReason' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)        
                ExtraFields.WriteExtention('PupilExtra',{'declinedPlaceReason':request.POST['declinedPlaceReason']}) 
            if 'declinedInterviewReason' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)        
                ExtraFields.WriteExtention('PupilExtra',{'declinedInterviewReason':request.POST['declinedInterviewReason']})
            if 'declinedAfterDepositReason' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)        
                ExtraFields.WriteExtention('PupilExtra',{'declinedAfterDepositReason':request.POST['declinedAfterDepositReason']})                                              
        else:
            AlreadyExists+=1                    
    if Counter > 0:
        request.session['Messages']=['%d Pupils Moved to %s' % (Counter, request.POST['NewGroup'])]
    else:
        request.session['Messages']=['Error : No pupils were Moved']
    if AlreadyExists > 0:
        request.session['Messages'].append('%d Pupils already exist in the target Group and have been removed from this one' % AlreadyExists)
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear, GroupName))
    
def MoveAnApplicant(request,school,AcYear,GroupName):
    ''' This view is used to move an applicant from one applicant group to another.
        An applicant may only be in one applicant group for a selected school '''
    GroupRec=Group.objects.get(Name=request.POST['NewGroup'])
    Counter = 0
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        PupilRec=Pupil.objects.get(id=int(Pupil_Id))
        OldPupilGroups=PupilGroup.objects.filter(Pupil=PupilRec,AcademicYear='Applicants',Group__Name__icontains=MenuTypes.objects.get(Name=school).SchoolId.Name)
        for groups in OldPupilGroups:
            groups.Active=False
            groups.save()
            cache.delete("Pupil_%s" % Pupil_Id)
        NewPupilGroup=PupilGroup(Pupil=Pupil.objects.get(id=int(Pupil_Id)),AcademicYear='Applicants',Group=GroupRec)
        NewPupilGroup.save()
        cache.delete("Pupil_%s" % Pupil_Id)        
        Counter +=1
            
    if Counter > 0:
        request.session['Messages']=['%d Pupils Moved to %s' % (Counter, GroupName)]
    else:
        request.session['Messages']=['Error : No pupils were Moved']
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear, GroupName))    

def MoveFromHolding(request,school,AcYear,GroupName):
    ''' This view is used to move an applicant from Holding to an applicant group.
        An applicant may only be in one applicant group for a selected school '''
    GroupRec=Group.objects.get(Name=request.POST['NewGroup'])
    HoldingRec=Group.objects.get(Name__icontains='Applicants.Holding')
    Counter = 0   
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        PupilRec=Pupil.objects.get(id=int(Pupil_Id))
        if PupilGroup.objects.filter(Pupil=PupilRec,Group=HoldingRec).exists():
            HoldingRec=PupilGroup.objects.get(Pupil=PupilRec,Group=HoldingRec)
            HoldingRec.Active=False
            HoldingRec.save()
            cache.delete("Pupil_%s" % Pupil_Id)
        OldPupilGroups=PupilGroup.objects.filter(Pupil=PupilRec,AcademicYear=AcYear,Group__Name__icontains=MenuTypes.objects.get(Name=school).SchoolId.Name)
        for groups in OldPupilGroups:
            groups.Active=False
            groups.save()
            cache.delete("Pupil_%s" % Pupil_Id)
        NewPupilGroup=PupilGroup(Pupil=PupilRec,AcademicYear=AcYear,Group=GroupRec)
        NewPupilGroup.save()
        cache.delete("Pupil_%s" % Pupil_Id)
        Counter +=1
       
        if Counter > 0:
            request.session['Messages']=['%d Pupils Moved to %s' % (Counter, GroupRec.Name)]
        else:
            request.session['Messages']=['Error : No pupils were Moved']
        return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear, GroupName)) 
        
def RemoveFromHolding(request,school,AcYear,GroupName,PupilNo):
    ''' This view just removes the pupil from the holding group. This is to be used 
    when a child has already been copied into another group and the link to holding
    is no longer required'''
    if PupilGroup.objects.filter(Pupil__id=int(PupilNo)).count() > 1:
        HoldingRec=PupilGroup.objects.get(Pupil__id=int(PupilNo),Group__Name='Applicants.Holding')
        HoldingRec.Active=False
        HoldingRec.save()
        cache.delete("Pupil_%s" % PupilNo)
        request.session['Messages']=['Pupils Removed successfully']
    else:
        request.session['Messages']=['Error : This Pupil is not in any other groups and cannot be removed']        
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear,GroupName))         

def UpdateApplicant(request,school,AcYear,GroupName,PupilNo):
    ''' This method is used to update the basic info on a pupil record from forms
    embeded in the manager screens'''
    Rec=UpdatePupilRecord(request,AcYear,school,PupilNo)
    Rec.Base()
    request.session['Messages']=['Pupil Updated']
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear, GroupName))     

    
    pass

def RemoveApplicant(request,school,AcYear,GroupName,PupilNo):
    ''' This View removes a Applicant pupil Record from the system completely.
    This view should only be available from the holding group'''
    if FamilyChildren.objects.filter(Pupil__id=int(PupilNo)).exists():
        FamilyChildren.objects.filter(Pupil__id=int(PupilNo)).delete()
    if PupilGroup.objects.filter(Pupil__id=int(PupilNo)).exists():
        PupilGroup.objects.filter(Pupil__id=int(PupilNo)).delete()
    Pupil.objects.get(id=int(PupilNo)).delete()
    request.session['Messages']=['Pupil Deleted']
    return HttpResponseRedirect('/WillowTree/%s/%s/Manage/%s/' % (school, AcYear, GroupName))    
    
def MoveCopyFromHolding(request,school,AcYear,PupilNo): 
    ''' This view proccesses a move or copy request from
    the manager holding page. '''
    if 'AppApplyForYear' in request.POST:
        RequiredYear = request.POST['AppApplyForYear']
    else:
        RequiredYear = ''
    SchoolName = MenuTypes.objects.get(Name=school).SchoolId.Name
    if PupilGroup.objects.filter(Pupil__id=int(PupilNo),
                                 Group__Name__istartswith="Applicants.%s.Main" % (SchoolName),
                                 Active=True).exists():
        if request.POST['MoveType'] == 'Move':    
            pupilGroupHolding = PupilGroup.objects.get(Pupil__id=PupilNo,Group__Name="Applicants.Holding",Active=True)
            pupilGroupHolding.Active = False
            pupilGroupHolding.save()
            cache.delete("Pupil_%s" % PupilNo)
        return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/Applicants.Holding/' % (school, AcYear))
    else:
        PupilRec = Pupil.objects.get(id=int(PupilNo))
        if request.POST['GroupType'] == 'Main':
            GroupRec=Group.objects.get(Name='Applicants.%s.Main.%s' % (SchoolName, RequiredYear))
        else:
            GroupRec = Group.objects.get(Name='Applicants.%s.Reserve' % (SchoolName))
        NewPupilGroup = PupilGroup(Pupil=PupilRec,AcademicYear=request.POST['AppAcYear'],
                                   Group=GroupRec,Active=True)
        NewPupilGroup.save()
        cache.delete("Pupil_%s" % PupilNo)
        if request.POST['MoveType']=='Move':
            HoldingRec=PupilGroup.objects.get(Pupil=PupilRec,Group__Name='Applicants.Holding',Active=True)
            HoldingRec.Active=False
            HoldingRec.save()
            cache.delete("Pupil_%s" % PupilNo)
        return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/Applicants.Holding/' % (school, AcYear))             
        
def MoveCopyToKindie(request,school,AcYear,PupilNo):
    '''
    This view processes the form from holding to add an applicant
    as a Kindergarten applicant. It is proccesses differently from the
    main schools as the two schools are managed as one'''
    PupilRec=Pupil.objects.get(id=int(PupilNo))
    if request.POST['School']=='Pimlico':
        GroupRec=Group.objects.get(Name='Applicants.Kindergarten.PimMain')
    else:
        GroupRec=Group.objects.get(Name='Applicants.Kindergarten.BatMain')
    NewPupilGroup=PupilGroup(Pupil=PupilRec,AcademicYear=request.POST['AppAcYear'],Group=GroupRec)
    NewPupilGroup.save()
    cache.delete("Pupil_%s" % PupilNo)
    if request.POST['MoveType']=='Move':
        HoldingRec=PupilGroup.objects.get(Pupil=PupilRec,Group__Name='Applicants.Holding')
        HoldingRec.Active=False
        HoldingRec.save()
        cache.delete("Pupil_%s" % PupilNo)        
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/Applicants.Holding/' % (school, AcYear))   





                  
    