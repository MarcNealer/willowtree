'''

Change Log:
12/03/12: dbmgr - Updated MoveToKindie to check to see if the Record exists before
                  Creating one.

'''

# Base python libray imports

import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q
from django.conf import settings
from django.core.servers.basehttp import FileWrapper
import csv
import logging
log = logging.getLogger(__name__)
from django.core.cache import cache

# WillowTree system imports
from MIS.modules.notes import NoteFunctions
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.notes import NoteFunctions
from MIS.modules.LettersLabels import LetterFunctions
from MIS.modules.gradeInput import GradeFunctions
from MIS.modules.manager import ManagerFunctions
from MIS.modules.StaffRecs import *
from MIS.modules.transfers import transferViews
from MIS.modules.emailFromWillowTree.emailFunctions import newEmailAccountRequest
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from MIS.modules.SEN import SENFunctions
from MIS.modules.SpecialGroups.SpecialGroupsFunctions import GetSpecialGroups


def Manage(request, school, AcYear, GroupName, ManageType,
           senView=False,
           ealView=False,
           gtView=False,
           moreAbledView=False):
    ''' General Manager views. This view is the main one used by teacher
    to work with a class. It shows the list of pupils, some selected
    data, plus a series of actions from a dropdown.'''
    StaffManager = False
    PupilYear=AcYear
    if GroupName.find('Alumni') > -1:
        PupilYear='Alumni'
    elif GroupName.find('Applicants.Holding') > -1 or GroupName.find('Applicants.Transfer') > -1:
        PupilYear='Holding'
    if ManageType=='This':
        PupilList=GeneralFunctions.Get_PupilListExact(PupilYear,GroupName)
        Type='This'
    else:
        PupilList=GeneralFunctions.Get_PupilList(PupilYear,GroupName)
        Type='All'
    if GroupName.find('Current') > -1:
        ManagerTemplate='CurrentManager.html'
    if GroupName.find('Special') > -1:
        ManagerTemplate='CurrentManager.html'
    elif GroupName.find('Applicants.Holding') > -1:
        ManagerTemplate='HoldingManager.html'
    elif GroupName.find('Applicants') > -1:
        ManagerTemplate='ApplicantsManager.html'
    elif GroupName.find('Alumni') > -1:
        ManagerTemplate='AlumniManager.html'
    elif GroupName.find('Transfer') > -1:
        ManagerTemplate='TransferManager.html'
    elif GroupName.find('Staff') > -1:
        ManagerTemplate = 'StaffManager.html'
        StaffManager = True
    schoolName = MenuTypes.objects.get(Name=school).SchoolId.Name
    if StaffManager:
        # staff manager code...
        staffRec = getAllStaffForAGrp(GroupName, request, AcYear)
        staffGroups = Group.objects.filter(Name__icontains='Staff')
        staffGroups = staffGroups.filter(Name__icontains=schoolName)
        letterList = LetterFunctions.LetterManager.GetlistOfLetters(GroupName)
        c = {'school': school, 'AcYear': AcYear,
             'LetterList': letterList,
             'GroupName': GroupName, 'School': school, 'AcYear': AcYear,
             'KWList': NoteFunctions.KeywordList('Note'),
             'staffRecs': staffRec, 'staffGroups': staffGroups,
             'userEmailAddress': GeneralFunctions.getUserEmailAddress(request)}
        return render_to_response(ManagerTemplate,c,context_instance=RequestContext(request,processors=[SetMenu]))
    else:
        #gets a list of year classes only...
        years = ['1','2','3','4','5','6','7','8','9','10']
        counter = 0
        listOfYearClasses = ['Applicants.%s.Accepted.Reception' % MenuTypes.objects.get(Name=school).SchoolId.Name]
        x = Group.objects.filter(Name__icontains="Applicants.%s.Accepted." % MenuTypes.objects.get(Name=school).SchoolId.Name)
        for i in range(len(x)):
            try:
    		 x = Group.objects.get(Name="Applicants.%s.Accepted.Year%s" % (MenuTypes.objects.get(Name=school).SchoolId.Name,
                                                                          years[counter]))
    		 listOfYearClasses.append(x.Name)
            except:
                pass
            counter += 1
        #ClassList=PupilThreadTest.Get_PupilListThread(PupilYear,GroupName)
        ClassList=[]
        for pupil in PupilList:
            ClassList.append(PupilRecord(pupil.id,AcYear))
        ClassListLength = str(len(ClassList))
        mainGroupLists=ManagerFunctions.ApplicantSubGroups(school,GroupName)
        gradeObj = GradeFunctions.SelectGradeReports(GroupName)
        currentGradeRecord = str(gradeObj.GetCurrentSeason())
        ClassList = sorted(ClassList, key=lambda pupil: pupil.Pupil.Surname.lower())
        # view sen only...senView
        if senView == True:
            senClassList = []
            for pupil in ClassList:
                if bool(pupil.ReturnCurrentActiveSen()) == True:
                    senClassList.append(pupil)
            ClassList = senClassList
        # view eal only...
        if ealView == True:
            senClassList = []
            for pupil in ClassList:
                if bool(pupil.ReturnCurrentActiveEal()) == True:
                    senClassList.append(pupil)
            ClassList = senClassList
        # end
        # view gt only...
        if gtView == True:
            gtClassList = []
            for pupil in ClassList:
                if bool(pupil.ReturnCurrentActiveGT()) == True:
                    gtClassList.append(pupil)
            ClassList = gtClassList
        # end
        # view more abled only...
        if moreAbledView == True:
            moreAbledClassList = []
            for pupil in ClassList:
                if bool(pupil.ReturnCurrentActiveMoreAbled()) == True:
                    moreAbledClassList.append(pupil)
            ClassList = moreAbledClassList
        # end
        ReportingSeasons=[x.GradeRecord.Name for x in ReportingSeason.objects.all()]
        ReportingSeasons.sort()
        if 'Year' in GroupName:
            AnalysisYr='Yr%s' % GroupName.split('Year')[1].split('.')[0]
            AnalysisSubjectList=set([x.GradeSubject.Name for x in GradeInputElement.objects.filter(GradeRecord__Name__istartswith='Yr%s' % GroupName.split('Year')[1].split('.')[0]).exclude(GradeSubject__Name__icontains='comment').exclude(GradeSubject__Name__icontains='Extra').exclude(GradeSubject__Name__icontains='Individual').exclude(GradeSubject__Name__icontains='Support')])
            AnalysisSubjectList=sorted(AnalysisSubjectList.union(set([x.Subject.Name for x in ParentSubjects.objects.all()])))

        else:
            AnalysisSubjectList=[]

        start_date = datetime.date.today() - datetime.timedelta(days=7)
        c={'school': school,
           'AcYear':AcYear,
           'ClassList':ClassList,
           'LetterList':LetterFunctions.LetterManager.GetlistOfLetters(GroupName),
           'GroupName':GroupName,
           'School':school,
           'KWList':NoteFunctions.KeywordList('Note'),
           'GroupList':GeneralFunctions.SchoolGroups(school),
           'userEmailAddress': GeneralFunctions.getUserEmailAddress(request),
           'SubjectList':Subject.objects.all(),
           'AnalysisSubjectList':AnalysisSubjectList,
           'ReportingSeasons':set(ReportingSeasons),
           'TeacherNames':TeacherNames(GroupName,AcYear),
           'ExtraSenOptions': SENFunctions.ExtraSenOptions(keywordsOnly=True),
           'statsData': ManagerFunctions.statsDataOnGroup(schoolName,
                                                          GroupName,
                                                          ClassList),
           'start_date': start_date.isoformat(),
           'end_date': datetime.date.today().isoformat()}
        # notes report code - roy
        keywords = NoteFunctions.KeywordList('Notes')
        c.update({'KWList2': keywords})
        # end
        if senView == True:
            c.update({'senView': 'True'})
        if ealView == True:
            c.update({'ealView': 'True'})
        if gtView == True:
            c.update({'gtView': 'True'})
        if moreAbledView == True:
            c.update({'moreAbledView': 'True'})
        # whole school code...
        if len(GroupName.split('.')) == 2:
            c.update({'wholeSchool': 'True'})
        # end
        c.update({'MainGroups':mainGroupLists})
        c.update(mainGroupLists.ReturnDictionary())
        c.update({'ListOfYearClasses':listOfYearClasses})
        c.update({'schoolName':mainGroupLists.School})
        c.update({'gradeObj': gradeObj, 'currentGradeRecord': currentGradeRecord})
        c.update({'ClassListLength': ClassListLength})
        if "Applicants.%s.Accepted." % MenuTypes.objects.get(Name=school).SchoolId.Name in GroupName:
            listOfClasses = []
            listOfClassesQuerySet = Group.objects.filter(Name__icontains='%s.' % GroupName)
            for i in listOfClassesQuerySet:
                listOfClasses.append(i.Name)
            c.update({'ListOfClasses':listOfClasses})
        if GroupName.find('Applicants') > -1:
            # get current school grps
            currentFormGrps = Group.objects.filter(Name__icontains="Current").filter(Name__icontains="Form").filter(Name__icontains=MenuTypes.objects.get(Name=school).SchoolId.Name)
            currentFormGrps2 = []
            for group in currentFormGrps:
                if len(group.Name.split('.')) == 6:
                    currentFormGrps2.append(group)
            # end
            # get kindergarten current grps
            currentKindergartenGrps = Group.objects.filter(Name__icontains="Current").filter(Name__icontains="kindergarten")
            currentKindergartenGrps2 = []
            for group in currentKindergartenGrps:
                if len(group.Name.split('.')) == 4:
                    currentKindergartenGrps2.append(group)
            # end
            c.update({'currentFormGrps': currentFormGrps2})
            c.update({'currentKindergartenGrps': currentKindergartenGrps2})
            c.update({'pastSchools': SchoolList.objects.all().order_by('Name')})
            c.update({'TypeofSchool': ['KinderGarten', 'Prep', 'Secondary']})
        c.update({"special_groups": GetSpecialGroups(school, AcYear).get_special_groups()})
        return render_to_response(ManagerTemplate,c,context_instance=RequestContext(request,processors=[SetMenu]))

def ReturnCSV(request,school,AcYear,GroupName,FileName,ManageType):
    ''' This View is used to generate a basic CSV file for teachers to download
    and use in '''
    PupilYear=AcYear
    if GroupName.find('Alumni') > -1:
        PupilYear='Alumni'
    elif GroupName.find('Applicants.Holding') > -1 or GroupName.find('Applicants.Transfer') > -1:
        PupilYear='Holding'
    if ManageType=='This':
        PupilList=GeneralFunctions.Get_PupilListExact(PupilYear,GroupName)
        Type='This'
    else:
        PupilList=GeneralFunctions.Get_PupilList(PupilYear,GroupName)
        Type='All'
    ClassList=[]
    for pupil in PupilList:
        ClassList.append(PupilRecord(pupil.id,AcYear))
    response= HttpResponse(mimetype="text/csv")
    response_writer= csv.writer(response)
    if 'Applicants' in GroupName:
        response_writer.writerow(['id',
                                  'FirstName',
                                  'Surname',
                                  'Gender',
                                  'DOB',
                                  'Groups'])
        for Records in ClassList:
            listOfGroups = ''
            for i in Records.GroupList():
                listOfGroups += '%s,  ' % i.Group.Name
            response_writer.writerow([Records.Pupil.id,
                                      Records.Pupil.FirstName(),
                                      Records.Pupil.Surname,
                                      Records.Pupil.Gender,
                                      Records.Pupil.DateOfBirth.strftime('%d/%m/%Y'),
                                      listOfGroups])
    else:
        response_writer.writerow(['id',
                                  'FirstName',
                                  'Surname',
                                  'Gender',
                                  'DOB',
                                  'House',
                                  'Form'])
        for Records in ClassList:
            response_writer.writerow([Records.Pupil.id,
                                      Records.Pupil.FirstName(),
                                      Records.Pupil.Surname,
                                      Records.Pupil.Gender,
                                      Records.Pupil.DateOfBirth.strftime('%d/%m/%Y'),
                                      Records.AcHouse,
                                      Records.Form()])
    response['Content-Disposition'] = 'attachment; filename=pupillist.csv'
    return response


def ReturnCSV2(request, school, AcYear, GroupName, FileName, ManageType):
    return ManagerFunctions.ReturnCSV(request,
                                      AcYear,
                                      GroupName,
                                      FileName,
                                      ManageType).CreateCSV()


def CopyAPupil(request,school,AcYear,GroupName):
    ''' This is a proccesses requests from manager views to copy a pupil into
    a group. It just requires the new group name, the pupil and the Academic year'''
    GroupRec=Group.objects.get(Name=request.POST['NewGroup'])
    Counter = 0
    AlreadyExists=0
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        if GroupName.find('Applicants') > -1:
            if not PupilGroup.objects.filter(Pupil=Pupil.objects.get(id=int(Pupil_Id)),
                                             AcademicYear='Applicants',
                                             Group=GroupRec).exists():
                if request.POST['NewGroup'] == 'Applicants.Holding':
                    NewPupilGroup=PupilGroup(Pupil=Pupil.objects.get(id=int(Pupil_Id)),
                                             AcademicYear='Holding',
                                             Group=GroupRec)
                else:
                    NewPupilGroup=PupilGroup(Pupil=Pupil.objects.get(id=int(Pupil_Id)),
                                             AcademicYear='Applicants',
                                             Group=GroupRec)
                NewPupilGroup.save()
                cache.delete("Pupil_%s" % Pupil_Id)
                Counter +=1
            else:
                AlreadyExists+=1

    if Counter > 0:
        request.session['Messages']=['%d Pupils copied into %s' % (Counter, GroupName)]
    else:
        request.session['Messages']=['Error : No pupils were copied']
    if AlreadyExists > 0:
        request.session['Messages'].append('%d Pupils already exit is the target Group' % AlreadyExists)
    try:
        log.warn('Pupils (%s) were copied to %s by %s' % (" ".join(request.POST.getlist('PupilSelect')),
                                                          NewPupilGroup.Group.Name,
                                                          request.user.username))
    except:
         log.warn('Pupils (%s) were copied to %s by %s' % (" ".join(request.POST.getlist('PupilSelect')),
                                                          '<ERROR - COULD NOT GET GRP>',
                                                          request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageAll/%s/' % (school, AcYear, GroupName))


def MoveAPupil(request,school,AcYear,GroupName):
    ''' This view proccesses forms from the manager views to move a pupil
    from the current group into a new group. The groupname passed
    in the URL is the old group name and the new group is passed in the
    request form object. After proccessing, this returns to the manager
    screen'''
    GroupRec=Group.objects.get(Name=request.POST['NewGroup'])
    OldGroup=Group.objects.get(Name=GroupName)
    Counter = 0
    AlreadyExists=0
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        OldPupilGroup=PupilGroup.objects.get(Pupil=Pupil.objects.get(id=int(Pupil_Id)),
                                             AcademicYear=AcYear,
                                             Group=OldGroup,
                                             Active=True)
        OldPupilGroup.Active=False
        OldPupilGroup.save()
        cache.delete("Pupil_%s" % Pupil_Id)
        if not PupilGroup.objects.filter(Pupil=Pupil.objects.get(id=int(Pupil_Id)),AcademicYear=AcYear,Group=GroupRec,Active=True).exists():
            if str(GroupRec) == 'Applicants.Holding':
                if PupilGroup.objects.filter(Pupil__id=int(Pupil_Id),Group__Name='Applicants.Holding',Active=True).exists():
                    pass
                else:
                    NewPupilGroup=PupilGroup(Pupil=Pupil.objects.get(id=int(Pupil_Id)),AcademicYear='Holding',Group=GroupRec)
                    NewPupilGroup.save()
                    cache.delete("Pupil_%s" % Pupil_Id)
            else:
                if len(PupilGroup.objects.filter(Pupil__id=int(Pupil_Id),
                                                 Group__Name=request.POST['NewGroup'],
                                                 Active='True')) == 0:
                    NewPupilGroup=PupilGroup(Pupil=Pupil.objects.get(id=int(Pupil_Id)),
                                             AcademicYear=AcYear,
                                             Group=GroupRec)
                    NewPupilGroup.save()
                    cache.delete("Pupil_%s" % Pupil_Id)
            Counter +=1
            if 'ApplicantInterviewTime' in request.POST or 'ApplicantInterviewDate' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)
                if 'ApplicantInterviewTime' in request.POST:
                    if request.POST['ApplicantInterviewTime'] != '':
                        if not ExtraFields.ReadExtention('PupilExtra')['ApplicantInterviewTime'][0]:
                            ExtraFields.WriteExtention('PupilExtra',
                                                       {'ApplicantInterviewTime':request.POST['ApplicantInterviewTime']})
                if 'ApplicantInterviewDate' in request.POST:
                    if request.POST['ApplicantInterviewDate'] != '':
                        if not ExtraFields.ReadExtention('PupilExtra')['ApplicantInterviewDate'][0]:
                            ExtraFields.WriteExtention('PupilExtra',
                                                       {'ApplicantInterviewDate':request.POST['ApplicantInterviewDate']})
                if request.POST['pastSchool'] != 'Not Known':
                    if len(PupilsNextSchool.objects.filter(PupilId__id=int(Pupil_Id))) == 0:
                        pastSchoolData = request.POST['pastSchool'].split(':')
                        pastSchoolObj = SchoolList.objects.filter(Name=pastSchoolData[0])[0]
                        pupilsNextSchool = PupilsNextSchool(PupilId=Pupil.objects.get(id=int(Pupil_Id)),
                                                            SchoolId=pastSchoolObj)
                        pupilsNextSchool.save()
            if 'declinedReason' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)
                ExtraFields.WriteExtention('PupilExtra',{'declinedReason':request.POST['declinedReason']})
            if 'withdrawnReason' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)
                ExtraFields.WriteExtention('PupilExtra',{'withdrawnReason':request.POST['withdrawnReason']})
            if 'declinedPlaceReason' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)
                ExtraFields.WriteExtention('PupilExtra',{'declinedPlaceReason':request.POST['declinedPlaceReason']})
            if 'declinedInterviewReason' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)
                ExtraFields.WriteExtention('PupilExtra',{'declinedInterviewReason':request.POST['declinedInterviewReason']})
            if 'declinedAfterDepositReason' in request.POST:
                ExtraFields = ExtendedRecord(BaseType='Pupil', BaseId=Pupil_Id)
                ExtraFields.WriteExtention('PupilExtra',{'declinedAfterDepositReason':request.POST['declinedAfterDepositReason']})
        else:
            AlreadyExists+=1
    if Counter > 0:
        request.session['Messages']=['%d Pupils Moved to %s' % (Counter, request.POST['NewGroup'])]
    else:
        request.session['Messages']=['Error : No pupils were Moved']
    if AlreadyExists > 0:
        request.session['Messages'].append('%d Pupils already exist in the target Group and have been removed from this one' % AlreadyExists)
    log.warn('Pupils (%s) were Moved to %s by%s' % (" ".join(request.POST.getlist('PupilSelect')),NewPupilGroup.Group.Name,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear, GroupName))

def MoveAnApplicant(request,school,AcYear,GroupName):
    ''' This view is used to move an applicant from one applicant group to another.
        An applicant may only be in one applicant group for a selected school '''
    GroupRec=Group.objects.get(Name=request.POST['NewGroup'])
    Counter = 0
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        PupilRec=Pupil.objects.get(id=int(Pupil_Id))
        OldPupilGroups=PupilGroup.objects.filter(Pupil=PupilRec,AcademicYear='Applicants',Group__Name__icontains=MenuTypes.objects.get(Name=school).SchoolId.Name)
        for groups in OldPupilGroups:
            groups.Active=False
            groups.save()
            cache.delete("Pupil_%s" % Pupil_Id)
        NewPupilGroup=PupilGroup(Pupil=Pupil.objects.get(id=int(Pupil_Id)),AcademicYear='Applicants',Group=GroupRec)
        NewPupilGroup.save()
        cache.delete("Pupil_%s" % Pupil_Id)
        Counter +=1

    if Counter > 0:
        request.session['Messages']=['%d Pupils Moved to %s' % (Counter, GroupName)]
        log.warn('Pupils (%s) were Moved to %s by%s' % (" ".join(request.POST.getlist('PupilSelect')),NewPupilGroup.Group.Name,request.user.username))

    else:
        request.session['Messages']=['Error : No pupils were Moved']
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear, GroupName))

def MoveFromHolding(request,school,AcYear,GroupName):
    ''' This view is used to move an applicant from Holding to an applicant group.
        An applicant may only be in one applicant group for a selected school '''
    GroupRec=Group.objects.get(Name=request.POST['NewGroup'])
    HoldingRec=Group.objects.get(Name__icontains='Applicants.Holding')
    Counter = 0
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        PupilRec=Pupil.objects.get(id=int(Pupil_Id))
        if PupilGroup.objects.filter(Pupil=PupilRec,Group=HoldingRec).exists():
            HoldingRec=PupilGroup.objects.get(Pupil=PupilRec,Group=HoldingRec)
            HoldingRec.Active=False
            HoldingRec.save()
            cache.delete("Pupil_%s" % Pupil_Id)
        OldPupilGroups=PupilGroup.objects.filter(Pupil=PupilRec,AcademicYear=AcYear,Group__Name__icontains=MenuTypes.objects.get(Name=school).SchoolId.Name)
        for groups in OldPupilGroups:
            groups.Active=False
            groups.save()
            cache.delete("Pupil_%s" % Pupil_Id)
        NewPupilGroup=PupilGroup(Pupil=PupilRec,AcademicYear=AcYear,Group=GroupRec)
        NewPupilGroup.save()
        cache.delete("Pupil_%s" % Pupil_Id)
        Counter +=1

        if Counter > 0:
            request.session['Messages']=['%d Pupils Moved to %s' % (Counter, GroupRec.Name)]
            log.warn('Pupils (%s) were Moved to %s by %s' % (" ".join(request.POST.getlist('PupilSelect')),NewPupilGroup.Group.Name,request.user.username))

        else:
            request.session['Messages']=['Error : No pupils were Moved']
        return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear, GroupName))

def RemoveFromHolding(request,school,AcYear,GroupName,PupilNo):
    ''' This view just removes the pupil from the holding group. This is to be used
    when a child has already been copied into another group and the link to holding
    is no longer required'''
    if PupilGroup.objects.filter(Pupil__id=int(PupilNo)).count() > 1:
        HoldingRec=PupilGroup.objects.get(Pupil__id=int(PupilNo),Group__Name='Applicants.Holding',Active=True)
        HoldingRec.Active=False
        HoldingRec.save()
        cache.delete("Pupil_%s" % PupilNo)
        request.session['Messages']=['Pupils Removed successfully']
        log.warn('Pupil (%s) was removed from Holding by %s' % (PupilNo,request.user.username))
    else:
        request.session['Messages']=['Error : This Pupil is not in any other groups and cannot be removed']
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear,GroupName))


def UpdateApplicant(request,school,AcYear,GroupName,PupilNo):
    ''' This method is used to update the basic info on a pupil record from forms
    embeded in the manager screens'''
    Rec=UpdatePupilRecord(request,school,AcYear,PupilNo)
    Rec.Base()
    request.session['Messages']=['Pupil Updated']
    log.warn('Pupil (%s) was Updated by %s' % (PupilNo,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % (school, AcYear, GroupName))


def RemoveApplicant(request,school,AcYear,GroupName,PupilNo):
    ''' This View removes a Applicant pupil Record from the system completely.
    This view should only be available from the holding group'''
    if FamilyChildren.objects.filter(Pupil__id=int(PupilNo)).exists():
        FamilyChildren.objects.filter(Pupil__id=int(PupilNo)).delete()
    if PupilGroup.objects.filter(Pupil__id=int(PupilNo)).exists():
        PupilGroup.objects.filter(Pupil__id=int(PupilNo)).delete()
    Pupil.objects.get(id=int(PupilNo)).delete()
    request.session['Messages']=['Pupil Deleted']
    log.warn('Pupil (%s) was Deleted from Holding by %s' % (PupilNo,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/Manage/%s/' % (school, AcYear, GroupName))

def MoveCopyFromHolding(request, school, AcYear, PupilNo):
    ''' This view proccesses a move or copy request from
    the manager holding page. UPDATE: This is also used to
    transfer pupils into the school the user is in '''
    SchoolName = MenuTypes.objects.get(Name=school).SchoolId.Name
    # test if pupil is already in school before transfer...
    transferPupilAlreadyInSchool = False
    for i in PupilGroup.objects.filter(Pupil__id=int(PupilNo),
                                       Active=True):
        if SchoolName in i.Group.Name:
            transferPupilAlreadyInSchool = True
            school_error_list = PupilGroup.objects.filter(Pupil__id=int(PupilNo),
                                                          Group__Name__icontains=".%s." % SchoolName,
                                                          Active=True).values_list("Group__Name", flat=True)
    if "transfer" in request.POST and transferPupilAlreadyInSchool:
        pupilObj = Pupil.objects.get(id=int(PupilNo))
        error_message = '''<span style="color:red;">ERROR!!</span>
                         %s %s is already in this following groups
                         for this school:<br /><br />''' % (pupilObj.Forename,
                                                            pupilObj.Surname)
        for group in school_error_list:
            error_message += "<a href='/WillowTree/%s/%s/ManageAll/%s/'>%s</a><br />" % (school,
                                                                                         AcYear,
                                                                                         group,
                                                                                         group)
        error_message += '''<br />Click <a href="/WillowTree/%s/%s/pupil/%s/">
                           here</a> to view Pupil Record. ''' % (school,
                                                                 AcYear,
                                                                 pupilObj.id)
        request.session['Messages'] = [error_message]
        return HttpResponseRedirect('/WillowTree/%s/%s/Transfer/SearchPage/' % (school, AcYear))
    else:
    # end
        if 'AppApplyForYear' in request.POST:
            RequiredYear = request.POST['AppApplyForYear']
        else:
            RequiredYear = ''
        if PupilGroup.objects.filter(Pupil__id=int(PupilNo),
                                     Group__Name__istartswith="Applicants.%s.Main" % (SchoolName),
                                     Active=True).exists():
            if request.POST['MoveType'] == 'Move':
                pupilGroupHolding = PupilGroup.objects.get(Pupil__id=PupilNo,Group__Name="Applicants.Holding",Active=True)
                pupilGroupHolding.Active = False
                pupilGroupHolding.save()
                cache.delete("Pupil_%s" % PupilNo)

            return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/Applicants.Holding/' % (school, AcYear))
        else:
            PupilRec = Pupil.objects.get(id=int(PupilNo))
            if request.POST['GroupType'] == 'Main':
                GroupRec=Group.objects.get(Name='Applicants.%s.Main.%s' % (SchoolName, RequiredYear))
            else:
                GroupRec = Group.objects.get(Name='Applicants.%s.Reserve' % (SchoolName))
            NewPupilGroup = PupilGroup(Pupil=PupilRec,AcademicYear=request.POST['AppAcYear'],
                                       Group=GroupRec,Active=True)
            NewPupilGroup.save()
            cache.delete("Pupil_%s" % PupilNo)
            if request.POST['MoveType']=='Move':
                HoldingRec=PupilGroup.objects.get(Pupil=PupilRec,Group__Name='Applicants.Holding',Active=True)
                HoldingRec.Active=False
                HoldingRec.save()
                cache.delete("Pupil_%s" % PupilNo)
            if "transfer" in request.POST:
                pupilObj = Pupil.objects.get(id=int(PupilNo))
                request.session['Messages'] = ['''%s %s has been successfully transfered. Click
                                               <a href="/WillowTree/%s/%s/pupil/%s/">
                                               here</a> to view Pupil Record.''' % (pupilObj.Forename,
                                                                                    pupilObj.Surname,
                                                                                    school,
                                                                                    AcYear,
                                                                                    pupilObj.id)]
                return HttpResponseRedirect('/WillowTree/%s/%s/Transfer/SearchPage/' % (school, AcYear))
            else:
                log.warn('Pupil (%s) was Moved to %s by %s' % (PupilNo,GroupRec,request.user.username))
                return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/Applicants.Holding/' % (school, AcYear))


def MoveCopyToKindie(request,school,AcYear,PupilNo):
    '''
    This view processes the form from holding to add an applicant
    as a Kindergarten applicant. It is proccesses differently from the
    main schools as the two schools are managed as one'''
    if PupilGroup.objects.filter(Pupil__id=int(PupilNo),
                                 Group__Name__icontains='Kindergarten',
                                 Active=True).exists():
        return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/Applicants.Holding/' % (school, AcYear))
    else:
        PupilRec=Pupil.objects.get(id=int(PupilNo))
        if request.POST['School']=='PimlicoLowerSeptember':
            GroupRec=Group.objects.get(Name='Applicants.Kindergarten.PimMain.September.Lower')
        if request.POST['School']=='PimlicoUpperSeptember':
            GroupRec=Group.objects.get(Name='Applicants.Kindergarten.PimMain.September.Upper')
        if request.POST['School']=='PimlicoLowerJanuary':
            GroupRec=Group.objects.get(Name='Applicants.Kindergarten.PimMain.January.Lower')
        if request.POST['School']=='PimlicoUpperJanuary':
            GroupRec=Group.objects.get(Name='Applicants.Kindergarten.PimMain.January.Lower')
        if request.POST['School']=='BatterseaLowerSeptember':
            GroupRec=Group.objects.get(Name='Applicants.Kindergarten.BatMain.September.Lower')
        if request.POST['School']=='BatterseaUpperSeptember':
            GroupRec=Group.objects.get(Name='Applicants.Kindergarten.BatMain.September.Upper')
        if request.POST['School']=='BatterseaLowerJanuary':
            GroupRec=Group.objects.get(Name='Applicants.Kindergarten.BatMain.January.Lower')
        if request.POST['School']=='BatterseaUpperJanuary':
            GroupRec=Group.objects.get(Name='Applicants.Kindergarten.BatMain.January.Upper')
        cache.delete("Pupil_%s" % PupilNo)
        if not PupilGroup.objects.filter(Pupil=PupilRec,Group=GroupRec,Active=True).exists():
            NewPupilGroup=PupilGroup(Pupil=PupilRec,AcademicYear=request.POST['AppAcYear'],Group=GroupRec)
            NewPupilGroup.save()
            if request.POST['MoveType']=='Move':
                HoldingRec=PupilGroup.objects.get(Pupil=PupilRec,Group__Name='Applicants.Holding',Active=True)
                HoldingRec.Active=False
                HoldingRec.save()
            cache.delete("Pupil_%s" % PupilNo)
        log.warn('Pupil (%s) was moved to %s by %s' % (PupilNo,GroupRec.Name,request.user.username))
        return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/Applicants.Holding/' % (school, AcYear))


def EditStaffGrp(request, school, AcYear, GroupName):
    ''' This view adds or removes one or more staff members from a grp'''
    if request.POST['EditType'] == 'Add':
        for staffId in request.POST.getlist('PupilSelect'):
            staffObj = StaffRecord(int(staffId), request, AcYear)
            staffObj.AddToGroup(request.POST['NewGroup'])
            log.warn('Staff (%s) were added to %s by %s' % ("'".join(request.POST.getlist('PupilSelect')),request.POST['NewGroup'],request.user.username))
    if request.POST['EditType'] == 'Remove':
        for staffId in request.POST.getlist('PupilSelect'):
            staffObj = StaffRecord(int(staffId), request, AcYear)
            staffObj.RemoveFromGroup(request.POST['RemoveGroup'])
            log.warn('Staff (%s) were Removed from %s by %s' % ("'".join(request.POST.getlist('PupilSelect')),request.POST['RemoveGroup'],request.user.username))
    varTuple = (school, AcYear, GroupName)
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % varTuple)


def removeFromAllGrpsMvToHolding(request, school, AcYear, PupilId):
    ''' Removes a single pupil or applicant, from the main pupil record
    page, from all groups and puts them in the Holdings group '''
    UpdatePupilRecord(request,
                      school,
                      AcYear,
                      PupilId).removeFromAllGrpsMvToHolding()
    varTuple = (school, 'Holding', PupilId)
    log.warn('Pupil (%s) was removed from all groups and placed in holding by %s' % (PupilId,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % varTuple)


def removePupilFromGrp(request, school, AcYear, GroupName):
    ''' Removes pupils from the grp they are in from the manager screens.
    Will only remove pupils or applicants from this group if they are already
    a member of another group'''
    for pupilId in request.POST.getlist('PupilSelect'):
        if PupilGroup.objects.filter(Pupil__id=int(pupilId),
                                     Active=True).count > 1:
            temp = PupilGroup.objects.get(Pupil__id=int(pupilId),
                                          Group__Name=GroupName)
            temp.Active = False
            temp.save()
            cache.delete('Pupil_%s' % pupilId)
    varTuple = (school, AcYear, GroupName)
    log.warn('Pupils (%s) were removed from %s by %s' % ("'".join(request.POST.getlist('PupilSelect')),GroupName,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % varTuple)


def moveApplicantToCurrent(request, school, AcYear, GroupName):
    ''' Moves an applicant to a main form grp. This will also remove the
    applicant from the applicant grp that is associated with the school
    that the applicant is joining '''
    messages = []
    currentYear = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    newGroupObj = Group.objects.get(Name=request.POST['NewGroup'])
    if 'pupilIdFromHolding' in request.POST:
        pupilList = [request.POST['pupilIdFromHolding']]
        oldGroupObj = 'Applicants.Holding'
    else:
        pupilList = request.POST.getlist('PupilSelect')
        oldGroupObj = Group.objects.get(Name=GroupName)
    for pupilId in pupilList:
        pupilObj = Pupil.objects.get(id=int(pupilId))
        pupilGrpObj = PupilGroup(Pupil=pupilObj,
                                 Group=newGroupObj,
                                 AcademicYear=currentYear)
        pupilGrpObj.save()
        pupilGroups = PupilGroup.objects.get(Pupil__id=pupilObj.id,
                                             Group__Name=oldGroupObj,
                                             Active=True)
        pupilGroups.Active = False
        pupilGroups.save()
        cache.delete('Pupil_%s' % pupilId)
        if 'createEmailAddress' in request.POST:
            newEmailAccountRequest(pupilObj, 'pupil')
        messages.append('%s %s moved to group %s' % (pupilObj.Forename,
                                                     pupilObj.Surname,
                                                     request.POST['NewGroup']))
    request.session['Messages'] = messages
    log.warn('Pupils (%s) were moved to current (group %s ) by %s' % ("'".join(request.POST.getlist('PupilSelect')),
                                                                      GroupName,
                                                                      request.user.username))
    if 'pupilIdFromHolding' in request.POST:
        varTuple = (school, AcYear)
        return HttpResponseRedirect('/WillowTree/%s/%s/Manage/Applicants.Holding/' % varTuple)
    else:
        varTuple = (school, AcYear, GroupName)
        return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % varTuple)


def SetTeachers(request, school, AcYear, GroupName):
    TeacherNameObject=TeacherNames(GroupName,AcYear)
    TeacherNameObject.SetTeachers(request)
    varTuple = (school, AcYear, GroupName)
    log.warn('Teachers set for group %s  by %s' % (GroupName,request.user.username))
    return HttpResponseRedirect('/WillowTree/%s/%s/ManageThis/%s/' % varTuple)


def move_to_group_from_pupil_record(request, school, ac_year, pupil_id):
    """
    move to group from pupil record
    """
    for key, value in request.POST.iteritems():
        if "new_group_" in key:
            # so to not have more than one house group per child
            if ".House." in value and PupilGroup.objects.filter(Pupil__id=pupil_id,
                                                                Group__Name=value,
                                                                Active=True).exists():
                pass
            else:
                if len(PupilGroup.objects.filter(Pupil__id=pupil_id,
                                                 Group__Name=value,
                                                 Active=True)) == 0:
                    new_pupil_group = PupilGroup(AcademicYear=ac_year,
                                                 Pupil=Pupil.objects.get(id=int(pupil_id)),
                                                 Group=Group.objects.get(Name=value))
                    new_pupil_group.save()
                    cache.delete('Pupil_%s' % pupil_id)
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school, ac_year, pupil_id))


def remove_from_group_from_pupil_record(request, school, ac_year, pupil_id):
    """
    Remove pupil from a group from pupil record
    """
    for key, value in request.POST.iteritems():
        if "old_group_" in key:
            for pupil_group in PupilGroup.objects.filter(Pupil__id=pupil_id,
                                                         Group__Name=value,
                                                         Active=True):
                deactivating_group = pupil_group
                deactivating_group.Active = False
                deactivating_group.save()
                cache.delete('Pupil_%s' % pupil_id)
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school, ac_year, pupil_id))