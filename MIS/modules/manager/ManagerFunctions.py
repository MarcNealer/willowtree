# Django middleware imports
from django.http import HttpResponse
from django.utils.html import strip_tags

# WillowTree system imports
from MIS.models import MenuTypes
from MIS.models import PupilsNextSchool
from MIS.models import Group
from MIS.models import PupilGroup
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import PupilRecord
from MIS.models import Family, FamilyContact
from MIS.modules import ExtendedRecords
from MIS.modules.SEN.SENFunctions import ExtraSenOptions

# Base python libray imports
import csv
import datetime

class ApplicantSubGroups:
    def __init__(self, school, GroupName):
        self.School = MenuTypes.objects.get(Name=school).SchoolId.Name
        if 'Main.' in GroupName:
            self.GroupYear = GroupName.split('Main.')[1].split('.')[0]
        elif 'Accepted.' in GroupName:
            self.GroupYear = GroupName.split('Accepted.')[1].split('.')[0]
        else:
            self.GroupYear = ''
        GroupStart = 'Applicants.%s.Main.%s' % (self.School, self.GroupYear)
        self.Main = GroupStart
        self.Interview = '%s.Interview' % (GroupStart)
        self.Offer = '%s.Offer' % (GroupStart)
        self.NotOffered = '%s.NotOffered' % GroupStart
        self.Wait = '%s.Wait' % GroupStart
        self.DeclinedInterview = '%s.Declined.Interview' % (GroupStart)
        self.Declined = '%s.Declined' % GroupStart
        self.DeclinedAfterDeposit = '%s.Declined.AfterDeposit' % GroupStart
        self.DeclinedPlace = '%s.Declined.Place' % GroupStart
        self.DeclinedNoShow = '%s.Declined.NoShow' % GroupStart

    def ReturnDictionary(self):
        return {'ApplicantsMainGroup': self.Main,
                'InterviewMainGroup': self.Interview,
                'OfferMainGroup': self.Offer,
                'InterviewDeclinedMainGroup': self.DeclinedInterview}


class ReturnCSV():
    """ general CSV class. Allow the user to select many different options
    from the various manager screens for their CSV file """
    def __init__(self, request, AcYear, GroupName, FileName, ManageType):
        self.requestPOST = request.POST
        self.acYear = AcYear
        self.groupName = GroupName
        self.fileName = FileName
        self.manageType = ManageType
        self.extRecord = False
        self.extRecordSen = False
        self.extRecordEal = False
        self.extraSenOptions = False

    def __CreateGroup__(self):
        if self.groupName.find('Alumni') > -1:
            self.acYear='Alumni'
        elif self.groupName.find('Applicants.Holding') > -1 or self.groupName.find('Applicants.Transfer') > -1:
            self.acYear='Holding'
        if self.manageType=='This':
            PupilList=GeneralFunctions.Get_PupilListExact(self.acYear,self.groupName)
            Type='This'
        else:
            PupilList=GeneralFunctions.Get_PupilList(self.acYear,self.groupName)
            Type = 'All'
        # messy code to get pupil selector working... (roy)
        PupilList2 = []
        for i in PupilList:
            if unicode(i.id) in self.requestPOST.getlist('PupilSelect'):
                PupilList2.append(i)
        PupilList = PupilList2
        # end
        self.ClassList=[]
        if 'fromSenView' in self.requestPOST:
            for pupil in PupilList:
                if bool(PupilRecord(pupil.id, self.acYear).ReturnCurrentActiveSen()) == True:
                    self.ClassList.append(PupilRecord(pupil.id,self.acYear))
        elif 'fromEalView' in self.requestPOST:
            for pupil in PupilList:
                if bool(PupilRecord(pupil.id, self.acYear).ReturnCurrentActiveEal()) == True:
                    self.ClassList.append(PupilRecord(pupil.id,self.acYear))
        else:
            for pupil in PupilList:
                self.ClassList.append(PupilRecord(pupil.id,self.acYear))
        return

    def  __ageOfToday__(self, birthdate):
        ''' returns a pupils age based on todays date '''
        todaydate = datetime.date.today()
        year = (todaydate.year - birthdate.year) - int((todaydate.month,
                todaydate.day) < (birthdate.month, birthdate.day))
        month = todaydate.month - birthdate.month
        if month < 1:
            month = 12 + month
        month = month - int(todaydate.day < birthdate.day)
        return "%dyears %dmonths" % (year, month)

    def CreateCSV(self):
        self.fileName='pupillist_%s.csv' % self.todaysDate()
        self.response = HttpResponse(mimetype="text/csv")
        self.response_writer = csv.writer(self.response)
        self.__CreateGroup__()
        self.__AddTitlesLine__()
        self.__WriteDataLines__()
        self.response['Content-Disposition'] = 'attachment; filename=%s' % self.fileName
        return self.response

    def todaysDate(self):
        '''  returns todays date in UK format '''
        today = datetime.datetime.now()
        return '%s-%s-%s' % (today.day, today.month, today.year)

    def PotentialCSV(self, ClassList, fileName='PotentialClassList.csv'):
        self.response = HttpResponse(mimetype="text/csv")
        self.response_writer = csv.writer(self.response)
        self.ClassList=ClassList
        self.__AddTitlesLine__()
        self.__WriteDataLines__()
        self.response['Content-Disposition'] = 'attachment; filename=%s' % fileName
        return self.response

    def __getExtention__(self, pupilId):
        ''' gets the pupil's extention record '''
        self.extRecord = ExtendedRecords.ExtendedRecord('Pupil',
                                                        pupilId).ReadExtention('PupilExtra')
        return

    def __getExtentionSEN__(self, pupilId):
        ''' gets the pupil's SEN extention record '''
        self.extRecordSen = ExtendedRecords.ExtendedRecord('Pupil',
                                                           pupilId).ReadExtention('PupilSEN')
        return

    def __getExtentionEAL__(self, pupilId):
        ''' gets the pupil's EAL extention record '''
        self.extRecordEal = ExtendedRecords.ExtendedRecord('Pupil',
                                                           pupilId).ReadExtention('PupilEAL')
        return

    def __getSenExtraOptions__(self, acYear, pupilId):
        ''' gets extra sen option data '''
        self.extraSenOptions = ExtraSenOptions(self.acYear, pupilId)
        return

    def __AddTitlesLine__(self):
        self.writeRowTitle = []
        if "pupilId" in self.requestPOST:
            self.writeRowTitle.append("id")
        if "pupilForename" in self.requestPOST:
            self.writeRowTitle.append("FirstName")
        if "pupilSurname" in self.requestPOST:
            self.writeRowTitle.append("Surname")
        if "pupilOtherNames" in self.requestPOST:
            self.writeRowTitle.append("Other Names")
        if "pupilGender" in self.requestPOST:
            self.writeRowTitle.append("Gender")
        if "pupilDOB" in self.requestPOST:
            self.writeRowTitle.append("DOB")
        if "pupilDOB2" in self.requestPOST:
            self.writeRowTitle.append("DOB - format 2")
        if "pupilGroups" in self.requestPOST:
            self.writeRowTitle.append("Groups")
        if "pupilHouse" in self.requestPOST:
            self.writeRowTitle.append("House")
        if "pupilForm" in self.requestPOST:
            self.writeRowTitle.append("Form")
        if "pupilEmail" in self.requestPOST:
            self.writeRowTitle.append("Pupil Email")
        if "Ethnicity" in self.requestPOST:
            self.writeRowTitle.append("Ethnicity")
        if "Nationality" in self.requestPOST:
            self.writeRowTitle.append("Nationality")
        if "Religion" in self.requestPOST:
            self.writeRowTitle.append("Religion")
        if "PrimaryLanguage" in self.requestPOST:
            self.writeRowTitle.append("PrimaryLanguage")
        if "ageOfToday" in self.requestPOST:
            self.writeRowTitle.append("Age of today")
        if "siblingCount" in self.requestPOST:
            self.writeRowTitle.append("Sibling Count")
        if "P_Internet" in self.requestPOST:
            self.writeRowTitle.append("Internet Permissions")
        if "P_Media" in self.requestPOST:
            self.writeRowTitle.append("Media Permissions")
        if "pupilRegistrationDate" in self.requestPOST:
            self.writeRowTitle.append("Registration Date")
        if "previousSchool" in self.requestPOST:
            self.writeRowTitle.append("Previous School")
        if "pupilFormDerivedFromGroupName" in self.requestPOST:
            self.writeRowTitle.append("Form Derived From Group Name")
        if "familyAddress" in self.requestPOST:
            self.writeRowTitle.append("Family Address")
        if "postcode" in self.requestPOST:
            self.writeRowTitle.append("Postcode")
        if "contactDetails1" in self.requestPOST:
            self.writeRowTitle.append("Contact 1 Details")
        if "contactDetails2" in self.requestPOST:
            self.writeRowTitle.append("Contact 2 Details")
        if "contactDetails3" in self.requestPOST:
            self.writeRowTitle.append("Contact 3 Details")
        if "medical_details" in self.requestPOST:
            self.writeRowTitle.append("Medical Details")

        # Applicant Extra
        if "Source" in self.requestPOST:
            self.writeRowTitle.append("Source")
        if "Interview_Time" in self.requestPOST:
            self.writeRowTitle.append("Interview Time")
        if "Interview_Date" in self.requestPOST:
            self.writeRowTitle.append("Interview Date")
        if "Acceptance_Deadline" in self.requestPOST:
            self.writeRowTitle.append("Acceptance Deadline")
        if "Standardised_Score" in self.requestPOST:
            self.writeRowTitle.append("Standardised Score")
        if "Percentage_Score" in self.requestPOST:
            self.writeRowTitle.append("Percentage Score")
        if "Applicant_Notes" in self.requestPOST:
            self.writeRowTitle.append("Applicant Notes")

        if "Con_1_Forename" in self.requestPOST:
            self.writeRowTitle.append("Contact1 Forename")
        if "Con_1_Surname" in self.requestPOST:
            self.writeRowTitle.append("Contact1 Surname")
        if "Con_1_Mobile" in self.requestPOST:
            self.writeRowTitle.append("Contact1 Mobile")
        if "Con_1_Email" in self.requestPOST:
            self.writeRowTitle.append("Contact1 Email")
        if "Con_1_Type" in self.requestPOST:
            self.writeRowTitle.append("Contact1 Type")
        if "Con_1_car_reg" in self.requestPOST:
            self.writeRowTitle.append("Contact1 Car Reg")
        if "Con_1_messaging_status" in self.requestPOST:
            self.writeRowTitle.append("Contact1 Messaging Status")
        if "Con_2_Forename" in self.requestPOST:
            self.writeRowTitle.append("Contact2 Forename")
        if "Con_2_Surname" in self.requestPOST:
            self.writeRowTitle.append("Contact2 Surname")
        if "Con_2_Mobile" in self.requestPOST:
            self.writeRowTitle.append("Contact2 Mobile")
        if "Con_2_Email" in self.requestPOST:
            self.writeRowTitle.append("Contact2 Email")
        if "Con_2_Type" in self.requestPOST:
            self.writeRowTitle.append("Contact2 Type")
        if "Con_2_car_reg" in self.requestPOST:
            self.writeRowTitle.append("Contact2 Car Reg")
        if "Con_2_messaging_status" in self.requestPOST:
            self.writeRowTitle.append("Contact2 Messaging Status")
        if "Con_3_Forename" in self.requestPOST:
            self.writeRowTitle.append("Contact3 Forename")
        if "Con_3_Surname" in self.requestPOST:
            self.writeRowTitle.append("Contact3 Surname")
        if "Con_3_Mobile" in self.requestPOST:
            self.writeRowTitle.append("Contact3 Mobile")
        if "Con_3_Email" in self.requestPOST:
            self.writeRowTitle.append("Contact3 Email")
        if "Con_3_Type" in self.requestPOST:
            self.writeRowTitle.append("Contact3 Type")
        if "Con_3_car_reg" in self.requestPOST:
            self.writeRowTitle.append("Contact3 Car Reg")
        if "Con_3_messaging_status" in self.requestPOST:
            self.writeRowTitle.append("Contact3 Messaging Status")
        if "familyHomeSalutation" in self.requestPOST:
            self.writeRowTitle.append("1st Family Home Salutation")
        if "familyPostalTitle" in self.requestPOST:
            self.writeRowTitle.append("1st Family Postal Title")
        if "familyAddressLine1" in self.requestPOST:
            self.writeRowTitle.append("1st Family Address Line 1")
        if "familyAddressLine2" in self.requestPOST:
            self.writeRowTitle.append("1st Family Address Line 2")
        if "familyAddressLine3" in self.requestPOST:
            self.writeRowTitle.append("1st Family Address Line 3")
        if "familyAddressLine4" in self.requestPOST:
            self.writeRowTitle.append("1st Family Address Line 4")
        if "familyPostCode" in self.requestPOST:
            self.writeRowTitle.append("1st Family Post Code")
        if "familyCountry" in self.requestPOST:
            self.writeRowTitle.append("1st Family Country")
        if "familyPhone1" in self.requestPOST:
            self.writeRowTitle.append("1st Family Phone 1")
        if "familyPhone2" in self.requestPOST:
            self.writeRowTitle.append("1st Family Phone 2")
        if "familyPhone3" in self.requestPOST:
            self.writeRowTitle.append("1st Family Phone 3")
        if "familyEmailAddress" in self.requestPOST:
            self.writeRowTitle.append("1st Family Email Address")
        if "2ndfamilyHomeSalutation" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Home Salutation")
        if "2ndfamilyPostalTitle" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Postal Title")
        if "2ndfamilyAddressLine1" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Address Line 1")
        if "2ndfamilyAddressLine2" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Address Line 2")
        if "2ndfamilyAddressLine3" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Address Line 3")
        if "2ndfamilyAddressLine4" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Address Line 4")
        if "2ndfamilyPostCode" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Post Code")
        if "2ndfamilyCountry" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Country")
        if "2ndfamilyPhone1" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Phone 1")
        if "2ndfamilyPhone2" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Phone 2")
        if "2ndfamilyPhone3" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Phone 3")
        if "2ndfamilyEmailAddress" in self.requestPOST:
            self.writeRowTitle.append("2nd Family Email Address")
        if "3rdfamilyHomeSalutation" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Home Salutation")
        if "3rdfamilyPostalTitle" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Postal Title")
        if "3rdfamilyAddressLine1" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Address Line 1")
        if "3rdfamilyAddressLine2" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Address Line 2")
        if "3rdfamilyAddressLine3" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Address Line 3")
        if "3rdfamilyAddressLine4" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Address Line 4")
        if "3rdfamilyPostCode" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Post Code")
        if "3rdfamilyCountry" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Country")
        if "3rdfamilyPhone1" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Phone 1")
        if "3rdfamilyPhone2" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Phone 2")
        if "3rdfamilyPhone3" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Phone 3")
        if "3rdfamilyEmailAddress" in self.requestPOST:
            self.writeRowTitle.append("3rd Family Email Address")
        if "UPN" in self.requestPOST:
            self.writeRowTitle.append("UPN")
        if "SEN_Y_N" in self.requestPOST:
            self.writeRowTitle.append("SEN")
        if "SEN_Alert" in self.requestPOST:
            self.writeRowTitle.append("SEN Alert")
        if "SEN_extra_time" in self.requestPOST:
            self.writeRowTitle.append("SEN Extra Time")
        if "SEN_laptop" in self.requestPOST:
            self.writeRowTitle.append("SEN Laptop")
        if "SEN_precis" in self.requestPOST:
            self.writeRowTitle.append("SEN Precis")
        listOne = ExtraSenOptions(keywordsOnly=True).standard_SEN_search_list
        listTwo = ExtraSenOptions(keywordsOnly=True).difficulties_SEN_search_list
        testList = listOne + listTwo
        for i in testList:
            if "extraOptions%s" % i in self.requestPOST:
                self.writeRowTitle.append(i.replace('_', ' '))
        if "EAL_Y_N" in self.requestPOST:
            self.writeRowTitle.append("EAL")
        if "EAL_Alert" in self.requestPOST:
            self.writeRowTitle.append("EAL Alert")
        if "ealLanguage1" in self.requestPOST:
            self.writeRowTitle.append("Language 1")
        if "ealLanguage2" in self.requestPOST:
            self.writeRowTitle.append("Language 2")
        if "ealLanguage3" in self.requestPOST:
            self.writeRowTitle.append("Language 3")
        if "EAL_precis" in self.requestPOST:
            self.writeRowTitle.append("EAL Precis")

        # Most Abled/More Abled
        if "subjects_most_abled_in_level_1" in self.requestPOST:
            self.writeRowTitle.append("Most Abled Lv1")
        if "subjects_most_abled_in_level_2" in self.requestPOST:
            self.writeRowTitle.append("Most Abled Lv2")
        if "more_abled_Y_N" in self.requestPOST:
            self.writeRowTitle.append("More Abled")

        # CAT data
        if "cat_data" in self.requestPOST:
            self.writeRowTitle.append("CAT Score")
            self.writeRowTitle.append("Verbal Score")
            self.writeRowTitle.append("NonVerbal Score")
            self.writeRowTitle.append("Quantitative Score")
            self.writeRowTitle.append("Spatial Score")
        # a relic of the past!!!
        if "interviewTime" in self.requestPOST:
            self.writeRowTitle.append("Interview Time XXX")
        if "interviewDate" in self.requestPOST:
            self.writeRowTitle.append("Interview Date XXX")
        self.response_writer.writerow(self.writeRowTitle)
        return

    def __WriteDataLines__(self):
        for Records in self.ClassList:
            self.writeRowList = []
            temp1 = ''
            temp2 = ''
            temp3 = ''
            if "id" in self.writeRowTitle:
                self.writeRowList.append(Records.Pupil.id)
            if "FirstName" in self.writeRowTitle:
                self.writeRowList.append(Records.Pupil.FirstName())
            if "Surname" in self.writeRowTitle:
                self.writeRowList.append(Records.Pupil.Surname)
            if "Other Names" in self.writeRowTitle:
                self.writeRowList.append(Records.Pupil.OtherNames)
            if "Gender" in self.writeRowTitle:
                self.writeRowList.append(Records.Pupil.Gender)
            if "DOB" in self.writeRowTitle:
                self.writeRowList.append(Records.Pupil.DateOfBirth.strftime('%d/%m/%Y'))
            if "DOB - format 2" in self.writeRowTitle:
                self.writeRowList.append(Records.Pupil.DateOfBirth.strftime('%d-%b-%y'))
            if "Groups" in self.writeRowTitle:
                listOfGroups = ''
                for i in Records.GroupList():
                    listOfGroups += '%s,  ' % i.Group.Name
                self.writeRowList.append(listOfGroups)
            if "House" in self.writeRowTitle:
                self.writeRowList.append(Records.AcademicHouse())
            if "Form" in self.writeRowTitle:
                self.writeRowList.append(Records.Form())
            if "Pupil Email" in self.writeRowTitle:
                self.writeRowList.append(Records.Pupil.EmailAddress)
            if "Ethnicity" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append("%s(%s)" % (self.extRecord['Ethnicity'][0],
                                                         self.extRecord['Ethnicity'][1]))
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append("%s(%s)" % (self.extRecord['Ethnicity'][0],
                                                         self.extRecord['Ethnicity'][1]))
            if "Nationality" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append("%s(%s)" % (self.extRecord['Nationality'][0],
                                                         self.extRecord['Nationality'][1]))
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append("%s(%s)" % (self.extRecord['Nationality'][0],
                                                         self.extRecord['Nationality'][1]))
            if "Religion" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append("%s(%s)" % (self.extRecord['Religion'][0],
                                                         self.extRecord['Religion'][1]))
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append("%s(%s)" % (self.extRecord['Religion'][0],
                                                         self.extRecord['Religion'][1]))
            if "PrimaryLanguage" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append("%s(%s)" % (self.extRecord['PrimaryLanguage'][0],
                                                         self.extRecord['PrimaryLanguage'][1]))
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append("%s(%s)" % (self.extRecord['PrimaryLanguage'][0],
                                                         self.extRecord['PrimaryLanguage'][1]))
            if "Age of today" in self.writeRowTitle:
                self.writeRowList.append(self.__ageOfToday__(Records.Pupil.DateOfBirth))
            if "Sibling Count" in self.writeRowTitle:
                self.writeRowList.append(str(len(Records.Siblings())))
            if "Internet Permissions" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append(self.extRecord['P_Internet'][0])
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecord['P_Internet'][0])
            if "Media Permissions" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append(self.extRecord['P_Media'][0])
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecord['P_Media'][0])
            if "Registration Date" in self.writeRowTitle:
                self.writeRowList.append(Records.Pupil.RegistrationDate.strftime('%d-%b-%y'))
            if "Previous School" in self.writeRowTitle:
                try:
                    prevSch = PupilsNextSchool.objects.get(PupilId=int(Records.Pupil.id))
                    prevSchData = '%s - %s' % (prevSch.SchoolId.Name,
                                               prevSch.SchoolId.Address.PostCode)
                except:
                    prevSchData = 'No Data'
                self.writeRowList.append(prevSchData)
            if "Form Derived From Group Name" in self.writeRowTitle:
                try:
                    self.writeRowList.append(self.groupName.split('.')[5])
                except:
                    self.writeRowList.append('error')
            if "Family Address" in self.writeRowTitle:
                famString2 = 'not found'
                try:
                    famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                    self.famObj = famObjList[0]
                    famAddress = self.famObj.Address.filter()
                    famAddress = famAddress[0]
                    famString2 = ''
                    if famAddress.HomeSalutation:
                        famString2 += '%s, ' % famAddress.HomeSalutation
                    if famAddress.AddressLine1:
                        famString2 += '%s, ' % famAddress.AddressLine1
                    if famAddress.AddressLine2:
                        famString2 += '%s, ' % famAddress.AddressLine2
                    if famAddress.AddressLine3:
                        famString2 += '%s, ' % famAddress.AddressLine3
                    if famAddress.AddressLine4:
                        famString2 += '%s, ' % famAddress.AddressLine4
                    if famAddress.PostCode:
                        famString2 += '%s' % famAddress.PostCode
                except:
                    pass
                self.writeRowList.append(famString2)
            if "Contact 1 Details" in self.writeRowTitle:
                temp1 = ''
                temp1 = self.__GetContactRecord__(Records.Pupil.id, 1)
                self.writeRowList.append(temp1['truncate'])
            if "Contact 2 Details" in self.writeRowTitle:
                temp2 = ''
                temp2 = self.__GetContactRecord__(Records.Pupil.id, 2)
                self.writeRowList.append(temp2['truncate'])
            if "Contact 3 Details" in self.writeRowTitle:
                temp3 = ''
                temp3 = self.__GetContactRecord__(Records.Pupil.id, 3)
                self.writeRowList.append(temp3['truncate'])
            if "Medical Details" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append(strip_tags(self.extRecord['pupils_medical_details'][0]))
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append(strip_tags(self.extRecord['pupils_medical_details'][0]))

            # Applicant extra
            if "Source" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append(self.extRecord['ApplicantSource'][0])
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecord['ApplicantSource'][0])
            if "Interview Time" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append(self.extRecord['ApplicantInterviewTime'][0])
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecord['ApplicantInterviewTime'][0])
            if "Interview Date" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append(self.extRecord['ApplicantInterviewDate'][0])
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecord['ApplicantInterviewDate'][0])
            if "Acceptance Deadline" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append(self.extRecord['applicantAcceptanceDeadline'][0])
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecord['applicantAcceptanceDeadline'][0])
            if "Standardised Score" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append(self.extRecord['ApplicantStandardisedScore'][0])
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecord['ApplicantStandardisedScore'][0])
            if "Percentage Score" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append(self.extRecord['applicantPercentageScore'][0])
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecord['applicantPercentageScore'][0])
            if "Applicant Notes" in self.writeRowTitle:
                if self.extRecord:
                    self.writeRowList.append(self.extRecord['applicantNotes'][0])
                else:
                    self.__getExtention__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecord['applicantNotes'][0])

            # contact details
            if "Contact1 Forename" in self.writeRowTitle:
                try:
                    if not temp1:
                        temp1 = self.__GetContactRecord__(Records.Pupil.id, 1)
                    self.writeRowList.append(temp1['forename'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact1 Surname" in self.writeRowTitle:
                try:
                    if not temp1:
                        temp1 = self.__GetContactRecord__(Records.Pupil.id, 1)
                    self.writeRowList.append(temp1['surname'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact1 Mobile" in self.writeRowTitle:
                try:
                    if not temp1:
                        temp1 = self.__GetContactRecord__(Records.Pupil.id, 1)
                    self.writeRowList.append(temp1['mobile'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact1 Email" in self.writeRowTitle:
                try:
                    if not temp1:
                        temp1 = self.__GetContactRecord__(Records.Pupil.id, 1)
                    self.writeRowList.append(temp1['email'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact1 Type" in self.writeRowTitle:
                try:
                    if not temp1:
                        temp1 = self.__GetContactRecord__(Records.Pupil.id, 1)
                    self.writeRowList.append(temp1['type'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact1 Car Reg" in self.writeRowTitle:
                try:
                    if not temp1:
                        temp1 = self.__GetContactRecord__(Records.Pupil.id, 1)
                    self.writeRowList.append(temp1['car_reg_details'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact1 Messaging Status" in self.writeRowTitle:
                try:
                    if not temp1:
                        temp1 = self.__GetContactRecord__(Records.Pupil.id, 1)
                    self.writeRowList.append(temp1['messaging_status'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact2 Forename" in self.writeRowTitle:
                try:
                    if not temp2:
                        temp2 = self.__GetContactRecord__(Records.Pupil.id, 2)
                    self.writeRowList.append(temp2['forename'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact2 Surname" in self.writeRowTitle:
                try:
                    if not temp2:
                        temp2 = self.__GetContactRecord__(Records.Pupil.id, 2)
                    self.writeRowList.append(temp2['surname'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact2 Mobile" in self.writeRowTitle:
                try:
                    if not temp2:
                        temp2 = self.__GetContactRecord__(Records.Pupil.id, 2)
                    self.writeRowList.append(temp2['mobile'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact2 Email" in self.writeRowTitle:
                try:
                    if not temp2:
                        temp2 = self.__GetContactRecord__(Records.Pupil.id, 2)
                    self.writeRowList.append(temp2['email'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact2 Type" in self.writeRowTitle:
                try:
                    if not temp2:
                        temp2 = self.__GetContactRecord__(Records.Pupil.id, 2)
                    self.writeRowList.append(temp2['type'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact2 Car Reg" in self.writeRowTitle:
                try:
                    if not temp2:
                        temp1 = self.__GetContactRecord__(Records.Pupil.id, 2)
                    self.writeRowList.append(temp1['car_reg_details'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact2 Messaging Status" in self.writeRowTitle:
                try:
                    if not temp2:
                        temp1 = self.__GetContactRecord__(Records.Pupil.id, 2)
                    self.writeRowList.append(temp1['messaging_status'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact3 Forename" in self.writeRowTitle:
                try:
                    if not temp3:
                        temp3 = self.__GetContactRecord__(Records.Pupil.id, 3)
                    self.writeRowList.append(temp3['forename'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact3 Surname" in self.writeRowTitle:
                try:
                    if not temp3:
                        temp3 = self.__GetContactRecord__(Records.Pupil.id, 3)
                    self.writeRowList.append(temp3['surname'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact3 Mobile" in self.writeRowTitle:
                try:
                    if not temp3:
                        temp3 = self.__GetContactRecord__(Records.Pupil.id, 3)
                    self.writeRowList.append(temp3['mobile'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact3 Email" in self.writeRowTitle:
                try:
                    if not temp3:
                        temp3 = self.__GetContactRecord__(Records.Pupil.id, 3)
                    self.writeRowList.append(temp3['email'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact3 Type" in self.writeRowTitle:
                try:
                    if not temp3:
                        temp3 = self.__GetContactRecord__(Records.Pupil.id, 3)
                    self.writeRowList.append(temp3['type'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact3 Car Reg" in self.writeRowTitle:
                try:
                    if not temp3:
                        temp3 = self.__GetContactRecord__(Records.Pupil.id, 3)
                    self.writeRowList.append(temp3['car_reg_details'])
                except:
                    self.writeRowList.append("ERROR")
            if "Contact3 Messaging Status" in self.writeRowTitle:
                try:
                    if not temp3:
                        temp3 = self.__GetContactRecord__(Records.Pupil.id, 3)
                    self.writeRowList.append(temp3['messaging_status'])
                except:
                    self.writeRowList.append("ERROR")
            self.__get_family_address_details__(0, Records)
            self.__get_family_address_details__(1, Records)
            self.__get_family_address_details__(2, Records)
            if 'UPN' in self.writeRowTitle:
                if not self.extRecord:
                    self.__getExtention__(Records.Pupil.id)
                self.writeRowList.append(self.extRecord['UPN'][0])
            if 'SEN' in self.writeRowTitle:
                sen = ' '
                try:
                    for i in Records.ActivePupilAlerts()[0]:
                        if i.AlertType.AlertGroup.Name == 'SEND':
                            sen = 'Y'
                except:
                    pass
                self.writeRowList.append(sen)
            if 'SEN Alert' in self.writeRowTitle:
                senAlert = ' '
                try:
                    for i in Records.ActivePupilAlerts()[0]:
                        if i.AlertType.AlertGroup.Name == 'SEND':
                            senAlert = i.AlertType.Name
                except:
                    pass
                self.writeRowList.append(senAlert)
            if 'SEN Extra Time' in self.writeRowTitle:
                if self.extRecordSen:
                    self.writeRowList.append(self.extRecordSen['senExtraTime'][0])
                else:
                    self.__getExtentionSEN__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecordSen['senExtraTime'][0])
            if 'SEN Laptop' in self.writeRowTitle:
                if self.extRecordSen:
                    self.writeRowList.append(self.extRecordSen['senLaptop'][0])
                else:
                    self.__getExtentionSEN__(Records.Pupil.id)
                    self.writeRowList.append(self.extRecordSen['senLaptop'][0])
            if 'SEN Precis' in self.writeRowTitle:
                try:
                    self.writeRowList.append(strip_tags(Records.ReturnCurrentActiveSen().AlertDetails))
                except:
                    self.writeRowList.append('')

            listOne = ExtraSenOptions(keywordsOnly=True).standard_SEN_search_list
            listTwo = ExtraSenOptions(keywordsOnly=True).difficulties_SEN_search_list
            testList = listOne + listTwo
            for i in testList:
                if i.replace('_', ' ') in self.writeRowTitle:
                    if not self.extraSenOptions:
                        self.__getSenExtraOptions__(self.acYear, Records.Pupil.id)
                    if i in self.extraSenOptions.extraOptionsInList:
                        self.writeRowList.append('TRUE')
                    else:
                        self.writeRowList.append(' ')
            if 'EAL' in self.writeRowTitle:
                eal = ''
                try:
                    for i in Records.ActivePupilAlerts()[0]:
                        if i.AlertType.AlertGroup.Name == 'EAL':
                            eal = 'Y'
                except:
                    pass
                self.writeRowList.append(eal)
            if 'EAL Alert' in self.writeRowTitle:
                ealAlert = ' '
                try:
                    for i in Records.ActivePupilAlerts()[0]:
                        if i.AlertType.AlertGroup.Name == 'EAL':
                            ealAlert = i.AlertType.Name
                except:
                    pass
                self.writeRowList.append(ealAlert)
            if 'Language 1' in self.writeRowTitle:
                if not self.extRecordEal:
                    self.__getExtentionEAL__(Records.Pupil.id)
                language1 = ''
                try:
                    language1 = '%s/%s' % (self.extRecordEal['ealLanguage1'][0],
                                           self.extRecordEal['ealLanguage1'][1])
                    if language1 == '/':
                        language1 = ''
                except:
                    pass
                self.writeRowList.append(language1)
            if 'Language 2' in self.writeRowTitle:
                if not self.extRecordEal:
                    self.__getExtentionEAL__(Records.Pupil.id)
                language2 = ''
                try:
                    language2 = '%s/%s' % (self.extRecordEal['ealLanguage2'][0],
                                           self.extRecordEal['ealLanguage2'][1])
                    if language2 == '/':
                        language2 = ''
                except:
                    pass
                self.writeRowList.append(language2)
            if 'Language 3' in self.writeRowTitle:
                if not self.extRecordEal:
                    self.__getExtentionEAL__(Records.Pupil.id)
                language3 = ''
                try:
                    language3 = '%s/%s' % (self.extRecordEal['ealLanguage3'][0],
                                           self.extRecordEal['ealLanguage3'][1])
                    if language3 == '/':
                        language3 = ''
                except:
                    pass
                self.writeRowList.append(language3)
            if 'EAL Precis' in self.writeRowTitle:
                try:
                    self.writeRowList.append(strip_tags(Records.ReturnCurrentActiveEal().AlertDetails))
                except:
                    self.writeRowList.append('')

            # Most Abled/More Abled
            if 'Most Abled Lv1' in self.writeRowTitle:
                most_abled_1 = ' '
                try:
                    for i in Records.ActivePupilAlerts()[0]:
                        if i.AlertType.AlertGroup.Name == 'Gifted and Talented':
                            most_abled_1 = " ".join(i.AlertDetails.split("<br />")[1:])
                except:
                    pass
                self.writeRowList.append(most_abled_1)
            if 'Most Abled Lv2' in self.writeRowTitle:
                most_abled_1 = ' '
                try:
                    for i in Records.ActivePupilAlerts()[0]:
                        if i.AlertType.AlertGroup.Name == 'Gifted and Talented 2':
                            most_abled_1 = " ".join(i.AlertDetails.split("<br />")[1:])
                except:
                    pass
                self.writeRowList.append(most_abled_1)
            if 'More Abled' in self.writeRowTitle:
                more_abled = ' '
                try:
                    for i in Records.ActivePupilAlerts()[0]:
                        if i.AlertType.AlertGroup.Name == 'More Abled':
                            more_abled = 'Y'
                except:
                    pass
                self.writeRowList.append(more_abled)

            # CAT Data
            if 'CAT Score' in self.writeRowTitle:
                try:
                    #yr = PupilRecord(Records.Pupil.id, self.acYear).Form()[0]
                    yr = PupilGroup.objects.filter(AcademicYear=self.acYear,
                                                   Group__Name__icontains='.Form.',
                                                   Pupil__id=Records.Pupil.id)[0].Group.Name.split('.')[3][-1]

                    cat_data_extension = ExtendedRecords.ExtendedRecord('Pupil',
                                                                        Records.Pupil.id).ReadExtention('Yr%s_Cat' % yr,
                                                                                                        'Form_Comment')
                    self.writeRowList.append(cat_data_extension['CATScore'][0])
                    self.writeRowList.append(cat_data_extension['VerbalScore'][0])
                    self.writeRowList.append(cat_data_extension['NonVerbalScore'][0])
                    self.writeRowList.append(cat_data_extension['QuantativeScore'][0])
                    self.writeRowList.append(cat_data_extension['SpartialScore'][0])
                except Exception as error:
                    self.writeRowList.append(error)
                    self.writeRowList.append(error)
                    self.writeRowList.append(error)
                    self.writeRowList.append(error)
                    self.writeRowList.append(error)

            # a relic from the past
            if 'Interview Time'in self.writeRowTitle:
                if not self.extRecord:
                    self.__getExtention__(Records.Pupil.id)
                interviewTime = ''
                try:
                    hour = self.extRecord['ApplicantInterviewTime'][0].hour
                    minute = self.extRecord['ApplicantInterviewTime'][0].minute
                    if minute == 0:
                        minute = '00'
                    interviewTime = '%s:%s' % (str(hour), str(minute))
                except:
                    pass
                self.writeRowList.append(interviewTime)

            if 'Interview Date'in self.writeRowTitle:
                if not self.extRecord:
                    self.__getExtention__(Records.Pupil.id)
                interviewDate = ''
                try:
                    day = self.extRecord['ApplicantInterviewDate'][0].day
                    month = self.extRecord['ApplicantInterviewDate'][0].month
                    year = self.extRecord['ApplicantInterviewDate'][0].year
                    interviewDate = '%s-%s-%s' % (str(day),
                                                  str(month),
                                                  str(year))
                except:
                    pass
                self.writeRowList.append(interviewDate)
            self.extRecord = False
            self.extRecordSen = False
            self.extRecordEal = False
            self.extraSenOptions = False
            self.response_writer.writerow(self.writeRowList)
            temp1 = ''
            temp2 = ''
        return

    def __GetContactRecord__(self, pupilId, contactPriority):
        contactRec = {}
        contactRec['truncate'] = 'not found'
        contactRec['type'] = 'not found'
        contactRec['forename'] = 'not found'
        contactRec['surname'] = 'not found'
        contactRec['mobile'] = 'not found'
        contactRec['email'] = 'not found'
        contactRec['car_reg_details'] = 'not found'
        contactRec['messaging_status'] = ' '
        famObjList = Family.objects.filter(familychildren__Pupil__id=int(pupilId))
        for contacts in FamilyContact.objects.filter(FamilyId=famObjList[0]):
            if contacts.Priority == contactPriority:
                x = ExtendedRecords.ExtendedRecord('Contact', contacts.Contact.id).ReadExtention('ContactExtra')
                mobileNumber = x['mobileNumber'][0]
                contactRec['truncate'] = "%s: %s %s, Mobile: %s, Email: %s" % (contacts.Relationship.Type,
                                                                               contacts.Contact.Forename,
                                                                               contacts.Contact.Surname,
                                                                               mobileNumber,
                                                                               contacts.Contact.EmailAddress)
                contactRec['type'] = contacts.Relationship.Type
                contactRec['forename'] = contacts.Contact.Forename
                contactRec['surname'] = contacts.Contact.Surname
                contactRec['mobile'] = mobileNumber
                contactRec['email'] = contacts.Contact.EmailAddress
                contactRec['car_reg_details'] = x['car_reg_details'][0]
                if contacts.SMS and contacts.EMAIL:
                    contactRec['messaging_status'] = "SMS and EMAIL"
                if contacts.SMS and not contacts.EMAIL:
                    contactRec['messaging_status'] = "SMS"
                if contacts.EMAIL and not contacts.SMS:
                    contactRec['messaging_status'] = "EMAIL"
        return contactRec

    def __get_family_address_details__(self, family_number, Records):
        append_text = str()
        if family_number == 0:
            append_text = '1st'
        if family_number == 1:
            append_text = '2nd'
        if family_number == 2:
            append_text = '3rd'
        if "%s Family Home Salutation" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.HomeSalutation:
                    famString2 += '%s' % famAddress.HomeSalutation
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Postal Title" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.PostalTitle:
                    famString2 += '%s' % famAddress.PostalTitle
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Address Line 1" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.AddressLine1:
                    famString2 += '%s' % famAddress.AddressLine1
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Address Line 2" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.AddressLine2:
                    famString2 += '%s' % famAddress.AddressLine2
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Address Line 3" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.AddressLine3:
                    famString2 += '%s' % famAddress.AddressLine3
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Address Line 4" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.AddressLine4:
                    famString2 += '%s' % famAddress.AddressLine4
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Post Code" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.PostCode:
                    famString2 += '%s' % famAddress.PostCode
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Country" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.Country:
                    famString2 += '%s' % famAddress.Country
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Phone 1" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.Phone1:
                    famString2 += '%s' % famAddress.Phone1
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Phone 2" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.Phone2:
                    famString2 += '%s' % famAddress.Phone2
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Phone 3" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.Phone3:
                    famString2 += '%s' % famAddress.Phone3
            except:
                pass
            self.writeRowList.append(famString2)
        if "%s Family Email Address" % append_text in self.writeRowTitle:
            famString2 = ''
            try:
                famObjList = Family.objects.filter(familychildren__Pupil__id=int(Records.Pupil.id))
                self.famObj = famObjList[0]
                famAddress = self.famObj.Address.filter()
                famAddress = famAddress[family_number]
                famString2 = ''
                if famAddress.EmailAddress:
                    famString2 += '%s' % famAddress.EmailAddress
            except:
                pass
            self.writeRowList.append(famString2)


class statsDataOnGroup():
    '''
    returns stats data on a group including how many boys and girls in each
    house and how many total in each house.

    Note: pupilList must consist of PupilRecord objects.

    There are two main uses for this class -->

    example using grp only =
    ManagerFunctions.statsDataOnGroup(schoolName, GroupName, ClassList)

    example using whole school (will return list of data) =
    ManagerFunctions.statsDataOnGroup(schoolName,
                                      AcademicYear='2013-2013',
                                      wholeSchool=True)
    '''
    def __init__(self,
                 school,
                 GroupName=False,
                 pupilList=False,
                 AcademicYear=False,
                 wholeSchool=False):
        self.school = school
        if GroupName:
            self.groupName = GroupName
        if pupilList:
            self.pupilList = pupilList
        else:
            self.pupilList = []
        if AcademicYear:
            self.AcademicYear = AcademicYear
        if wholeSchool:
            self.__getStatsDataForWholeSchool__()
        else:
            self.__formReturnDictionary__()
            self.__getStatsData__()

    def __returnAllHouses__(self):
        ''' returns all houses in a list based upon school string '''
        houses = []
        for i in Group.objects.filter(Name__icontains='Current.%s.House.House.' % self.school):
            houses.append(i.Name.split('.')[-1])
        return houses

    def __formReturnDictionary__(self):
        self.returnDic = {'Total': {'Total': 0, 'Boys': 0, 'Girls': 0}}
        for house in self.__returnAllHouses__():
            self.returnDic[house] = {'Total': 0, 'Boys': 0, 'Girls': 0}
        return

    def __getStatsData__(self):
        ''' returns stats data '''
        for pupil in self.pupilList:
            for house in self.__returnAllHouses__():
                if pupil.AcademicHouse() == house:
                    self.returnDic[house]['Total'] += 1
                    if pupil.Pupil.Gender == 'M':
                        self.returnDic[house]['Boys'] += 1
                    if pupil.Pupil.Gender == 'F':
                        self.returnDic[house]['Girls'] += 1
        for pupil in self.pupilList:
            self.returnDic['Total']['Total'] += 1
            if pupil.Pupil.Gender == 'M':
                self.returnDic['Total']['Boys'] += 1
            if pupil.Pupil.Gender == 'F':
                self.returnDic['Total']['Girls'] += 1
        return self.returnDic

    def __getAllPupilIdsForASchool__(self):
        ''' gets all pupil Ids for a School '''
        idList = [i.Pupil.id for i in set(PupilGroup.objects.filter(Group__Name__icontains='Current.%s' % self.school,
                                                                    AcademicYear=self.AcademicYear,
                                                                    Active=True))]
        return set(idList)

    def __getStatsDataForWholeSchool__(self):
        ''' returns stats data for a whole school '''
        self.wholeSchoolData = []
        # whole school stats...
        self.__formReturnDictionary__()
        temp = []
        for pupilId in self.__getAllPupilIdsForASchool__():
            temp.append(PupilRecord(pupilId, self.AcademicYear))
        if len(temp) > 0:
            self.pupilList = temp
            self.wholeSchoolData.append(['Whole School Stats',
                                         self.__getStatsData__()])
        # each grp in school stats...
        for grp in Group.objects.filter(Name__icontains='Current.%s.' % self.school):
            self.__formReturnDictionary__()
            self.groupName = grp.Name
            idList = [i.Pupil.id for i in set(PupilGroup.objects.filter(Group__Name=self.groupName,
                                                                        AcademicYear=self.AcademicYear,
                                                                        Active=True))]
            temp = []
            for pupilId in set(idList):
                temp.append(PupilRecord(pupilId, self.AcademicYear))
            if len(temp) > 0:
                self.pupilList = temp
                self.wholeSchoolData.append(['.'.join(grp.Name.split('.')[3:]),
                                             self.__getStatsData__()])
