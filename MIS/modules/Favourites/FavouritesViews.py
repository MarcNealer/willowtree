# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *

# WillowTree system imports
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.models import *
from MIS.modules.Favourites import FavouritesFunctions

def AddFavourite(request,school,AcYear,GroupName): 
    ''' Add a link for this group to the staff members favourite list. The 
    favourites are then shown on the welcome screen'''
    if not StaffGroupFavourite.objects.filter(Staff=request.session['StaffId'],Group__Name=GroupName).exists():
        newRec=StaffGroupFavourite(Staff=request.session['StaffId'],Group=Group.objects.get(Name=GroupName),PoolName='ALL')
        newRec.save()
    return HttpResponseRedirect('/WillowTree/%s/%s/Welcome/' % (school,AcYear)) 
    
def RemoveFavourite(request,school,AcYear,GroupId,StaffId):
    if StaffGroupFavourite.objects.filter(Staff__id=int(StaffId),Group__id=int(GroupId)).exists():
        StaffGroupFavourite.objects.filter(Staff__id=int(StaffId),Group__id=int(GroupId)).delete()
    return HttpResponseRedirect('/WillowTree/%s/%s/Welcome/' % (school,AcYear))
def ChangeFavouritePool(request,school,StaffId,GroupId):
    if 'PoolName' in request.POST:
        favRec=StaffGroupFavourite.objects.get(Staff__id=int(StaffId),Group__id=int(GroupId))
        favRec.PoolName=request.POST['PoolName']
    return HttpResponseRedirect('/WillowTree/%s/%s/Welcome/' % (school,AcYear))        