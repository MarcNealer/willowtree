# -*- coding: utf-8 -*-
"""
Created on Tue Jan 22 08:42:54 2013

@author: dbmgr
"""

from MIS.models import *

class StaffFavs():
    ''' Create an object of all the favourites marked by a selected staff member'''
    def __init__(self,request):
        self.StaffRec=request.session['StaffId']
        self.GroupList=StaffGroupFavourite.objects.filter(Staff=request.session['StaffId']).order_by('PoolName','Group__Name')
    def PoolList(self):
        Pools=set()
        for groups in self.GroupList:
            if not groups.PoolName=='All':
                Pools.add(groups.PoolName)
        return Pools