# python imports
import csv
import datetime
import copy


# Django imports
from MIS.models import *
from MIS.modules.PupilRecs import *
from MIS.modules.ExtendedRecords import *
from MIS.modules.GeneralFunctions import *


# Django middleware imports
from django.core.cache import cache


def ExtendedRecordExtract(GroupName,AcYear,RecordName,Subject):
    fileobj=open('ExtendedExtract_%s_%s.csv' % (RecordName,GroupName.split('.')[-1]),'wb')
    RecList=ExtentionRecords.objects.get(Name=RecordName)
    FieldNames=['Pupil']
    for fields in RecList.Fields.all():
        FieldNames.append(fields.Name)
    writerobj=csv.DictWriter(fileobj,FieldNames, extrasaction='ignore')
    writerobj.writeheader()
    pupillist=Get_PupilList(AcYear,GroupName)
    for pupils in pupillist:
        ExRec=ExtendedRecord('Pupil',pupils.id)
        pupilrec=ExRec.ReadExtention(RecordName,SubjectName=Subject)
        formatedrec={'Pupil':pupils.FullName()}
        for key,data in pupilrec.items():
            formatedrec[key]=data[0]
        writerobj.writerow(formatedrec)
    fileobj.close()

class GradeAnalysisExtract(PupilObject):
    def __init__(self,PupilId,AcYear,SubjectList,GradeRecordList):
        PupilObject.__init__(self,PupilId,AcYear)
        self.GradeRecords={}
        self.SubjectName=SubjectList[0]
        self.SubjectList=SubjectList
        if GradeRecordList:
            ExtentionRecs=ExtentionRecords.objects.filter(Name__in=GradeRecordList)
        else:
            ExtentionRecs=ExtentionRecords.objects.all()
        for recs in ExtentionRecs:
            subjectKey={}
            for subjectrec in self.SubjectList:
                subjectKey[subjectrec]=SimpleSingleRecord(PupilId,recs,subjectrec)
            self.GradeRecords[recs.Name]=copy.deepcopy(subjectKey)
    def hasAlert(self,AlertName):
        for alert_items in self.ActivePupilAlerts():
            for alerts in alert_items:
                if alerts.AlertType.AlertGroup.Name == AlertName:
                    return alerts.AlertType.Name
        return 'None'
    def IsSEN(self):
        return self.hasAlert('SEND')
    def IsEAL(self):
        return self.hasAlert('EAL')
    def IsMoreAble(self):
        return not 'None' in self.hasAlert('More Abled')
    def IsGiftedAndTalented(self):
        GTdata=PupilGiftedTalented.objects.filter(Pupil__id=int(self.PupilId),
                                                  Active=True,Subject__Name=self.SubjectName)
        if GTdata.exists():
            return 'MostAble %s' % GTdata[0].Level
        else:
            return 'None'
    def IsSumBirth(self):
        return self.Pupil.DateOfBirth.month > 4 and self.Pupil.DateOfBirth.month < 9
class PupilRecAnalysis(PupilObject):
    def hasAlert(self,AlertName):
        for alert_items in self.ActivePupilAlerts():
            for alerts in alert_items:
                if alerts.AlertType.AlertGroup.Name == AlertName:
                    return alerts.AlertType.Name
        return 'None'
    def IsSEN(self):
        return self.hasAlert('SEND')
    def IsEAL(self):
        return self.hasAlert('EAL')
    def IsMoreAble(self):
        if 'None' in self.hasAlert('More Abled'):
            return False
        else:
            return True

    def IsGiftedAndTalented(self,SubjectName):
        GTdata=PupilGiftedTalented.objects.filter(Pupil__id=int(self.PupilId),
                                                  Active=True,Subject__Name=SubjectName)
        if GTdata.exists():
            return 'MostAble %s' % GTdata[0].Level
        else:
            return 'None'
    def IsSumBirth(self):
        return self.Pupil.DateOfBirth.month > 4 and self.Pupil.DateOfBirth.month < 9
class SimpleSingleRecord():
    def __init__(self,PupilId,GradeRec,SubjectName):
        self.PupilId=PupilId
        self.GradeRec=GradeRec
        self.SubjectName=SubjectName
    def Grade(self):
        rec=cache.get('%s_%s_%s' % (self.GradeRec.Name, self.SubjectName,self.PupilId))
        if rec:
            return rec
        else:
            graderec=ExtendedRecord('Pupil',self.PupilId)
            recData=graderec.ReadExtention(self.GradeRec.Name,self.SubjectName)
            cache.set('%s_%s_%s' % (self.GradeRec.Name, self.SubjectName,self.PupilId),recData,3600)
            return recData



class ExtendedRecordExtracts():
    '''
    used to Extract grade/extended record dataS
    '''
    def __init__(self,
                 AcYear,
                 GroupName=False,
                 subject=False):
            self.AcYear = AcYear
            self.__todaysDate__()
            if GroupName:
                self.GroupName = GroupName
            if subject:
                self.Subject = subject
            else:
                self.Subjects = Subject.objects.all()

    def __todaysDate__(self):
        today = datetime.datetime.now()
        self.todaysDate = '%s-%s-%s' % (today.day, today.month, today.year)
        return

    def __getSubSubjects__(self, listOfMainSubs):
        ''' gets all subjects and sub subjects from a list of subjects,
        returns a list of subjects and sub subjects '''
        subSubjects = []
        for i in Subject.objects.all():
            for i2 in listOfMainSubs:
                if ParentSubjects.objects.filter(Subject__Name=i,
                                                 ParentSubject__Name=i2):
                    subSubjects.append(i.Name)
        temp = set(listOfMainSubs+subSubjects)
        return sorted(list(temp))

    def type1Report(self, reportSeason, listOfPupilIds, datafield, withSubSubjects=False):
        '''
        Pulls one type of specified grade, for example effort, for each
        subject for a group of pupils.
        '''
        self.reportSeason = reportSeason
        self.datafield = datafield
        self.returnData = []
        mainSubjects = [i.GradeSubject.Name for i in GradeInputElement.objects.filter(GradeRecord__Name=reportSeason)]
        if withSubSubjects:
            self.Subjects = self.__getSubSubjects__(mainSubjects)
        else:
            self.Subjects = mainSubjects
        for idNo in listOfPupilIds:
            pupilObj = PupilRecord(idNo, self.AcYear)
            data = {}
            for subject in self.Subjects:
                try:
                    data[subject] = [cache.get('%s_%s_%s' % (reportSeason, subject, pupilObj.Pupil.id))[datafield][0],
                                    cache.get('%s_%s_%s' % (reportSeason, subject, pupilObj.Pupil.id))[datafield][1]]
                except:
                    try:
                        extGradeRec = ExtendedRecord('Pupil', pupilObj.Pupil.id)
                        if extGradeRec.ReadExtention(reportSeason, subject)[datafield][0] != '':
                            data[subject] = [extGradeRec.ReadExtention(reportSeason, subject)[datafield][0],
                                            extGradeRec.ReadExtention(reportSeason, subject)[datafield][1]]
                        else:
                            data[subject] = ['','']
                    except:
                        data[subject] = ['error','error']
            self.returnData.append({'pupil': pupilObj, 'data': data})
        return self

    def type2Report(self, reportSeason, listOfPupilIds, subject):
        '''
        All fields for a list of pupils for a single subject.
        '''
        self.subject = subject
        self.reportSeason = reportSeason
        requestList = ['Effort','Attainment','Comment', 'Level', 'TeacherName']
        self.returnData = []
        for pupilId in listOfPupilIds:
            pupilObj = PupilRecord(pupilId, self.AcYear)
            data = {}
            for i in requestList:
                try:
                    data[i] = [cache.get('%s_%s_%s' % (reportSeason, self.subject, pupilObj.Pupil.id))[i][0],
                              cache.get('%s_%s_%s' % (reportSeason, self.subject, pupilObj.Pupil.id))[i][1]]
                except:
                    try:
                        extGradeRec = ExtendedRecord('Pupil', pupilObj.Pupil.id)
                        if extGradeRec.ReadExtention(reportSeason, self.subject)[i][0] != '':
                            data[i] = [extGradeRec.ReadExtention(reportSeason, self.subject)[i][0],
                                       extGradeRec.ReadExtention(reportSeason, self.subject)[i][1]]
                        else:
                            data[i] = ['','']
                    except:
                        data[i] = ['error','error']
            self.returnData.append({'pupil': pupilObj, 'data': data})
        return self

    def type3Report(self, reportSeason, listOfPupilIds, dataTuple):
        '''
        Returns data based upon a tuple containing different data types for
        getting grade data for a group a pupils.

        example tuple: [('Reading', 'Level'),
                        ('Writing', 'Level'),
                        ('English', 'Effort'),
                        ('English', 'Attainment'),
                        ('Mathematics', 'Exam1')]
        '''
        self.reportSeason = reportSeason
        self.dataTuple = dataTuple
        self.returnData = []
        for pupilId in listOfPupilIds:
            pupilObj = PupilRecord(pupilId, self.AcYear)
            data = {}
            for i in dataTuple:
                dicKey = '%s_%s' % (i[0], i[1])
                try:
                    data[dicKey] = [cache.get('%s_%s_%s' % (reportSeason, i[0], pupilObj.Pupil.id))[i[1]][0],
                                    cache.get('%s_%s_%s' % (reportSeason, i[0], pupilObj.Pupil.id))[i][1][1]]
                except:
                    try:
                        extGradeRec = ExtendedRecord('Pupil', pupilObj.Pupil.id)
                        if extGradeRec.ReadExtention(reportSeason, i[0])[i[1]][0] != '':
                            data[dicKey] = [extGradeRec.ReadExtention(reportSeason, i[0])[i[1]][0],
                                            extGradeRec.ReadExtention(reportSeason, i[0])[i[1]][1]]
                        else:
                            data[dicKey] = ['','']
                    except:
                        data[dicKey] = ['error','error']
            self.returnData.append({'pupil': pupilObj, 'data': data})
        return self

    def type4Report(self):
        '''
        a tupil containing different data uses for getting grade data for a group a pupils.

        for example: [('Yr6_Hf_M', 'Rd', 'Level'),
                      ('Yr6_Hf_M','WR', 'Level'),
                      (Yr7_Eof_M', 'EN', 'Effort'),
                      ('Yr3_Hf_S', 'EN', 'Attainment'),
                      ('Yr6_Hf_M', 'MA', 'Exam1')]
        '''
        self.reportSeason = reportSeason
        self.dataTuple = dataTuple
        self.returnData = []
        for pupilId in listOfPupilIds:
            pupilObj = PupilRecord(pupilId, self.AcYear)
            data = {}
            for i in dataTuple:
                dicKey = '%s_%s' % (i[0], i[1], i[2])
                try:
                    data[dicKey] = [cache.get('%s_%s_%s' % (reportSeason, i[0], pupilObj.Pupil.id))[i[1]][0],
                                    cache.get('%s_%s_%s' % (reportSeason, i[0], pupilObj.Pupil.id))[i][1][1]]
                except:
                    try:
                        extGradeRec = ExtendedRecord('Pupil', pupilObj.Pupil.id)
                        if extGradeRec.ReadExtention(reportSeason, i[0])[i[1]][0] != '':
                            data[dicKey] = [extGradeRec.ReadExtention(reportSeason, i[0])[i[1]][0],
                                            extGradeRec.ReadExtention(reportSeason, i[0])[i[1]][1]]
                        else:
                            data[dicKey] = ['','']
                    except:
                        data[dicKey] = ['error','error']
            self.returnData.append({'pupil': pupilObj, 'data': data})
        return self

    def type5Report(self):
        '''
        have a filter on this report that for example gets all the eot maths data
        a pupil would have.
        '''