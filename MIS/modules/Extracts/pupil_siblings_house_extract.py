from MIS.models import Group
from MIS.models import PupilGroup
from MIS.modules.PotentialGroups import PotentialGroupsFunctions


def run(ac_year, next_ac_year, school):
    """
    This function displays a pupils siblings house. This will only really ever be used for Clapham where new pupils
    need to be assigned the same house as their current siblings in the school
    """
    f = open('siblings_to_house.csv', 'w')
    current_groups = []
    for i in [i.Name for i in Group.objects.filter(Name__icontains="Current.%s" % school).filter(Name__icontains="Form")]:
        if len(i.split('.')) == 6:
            if PupilGroup.objects.filter(Group__Name=i).exists():
                current_groups.append(i)
    groups = current_groups
    length_of_job = len(groups)
    counter = 1
    f.write("pupil_id, pupil_forename, pupil_surname, current_group, potential_group, current_house, siblings_houses")
    for group in groups:
        for i in PotentialGroupsFunctions.PotentialGroup(school,
                                                         group,
                                                         ac_year,
                                                         next_ac_year).ClassList():
            sibling_house_list = ''
            if i.Siblings():
                for siblings_house in i.Siblings():
                    sibling_house_list += '%s ' % siblings_house.AcademicHouse()
            f.write("%s, %s, %s, %s, %s, %s, %s\n" % (i.Pupil.id,
                                                      i.Pupil.Forename,
                                                      i.Pupil.Surname,
                                                      i.Form(),
                                                      group.split('.')[-1],
                                                      i.AcHouse,
                                                      sibling_house_list))
        print '%d of %d completed' % (counter, length_of_job)
        counter += 1
