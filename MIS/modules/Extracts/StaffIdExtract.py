# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 15:55:49 2013

@author: marc
"""

import csv
from MIS.models import *
from django.contrib.auth.models import User, Permission

def CreateCSV(schoolName):
    stafflist=Staff.objects.filter(DefaultMenu__Name__icontains=schoolName).values()
    fileobj=open('Staffextract_%s.csv' % schoolName,'wb')
    csvObj=csv.DictWriter(fileobj,['id','Forename','Surname'],extrasaction='ignore')
    csvObj.writeheader()
    csvObj.writerows(stafflist)
    fileobj.close()


def CreateStaffAccessList():
    stafflist=Staff.objects.filter(Active=True)
    outlist=[]
    for staffrec in stafflist:
        if staffrec.User:
            outrec={'User':staffrec.User.username,
                    'Surname':staffrec.Surname,
                    'Forename':staffrec.Forename,
                    'Permissions':':'.join([x.name for x in staffrec.User.user_permissions.all()]),
                    'DefaultMenu':staffrec.DefaultMenu}
            outlist.append(outrec)
    fileobj=open('StaffAccessList.csv','wb')
    csvObj=csv.DictWriter(fileobj,['User','Surname','Forename',
                                   'Permissions','DefaultMenu'],extrasaction='ignore')
    csvObj.writeheader()
    csvObj.writerows(outlist)
    fileobj.close()

