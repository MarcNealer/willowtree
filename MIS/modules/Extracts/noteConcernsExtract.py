from MIS.models import *
from MIS.modules.GeneralFunctions import Get_PupilList
import datetime
import csv
from django.utils.html import strip_tags

def run():
    ''' '''
    k = ['Esafety incident',
         'Bullying concern',
         'Social concern',
         'Racist concern',
         'Negative',
         'Pastoral']
    pn = []
    pupils = Get_PupilList('2013-2014',
                           'Current.Fulham')
    startDate = datetime.datetime.strptime('2013-09-02',"%Y-%m-%d")
    endDate = datetime.datetime.strptime('2013-12-15',"%Y-%m-%d")
    for i in pupils:
        for keyword in k:
            notes = PupilNotes.objects.filter(PupilId__id=i.id,
                                              NoteId__Keywords__icontains=keyword,
                                              NoteId__RaisedDate__range=(startDate,endDate),
                                              Active='True')
            if len(notes) != 0:
                for n in notes:
                    pn.append(n)

    # create csv file
    with open('eggs.csv', 'wb') as csvfile:
        response_writer = csv.writer(csvfile, delimiter=',')
        response_writer.writerow(['Pupil Id',
                                  'Forename',
                                  'Surname',
                                  'Filter Date Range',
                                  'Keywords',
                                  'Note Text',
                                  'IsAnIssue',
                                  'IssueStatus',
                                  'RaisedBy',
                                  'RaisedDate',
                                  'ModifiedBy',
                                  'ModifiedDate'])
        for i in pn:
            response_writer.writerow([i.PupilId.id,
                                      i.PupilId.Forename,
                                      i.PupilId.Surname,
                                      '%s - %s' % (startDate.isoformat(), endDate.isoformat()),
                                      i.NoteId.Keywords,
                                      strip_tags(i.NoteId.NoteText),
                                      i.NoteId.IsAnIssue,
                                      i.NoteId.IssueStatus,
                                      '%s %s' % (i.NoteId.RaisedBy.Forename,i.NoteId.RaisedBy.Surname),
                                      str(i.NoteId.RaisedDate).split(' ')[0],
                                      i.NoteId.ModifiedBy,
                                      str(i.NoteId.ModifiedDate).split(' ')[0]])