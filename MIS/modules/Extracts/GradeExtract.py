# Python imports

# Django imports
from django.db.models import Q
from MIS.modules.ExtendedRecords import ExtendedRecord
from MIS.modules.GeneralFunctions import Get_PupilList
from MIS.modules.gradeInput.GradeFunctions import PupilGradeRecord


class GradeExtract():
    """
    This class is used to extract effort, level, attainment and grade data for multiple subjects for a pupil group.
    """
    def __init__(self,
                 ac_year,
                 pupil_group,
                 grade_record,
                 subjects_array,
                 effort=False,
                 level=False,
                 attainment=False,
                 exam_1=False,
                 exam_2=False,
                 exam_3=False):
        self.ac_year = ac_year
        self.pupil_group = pupil_group
        self.grade_record = grade_record
        self.subjects_array = subjects_array
        self.effort = effort
        self.level = level
        self.attainment = attainment
        self.exam_1 = exam_1
        self.exam_2 = exam_2
        self.exam_3 = exam_3

    def __create_header_for_csv__(self):
        """
        Creates header for the returned data.
        """
        returned_data = ['Pupil Name']
        for subject in self.subjects_array:
            if self.effort:
                returned_data.append('%s Effort' % subject)
            if self.level:
                returned_data.append('%s Level' % subject)
            if self.attainment:
                returned_data.append('%s Attainment' % subject)
            if self.exam_1:
                returned_data.append('%s Exam 1' % subject)
            if self.exam_2:
                returned_data.append('%s Exam 2' % subject)
            if self.exam_3:
                returned_data.append('%s Exam 3' % subject)
        return returned_data

    def __get_data_for_a_subject__(self, pupil_id, subject):
        """
        Returns data based upon a subject. Data returned depends upon values entered into this class, for example
        effort, level, attainment, exam_1 etc...
        """
        returned_data = []
        record = ExtendedRecord('Pupil', pupil_id).ReadExtention(self.grade_record, subject)
        if self.effort:
            returned_data.append(record['Effort'][0])
        if self.level:
            returned_data.append(record['Level'][0])
        if self.attainment:
            returned_data.append(record['Attainment'][0])
        if self.exam_1:
            if record['Exam1Type'][0] != "":
                returned_data.append("%s (%s)" % (record['Exam1'][0], record['Exam1Type'][0]))
            else:
                returned_data.append(record['Exam1'][0])
        if self.exam_2:
            if record['Exam2Type'][0] != "":
                returned_data.append("%s (%s)" % (record['Exam2'][0], record['Exam2Type'][0]))
            else:
                returned_data.append(record['Exam2'][0])
        if self.exam_3:
            if record['Exam3Type'][0] != "":
                returned_data.append("%s (%s)" % (record['Exam3'][0], record['Exam3Type'][0]))
            else:
                returned_data.append(record['Exam3'][0])
        return returned_data

    def run(self):
        """
        Collect grade data and return in a format ready for csv creation
        """
        returned_data = [self.__create_header_for_csv__()]
        for pupil in Get_PupilList(self.ac_year, self.pupil_group):
            data_for_pupil = [pupil.FullName()]
            for subject in self.subjects_array:
                for data in self.__get_data_for_a_subject__(pupil.id, subject):
                    data_for_pupil.append(data)
            returned_data.append(data_for_pupil)
        return returned_data


class RawGradeExtract():
    def __init__(self, ac_year, grp, record, subject):
        self.ac_year = ac_year
        self.grp = grp
        self.record = record
        self.subject = subject

    def run(self):
        """
        Used to extract all grade data for a group of pupils for a single subject

        example for getting out spelling data:

        x = RawGradeExtract.run('2013-2014', 'Current.Kensington.PrepSchool.Year5', 'Yr5_Sp_M', 'Form_Comment')
        """
        return_data = []
        try:
            pupil_ids = [i.id for i in Get_PupilList(self.ac_year, self.grp)]
            x = PupilGradeRecord(pupil_ids[0], self.ac_year, self.record)
            r = x.Grades[self.record].Grades()
            data_dic = r[self.subject].Current()
            header_data = ['Forename',
                           'Surname',
                           'Form']
            for k, v in data_dic.iteritems():
                header_data.append(k)
            return_data.append(header_data)
            for i in pupil_ids:
                data_list = []
                x = PupilGradeRecord(i, self.ac_year, self.record)
                r = x.Grades[self.record].Grades()
                data_dic = r[self.subject].Current()
                data_list.append(x.Pupil.Forename)
                data_list.append(x.Pupil.Surname)
                data_list.append(x.Form())
                for k, v in data_dic.iteritems():
                    data_list.append(v[0])
                return_data.append(data_list)
        except NameError:
            return_data.append('error - please see WillowTree admin team')
        return return_data