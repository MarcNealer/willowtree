__author__ = 'marc'

from MIS.modules.Extracts.ISAMSExtracts import *
from MIS.modules.attendance.AttendanceFunctions import *
from MIS.modules.GeneralFunctions import *
import unicodecsv
class AttendanceExtract(WillowTreeExtracts):
    def makeAttendanceReport(self):
        pupillist=Get_PupilList(self.AcYear,GroupName='Current.Fulham')
        outdata=[]
        for pupils in pupillist:
            rec=PupilAttendanceRec(pupils.id,self.AcYear)
            rec.Yearly()
            dataline=[pupils.id,
                      pupils.FirstName(),
                      pupils.Surname,
                      rec.Stats.AttendedWithNonStatPercentage,
                      rec.Stats.AttendedExcludeNonStatPercentage]
            outdata.append(dataline)
        self.__WritetoCSV__(outdata,
                          'AttendanceExtract',
                          ['id','FirstName','Surname',
                           'Attendance % (Include Non Stat)',
                           'Attendance % (Include Non Stat)'])

    def __WritetoCSV(self,OutputData,filename,Headers):
        fileobj=open('%s.csv' % filename,'wb')
        csvObj=unicodecsv.writer(fileobj,dialect='excel-tab')
        csvObj.writerow(Headers)
        csvObj.writerows(self.__cleanData(OutputData))
        fileobj.close()
    def __WritetoCSV__(self,OutputData,filename,Headers):
        fileobj=open('%s.csv' % filename,'wb')
        csvObj=unicodecsv.writer(fileobj,dialect='excel-tab')
        csvObj.writerow(Headers)
        csvObj.writerows(self.__cleanData(OutputData))
        fileobj.close()

    def __cleanData(self,data):
        newdata=[]
        for items in data:
            newdata.append(self.__stripNewlines(items))
        return newdata
    def __stripNewlines(self,dataline):
        newdataline=[]
        for items in dataline:
            if isinstance(items,str) or isinstance(items,unicode):
                newdataline.append(items.replace('\n', '').replace('\r', '').replace('\r\n',''))
            else:
                newdataline.append(items)
        return newdataline

    def __addItem(self,ItemName,data):
        if ItemName in data:
            return data[ItemName][0]
        else:
            return ''