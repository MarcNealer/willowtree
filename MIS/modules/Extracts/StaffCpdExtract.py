# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 15:55:49 2013

@author: marc
"""

import csv
from MIS.models import *

def CreateCSV(schoolName):
    cpdList = StaffCpdRecords.objects.filter(Staff__DefaultMenu__Name__icontains=schoolName,
                                          Active=True)
    with open('staff_cpd_extract_for_%s.csv' % schoolName, 'wb') as csvfile:
        csvObj = csv.writer(csvfile, delimiter=',')
        csvObj.writerow(['id',
                         'Forename',
                         'Surname',
                         'CPD',
                         'TimeTaken',
                         'DateTaken',
                         'Duration',
                         'Details',
                         'DateExpires',
                         'CertIssued',
                         'Provider',
                         'InternalExternal'])
        for i in cpdList:
            cpdTemp = [i.Staff.id,
                       i.Staff.Forename,
                       i.Staff.Surname,
                       i.Name,
                       i.TimeTaken,
                       i.DateTaken,
                       i.Duration,
                       i.Details,
                       i.DateExpires,
                       i.CertIssued,
                       i.Provider,
                       i.InternalExternal]
            csvObj.writerow(cpdTemp)
    return