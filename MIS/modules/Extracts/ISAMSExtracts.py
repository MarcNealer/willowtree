__author__ = 'marc'
from MIS.models import *
import unicodecsv
import cPickle
import base64
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.ExtendedRecords import *
import gc
'''
when running this, remember that unicodecsv has to be installed using pip
'''

class WillowTreeExtracts():
    def __init__(self):
        self.AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    def __WritetoCSV(self,OutputData,filename,Headers):
        fileobj=open('%s.csv' % filename,'wb')
        csvObj=unicodecsv.writer(fileobj,dialect='excel-tab')
        csvObj.writerow(Headers)
        csvObj.writerows(self.__cleanData(OutputData))
        fileobj.close()
    def __WritetoCSV__(self,OutputData,filename,Headers):
        fileobj=open('%s.csv' % filename,'wb')
        csvObj=unicodecsv.writer(fileobj,dialect='excel-tab')
        csvObj.writerow(Headers)
        csvObj.writerows(self.__cleanData(OutputData))
        fileobj.close()

    def __cleanData(self,data):
        newdata=[]
        for items in data:
            newdata.append(self.__stripNewlines(items))
        return newdata
    def __stripNewlines(self,dataline):
        newdataline=[]
        for items in dataline:
            if isinstance(items,str) or isinstance(items,unicode):
                newdataline.append(items.replace('\n', '').replace('\r', '').replace('\r\n',''))
            else:
                newdataline.append(items)
        return newdataline

    def __addItem(self,ItemName,data):
        if ItemName in data:
            return data[ItemName][0]
        else:
            return ''

    def __decodeExtensionData(self,encripteddata):
        data=[]
        fields=set()
        decripteddata=[]
        for items in encripteddata:
            dd=cPickle.loads(base64.b64decode(items[1]))
            decripteddata.append([items[0],dd])
            fields.update(dd.keys())

        for items in decripteddata:
            dataline=[]
            dataline.append(items[0])
            for fieldnames in fields:
                dataline.append(self.__addItem(fieldnames,items[1]))
            data.append(self.__stripNewlines(dataline))
        fields=list(fields)
        fields.insert(0,'PupilId')

        return [fields,data]


    def __decodeGradeData(self,encripteddata):
        data=[]
        fields=set()
        decripteddata=[]
        for items in encripteddata:
            dd=cPickle.loads(base64.b64decode(items[1]))
            decripteddata.append([items[0],dd,items[2]])
            fields.update(dd.keys())

        for items in decripteddata:
            dataline=[]
            dataline.append(items[0])
            dataline.append(items[2])
            for fieldnames in fields:
                dataline.append(self.__addItem(fieldnames,items[1]))
            data.append(dataline)
        fields=list(fields)
        fields.insert(0,'PupilId')
        fields.insert(1,'Subject')
        return [fields,data]

    def __getContactAddress(self,contactid):
        families=FamilyContact.objects.filter(Priority__lte=3,Contact__id=contactid)
        if families.exists():
            addresses=families[0].FamilyId.Address.all()
            return "%d" % addresses[0].id
        else:
            return ''

    def RunExtract(self):
        self.AddressList()
        self.SubjectsExtract()
        self.SchoolListExtract()
        self.PupilsPastSchoolExtract()
        self.PupilsNextSchoolExtract()
        self.ProgressToFutureSchool()
        self.GroupExtract()
        self.PupilGroupList()
        self.ApplicantGroupExtract()
        self.StaffGroupList()
        self.PupilNotesExtract()
        self.PupilAlertsExtract()
        self.PupilDocumentsExtract()
        self.PupilBaseExtract()
        self.PupilExtraExtract()
        self.PupilEALExtract()
        self.PupilSENExtract()
        self.PupilGradeDataYrRExtract()
        self.PupilGradeDataYr1Extract()
        self.PupilGradeDataYr2Extract()
        self.PupilGradeDataYr3Extract()
        self.PupilGradeDataYr4Extract()
        self.PupilGradeDataYr5Extract()
        self.PupilGradeDataYr6Extract()
        self.PupilGradeDataYr7Extract()
        self.PupilGradeDataYr8Extract()
        self.PupilCustomFields()
        self.ContactCustomFields()
        self.StaffCustomFields()
        self.FamilyDetailsExtract()
        self.SiblingsExtract()
        self.ContactExtract()
        self.ContactExtraDataExtract()
        self.PupilTargetsExtract()
        self.PupilProvisionsExtract()
        self.PupilMAExtract()
        self.PupilAttendanceExtract()
        self.StaffExtract()
        self.StaffEmergencyContactExtract()
        self.StaffExtraExtract()
        self.StaffCPDRecordsExtract()
        self.Letters_BodyText()
        self.Letters_Templates()


    def AddressList(self):
        data=Address.objects.filter(Active=True).values_list('id','HomeSalutation','PostalTitle','AddressLine1','AddressLine2',
                                                             'AddressLine3','AddressLine4','PostCode','Country',
                                                             'Phone1','Phone2','Phone3','EmailAddress')
        self.__WritetoCSV(data,'AddressList',['id','HomeSalutation','PostalTitle','AddressLine1','AddressLine2',
                                              'AddressLine3','AddressLine4','PostCode','Country','Phone1',
                                              'Phone2','Phone3','EmailAddress'])
    def SubjectsExtract(self):
        '''
        list of subjects held in WillowTree
        '''
        data=Subject.objects.all().values_list('id','Name','Desc')
        self.__WritetoCSV(data,'SubjectList',['id','Name','Desc'])

    def SchoolListExtract(self):
        '''
        List of Other schools held in WillowTree
        '''
        data=SchoolList.objects.all().values_list('id','Name','Type','HeadTeacher','Desc')
        self.__WritetoCSV(data,'SchoolList',['id','Name','Type','HeadTeacher','Desc'])

    def PupilsPastSchoolExtract(self):
        '''
        List of connections between a pupil record and their past school
        '''
        data=PupilsPastSchool.objects.all().values_list('PupilId','SchoolId')
        self.__WritetoCSV(data,'PastSchoolsConnections',['PupilId','SchoolId'])

    def PupilsNextSchoolExtract(self):
        '''
        List of connections to the pupils next school ggg
        '''

        data=PupilsNextSchool.objects.all().values_list('PupilId','SchoolId')
        self.__WritetoCSV(data,'NextSchoolConnections',['PupilId','SchoolId'])

    def ProgressToFutureSchool(self):
        '''
        Records Tracking a pupils progress on their next school application
        '''
        data=PupilFutureSchoolProgress.objects.filter(Active=True).values_list('Pupil','School','LastUpdatedBy','Progress','EntryYear','Interview','Details')
        self.__WritetoCSV(data,'FutureSchoolProgress',['Pupil','School','LastUpdatedBy','Progress','EntryYear','Interview','Details'])

    def GroupExtract(self):
        self.__WritetoCSV(Group.objects.all().values_list('id','Name'),'GroupList',['id','Name'])


    def PupilGroupList(self):
        '''
        records Linking a pupil to a group
        '''
        data=PupilGroup.objects.filter(Active=True,AcademicYear=self.AcYear)
        extractdata=[]
        for items in data:
            extractdata.append([items.Pupil.id,items.Group.Name])
        self.__WritetoCSV(extractdata,'PupilGroupList',['Pupil','Group'])

    def ApplicantGroupExtract(self):
        data=PupilGroup.objects.filter(Active=True,AcademicYear__in=['2015-2016','2016-2017','2017-2018','2018-2019','2019-2020'])
        extractdata=[]
        for items in data:
            extractdata.append([items.Pupil_id,items.AcademicYear,items.Group.Name])
        self.__WritetoCSV(extractdata,'ApplicantGroupList',['Pupil','AcYear','Group'])

    def StaffGroupList(self):
        '''
        records Linking a pupil to a group
        '''
        data=StaffGroup.objects.all()
        extractdata=[]
        for items in data:
            extractdata.append([items.Staff.id,items.Group.Name])
        self.__WritetoCSV(extractdata,'StaffGroupList',['Staff','Group'])

    def PupilNotesExtract(self):
        data=PupilNotes.objects.filter(Active=True)
        extractdata=[]
        for items in data:
            extractdata.append([items.PupilId.id,
                                items.NoteId.Keywords,
                                items.NoteId.NoteText,
                                items.NoteId.RaisedBy])
        self.__WritetoCSV(extractdata,'PupilNotes',['Pupil','Keywords','NoteText','RaisedBy'])

    def PupilAlertsExtract(self):
        '''
        Alerts are customised flags attached to each child. The ones we have now are SEN1-5, EAL1-5, MoreAble,
        Mostable, Heart (emotional issue), and left handed
        '''
        data=PupilAlert.objects.filter(Active=True)
        extractdata=[]
        for items in data:
            extractdata.append([items.PupilId.id,
                               items.StartDate,
                               items.StopDate,
                               items.AlertType.Name,
                               items.AlertDetails])
        self.__WritetoCSV(extractdata,'PupilAlerts',['Pupil','Startdate','Stopdate','Alert','AlertDetails'])

    def PupilDocumentsExtract(self):
        '''
        These are documents attached to pupils and stored on an AWS S3 bucket.
        The details here will say where the document is on the S3 bucket and
        which child it is attached to
        '''
        data=PupilDocument.objects.all()
        extractdata=[]
        for items in data:
            extractdata.append([items.PupilId.id,
                                items.DocumentId.id,
                                items.DocumentId.DocumentName,
                                items.DocumentId.Filename,
                                items.DocumentId.UploadDate])
        self.__WritetoCSV(extractdata,'PupilDocuments',['Pupil','Document Id','Doc Name','Filename','UploadDate'])


    def PupilBaseExtract(self):
        data=Pupil.objects.all()
        extractdata=[]
        for items in data:
            PupilPic=str(items.Picture).split('/')[-1]
            if "student" in PupilPic.lower():
                PupilPic=""
            extractdata.append([items.id,
                                items.Forename,
                                items.OtherNames,
                                items.Surname,
                                items.NickName,
                                items.EmailAddress,
                                items.Title,
                                items.DateOfBirth,
                                items.Gender,
                                PupilPic,
                                items.RegistrationDate])
        self.__WritetoCSV(extractdata,'PupilBase',['id','Forename','OtherNames',
                                                   'Surname','NickName','EmailAddress','Title',
                                                   'DOB','Gender','Picture','RegDate'])
    def PupilExtraExtract(self):
        decriptdata=self.__decodeExtensionData(ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                                                            BaseId__in=Pupil.objects.all().values_list('id'),
                                                                            ExtentionRecord__Name='PupilExtra').values_list('BaseId','Data'))
        self.__WritetoCSV(decriptdata[1],'PupilExtra',decriptdata[0])


    def PupilEALExtract(self):
        decriptdata=self.__decodeExtensionData(ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                                                            BaseId__in=Pupil.objects.all().values_list('id'),
                                                                            ExtentionRecord__Name='PupilEAL').values_list('BaseId','Data'))
        self.__WritetoCSV(decriptdata[1],'PupilEAL',decriptdata[0])


    def PupilSENExtract(self):
        decriptdata=self.__decodeExtensionData(ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                                                            BaseId__in=Pupil.objects.all().values_list('id'),
                                                                            ExtentionRecord__Name='PupilSEN').values_list('BaseId','Data'))
        self.__WritetoCSV(decriptdata[1],'PupilSEN',decriptdata[0])


    def PupilGradeDataYrRExtract(self):
        gradedata=ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                               BaseId__in=Pupil.objects.all().values_list('id'),
                                               ExtentionRecord__Name__istartswith='YrR').values_list('BaseId','Data','Subject')
        decriptdata=self.__decodeGradeData(gradedata)
        self.__WritetoCSV(decriptdata[1],'PupilGradeDataReception',decriptdata[0])

    def PupilGradeDataYr1Extract(self):
        gradedata=ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                               BaseId__in=Pupil.objects.all().values_list('id'),
                                               ExtentionRecord__Name__istartswith='Yr1').values_list('BaseId','Data','Subject')
        decriptdata=self.__decodeGradeData(gradedata)
        self.__WritetoCSV(decriptdata[1],'PupilGradeDataYear1',decriptdata[0])

    def PupilGradeDataYr2Extract(self):
        gradedata=ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                               BaseId__in=Pupil.objects.all().values_list('id'),
                                               ExtentionRecord__Name__istartswith='Yr2').values_list('BaseId','Data','Subject')
        decriptdata=self.__decodeGradeData(gradedata)
        self.__WritetoCSV(decriptdata[1],'PupilGradeDataYear2',decriptdata[0])


    def PupilGradeDataYr3Extract(self):
        gradedata=ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                               BaseId__in=Pupil.objects.all().values_list('id'),
                                               ExtentionRecord__Name__istartswith='Yr3').values_list('BaseId','Data','Subject')
        decriptdata=self.__decodeGradeData(gradedata)
        self.__WritetoCSV(decriptdata[1],'PupilGradeDataYear3',decriptdata[0])

    def PupilGradeDataYr4Extract(self):
        gradedata=ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                               BaseId__in=Pupil.objects.all().values_list('id'),
                                               ExtentionRecord__Name__istartswith='Yr4').values_list('BaseId','Data','Subject')
        decriptdata=self.__decodeGradeData(gradedata)
        self.__WritetoCSV(decriptdata[1],'PupilGradeDataYear4',decriptdata[0])

    def PupilGradeDataYr5Extract(self):
        gradedata=ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                               BaseId__in=Pupil.objects.all().values_list('id'),
                                               ExtentionRecord__Name__istartswith='Yr5').values_list('BaseId','Data','Subject')
        decriptdata=self.__decodeGradeData(gradedata)
        self.__WritetoCSV(decriptdata[1],'PupilGradeDataYear5',decriptdata[0])

    def PupilGradeDataYr6Extract(self):
        gradedata=ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                               BaseId__in=Pupil.objects.all().values_list('id'),
                                               ExtentionRecord__Name__istartswith='Yr6').values_list('BaseId','Data','Subject')
        decriptdata=self.__decodeGradeData(gradedata)
        self.__WritetoCSV(decriptdata[1],'PupilGradeDataYear6',decriptdata[0])

    def PupilGradeDataYr7Extract(self):
        gradedata=ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                               BaseId__in=Pupil.objects.all().values_list('id'),
                                               ExtentionRecord__Name__istartswith='Yr7').values_list('BaseId','Data','Subject')
        decriptdata=self.__decodeGradeData(gradedata)
        self.__WritetoCSV(decriptdata[1],'PupilGradeDataYear7',decriptdata[0])

    def PupilGradeDataYr8Extract(self):
        gradedata=ExtentionData.objects.filter(Active=True,BaseType='Pupil',
                                               BaseId__in=Pupil.objects.all().values_list('id'),
                                               ExtentionRecord__Name__istartswith='Yr8').values_list('BaseId','Data','Subject')
        decriptdata=self.__decodeGradeData(gradedata)
        self.__WritetoCSV(decriptdata[1],'PupilGradeDataYear8',decriptdata[0])

    def __CustomFields(self,type,filename):
        ExData=ExtentionData.objects.filter(BaseType=type,Active=True).values_list('ExtentionRecord')
        print 'got data records'
        ExRecords=ExtentionRecords.objects.filter(id__in=list(set([x[0] for x in ExData])))
        print 'got Extention Records'
        ExFields=set()
        for items in ExRecords:
            for field in items.Fields.all():
                ExFields.add(field)
        extractData=[]
        for items in ExFields:
            extractData.append([items.Name,
                                items.Type,
                                items.PickList.Data])
        self.__WritetoCSV(extractData,filename,['Name','Type','Picklist'])

    def PupilCustomFields(self):
        self.__CustomFields('Pupil','PupilCustomFields')

    def ContactCustomFields(self):
        self.__CustomFields('Contact','ContactCustomFields')

    def StaffCustomFields(self):
        self.__CustomFields('Staff','StaffCustomFields')


    def FamilyDetailsExtract(self):
        data=Family.objects.all()
        extractdata=[]
        for familyrecs in data:
            FamilyC=FamilyChildren.objects.filter(FamilyType=1,FamilyId=familyrecs).values_list('Pupil')
            FamilyA=FamilyContact.objects.filter(FamilyId=familyrecs)
            for contacts in FamilyA:
                for children in FamilyC:
                    extractdata.append([children[0],
                                        contacts.Contact.id,
                                        contacts.Relationship.Type,
                                        contacts.Priority,
                                        contacts.SMS,
                                        contacts.EMAIL])
        self.__WritetoCSV(extractdata,'RelationshipList',
                          ['Pupil','Contact','Relationship','Priority','SMS?','EMAIL?'])

    def SiblingsExtract(self):
        data=Family.objects.all()
        extractdata=[]
        for familyrecs in data:
            FamilyC=FamilyChildren.objects.filter(FamilyType=1,FamilyId=familyrecs).values_list('Pupil')
            for recs in FamilyC:
                for recs1 in FamilyC:
                    if not recs == recs1:
                        extractdata.append([recs[0],recs1[0]])

        self.__WritetoCSV(extractdata,'SiblingsList',['Pupil','Sibling'])

    def ContactExtract(self):
        data=Contact.objects.all().values_list('id','Forename','Surname','OtherNames',
                                               'EmailAddress','Title','Address','WorkAddress')
        fixeddata=[]
        for items in data:
            dataline=list(items)
            if not items[6]:
                dataline[6]=self.__getContactAddress(items[0])
                dataline.insert(7,'Check')
            else:
                dataline.insert(7,'Ok')
            fixeddata.append(dataline)


        self.__WritetoCSV(fixeddata,'ContactList',['id','Forename','Surname','OtherNames',
                                              'EmailAddress','Title','Address','Check_Address?','WorkAddress'])


    def ContactExtraDataExtract(self):
        decriptdata=self.__decodeExtensionData(ExtentionData.objects.filter(Active=True,BaseType='Contact',
                                                                            BaseId__in=Contact.objects.all().values_list('id'),
                                                                            ExtentionRecord__Name='ContactExtra').values_list('BaseId','Data'))
        self.__WritetoCSV(decriptdata[1],'ContactExtraData',decriptdata[0])

    def PupilTargetsExtract(self):
        '''
        Please Note that these are a series of targets connected to various
        Learning support issues
        '''
        self.__WritetoCSV(PupilTarget.objects.filter(Active=True).values_list('Pupil',
                                                                              'Type',
                                                                              'Target',
                                                                              'Review',
                                                                              'Progress',
                                                                              'ReviewDate'),
                          'PupilTargets',
                          ['Pupil','Type','Target','Review','Progress','ReviewDate'])

    def PupilProvisionsExtract(self):
        self.__WritetoCSV(PupilProvision.objects.filter(Active=True).values_list('Pupil',
                                                                                 'Type',
                                                                                 'SENType',
                                                                                 'Details',
                                                                                 'StartDate',
                                                                                 'StopDate'),
                          'PupilProvisions',
                          ['Pupil','Type','SENType','Details','StartDate','StopDate'])

    def PupilMAExtract(self):
        self.__WritetoCSV(PupilGiftedTalented.objects.filter(Active=True).values_list('Pupil',
                                                                                      'Subject',
                                                                                      'Details',
                                                                                      'Mentor',
                                                                                      'Level',
                                                                                      'DateIdentified',
                                                                                      'DateNotGiftedAndTalented',
                                                                                      'DateAdded',
                                                                                      'DateClosed',
                                                                                      'ReasonsForRemovalGiftedAndTalented'),
                          'PupilMostAble',
                          ['Pupil','Subject','Details','Mentor','Level','DateIdentified',
                           'DateNotGiftedAndTalented','DateAdded','DateClosed','ReasonsForRemoval'])

    def PupilAttendanceExtract(self):
        data=PupilAttendance.objects.filter(Active=True)
        extractdata=[]
        for items in data:
            extractdata.append([items.Pupil.id,
                                items.AttendanceDate,
                                items.AmPm,
                                items.Teacher.id,
                                items.RecordedDate,
                                items.Code.AttendanceCode])
        self.__WritetoCSV(extractdata,'PupilAttendance',['Pupil','AttendanceDate','AmPm','Teacher','AttendanceCode'])


    def StaffExtract(self):
        self.__WritetoCSV(Staff.objects.filter(Active=True).values_list('id','Forename','OtherNames','Surname',
                                                                        'EmailAddress','Title','DateOfBirth',
                                                                        'Gender','Address'),'StaffList',
                          ['id','Forename','OtherNames','Surname','EmailAddress','Title','DateOfBirth','Gender','Address'])

    def StaffEmergencyContactExtract(self):
        self.__WritetoCSV(StaffEmergencyContact.objects.all().values_list('StaffId','AddressId','Priority'),'StaffEmergencyContact',
                          ['Staff','Address','Priority'])

    def StaffExtraExtract(self):
        decriptdata=self.__decodeExtensionData(ExtentionData.objects.filter(Active=True,BaseType='Staff',
                                                                            BaseId__in=Staff.objects.all().values_list('id'),
                                                                            ExtentionRecord__Name='StaffExtra').values_list('BaseId','Data'))
        self.__WritetoCSV(decriptdata[1],'StaffExtra',decriptdata[0])

    def StaffCPDRecordsExtract(self):
        self.__WritetoCSV(StaffCpdRecords.objects.filter(Active=True).values_list('Staff','Name','TimeTaken',
                                                                                  'DateTaken','Duration','Details',
                                                                                  'DateExpires','CertIssued',
                                                                                  'Provider','InternalExternal'),
                          'StaffCPD',
                          ['Staff','Name','TimeTaken','DateTaken','Duration','Details','DateExpires','CertIssued',
                           'Provider','InternalExternal'])
    def Letters_BodyText(self):
        self.__WritetoCSV(LetterBody.objects.filter(Active=True).values_list('Name','BodyText','Description','LetterTemplate'),
                          'LetterBodyText',
                          ['Name','BodyText','Description','LetterTemplate'])
    def Letters_Templates(self):
        self.__WritetoCSV(LetterBody.objects.filter(Active=True).values_list('Name','FileName'),
                          'LetterTemplates',
                          ['Name','FileName'])