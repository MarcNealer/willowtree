"""
Created on Tue Feb  4 11:31:14 2014

@author: roy
"""
from MIS.modules.GeneralFunctions import Get_PupilList
from MIS.modules.gradeInput.GradeFunctions import *
import csv

def run(acYear, grp, record, subject):
    '''
    Used to extract all grade data for a group of pupils for a single subject

    example for getting out spelling data:

    run('2013-2014', 'Current.Kensington.PrepSchool.Year5', 'Yr5_Sp_M', 'Form_Comment')
    '''
    returnData = []
    pupilIds = [i.id for i in Get_PupilList(acYear, grp)]
    x = PupilGradeRecord(pupilIds[0], acYear, record)
    r = x.Grades[record].Grades()
    dataDic = r[subject].Current()
    headerData = ['Forename',
                  'Surname',
                  'Form']
    for k, v in dataDic.iteritems():
        headerData.append(k)
    returnData.append(headerData)
    for i in pupilIds:
        dataList = []
        x = PupilGradeRecord(i, acYear, record)
        r = x.Grades[record].Grades()
        dataDic = r[subject].Current()
        dataList.append(x.Pupil.Forename)
        dataList.append(x.Pupil.Surname)
        dataList.append(x.Form())
        for k, v in dataDic.iteritems():
            dataList.append(v[0])
        returnData.append(dataList)
    # return csv file
    with open('extract_%s_%s_%s.csv' % (grp, record, subject), 'wb') as csvFile:
        w = csv.writer(csvFile)
        for i in returnData:
            w.writerow(i)
    return 'operation complete'
