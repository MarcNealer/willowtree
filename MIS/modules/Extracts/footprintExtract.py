# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 15:55:49 2013

@author: marc
"""

import csv
from MIS.models import *


def footprintExtract(School,AcYear):
    subjectlist={'Year1':['English','Mathematics','Science','Games','Ballet','Gym','French','ICT','Art','Drama','Topic'],
    'Year2':['English','Mathematics','Science','Games','Ballet','Gym','French','ICT','Art','Drama','Topic'],
    'Year3':['English','Mathematics','Science','Games','Ballet','Gym','French','ICT','Art','Drama','Topic'],
    'Year4':['English','Mathematics','Science','Games','Ballet','Gym','French','ICT','Art','Drama','Topic'],
    'Year5':['English','Mathematics','Science','Games','Ballet','Gym','French','ICT','Art','Drama','Humanities','Latin'],
    'Year6':['English','Mathematics','Science','Games','Ballet','Gym','French','ICT','Art','Drama','Geography','History','Latin'],
    'Year7':['English','Mathematics','Science','Games','Ballet','Gym','French','ICT','Art','Drama','Geography','History','Latin'],
    'Year8':['English','Mathematics','Science','Games','Ballet','Gym','French','ICT','Art','Drama','Geography','History','Latin']}

    if 'Battersea' in School:
        subjectlist['Year6'].append('German')
        subjectlist['Year6'].append('Spanish')
    if 'Clapham' in School:
        subjectlist['Year7'].append('DT') 
        subjectlist['Year8'].append('DT')
        
    recordList=PupilGroup.objects.filter(Group__Name__icontains='Form',AcademicYear=AcYear,Active=True).filter(Group__Name__istartswith='Current').filter(Group__Name__icontains=School).exclude(Group__Name__icontains='Reception')
    ReportList=[]
    for recs in recordList:
        for items in subjectlist[recs.Group.Name.split('.')[3]]:
            ReportList.append({'Name':recs.Pupil.FullName(),'Gender':recs.Pupil.Gender,'Group':recs.Group.Name,'Subject':items})
    recordList=PupilGroup.objects.filter(Group__Name__icontains='Set',AcademicYear=AcYear,Active=True).filter(Group__Name__istartswith='Current').filter(Group__Name__icontains=School).exclude(Group__Name__icontains='Reception')
    for recs in recordList:
        found=False
        for items in ReportList:
            if recs.Pupil.FullName() == items['Name'] and recs.Group.Name.split('.')[-2]==items['Subject']:
                items['Group']=recs.Group.Name
    outputReport=[]
    for items in ReportList:
        outputReport.append([items['Name'],items['Gender'],items['Group'],items['Subject']])
    fileobj=open('foorprintExtract_%s.csv' % School,'wb')
    csvObj=csv.writer(fileobj)
    csvObj.writerows(outputReport)
    fileobj.close()

    