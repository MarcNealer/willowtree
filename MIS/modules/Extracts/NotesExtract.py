# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 15:55:49 2013

@author: marc
"""

import csv
from MIS.models import *
from MIS.modules.PupilRecs import *

def NoteExtract(PupilId):
    pupilrec=Pupil.objects.get(id=PupilId)
    pupilnotes=PupilNotes.objects.filter(PupilId=pupilrec,Active=True)
    dataout=[]
    for notes in pupilnotes:
        dataout.append([notes.NoteId.Keywords,notes.NoteId.RaisedBy.FullName(),notes.NoteId.RaisedDate,notes.NoteId.NoteText])
    fileobj=open('NoteExtract_%s.csv' % pupilrec.Surname,'wb')
    csvObj=csv.writer(fileobj)
    csvObj.writerows(dataout)
    fileobj.close()

    