# -*- coding: utf-8 -*-
"""
Created on Sun Jul  7 15:55:49 2013

@author: marc
"""

import csv
from MIS.models import *
from MIS.modules.PupilRecs import *


def OliverExtract(AcYear, NewAcYear):
    OldPupils=[x.Pupil.id for x in PupilGroup.objects.filter(Group__Name__istartswith='Current',AcademicYear=AcYear).exclude(Group__Name__icontains='Kindergarten').exclude(Group__Name__contains='House')]
    OldPupils=set(OldPupils)
    NewPupils=[x.Pupil.id for x in PupilGroup.objects.filter(Group__Name__istartswith='Current',AcademicYear=NewAcYear).exclude(Group__Name__icontains='Kindergarten').exclude(Group__Name__contains='House')]
    NewPupils=set(NewPupils)
    NewPupils=NewPupils.difference(OldPupils)
    pupildata=[]
    for pupilids in NewPupils:
        record=PupilRecord(pupilids,NewAcYear)
        datarow=[]
        print pupilids
        print record.Pupil.EmailAddress
        if not record.Pupil.EmailAddress:
            Email=AssignEmailAddress(pupilids,NewAcYear)            
        elif not 'thomasvle' in record.Pupil.EmailAddress:
            Email=AssignEmailAddress(pupilids,NewAcYear)
        else:
            Email=record.Pupil.EmailAddress
        datarow.append(Email.split('@')[0])
        datarow.append(record.Pupil.FirstName())
        datarow.append(record.Pupil.Surname)
        datarow.append('Student')
        datarow.append(Email)
        datarow.append('Students')
        try:
            datarow.append(PupilGroup.objects.filter(Pupil__id=pupilids,Group__Name__istartswith='Current',AcademicYear=NewAcYear)[0].Group.Name.split('.')[1])
        except:
            datarow.append('Error')
        PupilYear=record.Form()[0]
        if PupilYear=='R':
            PupilYear=0
        datarow.append(PupilYear)
        datarow.append(record.Form())
        datarow.append('%s0%s' % (datarow[0][0:-1],datarow[0][-1]))
        datarow.append(record.Pupil.Gender)
        datarow.append(record.Pupil.DateOfBirth.strftime('%d/%m/%Y'))
        datarow.append(record.Pupil.id)
        print datarow
        pupildata.append(datarow)
    fileobj=open('OliverExtract_%s.csv' % NewAcYear,'wb')
    csvObj=csv.writer(fileobj)
    csvObj.writerows(pupildata)
    fileobj.close()
    
def AssignEmailAddress(pupilid,AcYear):
    pupilrec=Pupil.objects.get(id=pupilid)
    school=PupilGroup.objects.filter(Pupil=pupilrec,Group__Name__icontains='Form',AcademicYear=AcYear).filter(Group__Name__istartswith='Current')[0].Group.Name.split('Current.')[1][:3]
    counter = 1
    found=False
    while not found:
        testemail='%s%s%d@%s.thomasvle-s.co.uk' % (pupilrec.Forename[0],pupilrec.Surname.replace(' ','').replace('-','').replace("'",'')[:3],counter,school)
        if not Pupil.objects.filter(EmailAddress=testemail).exists():
            pupilrec.EmailAddress=testemail
            pupilrec.save()
            return testemail
        else:
            counter+=1



def findOldEmails(filename):
    maillist=csv.DictReader(open(filename,'rb'))
    listout=csv.DictWriter(open('RedundantMails.csv','w'),
                           delimiter=',',fieldnames=['Email','FullName'],
                           extrasaction='ignore')
    for items in maillist:
        if not PupilGroup.objects.filter(Group__Name__istartswith='Current',
                                         Pupil__EmailAddress__iexact=items['Email'],
                                         Active=True).exists():
            listout.writerow(items)
    listout.close()
    maillist.close()

    