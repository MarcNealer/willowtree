# Django middleware imports
from django.shortcuts import HttpResponse
from django.http import HttpResponseBadRequest
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.core.context_processors import csrf
from django.http import *
from django.core.cache import cache
from django.template.loader import render_to_string
from django.utils import simplejson


# WillowTree system imports
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.ExtendedRecords import *
from MIS.modules.dataimports.importPupilPictures2 import ImportPupilPictures2

# python imports
import csv

import logging
log = logging.getLogger(__name__)

def EditPupilDetails(request,school,AcYear,PupilNo):
    ''' Modifies the Pupils Academic house and Form'''
    newPupilRecord = UpdatePupilRecord(request,school,AcYear,PupilNo)
    newPupilRecord.Base()
    if not 'Kindergarten' in MenuTypes.objects.get(Name=school).SchoolId.Name:
        if 'PassPupilReferenceCode' in request.POST:
            newPupilRecord.updatePassPupilReferenceCode(request.POST['PassPupilReferenceCode'])
    newPupilRecord.updateCache()
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,AcYear,PupilNo))

def EditPupilFormHouse(request,school,AcYear,PupilNo):
    ''' Modifies the Pupils Academic house and Form'''
    newPupilRecord = UpdatePupilRecord(request,school,AcYear,PupilNo)
    # update house
    if "newHouse" in request.POST:
        if request.POST['newHouse'] != "None":
            newPupilRecord.MoveHouse(request.POST['newHouse'])
    #end
    # update form
    if "newForm" in request.POST:
        if request.POST['newForm'] != "None":
            newPupilRecord.MoveForm(request.POST['newForm'])
    newPupilRecord.updateCache()
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,AcYear,PupilNo))



def update_pupil_picture(request, school, AcYear, PupilNo):
    """
    Allows a user to update a pupils picture. The uploaded picture is re-sized
    """
    new_pupil_record = UpdatePupilRecord(request, school, AcYear, PupilNo)
    new_pupil_record.picture()
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school, AcYear, PupilNo))


def EditPupilPermissions(request,school,AcYear,PupilNo):
    try:
        internetPermissions = request.POST['internet_permissions_checkbox']
    except:
        internetPermissions = False
    try:
        mediaPermissions = request.POST['media_permissions_checkbox']
    except:
        mediaPermissions = False
    UpdatePupilPermissions = UpdatePupilRecord(request,school,AcYear,PupilNo)
    UpdatePupilPermissions.Permissions(internetPermissions=internetPermissions,mediaPermissions=mediaPermissions)
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,AcYear,PupilNo))

def EditPupilOtherInformation(request,school,AcYear,PupilNo):
    UpdatePupilPermissions = UpdatePupilRecord(request,school,AcYear,PupilNo)
    UpdatePupilPermissions.UpdatePupilOtherInformation()
    if request.POST['pastSchool'] == 'Remove from School?' or request.POST['pastSchool'] == 'Not Known':
        try:
            removeSchool = PupilsNextSchool.objects.get(PupilId=int(PupilNo))
            removeSchool.delete()
        except:
            pass
    else:
        removeSchool = PupilsNextSchool.objects.filter(PupilId=int(PupilNo))
        for i in removeSchool:
            temp = i
            temp.delete()
        pastSchoolData = request.POST['pastSchool'].split(': ')
        pastSchoolObj = SchoolList.objects.get(Name=pastSchoolData[0])
        pupilsNextSchool = PupilsNextSchool(PupilId=Pupil.objects.get(id=int(PupilNo)),
                                            SchoolId=pastSchoolObj)
        pupilsNextSchool.save()
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,AcYear,PupilNo))


def UpdateLeavingDetails(request,school,AcYear,PupilNo):
    newPupilRecord = UpdatePupilRecord(request,school,AcYear,PupilNo)
    newPupilRecord.UpdateLeavingDetails()
    newPupilRecord.updateCache()
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,AcYear,PupilNo))
    pass


def pupilModifyMedicalDetails(request, school, AcYear, PupilNo):
    ''' adds medical data to a pupils record '''
    exRecord = ExtendedRecord('Pupil', int(PupilNo))
    exRecord.WriteExtention('PupilExtra',
                            {'pupils_medical_details': request.POST['pupilModifyMedicalDetails']})
    cache.delete('Pupil_%s' % PupilNo)
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,
                                                                 AcYear,
                                                                 PupilNo))


def pupilAddFutureSchoolsProgressDetails(request, school, AcYear, PupilNo):
    ''' adds future schools progress data to a pupils record '''
    pupilObj = Pupil.objects.get(id=int(PupilNo))
    schoolObj = SchoolList.objects.get(id=int(request.POST['school']))
    staffObj = Staff.objects.get(User__id=request.session['_auth_user_id'])
    PupilFutureSchoolProgressObj = PupilFutureSchoolProgress(Pupil=pupilObj,
                                                             School=schoolObj,
                                                             LastUpdatedBy=staffObj)
    if 'progress' in request.POST or request.POST['progress'] != '':
        PupilFutureSchoolProgressObj.Progress = request.POST['progress']
    if 'entryYear' in request.POST or request.POST['entryYear'] != '':
        PupilFutureSchoolProgressObj.EntryYear = request.POST['entryYear']
    if 'details' in request.POST or request.POST['details'] != '':
        PupilFutureSchoolProgressObj.Details = request.POST['details']
    PupilFutureSchoolProgressObj.save()
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,
                                                                 AcYear,
                                                                 PupilNo))


def pupilModifyFutureSchoolsProgressDetails(request, school, AcYear, PupilNo, schoolId):
    ''' adds future schools progress data to a pupils record '''
    futureSchProObj = PupilFutureSchoolProgress.objects.get(id=int(schoolId))
    futureSchProObj.staffObj = Staff.objects.get(User__id=request.session['_auth_user_id'])
    if 'school' in request.POST or request.POST['school'] != '':
        futureSchProObj.School = SchoolList.objects.get(id=int(request.POST['school']))
    if 'progress' in request.POST or request.POST['progress'] != '':
        futureSchProObj.Progress = request.POST['progress']
    if 'entryYear' in request.POST or request.POST['entryYear'] != '':
        futureSchProObj.EntryYear = request.POST['entryYear']
    if 'details' in request.POST or request.POST['details'] != '':
        futureSchProObj.Details = request.POST['details']
    futureSchProObj.save()
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,
                                                                 AcYear,
                                                                 PupilNo))


def  pupilDeleteFutureSchoolsProgressDetails(request, school, AcYear, PupilNo, schoolId):
    ''' deletes future schools progress data to a pupils record '''
    futureSchProObj = PupilFutureSchoolProgress.objects.get(id=int(schoolId))
    futureSchProObj.delete()
    staffId = Staff.objects.get(User__id=request.session['_auth_user_id']).id
    log.warn('staff (%s) deleted future schools progress data from pupil (%s)' % (staffId,
                                                                                  PupilNo))
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,
                                                                 AcYear,
                                                                 PupilNo))


def pupilModifyFutureSchoolsProgressInterview(request, school, AcYear, PupilNo, schoolId):
    ''' modifies future schools progress interview details '''
    futureSchProObj = PupilFutureSchoolProgress.objects.get(id=int(schoolId))
    InterviewDetails = datetime.datetime.strptime('%s-%s' % (request.POST['interviewTime'],
                                                             request.POST['interviewDate']),
                                                             '%H:%M-%Y-%m-%d')
    futureSchProObj.Interview = InterviewDetails
    futureSchProObj.save()
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,
                                                                 AcYear,
                                                                 PupilNo))


def mass_upload_of_pupil_pictures_home(request, school, ac_year):
    """
    Mass upload of pupil pictures home page.
    """
    c = {'school': school,
         'AcYear': ac_year}
    con_inst = RequestContext(request, processors=[SetMenu])
    return render_to_response('mass_upload_of_pupil_pictures_home.html', c,
                              context_instance=con_inst)


def mass_upload_of_pupil_pictures(request, school, ac_year):
    """
    Mass upload of pupil pictures.
    """
    try:
        messages = ImportPupilPictures2(request).upload_pupil_pictures()
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="mass_upload_of_pupil_pictures_messages_to_user.csv"'

        writer = csv.writer(response)
        writer.writerow(["file_name", "message to user", "success or fail"])
        for message in messages:
            writer.writerow([message[0], message[1], message[2]])
        return response
    except Exception as error:
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename="mass_upload_of_pupil_pictures_error.csv"'
        writer = csv.writer(response)
        writer.writerow(["success or fail"])
        writer.writerow(["FAIL: %s" % error])
        return response


def view_current_pupil_groups_for_a_school_as_a_list(request, school, ac_year):
    """
    View current pupil groups for a school as a list.
    """
    try:
        school_name = MenuTypes.objects.get(Name=school).SchoolId.Name
        pupil_groups = Group.objects.filter(Name__icontains='Current.%s' % school_name).order_by('Name').values_list('Name',
                                                                                                            flat=True)
        return HttpResponse(simplejson.dumps(pupil_groups), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)