from django.core.mail import send_mail
from django.utils.html import strip_tags
import threading

#WillowTree imports
from MIS.models import *
from MIS.modules.ExtendedRecords import ExtendedRecord
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from MIS.modules import GeneralFunctions
import logging
log = logging.getLogger(__name__)


class EmailToRingwood(threading.Thread):
    """
    Emails dat from WillowTree to Ringwood
    """
    def __init__(self, data):
        threading.Thread.__init__(self)
        self.data = data
        self.willowtree_email = SystemVarData.objects.get(Variable__Name='WillowTree Email Address').Data
        self.ringwood_email = [SystemVarData.objects.get(Variable__Name='Ringwood Email Address').Data]

    def run(self):
        try:
            self.__send_email__('Message from WillowTree - DO NOT REPLY',
                                self.willowtree_email,
                                self.ringwood_email)
        except Exception, e:
            log.warn("EMAIL ERROR FROM emailToRingwood CLASS: %s" % e)

    def __send_email__(self, header, sent_from, sent_to_list):
        """
        Simple function that sends emails
        """
        send_mail(header,
                  self.data,
                  sent_from,
                  sent_to_list,
                  fail_silently=False)


class willowTreeEmails():
    ''' class for sending emails from Willow Tree '''
    def __init__(self, request,
                 pupilList,
                 fromNoteViews=False):
        if fromNoteViews:
            if 'emailPupils' in request.POST:
                self.emailPupils = True
                self.pupilAddressList = []
                for pupil in pupilList:
                    self.pupilAddressList.append(pupil.EmailAddress)
            else:
                self.emailPupils = False
            if 'emailContacts' in request.POST:
                self.emailContacts = True
                self.listOfContactEmailAddress = []
                listOfFamilyIds = []
                for pupil in pupilList:
                    x = FamilyChildren.objects.get(Pupil__id=int(pupil.id))
                    listOfFamilyIds.append(x.FamilyId.id)
                for family in listOfFamilyIds:
                    famEmailAddress = []
                    famObj = Family.objects.get(id=family)
                    for email in famObj.Address.all():
                        if email.EmailAddress != '':
                            famEmailAddress.append(email.EmailAddress)
                    if len(famEmailAddress) > 0:
                        for i in famEmailAddress:
                            self.listOfContactEmailAddress.append(i)
                    else:
                        x = FamilyContact.objects.filter(FamilyId=int(family))
                        for contact in x:
                            if contact.Priority == 1:
                                contactEmail = contact.Contact.EmailAddress
                                self.listOfContactEmailAddress.append(contactEmail)
            else:
                self.emailContacts = False
            self.emailFrom = request.POST['emailFrom']
            self.emailHeader = request.POST['emailHeader']
            self.emailMessage = request.POST['NoteTextArea2']
            self.emailMessageTextOnly = strip_tags(self.emailMessage)
            self.emailAddressList = self.__defineEmailList__()

    def __defineEmailList__(self):
        ''' arranges a list of emails to send '''
        if self.emailPupils == True and self.emailContacts == True:
            emailsList = self.pupilAddressList + self.listOfContactEmailAddress
        elif self.emailPupils == True:
            emailsList = self.pupilAddressList
        elif self.emailContacts == True:
            emailsList = self.listOfContactEmailAddress
        else:
            emailsList = []
        return emailsList

    def send(self):
        ''' method to send emails, returns a tuple containing email header,
        who from and address list. NOTE: This sends the Email as text only'''
        send_mail(self.emailHeader,
                  self.emailMessageTextOnly,
                  self.emailFrom,
                  self.emailAddressList,
                  fail_silently=True)
        emailResult = (self.emailHeader,
                       self.emailFrom,
                       self.emailAddressList)
        return emailResult


def newEmailAccountRequest(obj, Type):
    '''
    When a new member of staff or pupil is added to Willow Tree, the system
    will send an email to IT Support so that they may create an email account.

    Note: When pupils join the schools en mass at the beginning of the academic
    year this function is not used - instead a csv extract is used to import
    to the email system.

    obj = either a Staff or Pupil object
    Type = either 'pupil' or 'staff' string
    '''
    try:
        JobTitle = ExtendedRecord('Staff',
                                  int(obj.id)).ReadExtention('StaffExtra')['staffJobTitle'][0]
    except:
        JobTitle = 'N/A'

    try:
        staffStartDateObj = ExtendedRecord('Staff',
                                           int(obj.id)).ReadExtention('StaffExtra')['staffStartDate'][0]
        staffStartDate = '%s-%s-%s' % (staffStartDateObj.day,
                                       staffStartDateObj.month,
                                       staffStartDateObj.year)
    except:
        staffStartDate = 'N/A'
    if Type == 'staff':
        school = obj.DefaultMenu.SchoolId.Name
    if Type == 'pupil':
        school = PupilGroup.objects.filter(Pupil__id=obj.id,
                                           Group__Name__icontains="Current",
                                           Active=True)[0]
        school = school.Group.Name.split('.')[1]
    emailHeader = 'WillowTree: Email Account Generation Request'
    emailMessageTextOnly = '''
    New Willow Tree %s with the following details needs an email account to be set-up:\n
    Id: %s
    Start date: %s
    Job title: %s
    Forename: %s
    Surname: %s
    School: %s
    EmailAddress: %s\n\n
    Regards,
    WillowTree\n\n''' % (Type.capitalize(),
                         obj.id,
                         staffStartDate,
                         JobTitle,
                         obj.Forename,
                         obj.Surname,
                         school,
                         obj.EmailAddress)
    emailFrom = SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data
    emailAddressList = SystemVarData.objects.get(Variable__Name="IT Support Email").Data
    send_mail(emailHeader,
              emailMessageTextOnly,
              emailFrom,
              [emailAddressList],
              fail_silently=True)
    return

class EmailFromNoteSendThread(threading.Thread):
    ''' This is a threading class used to issue emails to staff members
    when a Note is raised'''
    def __init__(self,NoteRec,StaffRec,school):
        threading.Thread.__init__(self)
        self.AcYear=GlobalVariables.Get_SystemVar('CurrentYear')
        self.NoteRec=NoteRec
        self.StaffRec=StaffRec
        self.school=school
        self.StaffGroupList=set()
        self.GroupList=set()
        self.EmailList=set()
        self.ReceiptRequired=True
        self.EmailHeader=''
        self.EmailText=''
        self.MultiplePupils=False
        self.PupilName=[]
        self.AttachedPupils()
        self.SenderDetails()

    def run(self):
        if self.IsEmailRequired():
            self.GetStaffEmailList()
            if self.EmailList:
                self.CompileEmail()
                self.SendEmails()
                self.SendReceipt()

    def IsEmailRequired(self):
        IsRequired=False
        NoteKeywordList=set(self.NoteRec.Keywords.split(':'))
        log.warn(self.GroupList)
        CheckList=NoteNotification.objects.filter(Pupilgroup__Name__in=self.GroupList)
        for recs in CheckList:
            log.warn(recs)
            if set(recs.Keywords.split(':')) <= NoteKeywordList:
                IsRequired=True
                self.StaffGroupList.add(recs.NotificationGroup)
        return IsRequired

    def GetStaffEmailList(self):
        log.warn(self.StaffGroupList)
        for group in self.StaffGroupList:
            staffList=StaffGroup.objects.filter(Group=group)
            log.warn(staffList)
            for staff in staffList:
                if staff.Staff.EmailAddress:
                    self.EmailList.add(staff.Staff.EmailAddress)
        return

    def CompileEmail(self):
        if self.MultiplePupils:
            self.EmailHeader = 'Note Raised for multiple pupils -- Keywords(%s)' % self.NoteRec.Keywords
        else:
            self.EmailHeader =' Note Raised for %s -- Keywords (%s)' % (self.PupilName[0], self.NoteRec.Keywords)
        self.EmailText=''' A Note has been raised for %s \n
        with Keywords %s \n
        NoteDetails:\n
        %s \n\n
        Note Raised By %s''' % (','.join(self.PupilName),self.NoteRec.Keywords,
                                strip_tags(self.NoteRec.NoteText),self.NoteRec.RaisedBy.FullName())

    def SendEmails(self):
        try:
            if self.EmailList:
                for recipient in self.EmailList:
                    log.warn(recipient)
                    send_mail(self.EmailHeader,
                              self.EmailText,
                              self.WTEmail,
                              [recipient],
                              fail_silently=True)
                    log.warn('Note Email sent to %s' % recipient)
        except Exception, error:
            log.warn('Problem with EmailFromNoteSendThread class: %s' % error)
    def SendReceipt(self):
        if self.ReceiptRequired:
            Header=self.EmailHeader + ' --Receipt'
            emailtext='Note Email Receipt \n The following were emailed copies of your note \n'
            for recipients in self.EmailList:
                emailtext+='%s \n' % recipients
            log.warn(emailtext)
            send_mail(Header,
                      emailtext,
                      self.WTEmail,
                      [self.SenderEmail],
                      fail_silently=True)

    def AttachedPupils(self):
        try:
            listOfPupils=PupilNotes.objects.filter(NoteId=self.NoteRec)
            if listOfPupils.count() > 1:
                self.MultiplePupils=True
            for pupils in listOfPupils:
                log.warn(pupils)
                self.PupilName.append(pupils.PupilId.FullName())
                pupilGroupList=PupilGroup.objects.filter(Pupil=pupils.PupilId,AcademicYear=self.AcYear)
                log.warn(pupilGroupList)
                for groups in pupilGroupList:
                    log.warn(groups.Group.Name)
                    newgroups=GeneralFunctions.SubGroups(groups.Group.Name)
                    log.warn(newgroups)
                    self.GroupList=self.GroupList.union(newgroups)
        except:
            log.warn('Attached Pupils Failed')

    def SenderDetails(self):
        self.WTEmail=SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data
        if self.NoteRec.RaisedBy:
            self.SenderName=self.NoteRec.RaisedBy.FullName()
            if self.NoteRec.RaisedBy.EmailAddress:
                self.SenderEmail=self.NoteRec.RaisedBy.EmailAddress
            else:
                self.SenderEmail=SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data
                self.SenderName+=' No email address on Record'
                self.ReceiptRequried=False
        else:
            self.SenderEmail=SystemVarData.objects.get(Variable__Name="WillowTree Email Address").Data
            self.SenderName='WillowTree'
            self.ReceiptRequried=False


class EmailFromIssueThread(EmailFromNoteSendThread):
    def __init__(self,NoteRec,StaffRec,school):
        EmailFromNoteSendThread.__init__(self,NoteRec,StaffRec,school)

    def IsEmailRequired(self):
        log.warn('Check to see if its an issue')
        IsRequired=False
        NoteKeywordList=set(self.NoteRec.Keywords.split(':'))
        log.warn(self.GroupList)
        CheckList=NoteToIssue.objects.filter(Pupilgroup__Name__in=self.GroupList)
        for recs in CheckList:
            log.warn(recs)
            if set(recs.Keywords.split(':')) <= NoteKeywordList:
                IsRequired=True
                self.StaffGroupList.add(recs.NotificationGroup)
        log.warn(IsRequired)
        return IsRequired

    def run(self):
        try:
            if not self.MultiplePupils:
                if self.IsEmailRequired():
                    self.GetStaffEmailList()
                    if self.EmailList:
                        self.CompileEmail()
                        self.SendEmails()
                        self.SendReceipt()
        except Exception, error:
            log.warn('Problem with EmailFromIssueThread class: %s' % error)

    def CompileEmail(self):
        if not self.MultiplePupils:
            self.NoteRec.IsAnIssue=True
            self.NoteRec.IssueStatus='Open'
            self.NoteRec.save()
            self.EmailHeader =' Issue Raised for %s -- Keywords (%s)' % (self.PupilName[0], self.NoteRec.Keywords)
            self.EmailText=''' An Issue has been opened for %s \n
            with Keywords %s \n
            NoteDetails:\n
            %s \n\n
            Issue Raised By %s''' % (','.join(self.PupilName),self.NoteRec.Keywords,
                                    strip_tags(self.NoteRec.NoteText),self.NoteRec.RaisedBy.FullName())

    def SendReceipt(self):
        if self.ReceiptRequired:
            Header=self.EmailHeader + ' --Receipt'
            emailtext='Issue Email Receipt \n Your Note has been raised to and issue and the following were emailed copies of your Issue \n'
            for recipients in self.EmailList:
                emailtext+='%s \n' % recipients
            send_mail(Header,
                      emailtext,
                      self.SenderEmail,
                      [self.SenderEmail],
                      fail_silently=True)