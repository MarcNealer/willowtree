# Django imports
from django.core.mail import send_mail
from django.utils.html import strip_tags
from django.http import HttpResponseRedirect

# WillowTree imports
from MIS.models import SystemVarData
from MIS.models import MenuTypes


def testEmail(request, school, AcYear):
    ''' test if email works '''
    send_mail('from willow tree test',
              'Hello World 2?',
              'rhanley@thomas-s.co.uk',
              ['royhanley8@gmail.com'],
              fail_silently=False)
    httpAddress = '/WillowTree/%s/%s/Welcome/'
    tupleData = (school, AcYear)
    return HttpResponseRedirect(httpAddress % tupleData)


def emailItSupport(request, school, AcYear):
    ''' email IT Support from WillowTree.

    Remember to include 'itSupportText' textarea and a 'lastURL' options from
    the html page to here.

    The 'lastURL' option is got by adding a "'path': request.get_full_path"
    variable and passing that to the template.
    '''
    itSupportEmail = SystemVarData.objects.get(Variable__Name="IT Support Email",
                                               Variable__VariableType="System")
    staffObj = request.session['StaffId']
    nameSchoolTup = (staffObj.id,
                     staffObj.Forename,
                     staffObj.Surname,
                     MenuTypes.objects.get(Name=school).SchoolId.Name)
    if staffObj.EmailAddress is not '':
        emailFrom = staffObj.EmailAddress
    else:
        emailFrom = staffObj.Address.EmailAddress
    message = "%s\n\nRegards,\nId-%s %s %s in %s\n\n" % (request.POST['itSupportText'],
                                                         staffObj.id,
                                                         staffObj.Forename,
                                                         staffObj.Surname,
                                                         MenuTypes.objects.get(Name=school).SchoolId.Name)
    send_mail("Willow Tree Issue from: Id-%s %s %s in %s" % nameSchoolTup,
              strip_tags(message),
              emailFrom,
              [itSupportEmail.Data],
              fail_silently=True)
    httpAddress = request.POST['lastURL']
    return HttpResponseRedirect(httpAddress)
