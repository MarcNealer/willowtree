'''

Change Log:
12/03/12: dbmgr - Updated MoveToKindie to check to see if the Record exists before 
                  Creating one.
    
'''

# Base python libray imports

import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q
from django.conf import settings
from django.core.servers.basehttp import FileWrapper
import csv

# WillowTree system imports
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.notes import NoteFunctions
from MIS.modules.LettersLabels import LetterFunctions
from MIS.modules.PotentialGroups import PotentialGroupsFunctions
from MIS.modules.manager import ManagerFunctions


def Manage(request, school, AcYear, NewAcYear, GroupName):
    ''' General Manager views. This view is the main one used by teacher
    to work with a class. It shows the list of pupils, some selected
    data, plus a series of actions from a dropdown.'''
    classObj=PotentialGroupsFunctions.PotentialGroup(school,GroupName,AcYear,NewAcYear)
    c={'school': school,
       'AcYear':AcYear,
       'ClassList':classObj.getGroupList(),
       'ClassObj':classObj,
       'LetterList':LetterFunctions.LetterManager.GetlistOfLetters(classObj.transferRule.ApplicantGroup.Name),
       'GroupName':GroupName,
       'School':school,
       'AcYear':AcYear,
       'PotentialGroup':True,
       'KWList':NoteFunctions.KeywordList('Note'),
       'userEmailAddress': GeneralFunctions.getUserEmailAddress(request)}
    return render_to_response('PotentialManager.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def MoveClass(request,school,AcYear,GroupName, NewAcYear):
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        updateRec=UpdatePupilRecord(request,school,AcYear, int(Pupil_Id))
        updateRec.UpdateNewFormClass()
    return HttpResponseRedirect('/WillowTree/%s/%s/PotentialGroup/%s/%s/' % (school, AcYear, GroupName,NewAcYear))
    
def AssignHouse(request,school,AcYear,GroupName,NewAcYear):
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        updateRec=UpdatePupilRecord(request,school,AcYear, int(Pupil_Id))
        updateRec.MoveHouse(request.POST['NewHouse'])
    return HttpResponseRedirect('/WillowTree/%s/%s/PotentialGroup/%s/%s/' % (school, AcYear, GroupName,NewAcYear))

def ModifyTeacherVariables(request,school,AcYear,GroupName,NewAcYear):
    newvars=GlobalVariablesFunctions.GlobalVariables('Yearly',NewAcYear)
    if 'FormTeacher' in request.POST:
        newvars.safeWrite_SystemVar("%s.FormTeacher" % GroupName,'String',request.POST['FormTeacher'])
    if 'FormTeachersAll' in request.POST:
        newvars.safeWrite_SystemVar("%s.FormTeachersAll" % GroupName,'String',request.POST['FormTeachersAll'])
    if 'Tutors' in request.POST:
        newvars.safeWrite_SystemVar("%s.Tutors" % GroupName,'String',request.POST['Tutors'])
    return HttpResponseRedirect('/WillowTree/%s/%s/PotentialGroup/%s/%s/' % (school, AcYear, GroupName,NewAcYear))

def ClassList(request,school,AcYear,GroupName,NewAcYear):
    c={'class': PotentialGroupsFunctions.PotentialClassList(request.POST,GroupName,school)}
    return render_to_response('ClassLists/BaseClassList.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    
    
def PotentialCSV(request, school, AcYear, GroupName, NewAcYear,FileName='PotentialClassList.csv'):
    ClassList=[]
    ManageType=None
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        ClassList.append(PupilRecord(Pupil_Id,NewAcYear))
    return ManagerFunctions.ReturnCSV(request,
                                      AcYear,
                                      GroupName,
                                      FileName,
                                      ManageType).PotentialCSV(ClassList)
def ChooseClasslists(request,school,AcYear,NewAcYear):
    c={'formList':GeneralFunctions.GetForms(school),'NewAcYear':NewAcYear,'School':school,'AcYear':AcYear,}
    return render_to_response('PotentialClassListSelect.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    
def MultiClassListsGen(request, school, AcYear, NewAcYear):
    c = {'GroupList':PotentialGroupsFunctions.PotentialClassLists(school,AcYear,NewAcYear,request.POST)}
    return render_to_response('ClassLists/BaseClassListMultiple.html',c,context_instance=RequestContext(request,processors=[SetMenu]))