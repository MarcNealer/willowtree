from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.globalVariables.GlobalVariablesFunctions import *

'''
Module Potential Group Functions
Created by dbmgr 12/05/2013

This module contains classes for creating actions and functions for
potential groups. Potential groups are groups of children that may
exist in future academic years.
'''

class PotentialGroup():
    def __init__(self,school,NewGroup,AcYear,NewAcYear):
        self.transferRule = RollOverRule.objects.get(NewGroup__Name=NewGroup)
        self.newParentGroup=GeneralFunctions.GetParentGroup(self.transferRule.NewGroup.Name)
        self.NewGroup=NewGroup
        self.school=school
        self.AcYear = AcYear
        self.NewAcYear = NewAcYear
        self.__getTeacherVars__()
    def __GetOldRepeater__(self):
        '''
        Internal Method: Do not run outside of this class

        This method searches for any pupils that are marked for repeating
        their current year.
        '''
        RepeaterList=set()
        if self.transferRule.NewGroup==None:
            return RepeatList
        else:
            PupilList=GeneralFunctions.Get_PupilList(AcYear=self.AcYear,GroupName=self.transferRule.NewGroup.Name)
            for pupils in PupilList:
                PupilRec=PupilObject(pupils.id,self.AcYear)
                if PupilRec.hasAlert('Repeating'):
                    RepeaterList.add(PupilRec)
            return RepeaterList
    def __GetRepeater__(self):
        RepeaterList=set()
        if self.transferRule.NewGroup==None:
            return RepeaterList
        pupillist=GeneralFunctions.Get_PupilList(AcYear=self.AcYear,GroupName=self.newParentGroup)
        alertlist=PupilAlert.objects.filter(PupilId__in=pupillist,AlertType__Name__icontains='Repeat',Active=True)
        if alertlist.exists():
            for alert in alertlist:
                xPupilRec=PupilObject(alert.PupilId.id,self.AcYear)
                if 'NewFormClass' in xPupilRec.Extended():
                    newClass=xPupilRec.Extended()['NewFormClass'][0]
                else:
                    newClass=''
                if ((xPupilRec.isInGroup(self.transferRule.NewGroup.Name)) and not (self.NewAcYear in newClass)) or ((self.NewAcYear in newClass) and (self.transferRule.NewGroup.Name in newClass)):
                    RepeaterList.add(xPupilRec)
        return RepeaterList

    def __GetOldGroup__(self):
        CurrentList=set()
        if self.transferRule.OldGroup==None:
            return CurrentList
        else:
            PupilList=GeneralFunctions.Get_PupilList(AcYear=self.AcYear,
                                                     GroupName=GeneralFunctions.GetParentGroup(self.transferRule.OldGroup.Name))
            for pupils in PupilList:
                PupilRec=PupilObject(pupils.id,self.AcYear)
                extFields=PupilRec.Extended()
                if 'NewFormClass' in extFields:
                    NewFormClass=extFields['NewFormClass'][0]
                else:
                    NewFormClass=''
                if 'LeaverStatus' in extFields:
                    LeaverStatus=extFields['LeaverStatus'][0]
                else:
                    LeaverStatus=''


                if ((PupilRec.isInGroup(self.transferRule.OldGroup.Name) and not (self.NewAcYear in NewFormClass)) or (self.NewAcYear in NewFormClass and self.transferRule.NewGroup.Name in NewFormClass)) and not ( 'EOY' in LeaverStatus or 'LEFT' in LeaverStatus or PupilRec.hasAlert('Repeating')):

                    CurrentList.add(PupilRec)


            return CurrentList
    def __GetApplicants__(self):
        ApplicantList=set()
        if self.transferRule.ApplicantGroup==None:
            return ApplicantList
        else:
            PupilList=GeneralFunctions.Get_PupilList(AcYear=self.NewAcYear,GroupName=self.transferRule.ApplicantGroup.Name)
            for pupils in PupilList:
                ApplicantList.add(PupilRecord(pupils.id,self.NewAcYear))
            return ApplicantList
    def getGroupList(self):
        self.groupList=set()
        self.groupList.update(self.__GetRepeater__())
        self.groupList.update(self.__GetOldGroup__())
        self.groupList.update(self.__GetApplicants__())
        self.groupList=list(self.groupList)
        self.__GenerderCounter__()
        return sorted(self.groupList, key=lambda pupil: pupil.Pupil.Surname.lower())
    def ClassList(self):
        return self.getGroupList()
    def PossibleGroups(self):
        FormGroupList=[]
        for items in GeneralFunctions.ChildGroups(GeneralFunctions.GetParentGroup(self.transferRule.NewGroup.Name)):
            if 'Form' in items.split(".")[-2]:
                FormGroupList.append(items)
        return FormGroupList
    def PossibleHouses(self):
        return GeneralFunctions.GetAcHousesInSchool(self.school).getHouses()
    def __getTeacherVars__(self):
        self.FormTeacher=GlobalVariables.Get_SystemVar("%s.FormTeacher" % self.NewGroup, self.NewAcYear)
        if not self.FormTeacher:
            self.FormTeacher=''
        self.FormTeachersAll=GlobalVariables.Get_SystemVar("%s.FormTeachersAll" % self.NewGroup,self.NewAcYear)
        if not self.FormTeachersAll:
            self.FormTeachersAll=''
        self.Tutors=GlobalVariables.Get_SystemVar("%s.Tutors" % self.NewGroup,self.NewAcYear)
        if not self.Tutors:
            self.Tutors=''
        return
    def __GenerderCounter__(self):
        self.NumberOfBoys=0
        self.NumberOfGirls=0
        self.HouseNumbers={}
        for pupils in self.groupList:
            if 'F' in pupils.Pupil.Gender:
                self.NumberOfGirls+=1
                if pupils.AcademicHouse() in self.HouseNumbers:
                    self.HouseNumbers[pupils.AcademicHouse()]+=1
                else:
                    self.HouseNumbers[pupils.AcademicHouse()]=0
            else:
                self.NumberOfBoys+=1
                if pupils.AcademicHouse() in self.HouseNumbers:
                    self.HouseNumbers[pupils.AcademicHouse()]+=1
                else:
                    self.HouseNumbers[pupils.AcademicHouse()]=0
        self.NumberOfPupils=self.NumberOfBoys+ self.NumberOfGirls
        return
    def SchoolName(self):
         return GeneralFunctions.GetSchoolName(self.school)
    def Name(self):
        return self.transferRule.NewGroup.MenuName
    def RollOver(self):
        self.getGroupList()
        newgroup=Group.objects.get(Name=self.NewGroup)
        for pupils in self.groupList:
            newPupilGroup=PupilGroup(Pupil=pupils.Pupil,Group=newgroup,AcademicYear=self.NewAcYear)
            newPupilGroup.save()


class PotentialClassList():
    def __init__(self,PostData,GroupName,school):
        self.GroupName=GroupName
        self.PostData=PostData
        self.SchoolName=GeneralFunctions.GetSchoolName(school)
        self.__setClassListDetails__()
        self.__setClassDetails__()
        self.__setClassList__()
        return
    def __setClassDetails__(self):
        groupsplit=self.GroupName.split('.')
        self.Name=groupsplit[-1]
        self.Year=groupsplit[-3]
        if 'Year' in self.Year:
            self.Year=self.Year.split('Year')[1]
        self.Teachers=self.PostData['FormTeachersAll']
        self.FormTeacher=self.PostData['FormTeacher']
        self.Tutors=self.PostData['Tutors']
        self.NumberOfPupils=self.PostData['NumberOfPupils']
        self.NumberOfGirls=self.PostData['NumberOfGirls']
        self.NumberOfBoys=self.PostData['NumberOfBoys']
        self.HouseNumbers=eval(self.PostData['HouseNumbers'])
        self.NewAcYear=self.PostData['NewAcYear']
        return
    def __setClassListDetails__(self):
        self.ClassListFile="ClassLists/%s.html" % self.PostData['ClassList']
        self.ClassListName="%s List" % self.PostData['ClassList']
        return
    def __setClassList__(self):
        self.ClassList=[]
        for Pupil_Id in self.PostData.getlist('PupilSelect'):
            self.ClassList.append(PupilRecord(Pupil_Id,self.NewAcYear))
        self.ClassList=sorted(self.ClassList, key=lambda pupil: pupil.Pupil.Surname.lower())
        return

class PotentialClassLists():
    def __init__(self,school,AcYear,NewAcYear,PostData):
        self.GroupList={}
        self.FormList=GeneralFunctions.GetForms(school)
        self.PostData=PostData
        self.school=school
        self.AcYear=AcYear
        self.NewAcYear=NewAcYear
        self.__generate__()
        self.ClassListFile="ClassLists/%s.html" % self.PostData['ClassList']

    def __generate__(self):
        for forms in self.FormList:
            if forms.MenuName in self.PostData:
                self.GroupList[forms.MenuName]=PotentialGroup(self.school,forms.Name,self.AcYear,self.NewAcYear)
    def getGroupList(self):
        return self.GroupList


