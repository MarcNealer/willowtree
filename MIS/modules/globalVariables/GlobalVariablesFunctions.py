from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache

# logging
import logging
log = logging.getLogger(__name__)

class GlobalVariables():
    ''' This class is a controller for accessing WillowTree Global Variables

    The global variables are used to hold all sorts of adhoc information, such as
    Dfes number LEA numbers, Set and Year averages for reports etc.

    They come in two forms, System and Yearly. System variables can only occur once
    in the system, where yearly will create a new variable for each academic year.

    Class usage x=GlobalVariables('System'/'Yearly)
    '''
    def __init__(self,xVarType,AcYear=None):
        self.VarType=xVarType
        self.AcYear='System'
        if not self.VarType=='System':
            if not AcYear:
                self.AcYear=GlobalVariables.Get_SystemVar('CurrentYear')
            else:
                self.AcYear=AcYear
        self.VariableList=SystemVarData.objects.filter(Variable__VariableType=xVarType,AcademicYear=self.AcYear)
    def GetVariableListByName(self,xVarName):
        ''' Returns a list of all variables of the selected type in a query set'''
        return VariableList.filter(Variable__icontains=xVarName)
    def GetVariableList(self):
        ''' returns a list of variables by the type set on the class'''
        return self.VariableList
    def __CreateEmptyValues__(self):
        VarList=SystemVariable.objects.filter(VariableType=self.VarType)
        for Vars in VarList:
            if not SystemVarData.objects.filter(Variable=Vars,AcademicYear=self.AcYear).exists():
                newVar=SystemVarData(Variable=Vars,AcademicYear=self.AcYear,Data='')
                newVar.save()
    def exists_SystemVar(self,VariableName):
        if SystemVariable.objects.filter(Name=VariableName).exists():
            return True
        else:
            return False
    def safeWrite_SystemVar(self,VariableName,DataType,dataitem):
        if not self.exists_SystemVar(VariableName):
            newVar=SystemVariable(Name=VariableName,VariableType=self.VarType,DataType=DataType)
            newVar.save()
        self.Write_SystemVar(VariableName,dataitem,self.AcYear)
        return
    def create_SystemVar(self,VariableName,xDataType):
        if not self.exists_SystemVar(VariableName):
            newVar=SystemVariable(Name=VariableName,VariableType=self.VarType,DataType=xDataType)
            newVar.save()
            newVardata=SystemVarData(Variable=newVar,AcademicYear=self.AcYear,Data='')
            newVardata.save()
            return True
        else:
            return False


    @staticmethod
    def Get_SystemVar(VariableName,AcYear=None):
        ''' Static method to get a system variable. If the said variable is a yearly
        one, it will assume the current Academic Year unless AcYear is passed as a parm.
        This method will read the variable data, which is stored as a string, but will
        return the variable in the defined variable format, such as date,boooleab, string, float etc

        Numerical data is always returned as a float
        Date data is returned as a python date object
        '''
        if SystemVariable.objects.filter(Name=VariableName).exists():
            VarDetails=SystemVariable.objects.get(Name=VariableName)
            if 'Yearly' in VarDetails.VariableType:
                if not AcYear:
                    AcYear=GlobalVariables.Get_SystemVar('CurrentYear')
            else:
                if not AcYear:
                    AcYear='System'

            if SystemVarData.objects.filter(Variable=VarDetails,AcademicYear=AcYear).exists():
                Sysdata=SystemVarData.objects.get(Variable=VarDetails,AcademicYear=AcYear)
                if len(Sysdata.Data)==0:
                    return False
                if VarDetails.DataType=='Numerical':
                    return float(Sysdata.Data)
                elif VarDetails.DataType=='Date':
                    return datetime.datetime.strptime(Sysdata.Data,'%Y-%m-%d')
                elif VarDetails.DataType=='Boolean':
                    return eval(Sysdata.Data)
                else:
                    return Sysdata.Data
            else:
                return False
        else:
            return False

    @staticmethod
    def Write_SystemVar(VariableName, VariableData, AcYear=None, request=None):
        ''' This is a static method used to write out a stated global variable.
        If the variable is a Yearly one, it will use the current Academic year,
        unless AcYear is passed.

        Variable data need to be passed in the format of the defined variable. This
        function will return an error if the data is not passed in the correct formt
        Once passed, it will convert the data into a string to store in the table.
        '''
        VarDetails=SystemVariable.objects.get(Name=VariableName)
        if 'Yearly' in VarDetails.VariableType:
            if not AcYear:
                AcYear=GlobalVariables.Get_SystemVar('CurrentYear')
        else:
            AcYear='System'
        if SystemVarData.objects.filter(Variable=VarDetails,AcademicYear=AcYear).exists():
            Sysdata=SystemVarData.objects.get(Variable=VarDetails,AcademicYear=AcYear)
            Sysdata.Data=str(VariableData)
        else:
            if VarDetails.DataType=='Date':
                try:
                    Sysdata=SystemVarData(Variable=VarDetails,AcademicYear=AcYear,Data=VariableData.strftime('%Y-%m-%d'))
                except:
                    Sysdata=SystemVarData(Variable=VarDetails,AcademicYear=AcYear,Data=str(VariableData))
            else:
                Sysdata=SystemVarData(Variable=VarDetails,AcademicYear=AcYear,Data=str(VariableData))
        Sysdata.save()
        if request:
            log.warn('Yearly or System Variable with id:%s  has been changed by %s' % (VarDetails.id,
                                                                                      request.user.username))
        else:
            log.warn('Yearly or System Variable with id:%s  has been changed' % (VarDetails.id))            
        return

class TeacherNames(object):
    def __init__(self,GroupName,AcYear):
        self.GroupName=GroupName
        self.AcYear=AcYear
        self.VariableObj=GlobalVariables('Yearly',self.AcYear)
    def __Check__(self,Type):
        varname='%s.%s' % (self.GroupName,Type)
        self.VariableObj.create_SystemVar(varname,'String')
        return varname
    def FormTeacher(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('FormTeacher'),self.AcYear)
    def SetFormTeacher(self,TeacherName):
        return GlobalVariables.Write_SystemVar(self.__Check__('FormTeacher'),TeacherName,AcYear=self.AcYear)
    def AllTeachers(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('FormTeachersAll'),self.AcYear)
    def SetAllTeachers(self,TeacherNames):
        return GlobalVariables.Write_SystemVar(self.__Check__('FormTeachersAll'),TeacherNames,self.AcYear)
    def Tutor(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Tutor'),self.AcYear)
    def SetTutor(self,TutorName):
        return GlobalVariables.Write_SystemVar(self.__Check__('Tutor'),TutorName,self.AcYear)
    def SubjectTeachers(self):
        teacherlist=GlobalVariables.Get_SystemVar(self.__Check__('SubjectTeachers'),self.AcYear)
        if teacherlist:
            return eval(GlobalVariables.Get_SystemVar(self.__Check__('SubjectTeachers'),self.AcYear))
        else:
            return {}
    def SetSubjectTeacher(self,SubjectName,TeacherName):
        data=self.SubjectTeachers()
        data[SubjectName]=TeacherName
        if not 'None' in SubjectName:
            return GlobalVariables.Write_SystemVar(self.__Check__('SubjectTeachers'),str(data),self.AcYear)
        else:
            return
    def SetTeachers(self,request):
        if 'FormTeacher' in request.POST:
            self.SetFormTeacher(request.POST['FormTeacher'])
        elif 'AllTeachers' in request.POST:
            self.SetAllTeachers(request.POST['AllTeachers'])
        elif 'Tutor' in request.POST:
            self.SetTutor(request.POST['Tutor'])
        elif 'SubjectTeacher' in request.POST:
            self.SetSubjectTeacher(request.POST['Subject'],request.POST['SubjectTeacher'])
        return
class YearDates():
    def __init__(self,AcYear):
        self.AcYear=AcYear
        self.VariableObj=GlobalVariables('Yearly',self.AcYear)
        self.NotschooldayCode=AttendanceNotTakenCode.objects.get(Code='#')
    def __Check__(self,xvar):
        self.VariableObj.create_SystemVar(xvar,'Date')
        return xvar
    def MichStart(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Michaelmas.Start'),self.AcYear)
    def setMichStart(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Michaelmas.Start'),xvar,self.AcYear)      
    def MichHalfStart(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Michaelmas.HalfTerm.Start'),self.AcYear)
    def setMichHalfStart(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Michaelmas.HalfTerm.Start'),xvar,self.AcYear)      
    def MichHalfStop(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Michaelmas.HalfTerm.Stop'),self.AcYear)
    def setMichHalfStop(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Michaelmas.HalfTerm.Stop'),xvar,self.AcYear)      
    def MichStop(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Michaelmas.Stop'),self.AcYear)
    def setMichStop(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Michaelmas.Stop'),xvar,self.AcYear)      
    def LentStart(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Lent.Start'),self.AcYear)
    def setLentStart(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Lent.Start'),xvar,self.AcYear)      
    def LentHalfStart(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Lent.HalfTerm.Start'),self.AcYear)
    def setLentHalfStart(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Lent.HalfTerm.Start'),xvar,self.AcYear)      
    def LentHalfStop(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Lent.HalfTerm.Stop'),self.AcYear)
    def setLentHalfStop(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Lent.HalfTerm.Stop'),xvar,self.AcYear)      
    def LentStop(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Lent.Stop'),self.AcYear)
    def setLentStop(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Lent.Stop'),xvar,self.AcYear)                                    
    def SummerStart(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Summer.Start'),self.AcYear)
    def setSummerStart(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Summer.Start'),xvar,self.AcYear)      
    def SummerHalfStart(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Summer.HalfTerm.Start'),self.AcYear)
    def setSummerHalfStart(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Summer.HalfTerm.Start'),xvar,self.AcYear)      
    def SummerHalfStop(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Summer.HalfTerm.Stop'),self.AcYear)
    def setSummerHalfStop(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Summer.HalfTerm.Stop'),xvar,self.AcYear)      
    def SummerStop(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Summer.Stop'),self.AcYear)
    def setSummerStop(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Summer.Stop'),xvar,self.AcYear)
    def Holiday1(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Holiday.Adhoc1'),self.AcYear)
    def setHoliday1(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Holiday.Adhoc1'),xvar,self.AcYear)
    def Holiday2(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Holiday.Adhoc2'),self.AcYear)
    def setHoliday2(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Holiday.Adhoc2'),xvar,self.AcYear)
    def Holiday3(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Holiday.Adhoc3'),self.AcYear)
    def setHoliday3(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Holiday.Adhoc3'),xvar,self.AcYear)
    def Holiday4(self):
        return GlobalVariables.Get_SystemVar(self.__Check__('Holiday.Adhoc4'),self.AcYear)
    def setHoliday4(self,xvar):
        return GlobalVariables.Write_SystemVar(self.__Check__('Holiday.Adhoc4'),xvar,self.AcYear)   

    def ModifyDates(self,Term,Postdata):
        if 'Mich' in Term:
            self.__UpdateMich__(Postdata)
        elif 'Lent' in Term:
            self.__UpdateLent__(Postdata)
        elif 'Summer' in Term:
            self.__UpdateSummer__(Postdata)
        elif 'Adhoc' in Term:
            self.__UpdateAdhoc__(Postdata)
    def __UpdateMich__(self,Postdata):
        if 'HalfStart' in Postdata:
            self.setMichHalfStart(Postdata['HalfStart'])
        if 'HalfStop' in Postdata:
            self.setMichHalfStop(Postdata['HalfStop'])
        if 'Start' in Postdata:
            self.setMichStart(Postdata['Start'])
        if 'Stop' in Postdata:
            self.setMichStop(Postdata['Stop'])
        self.__UpdateNotSchoolDays__()
    def __UpdateLent__(self,Postdata):
        if 'HalfStart' in Postdata:
            self.setLentHalfStart(Postdata['HalfStart'])
        if 'HalfStop' in Postdata:
            self.setLentHalfStop(Postdata['HalfStop'])
        if 'Start' in Postdata:
            self.setLentStart(Postdata['Start'])
        if 'Stop' in Postdata:
            self.setLentStop(Postdata['Stop'])
        self.__UpdateNotSchoolDays__()
    def __UpdateSummer__(self,Postdata):
        if 'HalfStart' in Postdata:
            self.setSummerHalfStart(Postdata['HalfStart'])
        if 'HalfStop' in Postdata:
            self.setSummerHalfStop(Postdata['HalfStop'])
        if 'Start' in Postdata:
            self.setSummerStart(Postdata['Start'])
        if 'Stop' in Postdata:
            self.setSummerStop(Postdata['Stop'])
        self.__UpdateNotSchoolDays__()
    def __UpdateAdhoc__(self,Postdata):
        if 'one' in Postdata:
            self.setHoliday1(Postdata['one'])
        if 'two' in Postdata:
            self.setHoliday2(Postdata['two'])
        if 'three' in Postdata:
            self.setHoliday3(Postdata['three'])
        if 'four' in Postdata:
            self.setHoliday4(Postdata['four'])
        self.__UpdateNotSchoolDays__()
    def __SaveNotSchoolDays__(self,date1,date2):

        allschool=Group.objects.get(Name='Current')
        for holidays in [date1+datetime.timedelta(n) for n in range(1,(date2-date1).days)]:
            newrec=AttendanceNotTaken(Code=self.NotschooldayCode,Date=holidays,
                                      Type='AllDay',Group=allschool,
                                      AcademicYear=self.AcYear)
            newrec.save()
    def __UpdateNotSchoolDays__(self):
        NotSchooldays=AttendanceNotTaken.objects.filter(AcademicYear=self.AcYear,
                                                        Code=self.NotschooldayCode)
        NotSchooldays.delete()
        if self.MichHalfStart() and self.MichHalfStop():
            self.__SaveNotSchoolDays__(self.MichHalfStart()-datetime.timedelta(1),self.MichHalfStop()+datetime.timedelta(1))
        if self.MichStop() and self.LentStart():
            self.__SaveNotSchoolDays__(self.MichStop(),self.LentStart())
        if self.LentHalfStart() and self.LentHalfStop():
            self.__SaveNotSchoolDays__(self.LentHalfStart()-datetime.timedelta(1),self.LentHalfStop()+datetime.timedelta(1))
        if self.LentStop() and self.SummerStart():
            self.__SaveNotSchoolDays__(self.LentStop(),self.SummerStart())
        if self.SummerHalfStart() and self.SummerHalfStop():
            self.__SaveNotSchoolDays__(self.SummerHalfStart()-datetime.timedelta(1),self.SummerHalfStop()+datetime.timedelta(1))
        if self.Holiday1():
            self.__SaveNotSchoolDays__(self.Holiday1()-datetime.timedelta(1),self.Holiday1()+datetime.timedelta(1))
        if self.Holiday2():
            self.__SaveNotSchoolDays__(self.Holiday2()-datetime.timedelta(1),self.Holiday2()+datetime.timedelta(1))            
        if self.Holiday3():
            self.__SaveNotSchoolDays__(self.Holiday3()-datetime.timedelta(1),self.Holiday3()+datetime.timedelta(1))       
        if self.Holiday4():
            self.__SaveNotSchoolDays__(self.Holiday4()-datetime.timedelta(1),self.Holiday4()+datetime.timedelta(1))         