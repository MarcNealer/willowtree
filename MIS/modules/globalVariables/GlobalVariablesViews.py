# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *

# WillowTree system imports
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.models import *
from MIS.modules.globalVariables import GlobalVariablesFunctions

def GlobalVariablesManager(request,school,AcYear,variableType):
    c = {'AcYear':AcYear,'school':school}
    if variableType == 'system':
        c.update({'GlobalVariables':GlobalVariablesFunctions.GlobalVariables('System').GetVariableList(),
                  'SystemVariableTrue':True})
    if variableType == 'yearly':
        c.update({'GlobalVariables':GlobalVariablesFunctions.GlobalVariables('Yearly',AcYear).GetVariableList()})
    return render_to_response('GlobalVariableManager.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def EditGlobalVariablesManager(request,school,AcYear):
    GlobalVariablesFunctions.GlobalVariables.Write_SystemVar(request.POST['Name'],
                                                             request.POST['Data'],
                                                             request.POST['AcademicYear'],
                                                             request)
    if request.POST['AcademicYear'] == unicode('System'):
        return HttpResponseRedirect('/WillowTree/%s/%s/EditSystemVariable/' % (school,AcYear))
    else:
        return HttpResponseRedirect('/WillowTree/%s/%s/EditYearlyVariable/' % (school,AcYear))
        
def ManageYearDates(request,school,AcYear):
    c={'YearDates':GlobalVariablesFunctions.YearDates(AcYear)}
    return render_to_response('YearlyDatesManager.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def ModifyYearDates(request,school,AcYear,Term):
    yeardates=GlobalVariablesFunctions.YearDates(AcYear)
    yeardates.ModifyDates(Term,request.POST)
    c={'YearDates':yeardates}
    return render_to_response('YearlyDatesManager.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
      