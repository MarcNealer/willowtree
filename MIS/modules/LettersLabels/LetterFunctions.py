from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from MIS.modules.PupilRecs import *
from MIS.modules.FamilyRecord import *
from MIS.modules.StaffRecs import StaffLetterRecord
from MIS.modules.Schools.SchoolsFunctions import SchoolLetterRecord
from MIS.modules import GeneralFunctions

class TemplateRecord():
    ''' This class gets a list of Base Letter templates. Its used to feed into
    Views for letter creation'''
    def __init__(self):
        self.AllTemplates=LetterTemplate.objects.filter(Active=True)

    def PupilTemplates(self):
        ''' Returns a list of letter templates who's data base is a pupil record '''
        return self.AllTemplates.filter(LetterBaseType='Pupil')

    def FamilyTemplates(self):
        ''' Returns a list of letter templates who's data base is a Family Record '''
        return self.AllTemplates.filter(LetterBaseType='Family')

    def StaffTemplates(self):
        ''' Returns a list of letter templates who's data base is a Staff Record '''
        return self.AllTemplates.filter(LetterBaseType='Staff')

    def SchoolTemplates(self):
        ''' Returns a list of letter templates who's data base is a School Record '''
        return self.AllTemplates.filter(LetterBaseType='School')


def letterVariableHelpData():
    return {'pupilLetterVariableHelp': ['Example of a date adhoc variable............ {{ Var.Date1|date:\'l jS F Y\' }}',
                                        'Example of an adhoc variable................ {{ Var.Adhoc1 }}',
                                        'Example of a pupil forname ................. {{ Pupil.Pupil.Forename }}',
                                        'Example of a pupil surname ................. {{ Pupil.Pupil.Surname }}',
                                        'Example of a pupil he/she - capitalised .... {{ Pupil.Pupil.HeShe }}',
                                        'Example of a pupil\'s his/her - uncapitalised . {{ Pupil.Pupil.hisher }}',
                                        'Example of a pupil\'s date of birth............ {{ Pupil.Pupil.DateOfBirth|date:\'l jS F Y\' }}',
                                        'Example of a pupil\'s academic house .......... {{ Pupil.AcademicHouse }}',
                                        'Example of a pupil\'s form .................... {{ Pupil.Form }}'],
            'pupilLetterVariableExample': '{{ Pupil.Forename }} was born on the {{ Pupil.DateOfBirth|date:\'l jS F Y\' }} and is in {{ Pupil.Form }} form.',
            'familyLetterVariableHelp': ['Example of a date adhoc variable........... {{ Var.Date1|date:\'l jS F Y\' }}',
                                         'Example of an adhoc variable............... {{ Var.Adhoc1 }}',
                                         'Example of forenames ...................... {{ Family.Forenames }}',
                                         'Example of child/children.................. {{ Family.ChildChildren }}',
                                         'Example of he/she/they - uncapitalised..... {{ Family.heshethey }}',
                                         'Example of he/she/they - capitalised....... {{ Family.HeSheThey }}',
                                         'Example of his/her/their - uncapitalised... {{ Family.hishertheir }}'],
            'pupilLetterVariableExample': 'Your children {{ Family.Forenames }} have been accepted to join the school.',
            'staffLetterVariableHelp': ['Example of a date adhoc variable............ {{ Var.Date1|date:\'l jS F Y\' }}',
                                        'Example of an adhoc variable................ {{ Var.Adhoc1 }}',
                                        'Example of staff title...................... {{ Staff.Title }}',
                                        'Example of staff forename................... {{ Staff.Forename }}',
                                        'Example of staff surename................... {{ Staff.Surname }}'],
            'staffLetterVariableExample': '{{ Staff.Title }} {{ Staff.Surname }} is your first name {{ Staff.Forename }}?',
            'schoolLetterVariableHelp': ['Example of a date adhoc variable............ {{ Var.Date1|date:\'l jS F Y\' }}',
                                         'Example of an adhoc variable................ {{ Var.Adhoc1 }}',
                                         'Example of school name...................... {{ School.Name }}'],
            'schoolLetterVariableExample': 'The name of your school is {{ School.Name }}'}


def SaveLetter(request,school,AcYear):
    ''' Function used to save a new letter. Used to proccess the form request from
    the letter creation pages

    Usage SaveLetter(request,'BatterseaAdmin','2012-2013')

    Return Data: Letter Object
    '''
    filterStr = "{% load WillowFilters %}"
    if filterStr not in request.POST['LetterBody']:
        bodyText = filterStr + request.POST['LetterBody'].replace('\"','\'')
    else:
        bodyText = request.POST['LetterBody'].replace('\"','\'')
    description = request.POST['Description']
    description = description.replace("\n", "")
    description = description.replace("\r\r", "\r")
    description = description.replace("\r\r\r", "\r")

    # letters for schools are not linked to a group within WillowTree.
    try:
        group = Group.objects.get(Name=request.POST['Selectgroup'])
    except:
        group = None
    NewLetter=LetterBody(AttachedTo=group,
                         Name=request.POST['LetterName'],
                         BodyText=bodyText,
                         SignatureName=request.POST['SignatureName'],
                         SignatureTitle=request.POST['SignatureTitle'],
                         LetterTemplate=LetterTemplate.objects.get(Name=request.POST['TemplateList']),
                         ObjectBaseType=request.POST['BaseType'],
                         CreatedBy=request.session['StaffId'],
                         ModifiedBY=request.session['StaffId'],
                         Description=description)
    NewLetter.save()
    if 'StaffGroup' in request.POST:
        if request.POST['StaffGroup'].find('None') < 0:
            NewLetter.ExtraStaffAccess=Group.objects.get(id=int(request.POST['StaffGroup']))
            NewLetter.save()
    return NewLetter

def GetlistOfLetters(GroupName):
    ''' This function returns a list of letters assigned to a group or its parents '''
    Grouplength=len(GroupName.split('.'))
    GroupList=[]
    GroupList.append(GroupName)
    for count in range(0,Grouplength):
        GroupList.append('.'.join(GroupName.split('.')[0:count]))
    return LetterBody.objects.filter(AttachedTo__Name__in=GroupList,Active=True)


def CreateFromCopy(request,school,AcYear):
    ''' This function creates a copy of a letter based upon another letter'''
    pass

def Inactivate(LetterId):
    ''' This function inactivates a letter '''
    LetterDetails=LetterBody.objects.get(id=int(LetterId))
    LetterDetails.Active=False
    LetterDetails.save()
    return


class GenerateFamilyLetter():
    ''' This Class is used to generate the letters that have family records as their base
    It interacts with the Family records to generate the letter.

    Usage x=GenerateFamilyLetter(request, LetterRecord, MenuName, AcademicYear)

    There are no direct methods except the init for this class. Is used more to
    store the different data objects needed for letter creation under a single
    name space that can be passed to the letter HTML template.

    The list of families are found by using the list of pupils passed from a HTML
    POST request form in the request object
    '''
    def __init__(self,request,Letter_Id,school,AcYear):
        self.PupilList=[]
        for PupilIds in request.POST.getlist('PupilSelect'):
            self.PupilList.append(Pupil.objects.get(id=int(PupilIds)))

        self.FamilyList=GeneralFunctions.FamiliesInAGroup(request,AcYear,PupilList=self.PupilList)
        self.VarList={}
        self.OtherItems={}
        self.OtherItems['SchoolName']=MenuTypes.objects.get(Name=school).SchoolId.Name
        self.OtherItems['CurrentYear']=datetime.datetime.now().year
        self.OtherItems['NextYear']=self.OtherItems['CurrentYear']+1
        self.LetterDate=datetime.datetime.strptime(request.POST['LetterDate'],'%Y-%m-%d')
        if 'Date1' in request.POST:
            self.VarList['Date1']=datetime.datetime.strptime(request.POST['Date1'],'%Y-%m-%d')
        if 'Date2' in request.POST:
            self.VarList['Date2']=datetime.datetime.strptime(request.POST['Date2'],'%Y-%m-%d')
        if 'Date3' in request.POST:
            self.VarList['Date3']=datetime.datetime.strptime(request.POST['Date3'],'%Y-%m-%d')
        if 'Adhoc1' in request.POST:
            self.VarList['Adhoc1']=request.POST['Adhoc1']
        if 'Adhoc2' in request.POST:
            self.VarList['Adhoc2']=request.POST['Adhoc2']
        if 'Adhoc3' in request.POST:
            self.VarList['Adhoc3']=request.POST['Adhoc3']
        if 'Adhoc4' in request.POST:
            self.VarList['Adhoc4']=request.POST['Adhoc4']
        if 'Adhoc5' in request.POST:
            self.VarList['Adhoc5']=request.POST['Adhoc5']
        if 'Adhoc6' in request.POST:
            self.VarList['Adhoc6']=request.POST['Adhoc6']
        self.LetterDetails=LetterBody.objects.get(id=int(Letter_Id))
        for FamilyDetails in self.FamilyList:
            FamilyDetails.GenerateLetterBody(self.LetterDetails,self.VarList,self.OtherItems)
        self.SchoolDetails=MenuTypes.objects.get(Name=school).SchoolId

class GeneratePupilLetter():
    ''' This Class is used to generate the letters that have Pupil records as their base
    It interacts with the Family records to generate the letter.

    Usage x=GenerateFamilyLetter(request, LetterRecord, MenuName, AcademicYear)

    There are no direct methods except the init for this class. Is used more to
    store the different data objects needed for letter creation under a single
    name space that can be passed to the letter HTML template

    The List of pupils requireing letters is found via the pupilselect object in
    request.POST. Thus you have to pass the list of pupils from the HTML form.
    '''
    def __init__(self,request,Letter_Id,school,AcYear):
        self.FamilyList=[]
        self.VarList={}
        self.OtherItems={}
        self.OtherItems['SchoolName']=MenuTypes.objects.get(Name=school).SchoolId.Name
        self.OtherItems['CurrentYear']=datetime.datetime.now().year
        self.OtherItems['NextYear']=self.OtherItems['CurrentYear']+1
        self.LetterDate=datetime.datetime.strptime(request.POST['LetterDate'],'%Y-%m-%d')
        if 'Date1' in request.POST:
            self.VarList['Date1']=datetime.datetime.strptime(request.POST['Date1'],'%Y-%m-%d')
        if 'Date2' in request.POST:
            self.VarList['Date2']=datetime.datetime.strptime(request.POST['Date2'],'%Y-%m-%d')
        if 'Date3' in request.POST:
            self.VarList['Date3']=datetime.datetime.strptime(request.POST['Date3'],'%Y-%m-%d')
        if 'Adhoc1' in request.POST:
            self.VarList['Adhoc1']=request.POST['Adhoc1']
        if 'Adhoc2' in request.POST:
            self.VarList['Adhoc2']=request.POST['Adhoc2']
        if 'Adhoc3' in request.POST:
            self.VarList['Adhoc3']=request.POST['Adhoc3']
        if 'Adhoc4' in request.POST:
            self.VarList['Adhoc4']=request.POST['Adhoc4']
        if 'Adhoc5' in request.POST:
            self.VarList['Adhoc5']=request.POST['Adhoc5']
        if 'Adhoc6' in request.POST:
            self.VarList['Adhoc6']=request.POST['Adhoc6']
        self.LetterDetails=LetterBody.objects.get(id=int(Letter_Id))
        for PupilNo in request.POST.getlist('PupilSelect'):
            Rec=FamilyRecord(request,AcYear,PupilId=PupilNo)
            Rec.GenerateLetterBody(self.LetterDetails,self.VarList,self.OtherItems,LetterType='Pupil')
            self.FamilyList.append(Rec)
        self.SchoolDetails=MenuTypes.objects.get(Name=school).SchoolId


class GenerateStaffLetter():
    """
    Similar to the GeneratePupilLetter class above only for staff letters
    """
    def __init__(self, request, Letter_Id, school, AcYear):
        self.StaffList = list()
        self.VarList = dict()
        self.OtherItems = dict()
        self.OtherItems['SchoolName'] = MenuTypes.objects.get(Name=school).SchoolId.Name
        self.OtherItems['CurrentYear'] = datetime.datetime.now().year
        self.OtherItems['NextYear'] = self.OtherItems['CurrentYear'] + 1
        self.LetterDate = datetime.datetime.strptime(request.POST['LetterDate'], '%Y-%m-%d')
        if 'Date1' in request.POST:
            self.VarList['Date1'] = datetime.datetime.strptime(request.POST['Date1'], '%Y-%m-%d')
        if 'Date2' in request.POST:
            self.VarList['Date2'] = datetime.datetime.strptime(request.POST['Date2'], '%Y-%m-%d')
        if 'Date3' in request.POST:
            self.VarList['Date3'] = datetime.datetime.strptime(request.POST['Date3'], '%Y-%m-%d')
        if 'Adhoc1' in request.POST:
            self.VarList['Adhoc1'] = request.POST['Adhoc1']
        if 'Adhoc2' in request.POST:
            self.VarList['Adhoc2'] = request.POST['Adhoc2']
        if 'Adhoc3' in request.POST:
            self.VarList['Adhoc3'] = request.POST['Adhoc3']
        if 'Adhoc4' in request.POST:
            self.VarList['Adhoc4'] = request.POST['Adhoc4']
        if 'Adhoc5' in request.POST:
            self.VarList['Adhoc5'] = request.POST['Adhoc5']
        if 'Adhoc6' in request.POST:
            self.VarList['Adhoc6'] = request.POST['Adhoc6']
        self.LetterDetails = LetterBody.objects.get(id=int(Letter_Id))
        for staff_id in request.POST.getlist('PupilSelect'):
            Rec = StaffLetterRecord(staff_id, request, AcYear)
            Rec.generate_letter_body(self.LetterDetails, self.VarList, self.OtherItems)
            self.StaffList.append(Rec)
        self.SchoolDetails = MenuTypes.objects.get(Name=school).SchoolId


class GenerateSchoolLetter():
    """
    Similar to the GeneratePupilLetter class above only for staff letters
    """
    def __init__(self, request, Letter_Id, school, AcYear):
        self.school_list = list()
        self.VarList = dict()
        self.OtherItems = dict()
        self.OtherItems['SchoolName'] = MenuTypes.objects.get(Name=school).SchoolId.Name
        self.OtherItems['CurrentYear'] = datetime.datetime.now().year
        self.OtherItems['NextYear'] = self.OtherItems['CurrentYear'] + 1
        self.LetterDate = datetime.datetime.strptime(request.POST['LetterDate'], '%Y-%m-%d')
        if 'Date1' in request.POST:
            self.VarList['Date1'] = datetime.datetime.strptime(request.POST['Date1'], '%Y-%m-%d')
        if 'Date2' in request.POST:
            self.VarList['Date2'] = datetime.datetime.strptime(request.POST['Date2'], '%Y-%m-%d')
        if 'Date3' in request.POST:
            self.VarList['Date3'] = datetime.datetime.strptime(request.POST['Date3'], '%Y-%m-%d')
        if 'Adhoc1' in request.POST:
            self.VarList['Adhoc1'] = request.POST['Adhoc1']
        if 'Adhoc2' in request.POST:
            self.VarList['Adhoc2'] = request.POST['Adhoc2']
        if 'Adhoc3' in request.POST:
            self.VarList['Adhoc3'] = request.POST['Adhoc3']
        if 'Adhoc4' in request.POST:
            self.VarList['Adhoc4'] = request.POST['Adhoc4']
        if 'Adhoc5' in request.POST:
            self.VarList['Adhoc5'] = request.POST['Adhoc5']
        if 'Adhoc6' in request.POST:
            self.VarList['Adhoc6'] = request.POST['Adhoc6']
        self.LetterDetails = LetterBody.objects.get(id=int(Letter_Id))
        for school_id in request.POST.getlist('PupilSelect'):
            Rec = SchoolLetterRecord(school_id, request, AcYear)
            Rec.generate_letter_body(self.LetterDetails, self.VarList, self.OtherItems)
            self.school_list.append(Rec)
        self.SchoolDetails = MenuTypes.objects.get(Name=school).SchoolId


class GenerateLabels():
    ''' This class is used to create a series of objects that are passed to the
    required label template'''
    def __init__(self,request,school,AcYear):
        self.PupilList=[]
        for PupilIds in request.POST.getlist('PupilSelect'):
            self.PupilList.append(Pupil.objects.get(id=int(PupilIds)))
        self.FamilyList=[]
        if request.POST['LabelType']=='AddressFamily':
            self.FamilyList=GeneralFunctions.FamiliesInAGroup(request,AcYear,PupilList=self.PupilList)
        else:
            for PupilNo in self.PupilList:
                self.FamilyList.append(FamilyRecord(request,AcYear,PupilId=PupilNo.id))
    def OverrideForm(self,NewForm):
        for eachFamily in self.FamilyList:
            eachFamily.PupilDetails.Form=Group.objects.get(Name=NewForm).MenuName

class LetterManager():
    @staticmethod
    def SaveLetter(request,school,AcYear):
        bodyText = request.POST['LetterBody'].replace('\"','\'')
        description = request.POST['Description']
        description = description.replace("\r\r", "\r")
        description = description.replace("\r\r\r", "\r")
        NewLetter=LetterBody(AttachedTo=Group.objects.get(Name=request.POST['Selectgroup']),
                             Name=request.POST['LetterName'],
                             BodyText=bodyText,
                             SignatureName=request.POST['SignatureName'],
                             SignatureTitle=request.POST['SignatureTitle'],
                             LetterTemplate=LetterTemplate.objects.get(Name=request.POST['TemplateList']),
                             ObjectBaseType=request.POST['BaseType'],
                             CreatedBy=request.session['StaffId'],
                             ModifiedBY=request.session['StaffId'],
                             Description=description)
        NewLetter.save()
        if 'StaffGroup' in request.POST:
            if request.POST['StaffGroup'].find('None') < 0:
                NewLetter.ExtraStaffAccess=Group.objects.get(id=int(request.POST['StaffGroup']))
                NewLetter.save()
        return NewLetter
    @staticmethod
    def GetlistOfLetters(GroupName):
        Grouplength=len(GroupName.split('.'))
        GroupList=[]
        GroupList.append(GroupName)
        for count in range(0,Grouplength):
            GroupList.append('.'.join(GroupName.split('.')[0:count]))
        return LetterBody.objects.filter(AttachedTo__Name__in=GroupList,Active=True)

    @staticmethod
    def CreateFromCopy(request,school,AcYear):
        pass
    @staticmethod
    def Inactivate(LetterId):
        LetterDetails=LetterBody.objects.get(id=int(LetterId))
        LetterDetails.Active=False
        LetterDetails.save()
        return