# Django imports
from django.utils import simplejson
from django.shortcuts import HttpResponse
from django.http import HttpResponseBadRequest
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.core import serializers

# WillowTree imports
from MIS.ViewExtras import SetMenu
from MIS.models import ElectronicSignature, MenuTypes, School


def electronic_signatures_home_page(request, school, ac_year):
    """
    Electronic signatures home page.
    """
    c = {'school': school,
         'AcYear': ac_year}
    con_inst = RequestContext(request, processors=[SetMenu])
    return render_to_response('electronic_signatures_home_page.html', c,
                              context_instance=con_inst)


def view_electronic_signatures_for_a_school(request, school, ac_year):
    """
    View all electronic signatures for a school.
    """
    try:
        school_name = MenuTypes.objects.get(Name=school).SchoolId.Name
        return_data = serializers.serialize('json', ElectronicSignature.objects.filter(School__Name=school_name,
                                                                                       Active=True),
                                            indent=2,
                                            use_natural_keys=True)
        return HttpResponse(return_data, 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def add_electronic_signature_for_a_school(request, school, ac_year):
    """
    Add single electronic signature for a school.
    """
    try:
        # check name for this signature doesn't already exist
        all_existing_image_names = ElectronicSignature.objects.filter(Active=True).values_list('Name', flat=True)
        name_of_signature = request.POST['name_of_signature']
        if name_of_signature not in all_existing_image_names:
            school_name_of_signature = MenuTypes.objects.get(Name=school).SchoolId.Name
            image_for_signature = request.FILES['image_for_signature']
            new_electronic_signature = ElectronicSignature(Name=name_of_signature,
                                                           School=School.objects.get(Name=school_name_of_signature),
                                                           SignatureImage=image_for_signature)
            new_electronic_signature.save()
            return HttpResponse(simplejson.dumps({'message from server': 'signature successfully saved'}),
                                'application/json')
        else:
            error_message = {'message from server': 'ERROR: An electronic signature with this name already exists'}
            return HttpResponse(simplejson.dumps(error_message), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def remove_electronic_signature_for_a_school(request, school, ac_year, electronic_signature_id):
    """
    Remove a single electronic signature from a school.
    """
    try:
        not_needed_electronic_signature = ElectronicSignature.objects.get(id=electronic_signature_id)
        not_needed_electronic_signature.Active = False
        not_needed_electronic_signature.save()
        return HttpResponse(simplejson.dumps({'message from server': 'signature successfully removed'}),
                            'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)