# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q
from django.conf import settings
# WillowTree system imports

from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules.PupilRecs import *
from MIS.modules import GeneralFunctions
from MIS.modules.LettersLabels import LetterFunctions

def CreateLetter(request,school,AcYear):
    ''' View to produce the page to create a new letter template '''
    c={'school':school,'AcYear':AcYear,'TemplateDetails':LetterFunctions.TemplateRecord(),
       'GroupList':GeneralFunctions.SchoolGroupsList(school)}
    c.update(LetterFunctions.letterVariableHelpData())

    return render_to_response('CreateALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def SaveALetter(request,school,AcYear):
    ''' View to proccess the results of CreateLetter view '''
    c={'school':school,'AcYear':AcYear,'TemplateDetails':LetterFunctions.TemplateRecord(),'GroupList':GeneralFunctions.SchoolGroupsList(school)}
    c.update({'LetterDetails':LetterFunctions.SaveLetter(request,school,AcYear)})
    return render_to_response('ModifyALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def SelectLetterToCopy(request, AcYear, school):
    ''' View to allow users to select a letter to copy and create a new letter '''
    c={'school':school,'AcYear':AcYear,'LetterList':LetterBody.objects.filter(Active=True)}
    return render_to_response('SelectALetterToCopy.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def CopyLetter(request,AcYear,school):
    ''' View to create a page that allows users to modify a letter selected in SelectLetterToModify '''
    c={'school':school,'AcYear':AcYear}
    c.update(LetterFunctions.letterVariableHelpData())
    if not request.POST['SelectLetter']=='None':
        c.update({'LetterDetails':LetterBody.objects.get(id=int(request.POST['SelectLetter'])),'TemplateDetails':LetterFunctions.TemplateRecord(),'GroupList':GeneralFunctions.SchoolGroupsList(school)})
        c.update(LetterFunctions.letterVariableHelpData())
        return render_to_response('CopyALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    else:
        errmsg=['No Letter selected']
        c.update({'LetterList':LetterBody.objects.filter(Active=True),'return_msg':errmsg})
        return render_to_response('SelectALetterToCopy.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def SelectLetterToModify(request,AcYear,school):
    ''' View to allow users to select a letter to modify'''
    c={'school':school,'AcYear':AcYear,'LetterList':LetterBody.objects.filter(Active=True)}
    return render_to_response('SelectALetterToModify.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def ModifyLetter(request,AcYear,school):
    ''' View to create a page that allows users to modify a letter selected in SelectLetterToModify '''
    c={'school':school,'AcYear':AcYear}
    c.update(LetterFunctions.letterVariableHelpData())
    if not request.POST['SelectLetter']=='None':
        c.update({'LetterDetails':LetterBody.objects.get(id=int(request.POST['SelectLetter'])),'TemplateDetails':LetterFunctions.TemplateRecord(),'GroupList':GeneralFunctions.SchoolGroupsList(school)})
        c.update(LetterFunctions.letterVariableHelpData())
        return render_to_response('ModifyALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    else:
        errmsg=['No Letter selected']
        c.update({'LetterList':LetterBody.objects.filter(Active=True),'return_msg':errmsg})
        return render_to_response('SelectALetterToModify.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def SaveALetterModification(request,AcYear,school,LetterNo):
    ''' View to proccess changes to a letter from the ModiftyLetter view '''
    LetterFunctions.Inactivate(LetterNo)
    c={'school':school,'AcYear':AcYear,'TemplateDetails':LetterFunctions.TemplateRecord(),'GroupList':GeneralFunctions.SchoolGroupsList(school)}
    c.update({'LetterDetails':LetterFunctions.SaveLetter(request,school,AcYear),'return_msg':['Letter Saved']})
    return render_to_response('ModifyALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def SelectALetter(request,AcYear,school,GroupName):
    ''' View used to for selecting a letter to generate for a group. This view is
    an old one that was linked to the menus. Letter Generation is now part of the
    manager screen and thus this is not used'''
    c={'school':school,'AcYear':AcYear,'GroupName':GroupName}
    c.update({'LetterList':LetterFunctions.GetlistOfLetters(GroupName)})
    return render_to_response('SelectALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def GenerateALetter(request, AcYear, school, GroupName):
    """
    Used to process a generate letter request from either the menu or from the Managers
    """
    if 'SelectLetter' in request.POST:
        LetterRecord = LetterBody.objects.get(id=int(request.POST['SelectLetter']))
        if LetterRecord.ObjectBaseType == 'Family':
            c = {'Letter': LetterFunctions.GenerateFamilyLetter(request, LetterRecord.id, school, AcYear)}
        elif LetterRecord.ObjectBaseType == 'Pupil':
            c = {'Letter': LetterFunctions.GeneratePupilLetter(request, LetterRecord.id, school, AcYear)}
        elif LetterRecord.ObjectBaseType == 'Staff':
            c = {'Letter': LetterFunctions.GenerateStaffLetter(request, LetterRecord.id, school, AcYear)}
        elif LetterRecord.ObjectBaseType == 'School':
            c = {'Letter': LetterFunctions.GenerateSchoolLetter(request, LetterRecord.id, school, AcYear)}
        else:
            pass
    return render_to_response(LetterRecord.LetterTemplate.Filename, c,
                              context_instance=RequestContext(request, processors=[SetMenu]))


def GenerateLetterLabels(request,AcYear,school,PotentialGroup=False):
    '''This view is used to process a request to generate labels'''
    familylist=LetterFunctions.GenerateLabels(request,school,AcYear)

    
    LabelType=request.POST['LabelType']
    if PotentialGroup and not 'AddressFamily' in LabelType:
        familylist.OverrideForm(PotentialGroup)
    c={'Label':familylist,'MEDIA_REDIRECT':settings.MEDIA_REDIRECT}
    
    if LabelType=='AddressFamily' or LabelType=='AddressPupil':
        return render_to_response('LetterTemplates/AddressLabel_L7163.html',c)
    elif LabelType=='AddressWithPupilName':
        return render_to_response('LetterTemplates/AddressLabelPupil_L7163.html',c)
    elif LabelType=='ChildLabel1':
        LabelDetails={'Font':'Default','Colour':'Default','Size':'Default','Extra':'None'}
        if 'ChildLabel1Font' in request.POST:
            if len(request.POST['ChildLabel1Font'])>0:
                LabelDetails['Font']=request.POST['ChildLabel1Font']
        if 'ChildLabel1Size' in request.POST:
            if len(request.POST['ChildLabel1Size'])>0:
                LabelDetails['Size']=request.POST['ChildLabel1Size']
        if 'ChildLabel1Colour' in request.POST:
            if len(request.POST['ChildLabel1Colour'])>0:
                LabelDetails['Colour']=request.POST['ChildLabel1Colour']
        if 'ChildLabel1Extra' in request.POST:
            if len(request.POST['ChildLabel1Extra'])>0:
                LabelDetails['Extra']=request.POST['ChildLabel1Extra']
        if 'ChildLabel1Adhoc' in request.POST:
            if len(request.POST['ChildLabel1Adhoc']) > 0:
                LabelDetails['Adhoc']=request.POST['ChildLabel1Adhoc']
        c.update(LabelDetails)
        return render_to_response('LetterTemplates/ChildLabel1_L7163.html',c)
    elif LabelType=='ChildLabel2':
        LabelDetails={'Font':'Defailt','Colour':'Default','Size':'Default','Extra':'None'}
        if 'ChildLabel2Font' in request.POST:
            if len(request.POST['ChildLabel2Font'])>0:
                LabelDetails['Font']=request.POST['ChildLabel2Font']
        if 'ChildLabel2Size' in request.POST:
            if len(request.POST['ChildLabel2Size'])>0:
                LabelDetails['Size']=request.POST['ChildLabel2Size']
        if 'ChildLabel2Colour' in request.POST:
            if len(request.POST['ChildLabel2Colour'])>0:
                LabelDetails['Colour']=request.POST['ChildLabel2Colour']
        if 'ChildLabel2Extra' in request.POST:
            if len(request.POST['ChildLabel2Extra'])>0:
                LabelDetails['Extra']=request.POST['ChildLabel2Extra']
        if 'ChildLabel2Image' in request.POST:
            if len(request.POST['ChildLabel2Image'])>0:
                LabelDetails['Image']=request.POST['ChildLabel2Image']
        if 'ChildLabel1Adhoc' in request.POST:
            if len(request.POST['ChildLabel2Adhoc']) > 0:
                LabelDetails['Adhoc']=request.POST['ChildLabel2Adhoc']
        c.update(LabelDetails)     
        return render_to_response('LetterTemplates/ChildLabel2_L7163.html',c,context_instance=RequestContext(request))
    elif LabelType=='ParentPupilNames':
        LabelDetails={'Font':'Default','Colour':'Default','Size':'Default','Extra':'None','LabelType':'IncPupils','Titles':True}
        if 'CPLabelType' in request.POST:
            if len(request.POST['CPLabelType'])>0:
                LabelDetails['LabelType']=request.POST['CPLabelType']
        if 'CPLabelTitle' in request.POST:
            if len(request.POST['CPLabelTitle'])>0:
                if 'False' in request.POST['CPLabelTitle']:
                    LabelDetails['Titles']=False
        if 'CPLabelFont' in request.POST:
            if len(request.POST['CPLabelFont'])>0:
                LabelDetails['Font']=request.POST['CPLabelFont']
        if 'CPLabelSize' in request.POST:
            if len(request.POST['CPLabelSize'])>0:
                LabelDetails['Size']=request.POST['CPLabelSize']
        if 'CPLabelColour' in request.POST:
            if len(request.POST['CPLabelColour'])>0:
                LabelDetails['Colour']=request.POST['CPLabelColour']
        c.update(LabelDetails)        
        c.update({'LabelCounter':GeneralFunctions.TemplateCounter()})
        return render_to_response('LetterTemplates/ParentPupilNames_L7163.html',c)


def generate_staff_letter_labels(request, AcYear, school):
    """
    This function generates labels for staff members
    """
    staff_list = list()
    for staff_id in request.POST.getlist('PupilSelect'):
        staff_list.append(Staff.objects.get(id=staff_id))
    c = {'staff_list': staff_list,'MEDIA_REDIRECT':settings.MEDIA_REDIRECT}
    label_type = request.POST['LabelType']
    if label_type == 'StaffLabel1':
        label_details = {'Font': 'Default',
                         'Colour': 'Default',
                         'Size': 'Default',
                         'Extra': 'None'}
        if 'StaffLabel1Font' in request.POST:
            if len(request.POST['StaffLabel1Font']) > 0:
                label_details['Font'] = request.POST['StaffLabel1Font']
        if 'StaffLabel1Size' in request.POST:
            if len(request.POST['StaffLabel1Size']) > 0:
                label_details['Size'] = request.POST['StaffLabel1Size']
        if 'StaffLabel1Colour' in request.POST:
            if len(request.POST['StaffLabel1Colour']) > 0:
                label_details['Colour'] = request.POST['StaffLabel1Colour']
        if 'StaffLabel1Extra' in request.POST:
            if len(request.POST['StaffLabel1Extra']) > 0:
                label_details['Extra'] = request.POST['StaffLabel1Extra']
        if 'StaffLabel1Adhoc' in request.POST:
            if len(request.POST['StaffLabel1Adhoc']) > 0:
                label_details['Adhoc'] = request.POST['StaffLabel1Adhoc']
        c.update(label_details)
        return render_to_response('LetterTemplates/StaffLabel1_L7163.html', c)
    elif label_type == 'AddressWithStaffName':
        return render_to_response('LetterTemplates/AddressLabelStaff_L7163.html', c)


def generate_school_letter_labels(request, AcYear, school):
    """
    This function generates labels for schools in from the school list model
    """
    school_list = list()
    for school_id in request.POST.getlist('PupilSelect'):
        school_list.append(SchoolList.objects.get(id=school_id))
    c = {'school_list': school_list,'MEDIA_REDIRECT':settings.MEDIA_REDIRECT}
    if 'AddressWithSchoolNameWithPostalTitle' in request.POST['LabelType']:
        c.update({'with_postal_title': 'true'})
    return render_to_response('LetterTemplates/AddressLabelSchool_L7163.html', c)