
	var ArrayKW = new Array ();
{% load static from staticfiles %}
	var ArrayPrimary= new Array();
	var ArraySecondary= new Array();
	var ArrayPupilList= new Array();
	var ArrayHPTrigger= new Array();
	var NoPrimary = 0;
	var NoSecondary = 0;
	
	{% if staffRecs %}
		{% for staff in staffRecs %}
			ArrayPupilList.push("{{ staff.StaffId }}");
		{% endfor %}
	{% endif %}	
	{% if ClassList %}
		{% for Pupils in ClassList %}
			ArrayPupilList.push("{{Pupils.Pupil.id}}");
		{% endfor %}
	{% endif %}
	{% if schools %}
		{% for sch in schools %}
			ArrayPupilList.push("{{ sch.id }}");
		{% endfor %}	
	{% endif %}
	
	{% for Words in KWList.All %}
		ArrayKW.push("{{ Words.Keyword }}");
	{% endfor %}
	{% for Words in KWList.Primary %}
		ArrayPrimary.push("{{ Words.Keyword }}");
	{% endfor %}	
	{% for Words in KWList.Secondary %}
		ArraySecondary.push({'Pool':"{{ Words.KeywordPool }}",'Keyword':"{{ Words.Keyword}}"});
	{% endfor %}
	{% for Words in KWList.Subject %}
		ArraySecondary.push({'Pool':"{{ Words.KeywordPool }}",'Keyword':"{{ Words.Keyword}}"});
	{% endfor %}	
	{% for Words in KWList.Tertiary %}
		ArraySecondary.push({'Pool':"{{ Words.KeywordPool }}",'Keyword':"{{ Words.Keyword}}"});
	{% endfor %}
	{% for Triggers in KWList.HousePointWords %}
		ArrayHPTrigger.push('{{Triggers.Keyword}}');
	{% endfor %}
	var Subjects=false;
	var ShowHousePoints=0;
	
function UpdatePupilList(PupilNo)
{
	if (ArrayPupilList.indexOf(PupilNo) > -1)
	{
		ArrayPupilList.splice(ArrayPupilList.indexOf(PupilNo),1);
	}
	else
	{
		ArrayPupilList.push(PupilNo);
	}

}

function ToggleSelectAll()
{
	if(document.getElementById('SelectAll').checked==true)
	{
	// staff manager page
	{% if staffRecs %}
		{% for staff in staffRecs %}
		ArrayPupilList.push("{{ staff.StaffId }}");
		document.getElementById('Checked'+'{{ staff.StaffId }}').checked=true; 
		{% endfor %}
	{% endif %}
	// pupil manager page
	{% if ClassList %}
		{% for Pupils in ClassList %}
		ArrayPupilList.push("{{Pupils.Pupil.id}}");
		document.getElementById('Checked'+'{{Pupils.Pupil.id}}').checked=true; 
		{% endfor %}		
	{% endif %}
	// schools list page
	{% if schools %}
		{% for sch in schools %}
		ArrayPupilList.push("{{ sch.id }}");
		document.getElementById('Checked'+'{{ sch.id }}').checked=true; 
		{% endfor %}		
	{% endif %}	
	}
	else
	{ 
		for(i=0;i< ArrayPupilList.length;i++)
		{
			document.getElementById('Checked'+ArrayPupilList[i]).checked=false;
		}
		ArrayPupilList.length=0	
	}
}

function AddFor(PupilId,AddType)
{
	var Plist=document.getElementById(AddType+'PupilSelect');
	for(i=0;i<Plist.length;i++)
	{
		Plist.options[i].selected=false;
	}
	var PupilItem=document.getElementById(AddType+PupilId);
	PupilItem.selected=true;
}

function AddForSelected(AddType)
{
	var Plist=document.getElementById(AddType+'PupilSelect');
	for(i=0;i<Plist.length;i++)
	{
		Plist.options[i].selected=false;
	}
	for(i=0;i<ArrayPupilList.length;i++)
	{
		var PupilItem=document.getElementById(AddType+ArrayPupilList[i]);
		PupilItem.selected=true;
	}
	if (ArrayPupilList.length != 1) {
		alert("Warning: You are performing this action on "+ArrayPupilList.length+" objects");
	}
}
function displaySubjects()
{
	if (Subjects==false)
	{
		displaySecondary('Subject');
		Subjects=true;
	}
	else
	{
		hideSecondary('Subject');
		Subjects=false;
	}
	
}
		
function SelectKeyword(KW)
{
	var KWobject=document.getElementById(KW);
	var KWSelectObject=document.getElementById('KWSelect');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
		
		if (ArrayPrimary.indexOf(KW) > -1)
		{
			NoPrimary=NoPrimary+1;
		}
		if (ArraySecondary.indexOf(KW) > -1)
		{
			NoSecondary=NoSecondary+1;
		}
		if (NoPrimary > 0 && NoSecondary > 0)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='block';
		}
	}
	else
	{
		KWobject.style.backgroundColor='';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;
		if (ArrayPrimary.indexOf(KW) > -1)
		{
			NoPrimary=NoPrimary-1;
		}
		if (ArraySecondary.indexOf(KW) > -1)
		{
			NoSecondary=NoSecondary-1;
		}
		if (NoPrimary < 1 || NoSecondary < 1)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='none';
		}
	}
}

function SelectPrimary(KW) 
{
	var KWobject=document.getElementById(KW);
	var KWSelectObject=document.getElementById('KWSelect');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
		displaySecondary(KW);
		NoPrimary=NoPrimary+1;

		if (NoPrimary > 0 && NoSecondary > 0)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='block';
		}
	}
	else
	{
		KWobject.style.backgroundColor='';
		hideSecondary(KW);
		NoPrimary=NoPrimary-1;
		if (NoPrimary < 1 || NoSecondary < 1)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='none';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;			
		}
	}
}	

function SelectSecondary(KW) 
{
	var KWobject=document.getElementById(KW);
	var KWSelectObject=document.getElementById('KWSelect');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
		displaySecondary(KW);
		NoSecondary=NoSecondary+1;

		if (NoPrimary > 0 && NoSecondary > 0)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='block';
		}
	}
	else
	{
		KWobject.style.backgroundColor='';
		hideSecondary(KW);
		NoSecondary=NoSecondary-1;
		if (NoPrimary < 1 || NoSecondary < 1)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='none';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;			
		}
	}
}	



function SelectTertiary(KW) 
{
	var KWobject=document.getElementById(KW);
	var KWSelectObject=document.getElementById('KWSelect');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
		NoSecondary=NoSecondary+1;

		if (NoPrimary > 0 && NoSecondary > 0)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='block';
		}
	}
	else
	{
		KWobject.style.backgroundColor='';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;
		hideSecondary(KW);
		NoSecondary=NoSecondary-1;
		if (NoPrimary < 1 || NoSecondary < 1)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='none';
		}
	}
}
function displayHousePoint(KW)
{
	if (ArrayHPTrigger.indexof(KW) > -1)
	{
		ShowHousePoints+=1;
		var HPForm=document.getElementById('HousePoints');
		HPForm.style.display='block';
	}
}
function hideHousePoint(KW)
{
	if (ArrayHPTrigger.indexof(KW) > -1)
	{
		ShowHousePoints-=1;
		if (ShowHousePoints < 1)
		{
			var HPForm=document.getElementById('HousePoints');
			HPForm.style.display='none';
		}
	}
}
		
		
function displaySecondary(KW)
{
	for (i=0;i< ArraySecondary.length;i++)
	{
		if (ArraySecondary[i]['Pool']==KW)	
		{
			var Keyword=document.getElementById(ArraySecondary[i]['Keyword']);
			Keyword.style.display='block';
		}
	}
}
function hideSecondary(KW)
{
	for (i=0;i< ArraySecondary.length;i++)
	{
		if (ArraySecondary[i]['Pool']==KW)	ArrayKW
		{
			var Keyword=document.getElementById(ArraySecondary[i]['Keyword']);
			Keyword.style.display='none';
			if (Keyword.style.backgroundColor !='')
			{
				Keyword.style.backgroundColor='';
				NoSecondary=NoSecondary-1;
				if (NoSecondary < 1)
				{
					var DetailsBox=document.getElementById('DetailsBox');
					DetailsBox.style.display='none';
				}
				var KWSelectObject=document.getElementById('KWSelect');
				KWSelectObject.options[ArrayKW.indexOf(ArraySecondary[i]['Keyword'])].selected=false;
				
				for (x=0;x< ArraySecondary.length;x++)
				{
					if (ArraySecondary[x]['Pool']==ArraySecondary[i]['Keyword'])	
					{
						var Keyword=document.getElementById(ArraySecondary[x]['Keyword']);
						Keyword.style.display='none';
						if (Keyword.style.backgroundColor !='')
						{
							Keyword.style.backgroundColor='';
							NoSecondary=NoSecondary-1;
							if (NoSecondary < 1)
							{
								var DetailsBox=document.getElementById('DetailsBox');
								DetailsBox.style.display='none';
							}
							var KWSelectObject=document.getElementById('KWSelect');
							KWSelectObject.options[ArrayKW.indexOf(ArraySecondary[x]['Keyword'])].selected=false;			
						}

					}
				}				
				
				
							
			}

		}
	}
}

function ShowLetterDesc()
{
	var DescElement=document.getElementById('LetterDescription');
	switch (document.getElementById("SelectLetter").value)
	{
		
		{% for Letters in LetterList %}
		case '{{Letters.id}}' :
			DescElement.innerHTML='{{Letters.Description|linebreaks}}';
			break;

	{% endfor %}
		default:
			DescElement.innerHTML='No Description';	
	}
}

function SelectFilterKeyword2(KW)
{
	var keyWord = KW+'__2';
	var KWobject=document.getElementById(keyWord);
	var KWSelectObject=document.getElementById('KWSelect2');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
	}
	else
	{
		KWobject.style.backgroundColor='';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;
	}
}	
