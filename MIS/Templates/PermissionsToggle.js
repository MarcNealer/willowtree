
function togglestyleInternet(el){
{% load static from staticfiles %}
    if(el.id == "PupilInternetPermissionsHave") {
    	el.id = "PupilInternetPermissionsHaveNot";
    	el.style.backgroundImage = "url('{% static "images/icons/artistica2/png/32x32/user_delete.png" %}')";
    } else {
    	el.id = "PupilInternetPermissionsHave";
    	el.style.backgroundImage = "url('{% static "images/icons/artistica2/png/32x32/user_accept.png" %}')";
    }
}

function toggleCheckBoxInternet(a,b){ 
	if (a.id == "PupilInternetPermissionsHave") {
		b.checked = true;
	} else {
		b.checked = false;
	}
}

function togglestyleMedia(el){
    if(el.id == "PupilMediaPermissionsHave") {
    	el.id = "PupilMediaPermissionsHaveNot";
    	el.style.backgroundImage = "url('{% static "images/icons/artistica2/png/32x32/user_delete.png" %}')";
    } else {
    	el.id = "PupilMediaPermissionsHave";
    	el.style.backgroundImage = "url('{% static "images/icons/artistica2/png/32x32/user_accept.png" %}')";
    }
}

function toggleCheckBoxMedia(a,b){ 
	if (a.id == "PupilMediaPermissionsHave") {
		b.checked = true;
	} else {
		b.checked = false;
	}
}
