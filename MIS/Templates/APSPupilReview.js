
//Define data here : 
{% load static from staticfiles %}
var jsonp = {{ DataObj.JSONData|safe }}
//Calculate the result here: Updated
$(document).ready(function() {
	var objData = $.parseJSON(jsonp);
	FilterData(objData, -1, -1, -1, -1, -1, -1, "");
	//Copy booker to 1st golfer:
    $("#chkGender").change(function () {
    	CreateFilter(objData);
    });
    $("#chkSen").change(function () {
    	CreateFilter(objData);
    });
    $("#chkeAL").change(function () {
    	CreateFilter(objData);
    });
    $("#chkgt").change(function () {
    	CreateFilter(objData);
    });

    $("#chkMoreAble").change(function () {
    	CreateFilter(objData);
    });

    $("#chkSB").change(function () {
    	CreateFilter(objData);
    });
    //Show golfers for toId
    $('#sltClass').change(function () {
        CreateFilter(objData);	
    });
    
    $("#btnreset").click(function () {

    	$('#sltClass').val("");
    	$('#chkGender').val("-1");
    	$('#chkSen').val("-1");
    	$('#chkeAL').val("-1");
    	$('#chkgt').val("-1");
    	$('#chkMoreAble').val("-1");
    	$('#chkSB').val("-1");

    	CreateFilter(objData);
    });
});

function CreateFilter(objData)
{
	var iGender = $("#chkGender").val();
	var iSen = $("#chkSen").val();
	var iGt = $("#chkgt").val();
	var iMoreable = $("#chkMoreAble").val();
	var iEAL = $("#chkeAL").val();
	var iSB = $("#chkSB").val();
	var sClass = $('#sltClass').val();
	console.log(iGender + '-' + iSen + '-' + iGt + '-' + iMoreable);
	FilterData(objData, iGender, iSen, iGt, iMoreable, iEAL, iSB, sClass);

}

function FilterData(objData, iGender, iSen, iGt, iMoreable, iEAL, iSB, sClass)
{
	var content = "";
	$('#tblResult >tbody').html("");
	//Make the data
	var TotalPrevSummer = 0;
	var TotalMich = 0;
	var TotalLent = 0;
	var TotalSummer = 0;
	var TotalProgress = 0;
	
	var CntMich = 0;
	var CntLent = 0;
	var CntSummer = 0;
	var CntPrevSummer = 0;

	$.each(objData, function (i, item) {
		//Filter here:
		var isValid = true;
		if(iGender == 1 && item['Gender'] == 'F')
		{
			isValid = false;
		}
		if(iGender == 0 && item['Gender'] == 'M')
		{
			isValid = false;
		}
		if(iSen == 1 && item['SEN'] == 'F')
		{
			isValid = false;
		}
		if(iSen == 0 && item['SEN'] == 'T')
		{
			isValid = false;
		}
		if(iGt == 1 && item['G&T'] == 'F')
		{
			isValid = false;
		}
		if(iGt == 0 && item['G&T'] == 'T')
		{
			isValid = false;
		}
		if(iMoreable == 1 && item['MoreAble'] == 'F')
		{
			isValid = false;
		}
		if(iMoreable == 0 && item['MoreAble'] == 'T')
		{
			isValid = false;
		}
		
		if(iEAL == 1 && item['EAL'] == 'F')
		{
			isValid = false;
		}
		if(iEAL == 0 && item['EAL'] == 'T')
		{
			isValid = false;
		}
		
		if(iSB == 1 && item['SumBirth'] == 'F')
		{
			isValid = false;
		}
		if(iSB == 0 && item['SumBirth'] == 'T')
		{
			isValid = false;
		}
		
		//Filter by class:
		if(sClass != "")
		{
			if(sClass != item['Class'])
			{
				isValid = false;
			}
		}
		

		if(isValid)
		{
			var strLS = '';
			if(item['SEN'] == 'T')
			{
				strLS += 'SEN,';
			}	
			if(item['EAL'] == 'T')
			{
				strLS += 'EAL4,';
			}	
			if(item['G&T'] == 'T')
			{
				strLS += 'EMT,';
			}	
			if(item['MoreAble'] == 'T')
			{
				strLS += 'SumBirth,';
			}
			//Calculate Progress So far:
			var strProgress = parseInt(item['Summer']);
			if($.trim(item['Summer']) == "")
			{
				if($.trim(item['Lent']) == "")
				{
					strProgress =  parseInt(item['Mich']) - parseInt(item['PrevSummer']);
				}
				else
				{
					strProgress = parseInt(item['Lent']) - parseInt(item['PrevSummer']) ;
				}
			}
			else
			{
				TotalSummer += strProgress;
				strProgress = strProgress - parseInt(item['PrevSummer']);
				CntSummer++;
			}	
			if($.trim(item['Lent']) != "")
			{
				TotalLent += parseInt(item['Lent']);
				CntLent++;
			}
			
			if($.trim(item['Mich']) != "")
			{
				TotalMich += parseInt(item['Mich']);
				CntMich++;
			}	
				
			if($.trim(item['PrevSummer']) != "")
			{
				TotalPrevSummer += parseInt(item['PrevSummer']);
				CntPrevSummer++;
			}	
				
			
			TotalProgress += strProgress;
			
			content += '<tr>';
			content += '<td>' + item['Fullname'] + '</td>';
			content += '<td>' + item['Class'] + '</td>';
			content += '<td>' + item['Gender'] + '</td>';
			content += '<td>' + strLS + '</td>';
			content += '<td>' + item['PrevSummer'] + '</td>';
			content += '<td>' + item['Mich'] + '</td>';
			content += '<td>' + item['Lent'] + '</td>';
			content += '<td>' + item['Summer'] + '</td>';
			content += '<td><b>' + strProgress + '</b></td>';
			content += '</tr>';
		}
	});
	//set session data: 
    $('#tblResult >tbody').html(content); 
	//total rows: 
    var TotalRows = objData.length;
    var strFooter = "<tr>";
	strFooter += "<td colspan='4' align='center'><b>Average</b></td>";
	strFooter += "<td><b>" + (TotalPrevSummer/CntPrevSummer).toFixed(2) + "</b></td>";
	strFooter += "<td><b>" + (TotalMich/CntMich).toFixed(2) + "</b></td>";
	strFooter += "<td><b>" + (TotalLent/CntLent).toFixed(2) + "</b></td>";
	strFooter += "<td><b>" + (TotalSummer/CntSummer).toFixed(2) + "</b></td>";
	strFooter += "<td><b>" + (TotalProgress/TotalRows).toFixed(2) + "</b></td>";
	strFooter += "</tr>";
    $('#tblResult >tfoot').html(strFooter); 

}

