
	var ArrayKW = new Array ();
{% load static from staticfiles %}
	var ArrayPrimary= new Array();
	var ArraySecondary= new Array();
	var ArrayPupilList= new Array();
	var ArrayHPTrigger= new Array();
	var NoPrimary = 0;
	var NoSecondary = 0;
	{% if standardNotesOnly %}
		{% for Words in KWList %} 
			ArrayKW.push("{{ Words }}");
		{% endfor %}
	{% endif %}	
	{% if achievementsOnly %}
		{% for Words in KWList.achievementKeywordsOnly %} 
			ArrayKW.push("{{ Words }}");
		{% endfor %}	
	{% else %}
		{% for Words in KWList.All %} 
			ArrayKW.push("{{ Words }}");
		{% endfor %}
	{% endif %}
	{% for Words in KWList.Primary %}
		ArrayPrimary.push("{{ Words.Keyword }}");
	{% endfor %}	
	{% for Words in KWList.Secondary %}
		ArraySecondary.push({'Pool':"{{ Words.KeywordPool }}",'Keyword':"{{ Words.Keyword}}"});
	{% endfor %}
	{% for Words in KWList.Subject %}
		ArraySecondary.push({'Pool':"{{ Words.KeywordPool }}",'Keyword':"{{ Words.Keyword}}"});
	{% endfor %}	
	{% for Words in KWList.Tertiary %}
		ArraySecondary.push({'Pool':"{{ Words.KeywordPool }}",'Keyword':"{{ Words.Keyword}}"});
	{% endfor %}
	{% for Triggers in KWList.HousePointWords %}
		ArrayHPTrigger.push('{{Triggers.Keyword}}');
	{% endfor %}
	{% for PupilDetails in PupilList %}
		ArrayPupilList.push("{{ PupilDetails.id }}");
	{% endfor %}
	var Subjects=false;
	var ShowHousePoints=0;

function displaySubjects()
{
	if (Subjects==false)
	{
		displaySecondary('Subject');
		Subjects=true;
	}
	else
	{
		hideSecondary('Subject');
		Subjects=false;
	}
	
}
		
function SelectPupil(Pupil_Id)
{
	var PupilSelectObject=document.getElementById('PupilSelect');
	var PupilObject=document.getElementById(Pupil_Id);
	if (PupilSelectObject.options[ArrayPupilList.indexOf(Pupil_Id)].selected==true)
	{
		PupilSelectObject.options[ArrayPupilList.indexOf(Pupil_Id)].selected=false;
		PupilObject.style.backgroundColor='';
	}
	else
	{
		PupilSelectObject.options[ArrayPupilList.indexOf(Pupil_Id)].selected=true;
		PupilObject.style.backgroundColor="#ADA96E";
	}
}
function SelectKeyword(KW)
{
	var KWobject=document.getElementById(KW);
	var KWSelectObject=document.getElementById('KWSelect');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
		
		if (ArrayPrimary.indexOf(KW) > -1)
		{
			NoPrimary=NoPrimary+1;
		}
		if (ArraySecondary.indexOf(KW) > -1)
		{
			NoSecondary=NoSecondary+1;
		}
		if (NoPrimary > 0 && NoSecondary > 0)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='block';
		}
	}
	else
	{
		KWobject.style.backgroundColor='';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;
		if (ArrayPrimary.indexOf(KW) > -1)
		{
			NoPrimary=NoPrimary-1;
		}
		if (ArraySecondary.indexOf(KW) > -1)
		{
			NoSecondary=NoSecondary-1;
		}
		if (NoPrimary < 1 || NoSecondary < 1)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='none';
		}
	}
}

function SelectFilterKeyword2(KW)
{
	var keyWord = KW+'__2';
	var KWobject=document.getElementById(keyWord);
	var KWSelectObject=document.getElementById('KWSelect2');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
	}
	else
	{
		KWobject.style.backgroundColor='';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;
	}
}

function SelectFilterKeyword(KW)
{
	var KWobject=document.getElementById(KW);
	var KWSelectObject=document.getElementById('KWSelect');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
	}
	else
	{
		KWobject.style.backgroundColor='';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;
	}
}

function SelectPrimary(KW) 
{
	var KWobject=document.getElementById(KW);
	var KWSelectObject=document.getElementById('KWSelect');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
		displaySecondary(KW);
		NoPrimary=NoPrimary+1;

		if (NoPrimary > 0 && NoSecondary > 0)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='block';
		}
	}
	else
	{
		KWobject.style.backgroundColor='';
		hideSecondary(KW);
		NoPrimary=NoPrimary-1;
		if (NoPrimary < 1 || NoSecondary < 1)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='none';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;			
		}
	}
}	

function SelectSecondary(KW) 
{
	var KWobject=document.getElementById(KW);
	var KWSelectObject=document.getElementById('KWSelect');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
		displaySecondary(KW);
		NoSecondary=NoSecondary+1;

		if (NoPrimary > 0 && NoSecondary > 0)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='block';
		}
	}
	else
	{
		KWobject.style.backgroundColor='';
		hideSecondary(KW);
		NoSecondary=NoSecondary-1;
		if (NoPrimary < 1 || NoSecondary < 1)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='none';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;			
		}
	}
}	



function SelectTertiary(KW) 
{
	var KWobject=document.getElementById(KW);
	var KWSelectObject=document.getElementById('KWSelect');
	
	if ( KWobject.style.backgroundColor == '')
	{
		KWobject.style.backgroundColor="#ADA96E";
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=true;
		NoSecondary=NoSecondary+1;

		if (NoPrimary > 0 && NoSecondary > 0)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='block';
		}
	}
	else
	{
		KWobject.style.backgroundColor='';
		KWSelectObject.options[ArrayKW.indexOf(KW)].selected=false;
		hideSecondary(KW);
		NoSecondary=NoSecondary-1;
		if (NoPrimary < 1 || NoSecondary < 1)
		{
			var DetailsBox=document.getElementById('DetailsBox');
			DetailsBox.style.display='none';
		}
	}
}
function displayHousePoint(KW)
{
	if (ArrayHPTrigger.indexof(KW) > -1)
	{
		ShowHousePoints+=1;
		var HPForm=document.getElementById('HousePoints');
		HPForm.style.display='block';
	}
}
function hideHousePoint(KW)
{
	if (ArrayHPTrigger.indexof(KW) > -1)
	{
		ShowHousePoints-=1;
		if (ShowHousePoints < 1)
		{
			var HPForm=document.getElementById('HousePoints');
			HPForm.style.display='none';
		}
	}
}
		
		
function displaySecondary(KW)
{
	for (i=0;i< ArraySecondary.length;i++)
	{
		if (ArraySecondary[i]['Pool']==KW)	
		{
			var Keyword=document.getElementById(ArraySecondary[i]['Keyword']);
			Keyword.style.display='block';
		}
	}
}
function hideSecondary(KW)
{
	for (i=0;i< ArraySecondary.length;i++)
	{
		if (ArraySecondary[i]['Pool']==KW)	
		{
			var Keyword=document.getElementById(ArraySecondary[i]['Keyword']);
			Keyword.style.display='none';
			if (Keyword.style.backgroundColor !='')
			{
				Keyword.style.backgroundColor='';
				NoSecondary=NoSecondary-1;
				if (NoSecondary < 1)
				{
					var DetailsBox=document.getElementById('DetailsBox');
					DetailsBox.style.display='none';
				}
				var KWSelectObject=document.getElementById('KWSelect');
				KWSelectObject.options[ArrayKW.indexOf(ArraySecondary[i]['Keyword'])].selected=false;
				
				for (x=0;x< ArraySecondary.length;x++)
				{
					if (ArraySecondary[x]['Pool']==ArraySecondary[i]['Keyword'])	
					{
						var Keyword=document.getElementById(ArraySecondary[x]['Keyword']);
						Keyword.style.display='none';
						if (Keyword.style.backgroundColor !='')
						{
							Keyword.style.backgroundColor='';
							NoSecondary=NoSecondary-1;
							if (NoSecondary < 1)
							{
								var DetailsBox=document.getElementById('DetailsBox');
								DetailsBox.style.display='none';
							}
							var KWSelectObject=document.getElementById('KWSelect');
							KWSelectObject.options[ArrayKW.indexOf(ArraySecondary[x]['Keyword'])].selected=false;			
						}

					}
				}				
				
				
							
			}

		}
	}
}
