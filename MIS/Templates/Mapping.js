
//Define data here : 
{% load static from staticfiles %}

var jsonp =  {{ DataObj.JSONData|safe }}
//Calculate the result here: 
$(document).ready(function() {
	var objData = $.parseJSON(jsonp);
	CreateFilter(objData);
	
	//Copy booker to 1st golfer:
    $(".gender").change(function () {
    	CreateFilter(objData);
    });
    $(".sen").change(function () {
    	CreateFilter(objData);
    });
    $(".ckgt").change(function () {
    	CreateFilter(objData);
    });
    $(".moreable").change(function () {
    	CreateFilter(objData);
    });
    $(".summerbirth").change(function () {
    	CreateFilter(objData);
    });
    $(".showempty").change(function () {
    	CreateFilter(objData);
    });
    
    $("#btnreset").click(function () {
    	$("#frmReport input[name='chkGender']")[0].checked = true;
    	$("#frmReport input[name='chkSen']")[0].checked = true;
    	$("#frmReport input[name='chkgt']")[0].checked = true;
    	$("#frmReport input[name='chkMoreAble']")[0].checked = true;
    	$("#frmReport input[name='chkSB']")[0].checked = true;
    	
    	$('#sltClass').val("");
    	CreateFilter(objData);
    });

    //Show golfers for toId
    $('#sltClass').change(function () {
        CreateFilter(objData);	
    });

    $('[title]').qtip(); 	
});
function CreateFilter(objData)
{
	var iGender = $("#frmReport input[name='chkGender']:checked").val();
	var iSen = $("#frmReport input[name='chkSen']:checked").val();
	var iGt = $("#frmReport input[name='chkgt']:checked").val();
	var iMoreable = $("#frmReport input[name='chkMoreAble']:checked").val();
	var iSB = $("#frmReport input[name='chkSB']:checked").val();
	var chkShowEmpty = $("#frmReport input[name='chkShowEmpty']:checked").val();
	var isShow = false;
	if(chkShowEmpty == undefined)
	{
		isShow = true;
	} 
	
	var sClass = $('#sltClass').val();
	console.log(iGender + '-' + iSen + '-' + iGt + '-' + iMoreable + '-' + sClass + '-' + iSB);
	FilterData(objData, iGender, iSen, iGt, iMoreable, iSB, sClass, isShow);

    $('[title]').qtip(); 	

}

function FilterData(objData, iGender, iSen, iGt, iMoreable, iSB, sClass, isShow)
{
	var content = "";
	$('#tblResult >tbody').html("");
	var arrResult = [];
	
	//Init: 
	var objRow = new Object();
	var arrResult = [];
	//Init the data: 
	var objRow = new Object();
	objRow.key = 'w';
	objRow.Cnt = 0;
	arrResult.push(objRow);
	for(var i=1;i<=8;i++)
	{
		var objRow = new Object();
		objRow.key = i +'c';
		objRow.Cnt = 0;
		arrResult.push(objRow);
		var objRow = new Object();
		objRow.key = i +'b';
		objRow.Cnt = 0;
		arrResult.push(objRow);
		var objRow = new Object();
		objRow.key = i +'a';
		objRow.Cnt = 0;
		arrResult.push(objRow);
	}
	
	//Make the data
	$.each(objData, function (i, item) {
		//Filter here:
		var isValid = true;
		if(iGender == 1 && item['Gender'] == 'F')
		{
			isValid = false;
		}
		if(iGender == 0 && item['Gender'] == 'M')
		{
			isValid = false;
		}
		if(iSen == 1 && item['Gender'] == 'F')
		{
			isValid = false;
		}
		if(iSen == 0 && item['Gender'] == 'T')
		{
			isValid = false;
		}
		if(iGt == 1 && item['G&T'] == 'F')
		{
			isValid = false;
		}
		if(iGt == 0 && item['G&T'] == 'T')
		{
			isValid = false;
		}
		if(iMoreable == 1 && item['MoreAble'] == 'F')
		{
			isValid = false;
		}
		if(iMoreable == 0 && item['MoreAble'] == 'T')
		{
			isValid = false;
		}
		if(iSB == 1 && item['SummerBirth'] == 'F')
		{
			isValid = false;
		}
		if(iSB == 0 && item['SummerBirth'] == 'T')
		{
			isValid = false;
		}
		
		//Filter by class:
		if(sClass != "")
		{
			if(sClass != item['Class'])
			{
				isValid = false;
			}
		}
		
		if(isValid)
		{
			//List all previous year: 
			var prevYear = item['Level1'];
			var curYear = item['Level2'];
			//Find value of Rows for previous year: 
			//Update case here : 
			$.each(arrResult, function (k, item1) {
				if(item1.key == prevYear)
				{
					//Calculate text :
					var strTextColour = "";
					var strTitle = ""; 
					var strCnt = "";
					if(item['SEN'] == 'T')
					{
						strTitle += item['SEN_Details'] + ',';
						strTextColour = 'color="forestgreen"';
					}	
					if(parseInt(item['EAL']) > 0 )
					{
						strCnt = '(' + item['EAL'] + ')';
						strTitle += 'EAL' + item['EAL'] + ',';
					}	
					if(item['SummerBirth'] == 'T')
					{
						strCnt = '<font color="gold">(SB)</font>';
						strTitle += 'SumBirth,';
					}	
					if(item['G&T'] == 'T')
					{
						strTitle +=  item['G&T_Details'] + ',';
						strTextColour = 'color="red"';
					}	
					
					if(item['SEN'] == 'F' && item['SummerBirth'] == 'F' &&  item['G&T'] == 'F')
					{
						strTextColour = 'color="lightseagreen"';
					}
					/*
					if(item['MoreAble'] == 'T')
					{
					}
					*/	
					if(item1[curYear] != null)
					{
						item1[curYear] += "<br/><font " + strTextColour + " title='" + strTitle + "'>" + item['Fullname'] + strCnt + "</font>";
					}
					else
					{
						item1[curYear] = "<font " + strTextColour + " title='" + strTitle + "'>" + item['Fullname'] + strCnt + "</font>";
					}
					item1.Cnt += 1;
				}
			});
		}
	});

	var strColumn = "1c,1b,1a,2c,2b,2a,3c,3b,3a,4c,4b,4a,5c,5b,5a,6c,6b,6a,7c,7b,7a,8c,8b,8a,";
	var arrCols = strColumn.split(","); 
	if(isShow)
	{
		var strExitColumns = "";
		$.each(arrResult, function (i, item) {
			if(item.Cnt > 0)
			{
				for(var i=1;i<=8;i++)
				{
					var strCol = "";
					var strCol1 = "";
					if(item[i +'c'])
					{
						strCol = i+'c';
						if(i>1)
						{
							strCol1 = (i-1).toString() + 'a';
						}
					}
					if(item[i +'b'])
					{
						strCol = i+'b'; 
						strCol1 = i+'c';
					}
					if(item[i +'a'])
					{
						strCol = i+'a';
						strCol1 = i+'b';
					}
					
					if(strCol != "" && strExitColumns.indexOf(strCol) == -1)
					{
						strExitColumns += strCol + ',';	
					}
					if(strCol1 != "" && strExitColumns.indexOf(strCol1) == -1)
					{
						strExitColumns += strCol1 + ',';	
					}
					
				}
			}
	    });
		console.log('exit columns: ' + strExitColumns );
		$.each(arrCols, function (i, item) {
			if(strExitColumns.indexOf(item) == -1)
			{
				strColumn  = replaceAll(item + ',', '', strColumn);
			}
		});
		arrCols = strColumn.split(",");
		console.log("COLUMNS*************");
		console.log(arrCols);
		
		//Sort columns: 
		/*arrCols.sort(function(a,b) {
		    if ( a < b )
		        return -1;
		    if ( a > b)
		        return 1;
		    return 0;
		} );
		*/
	}
	content = '<tr><td width="170" bgcolor="#F7F8F9"><b>Across=Current Year<br/>Down=Prev Year</b></td>';
	var iTotalCols = 1;
	$.each(arrCols, function (i, item) {
		if(item != '')
		{
			iTotalCols++;
		}
	});
	var perCol = 4;
	if(isShow)
	{
		perCol = 100/iTotalCols;
	}
	$.each(arrCols, function (i, item) {
		if(item != '')
		{
			content += '<td bgcolor="#F7F8F9" width="' + perCol + '%"><b>' + item + '</b></td>';
		}
	});
	content += '<tr>';
	
	var iCount = 3;
	var isCount = 1;
	if(isShow && iGt != "-1")
	{
		isCount = 2;
	}
	$.each(arrResult, function (i, item) {
		var strDisplay = '';
		if(isShow)
		{
			if(item.Cnt == 0)
			{
				strDisplay = ' style="display:none;" ';
			}
		}
		content += '<tr' + strDisplay + '>';
		
		//content += '<tr>';
		content += '<td bgcolor="#F7F8F9"><b>' + item.key + '</b></td>';
		var iSub = 1;
		$.each(arrCols, function (j, itemcol) {
			var strcolour = "";	
			if(!isShow)
			{
				if(iSub == iCount)
				{
					strcolour = " bgcolor='peachpuff' ";
				}
				else if(iSub == iCount + 1)
				{
					strcolour = " bgcolor='plum' ";
				}
			}
			if(itemcol != '')
			{
				if(isShow)
				{
					if(iSub == isCount)
					{
						strcolour = " bgcolor='peachpuff' ";
					}
					else if(iSub == isCount + 1)
					{
						strcolour = " bgcolor='plum' ";
					}
				}
				//bgcolor="plum" id="cell_' + item.key + '"
				//(*) Shows the EAL level is applicable
				if(item[itemcol])
				{
					/*
					if(strcolour == "")
					{
						strcolour = " bgcolor='plum' ";
					}*/
					content += '<td ' + strcolour + '>' + item[itemcol] + '</td>';
				}
				else
					content += '<td' + strcolour + '></td>';	
			}
			iSub++;
			
			
		});
		if(item.Cnt > 0)
		{
			isCount++;
		}	
		if(item.key != "1b" && item.key != "1a")
		{
			iCount++;
		}
		content += '</tr>';
    });
	
	//set session data: 
    $('#tblResult >tbody').html(content);

    //Add another colour now: 
	/*$.each(arrResult, function (i, item) {
		if(item.Cnt > 0)
		{
			$.each(arrCols, function (j, itemcol) {	
				if(itemcol != '')
				{
					//(*) Shows the EAL level is applicable
					if(item[itemcol] && $('#cell_' + item.key).prev().text() == "")
					{
						$('#cell_' + item.key).prev().css('backgroundColor', 'peachpuff');
					}
				}
			});
		}
	});
	 */
}

function getById(id, myArray) {
    return myArray.filter(function(obj) {
      if(obj.key == id) {
        return obj; 
      }
    })[0];
}

function replaceAll(find, replace, str) 
{
  while( str.indexOf(find) > -1)
  {
    str = str.replace(find, replace);
  }
  return str;
}

