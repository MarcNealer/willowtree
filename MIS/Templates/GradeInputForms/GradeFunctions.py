
# -*- coding: utf-8 -*-
{% load static from staticfiles %}
"""
Created on Tue Jan 22 08:42:54 2013

@author: dbmgr
"""
from django.core.cache import cache
import datetime
import copy
from MIS.models import *
from MIS.modules.ExtendedRecords import *
from MIS.modules import GeneralFunctions
from MIS.modules.PupilRecs import *
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules.attendance.AttendanceFunctions import *
import numpy

class PupilGrade():
    ''' Pupil Grade Object. This object holds basic details on the pupil, along
    with the grades for the Current Record passed as an argument, and its previous
    terms grades as satted in GradeInputElement'''
    def __init__(self,PupilId,CurrentRecord,SingleSubject=None):
        if SingleSubject:
            self.GradeElements=GradeInputElement.objects.filter(GradeRecord__Name=CurrentRecord,GradeSubject__Name=SingleSubject)
        else:
            self.GradeElements=GradeInputElement.objects.filter(GradeRecord__Name=CurrentRecord)            
        self.ExtendedRecord=ExtendedRecord(BaseType='Pupil',BaseId=PupilId)
        self.GradeRecords=[]
        self.PupilItems=Pupil.objects.get(id=PupilId)
        for Records in self.GradeElements:
            GradeData={'Pupil':self.PupilItems,'Current':{},'Previous':{},'Form':Records.InputForm,'SubSubject':{},'GradeRecord':Records.GradeRecord.Name}
            CurRec=cache.get('%s_%s_%s' % (CurrentRecord,Records.GradeSubject.Name,PupilId))
            if not CurRec:
                CurRec=self.ExtendedRecord.ReadExtention(ExtentionName=Records.GradeRecord.Name,SubjectName=Records.GradeSubject.Name)
                cache.set('%s_%s_%s' % (CurrentRecord,Records.GradeSubject.Name,PupilId),CurRec,3600)
            GradeData['Current']=CurRec
            if Records.PrevGradeRecord:
                PrevRec=cache.get('%s_%s_%s' % (Records.PrevGradeRecord.Name,Records.GradeSubject.Name,PupilId))
                if not PrevRec:
                    PrevRec=self.ExtendedRecord.ReadExtention(ExtentionName=Records.PrevGradeRecord.Name,SubjectName=Records.GradeSubject.Name)
                    cache.set('%s_%s_%s' % (Records.PrevGradeRecord.Name,Records.GradeSubject.Name,PupilId),PrevRec,3600)
            else:
                PrevRec=None
            GradeData['Previous']=PrevRec
            
            if ParentSubjects.objects.filter(ParentSubject=Records.GradeSubject).exists():
                SubGradeData={}
                for subSubjects in ParentSubjects.objects.filter(ParentSubject=Records.GradeSubject):

                    subCurrRec=cache.get('%s_%s_%s' % (CurrentRecord,subSubjects.Subject.Name,PupilId))
                    if not subCurrRec:
                        subCurrRec=self.ExtendedRecord.ReadExtention(ExtentionName=Records.GradeRecord.Name,SubjectName=subSubjects.Subject.Name)
                        cache.set('%s_%s_%s' % (CurrentRecord,subSubjects.Subject.Name,PupilId),subCurrRec,3600)

                    subPrevRec=cache.get('%s_%s_%s' % (Records.PrevGradeRecord.Name,subSubjects.Subject.Name,PupilId))
                    if not subPrevRec:
                        subPrevRec=self.ExtendedRecord.ReadExtention(ExtentionName=Records.PrevGradeRecord.Name,SubjectName=subSubjects.Subject.Name)
                        cache.set('%s_%s_%s' % (Records.PrevGradeRecord.Name,subSubjects.Subject.Name,PupilId),subPrevRec,3600)
                    SubGradeData[subSubjects.Subject.Name]={'Current':subCurrRec,'Previous':subPrevRec}
                GradeData['SubSubject']=copy.deepcopy(SubGradeData)

            self.GradeRecords.append([Records.GradeSubject.Name,GradeData])



class GroupGrade():
    ''' Group Grade Object. This gathers grades for a selected Grade record, for
    a preselected subject and group'''
    def __init__(self,AcademicYear,Group_Name,CurrentRecord,SubjectName):
        self.GradeElements=GradeInputElement.objects.get(GradeRecord__Name=CurrentRecord,GradeSubject__Name=SubjectName)
        self.GroupName=Group_Name
        self.GradeRecord=CurrentRecord
        PupilList=GeneralFunctions.Get_PupilListExact(AcYear=AcademicYear,GroupName=Group_Name)
        self.GroupGrades=[]
        for Pupils in PupilList:
            self.GroupGrades.append([Pupils.id,PupilGrade(Pupils.id,CurrentRecord,SubjectName)])
            
        
            
def UpdatePupilGrade(request,PupilId,CurrentRecord,Subject):
    UpdateData={}
    UpdateData.update(request.POST)
    for items in UpdateData.keys():
        if not len(UpdateData[items][0]) > 0:
            del UpdateData[items]
        else:
            UpdateData[items]=UpdateData[items][0]
    NewRecord=ExtendedRecord(BaseType='Pupil',BaseId=PupilId) 
    NewRecord.WriteExtention(CurrentRecord,UpdateData,Subject)
    cache.delete('%s_%s_%s' %(CurrentRecord,Subject,PupilId))
    return
            
class SelectGradeReports():
    ''' class to find a list of report records for grading. It holds a list
    of report records that are due for this time, plus a list of all the other
    grading reports '''
    def __init__(self,GroupName):
        self.GroupName=GroupName

    def SetCurrentSeason(self,GroupName):
        ''' Recusive function to find a the nearest Grade Report record to the name
        passed'''
        GroupItems=GroupName.split('.')
        GroupTest='.'.join(GroupItems[2:])
        Today=datetime.datetime.now().timetuple().tm_yday
        if ReportingSeason.objects.filter(GradeGroup__Name__icontains=GroupTest,GradeStartDay__lte=Today,GradeStopDay__gte=Today).exists():
            return ReportingSeason.objects.filter(GradeGroup__Name__icontains=GroupTest,GradeStartDay__lte=Today,GradeStopDay__gte=Today)[0]
        else:
            if len(GroupItems)>3:
                 return self.SetCurrentSeason('.'.join(GroupName.split('.'))[0:-1])
            else:
                return 'Not Found'
    def GetAllReports(self):
        ''' Returns a list of all reporting seasons'''
        return ReportingSeason.objects.all()
    def GetAllGradeRecords(self):
        ''' returns a list of Grade Input Elements'''
        return GradeInputElement.objects.all().order_by('GradeRecord','GradeSubject')
    def GetGradeSubjectsDictionary(self):
        ''' returns a dictionary of Subjects associated with a Grade Records '''
        GradeDic={}
        for Records in self.GetAllGradeRecords():
            if Records.GradeRecord.Name in GradeDic:
                GradeDic[Records.GradeRecord.Name].append(Records.GradeSubject.Name)
            else:
                GradeDic[Records.GradeRecord.Name]=[Records.GradeSubject.Name]
        return GradeDic
    def GetCurrentSeason(self):
        ''' Returns the report season record that is closest to the current date
        and the group name. If no match, then it will return false '''
        return self.SetCurrentSeason(self.GroupName)

class SingleGradeRecord():
    def __init__(self,Pupilid,GradeElement,ReportYear,ReportName):
        self.PupilId=Pupilid
        self.ExtendedRecord=ExtendedRecord('Pupil',self.PupilId)
        self.GradeElement=GradeElement
        self.ReportYear=ReportYear
        self.ReportName=ReportName
        self.PreviousRecord={}
        self.CurrentRecord={}
        self.Averages=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('%s.%s' % (self.ReportName,self.GradeElement.GradeSubject.Name),AcYear=self.ReportYear)
        if self.Averages:
            self.Averages=eval(self.Averages)
    def Previous(self):
        if self.PreviousRecord:
            return self.PreviousRecord
        else:
            if self.GradeElement.PrevGradeRecord:
                self.PreviousRecord=self.ExtendedRecord.ReadExtention(self.GradeElement.PrevGradeRecord.Name,self.GradeElement.GradeSubject.Name)
            return self.PreviousRecord
    def Current(self):
        if self.CurrentRecord:
            return self.CurrentRecord
        else:
            self.CurrentRecord=self.ExtendedRecord.ReadExtention(self.GradeElement.GradeRecord.Name,self.GradeElement.GradeSubject.Name)
            return self.CurrentRecord
    def Exam(self):
        Exam1=False
        Exam2=False
        Exam3=False
        if 'Exam1' in self.Current():
            if self.Current['Exam1']:
                Exam1=self.Current['Exam1']
        if 'Exam2' in self.Current:
            if self.Current['Exam2']:
                Exam2=self.Current['Exam2']
        if 'Exam3' in self.Current:
            if self.Current['Exam3']:
                Exam3=self.Current['Exam3'] 
        if Exam1 and Exam2 and Exam3:
            ExamTotal=numpy.sum[Exam1,Exam2,Exam3]
            if Subject =='Mathematics':
                return numpy.around([ExamTotal/2.2],1)
            else:
                return numpy.around([Examtotal/3],1)
        elif Exam1 and Exam2:
            return numpy.mean([Exam1,Exam2])
        elif Exam1:
            return Exam1
        else:
            return False
    
    def YearAverage(self):
        if self.Averages:
            if 'Year' in self.Averages:
                return self.Averages['Year']
            else:
                return False
        else:
            return False
            
    def SetAverage(self):
        pupilset=self.__GetPupilSet__()
        if pupilset and self.Averages:
            if pupilset in self.Averages:
                return self.Averages[pupilset]
            else:
                return False
        else:
            return False
    def __GetPupilSet__(self):
        # A report Name is required to find a set Average and thus a set
        if not self.ReportName:
            return False
        if not self.Averages:
            return False
        setGroup=PupilGroup.objects.filter(Pupil__id=self.PupilId,
                                           Group__Name__icontains='%s.Set' % self.GradeElement.SubjectGrade.Name,
                                           Active=True,AcademicYear=self.ReportYear)
        if setGroup.exists():
            return setGroup[0].Group.Name.split('.')[-1]
        else:
            SubSubjects=ParentSubjects.objects.filter(Subject__Name=Subject)
            if SubSubjects.exists():
                SetRecord=PupilGroup.objects.filter(Pupil__id=self.PupilId,
                                            Group__Name__icontains=SubSubjects[0].ParentSubject.Name,
                                            Active=True,AcademicYear=self.AcYear)
                if SetRecord.exists():
                    return SetRecord[0].split('.')[-1]
                else:
                    return False
            else:
                return False
    
class GradeRecordObject():
    def __init__(self,PupilId,gradeRecord,ReportYear=None):
        self.ReportYear=ReportYear
        if not ReportYear:
            self.ReportYear=GlobalVariablesFunctions.Get_SystemVar('CurrentYear')
        self.PupilId=PupilId
        self.GradeRecord=gradeRecord
        self.GradeData={}
        self.CatData={}
        
    def Grades(self):
        if not self.GradeData:
            GradeInputElements=GradeInputElement.objects.filter(GradeRecord__Name=self.GradeRecord)
            for inputElement in GradeInputElements:
                self.GradeData[inputElement.GradeSubject.Name]=SingleGradeRecord(self.PupilId,inputElement,self.ReportYear,self.GradeRecord)
        return self.GradeData
    def CatGrades(self):
        if not self.CatData:
            CatRec='%s_Ca' % self.GradeRecord.split('_')[0]
            ExRecord=ExtendedRecord('Pupil',self.PupilId)
            self.CatData=ExRecord.ReadExtention(CatRec,'Form_Comment')
            
                
class PupilGradeRecord(PupilAttendanceRec):
    def __init__(self,PupilId,AcYear,ReportName=None):
        PupilAttendanceRec.__init__(self,PupilId,AcYear)
        self.ReportName=ReportName
        self.ReportYear=None
        self.ReportAcYear=None
        self.__getPupilYear__()
        self.TeacherList={}
        if self.ReportName:
            self.__getReportYear__()
            self.__ReportAcYear__()
        self.Grades={}
        GradeInputElements=GradeInputElement.objects.all()
        for recs in GradeInputElements:
            if not recs.GradeRecord.Name in self.Grades:
                self.Grades[recs.GradeRecord.Name]=GradeRecordObject(self.PupilId,recs.GradeRecord.Name,self.ReportYear)

        
    def __getReportYear__(self):
        if self.ReportName:
            if 'Yr' in self.ReportName:
                self.ReportYear=int(self.ReportName.split('Yr',1)[1].split('_',1)[0])
            
    def __getPupilYear__(self):
        YearGroup=PupilGroup.objects.filter(Pupil__id=self.PupilId,AcademicYear=self.AcYear, Group__Name__icontains='year').filter(Group__Name__icontains='Form')[0]
        self.PupilYear=int(YearGroup.Group.Name.split('Year',1)[1].split('.',1)[0])
        return self.PupilYear
    def __ReportAcYear__(self):
        self.ReportAcYear=None
        pupilyear=self.PupilYear
        years=self.AcYear.split('-')
        yearsDiff=pupilyear-self.ReportYear
        self.ReportAcYear='%d-%d' % (int(years[0])-yearsDiff,int(years[1])-yearsDiff)
        
    def TeacherNames(self):
        ''' This gets a list of All teachers assigned to a child groups. It works on the
        basis of getting the Teachers assigned to the form groups first, then searches
        the other groups a child belongs to. These other groups will override the teacher
        set in the form class'''
        if not self.TeacherList:
            Formlist=PupilGroup.objects.filer(Group__Name__icontains='form.',Pupil__id=self.PupilId,AcademicYear=self.ReportAcYear)
            for groups in FormList:
                names=GlobalVariableFunctions.TeacherNames(groups.Name,self.ReportAcYear)
                self.TeacherList.update(names.SubjectTeachers())
                if names.FormTeacher():
                    self.TeacherList['FormTeacher']=names.FormTeacher()
                if names.AllTeachers():
                    self.TeacherList['AllTeachers']=names.AllTeachers()
                if names.Tutor():
                    self.TeacherList['Tutor']=names.Tutor()
            SetList=PupilGroup.objects.filter(Group__Name__icontains='Set',Pupil__id=self.PupilId,AcademicYear=self.ReportAcYear)
            for groups in SetList:
                names=GlobalVariableFunctions.TeacherNames(groups.Name,self.ReportAcYear)
                self.SubjectTeacherList.update(names.SubjectTeachers())
        return self.SubjectTeacherList
            
                

        
        
            