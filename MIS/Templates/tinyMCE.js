
tinyMCE.init({
{% load static from staticfiles %}
	mode : "textareas",
	theme : "advanced",
	plugins : "emotions,spellchecker,advhr,insertdatetime,preview,paste,advlist", 
			
	// Theme options - button# indicated the row# only
	theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,|,cut,copy,paste,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,insertdate,inserttime,|,spellchecker,advhr,,removeformat,|,sub,sup,|,emotions",
	theme_advanced_toolbar_location : "bottom",
	theme_advanced_toolbar_align : "center",
	theme_advanced_statusbar_location : "none",
	theme_advanced_resizing : false,
	paste_text_sticky : true,
	setup : function(ed) {ed.onInit.add(function(ed) { ed.pasteAsPlainText = true;});}
});
