
//Define data here : 
{% load static from staticfiles %}
var jsonp = {{ DataObj.JSONData|safe }}
//Calculate the result here: 
$(document).ready(function() {
	var objData = $.parseJSON(jsonp);
	FilterData(objData, -1, -1, -1, -1);
	//Copy booker to 1st golfer:
    $(".gender").change(function () {
    	var iGender = $("#frmReport input[name='chkGender']:checked").val();
    	var iSen = $("#frmReport input[name='chkSen']:checked").val();
    	var iGt = $("#frmReport input[name='chkgt']:checked").val();
    	var iMoreable = $("#frmReport input[name='chkMoreAble']:checked").val();
    	console.log(iGender + '-' + iSen + '-' + iGt + '-' + iMoreable);
    	FilterData(objData, iGender, iSen, iGt, iMoreable);
    });
    $(".sen").change(function () {
    	var iGender = $("#frmReport input[name='chkGender']:checked").val();
    	var iSen = $("#frmReport input[name='chkSen']:checked").val();
    	var iGt = $("#frmReport input[name='chkgt']:checked").val();
    	var iMoreable = $("#frmReport input[name='chkMoreAble']:checked").val();
    	console.log(iGender + '-' + iSen + '-' + iGt + '-' + iMoreable);
    	FilterData(objData, iGender, iSen, iGt, iMoreable);
    });
    $(".ckgt").change(function () {
    	var iGender = $("#frmReport input[name='chkGender']:checked").val();
    	var iSen = $("#frmReport input[name='chkSen']:checked").val();
    	var iGt = $("#frmReport input[name='chkgt']:checked").val();
    	var iMoreable = $("#frmReport input[name='chkMoreAble']:checked").val();
    	console.log(iGender + '-' + iSen + '-' + iGt + '-' + iMoreable);
    	FilterData(objData, iGender, iSen, iGt, iMoreable);
    });
    $(".moreable").change(function () {
    	var iGender = $("#frmReport input[name='chkGender']:checked").val();
    	var iSen = $("#frmReport input[name='chkSen']:checked").val();
    	var iGt = $("#frmReport input[name='chkgt']:checked").val();
    	var iMoreable = $("#frmReport input[name='chkMoreAble']:checked").val();
    	console.log(iGender + '-' + iSen + '-' + iGt + '-' + iMoreable);
    	FilterData(objData, iGender, iSen, iGt, iMoreable);
    });
    
    $("#btnreset").click(function () {
    	$("#frmReport input[name='chkGender']")[0].checked = true;
    	$("#frmReport input[name='chkSen']")[0].checked = true;
    	$("#frmReport input[name='chkgt']")[0].checked = true;
    	$("#frmReport input[name='chkMoreAble']")[0].checked = true;
    	var iGender = $("#frmReport input[name='chkGender']:checked").val();
    	var iSen = $("#frmReport input[name='chkSen']:checked").val();
    	var iGt = $("#frmReport input[name='chkgt']:checked").val();
    	var iMoreable = $("#frmReport input[name='chkMoreAble']:checked").val();
    	console.log(iGender + '-' + iSen + '-' + iGt + '-' + iMoreable);
    	FilterData(objData, iGender, iSen, iGt, iMoreable);
    });
});


function FilterData(objData, iGender, iSen, iGt, iMoreable)
{
	var content = "";
	
	$('#tblResult >tbody').html("");
	var arrResult = [];
	//Init the data: 
	var objRow = new Object();
	
	objRow.key = 'W';
	objRow.year = 0;
	objRow.ks6 = 0;
	objRow.ke6 = 0;
	objRow.kn6 = 0;
	arrResult.push(objRow);
	/*
	objRow = new Object();
	objRow.key = 'N/F';
	objRow.year = 0;
	objRow.ks6 = 0;
	objRow.ke6 = 0;
	objRow.kn6 = 0;
	arrResult.push(objRow);
	*/
	for(var i=1;i<=8;i++)
	{
		var objRow = new Object();
		objRow.key = i +'c';
		objRow.year = 0;
		objRow.Class1 = 0;
		objRow.Class2 = 0;
		objRow.Class3 = 0;
		objRow.Class4 = 0;
		objRow.Class5 = 0;
		objRow.Class6 = 0;
		arrResult.push(objRow);
		var objRow = new Object();
		objRow.key = i +'b';
		objRow.year = 0;
		objRow.Class1 = 0;
		objRow.Class2 = 0;
		objRow.Class3 = 0;
		objRow.Class4 = 0;
		objRow.Class5 = 0;
		objRow.Class6 = 0;
		arrResult.push(objRow);
		var objRow = new Object();
		objRow.key = i +'a';
		objRow.year = 0;
		objRow.Class1 = 0;
		objRow.Class2 = 0;
		objRow.Class3 = 0;
		objRow.Class4 = 0;
		objRow.Class5 = 0;
		objRow.Class6 = 0;
		arrResult.push(objRow);
	}
	//Make the data
	var total = 0;
	$.each(objData, function (i, item) {
		//Filter here:
		var isValid = true;
		if(iGender == 1 && item['Gender'] == 'F')
		{
			isValid = false;
		}
		if(iGender == 0 && item['Gender'] == 'M')
		{
			isValid = false;
		}
		if(iSen == 1 && item['Gender'] == 'F')
		{
			isValid = false;
		}
		if(iSen == 0 && item['Gender'] == 'T')
		{
			isValid = false;
		}
		if(iGt == 1 && item['G&T'] == 'F')
		{
			isValid = false;
		}
		if(iGt == 0 && item['G&T'] == 'T')
		{
			isValid = false;
		}
		if(iMoreable == 1 && item['MoreAble'] == 'F')
		{
			isValid = false;
		}
		if(iMoreable == 0 && item['MoreAble'] == 'T')
		{
			isValid = false;
		}

		if(isValid)
		{
			$.each(item.Level, function (j, subitem) {
				
				var key = '';
				var objRow = new Object();
				if ( subitem.hasOwnProperty('Level') ) {
					key = subitem['Level'];
		        }
				/*var curObj = getById(key, arrResult) ;
				if( curObj == null )
				{
					objRow.key = key;
					objRow.year = 1;
					if( item['Class'] == '6KS')
					{
						objRow.ks6 = 1;
					}
					if( item['Class'] == '6KE')
					{
						objRow.ke6 = 1;
					}
					if( item['Class'] == '6KN')
					{
						objRow.kn6 = 1;
					}
					arrResult.push(objRow);
				}		
				else
				{
				}
				*/
				//Update case here : 
				$.each(arrResult, function (k, item1) {
					if(item1.key == key)
					{
						item1.year = item1.year + 1;
						//Calculate total: 
						total = total + 1;
						if( item['Class'] == '{{DataObj.ClassList.0}}')
						{
							item1.Class1 = item1.Class1 + 1; 
						}	
						if( item['Class'] == '{{DataObj.ClassList.1}}')
						{
							item1.Class2 = item1.Class2 + 1; 
						}	
						if( item['Class'] == '{{DataObj.ClassList.2}}')
						{
							item1.Class3 = item1.Class3 + 1; 
						}
						if( item['Class'] == '{{DataObj.ClassList.3}}')
						{
							item1.Class4 = item1.Class4 + 1; 
						}
						if( item['Class'] == '{{DataObj.ClassList.4}}')
						{
							item1.Class5 = item1.Class5 + 1; 
						}
						if( item['Class'] == '{{DataObj.ClassList.5}}')
						{
							item1.Class6 = item1.Class6 + 1; 
						}	
					}
				});
			});
		}
	});

	console.log(arrResult);
	$.each(arrResult, function (i, item) {
		content += '<tr>';
		content += '<td bgcolor="#F7F8F9"><b>' + item['key'] + '</b></td>';
		if(item['year'] > 0)
		{
			var percent = (item['year']/total)*100;
			var round = 2;
			if(percent == 100)
				round = 0;
			content += '<td>' + percent.toFixed(round) + '</td>';
		}
		else
		{
			content += '<td bgcolor="#AAAAAA">0.00</td>';
		}
		if(item['ks6'] > 0)
		{
			var percent = (item['ks6']/item['year'])*100;
			var round = 2;
			if(percent == 100)
				round = 0;
			content += '<td>' + percent.toFixed(round) + '</td>';
		}
		else
		{
			content += '<td bgcolor="#AAAAAA">0.00</td>';
		}
		if(item['ke6'] > 0)
		{
			var percent = (item['ke6']/item['year'])*100;
			var round = 2;
			if(percent == 100)
				round = 0;
			content += '<td>' + percent.toFixed(round) + '</td>';
		}
		else
		{
			content += '<td bgcolor="#AAAAAA">0.00</td>';
		}
		if(item['kn6'] > 0)
		{
			var percent = (item['kn6']/item['year'])*100;
			var round = 2;
			if(percent == 100)
				round = 0;
			content += '<td>' + percent.toFixed(round) + '</td>';
		}
		else
		{
			content += '<td bgcolor="#AAAAAA">0.00</td>';
		}
		content += '</tr>';
    });
	//set session data: 
    $('#tblResult >tbody').html(content); 
}
function getById(id, myArray) {
    return myArray.filter(function(obj) {
      if(obj.key == id) {
        return obj; 
      }
    })[0];
}


