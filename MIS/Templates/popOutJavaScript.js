$(document).ready(function() {
	$.each( $( 'img[updated]') , function(){
		updated = new Date( $(this).attr('updated') );
		today = new Date();
		diff =  Math.floor(( today - updated ) / 86400000);
		newdate = $(this).attr('updated').split( '/' );
		newdate = newdate[2] +'/' + newdate[1] + '/' + newdate[0];
		if( diff <= 10 ){
			$(this).before('<div class="manage-updated"><img src="https:///willowtree-storage.s3.amazonaws.com/static/images/manage-updated.png" title="Updated ' + newdate + '"></div>');
		}		
	});
});

$(document).ready(function() {
{% load static from staticfiles %}
	$('a.login-window').click(function() {
		
        //Getting the variable's value from a link 
		var loginBox = $(this).attr('href');

		//Fade in the Popup
		$(loginBox).fadeIn(300);
		
		//Set the center alignment padding + border see css style
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});
	
	// When clicking on the button close or the mask layer the popup closed
	$('button.close2').on('click', function() { 
	  $('#mask , .login-popup').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});
});

$(document).ready(function() {
	$('button.login-window2').click(function() {
		
        //Getting the variable's value from a link 
		var loginBox = $(this).attr('id');

		//Fade in the Popup
		$(loginBox).fadeIn(300);
		
		//Set the center alignment padding + border see css style
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});
	
	// When clicking on the button close or the mask layer the popup closed
	$('button.close2').on('click', function() { 
	  $('#mask , .login-popup').fadeOut(300 , function() {
		$('#mask').remove();  
	}); 
	return false;
	});
});

$(document).ready(function() {
	$('button.login-window3').click(function() {
		
        //Getting the variable's value from a link 
		var loginBox = $(this).attr('id');

		//Fade in the Popup
		$(loginBox).fadeIn(300);
		
		//Set the center alignment padding + border see css style
		var popMargTop = ($(loginBox).height() + 24) / 2; 
		var popMargLeft = ($(loginBox).width() + 24) / 2; 
		
		$(loginBox).css({ 
			'margin-top' : -popMargTop,
			'margin-left' : -popMargLeft
		});
		
		// Add the mask to body
		$('body').append('<div id="mask"></div>');
		$('#mask').fadeIn(300);
		
		return false;
	});
	
	// When clicking on the button close or the mask layer the popup closed
	$('button.close3').on('click', function() { 
	  $('.login-popup2').fadeOut(300); 
	return false;
	});
});

// submit button code
function hideSubmitButton(submitButtonId)     
	{
	var submitButtonObj = document.getElementById(submitButtonId);
	submitButtonObj.style.display = "none";
	var processingDisplayId = submitButtonId.substring(0, submitButtonId.length - 1);
	processingDisplayId = processingDisplayId+'PD';
	var processingDisplayObj = document.getElementById(processingDisplayId);
	processingDisplayObj.style.display = "inline";
	}
	
function showSubmitButton2(submitButtonId)
	{
	var submitButtonObj = document.getElementById(submitButtonId);
	submitButtonObj.style.display = "inline";
	var processingDisplayId = submitButtonId.substring(0, submitButtonId.length - 1);
	processingDisplayId = processingDisplayId+'PD';
	var processingDisplayObj = document.getElementById(processingDisplayId);
	processingDisplayObj.style.display = "none";
	}