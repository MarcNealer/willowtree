
//Define data here : 
{% load static from staticfiles %}
var jsonp = {{ DataObj.JSONData|safe }}
//Calculate the result here: 
$(document).ready(function() {
	var objData = $.parseJSON(jsonp);
	CreateFilter(objData);
	
	//Copy booker to 1st golfer:
    $(".gender").change(function () {
    	CreateFilter(objData);
    });
    $(".sen").change(function () {
    	CreateFilter(objData);
    });
    $(".ckgt").change(function () {
    	CreateFilter(objData);
    });
    $(".moreable").change(function () {
    	CreateFilter(objData);
    });
    $("#btnreset").click(function () {
    	$("#frmReport input[name='chkGender']")[0].checked = true;
    	$("#frmReport input[name='chkSen']")[0].checked = true;
    	$("#frmReport input[name='chkgt']")[0].checked = true;
    	$("#frmReport input[name='chkMoreAble']")[0].checked = true;
    	$('#sltClass').val("");
    	CreateFilter(objData);
    });

    //Show golfers for toId
    $('#sltClass').change(function () {
        CreateFilter(objData);	
    });

});
function CreateFilter(objData)
{
	var iGender = $("#frmReport input[name='chkGender']:checked").val();
	var iSen = $("#frmReport input[name='chkSen']:checked").val();
	var iGt = $("#frmReport input[name='chkgt']:checked").val();
	var iMoreable = $("#frmReport input[name='chkMoreAble']:checked").val();
	var sClass = $('#sltClass').val();
	console.log(iGender + '-' + iSen + '-' + iGt + '-' + iMoreable + '-' + sClass);
	FilterData(objData, iGender, iSen, iGt, iMoreable, sClass);
}

function FilterData(objData, iGender, iSen, iGt, iMoreable, sClass)
{
	var content = "";
	$('#tblResult >tbody').html("");
	var arrResult = [];
	//Make the data
	$.each(objData, function (i, item) {
		//Filter here:
		var isValid = true;
		if(iGender == 1 && item['Gender'] == 'F')
		{
			isValid = false;
		}
		if(iGender == 0 && item['Gender'] == 'M')
		{
			isValid = false;
		}
		if(iSen == 1 && item['Gender'] == 'F')
		{
			isValid = false;
		}
		if(iSen == 0 && item['Gender'] == 'T')
		{
			isValid = false;
		}
		if(iGt == 1 && item['G&T'] == 'F')
		{
			isValid = false;
		}
		if(iGt == 0 && item['G&T'] == 'T')
		{
			isValid = false;
		}
		if(iMoreable == 1 && item['MoreAble'] == 'F')
		{
			isValid = false;
		}
		if(iMoreable == 0 && item['MoreAble'] == 'T')
		{
			isValid = false;
		}
		if(iMoreable == 1 && item['SumBirth'] == 'F')
		{
			isValid = false;
		}
		if(iMoreable == 0 && item['SumBirth'] == 'T')
		{
			isValid = false;
		}
		//Filter by class:
		if(sClass != "")
		{
			if(sClass != item['Class'])
			{
				isValid = false;
			}
		}
		
		if(isValid)
		{
			var objRow = new Object();
			objRow.forename = item['Fullname'];
			objRow.surename = '';
			objRow.gender = item['Gender'];
			objRow.cls = item['Class'];
			objRow.ls = '';
			if(item['SEN'] == 'T')
			{
				objRow.ls += 'SEN,';
			}	
			if(item['EAL'] == 'T')
			{
				objRow.ls += 'EAL4,';
			}	
			if(item['G&T'] == 'T')
			{
				objRow.ls += 'G&T,';
			}	
			if(item['MoreAble'] == 'T')
			{
				objRow.ls += 'MoreAble,';
			}
			if(item['SumBirth'] == 'T')
			{
				objRow.ls += 'SumBirth,';
			}	
			//Process replace end , character:
			
			objRow.forename = item['Fullname'];
			{% for recs in DataObj.RecordList %}
			objRow.{{recs}} = '';
			{% endfor %}
			$.each(item.Level, function (j, subitem) {
				var key = '';
				{% for recs in DataObj.RecordList %}
				if ( subitem.hasOwnProperty('{{recs}}') ) {
					objRow.{{recs}} = subitem['{{recs}}'];
				}
				{% endfor %}
			});
			arrResult.push(objRow);
		}
	});

	console.log(arrResult);

	$.each(arrResult, function (i, item) {
		content += '<tr>';
		content += '<td>' + item.forename + '</td>';
		content += '<td>' + item.surename + '</td>';
		content += '<td>' + item.gender + '</td>';
		content += '<td>' + item.cls + '</td>';
		content += '<td>' + item.ls + '</td>';
		{% for recs in DataObj.RecordList %}
		content += '<td>' + item.{{recs}} + '</td>';
		{% endfor %}
		content += '</tr>';
    });
	//set session data: 
    $('#tblResult >tbody').html(content);
}

function getById(id, myArray) {
    return myArray.filter(function(obj) {
      if(obj.key == id) {
        return obj; 
      }
    })[0];
}


