# Python imports
import simplejson

# Django imports
from django.shortcuts import HttpResponse
from django.http import HttpResponseBadRequest
from django.core import serializers
from django.shortcuts import render_to_response
from django.template import loader, RequestContext

from MIS.models import *
from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules.AnalysisReports.AnalysisReportsNew import *

def AnalysisStoreList(request,school,AcYear,GroupName):
    '''
    Gets a list of Analysis Reports in the Analysis store
    '''
    try:
        itemlist=AnalysisStore.objects.filter(ReportGroup__Name__icontains=GroupName,AcademicYear=AcYear).order_by('ReportType','ReportSubject')
        data=serializers.serialize("json", itemlist,indent=2, use_natural_keys=True)
        return HttpResponse(data, content_type='application/json')
    except:
        return HttpResponseBadRequest()

def AnalysisStoreAcYears(request):
    try:
        YearList=sorted(set([x.AcademicYear for x in AnalysisStore.objects.all()]))
        return HttpResponse(simplejson.dumps(YearList),content_type='application/json')
    except:
        return HttpResponseBadRequest()

def AnalysisStoreGroups(request,AcYear,school):
    try:
        GroupList=AnalysisStore.objects.filter(ReportGroup__Name__icontains=school,
                                               AcademicYear=AcYear)
        GroupNames=sorted(set([x.ReportGroup.Name for x in GroupList]))
        return HttpResponse(simplejson.dumps(GroupNames),content_type='application/json')
    except:
        return HttpResponseBadRequest()

def AnalysisStoreMain(request,school,AcYear):
    c={'school':school,'AcYear':AcYear}
    return render_to_response('Reports/AnalysisStoreMain.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def UpdateReport(request, school, AcYear, ReportId):
    try:
        ReportRecord=AnalysisStore.objects.get(id=int(ReportId))
        ReportSchool=ReportRecord.ReportGroup.Name.split('.')[1]
        ReportSchoolName='%sAdmin' % ReportSchool
        ReportUpdate=AnalysisReportUpdate(ReportRecord.AcademicYear,ReportSchool,ReportSchoolName,
                                           ReportRecord.ReportGroup.Name,ReportRecord.ReportTerm,
                                           ReportRecord.ReportType,ReportRecord.ReportSubject)
        ReportUpdate.run()
        return HttpResponse('%s %s %s %s' % (ReportId, ReportRecord.AcademicYear,ReportSchool,ReportSchoolName))
    except Exception as Error:
        return HttpResponseBadRequest(Error)