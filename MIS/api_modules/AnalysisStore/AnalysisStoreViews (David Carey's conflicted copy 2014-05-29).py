# Python imports
import simplejson

# Django imports
from django.shortcuts import HttpResponse
from django.http import HttpResponseBadRequest
from django.core import serializers

def GetList(request):
    try:
        search_results=AnalysisStore.objects.all()
        if not 'None' in request.POST['GroupName']:
            search_results.filter(GroupName=request.POST['GroupName'])
        if not 'None' in request.POST['Subject']:
            search_results.filter(ReportSubject__Name=request.POST['Subject'])
        if not 'None' in request.POST['AcYear']:
            search_results.filter(AcademicYear=request.POST['AcYear'])
        if not 'None' in request.POST['ReportType']:
            search_results.filter(ReportType=request.POST['ReportType'])
        return_data = serializers.serialize('json', search_results, indent=2)
        return HttpResponse(return_data, content_type='application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)

def GetGroupList(request,school):
    try:
        recordList=AnalysisStore.objects.all()
        grouplist=set([x.ReportGroup.Name for x in recordList.filter(ReportGroup__Name__icontains=school)])
        subjectList=set([x.Subject.Name for x in recordList])
        reportTypeList=set([x.ReportType for x in recordList])
        return_data = simplejson.dumps({'GroupList':sorted(grouplist),
                                       'SubjectList':sorted(subjectList),
                                       'ReportTypeList':sorted(reportTypeList)})
        return HttpResponse(return_data, content_type='application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)

def UpdateReport(request,reportid):
    try:
        report=AnalysisStoreobjects.filter(id=int(reportid))

    except Exception as error:
        return HttpResponseBadRequest(error)