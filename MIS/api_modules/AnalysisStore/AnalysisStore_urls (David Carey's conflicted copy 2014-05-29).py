# Django imports
from django.conf.urls import patterns, url

# BlueBird imports
from MIS.api_modules.AnalysisStore import AnalysisStoreViews
from MIS.ViewExtras import SchoolAuth


def grade_api_urls():
    grade_api_patterns = patterns('',
                                  url(r'^analysis/store/api/subjects/getlist/$',
                                      SchoolAuth(AnalysisStoreViews.GetList)),
                                  url(r'^analysis/store/api/grouplist/(?P<school>[^/]+)/$',
                                      SchoolAuth(GradeAPIViews.list_all_possible_groups)),
                                  url(r'^analysis/store/api/graderecord/all/$',
                                      SchoolAuth(GradeAPIViews.grade_record_all)),
                                  )
    return grade_api_patterns
