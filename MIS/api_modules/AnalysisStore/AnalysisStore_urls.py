# Django imports
from django.conf.urls import patterns, url

# BlueBird imports
from MIS.api_modules.AnalysisStore import AnalysisStoreViews
from MIS.ViewExtras import SchoolAuth, APIAuth


def AnalysisStore_urls():
    grade_api_patterns = patterns('',
                                  url(r'^analysis_store/academic_year/list/$',
                                      APIAuth(AnalysisStoreViews.AnalysisStoreAcYears)),
                                  url(r'^analysis_store/groups/list/(?P<school>[^/]+)/(?P<AcYear>[^/]+)/$',
                                      APIAuth(AnalysisStoreViews.AnalysisStoreGroups)),
                                  url(r'^analysis_store/list/(?P<school>[^/]+)/(?P<AcYear>[^/]+)/(?P<GroupName>[^/]+)/$',
                                      APIAuth(AnalysisStoreViews.AnalysisStoreList)),
                                  url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/analysis_store/$',
                                      SchoolAuth(AnalysisStoreViews.AnalysisStoreMain)),
                                  url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/analysis_store/update/(?P<ReportId>\d+)/$',
                                      APIAuth(AnalysisStoreViews.UpdateReport)),
                                  )
    return grade_api_patterns
