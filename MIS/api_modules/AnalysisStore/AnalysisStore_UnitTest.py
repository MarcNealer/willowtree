import sys
import os
import pickle
import simplejson

sys.path.append('/home/login/Dropbox/Projects/')
sys.path.append('/home/login/Dropbox/Projects/WillowTree/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings
import unittest
from django.test import Client
from MIS.models import *
from MIS.api_modules.AnalysisStore import *

from django.test import TestCase

class AnalysisStoreTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.client.login(username='mnealer ', password='one two three')
        return

    def test_academic_year_list(self):
        """
        grade_input_field_new view tests
        """
        resp=self.client.get('/analysis_store/academic_year/list/')
        self.assertEqual(resp.status_code,200)
        print resp.status_code
        #print resp.content
        self.assertContains(resp,'2013-2014')
        return

    def test_group_list(self):
        """
        grade_input_field_new view tests
        """
        resp=self.client.get('/analysis_store/groups/list/battersea/2013-2014')
        self.assertEqual(resp.status_code,200)
        print resp.status_code
        #print resp.content
        self.assertContains(resp,'Current.Battersea')
        return

    def test_group_list(self):
        """
        grade_input_field_new view tests
        """
        resp=self.client.get('/analysis_store/list/battersea/2013-2014/Current.Battersea.MiddleSchool.Year3/')
        self.assertEqual(resp.status_code,200)
        print resp.status_code
        #print resp.content
        self.assertContains(resp,'LevelsPercentage')
        return


