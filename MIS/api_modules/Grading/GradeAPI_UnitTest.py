import sys
import os
import simplejson

sys.path.append('/home/login/Dropbox/Projects/')
sys.path.append('/home/login/Dropbox/Projects/WillowTree/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings
import unittest
from django.test import Client

# WillowTree imports
from MIS.models import PupilGroup
from MIS.models import SystemVarData
from MIS.models import ExtentionRecords
from MIS.models import GradeInputFields


class GradeAPITests(unittest.TestCase):
    def setUp(self):
        self.ac_year = SystemVarData.objects.get(Variable__Name='CurrentYear').Data
        self.client = Client()
        self.client.login(username='royroy21', password='football101')
        return

    def puts_test_grade_input_field_data(self):
        """
        puts test grade input field data into WillowTree
        """
        # first entry
        post_data = {'GradeRecord': 'Yr1_Eot_M',
                     'GradeSubject': 'Mathematics',
                     'School': 'default'}
        self.client.post('/gradeapi/gradeinputfield/new/', post_data)
        existing_grade_input_fields_object_id = GradeInputFields.objects.get(GradeRecord__Name="Yr1_Eot_M",
                                                                             GradeSubject="Mathematics",
                                                                             School="default").id
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'Mathematics',
                     'School': 'default',
                     'GradeField': 'Level'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'Mathematics',
                     'School': 'default',
                     'GradeField': 'Attainment'}

        # second entry
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        post_data = {'GradeRecord': 'Yr2_Eot_M',
                     'GradeSubject': 'Mathematics',
                     'School': 'default'}
        self.client.post('/gradeapi/gradeinputfield/new/', post_data)
        existing_grade_input_fields_object_id = GradeInputFields.objects.get(GradeRecord__Name="Yr2_Eot_M",
                                                                             GradeSubject="Mathematics",
                                                                             School="default").id
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'Mathematics',
                     'School': 'default',
                     'GradeField': 'Level'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)

    def test_subject_list(self):
        """
        subject_list tests
        """
        response = self.client.get('/gradeapi/subjects/all/')
        self.assertEqual(simplejson.loads(response.content)[0]['fields']['Name'] == 'Art', True)
        return

    def test_list_all_possible_groups(self):
        """
        list_all_possible_groups tests
        """
        for school in ['Battersea', 'CLAPHAM', 'fulHAM', 'KenSingTon']:
            response = self.client.get('/gradeapi/grouplist/%s/' % school)
            for group in simplejson.loads(response.content):
                self.assertEqual(PupilGroup.objects.filter(Group__Name=group,
                                                           AcademicYear=self.ac_year,
                                                           Active=True).exists(), True)
        return

    def test_get_grade_records_for_group(self):
        """
        get_grade_records_for_group tests
        """
        response = self.client.get('/gradeapi/graderecord/all/')
        self.assertEqual(len(ExtentionRecords.objects.all()) == len(simplejson.loads(response.content)), True)
        return

    def test_get_grade_data_for_group(self):
        """
        get_grade_data_for_group tests
        """
        self.puts_test_grade_input_field_data()
        response = self.client.get('/gradeapi/gradedata/Yr1_Eot_M/Mathematics/Current.Battersea.LowerSchool.Year1.Form.1BE/')
        print "---> %s" % response.content
        self.assertEqual(len(GradeInputFields.objects.filter(GradeSubject="Mathematics",
                                                             GradeRecord__Name="Yr1_Eot_M",
                                                             School="default")) == 1, True)
        return

    def tearDown(self):
        pass

if __name__ == '__main__':
    unittest.main()