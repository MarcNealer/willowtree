# Django imports
from django.conf.urls import patterns, url

# BlueBird imports
from MIS.api_modules.Grading import GradeAPIViews
from MIS.ViewExtras import SchoolAuth


def grade_api_urls():
    pats = patterns('',
                    url(r'^gradeapi/subjects/all/$',
                        SchoolAuth(GradeAPIViews.subject_list)),
                    url(r'^gradeapi/grouplist/(?P<school>[^/]+)/$',
                        SchoolAuth(GradeAPIViews.list_all_possible_groups)),
                    url(r'^gradeapi/graderecord/all/$',
                        SchoolAuth(GradeAPIViews.grade_record_all)),
                    url(r'^gradeapi/gradedata/(?P<grade_record>[^/]+)/(?P<grade_subject>[^/]+)/(?P<group>[^/]+)/$',
                        SchoolAuth(GradeAPIViews.get_grade_records_for_group)),
                    url(r'^gradeapi/pupilgrades/(?P<PupilId>\d+)/$',
                        SchoolAuth(GradeAPIViews.getAllGradesForAPupil)),
                    )
    return pats