# Python imports
import pickle
import simplejson

# WillowTree imports
from MIS.models import PupilGroup
from MIS.models import Pupil
from MIS.models import SystemVarData
from MIS.models import ExtentionData
from MIS.models import ExtentionFields
from MIS.models import GradeInputFields


class GetGradeDataForAGroup():
    """
    Gets all grade data for a group of pupils
    """
    def __init__(self, grade_record, grade_subject, group_name, school="default"):
        self.ac_year = SystemVarData.objects.get(Variable__Name='CurrentYear').Data
        self.grade_record = grade_record
        self.grade_subject = grade_subject
        self.group_name = group_name
        self.school = school
        get_all_pupils_method = self.__get_all_pupils__()
        self.pupil_id_list = get_all_pupils_method[0]
        self.pupil_object_list = get_all_pupils_method[1]
        self.raw_grade_data = self.__get_all_grade_data__()


    def __get_all_pupils__(self):
        """
        Gets all pupils needed for operation
        """
        pupil_id_list = PupilGroup.objects.filter(Group__Name__icontains=self.group_name,
                                                  AcademicYear=self.ac_year,
                                                  Active=True).values_list('Pupil__id', flat=True).distinct()
        pupil_object_list = Pupil.objects.filter(id__in=pupil_id_list)
        return [pupil_id_list, pupil_object_list]

    def __get_all_grade_data__(self):
        """
        Gets all grade data needed for operation
        """
        raw_grade_data = ExtentionData.objects.filter(BaseType="Pupil",
                                                      BaseId__in=self.pupil_id_list,
                                                      Subject__Name=self.grade_subject,
                                                      ExtentionRecord__Name=self.grade_record,
                                                      Active=True)
        return raw_grade_data


    def __get_all_the_grade_input_fields__(self):
        """
        Gets all the grade input fields in a list
        """
        input_list = []
        unpicked_list = pickle.loads(GradeInputFields.objects.get(GradeSubject=self.grade_subject,
                                                                  GradeRecord__Name=self.grade_record,
                                                                  School=self.school).FieldList)
        for i in unpicked_list:
            input_list.append(i['name'])
        return input_list


    def __compile_data__(self):
        """
        Compiles pupil and raw grade data
        """
        return_data = []
        input_list = self.__get_all_the_grade_input_fields__()
        for pupil in self.pupil_object_list:
            grade_data_dictionary = dict()
            raw_grade_data = self.raw_grade_data.filter(BaseId=pupil.id)
            per_pupil_data = {"PupilId": pupil.id,
                              "PupilName": "%s %s" % (pupil.Forename, pupil.Surname),
                              "PictureURL": pupil.Picture.url}

            # get grade input fields needed
            grade_data_list = []
            for grade_data in raw_grade_data:
                for key, value in grade_data.Data.iteritems():
                    if key in input_list:
                        grade_data_dictionary = {"GradeData": {"FieldName": key,
                                                               "FieldType": ExtentionFields.objects.get(Name=key).Type,
                                                               "FieldData": value[0],
                                                               "PickList": eval(ExtentionFields.objects.get(Name=key).PickList.Data)}}
                grade_data_list.append(grade_data_dictionary)
            per_pupil_data.update({"GradeData": grade_data_list})
            return_data.append(per_pupil_data)
        return return_data

    def get_data(self):
        """
        returns a json object of all grade data for a group
        """
        return simplejson.dumps(self.__compile_data__())