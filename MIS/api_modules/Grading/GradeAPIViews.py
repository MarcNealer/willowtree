# Python imports
import simplejson

# Django imports
from django.shortcuts import HttpResponse
from django.http import HttpResponseBadRequest
from django.core import serializers

# WillowTree imports
from MIS.models import Subject
from MIS.models import PupilGroup
from MIS.models import SystemVarData
from MIS.models import ExtentionRecords
from MIS.models import ExtentionData
from MIS.api_modules.Grading.GradeAPIFunctions import GetGradeDataForAGroup


def subject_list(request):
    """
    returns all subjects
    """
    try:
        data = serializers.serialize("json",
                                     Subject.objects.all(),
                                     indent=2,
                                     use_natural_keys=True)
        return HttpResponse(data, mimetype='application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def list_all_possible_groups(request, school):
    """
    returns all current groups that have members for a school
    """
    try:
        ac_year = SystemVarData.objects.get(Variable__Name='CurrentYear').Data
        school_variable = school.lower().capitalize()

        # query
        groups = PupilGroup.objects.filter(Group__Name__icontains="Current.%s." % school_variable,
                                           AcademicYear=ac_year,
                                           Active=True).exclude(Group__Name__icontains=".House.")
        groups = groups.distinct('Group__Name').values_list('Group__Name', flat=True)

        return HttpResponse(simplejson.dumps(list(groups)), mimetype='application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def grade_record_all(request):
    """
    returns a serialised ExtentionRecords json object
    """
    try:
        data = serializers.serialize("json",
                                     ExtentionRecords.objects.all(),
                                     indent=2,
                                     use_natural_keys=True)
        return HttpResponse(data, mimetype='application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def get_grade_records_for_group(request, grade_record, grade_subject, group):
    """
    Gets all grade data for a group of pupils
    """
    test = True
    if test:
        grade_records = GetGradeDataForAGroup(grade_record, grade_subject, group).get_data()
        return HttpResponse(grade_records, mimetype='application/json')
    else:
        try:
            grade_records = GetGradeDataForAGroup(grade_record, grade_subject, group).get_data()
            return HttpResponse(grade_records, mimetype='application/json')
        except Exception as error:
            return HttpResponseBadRequest(error)

def getAllGradesForAPupil(request, PupilId):
    try:
        reclist=ExtentionData.objects.filter(BaseType='Pupil',BaseId=int(PupilId),ExtentionRecord__Name__istartswith='Yr',
                                                Active=True)
        datalist=[]
        for items in reclist:
            datalist.append({'Record':items.ExtentionRecord.Name,'Subject':items.Subject.Name,
                             'Data':items.Data})
        return HttpResponse(simplejson.dumps(datalist), mimetype='application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)