# Python imports
import simplejson

# Django imports
from django.shortcuts import HttpResponse
from django.http import HttpResponseBadRequest

# WillowTree imports
from MIS.api_modules.GradeInputFields.GradeInputFieldFunctions import GradeInputField


def grade_input_field_new(request):
    """
    Creates a new GradeInputFields record with a subject
    """
    try:
        grade_input_field_object = GradeInputField(request)
        grade_input_field_object.new()
        return HttpResponse(simplejson.dumps({'message': grade_input_field_object.message}),
                            'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def grade_input_field_add(request):
    """
    Adds a grade field to an existing GradeInputFields record
    """
    try:
        grade_input_field_object = GradeInputField(request)
        grade_input_field_object.add_field()
        return HttpResponse(simplejson.dumps({'message': grade_input_field_object.message}),
                            'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def grade_input_field_all(request):
    """
    Returns the GradeInputFields table in json format
    """
    try:
        grade_input_field_object = GradeInputField(request)
        return HttpResponse(grade_input_field_object.all(), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def grade_input_field_get(request, grade_input_field_id):
    """
    Returns the GradeInputFields table in json format based upon a GradeInputField id
    """
    try:
        grade_input_field_object = GradeInputField(request)
        return HttpResponse(grade_input_field_object.get(grade_input_field_id), 'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def grade_input_field_move_up(request):
    """
    Moves a grade field up the list of the pickled list in FieldList
    """
    try:
        grade_input_field_object = GradeInputField(request)
        grade_input_field_object.move("up")
        return HttpResponse(simplejson.dumps({'message': grade_input_field_object.message}),
                            'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def grade_input_field_move_down(request):
    """
    Moves a grade field down the list of the pickled list in FieldList
    """
    try:
        grade_input_field_object = GradeInputField(request)
        grade_input_field_object.move("down")
        return HttpResponse(simplejson.dumps({'message': grade_input_field_object.message}),
                            'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def grade_input_field_remove(request):
    """
    Removes a grade field from the pickled list in FieldList
    """
    try:
        grade_input_field_object = GradeInputField(request)
        grade_input_field_object.remove()
        return HttpResponse(simplejson.dumps({'message': grade_input_field_object.message}),
                            'application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)