# python imports
import pickle
import simplejson
from operator import itemgetter

# WillowTree imports
from MIS.models import GradeInputFields
from MIS.models import ExtentionRecords
from MIS.models import ExtentionFields


class GradeInputField():
    def __init__(self, request):
        self.request = request
        self.message = False

    def new(self):
        """
        Creates a new GradeInputFields record with a subject
        """
        grade_record = self.request.POST['GradeRecord']
        grade_subject = self.request.POST['GradeSubject']
        grade_school = self.request.POST['School']

        # check if GradeSubjectOrder record already exists
        if GradeInputFields.objects.filter(GradeSubject=grade_subject,
                                           GradeRecord__Name=grade_record,
                                           School=grade_school).exists():
            self.message = "ERROR: GradeInputFields record already exists"
            return
        else:

            # create new GradeSubjectOrder object
            existing_grade_subject = ExtentionRecords.objects.get(Name=grade_record)
            new_grade_input_fields_object = GradeInputFields(GradeSubject=grade_subject,
                                                             GradeRecord=existing_grade_subject,
                                                             School=grade_school,
                                                             FieldList=pickle.dumps([]))
            new_grade_input_fields_object.save()
            self.message = "SUCCESS: GradeInputFields record saved"
            return

    def all(self):
        """
        Returns the GradeInputFields table in json format
        """
        return_list = list()
        for entry in GradeInputFields.objects.all():
            temp_dictionary = {'id': entry.id,
                               'subject': entry.GradeSubject,
                               'grade_record': entry.GradeRecord.Name,
                               'field_list': pickle.loads(entry.FieldList)}
            return_list.append(temp_dictionary)
        return simplejson.dumps(return_list)

    def get(self, grade_input_field_id):
        """
        Returns the GradeInputFields table in json format based upon a GradeInputField id
        """
        entry = GradeInputFields.objects.get(id=grade_input_field_id)
        temp_dictionary = {'id': entry.id,
                           'subject': entry.GradeSubject,
                           'grade_record': entry.GradeRecord.Name,
                           'field_list': pickle.loads(entry.FieldList)}
        return simplejson.dumps(temp_dictionary)

    def add_field(self):
        """
        Adds a grade field to an existing GradeInputFields record
        """
        grade_record_id = self.request.POST['GradeInputFieldId']
        grade_field = self.request.POST['GradeField']
        if GradeInputFields.objects.filter(id=grade_record_id).exists():
            grade_input_field_object = GradeInputFields.objects.get(id=grade_record_id)
            grade_field_object = ExtentionFields.objects.get(Name=grade_field)
            if grade_field_object.PickList:
                pick_list = grade_field_object.PickList.Data
            else:
                pick_list = 'None'

            # if grade field is not present in FieldList will append to the end
            field_list_object = pickle.loads(grade_input_field_object.FieldList)
            amount_in_list = map(itemgetter('name'), field_list_object).count(grade_field)
            if amount_in_list is not 0:
                self.message = "ERROR: Grade field could not be added"
                return
            else:
                field_list_object.append({'id': grade_field_object.id,
                                         'name': grade_field_object.Name,
                                         'type': grade_field_object.Type,
                                         'picklist': pick_list})
                grade_input_field_object.FieldList = pickle.dumps(field_list_object)
                grade_input_field_object.save()
                self.message = "SUCCESS: Grade field added"
                return
        else:
            self.message = "ERROR: Grade field could not be added"
            return

    def move(self, up_down):
        """
        Moves a grade field up or down the list of the pickled list in FieldList
        """
        grade_record_id = self.request.POST['GradeInputFieldId']
        grade_field = self.request.POST['GradeField']
        grade_input_field = GradeInputFields.objects.get(id=grade_record_id)
        field_list = pickle.loads(grade_input_field.FieldList)
        index_of_to_be_moved = map(itemgetter('name'), field_list).index(grade_field)
        value_of_to_be_moved_up = field_list[index_of_to_be_moved]
        field_list.remove(value_of_to_be_moved_up)
        if up_down.lower() == "up":
            if index_of_to_be_moved > 0:
                field_list.insert(index_of_to_be_moved - 1, value_of_to_be_moved_up)
            else:
                self.message = "WARNING: Grade field already top of the list!"
                return
            grade_input_field.FieldList = pickle.dumps(field_list)
            grade_input_field.save()
            self.message = "SUCCESS: Grade field moved up"
            return
        elif up_down.lower() == "down":
            if index_of_to_be_moved == (len(field_list) - 1):
                field_list.insert(index_of_to_be_moved + 1, value_of_to_be_moved_up)
            else:
                self.message = "WARNING: Grade field already bottom of the list!"
                return
            grade_input_field.FieldList = pickle.dumps(field_list)
            grade_input_field.save()
            self.message = "SUCCESS: Grade field moved down"
            return
        else:
            self.message = "ERROR: up_down variable incorrectly defined"
            return

    def remove(self):
        """
        Removes a grade field from the pickled list in FieldList
        """
        grade_record_id = self.request.POST['GradeInputFieldId']
        grade_field = self.request.POST['GradeField']
        grade_input_field = GradeInputFields.objects.get(id=grade_record_id)
        field_list = pickle.loads(grade_input_field.FieldList)
        try:
            index_of_to_be_removed = map(itemgetter('name'), field_list).index(grade_field)
            value_of_to_be_removed = field_list[index_of_to_be_removed]
            field_list.remove(value_of_to_be_removed)
            grade_input_field.FieldList = pickle.dumps(field_list)
            grade_input_field.save()
            self.message = "SUCCESS: Grade field removed"
            return
        except ValueError as error:
            self.message = "ERROR: %s" % error
            return