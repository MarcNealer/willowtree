# Django imports
from django.conf.urls import patterns, url

# BlueBird imports
from MIS.api_modules.GradeInputFields import GradeInputFieldViews
from MIS.ViewExtras import SchoolAuth


def grade_input_field_urls():
    grade_input_field_patterns = patterns('',
                                          url(r'^gradeapi/gradeinputfield/new/$',
                                              SchoolAuth(GradeInputFieldViews.grade_input_field_new)),
                                          url(r'^gradeapi/gradeinputfield/all/$',
                                              SchoolAuth(GradeInputFieldViews.grade_input_field_all)),
                                          url(r'^gradeapi/gradeinputfield/addfield/$',
                                              SchoolAuth(GradeInputFieldViews.grade_input_field_add)),
                                          url(r'^gradeapi/gradeinputfield/get/(?P<grade_input_field_id>[^/]+)/$',
                                              SchoolAuth(GradeInputFieldViews.grade_input_field_get)),
                                          url(r'^gradeapi/gradeinputfield/moveup/$',
                                              SchoolAuth(GradeInputFieldViews.grade_input_field_move_up)),
                                          url(r'^gradeapi/gradeinputfield/movedown/$',
                                              SchoolAuth(GradeInputFieldViews.grade_input_field_move_down)),
                                          url(r'^gradeapi/gradeinputfield/remove/$',
                                              SchoolAuth(GradeInputFieldViews.grade_input_field_remove)),
                                          )
    return grade_input_field_patterns