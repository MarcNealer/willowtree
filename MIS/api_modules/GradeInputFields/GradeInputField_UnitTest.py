import sys
import os
import pickle
import simplejson

sys.path.append('/home/login/Dropbox/Projects/')
sys.path.append('/home/login/Dropbox/Projects/WillowTree/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings
import unittest
from django.test import Client
from MIS.models import GradeInputFields


class GradeInputFieldTests(unittest.TestCase):
    def setUp(self):
        self.client = Client()
        self.client.login(username='royroy21', password='football101')
        return

    def create_test_data(self):
        """
        Creates test data for use in tests
        """
        post_data = {'GradeRecord': 'Yr8_Eot_M',
                     'GradeSubject': 'French',
                     'School': 'Default'}
        response = self.client.post('/gradeapi/gradeinputfield/new/', post_data)
        return response

    def test_grade_input_field_new(self):
        """
        grade_input_field_new view tests
        """

        # creates a new GradeSubjectOrder object
        response = self.create_test_data()
        self.assertEqual('{"message": "SUCCESS: GradeInputFields record saved"}' in response.content, True)
        new_grade_input_fields_object = GradeInputFields.objects.filter(GradeRecord__Name="Yr8_Eot_M",
                                                                        GradeSubject="French",
                                                                        School="Default")
        self.assertEqual(len(new_grade_input_fields_object) == 1, True)

        # creates a duplicate GradeSubjectOrder object which the system should reject
        response = self.create_test_data()
        self.assertEqual('{"message": "ERROR: GradeInputFields record already exists"}' in response.content, True)
        new_grade_input_fields_object = GradeInputFields.objects.filter(GradeRecord__Name="Yr8_Eot_M",
                                                                        GradeSubject="French",
                                                                        School="Default")
        self.assertEqual(len(new_grade_input_fields_object) == 1, True)
        return

    def test_grade_input_field_add(self):
        """
        grade_input_field_add view tests
        """
        self.create_test_data()

        # adds a grade field to an existing GradeInputFields record
        existing_grade_input_fields_object_id = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                             GradeSubject="French",
                                                                             School="Default").id
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam1'}
        response = self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        self.assertEqual('{"message": "SUCCESS: Grade field added"}' in response.content, True)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[0]['name'] == "Exam1",
                         True)

        # adds another grade field to an existing GradeInputFields record
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam2'}
        response = self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        self.assertEqual('{"message": "SUCCESS: Grade field added"}' in response.content, True)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[1]['name'] == "Exam2",
                         True)

        # adds duplicate grade field to an existing GradeInputFields record
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam2'}
        response = self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        self.assertEqual('{"message": "ERROR: Grade field could not be added"}' in response.content, True)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(len(pickle.loads(existing_grade_input_fields_object.FieldList)) == 2,
                         True)
        return

    def test_grade_input_field_all(self):
        """
        grade_input_field_all view tests
        """

        # creating test data
        self.create_test_data()
        existing_grade_input_fields_object_id = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                             GradeSubject="French",
                                                                             School="Default").id
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam1'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam2'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)

        # main test logic
        response = self.client.get('/gradeapi/gradeinputfield/all/')
        response_content_as_dictionary = simplejson.loads(response.content)
        self.assertEqual(response_content_as_dictionary[0]['id'] == existing_grade_input_fields_object_id, True)
        self.assertEqual(response_content_as_dictionary[0]['subject'] == "French", True)
        self.assertEqual(response_content_as_dictionary[0]['grade_record'] == "Yr8_Eot_M", True)
        self.assertEqual(response_content_as_dictionary[0]['field_list'][0]['name'] == "Exam1", True)
        self.assertEqual(response_content_as_dictionary[0]['field_list'][1]['name'] == "Exam2", True)
        return

    def test_grade_input_field_get(self):
        """
        grade_input_field_get view tests
        """

        # creating test data
        self.create_test_data()
        existing_grade_input_fields_object_id = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                             GradeSubject="French",
                                                                             School="Default").id
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam1'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam2'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)

        # main test logic
        response = self.client.get('/gradeapi/gradeinputfield/get/%s/' % existing_grade_input_fields_object_id)
        response_content_as_dictionary = simplejson.loads(response.content)
        self.assertEqual(response_content_as_dictionary['id'] == existing_grade_input_fields_object_id, True)
        self.assertEqual(response_content_as_dictionary['subject'] == "French", True)
        self.assertEqual(response_content_as_dictionary['grade_record'] == "Yr8_Eot_M", True)
        self.assertEqual(response_content_as_dictionary['field_list'][0]['name'] == "Exam1", True)
        self.assertEqual(response_content_as_dictionary['field_list'][1]['name'] == "Exam2", True)
        return

    def test_grade_input_field_move_up(self):
        """
        grade_input_field_move_up view tests
        """

        # creating test data
        self.create_test_data()
        existing_grade_input_fields_object_id = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                             GradeSubject="French",
                                                                             School="Default").id
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam1'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam2'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[1]['name'] == "Exam2", True)
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[0]['name'] == "Exam1", True)

        # main test logic 1
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam1'}
        response = self.client.post('/gradeapi/gradeinputfield/moveup/', post_data)
        self.assertEqual('{"message": "WARNING: Grade field already top of the list!"}' in response.content, True)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[0]['name'] == "Exam1", True)
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[1]['name'] == "Exam2", True)

        # main test logic 2
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam2'}
        response = self.client.post('/gradeapi/gradeinputfield/moveup/', post_data)
        self.assertEqual('{"message": "SUCCESS: Grade field moved up"}' in response.content, True)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[0]['name'] == "Exam2", True)
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[1]['name'] == "Exam1", True)
        return

    def test_grade_input_field_move_down(self):
        """
        grade_input_field_move_down view tests
        """

        # creating test data
        self.create_test_data()
        existing_grade_input_fields_object_id = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                             GradeSubject="French",
                                                                             School="Default").id
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam1'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam2'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[1]['name'] == "Exam2", True)
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[0]['name'] == "Exam1", True)

        # main test logic 1
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam2'}
        response = self.client.post('/gradeapi/gradeinputfield/movedown/', post_data)
        self.assertEqual('{"message": "WARNING: Grade field already bottom of the list!"}' in response.content, True)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[0]['name'] == "Exam1", True)
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[1]['name'] == "Exam2", True)

        # main test logic 2
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam1'}
        response = self.client.post('/gradeapi/gradeinputfield/movedown/', post_data)
        self.assertEqual('{"message": "SUCCESS: Grade field moved down"}' in response.content, True)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[1]['name'] == "Exam1", True)
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[0]['name'] == "Exam2", True)
        return

    def test_grade_input_field_remove(self):
        """
        grade_input_field_remove view tests
        """

        # creating test data
        self.create_test_data()
        existing_grade_input_fields_object_id = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                             GradeSubject="French",
                                                                             School="Default").id
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam1'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam2'}
        self.client.post('/gradeapi/gradeinputfield/addfield/', post_data)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[1]['name'] == "Exam2", True)
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[0]['name'] == "Exam1", True)

        # main test logic 1
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam1'}
        response = self.client.post('/gradeapi/gradeinputfield/remove/', post_data)
        self.assertEqual('{"message": "SUCCESS: Grade field removed"}' in response.content, True)
        existing_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                          GradeSubject="French",
                                                                          School="Default")
        self.assertEqual(pickle.loads(existing_grade_input_fields_object.FieldList)[0]['name'] == "Exam2", True)
        self.assertEqual(len(pickle.loads(existing_grade_input_fields_object.FieldList)) == 1, True)

        # main test logic 2
        post_data = {'GradeInputFieldId': existing_grade_input_fields_object_id,
                     'GradeSubject': 'French',
                     'School': 'Default',
                     'GradeField': 'Exam3'}
        response = self.client.post('/gradeapi/gradeinputfield/remove/', post_data)
        self.assertEqual('ERROR' in response.content, True)

    def tearDown(self):
        new_grade_input_fields_object = GradeInputFields.objects.get(GradeRecord__Name="Yr8_Eot_M",
                                                                     GradeSubject="French",
                                                                     School="Default")
        new_grade_input_fields_object.delete()

if __name__ == '__main__':
    unittest.main()
