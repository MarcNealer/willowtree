from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
import os
import simplejson




@csrf_exempt
def isLoggedIn(request):
    if request.user.is_authenticated() and 'UserData' in request.session:
        data=simplejson.dumps({'surname':request.session['UserData']['Surname'],
                               'forename':request.session['UserData']['Forename'],
                               'username':request.user.username,
                               'groups':request.session['UserData']['Groups']})
        return HttpResponse(data, mimetype='application/json')
    elif request.user.is_authenticated():
        request.session['UserData']=baseSessionData(request.user.username)
        data=simplejson.dumps({'surname':request.session['UserData']['Surname'],
                               'forename':request.session['UserData']['Forename'],
                               'username':request.user.username,
                               'groups':request.session['UserData']['Groups']})
        return HttpResponse(data, mimetype='application/json')
    else:
        return HttpResponseForbidden()

#  # # # # # # # # # # # # # # # # #  # # # # #
#  Log user into the system
# # # # # # # # # # # # # # # # # # # # # # # #
@csrf_exempt
def Authenticate(request):
    ''' This view logs you onto the system '''
    c={}
    c.update(csrf(request))
    try:
        username = request.POST['username']
        password = request.POST['password']
    except:
        HttpResponseBadRequest()
    user = authenticate(username=username, password=password)
    if user is not None:
        # save userid and password in session
        # get user name from WillowTree
        # Get staff groups from WillowTree and store in session
        login(request, user)
        request.session['UserData']=buildSessionData(username, password)
        data=simplejson.dumps({'surname':request.session['UserData']['Surname'],
                               'forename':request.session['UserData']['Forename'],
                               'username':request.user.username,
                               'groups':request.session['UserData']['Groups']})
        return HttpResponse(data, mimetype='application/json')
    else:
        return HttpResponseForbidden()
@csrf_exempt
def Logout_View(request):
    logout(request)
