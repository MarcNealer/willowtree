__author__ = 'marc'
# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.core.context_processors import csrf
from django.http import *
import logging
log = logging.getLogger(__name__)


def loadHTML(request,appname,htmlfile):
    return render_to_response('%s/%s' % (appname,htmlfile),{},context_instance=RequestContext(request))
