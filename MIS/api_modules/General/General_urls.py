__author__ = 'marc'
# Django imports
from django.conf.urls import patterns, url
from MIS.ViewExtras import SchoolAuth, APIAuth
from MIS.api_modules.General import General

def General_urls():
    api_patterns = patterns('',
                            url(r'^loadhtml/(?P<appname>[^/]+)/(?P<htmlfile>[^/]+)/$',APIAuth(General.loadHTML)))
    return api_patterns