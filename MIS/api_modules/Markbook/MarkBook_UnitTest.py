import sys
import os
import pickle
import simplejson

sys.path.append('/home/login/Dropbox/Projects/')
sys.path.append('/home/login/Dropbox/Projects/WillowTree/')
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings
import unittest
from django.test import Client
from MIS.models import *


from django.test import TestCase
from django.contrib.auth.models import User

class MarkBookTests(TestCase):
    def setUp(self):
        self.client = Client()
        self.client.login(username='mnealer', password='one two three')
        self.AcYear='2013-2014'
        newMarkbook=TeacherMarkBook(BookName='test1_MarkBook_for_Battersea',
                                    BookGroup=Group.objects.get(Name='Current.Battersea'),
                                    AcademicYear=self.AcYear,
                                    BookData=simplejson.dumps([[1,2,3,4,4,5,6],['q','a','d','c','g','h','f']]),
                                    UpdatedBy=Staff.objects.get(User=User.objects.get(username='mnealer')))
        newMarkbook.save()
        self.testMarkBook=newMarkbook.id
        return

    def test_list_markbooks(self):
        """
        grade_input_field_new view tests
        """
        resp=self.client.get('/markbook/list/Current.Battersea/2013-2014/')
        self.assertEqual(resp.status_code,200)
        self.assertContains(resp,'test1_MarkBook_for_Battersea')
        return

    def test_get_MarkBook(self):
        """
        grade_input_field_new view tests
        """
        resp=self.client.get('/markbook/read/%d/' % self.testMarkBook)
        self.assertEqual(resp.status_code,200)
        self.assertContains(resp,'test1_MarkBook_for_Battersea')
        return

    def test_create_MarkBook(self):
        """
        grade_input_field_new view tests
        """
        postdata={'GroupName':'Current.Battersea.LowerSchool.Year1.Form.1BE',
                  'AcYear':'2013-2014','BookName':'Testx'}
        resp=self.client.post('/markbook/create/', postdata)
        self.assertEqual(resp.status_code,200)
        return

    def test_write_MarkBook(self):
        """
        grade_input_field_new view tests
        """
        postdata={'bookid':self.testMarkBook,
                  'Data':simplejson.dumps([['test','','',''],['test1','','']])}
        resp=self.client.post('/markbook/write/', postdata)
        self.assertEqual(resp.status_code,200)
        print resp
        return
    def test_delete_MarkBook(self):
        postdata={'bookid':self.testMarkBook}
        resp=self.client.post('/markbook/remove/', postdata)
        self.assertEqual(resp.status_code,200)
        return

if __name__ == '__main__':
    unittest.main()
