# Django imports
from django.conf.urls import patterns, url

# BlueBird imports
from MIS.api_modules.Markbook import MarkbookViews
from MIS.ViewExtras import SchoolAuth, APIAuth


def MarkBook_urls():
    grade_api_patterns = patterns('',
                                  url(r'^markbook/list/(?P<Group>[^/]+)/(?P<AcYear>[^/]+)/$', APIAuth(MarkbookViews.getMarkBooks)),
                                  url(r'^markbook/read/(?P<BookId>\d+)/$', APIAuth(MarkbookViews.readMarkBook)),
                                  url(r'^markbook/write/$', APIAuth(MarkbookViews.writeMarkBook)),
                                  url(r'^markbook/remove/$',APIAuth(MarkbookViews.deleteMarkBook)),
                                  url(r'^markbook/create/$',APIAuth(MarkbookViews.createMarkbook)),
                                  url(r'^(?P<school>[^/]+)/(?P<AcYear>[^/]+)/markbook/start/(?P<GroupName>[^/]+)/$',SchoolAuth(MarkbookViews.StartMarkBook)),
                                  )
    return grade_api_patterns
