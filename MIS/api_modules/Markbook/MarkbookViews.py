
# Django imports
from django.shortcuts import HttpResponse
from django.http import HttpResponseBadRequest
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.core import serializers

from MIS.models import *
from MIS.ViewExtras import SetMenu, SetLogo
import datetime
import simplejson

def StartMarkBook(request, AcYear, school,GroupName):
    try:
        c={'GroupName':GroupName,'AcYear':AcYear,'school':school}
        return render_to_response('MarkBook.html',c,
                                  context_instance=RequestContext(request,processors=[SetMenu]))
    except:
        return HttpResponseBadRequest()

def readMarkBook(request, BookId):
    try:
        itemlist=TeacherMarkBook.objects.filter(id=int(BookId))
        data=serializers.serialize("json", itemlist,indent=2, use_natural_keys=True)
        return HttpResponse(data, content_type='application/json')
    except:
        return HttpResponseBadRequest()

def writeMarkBook(request):
    try:
        bookitem=TeacherMarkBook.objects.get(id=int(request.POST['bookid']))
        bookitem.BookData=request.POST['Data']
        bookitem.UpdatedOn=datetime.datetime.now()
        bookitem.UpdatedBy=Staff.objects.get(User=request.user)
        bookitem.save()
        data=serializers.serialize("json",[bookitem],indent=2, use_natural_keys=True)
        return HttpResponse(data, content_type='application/json')
    except:
        return HttpResponseBadRequest()


def deleteMarkBook(request):
    try:
        bookitem=TeacherMarkBook.objects.get(id=int(request.POST['bookid']))
        bookitem.Active=False
        bookitem.save()
        return HttpResponse()
    except:
        return HttpResponseBadRequest()

def createMarkbook(request):
    try:
        group=request.POST['GroupName']
        datagroup=Group.objects.get(Name=group)
        AcYear=request.POST['AcYear']
        if TeacherMarkBook.objects.filter(BookName=request.POST['BookName'],
                                          BookGroup__Name__iexact=group,
                                          AcademicYear=AcYear,Active=True).exists():
            return HttpResponseBadRequest('Exists Already')
        datafield=[]
        pupillist=[]
        for pupils in PupilGroup.objects.filter(AcademicYear=AcYear,Group__Name__istartswith=group,Active=True):
            if not pupils.Pupil.id in pupillist:
                datafield.append([pupils.Pupil.id, pupils.Pupil.FirstName(),pupils.Pupil.Surname,'','','','','','','',''])
                pupillist.append(pupils.Pupil.id)
        datafield.sort(key=lambda x: x[2])
        datafield.insert(0,['Id','Forename','Surname','','','','','','','',''])
        newMarkBook=TeacherMarkBook(BookName=request.POST['BookName'],
                                    BookGroup=datagroup,
                                    AcademicYear=AcYear,
                                    BookData=simplejson.dumps(datafield),
                                    UpdatedBy=Staff.objects.get(User=request.user))
        newMarkBook.save()
        data=serializers.serialize("json", [newMarkBook],indent=2, use_natural_keys=True)
        return HttpResponse(data, content_type='application/json')
    except Exception as error:
        return HttpResponseBadRequest(error)


def getMarkBooks(request,AcYear,Group):
    try:
        itemlist=TeacherMarkBook.objects.filter(Active=True,
                                                BookGroup__Name__iexact=Group,
                                                AcademicYear__iexact=AcYear)
        data=serializers.serialize("json", itemlist,indent=2, use_natural_keys=True)
        return HttpResponse(data, content_type='application/json')
    except:
        return HttpResponseBadRequest()

