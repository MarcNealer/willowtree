# -*- coding: utf-8 -*-
"""
Created on Fri Jan 18 19:14:56 2013

@author: dbmgr
"""
# myapp/api.py
from tastypie.resources import ModelResource, Resource
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from MIS.models import *
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from tastypie import fields
from tastypie.authentication import BasicAuthentication
from tastypie.cache import SimpleCache
from MIS.modules.ExtendedRecords import *
from TimeTables.PrintTimeTablesFunctions import *
from django.template.loader import render_to_string
from django.contrib.auth import authenticate, login, logout
from tastypie.http import HttpUnauthorized, HttpForbidden
from django.conf.urls import url
from tastypie.utils import trailing_slash


AcYear=GlobalVariables.Get_SystemVar('CurrentYear')

# for groupCallFive api
from MIS.modules.GroupCall.groupCallFiveScripts import GroupCallDataExtractAmended
import urlparse
from tastypie.serializers import Serializer

class urlencodeSerializer(Serializer):
    formats = ['json', 'jsonp', 'xml', 'yaml', 'html', 'plist', 'urlencode']
    content_types = {
        'json': 'application/json',
        'jsonp': 'text/javascript',
        'xml': 'application/xml',
        'yaml': 'text/yaml',
        'html': 'text/html',
        'plist': 'application/x-plist',
        'urlencode': 'application/x-www-form-urlencoded',
        }
    def from_urlencode(self, data,options=None):
        """ handles basic formencoded url posts """
        qs = dict((k, v if len(v)>1 else v[0] )
            for k, v in urlparse.parse_qs(data).iteritems())
        return qs

    def to_urlencode(self,content):
        pass

class AddressResource(ModelResource):
    class Meta:
        queryset=Address.objects.all()
        resource_name='address'
        filtering = {
            "HomeSalutation":ALL,
            "PostalTitle": ALL,
            "AddressLine1": ALL,
            "AddressLine2": ALL,
            "AddressLine3": ALL,
            "AddressLine4": ALL,
            "PostCode": ALL,
            "Phone1" : ALL,
            "Phone2": ALL,
            "Phone3": ALL,
            "Active": ALL,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

class UserResource(ModelResource):
    class Meta:
        queryset=User.objects.all()
        resource_name='user'
        filtering = {
            "username":ALL,
        }
        excludes = ['password', 'is_active', 'is_staff', 'is_superuser']
        allowed_methods = ['get','post']
        authentication = BasicAuthentication()
        cache = SimpleCache()
        serializer = urlencodeSerializer()

    def override_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/login%s$" %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('login'), name="api_login"),
            url(r'^(?P<resource_name>%s)/logout%s$' %
                (self._meta.resource_name, trailing_slash()),
                self.wrap_view('logout'), name='api_logout'),
        ]


    def login(self, request, **kwargs):
        self.method_check(request, allowed=['post'])

        data = self.deserialize(request, request.body, format=request.META.get('CONTENT_TYPE', 'application/json'))

        username = data.get('username', '')
        password = data.get('password', '')

        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                login(request, user)
                staffrec=Staff.objects.get(User=user)
                return self.create_response(request, {
                    'success': True,'user':user.id, 'email':user.email,'WillowTreeId':staffrec.id
                })
            else:
                return self.create_response(request, {
                    'success': False,
                    'reason': 'disabled',
                    }, HttpForbidden )
        else:
            return self.create_response(request, {
                'success': False,
                'reason': 'incorrect',
                }, HttpUnauthorized )

    def logout(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        if request.user and request.user.is_authenticated():
            logout(request)
            return self.create_response(request, { 'success': True })
        else:
            return self.create_response(request, { 'success': False }, HttpUnauthorized)

class PupilResource(ModelResource):
    class Meta:
        queryset = Pupil.objects.filter(Active=True)
        resource_name = 'pupil'
        filtering = {
            "id" :ALL,
            "Forename" :ALL,
            "Surname" : ALL,
            "DateOfBirth" : ALL,
            "EmailAddress" : ALL,
            "Gender" : ALL,
            "Active": ALL,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

class GroupResource(ModelResource):
    class Meta:
        queryset=Group.objects.all()
        resource_name='group'
        filtering = {
            "id":   ALL,
            "MenuName": ALL,
            "Name": ALL,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

class PupilGroupResource(ModelResource):
    Pupil=fields.ForeignKey(PupilResource,'Pupil',full=True)
    Group=fields.ForeignKey(GroupResource,'Group',full=True)
    class Meta:
        queryset=PupilGroup.objects.filter(AcademicYear=AcYear,Active=True)
        resource_name='pupilgroups'
        filtering = {
            "Group": ALL_WITH_RELATIONS,
            "Pupil": ALL_WITH_RELATIONS,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

class ContactResource(ModelResource):
    class Meta:
        queryset=Contact.objects.filter(Active=True)
        resource_name='contact'
        filtering = {
            "id":   ALL,
            "Forename" :ALL,
            "Surname" : ALL,
            "DateOfBirth" : ALL,
            "EmailAddress" : ALL,
            "Gender" : ALL,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

    def dehydrate(self,Bundle):
        ExtendedData = ExtendedRecord(BaseType='Contact',
                                    BaseId=Bundle.data['id']).ReadExtention(ExtentionName='ContactExtra')
        Bundle.data['mobileNumber']=ExtendedData['mobileNumber'][0]
        return Bundle

class FamilyResource(ModelResource):
    Address = fields.ToManyField(AddressResource,'Address', full=True)
    class Meta:
        queryset=Family.objects.filter(Active=True)
        resource_name='family'
        filtering = {
            "id" : ALL,
            "FamilyName" : ALL,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

class FamilyContactResource(ModelResource):
    FamilyId=fields.ForeignKey(FamilyResource,'FamilyId',full=True)
    Contact=fields.ForeignKey(ContactResource,'Contact',full=True)
    class Meta:
        queryset=FamilyContact.objects.filter(Relationship__Type__in=['Mother','Father','OtherVLE'])
        resource_name='familycontact'
        filtering = {
            "FamilyId": ALL_WITH_RELATIONS,
            "Contact": ALL_WITH_RELATIONS,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

class FamilyChildrenResource(ModelResource):
    FamilyId=fields.ForeignKey(FamilyResource,'FamilyId',full=True)
    Pupil=fields.ForeignKey(PupilResource,'Pupil',full=True)
    class Meta:
        queryset=FamilyChildren.objects.all()
        resource_name='familychildren'
        filtering = {
            "Pupil": ALL_WITH_RELATIONS,
            "FamilyId": ALL_WITH_RELATIONS,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

class MenuResource(ModelResource):
    class Meta:
        queryset=MenuTypes.objects.filter()
        filtering = {
            'id':ALL,
            'Name':ALL
        }
        resource_name='menu'
        authentication = BasicAuthentication()
        cache = SimpleCache()
        serializer = urlencodeSerializer()


class StaffResource(ModelResource):
    User=fields.ForeignKey(UserResource,'User',full=True,null=True)
    Address=fields.ForeignKey(AddressResource,'Address',full=True,null=True)
    DefaultMenu=fields.ForeignKey(MenuResource,'DefaultMenu',full=True,null=True)
    class Meta:
        queryset=Staff.objects.filter(Active=True)
        resource_name='staff'
        filtering = {
            "id": ALL,
            "Forename" :ALL,
            "Surname" : ALL,
            "DateOfBirth" : ALL,
            "EmailAddress" : ALL,
            "Gender" : ALL,
            "Address":  ALL_WITH_RELATIONS,
            "DefaultMenu" : ALL_WITH_RELATIONS,
            "User": ALL_WITH_RELATIONS,
            "Active": ALL,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()


class StaffGroupResource(ModelResource):
    Staff=fields.ForeignKey(StaffResource,'Staff',full=True)
    Group=fields.ForeignKey(GroupResource,'Group',full=True)
    class Meta:
        queryset=StaffGroup.objects.filter(AcademicYear=AcYear)
        resource_name='staffgroup'
        filtering = {
            "Group": ALL_WITH_RELATIONS,
            "Staff": ALL_WITH_RELATIONS,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

class StaffFavResource(ModelResource):
    Staff=fields.ForeignKey(StaffResource,'Staff')
    Group=fields.ForeignKey(GroupResource,'Group',full=True)
    class Meta:
        queryset=StaffGroupFavourite.objects.all()
        resource_name='stafffavgroup'
        filtering = {
            "Group": ALL_WITH_RELATIONS,
            "Staff": ALL_WITH_RELATIONS,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

class SubjectResource(ModelResource):
    class Meta:
        queryset=Subject.objects.all()
        resource_name='Subject'
        filtering = {
            "Name": ALL,
            "Desc": ALL,
        }
        authentication = BasicAuthentication()
        allowed_methods = ['get']
        cache=SimpleCache()

###############################################################################
#                           GROUP CALL FIVE API                               #
#     groupcall login details- username: groupcall, password: morethanh*      #
###############################################################################


class Dict2obj(object):
    def __init__(self, d):
        self.data = d


class GetDataForGrpCallResourceAmended(Resource):
    data = fields.CharField(attribute='data')

    class Meta:
        resource_name = 'getData'
        authentication = BasicAuthentication()
        limit = 0

    def obj_get(self, request=None, **kwargs):
        return Dict2obj(GroupCallDataExtractAmended(kwargs['pk']).getData())

###############################################################################



class TimeTableObject(object):
    def __init__(self,TimeTable):
        if isinstance(TimeTable,PupilTimeTable):
            self.id=TimeTable.PupilId
        elif isinstance(TimeTable,StaffTimeTable):
            self.id=TimeTable.StaffId
        self.Monday=TimeTable.MondayHTML()
        self.Tuesday=TimeTable.TuesdayHTML()
        self.Wednesday=TimeTable.WednesdayHTML()
        self.Thursday=TimeTable.ThursdayHTML()
        self.Friday=TimeTable.FridayHTML()

class PupilTimeTableResource(Resource):
    Monday=fields.CharField(attribute='Monday')
    Tuesday=fields.CharField(attribute='Tuesday')
    Wednesday=fields.CharField(attribute='Wednesday')
    Thursday=fields.CharField(attribute='Thursday')
    Friday=fields.CharField(attribute='Friday')

    class Meta:
        resource_name = 'PupilTimeTable'
        object_class = TimeTableObject
        authentication = BasicAuthentication()
    def obj_get(self, request=None, **kwargs):
        return TimeTableObject(PupilTimeTable(kwargs['pk'],AcYear))
    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.id
        else:
            kwargs['pk'] = bundle_or_obj['id']

        return kwargs

class StaffTimeTableResource(Resource):
    Monday=fields.CharField(attribute='Monday')
    Tuesday=fields.CharField(attribute='Tuesday')
    Wednesday=fields.CharField(attribute='Wednesday')
    Thursday=fields.CharField(attribute='Thursday')
    Friday=fields.CharField(attribute='Friday')

    class Meta:
        resource_name = 'StaffTimeTable'
        object_class = TimeTableObject
        authentication = BasicAuthentication()
    def obj_get(self, request=None, **kwargs):
        return TimeTableObject(StaffTimeTable(kwargs['pk'],AcYear))
    def detail_uri_kwargs(self, bundle_or_obj):
        kwargs = {}

        if isinstance(bundle_or_obj, Bundle):
            kwargs['pk'] = bundle_or_obj.obj.id
        else:
            kwargs['pk'] = bundle_or_obj['id']

        return kwargs