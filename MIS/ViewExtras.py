#-------------------------------------------------------------------------------
# Name:        ViewExtras
# Purpose:     Wrappers and context proccessors used by views and URL's
#
# Author:      DBMgr
#
# Created:     16/04/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
# Base python libray imports
#
# Updated 11/04/2013-  dbmgr -- added code so that admin users have a 1 hour expirey time
import time
from operator import itemgetter

# Django middleware imports
from django.conf import settings
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.contrib.auth.models import User

# WillowTree system imports
from MIS.models import *
from MIS.modules.MenuBuilder.MenuBuilder import *
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules.resetPassword import resetPasswordViews

def APIAuth(view):
    def new_view(request, *args, **kwargs):
        school=request.META['PATH_INFO'].split('/')[1]
        if not request.user.is_authenticated():
            return HttpResponseForbidden("<h1>Unauthorised access to MIS.%s</h1><br>You do not have permission to access this area. <br>Do not contact support. Please consult with your Department head if access is required and they can contact DB Support" % school)
        return view(request, *args, **kwargs)
    return new_view

def SchoolAuth(view, allow_if_admin_menu=False):
    '''Wrapper function used in the URL.py file to check access authority when calling a page.

    By default users should have access authority to the calling school menu, i.e BatterseaTeacher etc.

    Theon on selected pages, there are extra levels of security such as updating pupil info, updating a
    Note, deleting a Note etc'''
    def new_view(request, *args, **kwargs):
        if len(request.META['PATH_INFO'].split('/')[1])> 1:
            school=request.META['PATH_INFO'].split('/')[1]
            if not request.user.is_authenticated():
                c={'school':school}
                c.update({'errmsg':'You have been logged out'})
                return render_to_response('Login1.html',c,context_instance=RequestContext(request,processors=[SetLogo]))
            elif not request.user.has_perm("MIS.%s" % school) and not request.user.is_superuser and not 'forum' in request.META['PATH_INFO'] and not 'getmedia' in request.META['PATH_INFO']:
                return HttpResponseForbidden("<h1>Unauthorised access to MIS.%s</h1><br>You do not have permission to access this area. <br>Do not contact support. Please consult with your Department head if access is required and they can contact DB Support" % school)
            else:
                if request.POST.has_key('ReqPermissons'):
                    Permissions = request.POST['ReqPermissons']
                    for permisson in Permissions:
                        if not request.user.has_perm("MIS.%s" %permisson):
                            return HttpResponseForbidden("<h1>Unauthorised access. Test 2</h1><br>You are not logged in or are trying to access a function that you don't have access right to.")
            if 'Admin' in school or 'Applicant' in school:
                request.session.set_expiry(3600)
            else:
                request.session.set_expiry(1800)
            if not 'StaffId' in request.session:
                request.session['StaffId']=Staff.objects.get(User=request.user)
        #reset password code
        userId = request.session['_auth_user_id']
        userObj = User.objects.get(id=int(userId))
        staffObj = Staff.objects.get(id=userObj.staff.id)
        if staffObj.ResetPassword == 1:
            return resetPasswordViews.newPasswordAfterReset(request, school, userId)
        #end of reset password code
        else:
            if allow_if_admin_menu:
                if 'Admin' in staffObj.DefaultMenu.Name:
                    return view(request, *args, **kwargs)
                else:
                    return HttpResponseForbidden("<h1>ACCESS DENIED: Admin only area</h1>")
            else:
                return view(request, *args, **kwargs)
    return new_view


def SetMenu(request):
    ''' Calls the MenuBuilder fucntion based upon the School Menu name asked for in the URL. If the menu
    is unknown, then a blank menu is returned, however access will not be granted to a URL of an
    Unknown Menu Name

    This function also sets the Banner logo on the pages'''

    school=request.META['PATH_INFO'].split('/')[1] 
    AcYear=request.META['PATH_INFO'].split('/')[2]

    if not 'WillowMenu' in request.session:
        AllMenu=MenuBuilder.AllGroups(school,AcYear)
        menuObject=MenuTypes.objects.get(Name=school)
        Logo=menuObject.Logo
        request.session['WillowMenu']=AllMenu
        request.session['WillowLogo']=Logo
        request.session['MenuName']=school
        request.session['School']=menuObject.SchoolId
        request.session['SchoolName']=menuObject.Name
        request.session['AcYear']=AcYear      
    elif not request.session['MenuName']==school or not request.session['AcYear']==AcYear:
        AllMenu=MenuBuilder.AllGroups(school,AcYear)
        menuObject=MenuTypes.objects.get(Name=school)
        Logo=menuObject.Logo
        request.session['WillowMenu']=AllMenu
        request.session['WillowLogo']=Logo
        request.session['School']=menuObject.SchoolId
        request.session['SchoolName']=menuObject.Name
        request.session['MenuName']=school
        request.session['AcYear']=AcYear
    else:
        AllMenu=request.session['WillowMenu']
        Logo=request.session['WillowLogo']
        
    if 'Messages' in request.session:
        Messages=request.session['Messages']
        del request.session['Messages']
    else:
        Messages=[]

    return {'AllGroupMenu':AllMenu,'Logo':'images/'+Logo,
    'UserName':request.user.username,'ActionTime':time.strftime("%H:%M",time.localtime()),
    'ActionDate':time.strftime("%d/%m/%y",time.localtime()),'LoggedIn':True,'Messages':Messages,
    'CurrentYear':GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear'),
    'Expire':request.session.get_expiry_age(),'AcYear':AcYear,'school':school,'MEDIA_REDIRECT':settings.MEDIA_REDIRECT}

def SetLogo(request):
    ''' Sets the Banner logo on the pages.
    Only used for pages where a logon is not required'''

    if len(request.META['PATH_INFO'].split('/')) > 2:
        school=request.META['PATH_INFO'].split('/')[1]
        if school=='None':
            Logo='Thomas_Logo.jpg'
        else:
            if MenuTypes.objects.filter(Name=school).exists():
                menuObject=MenuTypes.objects.get(Name=school)
                Logo=menuObject.Logo
            else:
                Logo='Thomas_Logo.jpg'                
    else:
        Logo='Thomas_Logo.jpg'
    return {'Logo':'images/'+Logo,'LoggedIn':False}