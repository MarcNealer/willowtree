#-------------------------------------------------------------------------------
# Name:        UK_Extra
# Purpose:
#
# Author:      DBMgr
#
# Created:     24/02/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python
from MIS.modules import GeneralFunctions
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules.ExtendedRecords import *

class UK_Extra():
    ''' This class is a wrapper for a number of static methods to be used as adhoc functions
    These  functions are UK education system base ones'''
    @staticmethod
    def Add_UPN(School):
        ''' UK_Extra.Add_UPN()
        This is a special static method used to bulk assign UPN Numbers.

        Basically is scans all pupils assigned to Current groups and checks to
        see if they have a UPN Number. If the don't have a number it will
        assign one.

        It uses a formula based upon a
        document TWG/04/3/3  Author Isabella Craig from the dfes
        '''
        LetterList=['A','B','C','D','E','F','G','H',
                    'J','K','L','M','N','P','Q','R',
                    'T','U','V','W','X','Y','Z']
        UPN_counter = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('%s.UPN_Counter' % School)
        AcYear = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
        for pupil_Id in GeneralFunctions.Get_PupilList(AcYear, GroupName='Current.%s' % School):
            ExRec = ExtendedRecord('Pupil',pupil_Id.id)
            if not ExRec.ReadExtention('PupilExtra')['UPN'][0]:
                lea = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('%s.LEA' % School)
                dfes = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('%s.DfES' % School)
                year = int(str(datetime.date.today().year)[2:4])
                UPNStr="%03d" % UPN_counter
                UPN_No = str(int(lea))+str(int(dfes))+str(int(year))+UPNStr
                UPN_calc=0
                for counter in range(0,12):
                    UPN_calc += (int(UPN_No[counter])*(counter+2))
                UPN_Number = LetterList[UPN_calc%23]+UPN_No
                ExRec.WriteExtention('PupilExtra', {'UPN': UPN_Number})
                UPN_counter +=1
                GlobalVariablesFunctions.GlobalVariables.Write_SystemVar('%s.UPN_Counter' % School, UPN_counter)
