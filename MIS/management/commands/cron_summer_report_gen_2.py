# imports
from django.core.management.base import BaseCommand
from MIS.modules.AnalysisReports.AnalysisReportsNew import *
from MIS.modules.globalVariables import GlobalVariablesFunctions

# import log
import logging
log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    15 01 * 6,7,8 *
    """
    def handle(self, *args, **options):
        try:
            ac_year = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
            test = AnalysisStoreCreator(ac_year, 'Summer')
            test.createYearlyReports()
            log.warn('cron_summer_report_gen_2.py successfully run')
        except Exception as error:
            log.warn('ERROR: cron_summer_report_gen_2.py failed (%s)' % error)
        return