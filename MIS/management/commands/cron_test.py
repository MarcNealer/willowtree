from django.core.management.base import BaseCommand
from MIS.models import Staff


class Command(BaseCommand):
    """
    This is a custom command created to test if this is a viable option to use with cron jobs. This command creates a
    text file on the server containing a list of staff names.
    """
    def add_arguments(self, parser):
        parser.add_argument('var', nargs='+', type=str)

    def handle(self, *args, **options):
        var_1 = args[0]
        f = open('/home/ubuntu/sites/WillowTree/file_from_cron_text.txt', 'w')
        for staff in Staff.objects.filter(Active=True):
            f.write('%s is %s\n' % (staff.FullName(), var_1))
        f.close()