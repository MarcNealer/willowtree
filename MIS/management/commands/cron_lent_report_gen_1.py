# imports
from django.core.management.base import BaseCommand
from MIS.modules.AnalysisReports.AnalysisReportsNew import *
from MIS.modules.globalVariables import GlobalVariablesFunctions

# import log
import logging
log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    15 00 * 6,7,8 *
    """
    def handle(self, *args, **options):
        try:
            ac_year = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
            test = AnalysisStoreCreator(ac_year, 'Lent')
            test.createTermReports()
            log.warn('cron_lent_gen_1.py successfully run')
        except Exception as error:
            log.warn('ERROR: cron_lent_report_gen_1.py failed: (%s)' % error)
        return