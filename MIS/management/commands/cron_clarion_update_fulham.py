# imports
from django.core.management.base import BaseCommand
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules.ThirdParty import ClarionCall

# import log
import logging
log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    20 11 * * *
    """
    ## AcYear,GroupName,msisdn,uploadCode,schoolName,uploadfile
    def handle(self, *args, **options):
        try:
            ac_year = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
            ClarionCall.ClarionCallExtract(ac_year,
                                           "Current.Fulham",
                                           "07797151759",
                                           "0122C162573B",
                                           "Thomas's Fulham",
                                           "dirsync-ICClub_337ACA3A570A540C00000122C1625754.xml")
            log.warn('cron_clarion_update_fulham.py successfully run')
        except Exception as error:
            log.warn('ERROR: cron_clarion_update_fulham.py failed (%s)' % error)
        return