# imports
from django.core.management.base import BaseCommand
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.models import *
from django.core.cache import cache

# import log
import logging
log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    30 02 * 6,7,8 *
    """
    def handle(self, *args, **options):
        try:
            ac_year = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
            for pupil_id in [i.id for i in Pupil.objects.all()]:
                try:
                    # get all active groups
                    pupil_groups = PupilGroup.objects.filter(Pupil__id=pupil_id, AcademicYear=ac_year, Active=True)

                    # determine pupil school
                    if pupil_groups.filter(Group__Name__icontains='Form').exists():

                        # set PupilGroup to False if pupil is in a house group of a another school
                        pupil_school = pupil_groups.filter(Group__Name__icontains='Form')[0].Group.Name.split('.')[1]
                        if pupil_groups.filter(Group__Name__icontains='House.House').exists():
                            for pupil_group in pupil_groups.filter(Group__Name__icontains='House.House'):
                                school_of_house = pupil_group.Group.Name.split('.')[1]
                                if school_of_house != pupil_school:
                                    pupil_group.Active = False
                                    pupil_group.save()
                                    cache.delete('Pupil_%s' % pupil_id)
                                    log.warn('SUCCESS: cron_remove_dodgy_house_pupil_groups.py removed %s from %s (%s)' % (pupil_id,
                                                                                                                           pupil_group.Group.Name,
                                                                                                                           pupil_group.AcademicYear))
                    else:

                        # set PupilGroup to False if pupil is not part of any Form group
                        if pupil_groups.filter(Group__Name__icontains='House.House').exists():
                            for pupil_group in pupil_groups.filter(Group__Name__icontains='House.House'):
                                pupil_group.Active = False
                                pupil_group.save()
                                cache.delete('Pupil_%s' % pupil_id)
                                log.warn('SUCCESS: cron_remove_dodgy_house_pupil_groups.py removed %s from %s (%s)' % (pupil_id,
                                                                                                                       pupil_group.Group.Name,
                                                                                                                       pupil_group.AcademicYear))
                except Exception as error:
                    log.warn('ERROR: cron_remove_dodgy_house_pupil_groups.py failed on pupil id: %s (%s)' % (pupil_id,
                                                                                                             error))
        except Exception as error:
            log.warn('ERROR: cron_remove_dodgy_house_pupil_groups.py failed (%s)' % error)
        return