# imports
from django.core.management.base import BaseCommand
from MIS.modules.AnalysisReports.AnalysisReportsNew import *
from MIS.UK_Extra import UK_Extra

# import log
import logging
log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    30 18 1 9 *
    """
    def handle(self, *args, **options):
        try:
            for school in ['Battersea', 'Clapham', 'Fulham', 'Kensington']:
                UK_Extra.Add_UPN(school)
            log.warn('cron_assign_upn_numbers.py successfully run')
        except Exception as error:
            log.warn('ERROR: cron_assign_upn_numbers.py failed (%s)' % error)
        return