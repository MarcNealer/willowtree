# imports
from django.core.management.base import BaseCommand
from MIS.modules.notes.BatterseaBehaviourReport import BatterseaBehaviourReport

# import log
import logging
log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    Runs BatterseaBehaviourReport.py
    """
    def handle(self, *args, **options):
        try:
            BatterseaBehaviourReport({'behaviour_keywords': ['Conduct', 'Detention'],
                                     'groups': (('Staff.Battersea.Behavior_Report.MS_PASTORAL', [3, 4, 5]),
                                               ('Staff.Battersea.Behavior_Report.MS_HOMS_PASTORAL', [6, 7, 8]),
                                               ('Staff.Battersea.Behavior_Report.MS_LT_PASTORAL', [9]),
                                               ('Staff.Battersea.Behavior_Report.US_PASTORAL', [3, 4, 5]),
                                               ('Staff.Battersea.Behavior_Report.US_HOMS_PASTORAL', [6, 7, 8]),
                                               ('Staff.Battersea.Behavior_Report.US_LT_PASTORAL', [9]))}).main_logic()
            log.warn('cron_battersea_behaviour_report.py successfully run')
        except Exception as error:
            log.warn('ERROR: failed to attach EOT reports to Clapham: %s' % error)
        return