# imports
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
from django.core.management.base import BaseCommand
from MIS.modules.AnalysisReports.AnalysisReportsNew import *
from MIS.modules.documentServer.AttachReports import AttachReport

# import log
import logging
log = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    Lent - 30 3 4 4 *
    Mich - 30 3 25 12 *
    Summer - 30 3 20 7 *

    This command attaches reports to pupil reports. Reports will not attach if one is already present.

    Usage (directly from the command line):
    python manage.py cron_attach_eot_reports Battersea M None (for Battersea Mich)
    python manage.py cron_attach_eot_reports test M None (for Battersea test group Mich)

    """
    def add_arguments(self, parser):
        parser.add_argument('term_variable', nargs='+', type=str)

    def handle(self, *args, **options):
        group_list = list()
        school = args[0]
        term = args[1]
        report_type = args[2]
        this_ac_year = GlobalVariables.Get_SystemVar('CurrentYear')

        # Fulham and Kensington
        group_list_small_school = [['Current.%s.LowerSchool.Year1' % school, 'Yr1_Eot_%s' % term, 'Default'],
                                   ['Current.%s.LowerSchool.Year2' % school, 'Yr2_Eot_%s' % term, 'Default'],
                                   ['Current.%s.PrepSchool.Year3' % school, 'Yr3_Eot_%s' % term, 'Default'],
                                   ['Current.%s.PrepSchool.Year4' % school, 'Yr4_Eot_%s' % term, 'Default'],
                                   ['Current.%s.PrepSchool.Year5' % school, 'Yr5_Eot_%s' % term, 'Default'],
                                   ['Current.%s.PrepSchool.Year6.' % school, 'Yr6_Eot_%s' % term, 'Default']]

        # Battersea and Clapham
        group_list_large_school = [['Current.%s.LowerSchool.Year1' % school, 'Yr1_Eot_%s' % term, 'Default'],
                                   ['Current.%s.LowerSchool.Year2' % school, 'Yr2_Eot_%s' % term, 'Default'],
                                   ['Current.%s.MiddleSchool.Year3' % school, 'Yr3_Eot_%s' % term, 'Default'],
                                   ['Current.%s.MiddleSchool.Year4' % school, 'Yr4_Eot_%s' % term, 'Default'],
                                   ['Current.%s.MiddleSchool.Year5' % school, 'Yr5_Eot_%s' % term, 'Default'],
                                   ['Current.%s.UpperSchool.Year6.Yr6_Sets.11Plus' % school, 'Yr6_Eot_%s' % term,
                                    '11Plus'],
                                   ['Current.%s.UpperSchool.Year6.Yr6_Sets.13Plus' % school, 'Yr6_Eot_%s' % term,
                                    '13Plus'],
                                   ['Current.%s.UpperSchool.Year7' % school, 'Yr7_Eot_%s' % term, 'Default'],
                                   ['Current.%s.UpperSchool.Year8' % school, 'Yr8_Eot_%s' % term, 'Default']]
        test_group = [['Current.Battersea.MiddleSchool.Year3.Form.3BN', 'Yr3_Eot_%s' % term, 'Default']]

        # Test school size
        if school in ['Fulham', 'Kensington']:
            group_list = group_list_small_school
        if school in ['Battersea', 'Clapham']:
            group_list = group_list_large_school
        if school == 'test':
            school = 'Battersea'
            group_list = test_group

        # Generate and attach reports
        for group in group_list:
            pupil_list = GeneralFunctions.Get_PupilList(this_ac_year, group[0])
            for pupil in pupil_list:
                if report_type is not "None":
                    report_type_here = report_type
                else:
                    report_type_here = group[2]
                attach_report = AttachReport(pupil_id=pupil.id,
                                             report_name=group[1],
                                             report_type=report_type_here,
                                             ac_year=this_ac_year,
                                             school=school)
                attach_report.run()