# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q
from SetManagerClass import *


# WillowTree system imports

from ViewExtras import SetMenu, SetLogo
from MIS.forms import *
from ModelFunctions import *

def PageNotFound(request):
    ''' PageNotFound : Used to render the page not found '''
    c={'errmsg': 'Page Not found : %s' % request.META['PATH_INFO']}
    return render_to_response('Welcome.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


# # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#   Set Management
#
# # # # # # # # # # # # # # # # # # # # # # # # # # #

# This view builds the primary set mangement page with the list of pupils
# and the radio buttons

def SetManagerNew(request,school,GroupId,GroupSets,AcYear):
    c={'SetManager':SetManagerObject(AcYear,GroupId,GroupSets),'school':school,'AcYear':AcYear}
    return render_to_response('SetManagementV2.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    

def SetManagerOld(request,school,GroupId,GroupSets,AcYear):
    ''' This view is used to show the page for changing pupils sets
    Sets consist of a manager group, and a number of set groups managed.
    These are passed as the GroupId and the GroupSets.

    The view creates a report which scans for all pupils in the parent of the Manager group, i.e
    if the group is Current.battersea.Year7.Maths, it scans for pupils in Current.battersea.Year7.

    All Pupils are set as though they are in this group.

    It then scans each of the group sets and if a child is in one of these groups, it overrides
    the default. The relevant data is than passed and displayed.
    The returned data is dealt with in UpdateSets'''
    request.session.set_expiry(1800)
    c={'school':school}
    c.update(csrf(request))
    request.session['OLD_PATH']=request.META['PATH_INFO']

    # obtains the setid's for later
    ManagerGroup=Group.objects.get(id=int(GroupId))
    GroupName=ManagerGroup.Name
    SetIdList=[[GroupId,True]]
    for groups in GroupSets.split('-'):
        SetIdList.append([groups,False])

    # Create the SetList for use in the Table Titles on the page
    SetList=[]
    for groups in SetIdList:
        Groupdetail=Group.objects.get(id=groups[0])
        SetList.append(Groupdetail.Name.split('.')[-1])

    # Create Pupil list with which set their In

    PupilList=[]
    for pupils in GroupFunctions.Get_PupilList(AcYear,GroupName='.'.join(ManagerGroup.Name.split('.')[0:-1])):
        PupilList.append([pupils,copy.deepcopy(SetIdList)])
    
    for pupils in PupilList:
        for sets in pupils[1]:
            if PupilGroup.objects.filter(Pupil=pupils[0],Group=Group.objects.get(id=sets[0]),AcademicYear=AcYear).exists():
                sets[1]=True
                pupils[1][0][1]=False
    c.update({'PupilList':PupilList,'SetList':SetList,'GroupName':GroupName,'GroupRef':GroupSets,'AcYear':AcYear,'ManagerId':SetIdList[0][0]})
    return render_to_response('SetManagement.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


# this view procceses the request sent back from the SetManagement page and moved
# the pupils into new sets

def UpdateSetsNew(request,school,GroupId,GroupSets,AcYear):
    c={'system_messages':SetManagerObject.ChangeGroups(request,AcYear,GroupId,GroupSets)}
    c.update({'SetManager':SetManagerObject(AcYear,GroupId,GroupSets),'school':school,'AcYear':AcYear})
    return render_to_response('SetManagementV2.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
     
    

def UpdateSetsOld(request,school,GroupId,GroupSets,AcYear):
    ''' Twin function to SetManager. This view proccesses the returned data, changing
    the Pupils Groups and then returns a simple page listing the Pupils changed and
    What action was taken'''

    c={'school':school}
    c.update(csrf(request))
    ListOfPupils=GroupFunctions.Get_PupilList(AcYear,GroupName='.'.join(Group.objects.get(id=GroupId).Name.split('.')[:-1]))
    ChangeList=[]
    GroupList=GroupSets.split('-')

    for eachpupil in ListOfPupils:
        if str(eachpupil.id) in request.POST:
            NewGroup= request.POST[str(eachpupil.id)]
            if NewGroup != GroupId:
                Found=False
                for groups in GroupList:
                    if PupilGroup.objects.filter(AcademicYear=AcYear,Group=Group.objects.get(id=groups),Pupil=eachpupil).exists():
                        if groups!=NewGroup:
                            p=PupilGroup.objects.get(AcademicYear=AcYear,Group=Group.objects.get(id=groups),Pupil=eachpupil).delete()
                            msg_del=GroupFunctions.RemoveFromSubGroups(groups,eachpupil,AcYear)
                            for msgs in msg_del:
                                ChangeList.append(msgs)
                            p=PupilGroup(AcademicYear=AcYear,Group=Group.objects.get(id=NewGroup),Pupil=eachpupil)
                            p.save()
                            OGD=Group.objects.get(id=int(groups))
                            NGD=Group.objects.get(id=int(NewGroup))
                            Msg=" %s %s has been moved from %s to %s" % (eachpupil.Forename,eachpupil.Surname,OGD.Name.split('.')[-1],NGD.Name.split('.')[-1])
                            ChangeList.append(Msg)
                            Found=True
                        else:
                            Found=True
                if Found==False:
                    p=PupilGroup(AcademicYear=AcYear,Group=Group.objects.get(id=NewGroup),Pupil=eachpupil)
                    p.save()
                    NGD=Group.objects.get(id=int(NewGroup))
                    Msg=" %s %s has been moved into %s" % (eachpupil.Forename,eachpupil.Surname,NGD.Name.split('.')[-1])
                    ChangeList.append(Msg)
                    Found=True
    c.update({'ChangeList':ChangeList})
    return HttpResponseRedirect(request.session['OLD_PATH'])

# # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#   Set Management Finished
#
# # # # # # # # # # # # # # # # # # # # # # # # # # #
