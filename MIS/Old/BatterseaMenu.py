from MIS.models import *
from ModelFunctions import *
from MIS.forms import *
import time, datetime, copy
from operator import itemgetter

#-------------------------------------------------------------------------------
# Name:        Battersea Menu Loader
# Purpose:
#
# Author:      DBMgr
#
# Created:     10/04/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def BatterseaMenu(Type='Teacher'):

    currentYear=int(datetime.date.today().year)
    AvailableClasses={
    'Reception':['RBE','RBN','RBS','RBW'],'Year1':['1BE','1BN','1BS','1BW'],
    'Year2':['2BE','2BN','2BS','2BW'],'Year3':['3BE','3BN','3BS','3BW'],
    'Year4':['4BE','4BN','4BS','4BW'],'Year5':['5BE','5BN','5BS','5BW'],
    'Year6':['6BE','6BN','6BS','6BW'],'Year7':['7BE','7BN','7BS','7BW']}
    
    BatMenu.CreateGroupOnly(GroupName='Applicants.Holding')    

    if Type=='Admin':
        BatMenu= MenuBuilder(Type='New',MenuType='BatterseaApplicants',BaseGroup='Applicants')

        for Years in range(currentYear,currentYear+5):
            BatMenu.SetLevel(0)
            BatMenu.AddItem(GroupName='Applicants.Battersea.Entry_%d' % Years,MenuName='Entry_%d' % Years)
            BatMenu.Up() 
            BatMenu.SetBaseGroup('Applicants.Battersea.Entry_%d' % Years)
            BatMenu.AddItem(GroupName='Main',MenuName='Main')
            Menu_Actions=['Manage']
            BatMenu.Up()           
            
            for SchoolYears in sorted(AvailableClasses.iterkeys()):
                BatMenu.AddItem(GroupName='Main.%s' % SchoolYears,SubMenus=['Lists'])
                BatMenu.Up()
                BatMenu.SetBaseGroup('Applicants.Battersea.Entry_%s.Main.%s' % (Years,SchoolYears))
                BaseGroup=BatMenu.GetBaseGroup()
                MGroups=[]
                for Items in ['Offer','OfferSibling','Wait','TurnedDown','NotOffered','NoShow']:
                    MGroups.append('%s.%s' % (BaseGroup,Items))
                    BatMenu.AddItem(GroupName=Items,SubMenus=['Analysis','Lists'],
                                    menuActions=Menu_Actions)
                BatMenu.AddItem(GroupName='Interview',NotSets=True,ManagedGroups=MGroups,
                                SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)

                AcceptedGroups=copy.deepcopy(AvailableClasses[SchoolYears])
                AcceptedGroups.append('TurnedDownAfterDeposit')
                BatMenu.AddItem(GroupName='Accepted',ManagedGroups=AcceptedGroups,
                                SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)

                BatMenu.Up()
                for classes in AvailableClasses[SchoolYears]:
                    BatMenu.AddItem(GroupName='Accepted.%s' % classes,
                                    SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
                BatMenu.AddItem(GroupName='Accepted.TurnedDownAfterDeposit',
                                SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
                BatMenu.Down()
                BatMenu.Down()
            BatMenu.Down()

            BatMenu.AddItem(GroupName='Reserve')
            BatMenu.Up()
            for SchoolYears in sorted(AvailableClasses.iterkeys()):
                BatMenu.AddItem(GroupName='%s' % SchoolYears,SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
            BatMenu.Down()
            BatMenu.Down()

    Menu_Actions_Form=['Manage','Attendance','LessonAtt','Grade']
    Menu_Actions_Set=['Manage','LessonAtt','Grade']
    Menu_Actions_Class=['Manage','LessonAtt','Grade']
    Menu_Actions=['Manage']


    if Type=='Admin':
        BatMenu= MenuBuilder(Type='New',MenuType='BatterseaAdmin',BaseGroup='Alumni')
        BatMenu.SetLevel(0)
        BatMenu.SetBaseGroup(None)
        BatMenu.AddItem(GroupName='Alumni',MenuName='Alumni')
        BatMenu.Up()

        BatMenu.AddItem(GroupName='Alumni.Battersea',MenuName='All',
                        SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
        BatMenu.AddItem(GroupName='Alumni.Battersea.AskedToLeave',MenuName='Asked To Leave',
                        SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
        BatMenu.SetLevel(0)
        BatMenu.SetBaseGroup('Current.Battersea')
    else:
        BatMenu= MenuBuilder(Type='New',MenuType='BatterseaTeacher',BaseGroup='Current.Battersea',Subjects=['Mathematics', 'Games','English', 'English_Reading', 'English_Writing', 'English_Speaking' ,' Music', 'Drama', 'Art' , 'Form_Comment', 'Heads_Comment', 'Religious_Education', 'Drama' , 'ICT'])
        



    # Lower school
    BatMenu.AddItem(GroupName='LowerSchool',MenuName='Lower School',Subjects=['Gym_Athletics'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions,SubMenus=['Lists'],
    Subjects=['REC_EYFS_cd', 'REC_EYFS_da' ,'REC_EYFS_ed',' REC_EYFS_kuw',' REC_EYFS_lsl',
    'REC_EYFS_lfct', 'REC_EYFS_nlc ', 'REC_EYFS_pd ', 'REC_EYFS_sd ','REC_EYFS_ssm' ,'REC_EYFS_calc'])

    BatMenu.Up()

    # Reception

    BatMenu.SetBaseGroup('Current.Battersea.LowerSchool.Reception')
    BatMenu.CreateGroupOnly(GroupName='Current.Battersea.LowerSchool.Reception.Form')

    SubMenus = ['Analysis','Lists','Letters','Labels','Adhoc']
    BatMenu.AddItem(GroupName='Form.RBN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Form.RBE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Form.RBS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Form.RBW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)


    BatMenu.SetLevel(1)

    # Year 1

    BatMenu.SetBaseGroup('Current.Battersea.LowerSchool')

    BatMenu.AddItem(GroupName='Year1',MenuName='Year 1',Subjects=['Science','Topic','PSHE','French'],SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.LowerSchool.Year1.Form')

    # 1BN
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BN.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'],SubMenus=SubMenus)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BN.BalGames.Ballet',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year1.Form.1BN.BalGames.BoysGames',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()
    BatMenu.Down()

    # 1BE

    BatMenu.AddItem(GroupName='Year1.Form.1BE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BE.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'],SubMenus=SubMenus)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BE.BalGames.Ballet',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year1.Form.1BE.BalGames.BoysGames',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()
    BatMenu.Down()

    # 1BS

    BatMenu.AddItem(GroupName='Year1.Form.1BS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BS.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'],SubMenus=SubMenus)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BS.BalGames.Ballet',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year1.Form.1BS.BalGames.BoysGames',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()
    BatMenu.Down()

    # 1BW

    BatMenu.AddItem(GroupName='Year1.Form.1BW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BW.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year1.Form.1BW.BalGames.Ballet',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year1.Form.1BW.BalGames.BoysGames',menuActions=Menu_Actions_Class,SubMenus=SubMenus)


    # Year 2

    BatMenu.SetLevel(1)

    BatMenu.AddItem(GroupName='Year2',MenuName='Year 2',Subjects=['Science','Topic','PSHE','French'],SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.LowerSchool.Year2.Form')

    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year2.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year2.Games.Boys',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year2.Games.Girls',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='Year2.Form.2BN',menuActions=Menu_Actions_Class, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year2.Form.2BE',menuActions=Menu_Actions_Class, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year2.Form.2BS',menuActions=Menu_Actions_Class, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year2.Form.2BW',menuActions=Menu_Actions_Class, SubMenus=SubMenus)

    # MiddleSchool

    BatMenu.SetLevel(0)
    BatMenu.SetBaseGroup('Current.Battersea')

    BatMenu.AddItem(GroupName='MiddleSchool',MenuName='Middle School',Subjects=['Gym_Athletics'])
    BatMenu.SetBaseGroup('Current.Battersea.MiddleSchool')
    BatMenu.Up()


    # Year 3

    BatMenu.AddItem(GroupName='Year3',MenuName='Year 3',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.MiddleSchool.Year3.Form')
    BatMenu.Up()

    # Year 3 Games
    BatMenu.AddItem(GroupName='Year3.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year3.Games.Boys',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year3.Games.Girls',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()
    # Year 3 Mathematics
    BatMenu.AddItem(GroupName='Year3.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year3.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Mathematics.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.Down()
    # Year 3 Spelling
    BatMenu.AddItem(GroupName='Year3.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year3.Spelling.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Spelling.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Spelling.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year3.Spelling.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    BatMenu.Down()

    BatMenu.AddItem(GroupName='Year3.Form.3BN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year3.Form.3BE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year3.Form.3BS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year3.Form.3BW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)


    BatMenu.SetLevel(1)

    # Year 4

    BatMenu.AddItem(GroupName='Year4',MenuName='Year 4',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.MiddleSchool.Year4.Form')
    BatMenu.Up()

    # Year 4 Games
    BatMenu.AddItem(GroupName='Year4.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year4.Games.Boys',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year4.Games.Girls',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()
    # Year 4 Mathematics
    BatMenu.AddItem(GroupName='Year4.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year4.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Mathematics.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.Down()
    # Year 4 Spelling
    BatMenu.AddItem(GroupName='Year4.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year4.Spelling.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Spelling.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Spelling.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year4.Spelling.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    BatMenu.Down()

    BatMenu.AddItem(GroupName='Year4.Form.4BN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year4.Form.4BE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year4.Form.4BS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year4.Form.4BW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)


    BatMenu.SetLevel(1)

 # Year 5

    BatMenu.AddItem(GroupName='Year5',MenuName='Year 5',Subjects=['History','Geography','Latin'],SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.MiddleSchool.Year5.Form')
    BatMenu.Up()

    # Year 5 Games
    BatMenu.AddItem(GroupName='Year5.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year5.Games.Boys',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year5.Games.Girls',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()
    # Year 5 Mathematics
    BatMenu.AddItem(GroupName='Year5.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year5.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year5.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year5.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year5.Mathematics.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()
    # Year 5 Spelling
    BatMenu.AddItem(GroupName='Year5.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year5.Spelling.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year5.Spelling.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year5.Spelling.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year5.Spelling.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()

    #Year 5 Form classes

    BatMenu.AddItem(GroupName='Year5.Form.5BN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year5.Form.5BE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year5.Form.5BS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year5.Form.5BW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)


    BatMenu.SetLevel(1)

    # Upper School

    BatMenu.SetLevel(0)
    BatMenu.SetBaseGroup('Current.Battersea')

    BatMenu.AddItem(GroupName='UpperSchool',MenuName='Upper School')
    BatMenu.SetBaseGroup('Current.Battersea.UpperSchool')
    BatMenu.Up()

    # Year 6

    BatMenu.AddItem(GroupName='Year6',MenuName='Year 6',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.Up()

    # Year 6 Sets

    BatMenu.AddItem(GroupName='Year6.Yr6_Sets',MenuName='Yr6 Sets',ManagedGroups=['11Plus','13Plus'])
    BatMenu.Up()
    BatMenu.SetBaseGroup('Current.Battersea.UpperSchool.Year6.Yr6_Sets')

    # 11 Plus Sets

    BatMenu.AddItem(GroupName='11Plus',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.Mathematics',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.English',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.English.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.English.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.English.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.Science',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.Science.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.Science.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.Science.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.French',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.French.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.French.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.French.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.Geography',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.Geography.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.Geography.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.Geography.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.History',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.History.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['History'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.History.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['History'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.History.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['History'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='11Plus.Latin',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='11Plus.Latin.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.Latin.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='11Plus.Latin.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.Down()

    # 13 Plus Sets

    BatMenu.AddItem(GroupName='13Plus',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.Mathematics',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.English',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.English.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.English.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.English.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.Science',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.Science.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.Science.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.Science.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.French',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.French.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.French.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.French.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.Geography',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.Geography.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.Geography.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.Geography.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.History',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.History.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['History'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.History.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['History'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.History.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['History'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.AddItem(GroupName='13Plus.Latin',ManagedGroups=['SetA','SetB','SetC'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='13Plus.Latin.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.Latin.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='13Plus.Latin.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.Down()
    BatMenu.Down()
    BatMenu.Down()

    #Year 6 Form classes
    BatMenu.CreateGroupOnly('Current.Battersea.UpperSchool.Year6.Form')
    BatMenu.SetBaseGroup('Current.Battersea.UpperSchool')

    BatMenu.AddItem(GroupName='Year6.Form.6BN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year6.Form.6BE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year6.Form.6BS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year6.Form.6BW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)

    BatMenu.Down()

    # Year 7

    BatMenu.AddItem(GroupName='Year7',MenuName='Year 7',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.UpperSchool.Year7.Form')
    BatMenu.Up()

    # Year 7 Games
    BatMenu.AddItem(GroupName='Year7.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.Games.Boys',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year7.Games.Girls',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()

    # Year 7 Mathematics
    BatMenu.AddItem(GroupName='Year7.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Mathematics.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.Down()
    # Year 7 English
    BatMenu.AddItem(GroupName='Year7.English',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.English.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.English.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.English.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.English.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.Down()


    # Year 7 Science
    BatMenu.AddItem(GroupName='Year7.Science',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.Science.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Science.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Science.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Science.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.Down()
    # Year 7 French
    BatMenu.AddItem(GroupName='Year7.French',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.French.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.French.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.French.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.French.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.Down()

    # Year 7 Geography
    BatMenu.AddItem(GroupName='Year7.Geography',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.Geography.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Geography.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Geography.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Geography.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.Down()

    # Year 7 Latin
    BatMenu.AddItem(GroupName='Year7.Latin',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year7.Latin.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Latin.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Latin.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year7.Latin.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.Down()


    #Year 7 Form classes

    BatMenu.AddItem(GroupName='Year7.Form.7BN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year7.Form.7BE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year7.Form.7BS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year7.Form.7BW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)


    BatMenu.SetLevel(1)


    # Year 8

    BatMenu.AddItem(GroupName='Year8',MenuName='Year 8',SubMenus=['Lists'],menuActions=Menu_Actions)
    BatMenu.CreateGroupOnly('Current.Battersea.UpperSchool.Year8.Form')
    BatMenu.Up()

    # Year 8 Games
    BatMenu.AddItem(GroupName='Year8.Games',ManagedGroups=['Boys','Girls'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.Games.Boys',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year8.Games.Girls',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    BatMenu.Down()

    # Year 8 Mathematics
    BatMenu.AddItem(GroupName='Year8.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Mathematics.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    BatMenu.Down()
    # Year 8 English
    BatMenu.AddItem(GroupName='Year8.English',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.English.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.English.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.English.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.English.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    BatMenu.Down()


    # Year 8 Science
    BatMenu.AddItem(GroupName='Year8.Science',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.Science.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Science.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Science.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Science.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    BatMenu.Down()
    # Year 8 French
    BatMenu.AddItem(GroupName='Year8.French',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.French.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.French.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.French.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.French.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    BatMenu.Down()

    # Year 8 Geography
    BatMenu.AddItem(GroupName='Year8.Geography',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.Geography.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Geography.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Geography.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Geography.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    BatMenu.Down()

    # Year 8 Latin
    BatMenu.AddItem(GroupName='Year8.Latin',ManagedGroups=['SetA','SetB','SetC','SetD'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Year8.Latin.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Latin.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Latin.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.AddItem(GroupName='Year8.Latin.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    BatMenu.Down()


    #Year 8 Form classes

    BatMenu.AddItem(GroupName='Year8.Form.8BN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year8.Form.8BE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year8.Form.8BS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='Year8.Form.8BW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)


    BatMenu.SetLevel(0)

    # Academic Houses 

    BatMenu.SetBaseGroup('Current.Battersea')
    BatMenu.AddItem(GroupName='AcHouse',MenuName='House')
    BatMenu.Up()
    BatMenu.AddItem(GroupName='AcHouse.House',ManagedGroups=['Becket','Hardy','Lawrence','More'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='AcHouse.House.Becket',menuActions=Menu_Actions,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='AcHouse.House.Hardy',menuActions=Menu_Actions,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='AcHouse.House.Lawrence',menuActions=Menu_Actions,SubMenus=SubMenus)
    BatMenu.AddItem(GroupName='AcHouse.House.More',menuActions=Menu_Actions,SubMenus=SubMenus)    
    BatMenu.SetLevel(0)
    #  Staff Groups and Menu Items

    BatMenu.CreateGroupOnly(GroupName='Staff')
    BatMenu.SetBaseGroup('Staff')
    BatMenu.AddItem(GroupName='Battersea',MenuName='Staff')
    BatMenu.SetBaseGroup('Staff.Battersea')
    BatMenu.Up()
    BatMenu.AddItem(GroupName='LowerSchool' ,MenuName='Lower School',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='LowerSchool.Form',MenuName='LS Form',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])    
    BatMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions,SubMenus=['List','Labels','Letters'])

    BatMenu.AddItem(GroupName='LowerSchool.Year1',MenuName='Year 1',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='LowerSchool.Year2',MenuName='Year 2',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.Down()
    BatMenu.AddItem(GroupName='MiddleSchool' ,MenuName='Middle School',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='MiddleSchool.Form',MenuName='MS Form',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='MiddleSchool.Year3',MenuName='Year 3',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='MiddleSchool.Year4',MenuName='Year 4',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='MiddleSchool.Year5',MenuName='Year 5',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.Down()

    BatMenu.AddItem(GroupName='UpperSchool' ,MenuName='Upper School',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.Up()
    BatMenu.AddItem(GroupName='UpperSchool.Form',MenuName='US Form',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])    
    BatMenu.AddItem(GroupName='UpperSchool.Year6',MenuName='Year 6',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='UpperSchool.Year7',MenuName='Year 7',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='UpperSchool.Year8',MenuName='Year 8',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.Down()

    BatMenu.AddItem(GroupName='Department' ,MenuName='Departments')
    BatMenu.Up()
    BatMenu.AddItem(GroupName='Department.SysAdmin',MenuName='SysAdmin',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='Department.OfficeAdmin',MenuName='Office Admin',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='Department.Transport',MenuName='Transport',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='Department.Maintinance',MenuName='Maintinance',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='Department.Catering',MenuName='Catering',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='Department.CurrHODS',MenuName="Curr-HoD's",menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='Department.CoHODS',MenuName="Co-Curr HoD's",menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])    
    BatMenu.AddItem(GroupName='Department.SLT',MenuName="SLT",menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='Department.PE',MenuName='PE',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    BatMenu.AddItem(GroupName='Department.SEN',MenuName='Special Needs',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])

    BatMenu.Down()
