# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *   
from django.db.models import Q

# WillowTree system imports

from ViewExtras import SetMenu, SetLogo
from ModelFunctions import *

def FamilySearchForm(request,school,AcYear):
    c={'school':school,'AcYear':AcYear}
    return render_to_response('FamilySearchForm.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    
def FindFamilies(request,school,AcYear):
    c={'school':school,'AcYear':AcYear}
    if 'Postcode' in request.POST:
        FamilyList=Family.objects.filter(Address__PostCode__icontains=request.POST['Postcode'],Active=True)
    else:
        FamilyList=Family.objects.filter(FamilyName__icontains=request.POST['Surname'],Active=True)    
    if len(FamilyList) > 0:
        c.update({'FamilyList':FamilyList})
    else:
        c.update({'errmsg':['No matching records found']})
    return render_to_response('FamilySelect.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def EditFamily(request,school,AcYear,FamilyNo):
    c={'school':school,'AcYear':AcYear,'FamilyDetails':FamilyRecord(request,AcYear,FamilyId=FamilyNo),'Relationships':RelationshipTypes()}
    return render_to_response('EditFamilyRecord.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def NewFamilyAddress(request,school,AcYear,FamilyNo):
    NewAddress=Address(HomeSalutation=request.POST['HomeSalutation'],PostalTitle=request.POST['PostalTitle'],
                      AddressLine1=request.POST['AddressLine1'],AddressLine2=request.POST['AddressLine2'],
                      PostCode=request.POST['PostCode'],Phone1=request.POST['Phone1'],EmailAddress=request.POST['EmailAddress'])
    NewAddress.save()
    if 'AddressLine3' in request.POST:
        NewAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        NewAddress.AddressLine4=request.POST['AddressLine4']
    if 'Phone2' in request.POST:
        NewAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        NewAddress.Phone3=request.POST['Phone3']
    if 'Note' in request.POST:
        NewAddress.Note=request.POST['Note']
    NewAddress.save()
    FamilyRec=Family.objects.get(id=int(FamilyNo))
    FamilyRec.Address.add(NewAddress)
    FamilyRec.save()
    
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%d/' % (school,AcYear,FamilyRec.id))
    
def ModifyFamilyAddress(request,school,AcYear,FamilyNo,AddressNo):
    FamilyAddress=Address.objects.get(id=int(AddressNo))
    FamilyAddress.HomeSalutation=request.POST['HomeSalutation']
    FamilyAddress.PostalTitle=request.POST['PostalTitle']
    FamilyAddress.AddressLine1=request.POST['AddressLine1']
    FamilyAddress.AddressLine2=request.POST['AddressLine2']
    FamilyAddress.PostCode=request.POST['PostCode']
    FamilyAddress.Phone1=request.POST['Phone1']
    FamilyAddress.EmailAddress=request.POST['EmailAddress']
    FamilyAddress.save()
    if 'AddressLine3' in request.POST:
        FamilyAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        FamilyAddress.AddressLine4=request.POST['AddressLine4']
    if 'Phone2' in request.POST:
        FamilyAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        FamilyAddress.Phone3=request.POST['Phone3']
    if 'Note' in request.POST:
        FamilyAddress.Note=request.POST['Note']
    FamilyAddress.save()
    
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def RemoveFamilyAddress(request,school,AcYear,FamilyNo,AddressNo):
    FamilyRec=Family.objects.get(id=int(FamilyNo))
    FamilyRec.Address.remove(Address.objects.get(id=int(AddressNo)))
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))
    
def FindExistingFamilyAddress(request,school,AcYear,FamilyNo):
    c={'school':school,'AcYear':AcYear,'FamilyId':FamilyNo}
    c.update({'AddressList':Address.objects.filter(PostCode__icontains=request.POST['PostCode'])})
    return render_to_response('SelectFamilyAddress.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    
def AddExistingFamilyAddress(request,school,AcYear,FamilyNo,AddressNo):
    FamilyRec=Family.objects.get(id=int(FamilyNo))
    FamilyRec.Address.add(Address.objects.get(id=int(AddressNo)))
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))

def NewFamilyContact(request,school,AcYear,FamilyNo):
    newContact=Contact(Title=request.POST['Title'], Forename=request.POST['Forename'],Surname=request.POST['Surname'],
                       DateOfBirth=request.POST['DateOfBirth'],Gender=request.POST['Gender'])
    newContact.save()
    if 'OtherNames' in request.POST:
        newContact.OtherNames=request.POST['OtherNames']
        newContact.save()
    newFamilyContact=FamilyContact(FamilyId=Family.objects.get(id=int(FamilyNo)),Contact=newContact,Relationship=FamilyRelationship.objects.get(Type=request.POST['Relationship']))
    newFamilyContact.save()
    if request.POST['Priority'].find('None') < 0:
        newFamilyContact.Priority=int(request.POST['Priority'])
        newFamilyContact.save()
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))
        
def RemoveFamilyContact(request,school,AcYear,FamilyNo,ContactNo):
    FamilyCont=FamilyContact.objects.get(FamilyId__id=int(FamilyNo),Contact__id=int(ContactNo))
    FamilyCont.delete()
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))   
    
def ModifyFamilyContact(request,school,AcYear,FamilyNo,ContactNo):
    FamilyCont=FamilyContact.objects.get(FamilyId__id=int(FamilyNo),Contact__id=int(ContactNo))
    FamilyCont.Relationship=FamilyRelationship.objects.get(Type=request.POST['Relationship'])
    if request.POST['Priority'].find('None') < 0:
        FamilyCont.Priority=int(request.POST['Priority'])
    FamilyCont.Contact.Title=request.POST['Title']
    FamilyCont.Contact.Forename=request.POST['Forename']
    FamilyCont.Contact.Surname=request.POST['Surname']
    FamilyCont.Contact.DateOfBirth=request.POST['DateOfBirth']
    FamilyCont.Contact.Gender=request.POST['Gender']
    FamilyCont.Contact.save()
    FamilyCont.save()
    
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))      
    
def ModifyContactAddressOld(request,school,AcYear,FamilyNo,AddressNo):
    OldAddress=Address.objects.get(id=int(AddressNo))
    OldAddress.HomeSalutation=request.POST['HomeSalutation']
    OldAddress.PostalTitle=request.POST['PostalTitle']
    OldAddress.AddressLine1=request.POST['AddressLine1']
    OldAddress.AddressLine2=request.POST['AddressLine2']
    if 'AddressLine3' in request.POST:
        OldAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        OldAddress.AddressLine4=request.POST['AddressLine4']
    OldAddress.PostCode=request.POST['PostCode']
    OldAddress.Phone1=request.POST['Phone1']
    if 'Phone2' in request.POST:
        OldAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        OldAddress.Phone3=request.POST['Phone3']
    OldAddress.EmailAddress=request.POST['EmailAddress']
    if 'Note' in request.POST:
        OldAddress.Note=request.POST['Note']
        
    OldAddress.save()
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))  

def ModifyContactAddressNew(request,school,AcYear,FamilyNo,ContactNo):
    NewAddress=Address(HomeSalutation=request.POST['HomeSalutation'],PostalTitle=request.POST['PostalTitle'],
                      AddressLine1=request.POST['AddressLine1'],AddressLine2=request.POST['AddressLine2'],
                      PostCode=request.POST['PostCode'],Phone1=request.POST['Phone1'],EmailAddress=request.POST['EmailAddress'])
    NewAddress.save()
    if 'AddressLine3' in request.POST:
        NewAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        NewAddress.AddressLine4=request.POST['AddressLine4']
    if 'Phone2' in request.POST:
        NewAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        NewAddress.Phone3=request.POST['Phone3']
    if 'Note' in request.POST:
        NewAddress.Note=request.POST['Note']
    NewAddress.save()
    ContactRec=Contact.objects.get(id=int(ContactNo))
    ContactRec.Address=(NewAddress)
    ContactRec.save()    
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))  

def ModifyContactAddressFromFamily(request,school,AcYear,FamilyNo,ContactNo,AddressNo):
    ContactRec=Contact.objects.get(id=int(ContactNo))
    ContactRec.Address=(Address.objects.get(id=int(AddressNo)))
    ContactRec.save() 
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))  
    
def ModifyContactWorkAddressOld(request,school,AcYear,FamilyNo,AddressNo):
    OldAddress=Address.objects.get(id=int(AddressNo))
    OldAddress.HomeSalutation=request.POST['HomeSalutation']
    OldAddress.PostalTitle=request.POST['PostalTitle']
    OldAddress.AddressLine1=request.POST['AddressLine1']
    OldAddress.AddressLine2=request.POST['AddressLine2']
    if 'AddressLine3' in request.POST:
        OldAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        OldAddress.AddressLine4=request.POST['AddressLine4']
    OldAddress.PostCode=request.POST['PostCode']
    OldAddress.Phone1=request.POST['Phone1']
    if 'Phone2' in request.POST:
        OldAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        OldAddress.Phone3=request.POST['Phone3']
    OldAddress.EmailAddress=request.POST['EmailAddress']
    if 'Note' in request.POST:
        OldAddress.Note=request.POST['Note']
        
    OldAddress.save()
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))  

def ModifyContactWorkAddressNew(request,school,AcYear,FamilyNo,ContactNo):
    NewAddress=Address(HomeSalutation=request.POST['HomeSalutation'],PostalTitle=request.POST['PostalTitle'],
                      AddressLine1=request.POST['AddressLine1'],AddressLine2=request.POST['AddressLine2'],
                      PostCode=request.POST['PostCode'],Phone1=request.POST['Phone1'],EmailAddress=request.POST['EmailAddress'])
    NewAddress.save()
    if 'AddressLine3' in request.POST:
        NewAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        NewAddress.AddressLine4=request.POST['AddressLine4']
    if 'Phone2' in request.POST:
        NewAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        NewAddress.Phone3=request.POST['Phone3']
    if 'Note' in request.POST:
        NewAddress.Note=request.POST['Note']
    NewAddress.save()
    ContactRec=Contact.objects.get(id=int(ContactNo))
    ContactRec.WorkAddress=(NewAddress)
    ContactRec.save()    
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))  

def ModifyContactWorkAddressFromFamily(request,school,AcYear,FamilyNo,ContactNo,AddressNo):
    ContactRec=Contact.objects.get(id=int(ContactNo))
    ContactRec.WorkAddress=(Address.objects.get(id=int(AddressNo)))
    ContactRec.save() 
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))      

def NewFamily(request,AcYear,school):
    NewAddress=Address(HomeSalutation=request.POST['HomeSalutation'],PostalTitle=request.POST['PostalTitle'],
                      AddressLine1=request.POST['AddressLine1'],AddressLine2=request.POST['AddressLine2'],
                      PostCode=request.POST['PostCode'],Phone1=request.POST['Phone1'],EmailAddress=request.POST['EmailAddress'])
    NewAddress.save()
    if 'AddressLine3' in request.POST:
        NewAddress.AddressLine3=request.POST['AddressLine3']
    if 'AddressLine4' in request.POST:
        NewAddress.AddressLine4=request.POST['AddressLine4']
    if 'Phone2' in request.POST:
        NewAddress.Phone2=request.POST['Phone2']
    if 'Phone3' in request.POST:
        NewAddress.Phone3=request.POST['Phone3']
    if 'Note' in request.POST:
        NewAddress.Note=request.POST['Note']
    NewAddress.save()
    FamilyRec=Family(FamilyName=request.POST['FamilyName'])
    FamilyRec.save()
    FamilyRec.Address.add(NewAddress)
    FamilyRec.save()    
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyRec.id))     
    
def NewPupilRecord(request, AcYear, school, FamilyNo):
    NewPupil=Pupil(Surname=request.POST['Surname'],Forename=request.POST['Forename'],DateOfBirth=request.POST['DateOfBirth'],Gender=request.POST['Gender'])
    NewPupil.save()
    if 'Title' in request.POST:
        NewPupil.Title=request.POST['Title']
    if 'KnownAs' in request.POST:
        NewPupil.KnownAs=request.POST['KnownAs']
    if 'OtherNames' in request.POST:
        NewPupil.OtherNames=request.POST['OtherNames']
    if 'EmailAddress' in request.POST:
        NewPupil.EmailAddress=request.POST['EmailAddress']
    if 'Picture' in request.FILES:
        NewPupil.Picture=request.FILES['Picture']
    NewPupil.save()
    AssignGroup=PupilGroup(AcademicYear='Holding', Pupil=NewPupil,Group=Group.objects.get(Name='Applicants.Holding'))
    AssignGroup.save()
    NewFamilyChild=FamilyChildren(FamilyId=Family.objects.get(id=int(FamilyNo)), Pupil=NewPupil)
    NewFamilyChild.save()
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%s/' % (school,AcYear,FamilyNo))         

def UpdateApplicantExtra(request, AcYear, school, PupilNo):
    ExtraFields=ExtendedRecord(BaseType='Pupil',BaseId=int(PupilNo))
    if 'ApplicantSource' in request.POST:
        ExtraFields.WriteExtention('PupilExtra',{'ApplicantSource':request.POST['ApplicantSource']})
    if 'ApplicantInterviewTime' in request.POST:
        ExtraFields.WriteExtention('PupilExtra',{'ApplicantInterviewTime':request.POST['ApplicantInterviewTime'].split('T')[1]})
    if 'ApplicantInterviewDate' in request.POST:
        ExtraFields.WriteExtention('PupilExtra',{'ApplicantInterviewDate':request.POST['ApplicantInterviewDate']})
    if 'ApplicantStandardisedScore' in request.POST:
        ExtraFields.WriteExtention('PupilExtra',{'ApplicantStandardisedScore':request.POST['ApplicantStandardisedScore']})    
    PupilOb=PupilObject(PupilNo,AcYear)
    cache.set('Pupil_%s' % PupilNo,PupilOb,900)
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,AcYear,PupilNo))   
    