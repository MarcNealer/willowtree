# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q

# WillowTree system imports

from ViewExtras import SetMenu, SetLogo
from MIS.forms import *
from ModelFunctions import *


def CreateLetter(request,school,AcYear):
    c={'school':school,'AcYear':AcYear,'TemplateDetails':TemplateRecord(),'GroupList':SchoolGroupsList(school)}
    return render_to_response('CreateALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def SaveALetter(request,school,AcYear):
    c={'school':school,'AcYear':AcYear,'TemplateDetails':TemplateRecord(),'GroupList':SchoolGroupsList(school)}
    c.update({'LetterDetails':LetterManager.SaveLetter(request,school,AcYear)})
    return render_to_response('ModifyALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def SelectLetterToCopy(request,AcYear,school):
    pass

def SelectLetterToModify(request,AcYear,school):
    c={'school':school,'AcYear':AcYear,'LetterList':LetterBody.objects.filter(Active=True)}
    return render_to_response('SelectALetterToModify.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def ModifyLetter(request,AcYear,school):
    c={'school':school,'AcYear':AcYear}  
    if not request.POST['SelectLetter']=='None':
        c.update({'LetterDetails':LetterBody.objects.get(id=int(request.POST['SelectLetter'])),'TemplateDetails':TemplateRecord(),'GroupList':SchoolGroupsList(school)})
        return render_to_response('ModifyALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    else:
        errmsg=['No Letter selected']
        c.update({'LetterList':LetterBody.objects.filter(Active=True),'return_msg':errmsg})
        return render_to_response('SelectALetterToModify.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
        
def SaveALetterModification(request,AcYear,school,LetterNo):
    LetterManager.Inactivate(LetterNo)
    c={'school':school,'AcYear':AcYear,'TemplateDetails':TemplateRecord(),'GroupList':SchoolGroupsList(school)}
    c.update({'LetterDetails':LetterManager.SaveLetter(request,school,AcYear),'return_msg':['Letter Saved']})
    return render_to_response('ModifyALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
       
        



def SelectALetter(request,AcYear,school,GroupName):
    c={'school':school,'AcYear':AcYear,'GroupName':GroupName}
    c.update({'LetterList':LetterManager.GetlistOfLetters(GroupName)})
    return render_to_response('SelectALetter.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def GenerateALetter(request,AcYear,school,GroupName):
    if 'SelectLetter' in request.POST:
        LetterRecord=LetterBody.objects.get(id=int(request.POST['SelectLetter']))
        if LetterRecord.ObjectBaseType=='Family':
            c={'Letter':GenerateFamilyLetter(request,LetterRecord.id,school,AcYear)}
        else:
            c={'Letter':GeneratePupilLetter(request,LetterRecord.id,school,AcYear)}            
    return render_to_response(LetterRecord.LetterTemplate.Filename,c,context_instance=RequestContext(request,processors=[SetMenu]))

def GenerateLetterLabels(request,AcYear,school):
    c={'Label':GenerateLabels(request,school,AcYear)}
    LabelType=request.POST['LabelType']
    if LabelType=='AddressFamily' or LabelType=='AddressPupil':
        return render_to_response('LetterTemplates/AddressLabel_L7163.html',c)
    elif LabelType=='AddressWithPupilName':
        return render_to_response('LetterTemplates/AddressLabelPupil_L7163.html',c)
    elif LabelType=='ChildLabel1':
        LabelDetails={'Font':'Defailt','Colour':'Default','Size':'Default','Extra':'None'}
        if 'ChildLabel1Font' in request.POST:
            if len(request.POST['ChildLabel1Font'])>0:
                LabelDetails['Font']=request.POST['ChildLabel1Font']
        if 'ChildLabel1Size' in request.POST:
            if len(request.POST['ChildLabel1Size'])>0:
                LabelDetails['Size']=request.POST['ChildLabel1Size']        
        if 'ChildLabel1Colour' in request.POST:
            if len(request.POST['ChildLabel1Colour'])>0:
                LabelDetails['Colour']=request.POST['ChildLabel1Colour']                
        if 'ChildLabel1Extra' in request.POST:
            if len(request.POST['ChildLabel1Extra'])>0:
                LabelDetails['Extra']=request.POST['ChildLabel1Extra']
        c.update(LabelDetails)
        return render_to_response('LetterTemplates/ChildLabel1_L7163.html',c)
    elif LabelType=='ChildLabel2':
        LabelDetails={'Font':'Defailt','Colour':'Default','Size':'Default','Extra':'None'}
        if 'ChildLabel2Font' in request.POST:
            if len(request.POST['ChildLabel2Font'])>0:
                LabelDetails['Font']=request.POST['ChildLabel2Font']
        if 'ChildLabel2Size' in request.POST:
            if len(request.POST['ChildLabel2Size'])>0:
                LabelDetails['Size']=request.POST['ChildLabel2Size']         
        if 'ChildLabel2Colour' in request.POST:
            if len(request.POST['ChildLabel2Colour'])>0:
                LabelDetails['Colour']=request.POST['ChildLabel2Colour']                
        if 'ChildLabel2Extra' in request.POST:
            if len(request.POST['ChildLabel2Extra'])>0:
                LabelDetails['Extra']=request.POST['ChildLabel2Extra']
        if 'ChildLabel2Image' in request.POST:
            if len(request.POST['ChildLabel2Image'])>0:
                LabelDetails['Image']=request.POST['ChildLabel2Image']                
        c.update(LabelDetails)
        return render_to_response('LetterTemplates/ChildLabel2_L7163.html',c,context_instance=RequestContext(request))        