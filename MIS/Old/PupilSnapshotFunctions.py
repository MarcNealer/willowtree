from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache

def getPupilSnapshot(xPupilId,xExtentionRecord,xAcYear):
    Pupilrec=cache.get('SShot%s%s' % (xExtentionRecord,PupilId))
    if not Pupilrec:
        if PupilSnapshot.objects.filter(PupilRecord__id=int(xPupil),GradeRecord__Name=xExtentionRecord).exists():
            PupilOb=PupilSnapshot.objects.getr(PupilRecord__id=int(xPupil),GradeRecord__Name=xExtentionRecord)
        else:
            PupilOb=pupilSnapshotObj(xPupilId,xExtentionRecord,AcYear)
        cache.set('SShot%s%s' % (xExtentionRecord,xPupilId,PupilOb,900))
        return PupilOb
    else:
        return Pupilrec

class pupilSnapshotObj():
    ''' This class builds and maintains pupil Snapshots'''
    def __init__(self,xPupilId,xExtentionRecord,xAcYear):
        # store the SEN status of the pupil
        # store the EAL status of the pupil
        # store the G/T status of the pupil
        # store whatever Sets the pupil belongs to
        self.setSetList(PupilRec)
        # Store the Year group they belong to
        # if the pupil is year 6, store their 11+/13+ status
        # if the pupil is year 7/8, then get their scholar status
        # Store their Form class
        # Store the form class teacher from Global Variables
        # Store the number of auth/unauth/lates  for the given Term
        pass
    def setSENStatus(self):
        pass
    def setEALStatus(self):
        pass
    def setGTStatus(self):
        pass
    def setSetList(self,PupilRec):
        self.SetList={}
        for sets in PupilGroup.objects.filter(Pupil__id=self.Pupil,Group__Name__icontains='Set',AcademicYear=self.AcYear):
            self.SetList[sets.Group.Name.split('.')[-2]]=sets.split('.')[-1]
    def setYearGroup(self):
        for xGroups in PupilGroup.objects.filter(Pupil__id=self.Pupil,Group__Name__icontains='Year',AcademicYear=self.AcYear):
            self.YearGroup='Year%s' % xGroups.split('Year')[1].split('.')[0]
    def setYear6Status(self):
        if self.YearGroup=='Year6':
            for xGroups in PupilGroup.objects.filter(Pupil__id=self.Pupil,Group__Name__icontains='Year6.Yr6',AcademicYear=self.AcYear):
                self.Year6Status=xGroups.split('Yr6_Sets.')[1].split('.')[0]
    def setYear7Status(self):
        if self.YearGroup=='Year7':
            if PupilGroup.objects.filter(Pupil_id=self.Pupil,Group__Name_icontaints='SetS',AcademicYear=self.AcYear):
                self.ScholarStatus=True
            else:
                self.ScholarStatus=False
    def setYear8Status(self):
        if self.YearGroup=='Year8':
            if PupilGroup.objects.filter(Pupil_id=self.Pupil,Group__Name_icontaints='SetS',AcademicYear=self.AcYear):
                self.ScholarStatus=True
            else:
                self.ScholarStatus=False
    def setFormClass(self):
        for xGroups in PupilGroup.objects.filter(Pupil__id=self.Pupil,Group__Name__icontains='Form',AcademicYear=self.AcYear):
            self.Form=xGroups.split('Form.')[1].split('.')[0]
    def setFormTeacher(self):
        VarList=GlobalVariables('Yearly').GetVariableList(self.Form)
        if len(VarList) > 0:
            self.FormTeacher=VarList[0].Data
    def setLates(self):
        pass
    def setAuthAbsences(self):
        pass
    def setUnauthAbences(self):
        pass
    