# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q

# WillowTree system imports

from ViewExtras import SetMenu, SetLogo
from MIS.forms import *
from ModelFunctions import *

def PageNotFound(request):
    ''' PageNotFound : Used to render the page not found '''
    c={'errmsg': 'Page Not found : %s' % request.META['PATH_INFO']}
    return render_to_response('Welcome.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


# # # # # #
#
#
#
#  Modify Alerts
#
#
# # # # # #

def ViewAlerts(request,school,AcYear,PupilNo):
    request.session.set_expiry(900)
    c={'school':school,'AcYear':AcYear}
    c.update(csrf(request))
    AlertDetails=Alerts(request,PupilNo)

    c.update({'PupilAlertList':AlertDetails.AlertList(),'PupilDetails':Pupil.objects.get(id=PupilNo)})

    return render_to_response('EditAlerts.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def UpdateAlerts(request, school, AcYear, PupilNo, Alert_Group):
    AlertItem=Alerts(request,PupilNo)
    return_msg=[]
    if AlertItem.Valid():
        if request.POST['save'].find('Activate') > -1:
            AlertItem.Save()
            return_msg.append('%s Alert Actived for Pupil No %s' %(Alert_Group,PupilNo))
        elif request.POST['save'].find('New') > -1:
            AlertItem.Close()
            AlertItem.Save()
            return_msg.append('New %s Alert Actived for Pupil No %s' %(Alert_Group,PupilNo))
        elif request.POST['save'].find('Modify') > -1:
            AlertItem.Modify()
            return_msg.append('%s Alert Modified for Pupil No %s' %(Alert_Group,PupilNo))
        elif request.POST['save'].find('Close') > -1:
            AlertItem.Close()
            return_msg.append('%s Alert Closed for Pupil No %s' %(Alert_Group,PupilNo))
    else:
        return_msg=AlertItem.ErrMsg
    c={'school':school,'AcYear':AcYear,'return_msg':return_msg,'PupilAlertList':AlertItem.AlertList(),'PupilDetails':Pupil.objects.get(id=PupilNo)}
    return render_to_response('EditAlerts.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
