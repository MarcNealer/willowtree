from MIS.models import *
from ModelFunctions import *
from MIS.forms import *
import time, datetime, copy
from operator import itemgetter

#-------------------------------------------------------------------------------
# Name:        Kensington Menu Loader
# Purpose:
#
# Author:      DBMgr
#
# Created:     10/04/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

def KensingtonMenu(Type='Teacher'):

    currentYear=int(datetime.date.today().year)
    AvailableClasses={
    'Reception':['RFE','RFN','RFS','RFW'],'Year1':['1FE','1FN','1FS','1FW'],
    'Year2':['2FE','2FN','2FS','2FW'],'Year3':['3FE','3FN','3FS','3FW'],
    'Year4':['4FE','4FN','4FS','4FW'],'Year5':['5FE','5FN','5FS','5FW'],}

    if Type=='Admin':
        KenMenu= MenuBuilder(Type='New',MenuType='KensingtonApplicants',BaseGroup='Applicants')

        for Years in range(currentYear,currentYear+5):
            KenMenu.SetLevel(0)
            KenMenu.AddItem(GroupName='Applicants.Kensington.Entry_%d' % Years,MenuName='Entry_%d' % Years)
            KenMenu.Up() 
            KenMenu.SetBaseGroup('Applicants.Kensington.Entry_%d' % Years)
            KenMenu.AddItem(GroupName='Main',MenuName='Main')
            Menu_Actions=['Manage']
            KenMenu.Up()           
            
            for SchoolYears in sorted(AvailableClasses.iterkeys()):
                KenMenu.AddItem(GroupName='Main.%s' % SchoolYears,SubMenus=['Lists'])
                KenMenu.Up()
                KenMenu.SetBaseGroup('Applicants.Kensington.Entry_%s.Main.%s' % (Years,SchoolYears))
                BaseGroup=KenMenu.GetBaseGroup()
                MGroups=[]
                for Items in ['Offer','OfferSibling','Wait','TurnedDown','NotOffered','NoShow']:
                    MGroups.append('%s.%s' % (BaseGroup,Items))
                    KenMenu.AddItem(GroupName=Items,SubMenus=['Analysis','Lists'],
                                    menuActions=Menu_Actions)
                KenMenu.AddItem(GroupName='Interview',NotSets=True,ManagedGroups=MGroups,
                                SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)

                AcceptedGroups=copy.deepcopy(AvailableClasses[SchoolYears])
                AcceptedGroups.append('TurnedDownAfterDeposit')
                KenMenu.AddItem(GroupName='Accepted',ManagedGroups=AcceptedGroups,
                                SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)

                KenMenu.Up()
                for classes in AvailableClasses[SchoolYears]:
                    KenMenu.AddItem(GroupName='Accepted.%s' % classes,
                                    SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
                KenMenu.AddItem(GroupName='Accepted.TurnedDownAfterDeposit',
                                SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
                KenMenu.Down()
                KenMenu.Down()
            KenMenu.Down()

            KenMenu.AddItem(GroupName='Reserve')
            KenMenu.Up()
            for SchoolYears in sorted(AvailableClasses.iterkeys()):
                KenMenu.AddItem(GroupName='%s' % SchoolYears,SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
            KenMenu.Down()
            KenMenu.Down()

    Menu_Actions_Form=['Manage','Attendance','LessonAtt','Grade']
    Menu_Actions_Set=['Manage','LessonAtt','Grade']
    Menu_Actions_Class=['Manage','LessonAtt','Grade']
    Menu_Actions=['Manage']


    if Type=='Admin':
        KenMenu= MenuBuilder(Type='New',MenuType='KensingtonAdmin',BaseGroup='Alumni')
        KenMenu.SetLevel(0)
        KenMenu.SetBaseGroup(None)
        KenMenu.AddItem(GroupName='Alumni',MenuName='Alumni')
        KenMenu.Up()

        KenMenu.AddItem(GroupName='Alumni.Kensington',MenuName='All',
                        SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
        KenMenu.AddItem(GroupName='Alumni.Kensington.AskedToLeave',MenuName='Asked To Leave',
                        SubMenus=['Analysis','Lists'],menuActions=Menu_Actions)
        KenMenu.SetLevel(0)
        KenMenu.SetBaseGroup('Current.Kensington')
    else:
        KenMenu= MenuBuilder(Type='New',MenuType='KensingtonTeacher',BaseGroup='Current.Kensington',Subjects=['Mathematics', 'Games','English', 'English_Reading', 'English_Writing', 'English_Speaking' ,' Music', 'Drama', 'Art' , 'Form_Comment', 'Heads_Comment', 'Religious_Education', 'Drama' , 'ICT'])
        



    # Lower school
    KenMenu.AddItem(GroupName='LowerSchool',MenuName='Lower School',Subjects=['Gym_Athletics'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions,SubMenus=['Lists'],
    Subjects=['REC_EYFS_cd', 'REC_EYFS_da' ,'REC_EYFS_ed',' REC_EYFS_kuw',' REC_EYFS_lsl',
    'REC_EYFS_lfct', 'REC_EYFS_nlc ', 'REC_EYFS_pd ', 'REC_EYFS_sd ','REC_EYFS_ssm' ,'REC_EYFS_calc'])

    KenMenu.Up()

    # Reception

    KenMenu.SetBaseGroup('Current.Kensington.LowerSchool.Reception')
    KenMenu.CreateGroupOnly(GroupName='Current.Kensington.LowerSchool.Reception.Form')

    SubMenus = ['Analysis','Lists','Letters','Labels','Adhoc']
    KenMenu.AddItem(GroupName='Form.RKN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Form.RKE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Form.RKS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Form.RKW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)


    KenMenu.SetLevel(1)

    # Year 1

    KenMenu.SetBaseGroup('Current.Kensington.LowerSchool')

    KenMenu.AddItem(GroupName='Year1',MenuName='Year 1',Subjects=['Science','Topic','PSHE','French'],SubMenus=['Lists'],menuActions=Menu_Actions)
    KenMenu.CreateGroupOnly('Current.Kensington.LowerSchool.Year1.Form')

    # 1BN
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KN.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'],SubMenus=SubMenus)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KN.BalGames.Ballet',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year1.Form.1KN.BalGames.BoysGames',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.Down()
    KenMenu.Down()

    # 1BE

    KenMenu.AddItem(GroupName='Year1.Form.1KE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KE.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'],SubMenus=SubMenus)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KE.BalGames.Ballet',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year1.Form.1KE.BalGames.BoysGames',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.Down()
    KenMenu.Down()

    # 1BS

    KenMenu.AddItem(GroupName='Year1.Form.1KS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KS.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'],SubMenus=SubMenus)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KS.BalGames.Ballet',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year1.Form.1KS.BalGames.BoysGames',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.Down()
    KenMenu.Down()

    # 1BW

    KenMenu.AddItem(GroupName='Year1.Form.1KW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KW.BalGames',MenuName='Bal/Games',ManagedGroups=['Ballet','BoysGames'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year1.Form.1KW.BalGames.Ballet',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year1.Form.1KW.BalGames.BoysGames',menuActions=Menu_Actions_Class,SubMenus=SubMenus)


    # Year 2

    KenMenu.SetLevel(1)

    KenMenu.AddItem(GroupName='Year2',MenuName='Year 2',Subjects=['Science','Topic','PSHE','French'],SubMenus=['Lists'],menuActions=Menu_Actions)
    KenMenu.CreateGroupOnly('Current.Kensington.LowerSchool.Year2.Form')

    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year2.Games',ManagedGroups=['Boys','Girls'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year2.Games.Boys',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year2.Games.Girls',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='Year2.Form.2KN',menuActions=Menu_Actions_Class, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year2.Form.2KE',menuActions=Menu_Actions_Class, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year2.Form.2KS',menuActions=Menu_Actions_Class, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year2.Form.2KW',menuActions=Menu_Actions_Class, SubMenus=SubMenus)

    # PrepSchool

    KenMenu.SetLevel(0)
    KenMenu.SetBaseGroup('Current.Kensington')

    KenMenu.AddItem(GroupName='PrepSchool',MenuName='Middle School',Subjects=['Gym_Athletics'])
    KenMenu.SetBaseGroup('Current.Kensington.PrepSchool')
    KenMenu.Up()


    # Year 3

    KenMenu.AddItem(GroupName='Year3',MenuName='Year 3',SubMenus=['Lists'],menuActions=Menu_Actions)
    KenMenu.CreateGroupOnly('Current.Kensington.PrepSchool.Year3.Form')
    KenMenu.Up()

    # Year 3 Games
    KenMenu.AddItem(GroupName='Year3.Games',ManagedGroups=['Boys','Girls'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year3.Games.Boys',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year3.Games.Girls',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.Down()
    # Year 3 Mathematics
    KenMenu.AddItem(GroupName='Year3.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year3.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year3.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year3.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year3.Mathematics.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.Down()
    # Year 3 Spelling
    KenMenu.AddItem(GroupName='Year3.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year3.Spelling.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year3.Spelling.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year3.Spelling.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year3.Spelling.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    KenMenu.Down()

    KenMenu.AddItem(GroupName='Year3.Form.3KN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year3.Form.3KE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year3.Form.3KS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year3.Form.3KW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)


    KenMenu.SetLevel(1)

    # Year 4

    KenMenu.AddItem(GroupName='Year4',MenuName='Year 4',SubMenus=['Lists'],menuActions=Menu_Actions)
    KenMenu.CreateGroupOnly('Current.Kensington.PrepSchool.Year4.Form')
    KenMenu.Up()

    # Year 4 Games
    KenMenu.AddItem(GroupName='Year4.Games',ManagedGroups=['Boys','Girls'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year4.Games.Boys',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year4.Games.Girls',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.Down()
    # Year 4 Mathematics
    KenMenu.AddItem(GroupName='Year4.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year4.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year4.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year4.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year4.Mathematics.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.Down()
    # Year 4 Spelling
    KenMenu.AddItem(GroupName='Year4.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year4.Spelling.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year4.Spelling.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year4.Spelling.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='Year4.Spelling.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Spelling'],ParentSubjects=False)
    KenMenu.Down()

    KenMenu.AddItem(GroupName='Year4.Form.4KN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year4.Form.4KE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year4.Form.4KS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year4.Form.4KW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)


    KenMenu.SetLevel(1)

 # Year 5

    KenMenu.AddItem(GroupName='Year5',MenuName='Year 5',Subjects=['History','Geography','Latin'],SubMenus=['Lists'],menuActions=Menu_Actions)
    KenMenu.CreateGroupOnly('Current.Kensington.PrepSchool.Year5.Form')
    KenMenu.Up()

    # Year 5 Games
    KenMenu.AddItem(GroupName='Year5.Games',ManagedGroups=['Boys','Girls'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year5.Games.Boys',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year5.Games.Girls',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.Down()
    # Year 5 Mathematics
    KenMenu.AddItem(GroupName='Year5.Mathematics',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year5.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year5.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year5.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year5.Mathematics.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.Down()
    # Year 5 Spelling
    KenMenu.AddItem(GroupName='Year5.Spelling',ManagedGroups=['SetA','SetB','SetC','SetD'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Year5.Spelling.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year5.Spelling.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year5.Spelling.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year5.Spelling.SetD',menuActions=Menu_Actions_Class,SubMenus=SubMenus)
    KenMenu.Down()

    #Year 5 Form classes

    KenMenu.AddItem(GroupName='Year5.Form.5KN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year5.Form.5KE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year5.Form.5KS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year5.Form.5KW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)


    KenMenu.SetLevel(1)



    KenMenu.AddItem(GroupName='Year6',MenuName='Year 6',SubMenus=['Lists'],menuActions=Menu_Actions)
    KenMenu.Up()

    # Year 6 Sets

    KenMenu.AddItem(GroupName='Year6.Yr6_Sets',MenuName='Yr6 Sets',ManagedGroups=['11Plus','13Plus'])
    KenMenu.Up()
    KenMenu.SetBaseGroup('Current.Kensington.UpperSchool.Year6.Yr6_Sets')

    # 11 Plus Sets

    KenMenu.AddItem(GroupName='Mathematics',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='.Mathematics.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.Mathematics.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.Mathematics.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Mathematics'],ParentSubjects=False)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='.English',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='.English.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.English.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.English.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['English'],ParentSubjects=False)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='.Science',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='.Science.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.Science.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.Science.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Science'],ParentSubjects=False)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='.French',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='.French.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.French.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.French.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['French'],ParentSubjects=False)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='.Geography',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='.Geography.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.Geography.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.Geography.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Geography'],ParentSubjects=False)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='.History',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='.History.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['History'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.History.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['History'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.History.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['History'],ParentSubjects=False)
    KenMenu.Down()
    KenMenu.AddItem(GroupName='.Latin',ManagedGroups=['SetA','SetB','SetC'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='.Latin.SetA',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.Latin.SetB',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    KenMenu.AddItem(GroupName='.Latin.SetC',menuActions=Menu_Actions_Class,SubMenus=SubMenus,Subjects=['Latin'],ParentSubjects=False)
    KenMenu.Down()
    KenMenu.Down()

    #Year 6 Form classes
    KenMenu.CreateGroupOnly('Current.Kensington.UpperSchool.Year6.Form')
    KenMenu.SetBaseGroup('Current.Kensington.UpperSchool')

    KenMenu.AddItem(GroupName='Year6.Form.6KN',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year6.Form.6KE',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year6.Form.6KS',menuActions=Menu_Actions_Form, SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='Year6.Form.6KW',menuActions=Menu_Actions_Form, SubMenus=SubMenus)

    KenMenu.SetLevel(0)

    # Academic Houses 

    KenMenu.SetBaseGroup('Current.Kensington')
    KenMenu.AddItem(GroupName='AcHouse',MenuName='House')
    KenMenu.Up()
    KenMenu.AddItem(GroupName='AcHouse.House',ManagedGroups=['Becket','Hardy','Lawrence','More'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='AcHouse.House.Becket',menuActions=Menu_Actions,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='AcHouse.House.Hardy',menuActions=Menu_Actions,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='AcHouse.House.Lawrence',menuActions=Menu_Actions,SubMenus=SubMenus)
    KenMenu.AddItem(GroupName='AcHouse.House.More',menuActions=Menu_Actions,SubMenus=SubMenus)    
    KenMenu.SetLevel(0)
    #  Staff Groups and Menu Items

    KenMenu.CreateGroupOnly(GroupName='Staff')
    KenMenu.SetBaseGroup('Staff')
    KenMenu.AddItem(GroupName='Kensington',MenuName='Staff')
    KenMenu.SetBaseGroup('Staff.Kensington')
    KenMenu.Up()
    KenMenu.AddItem(GroupName='LowerSchool' ,MenuName='Lower School',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='LowerSchool.Form',MenuName='LS Form',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])    
    KenMenu.AddItem(GroupName='LowerSchool.Reception',MenuName='Reception',menuActions=Menu_Actions,SubMenus=['List','Labels','Letters'])

    KenMenu.AddItem(GroupName='LowerSchool.Year1',MenuName='Year 1',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='LowerSchool.Year2',MenuName='Year 2',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.Down()
    KenMenu.AddItem(GroupName='MiddleSchool' ,MenuName='Prep School',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='MiddleSchool.Form',MenuName='MS Form',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='MiddleSchool.Year3',MenuName='Year 3',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='MiddleSchool.Year4',MenuName='Year 4',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='MiddleSchool.Year5',MenuName='Year 5',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.Down()

    KenMenu.AddItem(GroupName='UpperSchool' ,MenuName='Upper School',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.Up()
    KenMenu.AddItem(GroupName='UpperSchool.Form',MenuName='US Form',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])    
    KenMenu.AddItem(GroupName='UpperSchool.Year6',MenuName='Year 6',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.Down()

    KenMenu.AddItem(GroupName='Department' ,MenuName='Departments')
    KenMenu.Up()
    KenMenu.AddItem(GroupName='Department.SysAdmin',MenuName='SysAdmin',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='Department.OfficeAdmin',MenuName='Office Admin',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='Department.Transport',MenuName='Transport',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='Department.Maintinance',MenuName='Maintinance',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='Department.Catering',MenuName='Catering',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='Department.CurrHODS',MenuName="Curr-HoD's",menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='Department.CoHODS',MenuName="Co-Curr HoD's",menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])    
    KenMenu.AddItem(GroupName='Department.SLT',MenuName="SLT",menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='Department.PE',MenuName='PE',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])
    KenMenu.AddItem(GroupName='Department.SEN',MenuName='Special Needs',menuActions=Menu_Actions,SubMenus=['Lists','Labels','Letters'])

    KenMenu.Down()
