#-------------------------------------------------------------------------------
# Name:        DefaultData
# Purpose:      Pushes default records into the WillowTree MIS
#
# Author:      DBMgr
#
# Created:     12/02/2012
# Copyright:   (c) DBMgr 2012
# Licence:
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from WillowTree.MIS import models
from MIS.models import *
import datetime, copy, ConfigParser, csv
from ModelFunctions import *
from BatterseaMenu import *
from ClaphamMenu import *
from FulhamMenu import *
from KensingtonMenu import *



def importPupils(FileName,School,AcademicYear):
    ImportItems=csv.DictReader(open(FileName,'rb'))
    for Items in ImportItems:
        day,month,year=Items['Date of birth'].split('-')
        print "%s %s" % (Items['Forename'],Items['Surname'])
        if not Pupil.objects.filter(Old_Id=Items['Id']).exists():
            newPupil=Pupil(Old_Id=Items['Id'],Forename=Items['Forename'], Surname=Items['Surname'], NickName = Items['Called name'],
            OtherNames=Items['Forename 2'],Gender=Items['Sex'][0],DateOfBirth=datetime.datetime(int(year),int(month),int(day)),
            Picture='images/StudentPhotos/%s.jpg' % (Items['Id']),EmailAddress=Items['Email'])
            newPupil.save()
            if Items['Internet Permission']=='1':
                I_Perm=True
            else:
                I_Perm=False
            if Items['Media Permission']=='1':
                M_Perm=True
            else:
                M_Perm=False
            ExtraItems=ExtendedRecord(BaseType='Pupil',BaseId=newPupil.id)
            UpdateData={'UPN':Items['Unique Pupil Number'],'Ethnicity':Items['Ethnicity'],'Religion':Items['Religion'],'Nationality':Items['Nationality'],'PrimaryLanguage':Items['Language'],'EntryYear':Items['Entry Year'],'P_Internet':I_Perm,'P_Media':M_Perm}
            ExtraItems.WriteExtention('PupilExtra',UpdateData)
            if not Address.objects.filter(AddressLine1=Items['Address1'],PostCode=Items['Home post code']).exists():
                NewAddress=Address(HomeSalutation=Items['Home salutation'], PostalTitle=Items['Postal title'],
                                   AddressLine1=Items['Address1'],AddressLine2=Items['Address2'],AddressLine3=Items['Address3'],AddressLine4=Items['Address4'],
                PostCode=Items['Home post code'],Phone1=Items['Home phone number'],Phone2=Items['Mobile phone number'],EmailAddress=Items['Email'])
                NewAddress.save()
                FamilyID=Family(FamilyName='%s-%d' % (Items['Surname'],newPupil.id))
                FamilyID.save()
                FamilyID.Address.add(NewAddress)
            else:
                if Family.objects.filter(Address=Address.objects.get(AddressLine1=Items['Address1'],PostCode=Items['Home post code'])).exists():
                    FamilyID=Family.objects.get(Address=Address.objects.get(AddressLine1=Items['Address1'],PostCode=Items['Home post code']))
                else:
                    FamilyID=Family(FamilyName='%s-%d' % (Items['Surname'],newPupil.id))
                    FamilyID.save()
            NewFamilyChild=FamilyChildren(FamilyId=FamilyID,Pupil=newPupil)
            NewFamilyChild.save()
                    
            
            
        else:
            newPupil=Pupil.objects.get(Old_Id=Items['Id'])

        
        GroupFunctions.AssignGroup(newPupil.id,Items['Class'],School,AcademicYear)
        GroupFunctions.AssignGroup(newPupil.id,Items['Ac House'],School,AcademicYear)
        
def importContacts(FileName,School,AcademicYear):
    ImportItems=csv.DictReader(open(FileName,'rb'))
    for Items in ImportItems:
        if not Contact.objects.filter(Old_Id=Items['Id']).exists():              
            print "%s %s" % (Items['Forename'],Items['Surname'])
            NewContact=Contact(Forename=Items['Forename'],Surname=Items['Surname'],Title=Items['Title'],Gender=Items['Sex'][0],EmailAddress=Items['Email'],Old_Id=Items['Id'])
            NewContact.save()
            if not Address.objects.filter(AddressLine1=Items['Address1'],PostCode=Items['Home post code']).exists():
                print len(Items['Address1'])
                if len(Items['Address1']) > 2:
                    AddressId=Address(HomeSalutation=Items['Home salutation'], PostalTitle=Items['Home postal title'],
                                      AddressLine1=Items['Address1'],AddressLine2=Items['Address2'],AddressLine3=Items['Address3'],AddressLine4=Items['Address4'],
                                      PostCode=Items['Home post code'],Phone1=Items['Home phone number'],Phone2=Items['Mobile phone number'],EmailAddress=Items['Email'])
                    AddressId.save()
            else:
                if len(Items['Address1']) > 2:
                    AddressId=Address.objects.get(AddressLine1=Items['Address1'],PostCode=Items['Home post code'])
            if len(Items['Address1']) > 2:
                NewContact.Address=AddressId
                NewContact.save()
        else:
            NewContact=Contact.objects.get(Old_Id=Items['Id'])
                    
        # find Family and attach if not already attached
        if FamilyChildren.objects.filter(Pupil__Old_Id=Items['PupilId'],FamilyType=1).exists():
            FamilyDetails=FamilyChildren.objects.get(Pupil__Old_Id=Items['PupilId'],FamilyType=1).FamilyId
            if not FamilyContact.objects.filter(FamilyId=FamilyDetails,Contact=NewContact).exists():
                if Items['Relationship'].find('PAM') > -1:
                    Rel='Mother'
                else:
                    Rel='Father'
                NewFamilyContact=FamilyContact(FamilyId=FamilyDetails,Contact=NewContact,Relationship=FamilyRelationship.objects.get(Type=Rel),Priority=int(Items['Priority']))
                NewFamilyContact.save()
        
                
                    
                
                    
                    
                               
        