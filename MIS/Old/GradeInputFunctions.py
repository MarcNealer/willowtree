from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache


class GradeInputFormPupil():
    ''' This class will create the HTML input forms to be embeded into
    django templates for Grade entering. 
    To use this, create the object, passing the Pupilid and the 
    extention record name. It will build the required forms for that
    Pupil. '''
    def __init__(self,xPupilId,xExtRecord):
        pass
    def InputForm():
        # Get a list of subjecst and forms from GradeInputElement
        # Use this list to get all the current data 
        # Use this leist to get all Previous Terms data
        # Build the input body form and render using the above data
        # Return the rendered inputform to the template calling this method
        pass
    def GetFormList(self):
        ''' Gets a list of the input forms based upon the Extention records
        and the groups the child belongs to '''
        pass
    def GetGradeData(self):
        ''' Gets the Grade data and puts it in cache before rendering '''
        pass
    def GetThisTermsData(self):
        ''' Returns the previous Term based upon this term'''
        pass
    def GetLastTermsData(self):
        pass
    def BuildInputTemplate(self):
        pass
    def RenderInputTemplate(self):
        pass
    
class GradeInputFormGroup():
    def __init__(self,xGroupId,xExtRecord,xSubject):
        pass
    def InputForm():
        # Get a list of subjecst and forms from GradeInputElement
        # Use this list to get all the current data 
        # Use this leist to get all Previous Terms data
        # Build the input body form and render using the above data
        # Return the rendered inputform to the template calling this method
        pass
    def GetFormList(self):
        ''' Gets a list of the input forms based upon the Extention records
        and the groups the child belongs to '''
        pass
    def GetGradeData(self):
        ''' Gets the Grade data and puts it in cache before rendering '''
        pass
    def GetThisTermsData(self):
        ''' Returns the previous Term based upon this term'''
        pass
    def GetLastTermsData(self):
        pass
    def BuildInputTemplate(self):
        pass
    def RenderInputTemplate(self):
        pass
        
        
        
    
