from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache


class ExtendedRecord():
    def __init__(self,BaseType,BaseId,request=None):
        self.BaseType=BaseType
        self.request=request 
        if self.BaseType=='Pupil':
            self.BaseId=Pupil.objects.get(id=int(BaseId))
        elif self.BaseType=='Contact':
            self.BaseId=Contact.objects.get(id=int(BaseId))
        elif self.BaseType=='Staff':
            self.BaseId=Staff.objects.get(id=int(BaseId))
        elif self.BaseType=='Family':
            self.BaseId=Family.objects.get(id=int(BaseId))
        elif self.BaseType=='School':
            self.BaseId=School.objects.get(id=int(BaseId))
        else:
            return False
    def WriteExtention(self,ExtentionName,ExtentionData,SubjectName=None):
        '''Writes/Updates data in the extentions system. 
        Usage
        
        Create the object with BaseType and Id 
        Newobject=ExtendedRecord(request,BaseType='Pupil',BaseId=4)
        Then write data
        NewObject.WriteExtention(ExtentionName='Yr3_EndofTerm_Mich',ExtentionData={#Field:Data},SubjectName=#Optional)
        '''
        # get the extention index id via GetExtentionRecordId
        ExtentionIndexId=self.GetExtentionRecordIdForUpdate(ExtentionName,SubjectName)
        
        # get a list of the fields for looping through vua GetFields
        for eachField in self.GetFieldList(ExtentionName):
            
        # loop through the fields.
            if eachField['FieldName'] in ExtentionData:
                #Save each found field Item
                self.SaveFieldData(ExtentionIndexId,eachField,ExtentionData[eachField['FieldName']])
        return
        
    def ReadExtention(self,ExtentionName,SubjectName=None):
        ''' This methon if passed and Extention Name and Subject will return 
        a completed dictionary with each key being the the field name and the
        value being a list where element 0 is the data and Element 1 is the #
        alternative value. If there is no alternative value, the a blank is returned'''
        #get the ExtextionIndex Number
        ExtentionIndexId=self.GetExtentionRecordIdForRead(ExtentionName,SubjectName)
        # get a list of the fields for the extentionName
        ExtentionData={}
        if not type(ExtentionIndexId).__name__=='bool':
            for eachField in self.GetFieldList(ExtentionName):
                #loop through the fields and read the data from the table and store in the dictionary
                ExtentionData[eachField['FieldName']]=self.ReadFieldData(ExtentionIndexId,eachField)
            # Return the completed dictionary
        return ExtentionData
    def GetFieldList(self,ExtentionName):
        ''' Get a list of fields for a selected Extention Record, Returned data
        in a List of Dictionaries, where each Dictionary is of the format
        Fieldname,FieldType,FieldId, Picklist'''
        ThisExtentionRecord=ExtentionRecords.objects.get(Name=ExtentionName)
        ThisFieldsList=[]
        for eachField in ThisExtentionRecord.Fields.all():
            try:
                Picklistdata=eval(eachField.PickList.Data)
            except:
                Picklistdata=[]
            ThisFieldsList.append({'FieldName':eachField.Name,'FieldType':eachField.Type,'FieldId':eachField.id,'PickList':Picklistdata})
        return ThisFieldsList
        
    def GetExtentionRecordIdForUpdate(self,ExtentionName,SubjectName):
        ''' check to see if the extention record already exists.
        if not, create the base extention record and the extention index.
        Return the extention index id in either case. This also updates
        the updateby and UpdatedOn fields'''
        if self.BaseType=='Pupil':
            if ExtentionForPupil.objects.filter(PupilId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForPupil.objects.get(PupilId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)
                ThisRecord.ExtentionUpdatedOn=datetime.datetime.now()
                if not self.request==None:
                    ThisRecord.ExtentionUpdatedBy=Staff.objects.get(id=self.request.session['StaffId'])
                ThisRecord.save()
                return ThisRecord                
            else:
                NewIndex=ExtentionsIndex(ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName))
                NewIndex.save()
                if not self.request==None:
                    NewRecord=ExtentionForPupil(PupilId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now(),ExtentionUpdatedBy=Staff.objects.get(id=self.request.session['StaffId']))
                else:
                    NewRecord=ExtentionForPupil(PupilId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now())

                NewRecord.save()
                return NewRecord
                
        elif self.BaseType=='Staff':
            if ExtentionForStaff.objects.filter(StaffId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForStaff.objects.get(StaffId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)                
                ThisRecord.ExtentionUpdatedOn=datetime.datetime.now()
                if not self.request==None:
                    ThisRecord.ExtentionUpdatedBy=Staff.objects.get(id=self.request.session['StaffId'])
                ThisRecord.save()
                return ThisRecord   
            else:
                NewIndex=ExtentionsIndex(ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName))
                NewIndex.save()
                if not self.request==None:
                    NewRecord=ExtentionForStaff(StaffId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now(),ExtentionUpdatedBy=Staff.objects.get(id=self.request.session['StaffId']))
                else:
                    NewRecord=ExtentionForStaff(StaffId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now())

                NewRecord.save()
                return NewRecord
        elif self.BaseType=='Contact':
            if ExtentionForContact.objects.filter(ContactId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForStaff.objects.get(ContactId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)                
                ThisRecord.ExtentionUpdatedOn=datetime.datetime.now()
                if not self.request==None:
                    ThisRecord.ExtentionUpdatedBy=Staff.objects.get(id=self.request.session['StaffId'])
                ThisRecord.save()
                return ThisRecord   
            else:
                NewIndex=ExtentionsIndex(ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName))
                NewIndex.save()
                if not self.request==None:
                    NewRecord=ExtentionForContact(ContactId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now(),ExtentionUpdatedBy=Staff.objects.get(id=self.request.session['StaffId']))
                else:
                    NewRecord=ExtentionForContact(ContactId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now())

                NewRecord.save()
                return NewRecord
        elif self.BaseType=='Family':
            if ExtentionForFamily.objects.filter(FamilyId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForStaff.objects.get(FamilyId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)                
                ThisRecord.ExtentionUpdatedOn=datetime.datetime.now()
                if not self.request==None:
                    ThisRecord.ExtentionUpdatedBy=Staff.objects.get(id=self.request.session['StaffId'])
                ThisRecord.save()
                return ThisRecord                   
            else:
                NewIndex=ExtentionsIndex(ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName))
                NewIndex.save()
                if not self.request==None:
                    NewRecord=ExtentionForFamily(ContactId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now(),ExtentionUpdatedBy=Staff.objects.get(id=self.request.session['StaffId']))
                else:
                    NewRecord=ExtentionForFamily(ContactId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now())

                NewRecord.save()
                return NewRecord
        elif self.BaseType=='School':
            if ExtentionForSchool.objects.filter(SchoolId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForSchool.objects.get(SchoolId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)                
                ThisRecord.ExtentionUpdatedOn=datetime.datetime.now()
                if not self.request==None:
                    ThisRecord.ExtentionUpdatedBy=School.objects.get(id=self.request.session['StaffId'])
                ThisRecord.save()
                return ThisRecord                   
            else:
                NewIndex=ExtentionsIndex(ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName))
                NewIndex.save()
                if not self.request==None:
                    NewRecord=ExtentionForSchool(SchoolId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now(),ExtentionUpdatedBy=Staff.objects.get(id=self.request.session['StaffId']))
                else:
                    NewRecord=ExtentionForSchool(SchoolId=self.BaseId,ExtentionRecordId=ExtentionRecords.objects.get(Name=ExtentionName),ExtentionSubject=SubjectName,ExtentionIndexId=NewIndex,ExtentionUpdatedOn=datetime.datetime.now())

                NewRecord.save()
                return NewRecord
        else:
            return False
    def GetExtentionRecordIdForRead(self,ExtentionName,SubjectName):
        ''' check to see if the extention record already exists.
        if not, create the base extention record and the extention index.
        Return the extention index id in either case. This also updates
        the updateby and UpdatedOn fields'''
        if self.BaseType=='Pupil':
            if ExtentionForPupil.objects.filter(PupilId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForPupil.objects.get(PupilId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)
                return ThisRecord               
            else:
                return False
        elif self.BaseType=='Staff':
            if ExtentionForStaff.objects.filter(StaffId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForStaff.objects.get(StaffId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)                
                return ThisRecord   
            else:
                return False
        elif self.BaseType=='Contact':
            if ExtentionForContact.objects.filter(ContactId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForContact.objects.get(ContactId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)                
                return ThisRecord   
            else:
                return False
        elif self.BaseType=='Family':
            if ExtentionForFamily.objects.filter(FamilyId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForFamily.objects.get(FamilyId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)                
                return ThisRecord                   
            else:
                return False
        elif self.BaseType=='School':
            if ExtentionForSchool.objects.filter(SchoolId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName).exists():
                ThisRecord=ExtentionForSchool.objects.get(SchoolId=self.BaseId,ExtentionRecordId__Name=ExtentionName,ExtentionSubject=SubjectName)                
                return ThisRecord                   
            else: 
                return False                
        else:
            return False            
            
    def SaveFieldData(self,IndexId,FieldDetails,DataElement):
        ''' This method saves a data for a single field. It should be passed
        the data item, the details on the field and the Extention index id 
        Usage
        object.SaveFieldData(IndexId=*, fieldDetails=*, DataElement=*)        
        '''
        if unicode(DataElement) in unicode(FieldDetails['PickList']) and len(DataElement) > 0:
            AlternativeDataElement=FieldDetails['PickList'][[x for x,y in enumerate(FieldDetails['PickList']) if y[0]==unicode(DataElement)][0][1]]
            
        else:
            AlternativeDataElement=''
        if FieldDetails['FieldType']=='String':
            if ExtentionString.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionString.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
            NewField=ExtentionString(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement,AlternativeValue=AlternativeDataElement)
            NewField.save()
        elif FieldDetails['FieldType']=='Numerical':
            if ExtentionNumerical.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionNumerical.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
            NewField=ExtentionNumerical(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement,AlternativeValue=AlternativeDataElement)
            NewField.save()
        elif FieldDetails['FieldType']=='Date':
            if ExtentionDate.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionDate.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
            NewField=ExtentionDate(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement)
            NewField.save()
        elif FieldDetails['FieldType']=='Time':
            if ExtentionTime.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionTime.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
            NewField=ExtentionTime(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement)
            NewField.save()                      
        elif FieldDetails['FieldType']=='Boolean':
            if ExtentionBoolean.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists(): 
                ThisField=ExtentionBoolean.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False 
                ThisField.save()
            NewField=ExtentionBoolean(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement)
            NewField.save()                     
        elif FieldDetails['FieldType']=='TextBox':
            if ExtentionTextBox.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True).exists():
                ThisField=ExtentionTextBox.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True)
                ThisField.Active=False
                ThisField.save()
            NewField=ExtentionTextBox(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=ExtentionFields.objects.get(id=int(FieldDetails['FieldId'])),Active=True,Data=DataElement)
            NewField.save()    
        else:
            return False 
        
    def ReadFieldData(self,IndexId,FieldDetails):
        ''' Using the passed IndexId and Field ID will return the values for
        the said fields in a list'''
        FieldRecord=ExtentionFields.objects.get(id=int(FieldDetails['FieldId']))
        if FieldDetails['FieldType']=='String':
            if ExtentionString.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionString.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,ThisField.AlternativeValue,eval(FieldRecord.PickList.Data)]
            else:
                return ['','',eval(FieldRecord.PickList.Data)]
        elif FieldDetails['FieldType']=='Numerical':
            if ExtentionNumerical.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionNumerical.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,ThisField.AlternativeValue,eval(FieldRecord.PickList.Data)]
            else:
                return ['','',eval(FieldRecord.PickList.Data)]                
        elif FieldDetails['FieldType']=='Date':
            if ExtentionDate.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionDate.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,'',eval(FieldRecord.PickList.Data)]
            else:
                return ['','',eval(FieldRecord.PickList.Data)]                
        elif FieldDetails['FieldType']=='Time':
            if ExtentionTime.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionTime.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,'',eval(FieldRecord.PickList.Data)] 
            else:
                return ['','',eval(FieldRecord.PickList.Data)]                   
        elif FieldDetails['FieldType']=='Boolean':
            if ExtentionBoolean.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionBoolean.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,'',eval(FieldRecord.PickList.Data)]
            else:
                return ['','',eval(FieldRecord.PickList.Data)]                   
        elif FieldDetails['FieldType']=='TextBox':
            if ExtentionTextBox.objects.filter(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True).exists():
                ThisField=ExtentionTextBox.objects.get(ExtentionIndexId=IndexId.ExtentionIndexId,FieldId=FieldRecord,Active=True)
                return [ThisField.Data,'',eval(FieldRecord.PickList.Data)] 
            else:
                return ['','',eval(FieldRecord.PickList.Data)]                
        else:
            return false


class MenuBuilder():
    def __init__(self,MenuType, BaseGroup,Type='New',Subjects=None):
        self.OrderList=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        # set order depth to 0
        self.OrderDepth=0
        # Check to see if the MenuType exists. If not issue error msg
        # Set MenuType and Base Group
        if Type=='New':
            self.NewMenu(MenuType,BaseGroup,Subjects)
        else:
            if MenuTypes.objects.filter(Name=MenuType).exists():
                self.MenuType=MenuTypes.objects.get(Name=MenuType)
                if not Group.objects.filter(Name=BaseGroup).exists():
                    GroupName=''
                    GroupElements=BaseGroup.split('.')
                    for GroupBits in GroupElements:
                        GroupName+=GroupBits
                        if not Group.objects.filter(Name=GroupName).exists():
                            NewGroup=Group(Name=GroupName,MenuName=GroupElements[-1])
                            NewGroup.save()
                        GroupName+='.'
                self.BaseGroup=BaseGroup
                if Subjects:
                    NewGroup=Group.objects.get(Name=BaseGroup)
                    for Items in Subjects:
                        if not Subject.objects.filter(Name=Items):
                            NewSub=Subject(Name=Items,Desc=Items)
                            NewSub.save()
                        NewGroup.Subject.add(Subject.objects.get(Name=Items))
                    NewGroup.save()
            else:
                raise MenuError, "MenuType Does not exist"
    def NewMenu(self,MenuType,BaseGroup,Subjects=None):
        # Check the Menu table and delete items if they already exist
        if Menus.objects.filter(MenuType__Name=MenuType):
            Menus.objects.filter(MenuType__Name=MenuType).delete()
        # Check Base Group. If it exists, create it with the list of subjects (if any)
        self.BaseGroup=BaseGroup
        print MenuType
        self.MenuType=MenuTypes.objects.get(Name=MenuType)
        if not Group.objects.filter(Name=BaseGroup).exists():
            if not Group.objects.filter(Name=BaseGroup).exists():
                NewGroup=Group(Name=BaseGroup,MenuName=BaseGroup.split('.')[-1])
                NewGroup.save()
                if Subjects:
                    NewGroup=Group.objects.get(Name=BaseGroup)
                    for Items in Subjects:
                        if not Subject.objects.filter(Name=Items):
                            NewSub=Subject(Name=Items,Desc=Items)
                            NewSub.save()
                        NewGroup.Subject.add(Subject.objects.get(Name=Items))
                    NewGroup.save()
        # Set the Order list to [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10]
        self.OrderList=[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        # set order depth to 0
        self.OrderDepth=0
        return
    def AddItem(self,GroupName,MenuName=None,Subjects=None,
    ParentSubjects=True,menuActions=None,SubMenus=None, ManagedGroups=None,NotSets=False,Active=True):
        '''
        MenuBuilder.AddItem:

        This method adds a menu Item to the class
        Parameters
        Group : GroupName (required)
        MenuName : MenuName to be assgined to the group if created, else last element in the name is used (NotReq)
        Subjects : List of Subjects that must already exist in the database (NotReq)
        ParentSubjects : Boolean. Get a merged list of subjects from itself and parents (default=True)
        Attendance : Boolean. Is Daily attendance an option (default=False)
        LessonAtt : Boolean. Can it be used for lesson Attendance (default=False)
        Grade : Boolean. Can grades be entered for this groups (default =False)
        SubMenu: List. List of SubMenu options to be available (NotReq)
        ManagedGroups. List. List of Groups to be managed by this group(Set Mangement) (NotReq)
        Active : Boolean. Active (Default=True)
        '''
        # check to see if the group exists and create if not
        if not self.BaseGroup ==None:
            GroupName="%s.%s" % (self.BaseGroup,GroupName)
        GroupId=self.CreateGroupOnly(GroupName,MenuName,Subjects,ParentSubjects)
        # Set menu item in 'Menu' table using current order. Order to be set to the string representation of the number, with leading 0's
        self.OrderList[self.OrderDepth]+=10
        menuOrder='.'.join([str(item) for item in self.OrderList[0:self.OrderDepth+1]])
        if ManagedGroups:
            Managed=True
        else:
            Managed=False
        NewMenuItem=Menus(MenuType=self.MenuType, Order=menuOrder,Group=GroupId,Manage=Managed,Active=Active)
        NewMenuItem.save()
        if ManagedGroups:
            for Groups in ManagedGroups:
                if NotSets:
                    ManagedGroupName=Groups
                else:
                    ManagedGroupName="%s.%s" % (GroupName,Groups)
                Mgroup=self.CreateGroupOnly(ManagedGroupName)
                NewMenuItem.ManagedGroups.add(Mgroup)
        if menuActions:
            for Actions in menuActions:
                NewMenuItem.MenuActions.add(MenuActions.objects.get(Name=Actions))
        if SubMenus:
            for Items in SubMenus:
                if MenuSubMenu.objects.filter(Name=Items).exists():
                    NewMenuItem.SubMenus.add(MenuSubMenu.objects.get(Name=Items))
        NewMenuItem.save()

        return
    def Up(self):
        # Add 1 to the order Depth
        self.OrderDepth+=1
    def Down(self):
        # Reset the OrderList[OrderDepth] to 0
        # subtract 1 from order depth
        # Add 10 to OrderList[orderDepth]
        if self.OrderDepth > 0:
            for elements in range(self.OrderDepth,len(self.OrderList)):
                self.OrderList[elements]=0
            self.OrderDepth-=1

    def SetLevel(self,LevelNo):
        # Set orderDepth to LevelNo
        # for Items in OrderList from LevelNo+1 to the end, set to 0
        # add 10 to OrderList[orderdepth]
        if self.OrderDepth > 0 and LevelNo > -1:
            for elements in range(LevelNo+1,len(self.OrderList)):
                self.OrderList[elements]=0
            self.OrderDepth=LevelNo
    def SetBaseGroup(self,BaseGroup):
        if not BaseGroup==None:
            if not Group.objects.filter(Name=BaseGroup).exists():
                GroupName=''
                GroupElements=BaseGroup.split('.')
                for GroupBits in GroupElements:
                    GroupName+=GroupBits
                    if not Group.objects.filter(Name=GroupName).exists():
                        NewGroup=Group(Name=GroupName,MenuName=GroupElements[-1])
                        NewGroup.save()
                    GroupName+='.'
        self.BaseGroup=BaseGroup
    def CreateGroupOnly(self,GroupName,MenuName=None,Subjects=None,ParentSubjects=True):
        if not Group.objects.filter(Name=GroupName).exists():
            if not MenuName:
                MenuName=GroupName.split(".")[-1]
            Gp=Group(Name=GroupName,MenuName=MenuName,ParentSubjects=ParentSubjects)
            Gp.save()
            if Subjects:
                for Items in Subjects:
                    if Subject.objects.filter(Name=Subject).exists():
                        Gp.Subject.add(Subject.objects.get(Name=Subject))
                Gp.save()
        else:
            Gp=Group.objects.get(Name=GroupName)
        return Gp
    def GetBaseGroup(self):
        return self.BaseGroup

    @staticmethod
    def BuildMenu(MenuName,AcYear):
        if 'Applicants' in MenuName:
            ReturnMenu=''
            for Years in range(1,5):
                ThisAcYear=WillowFunc.get_FutureAcYear(AcYear,Years)
                ReturnMenu+="<li><a href='#'>%s *</a>\r\n<ul>\r\n" % ThisAcYear
                ReturnMenu+=MenuBuilder.AllGroups(MenuName,ThisAcYear)
                ReturnMenu+="</ul></li>"
            return ReturnMenu
        else:
            return MenuBuilder.AllGroups(MenuName,AcYear)
                
        
    @staticmethod
    def AllGroups(MenuName,AcYear):
        MenuDetails=Menus.objects.filter(MenuType__Name=MenuName,Active=True)
        DPE=0
        for items in MenuDetails:
            ItemDPE=len(items.Order.split('.'))
            if ItemDPE > DPE:
                DPE=ItemDPE
        UpperMap={}
        CurrentMap={}
        for counter in range(DPE,0,-1):
            for items in MenuDetails:
                menuLevel=items.MenuType.MenuLevel
                if len(items.Order.split('.'))==counter:
                    itemshtml= "<li><a href='#'>%s *</a>\r\n<ul>\r\n" % items.Group.MenuName

                    if items.Manage==True:
                        GroupList=''
                        for Groups in items.ManagedGroups.all():
                            GroupList+='%d-' % Groups.id
                        GroupList=GroupList[0:-1]
                        itemshtml+="<li><a href='/WillowTree/%s/%s/SetManager/%s/%s/'>Manage Set</a></li>\r\n" % (MenuName,AcYear,items.Group.id,GroupList)

                    itemshtml+="<li><a href='#'>Tasks</a>\r\n"
                    itemshtml+="<ul class='Tasks'>\r\n <li>\r\n"
                    itemshtml+="<table border=0><tr>\r\n"
                    itemshtml+="<td valign='top'>\r\n"
                    itemshtml+='<dl>\r\n <dt>Actions</dt>\r\n'                    
                    GroupActions=[]
                    for Actions in items.MenuActions.all().order_by('MenuOrder'):

                        GroupActions.append(Actions)
                        itemshtml+="<dd><a href='/WillowTree/%s/%s/%s/%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,Actions.Name,items.Group,Actions.MenuName)
                    itemshtml+="</dl>\r\n </td>"
                    for Sub_Menus in items.SubMenus.all().order_by('MenuOrder'):
                        Found_Items=False
                        Temp_HTML="<td valign='top'>\r\n  <dl>\r\n"
                        Temp_HTML+="<dt>%s</dt>\r\n" % Sub_Menus.Name
                        GroupsIn=GroupFunctions.SubGroups(items.Group.Name)
                        Lists=GroupMenuItems.objects.filter(Type__Name=Sub_Menus.Name,Group__Name__in=GroupsIn)

                        # Go through the items in to add to the submenu. Checks each to see if
                        # they should only be avaiable to items that have other elements already
                        # defined such as being a set manger, have attendance required, or
                        # have s selected name
                        for menuitems in Lists:
                            if menuitems.MenuLevel >= menuLevel:
                                if menuitems.GroupRequirements=='Managed':
                                    if items.Manage:
                                        Temp_HTML+="<dd><a href='/WillowTree/%s/%s%s%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,menuitems.MenuItem.ItemURL,items.Group,menuitems.MenuItem.Name)
                                        Found_Items=True
                                elif menuitems.GroupRequirements=='Attenance':
                                    if 'Attendance' in GroupActions:
                                        Temp_HTML+="<dd><a href='/WillowTree/%s/%s%s%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,menuitems.MenuItem.ItemURL,items.Group,menuitems.MenuItem.Name)
                                        Found_Items=True
                                elif menuitems.GroupRequirements=='LessonAtt':
                                    if 'LessonAtt' in GroupActions:
                                        Temp_HTML+="<dd><a href='/WillowTree/%s/%s%s%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,menuitems.MenuItem.ItemURL,items.Group,menuitems.MenuItem.Name)
                                        Found_Items=True
                                elif menuitems.GroupRequirements=='NameContains':
                                     if items.Group.Name.find(menuitems.ReqParms):
                                        Temp_HTML+="<dd><a href='/WillowTree/%s/%s%s%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,menuitems.MenuItem.ItemURL,items.Group,menuitems.MenuItem.Name)
                                        Found_Items=True
                                else:
                                    Temp_HTML+="<dd><a href='/WillowTree/%s/%s%s%s/'>%s</a></dd>\r\n" % (MenuName,AcYear,menuitems.MenuItem.ItemURL,items.Group,menuitems.MenuItem.Name)
                                    Found_Items=True

                        Temp_HTML+="</dl>\r\n</td>\r\n"
                        if Found_Items:
                            itemshtml+=Temp_HTML
                    itemshtml+="</tr>\r\n</table>\r\n"
                    itemshtml+="</li>\r\n </ul>\r\n"                    
                    for UpperElements in range(0,500,10):
                        keyItem="%s.%s" %(items.Order,str(UpperElements))
                        if UpperMap.has_key(keyItem):
                            itemshtml+=UpperMap[keyItem]
                    itemshtml+="\r\n</ul></li>\r\n"
                    CurrentMap[items.Order]=itemshtml

            UpperMap=copy.deepcopy(CurrentMap)
            CurrentMap={}
        MenuItems=UpperMap.keys()
        MenuItems.sort()
        ReturnMenu=''

        for keys in MenuItems:
            ReturnMenu+=UpperMap[keys]
        return ReturnMenu
        
class StaffRecord():
    def __init__(self,request,StaffId,AcYear):
        self.StaffId=StaffId
        self.AcYear=AcYear
        self.request=request
        self.Staff=Staff.objects.get(id=StaffId)
    def Extended(self):
        return ExtendedRecord(BaseType='Staff',BaseId=self.Staff.id,request=self.request).ReadExtention(ExtentionName='StaffExtra')
    def GroupList(self):
        return GroupFunctions.ListStaffGroups(self.StaffId,self.AcYear)
    def EmergencyContacts(self):
        #return StaffEmegencyContacts.objects.filter(StaffId=self.Staff)
        pass
    def StaffIssueCount(self):
        return self.ActiveStaffIssues()
    def StaffDocuments(self):
        return StaffDocument.objects.filter(StaffId=self.Staff)
    def ActiveStaffIssues(self):
        if StaffIssues.objects.filter(StaffId=self.StaffId).exclude(IssueId__IssueStatus='Closed').exists():
            return len(StaffIssues.objects.filter(StaffId=self.StaffId).exclude(IssueId__IssueStatus='closed'))
        else:
            return False
    def AttachIssue(self,IssueRecord):
        StaffIssueRecord=StaffIssues(StaffId=self.StaffId,IssueId=IssueRecord)
        StaffIssueRecord.save()
        return
    def GetIssueRecords(self):
        IssueRecords=StaffIssues.objects.filter(StaffId=self.StaffId)
        return IssueRecords   
    def AddAnIssue(self):
        KWList=' '.join(self.request.POST.getlist('KWSelect'))
        IssueRecord=IssueDetails(Keywords=KWList,IssueStatus='Open',IssueText=self.request.POST['IssueTextArea'],RaisedBy=self.request.session['StaffId'],
        RaisedDate=datetime.datetime.now(),ModifiedBy=self.request.session['StaffId'],ModifiedDate=datetime.datetime.now())
        IssueRecord.save()
        self.AttachIssue(IssueRecord)
        return IssueRecord   
    def GetIssues(self):
        IssueRecords=self.GetIssueRecords()
        Filters=''
        if not 'IssuesShowAll' in self.request.session:
            IssueRecords=IssueRecords.exclude(IssueId__IssueStatus='Closed')
            Filters+=('Active Only')
        else:
            if not self.request.session['IssuesShowAll']:
                IssueRecords=IssueRecords.exclude(IssueId__IssueStatus='Closed')
                Filters+=('Active Only')
        if 'IssuesKWordFilter' in self.request.session:
            Filters+=','.join(self.request.session['IssuesKWordFilter'])
            for words in self.request.session['IssuesKWordFilter']:
                IssueRecords=IssueRecords.filter(IssueId__Keywords__icontains=words)
        if 'IssuesStartFilter' in self.request.session:
            IssueRecords=IssueRecords.filter(IssueId__RaisedDate__gte=self.request.session['IssuesStartFilter'])
            Filters+=' StartDate:%s ' % self.request.session['IssuesStartFilter']
        if 'IssuesStopFilter' in self.request.session:
            IssueRecords=IssueRecords.filter(IssueId__RaisedDate__gte=self.request.session['IssuesStopFilter'])
            Filters+=' StopDate:%s ' % self.request.session['IssuesStopFilter']
        return {'IssueRecords':IssueRecords,'Filters':Filters}            

def PupilRecord(PupilId,AcYear):  
    Pupilrec=cache.get('Pupil_%s' % PupilId)
    if not Pupilrec:
        PupilOb=PupilObject(PupilId,AcYear)
        cache.set('Pupil_%s' % PupilId,PupilOb,900)
        return PupilOb
    else:
        return Pupilrec
       
class PupilObject():
    ''' Use this Class to create objects to collect all the infomation on a pupil
    inlcuding alerts and issues. Notes must have their own class as they may be
    attached to multiple pupils'''
    def __init__(self,PupilId,AcYear):
        self.PupilId=PupilId
        self.AcYear=AcYear
        self.Pupil=Pupil.objects.get(id=PupilId)
        self.ExtendedRecord=None
        self.ContactList=None
        self.EmergencyContacts()        
        self.EmergencyTable=None
        self.EmergencyUnordered=None
        
        self.SiblingList=None
        self.siblingsTable=None
        self.SiblingsUL=None
        
        self.FamilyRec=None
        self.FamilyRecord()
        
        self.Pupil_Status=None
        
        self.AcHouse=None
        self.AcademicHouse()
        
        self.PupilForm=None
        self.Form()
        
        self.PupilGroupList=None
        self.GroupList()
        
        self.PupilDocs=None
        self.PupilDocuments()

        

    def FamilyRecord(self):
        if not self.FamilyRec:
            self.FamilyRec=FamilyChildren.objects.get(Pupil=self.Pupil,FamilyType=1).FamilyId    
        return self.FamilyRec
    def PupilStatus(self):
        if not self.Pupil_Status:
            if PupilGroup.objects.filter(Pupil=self.Pupil,Group__Name__icontains='Current', AcademicYear=self.AcYear).exists():
                self.Pupil_Status= 'Current'
            elif PupilGroup.objects.filter(Pupil=self.Pupil,Group__Name__icontains='Applicant', AcademicYear='Applicants').exists():
                self.Pupil_Status= 'Applicant'
            elif PupilGroup.objects.filter(Pupil=self.Pupil,Group__Name__icontains='Alumni', AcademicYear='Alumni').exists():        
                self.Pupil_Status= 'Alumni'
            else:
                self.Pupil_Status= 'Not found'        
        return self.Pupil_Status
    def Extended(self):
        ''' Creates a dictionary of the extended field data for a given child. These are
        the fields listed in the PupilExtra extended record'''
        if not self.ExtendedRecord:
            self.ExtendedRecord=ExtendedRecord(BaseType='Pupil',BaseId=self.Pupil.id).ReadExtention(ExtentionName='PupilExtra')
        return self.ExtendedRecord
        
        
    def AcademicHouse(self):
        if not self.AcHouse:
            self.AcHouse=GroupFunctions.Get_PupilSet("House",self.PupilId,self.AcYear)
        return self.AcHouse
    def Form(self):
        if not self.PupilForm:
            GroupSet=GroupFunctions.ListPupilGroups(self.PupilId,self.AcYear)
            FindForm=False
            for items in GroupSet:
                if not FindForm:
                    if items.find('Form') > -1:
                        self.PupilForm=items.split('.')[5]
                        FindForm=True
                    elif items.find('Applicant') > -1:
                        self.PupilForm='Applicant'
                    else:
                        self.PupilForm='Alumni'
        return self.PupilForm
        
    def GroupList(self):
        if not self.PupilGroupList:
            self.PupilGroupList=GroupFunctions.ListPupilGroups(self.PupilId,self.AcYear)
        return self.PupilGroupList
        
    def EmergencyContacts(self):
        ''' creates and returns a list of Emergency contact records '''
        self.ContactList=[]
        if PupilContact.objects.filter(PupilId=self.Pupil).exists():
            ContactDetails=PupilContacts.objects.filter(PupilId=self.Pupil).order_by('Priority')
            for Contacts in ContactDetails:
                self.ContactList.append(Contacts.FamilyContactId)
        else:
            ContactDetails=FamilyContact.objects.filter(FamilyId=FamilyChildren.objects.get(Pupil=self.Pupil,FamilyType=1).FamilyId).order_by('Priority')
            for Contacts in ContactDetails:
                self.ContactList.append(Contacts)
        return self.ContactList
        
    def EmergencyContactsUL(self):
        ''' Returns the emergency contact records as an unordered list'''
        if not self.EmergencyUnordered:
            if not self.ContactList:
                self.EmergencyContacts()
            Template=template.loader.get_template('includes/EmergencyContactsList.html')
            self.EmergencyUnordered=Template.render(template.Context({'ContactList':self.ContactList,'Type':'List'}))
        return self.EmergencyUnordered 
        
    def EmergencyContactsTable(self):
        ''' Returns the emergency contact records as a table '''
        if not self.EmergencyTable:
            if not self.ContactList:
                self.EmergencyContacts()
            Template=template.loader.get_template('includes/EmergencyContactsList.html')
            self.EmergencyTable=Template.render(template.Context({'ContactList':self.ContactList,'Type':'Table'}))
        return self.EmergencyTable         
        
    def Siblings(self):
        ''' Creates a list of siblings of the selected pupil '''
        self.SiblingList=[]
        SiblingRecs=FamilyChildren.objects.filter(FamilyId=FamilyChildren.objects.get(Pupil=self.Pupil,FamilyType=1).FamilyId).exclude(Pupil=self.Pupil)
        for Sibs in SiblingRecs:
            self.SiblingList.append(PupilRecord(Sibs.Pupil.id,self.AcYear))
        return self.SiblingList
        
    def SiblingsUL(self):
        ''' Returns the siblings as an unorderd list '''
        if not self.siblingsUnordered:
            if not self.SiblingsList:
                self.Siblings()
            Template=template.loader.get_template('includes/SiblingsList.html')
            self.siblingsUnordered=Template.render(template.Context({'SiblingList':self.SiblingList,'Type':'List'}))
        return self.sibingsUnordered
        
    def SiblingsTable(self):
        ''' Returns the siblings list as a table '''
        if not self.siblingsTable:
            if not self.SiblingsList:
                self.Siblings()
            Template=template.loader.get_template('includes/SiblingsList.html')
            self.siblingsTable=Template.render(template.Context({'SiblingList':self.SiblingList,'Type':'Table'}))
        return self.sibingsTable        
            
                
    def PupilDocuments(self):
        if not self.PupilDocs:
            self.PupilDocs=PupilDocument.objects.filter(PupilId=self.Pupil)
        return self.PupilDocs
        
    def PupilIssueCount(self):
        return PupilNotes.objects.filter(PupilId=self.Pupil,NoteId__IsAnIssue=True,NoteId__Active=True).exclude(NoteId__IssueStatus='Closed').count()
        
    def ActivePupilAlerts(self):
        self.AlertList=[]
        if PupilAlert.objects.filter(PupilId=self.PupilId,Active=True,StartDate__lte=datetime.datetime.today()).exists:
            Alerts=PupilAlert.objects.filter(PupilId=self.PupilId,Active=True,StartDate__lte= datetime.datetime.today).order_by('AlertType__AlertGroup__DisplayOrder')
            self.AlertList.append(copy.deepcopy(Alerts))
            return self.AlertList
        else:
            return ''

         
            

class FamilyRecord():
    def __init__(self,request,AcYear,PupilId=None,FamilyId=None,GroupId=None, PupilList=None):
        self.request=request
        self.AcYear=AcYear
        self.FamilyId='Not assigned'
        self.PupilId=PupilId
        if not FamilyId==None:
            self.FamilyId=Family.objects.get(id=int(FamilyId))
        elif not PupilId==None:
            self.PupilDetails=PupilRecord(PupilId,AcYear)
            self.FamilyId=FamilyChildren.objects.get(Pupil=self.PupilDetails.Pupil.id).FamilyId
        if not GroupId==None:
            self.Group=Group.objects.get(id=int(GroupId))
            self.GroupList=GroupFunctions.Get_PupilList(AcYear,GroupName=self.Group.Name)
        elif not PupilList==None:
            self.GroupId=None
            self.GroupList=PupilList
        else:
            self.GroupId=None
            self.GroupList=[]

        self.FamilyPupils=[]
        for FamilyPupils in FamilyChildren.objects.filter(FamilyId=self.FamilyId):
            if not (GroupId==None and PupilList==None):
                if self.PupilInGroup(FamilyPupils.Pupil.id):
                    self.FamilyPupils.append(PupilRecord(FamilyPupils.Pupil.id,self.AcYear))
            else:
                self.FamilyPupils.append(PupilRecord(FamilyPupils.Pupil.id,self.AcYear))            
            
            

        if len(self.FamilyPupils)==1:
            self.Forenames=self.FamilyPupils[0].Pupil.FirstName()
            self.ChildChildren='child'
            if self.FamilyPupils[0].Pupil.Gender=='M':
                self.HeSheThey='He'
                self.heshethey='he'
                self.HisHerTheir='His'
                self.hishertheir='his'
            else:
                self.HeSheThey='She'
                self.heshethey='she'
                self.HisHerTheir='Her'
                self.hishertheir='her'                    
        else:
            self.ChildChildren='children'
            self.HeSheThey='They'
            self.heshethey='they'
            self.HisHerTheir='Their'
            self.hishertheir='their'                
            self.Forenames=""
            Counter=0
            for records in self.FamilyPupils:
                if Counter==len(self.FamilyPupils)-1:
                    self.Forenames+=" and %s" % records.Pupil.FirstName() 
                else:
                    self.Forenames+=" %s, " % records.Pupil.FirstName()
                Counter+=1
            
    def PupilInGroup(self,PupilId):
        ''' scans the PupilList and sees if a child from the selected family is in the
        selected Group'''
        if len(self.GroupList) > 0:
            for records in self.GroupList:
                if records.id==PupilId:
                    return True
            return False
        else:
            return True
    def FamilyContacts(self):
        return FamilyContact.objects.filter(FamilyId=self.FamilyId).order_by('Priority')
    def GenerateLetterBody(self,LetterDetails,VarList,OtherItems,LetterType='Family'):
        t=template.Template(LetterDetails.BodyText.replace('&quot;','"'))
        if LetterType=='Family':
            c=template.Context({'Family':self,'Var':VarList})
        else:
            c=template.Context({'Pupil':PupilRecord(self.PupilId,self.AcYear),'Var':VarList,'OtherItems':OtherItems})
        self.LetterBody=t.render(c)
        return

class WillowFunc:
    ''' WillowFunc is a collection of functions for use in the WillowTree DB system
    but are not part of a specific object'''
    @staticmethod
    def Get_SystemVar(VariableName,AcYear=None):
        VarDetails=SystemVariable.objects.get(Name=VariableName)
        if 'Yearly' in VarDetails.VariableType:
            if not AcYear:
                AcYear=WillowFunc.Get_SystemVar(CurrentYear)
        else:
            if not AcYear:
                AcYear='System'
                
        if SystemVarData.objects.filter(Variable=VarDetails,AcademicYear=AcYear).exists():
            Sysdata=SystemVarData.objects.get(Variable=VarDetails,AcademicYear=AcYear)
            if VarDetails.DataType=='Numerical':
                return float(Sysdata.Data)
            elif VarDetails.DataType=='Date':
                return datetime.datetime.strptime(Sysdata.Data,'%Y-%m-%d')
            elif VarDetails.DataType=='Boolean':
                return eval(Sysdata.Data)
            else:
                return Sysdata.Data
        else:
            return False
        
    @staticmethod
    def Write_SystemVar(VariableName,VariableData,AcYear=None):
        VarDetails=SystemVariable.objects.get(Name=VariableName)
        if 'Yearly' in VarDetails.VariableType:
            if not AcYear:
                AcYear=WillowFunc.Get_SystemVar(CurrentYear)
        else:
            AcYear='System'
        if SystemVarData.objects.filter(Variable=VarDetails,AcademicYear=AcYear).exists():
            Sysdata=SystemVarData.objects.get(Variable=VarDetails,AcademicYear=AcYear)
            if VarDetails.DataType=='Date':
                Sysdata.DataVariableData.strftime('%Y-%m-%d')
            else:
                Sysdata.Data=str(VariableData)
        else:
            if VarDetails.DataType=='Date':
                Sysdata=SystemVarData(Variable=VarDetails,AcademicYear=AcYear,Data=VariableData.strftime('%Y-%m-%d'))
            else:
                Sysdata=SystemVarData(Variable=VarDetails,AcademicYear=AcYear,Data=str(VariableData))   
        Sysdata.save()
        return
    @staticmethod
    def get_FutureAcYear(AcYear,NoYears):
        Year1, Year2 = AcYear.split(AcYear)
        return '%d/%d' % (int(Year1)+NoYears,int(Year2)+NoYears)
        



class KeywordList():
    ''' This class produces an object for accessing the Keywords
    for Notes and Issues '''
    def __init__(self,Type):
        self.Keywords=NoteKeywords.objects.filter(Active=True).order_by('Keyword')
    def Primary(self):
        return self.Keywords.filter(KeywordType='Primary')
    def Secondary(self):
        return self.Keywords.filter(KeywordType='Secondary')
    def Tertiary(self):
        return self.Keywords.filter(KeywordType='Tertiary')
    def Subject(self):
        return self.Keywords.filter(KeywordType='Subject')    
    def HousePointWords(self):        
        return self.Keywords.filter(HousePointsTrigger=True)    
    def All(self):
        return self.Keywords
    def StringListOnlyNoRepeats(self):
        Keywords = self.Keywords.values('Keyword').distinct() 
        KeywordListStringOnly = []
        for i in Keywords:
            KeywordListStringOnly.append(i.values()[0])
        return KeywordListStringOnly

class GroupFunctions():
    # Functions to read and modify pupil groups
    @staticmethod
    def AssignGroup(PupilId,GroupName,School,AcYear):
        ''' AssignGroup

        Simple static method used to assign a pupil to a selected group.

        Usage

        WillowFunc.AssginGroup(PupilId,'GroupName','Academic Year')
        '''
        PupilRec=Pupil.objects.get(id=PupilId)
        if len(GroupName) > 0 and Group.objects.filter(Name__iendswith=GroupName,Name__icontains=School,Name__startswith='Current').exists():
            if PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name__iendswith=GroupName,Name__icontains=School,Name__startswith='Current')).exists():
                OldGroup=PupilGroup.objects.get(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name__iendswith=GroupName,Name__icontains=School,Name__startswith='Current'))
                OldGroup.Active=False
                OldGroup.DateInactivated=datetime.date.today()
                OldGroup.save()
                
            groupclass = PupilGroup(AcademicYear=AcYear,Pupil=Pupil.objects.get(id=PupilId),Group=Group.objects.get(Name__iendswith=GroupName,Name__istartswith='Current.%s' % School))
            groupclass.save()
   
            
        elif  Group.objects.filter(Name='Current').exists() and not PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name='Current')).exists():
            
            groupassign = PupilGroup(AcademicYear=AcYear,Pupil=PupilRec,Group=Group.objects.get(Name='Current'))
            groupassign.save()
            
        if not PupilGroup.objects.filter(Group__Name__icontains='Alumni.%s' % School,Pupil=PupilRec).exists():
            groupAlumni = PupilGroup(Group=Group.objects.get(Name='Alumni.%s' % School),Pupil=PupilRec)
            groupAlumni.save()            

    @staticmethod
    def ChildGroups(GroupName):
        ''' This Method accepts a GroupName as a String and returns a set collection
        of the Childroups for use in searching for database matches.

        Example 'Current.Battersea.Year1' will return
        ['Current.Battersea.Year1.1BE','Current.Battersea.Year1.1BN','Current.Battersea.Year1.1BW']
        '''
        Groups=set()
        GroupList=Group.objects.filter(Name__contains=GroupName)
        for groups in GroupList:
            Groups.add(groups.Name)
        return Groups
    @staticmethod
    def RemoveFromSubGroups(Groupid,PupilId,AcYear):
        msg_return=[]
        GroupName=Group.objects.get(id=int(Groupid))
        Grouplist=Group.objects.filter(Name__contains=GroupName)
        for groups in Grouplist:
            if PupilGroup.objects.filter(AcademicYear=AcYear,Group=groups,Pupil=Pupil.objects.get(id=int(PupilId))).exists():
                PupilGroup.objects.get(AcademicYear=AcYear,Group=groups,Pupil=Pupil.objects.get(id=int(PupilId))).delete()
                msg_return.append(groups.Name)
        return msg_return        
        
    @staticmethod
    def SubGroups(GroupName):
        ''' This Method accepts a GroupName as a String and returns a set collection
        of the subgroups for use in searching for database matches.

        Example 'Current.Battersea.Year1.RBE' will return
        ['Current','Current.Battersea','Current.Battersea.Year1','Current.Battersea.Year1.RBE']
        '''
        Groups=set()
        for counter in range(0,len(GroupName.split('.'))):
            Groups.add('.'.join(GroupName.split('.')[0:counter]))
        return Groups    
    @staticmethod
    def ListPupilGroups(PupilId,AcYear):
        ''' Lists all the Groups a child belongs to '''
        GroupSet=set()
        if PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilId).exists():
            PupilGroupList=PupilGroup.objects.filter(AcademicYear=AcYear,Pupil=PupilId,Active=True)
            for groups in PupilGroupList:
                GroupSet.add(groups.Group.Name)
        if PupilGroup.objects.filter(AcademicYear='Applicants',Pupil=PupilId).exists():
            PupilGroupList=PupilGroup.objects.filter(AcademicYear='Applicants',Pupil=PupilId,Active=True)
            for groups in PupilGroupList:
                GroupSet.add(groups.Group.Name)
        if PupilGroup.objects.filter(AcademicYear='Alumni',Pupil=PupilId).exists():
            PupilGroupList=PupilGroup.objects.filter(AcademicYear='Alumni',Pupil=PupilId,Active=True)
            for groups in PupilGroupList:
                GroupSet.add(groups.Group.Name)                
        return sorted(GroupSet)
    @staticmethod
    def ListStaffGroups(StaffId,AcYear):
        ''' Lists all the Groups a child belongs to '''
        GroupSet=set()
        if StaffGroup.objects.filter(AcademicYear=AcYear,Staff=StaffId).exists():
            StaffGroupList=StaffGroup.objects.filter(AcademicYear=AcYear,Staff=StaffId)
            for groups in StaffGroupList:
                GroupSet.add(groups.Group.Name)
        return sorted(GroupSet)        
    @staticmethod
    def Get_PupilList(AcYear,GroupName=None,GroupNo=None):
        PupilSet=set()
        if GroupName:
            PupilList=PupilGroup.objects.filter(Group__Name__icontains=GroupName,AcademicYear=AcYear,Active=True)
        else:
            PupilList=PupilGroup.objects.filter(Group__id=int(GroupNo),Active=True)
        for eachPupil in PupilList:
            PupilSet.add(eachPupil.Pupil)
        return sorted(PupilSet, key=lambda pupil: pupil.Surname)
        
    @staticmethod
    def Get_PupilListExact(AcYear,GroupName=None,GroupNo=None):
        PupilSet=set()
        if GroupName:
            PupilList=PupilGroup.objects.filter(Group__Name=GroupName,AcademicYear=AcYear,Active=True)
        else:
            PupilList=PupilGroup.objects.filter(Group__id=int(GroupNo),Active=True)
        for eachPupil in PupilList:
            PupilSet.add(eachPupil.Pupil)
        return sorted(PupilSet, key=lambda pupil: pupil.Surname)          
    @staticmethod
    def Get_PupilSet(GroupDetail,PupilId,AcYear):
        GroupSet=GroupFunctions.ListPupilGroups(PupilId,AcYear)
        SetId=''
        for items in GroupSet:
            if items.find(GroupDetail) > -1:
                GroupName=Group.objects.get(Name__iexact=items)
                SetId=GroupName.MenuName
        return SetId
    @staticmethod
    def Get_SetManagerList(GroupId,GroupSets,AcYear):
        ManagerGroup=Group.objects.get(id=int(GroupId))
        GroupName=ManagerGroup.Name
        SetIdList=[Group.objects.get(Name='.'.join(ManagerGroup.Name.split('.')[0:-1])).id]
        for groups in GroupSets.split('-'):
            SetIdList.append(groups)

        # Create the SetList for use in the Table Titles on the page
        SetList=[]
        for groups in SetIdList:
            Groupdetail=Group.objects.get(id=groups)
            SetList.append(Groupdetail.Name.split('.')[-1])

        # Create Pupil list with which set their In

        PupilListA={}
        for pupils in GroupFunctions.Get_PupilList(AcYear,GroupName='.'.join(ManagerGroup.Name.split('.')[0:-1])):
            PupilDetails=Pupil.objects.get(id=pupils)
            PupilListA[pupils]=["%s %s" % (PupilDetails.Forename,PupilDetails.Surname),SetIdList[0],PupilDetails.Picture]
        for groups in SetIdList:
            PupilDetails=PupilGroup.objects.filter(Group=int(groups),AcademicYear=AcYear)
            for Details in PupilDetails:
                PupilListA[Details.Pupil.id]=["%s %s" % (Details.Pupil.Forename,Details.Pupil.Surname),Details.Group.id,Details.Pupil.Picture]

        PupilList=[]
        for records in PupilListA:
            PupilSetList=[]
            for sets in SetIdList:
                if PupilListA[records][1]==int(sets):
                    PupilSetList.append([sets,True])
                else:
                    PupilSetList.append([sets,False])
            PupilList.append([PupilListA[records][0],records,PupilListA[records][2],PupilSetList])
        return {'PupilList':PupilList,'SetList':SetList,'GroupName':GroupName,'GroupRef':GroupSets,'AcYear':AcYear,'ManagerId':SetIdList[0]}
        
    @staticmethod
    def Get_StaffList(AcYear,GroupName=None,GroupNo=None):
        StaffSet=set()
        if GroupName:
            StaffList=StaffGroup.objects.filter(Group__Name__icontains=GroupName,AcademicYear=AcYear)
        else:
            StaffList=StaffGroup.objects.filter(Group__id=int(GroupNo))
        for eachStaff in StaffList:
            StaffSet.add(eachStaff.Staff)
        return StaffSet
        
    @staticmethod
    def StaffInGroup(request,GroupId):
        Found=False
        for Groups in request.session['StaffGroups']:
            if Groups.Name.find(GroupId.Name) > -1:
                Found=True
        if request.user.is_superuser:
            Found=True
        return Found
        

    @staticmethod
    def PupilStatus(Pupil_Id,AcYear):
        ''' returns current, applicant, or alumni, plus school'''
        if PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name__istartswith='Current',AcademicYear=AcYear).exists():
            PupilStatus=PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name__istartswith='Current',AcademicYear=AcYear)
            return '.'.join(PupilStatus[len(PupilStatus)-1].Group.Name.split('.')[0:2])
        elif PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name_istartswith='Applicant',AcademicYear=AcYear).exists():
            PupilStatus=PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name__istartswith='Applicant',AcademicYear=AcYear)
            return '.'.join(PupilStatus[len(PupilStatus)-1].Group.Name.split('.')[0:2])
        elif PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name_istartswith='Alumni',AcademicYear=AcYear).exists():
            PupilStatus=PupilGroup.objects.filter(Pupil=Pupil_Id,Group__Name__istartswith='Alumni',AcademicYear=AcYear)
            return '.'.join(PupilStatus[len(PupilStatus)-1].Group.Name.split('.')[0:2])
        else:
            return 'Unknown'
    @staticmethod
    def FamiliesInAGroup(request,AcYear,Group_Id=None,PupilList=None):
        ''' This method takes a list of pupils as either a GroupId, or a straight list
        and return a list of Familiy records. The Group_Id has to be specified, but the 
        pupil list only if this is to be a subset of that group'''
        if PupilList:
            FamilyConnections=FamilyChildren.objects.filter(Pupil__in=PupilList)
        else:
            FamilyConnections=FamilyChildren.objects.filter(Pupil__in=GroupFunctions.Get_PupilList(AcYear,GroupName=Group.objects.get(id=int(Group_Id)).Name))
        FamilyIdSet=set()
        for records in FamilyConnections:
            if PupilList:
                FamilyIdSet.add(FamilyRecord(request,AcYear,FamilyId=records.FamilyId.id,PupilList=PupilList))
            else:
                FamilyIdSet.add(FamilyRecord(request,AcYear,FamilyId=records.FamilyId.id,GroupId=Group_Id))
        return FamilyIdSet
        
        
class SchoolGroupsList():
    ''' a class to return groups that are for the currently viewed school
    only'''
    def __init__(self,MenuName):
        self.GroupList=Group.objects.filter(Name__icontains=MenuTypes.objects.get(Name=MenuName).SchoolId.Name)        
    def Test(self):
        return 'test test test'
    def All(self):
        return self.GroupList
    def PupilGroups(self):
        return self.GroupList.exclude(Name__istartswith='Staff')
    def StaffGroups(self):
        return self.GroupList.filter(Name__istartswith='Staff')
    def CurrentGroups(self):
        return self.GroupList.filter(Name__istartswith='Current')       
    def ApplicantGroups(self):
        return self.GroupList.filter(Name__istartswith='Applicants') 
    def AlumniGroups(self):
        return self.GroupList.filter(Name__istartswith='Alumni')         

class Alerts():
    def __init__(self,request,Pupil_Id):
        self.Pupil_Id=Pupil.objects.get(id=int(Pupil_Id))
        self.request=request

    def ActivePupilAlerts(self):
        self.LIST=[]
        if PupilAlert.objects.filter(PupilId=self.Pupil_Id,Active=True,StartDate__lte=datetime.datetime.today()).exists:
            Alerts=PupilAlert.objects.filter(PupilId=self.Pupil_Id,Active=True,StartDate__lte= datetime.datetime.today).order_by('AlertType__AlertGroup__DisplayOrder')
            self.LIST.append(copy.deepcopy(Alerts))
            return self.LIST
        else:
            return ''

    def Close(self):
        if 'AlertClosedDetails' in self.request.POST:
            Closed_Details=self.request.POST['AlertClosedDetails']
        else:
            Closed_Details="No details supplied"
        OpenAlert=PupilAlert.objects.get(id=self.request.POST['AlertId'])
        if len(self.request.POST['StopDate']) > 2:
            OpenAlert.StopDate=self.request.POST['StopDate']
        else:
            OpenAlert.StopDate=datetime.datetime.today()
        OpenAlert.AlertClosedDetails=Closed_Details
        OpenAlert.Active=False
        OpenAlert.ClosedBy=self.request.session['StaffId']
        OpenAlert.UpdateDate=datetime.datetime.today()
        OpenAlert.save()
        return True
    def Save(self):
        if len(self.request.POST['StartDate']) > 2:
            Start_Date=self.request.POST['StartDate']
        else:
            Start_Date=datetime.datetime.today()
        RecordDetails=PupilAlert(PupilId=self.Pupil_Id,Active=True,AlertType=AlertType.objects.get(Name=self.request.POST['AlertType']),
        StartDate=Start_Date,AlertDetails=self.request.POST['AlertDetails'],RaisedBy=self.request.session['StaffId'],
        UpdateDate=datetime.datetime.today())
        RecordDetails.save()
        if len(self.request.POST['StopDate']) > 2:
            RecordDetails.StopDate=self.request.POST['StopDate']
            RecordDetails.ClosedBy=self.request.session['StaffId']
            RecordDetails.save()
        if 'AlertClosedDetails' in self.request.POST:
            RecordDetails.AlertClosedDetails=self.request.POST['AlertClosedDetails']
            RecordDetails.save()
        return True
    def Modify(self):
        RecordDetails=PupilAlert.objects.get(id=self.request.POST['AlertId'])
        RecordDetails.StartDate=self.request.POST['StartDate']
        if len(self.request.POST['StopDate']) > 2:
            RecordDetails.StopDate=self.request.POST['StopDate']
        RecordDetails.AlertType=AlertType.objects.get(Name=self.request.POST['AlertType'])
        RecordDetails.AlertDetails=self.request.POST['AlertDetails']
        if 'AlertClosedDetails' in self.request.POST:
            RecordDetails.AlertClosedDetails=self.request.POST['AlertClosedDetails']
        RecordDetails.RaisedBy=self.request.session['StaffId']
        RecordDetails.UpdateDate=datetime.datetime.today()
        RecordDetails.save()
        return

    def AlertList(self):
        PupilAlertList=[]
        ListOfAlertGroups=AlertGroup.objects.filter().order_by('DisplayOrder')
        for Groups in ListOfAlertGroups:
            PupilAlertGroup={'HasPermission':False}
            for StaffGroups in Groups.PermissionGroups.all():
                if not PupilAlertGroup['HasPermission']:
                    PupilAlertGroup['HasPermission']=GroupFunctions.StaffInGroup(self.request,StaffGroups)

            PupilAlertGroup['Active']=False
            PupilAlertGroup['Name']=Groups.Name
            PupilAlertGroup['ListOfAlerts']=[]
            ListOfAlertTypes=AlertType.objects.filter(AlertGroup=Groups)
            for Types in ListOfAlertTypes:
                PupilAlertGroup['ListOfAlerts'].append({'Name':Types.Name,'Active':False})
            if PupilAlert.objects.filter(PupilId=self.Pupil_Id,Active=True,AlertType__AlertGroup=Groups).exists():
                PupilAlertGroup['Active']=True
                PupilAlertGroup['Alert']=PupilAlert.objects.get(PupilId=self.Pupil_Id,Active=True,AlertType__AlertGroup=Groups)
                for items in PupilAlertGroup['ListOfAlerts']:
                    if items['Name'].find(PupilAlertGroup['Alert'].AlertType.Name) > -1:
                        items['Active']=True
            PupilAlertList.append(PupilAlertGroup)
        return PupilAlertList
    def Valid(self):
        return_valid=True
        self.ErrMsg=[]
        self.AlertItems=self.request.POST
        if self.request.POST['save']=='Activate' and PupilAlert.objects.filter(PupilId=self.Pupil_Id,AlertType__Name=self.request.POST['AlertType'],Active=True).exists():
            return_valid=False
            self.ErrMsg.append('System Error: An Alert of this nature already exists. Please refresh your Alerts Page')
        if self.request.POST['save']=='Activate' or self.request.POST['save']=='New':
            if len(self.request.POST['AlertDetails']) < 3:
                self.ErrMsg.append('Alert details comment is not long enough')
                return_valid=False
            if not 'AlertType' in self.request.POST:
                self.ErrMsg.append('No Alert Type was chosen')
                return_valid=False
        elif self.request.POST['save']=='Modify':
            if len(self.request.POST['AlertDetails']) < 3:
                self.ErrMsg.append('Alert details comment is not long enough')
                return_valid=False
        if not return_valid:
            self.ErrMsg.append('<br><br>')
            self.ErrMsg.append('Changes have been rejected')
            self.ErrMsg.append('Using the back page on the brower may recover your items')
        return return_valid

class NoteFunctions():
    @staticmethod
    def AddPupilNotes(request,PupilList):
        KWList=':'.join(request.POST.getlist('KWSelect'))
        NoteRecord=NoteDetails(Keywords=KWList,NoteText=request.POST['NoteTextArea'],RaisedBy=request.session['StaffId'],
        RaisedDate=datetime.datetime.now(),ModifiedBy=request.session['StaffId'],ModifiedDate=datetime.datetime.now())
        NoteRecord.save()
        if len(PupilList) ==1:
            TestForIssue=NoteFunctions.IsAnIssue(PupilList[0],KWList)
            if TestForIssue:
                NoteRecord.IsAnIssue=True
                NoteRecord.IssueStatus='Open'
                NoteRecord.save()

        for Pupil_Id in PupilList:
            PupilNoteRecord=PupilNotes(PupilId=Pupil_Id,NoteId=NoteRecord)
            PupilNoteRecord.save()
        return NoteRecord
    @staticmethod
    def IsAnIssue(PupilRec,KWList):
        IssueList=NoteToIssue.objects.all()
        NoteIsAnIssue=False
        for records in IssueList:
            FindAllKeywords=True
            IssueKWList=eval(records.Keywords)
            for Words in IssueKWList:
                if KWList.find(Words) < 0:
                    FindAllKeywords=False
            if FindAllKeywords:
                NoteIsAnIssue=True
                # send notification here
        return NoteIsAnIssue
                
            
    @staticmethod
    def AddNote(request,BaseId,Type):
        KWList=':'.join(request.POST.getlist('KWSelect'))
        NoteRecord=NoteDetails(Keywords=KWList,NoteText=request.POST['NoteTextArea'],RaisedBy=request.session['StaffId'],
        RaisedDate=datetime.datetime.now(),ModifiedBy=request.session['StaffId'],ModifiedDate=datetime.datetime.now())
        NoteRecord.save()
        if 'HousePoints' in request.POST:
            if not request.POST['HousePoints'] =='None':
                NoteRecord.HousePoints=int(request.POST['HousePoints'])
                NoteRecord.save()
        if Type=='Staff':
            StaffNoteRecord=StaffNotes(StaffId=BaseId,NoteId=NoteRecord)
            StaffNoteRecord.save()
        elif Type=='Family':
            FamilyNoteRecord=FamilyNotes(FamilyId=BaseId,NoteId=NoteRecord)
            FamilyNoteRecord.save()
        elif Type=='Contact':
            ContactNoteRecord=ContactNotes(ContactId=BaseId,NoteId=NoteRecord)
            ContactNoteRecord.save() 
        elif Type=='Pupil':
            PupilNoteRecord=PupilNotes(PupilId=BaseId,NoteId=NoteRecord)
            PupilNoteRecord.save()               
        return NoteRecord        
    @staticmethod
    def GetPupilNotes(request,Pupil_Id=None,Pupil_List=None,Group_Name=None): 
        # Returns a filtered list of Notes
        NoteRecords=PupilNotes.objects.all().order_by('NoteId__RaisedDate').reverse()
        NoteRecords=NoteRecords.filter(Active=True)
        if not Pupil_Id==None:
            NoteRecords=NoteRecords.filter(PupilId=Pupil.objects.get(id=int(Pupil_Id)))
        elif not Pupil_List==None:
            NoteRecords=NoteRecords.filter(PupilId__in=Pupil_List)
        else:
            NoteRecords=NoteRecords.filter(PupilId__in=GroupFunctions.Get_PupilList(AcYear=request.session['AcYear'],GroupName=Group_Name))
        Filters=''
        if 'NotesKWordFilter' in request.session:
            Filters=','.join(request.session['NotesKWordFilter'])
            for words in request.session['NotesKWordFilter']:
                NoteRecords=NoteRecords.filter(NoteId__Keywords__icontains=words)              
        #added filter code by roy...
        test = False
        for i in request.session.keys():
            if 'NotesStartFilter' == i:
                test = True
            elif 'NotesStopFilter' == i:
                test = True
        if test:
            if 'NotesStopFilter' not in request.session:
                temp = datetime.datetime.today()
                request.session['NotesStopFilter']= str(temp.date())         
            if 'NotesStartFilter' not in request.session:
                lastYear = date.today().year-1
                lastSeptember = datetime.datetime.strptime("%s-09-01" % lastYear ,"%Y-%m-%d")
                request.session['NotesStartFilter']= str(lastSeptember.date()) 
            startDate=request.session['NotesStartFilter']
            endDate=request.session['NotesStopFilter']
            endDatePlusOne = datetime.datetime.strptime(endDate,"%Y-%m-%d")
            endDatePlusOne = endDatePlusOne+timedelta(days=1)
            endDatePlusOne = str(endDatePlusOne.date())  
            NoteRecords=NoteRecords.filter(NoteId__RaisedDate__range=(startDate,endDatePlusOne))
            Filters+=' StartDate:%s ' % request.session['NotesStartFilter']
            Filters+=' StopDate:%s ' % request.session['NotesStopFilter']
        return {'NoteRecords':NoteRecords,'Filters':Filters}
    
    @staticmethod
    def GetNotes(request,Type,BaseId):
        # Returns a filtered list of Notes
        if Type=='Staff':
            NoteRecords=StaffNotes.objects.filter(StaffId=Staff.objects.get(id=int(BaseId))).order_by('NoteId__RaisedDate')
        elif Type=='Family':
            NoteRecords=FamilyNotes.objects.filter(FamilyId=Family.objects.get(id=int(BaseId))).order_by('NoteId__RaisedDate')
        elif Type=='Contact':
            NoteRecords=ContactNotes.objects.filter(ContactId=Contact.objects.get(id=int(BaseId))).order_by('NoteId__RaisedDate')
        NoteRecords=NoteRecords.filter(Active=True)
        Filters=''
        if 'NotesKWordFilter' in request.session:
            Filters=','.join(request.session['NotesKWordFilter'])
            for words in request.session['NotesKWordFilter']:
                NoteRecords=NoteRecords.filter(NoteId__Keywords__icontains=words)
        if 'NotesStartFilter' in request.session:
            NoteRecords=NoteRecords.filter(NoteId__RaisedDate__gte=request.session['NotesStartFilter'])
            Filters+=' StartDate:%s ' % request.session['NotesStartFilter']
        if 'NotesStopFilter' in request.session:
            NoteRecords=NoteRecords.filter(NoteId__RaisedDate__gte=request.session['NoteStopFilter'])
            Filters+=' StopDate:%s ' % request.session['NotesStopFilter']
        return {'NoteRecords':NoteRecords,'Filters':Filters}
    @staticmethod
    def GetSingleNote(Note_Id):
        if NoteDetails.objects.filter(id=int(Note_Id)).exists():
            return NoteDetails.objects.get(id=int(Note_Id))
        else:
            return False

    @staticmethod
    def UpdateNoteFromRequest(request,ObjectType):
        ''' This fuction Updates a Note. it does this by first setting the original
        Note to Inactive, and then saves a new note'''
        ObjectList=NoteFunctions.RemoveNote(request.POST['NoteId'],Type=ObjectType)
        return NoteFunctions.AddNote(request,ObjectList[0])        
    @staticmethod
    def PermissionToRemove(request):
        '''Checks permission to Remove, or inactivate a note against a pupils record.
        This action will not delete a Note, just mark as inactive'''
        return True
    @staticmethod
    def PermissionToUpdate(request,NoteRecord):
        ''' Checks permissions to Update a Note '''
        return True
    @staticmethod
    def RemoveSingleNote(Note_Id=None,BaseId=None,NoteType=None):
        ''' this function removes a single relationship to Note '''
        Note_Id=int(Note_Id)
        if NoteDetails.objects.filter(id=Note_Id).exists():
            if PupilNotes.objects.filter(NoteId__id=Note_Id,PupilId__id=BaseId,Active=True).exists() and NoteType.find('Pupil') > -1:
                PupilRecords=PupilNotes.objects.get(NoteId__id=Note_Id,PupilId__id=BaseId,Active=True)
                PupilRecords.Active=False
                PupilRecords.save()
                return [PupilRecords]
            elif StaffNotes.objects.filter(NoteId__id=Note_Id,StaffId__id=BaseId,Active=True).exists() and NoteType.find('Staff') > -1:
                StaffRecords=StaffNotes.objects.filter(NoteId__id=Note_Id,StaffId__id=BaseId)
                StaffList=[]
                for records in StaffRecords:
                    records.Active=False
                    records.save()
                    StaffList.append(records.StaffId)
                return StaffList               
            elif ContactNotes.objects.filter(NoteId__id=Note_Id,ContactId__id=BaseId,Active=True).exists() and NoteType.find('Contact') > -1:
                ContactRecords=ContactNotes.objects.filter(NoteId__id=Note_Id,ContactId__id=BaseId)
                ContactList=[]
                for records in ContactRecords:
                    records.Active=False
                    records.save()
                    ContactList.append(records.ContactId)
                return ContactList            
            elif FamilyNotes.objects.filter(NoteId__id=Note_Id,FamilyId__id=BaseId,Active=True).exists() and NoteType.find('Family') > -1:
                FamilyRecords=FamilyNotes.objects.filter(NoteId__id=Note_Id,FamilyId__id=Baseid)
                FamilyList=[]
                for records in FamilyRecords:
                    records.Active=False
                    records.save()
                    FamilyList.append(records.FamilyId)
                return FamilyList         
            else:
                return False
        else:
            return False
    @staticmethod        
    def RemoveNote(Note_Id,Type):
        ''' This Sets a note and its related realationship records to Inactive '''
        Note_Id=int(Note_Id)
        if NoteDetails.objects.filter(id=Note_Id,Active=True).exists():
            if PupilNotes.objects.filter(NoteId__id=Note_Id,Active=True).exists() and Type=='Pupil':
                PupilRecords=PupilNotes.objects.filter(NoteId__id=Note_Id)
                PupilList=[]
                for records in PupilRecords:
                    records.Active=False
                    records.save()
                    PupilList.append(records.PupilId)
                return PupilList
            elif StaffNotes.objects.filter(NoteId__id=Note_Id,Active=True).exists() and Type=='Staff':
                StaffRecords=StaffNotes.objects.filter(NoteId__id=Note_Id)
                StaffList=[]
                for records in StaffRecords:
                    records.Active=False
                    records.save()
                    StaffList.append(records.StaffId)
                return StaffList               
            elif ContactNotes.objects.filter(NoteId__id=Note_Id,Active=True).exists() and Type=='Contact':
                ContactRecords=ContactNotes.objects.filter(NoteId__id=Note_Id)
                ContactList=[]
                for records in ContactRecords:
                    records.Active=False
                    records.save()
                    ContactList.append(records.ContactId)
                return ContactList            
            elif FamilyNotes.objects.filter(NoteId__id=Note_Id,Active=True).exists() and Type=='Family':
                FamilyRecords=FamilyNotes.objects.filter(NoteId_id=Note_Id)
                FamilyList=[]
                for records in FamilyRecords:
                    records.Active=False
                    records.save()
                    FamilyList.append(records.FamilyPupilId)
                return FamilyList         
            else:
                return False
    @staticmethod
    def NotifyStaffOfNote(Details):
        return True


class DailyAttendance():
    def __init__(self,RequiredDate,AttendanceGroup):
        self.AttendanceGroup=AttendanceGroup
        self.RequiredDate=RequiredDate
        self.AttendanceData=PupilAttendance.objects.filter(AttendanceDate=self.RequiredDate,Group=self.AttendanceGroup,Active=True)
    def HasBeenTakenAM(self):
        if AttendanceTaken.objects.filter(AttendanceDate=self.RequiredDate,Group=self.AttendanceGroup,AttendanceType='D',AmPm='AM').exists():
            return 'Y'
        else:
            return 'N'    
    def HasBeenTakenPM(self):
        if AttendanceTaken.objects.filter(AttendanceDate=self.RequiredDate,Group=self.AttendanceGroup,AttendanceType='D',AmPm='PM').exists():
            return 'Y'
        else:
            return 'N' 
            
    def AuthorisedListAM(self):
        ''' Returns the Authorised absences '''
        return self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Authorised')            
    def AuthorisedCountAM(self):
        ''' Returns the Authorised absences '''
        return len(self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Authorised'))
    def AuthorisedListPM(self):
        ''' Returns the Authorised absences '''
        return self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Authorised')        
    def AuthorisedCountPM(self):
        ''' Returns the Authorised absences '''
        return len(self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Authorised'))            
            
            
    def UnauthorisedListAM(self):
        ''' Returns the Unauthorised absences '''
        return self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Unauthorised')            
    def UnauthorisedCountAM(self):
        ''' Returns the Unauthorised absences '''
        return len(self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Unauthorised'))
    def UnauthorisedListPM(self):
        ''' Returns the Unauthorised absences '''
        return self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Unauthorised')        
    def UnauthorisedCountPM(self):
        ''' Returns the Unauthorised absences '''
        return len(self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Unauthorised'))
        
            
    def LateListAM(self):
        ''' Returns the Late absences '''
        return self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Late')            
    def LateCountAM(self):
        ''' Returns the Late absences '''
        return len(self.AttendanceData.filter(AmPm='AM',Code__AttendanceCodeType='Late'))
    def LateListPM(self):
        ''' Returns the Late absences '''
        return self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Late')        
    def LateCountPM(self):
        ''' Returns the Late absences '''
        return len(self.AttendanceData.filter(AmPm='PM',Code__AttendanceCodeType='Late'))        

    def Report(self):
        ''' Returns a list of all the Pupils in the Selected group as a list of Dictionaries
        The dictionary is in the for {PupilDetails:PupilDetails, AM:AmAttCode,PM:PMAttCode}'''
        PupilList=GroupFuctions.Get_PupilList(GroupNo=self.AttendanceGroup.id)
        AttendanceReport=[]
        for eachpupil in PupilList:
            AttReport={'PupilDetails':eachpupil,'AM':'/','PM':"\\"}
            if self.AttendanceData.filter(Pupil=eachpupil,AmPm='AM').exists():
                AttReport['AM']=self.AttendanceData.filter(Pupil=eachpupil,AmPm='AM')[0].Code.AttendanceCode
            if self.AttendanceData.filter(Pupil=eachpupil,AmPm='PM').exists():
                AttReport['AM']=self.AttendanceData.filter(Pupil=eachpupil,AmPm='PM')[0].Code.AttendanceCode
            ReturnReport.append(AttReport)
            
        return sorted(AttendanceReport, key=lambda pupil: pupil['PupilDetails']['Surname'])
    @staticmethod
    def Get_ClassList(CurrentMenu):
        menuList=Menus.objects.filter(MenuType__Name=CurrentMenu,MenuActions__Name='Attendance')
        return [element.Group for element in menuList]


class ConfigParser():
    def __init__(self,FileName):
        parmfile=open(FileName,'rb')
        self.Parmdata={}
        Section=None
        for lines in parmfile.readlines():
            if lines.find('[') > -1 and lines.find(']') > -1 and lines.find('=') < 0:
                Section=lines.split('[')[1].split(']')[0]
                self.Parmdata[Section]={}
            elif Section is not None and lines.find('=') > -1:
                pname,pdata=lines.split('=',1)
                if pdata.find('\r') > -1:
                    pdata=pdata.split('\r')[0]
                elif pdata.find('\r\n') > -1:
                    pdata=pdata.split('\r\n')[0]
                else:
                    pdata=pdata.split('\n')[0]
                self.Parmdata[Section][pname]=pdata
    def items(self,SectionName):
        if self.Parmdata.has_key(SectionName):
            return self.Parmdata[SectionName]
    def get(self,SectionName,ItemName):
        if self.Parmdata.has_key(SectionName):
            if self.Parmdata[SectionName].has_key(ItemName):
                return self.Parmdata[SectionName][ItemName]

class TemplateRecord():
    def __init__(self):
        self.AllTemplates=LetterTemplate.objects.filter(Active=True)
    def PupilTemplates(self):
        return self.AllTemplates.filter(LetterBaseType='Pupil')
    def FamilyTemplates(self):
        return self.AllTemplates.filter(LetterBaseType='Family')

class LetterManager():
    @staticmethod
    def SaveLetter(request,school,AcYear):
        NewLetter=LetterBody(AttachedTo=Group.objects.get(Name=request.POST['Selectgroup']),
                             Name=request.POST['LetterName'],BodyText=request.POST['LetterBody'],
                             SignatureName=request.POST['SignatureName'],SignatureTitle=request.POST['SignatureTitle'],
                             LetterTemplate=LetterTemplate.objects.get(Name=request.POST['TemplateList']),
                             ObjectBaseType=request.POST['BaseType'],
                             CreatedBy=request.session['StaffId'],ModifiedBY=request.session['StaffId'],Description=request.POST['Description'])
        NewLetter.save()
        if 'StaffGroup' in request.POST:
            if request.POST['StaffGroup'].find('None') < 0:
                NewLetter.ExtraStaffAccess=Group.objects.get(id=int(request.POST['StaffGroup']))
                NewLetter.save()
        return NewLetter
    @staticmethod
    def GetlistOfLetters(GroupName):
        Grouplength=len(GroupName.split('.'))
        GroupList=[]
        for count in range(0,Grouplength):
            GroupList.append('.'.join(GroupName.split('.')[0:count]))
        return LetterBody.objects.filter(AttachedTo__Name__in=GroupList,Active=True)

    @staticmethod
    def CreateFromCopy(request,school,AcYear):
        pass
    @staticmethod
    def Inactivate(LetterId):
        LetterDetails=LetterBody.objects.get(id=int(LetterId))
        LetterDetails.Active=False
        LetterDetails.save()
        return
    
    
class GenerateFamilyLetter():
    def __init__(self,request,Letter_Id,school,AcYear):
        self.PupilList=[]
        for PupilIds in request.POST.getlist('PupilSelect'):
            self.PupilList.append(Pupil.objects.get(id=int(PupilIds)))
        
        self.FamilyList=GroupFunctions.FamiliesInAGroup(request,AcYear,PupilList=self.PupilList)
        self.VarList={}
        self.OtherItems={}
        self.OtherItems['SchoolName']=MenuTypes.objects.get(Name=school).SchoolId.Name
        self.OtherItems['CurrentYear']=datetime.datetime.now().year
        self.OtherItems['NextYear']=self.OtherItems['CurrentYear']+1
        self.LetterDate=datetime.datetime.strptime(request.POST['LetterDate'],'%Y-%m-%d')
        if 'Date1' in request.POST:
            self.VarList['Date1']=datetime.datetime.strptime(request.POST['Date1'],'%Y-%m-%d')
        if 'Date2' in request.POST:
            self.VarList['Date2']=datetime.datetime.strptime(request.POST['Date2'],'%Y-%m-%d')        
        if 'Date3' in request.POST:
            self.VarList['Date3']=datetime.datetime.strptime(request.POST['Date3'],'%Y-%m-%d')    
        if 'Adhoc1' in request.POST:
            self.VarList['Adhoc1']=request.POST['Adhoc1']    
        if 'Adhoc2' in request.POST:
            self.VarList['Adhoc2']=request.POST['Adhoc2'] 
        if 'Adhoc2' in request.POST:
            self.VarList['Adhoc2']=request.POST['Adhoc2']             
        self.LetterDetails=LetterBody.objects.get(id=int(Letter_Id))
        for FamilyDetails in self.FamilyList:
            FamilyDetails.GenerateLetterBody(self.LetterDetails,self.VarList,self.OtherItems)
        self.SchoolDetails=MenuTypes.objects.get(Name=school).SchoolId
        
class GeneratePupilLetter():
    def __init__(self,request,Letter_Id,school,AcYear):
        self.FamilyList=[]
        self.VarList={}
        self.OtherItems={}
        self.OtherItems['SchoolName']=MenuTypes.objects.get(Name=school).SchoolId.Name
        self.OtherItems['CurrentYear']=datetime.datetime.now().year
        self.OtherItems['NextYear']=self.OtherItems['CurrentYear']+1
        self.LetterDate=datetime.datetime.strptime(request.POST['LetterDate'],'%Y-%m-%d')
        if 'Date1' in request.POST:
            self.VarList['Date1']=datetime.datetime.strptime(request.POST['Date1'],'%Y-%m-%d')
        if 'Date2' in request.POST:
            self.VarList['Date2']=datetime.datetime.strptime(request.POST['Date2'],'%Y-%m-%d')        
        if 'Date3' in request.POST:
            self.VarList['Date3']=datetime.datetime.strptime(request.POST['Date3'],'%Y-%m-%d')    
        if 'Adhoc1' in request.POST:
            self.VarList['Adhoc1']=request.POST['Adhoc1']    
        if 'Adhoc2' in request.POST:
            self.VarList['Adhoc2']=request.POST['Adhoc2'] 
        if 'Adhoc2' in request.POST:
            self.VarList['Adhoc2']=request.POST['Adhoc2'] 
        self.LetterDetails=LetterBody.objects.get(id=int(Letter_Id))
        for PupilNo in request.POST.getlist('PupilSelect'):
            Rec=FamilyRecord(request,AcYear,PupilId=PupilNo)
            Rec.GenerateLetterBody(self.LetterDetails,self.VarList,self.OtherItems,LetterType='Pupil')
            self.FamilyList.append(Rec)
        self.SchoolDetails=MenuTypes.objects.get(Name=school).SchoolId
        
class GenerateLabels():
    def __init__(self,request,school,AcYear):
        self.PupilList=[]
        for PupilIds in request.POST.getlist('PupilSelect'):
            self.PupilList.append(Pupil.objects.get(id=int(PupilIds)))        
        self.FamilyList=[]
        if request.POST['LabelType']=='AddressFamily':
            self.FamilyList=GroupFunctions.FamiliesInAGroup(request,AcYear,PupilList=self.PupilList)            
        else:
            for PupilNo in self.PupilList:
                self.FamilyList.append(FamilyRecord(request,AcYear,PupilId=PupilNo.id))
    
class RelationshipTypes():
    def __init__(self)    :
        self.Records=FamilyRelationship.objects.all()
    def Adult(self):
        return self.Records.filter(RelType='A')
    def Child(self):
        return self.Recrods.filter(RelType='C')

class SchoolGroups():
    def __init__(self,school):
        self.School=MenuTypes.objects.get(Name=school).SchoolId
        self.GroupList=Group.objects.filter(Name__icontains=self.School.Name)
    def All(self):
        return self.GroupList
    def Current(self):
        return self.GroupList.filter(Name__istartswith='Current')
    def Applicants(self):
        return self.GroupList.filter(Name__istartswith='Applicants')

class GetAcHousesInSchool():
    """ Returns a list of academic house for a school, beware this will return a 
    full name for the group. for example: Current.Battersea.AcHouse.House.Lawrence"""
    def __init__(self,school):
        self.school = school
        self.AcHouseList = []       
        self.grps = Group.objects.filter(Name__contains=MenuTypes.objects.get(Name=self.school).SchoolId.Name)
        self.grps = self.grps.filter(Name__contains="AcHouse.House.")      
        for i in self.grps:
            self.AcHouseList.append(str(i))  
    def getHouses(self):    
        return self.AcHouseList
    
class GetFormsInSchool():
    """ Returns a list of forms for a school, beware this will return a 
    full name for the group. for example: Current.Battersea.UpperSchool.Year8.Form.8BS"""
    def __init__(self,school):
        self.school = school
        self.forms = [] 
        self.grps = Group.objects.filter(Name__contains=MenuTypes.objects.get(Name=self.school).SchoolId.Name)
        self.grps = self.grps.filter(Name__contains="Form.").exclude(Name__icontains='BalGames')        
        for i in self.grps:
            self.forms.append(str(i))        
    def getForms(self):    
        return self.forms

class UpdatePupilRecord():
    """ Update Pupil record Class"""
    def __init__(self,request,school,AcYear,PupilNo):
        self.PupilRec=Pupil.objects.get(id=int(PupilNo))
        self.PupilNo = PupilNo
        self.AcYear=AcYear
        self.School=school
        self.PostData=request.POST
        self.FileData=request.FILES        
    def Base(self):
        self.PupilRec.Surname=self.PostData['Surname']
        self.PupilRec.Forename=self.PostData['Forename']
        self.PupilRec.Gender=self.PostData['Gender']
        self.PupilRec.DateOfBirth=self.PostData['DateOfBirth']
        self.PupilRec.Title=self.PostData['Title']
        self.PupilRec.NickName=self.PostData['NickName']
        self.PupilRec.OtherNames=self.PostData['OtherNames']
        self.PupilRec.EmailAddress=self.PostData['EmailAddress']        
        self.PupilRec.save()
        self.updateCache()
        return         
    def removeFromGroup(self,group):       
        try:
            oldgroup = PupilGroup.objects.get(Pupil__id=self.PupilNo,Group__Name=group,Active=True)
            oldgroup.Active = False   
            oldgroup.save()
        except:
            pass
        return        
    def addToGroup(self,oldGroup,newGroup):
        self.removeFromGroup(oldGroup)
        group = Group.objects.get(Name=newGroup)
        newPupilGroupRecord = PupilGroup(AcademicYear=self.AcYear, Pupil=self.PupilRec, Group=group)      
        newPupilGroupRecord.save() 
        self.updateCache()
        return
    def removeFromAllGroupsButHolding(self):
        '''removes a pupil from all groups for the school they are a member 
        of and adds them to the holding group'''
        groups = PupilGroup.objects.filter(Pupil__id=self.PupilNo,Active=True)
        schoolName = MenuTypes.objects.get(Name=self.School).SchoolId.Name
        counter = 0
        tempList = range(len(groups))
        for i in tempList:
            if '%s' % str(schoolName) in str(groups[counter].Group):
                groups[counter].Active = False
                groups[counter].save()
            counter += 1
        groups2 = PupilGroup.objects.filter(Pupil__id=self.PupilNo,Group__Name='Applicants.Holding',Active=True)
        if len(groups2) == 0:
            group = Group.objects.get(Name='Applicants.Holding')
            newPupilGroupRecord = PupilGroup(AcademicYear='Holding', Pupil=self.PupilRec, Group=group)
            newPupilGroupRecord.save() 
        else:
            pass
        return
    def MoveBackToHolding(self):
        ''' Roy.
        
        Compare this one with your one!
        '''
        PupilGroups = Pupilgroups.objects.filter(Pupil=self.PupilRec,Group__Name__icontains=MenuTypes.objects.get(Name=self.School).SchoolId.Name,Active=True)
        for ActiveGroups in PupilGroups:
            item=ActiveGroup
            item.Active=False
            item.save()
        if not PupilGroup.objects.filter(Pupil=self.PupilRec,Active=True,Group__Name='Applicants.Holding').exists():
            NewPupilGroup=PupilGroup(Pupil=self.PupilRec,Group=Group.objects.get(Name='Applicants.Holing'))
            NewPupilGroup.save()        
    def Permissions(self,internetPermissions,mediaPermissions):
        pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=self.PupilNo)
        pupilExtentionRecord.WriteExtention('PupilExtra', {'P_Internet':internetPermissions})
        pupilExtentionRecord.WriteExtention('PupilExtra', {'P_Media':mediaPermissions})
        self.updateCache()        
        return        
    def getPupilOtherInformationPickLists(self):
        pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=self.PupilNo)
        test = pupilExtentionRecord.ReadExtention('PupilExtra')
        if bool(test) == False:
            pupilExtentionRecord.WriteExtention('PupilExtra',{'P_Internet':False})
        return pupilExtentionRecord.ReadExtention('PupilExtra')        
    def UpdatePupilOtherInformation(self):
        pupilExtentionRecord = ExtendedRecord(BaseType='Pupil', BaseId=self.PupilNo)
        pupilExtentionRecord.WriteExtention('PupilExtra',{'UPN':self.PostData['UPN'],
                                                          'EntryYear':self.PostData['EntryYear'],
                                                          'Ethnicity':self.PostData['Ethnicity'],    
                                                          'Nationality':self.PostData['Nationality'],
                                                          'Religion':self.PostData['Religion'],
                                                          'PrimaryLanguage':self.PostData['PrimaryLanguage']})
        self.updateCache()
        return
    def picture(self):
        self.PupilRec=self.FileData['Picture'] 
        self.PupilRec.save()
        return        
    def SpecialNeeds(self):
        pass          
    def updateCache(self):
        cache.delete('Pupil_%s' % self.PupilNo)    
        PupilRecord(self.PupilNo, self.AcYear) 
        return      
        
        
        
        
        
        
        
    