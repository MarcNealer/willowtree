#-------------------------------------------------------------------------------
# Name:        DefaultData
# Purpose:      Pushes default records into the WillowTree MIS
#
# Author:      DBMgr
#
# Created:     12/02/2012
# Copyright:   (c) DBMgr 2012
# Licence:
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from WillowTree.MIS import models
from MIS.models import *
import datetime, copy, ConfigParser, csv
from ModelFunctions import *

def GradeImport(FileName,school):
    f=open(FileName,'r')
    found=False
    for lines in f.readlines():
        print lines
        if lines.find('Old_Id=') > -1:
            Old_StudentId=lines.split('=')[1].split(' ')[0]
            found=False
            if Pupil.objects.filter(Old_Id="%s-%s" % (school,Old_StudentId)).exists():
                NewGradeData=ExtendedRecord(BaseType='Pupil',BaseId=Pupil.objects.get(Old_Id="%s-%s" % (school,Old_StudentId)).id)
                found=True
        elif found:
            inputData=eval(lines.replace('\\x',''))
            if not Subject.objects.filter(Name=inputData['SubjectName']).exists():
                NewSubject=Subject(Name=inputData['SubjectName'],Desc=inputData['SubjectName'])
            NewGradeData.WriteExtention(ExtentionName=inputData['Exam'],ExtentionData=inputData['ExtentionData'],SubjectName=inputData['SubjectName'])