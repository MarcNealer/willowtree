# -*- coding: utf-8 -*-
from MIS.models import *
from ModelFunctions import *
from MIS.forms import *
import time, datetime, copy,sys
from operator import itemgetter
def ApplicantsMenu(SchoolName):
    menu= MenuBuilder(Type='New',MenuType='%sApplicants' % SchoolName,BaseGroup='Applicants')
    menu.SetLevel(0)
    menu.AddItem(GroupName='%s.Main' % SchoolName,MenuName='Main',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.Up()
    menu.SetBaseGroup('Applicants.%s.Main' % SchoolName)
    menu.AddItem(GroupName='Interview',MenuName='Interview',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])    
    menu.AddItem(GroupName='Offer',MenuName='Offer',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.AddItem(GroupName='NotOffered',MenuName='Not Offered',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.AddItem(GroupName='Wait',MenuName='Wait Listed',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.AddItem(GroupName='Declined',MenuName='Declined',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.Up()
    menu.AddItem(GroupName='Declined.NoShow',MenuName='No Show',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.AddItem(GroupName='Declined.Interview',MenuName='Interview',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.AddItem(GroupName='Declined.Place',MenuName='Place',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists']) 
    menu.AddItem(GroupName='Declined.AfterDeposit',MenuName='After Deposit',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.SetLevel(0)
    menu.SetBaseGroup('Applicants.%s' % SchoolName)
    menu.AddItem(GroupName='Accepted',MenuName='Accepted',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    
    SchoolLetter=SchoolName[0]
    
    menu.SetBaseGroup('Applicants.%s.Accepted' % SchoolName)
    menu.Up()
    menu.AddItem(GroupName='Reception',MenuName='Reception',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.Up()
    menu.AddItem(GroupName='Reception.R%sN' % SchoolLetter, MenuName='R%sN' % SchoolLetter, menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.AddItem(GroupName='Reception.R%sE' % SchoolLetter, MenuName='R%sE' % SchoolLetter, menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.AddItem(GroupName='Reception.R%sS' % SchoolLetter, MenuName='R%sS' % SchoolLetter, menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.AddItem(GroupName='Reception.R%sW' % SchoolLetter, MenuName='R%sW' % SchoolLetter, menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.SetLevel(1)
    
    if SchoolLetter =='C' or SchoolLetter=='B':
        years=range(1,9)
    else:
        years=range(1,7)
    
    for Yrs in years:
        menu.AddItem(GroupName='Year%d' % Yrs,MenuName='Year%d' % Yrs,menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
        menu.Up()
        menu.AddItem(GroupName='Year%d.%d%sN' % (Yrs,Yrs,SchoolLetter), MenuName='%d%sN' % (Yrs,SchoolLetter), menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
        menu.AddItem(GroupName='Year%d.%d%sE' % (Yrs,Yrs,SchoolLetter), MenuName='%d%sE' % (Yrs,SchoolLetter), menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
        menu.AddItem(GroupName='Year%d.%d%sS' % (Yrs,Yrs,SchoolLetter), MenuName='%d%sS' % (Yrs,SchoolLetter), menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
        menu.AddItem(GroupName='Year%d.%d%sW' % (Yrs,Yrs,SchoolLetter), MenuName='%d%sW' % (Yrs,SchoolLetter), menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
        menu.SetLevel(1)
    
    menu.SetLevel(0)
    menu.SetBaseGroup('Applicants.%s' % SchoolName)
    menu.AddItem(GroupName='Reserve',MenuName='Reserve',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    menu.AddItem(GroupName='Withdrawn',MenuName='Withdrawn',menuActions=['ManageThis','ManageAll'],SubMenus=['Lists'])
    







