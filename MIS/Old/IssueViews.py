# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q

# WillowTree system imports

from ViewExtras import SetMenu, SetLogo
from MIS.forms import *
from ModelFunctions import *



 #  #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
 #                                                                                      #
 #              Notes views                                                             #
 #  #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #

def CreateAnIssue(request, school, AcYear, PupilNo):
    ''' This View is activated from the pupil page and brings up the
    create an issue page '''
    c={'school':school,'AcYear':AcYear,'KWList':KeywordList('Issues'),'PupilDetails':PupilRecord(PupilNo,AcYear)}
    return render_to_response('CreateAnIssue.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def SaveAnIssue(request,school,AcYear,PupilNo):
    ''' called by the CreateAnIssue page to save the created Note '''
    # save the Note and return the saved record
    PupilDetails=PupilRecord(PupilNo,AcYear)
            
    c={'school':school,'AcYear':AcYear}
    c.update({'PupilDetails':PupilDetails})
    c.update({'IssueRecord':PupilDetails.AddAnIssue()})
    # Send data to see if any staff should be notified
    PupilDetails.NotifyStaffOfIssue(c)
    return render_to_response('IssueSaved.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def ViewPupilIssues(request,school,AcYear,PupilNo):
    ''' Displays all Notes that meet the filter criteria. This is called from the
    Pupil Record Page'''
    PupilDetails=PupilRecord(PupilNo,AcYear)
    c={'school':school,'AcYear':AcYear,'PupilDetails':PupilDetails,'KWList':KeywordList('Issues')}
    c.update(PupilDetails.GetIssues())
    return render_to_response('ViewPupilIssues.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def ViewStaffIssues(request,school,AcYear,StaffNo):
    ''' Displays all Notes that meet the filter criteria. This is called from the
    Pupil Record Page'''

    c={'school':school,'AcYear':AcYear,'StaffDetails':Staff.objects.get(id=int(StaffNo))}
    c.update(StaffIssueFunctions(request,StaffNo).GetIssues())
    c.update({'KWList':NoteKeywords.objects.filter(Active=True)})
    return render_to_response('ViewStaffIssues.html',c,context_instance=RequestContext(request,processors=[SetMenu]))



def FilterIssues(request,school,AcYear,PupilNo):
    ''' This is called by the View Notes page. It reads the filter form entries and
    puts them in to the session data before calling the Notes display pages. These 
    pages then use this filter data to filter the Notes to be displayed'''
    
    # # # #
    # Find any old filter data in the session and delete them
    if 'IssuesKWordFilter' in request.session:
        del request.session['IssuesKWordFilter']
    if 'IssuesStartFilter' in request.session:
        del request.session['IssuesStartFilter']
    if 'IssuesStopFilter' in request.session:
        del request.session['IssuesStopFilter']
    if 'IssuesShowAll' in request.session:
        del request.session['IssuesShowAll']

    FilterWords=[]
    #  # # ## # # # # 
    # Store Keywords in the session area
    # # # # 
    if 'KWSelect' in request.POST:
        for words in request.POST.getlist('KWSelect'):
            FilterWords.append(words)
        request.session['IssuesKWordFilter']=FilterWords
    #   #   #   #   #   #   #   #
    #  STore possible start and stop date filter data in session
    #   #   #   #   #   #   #   #   #   
    if 'StartDate' in request.POST:
        if len(request.POST['StartDate']) > 0:
            request.session['IssuesStartFilter']=request.POST['StartDate']
    if 'StopDate' in request.POST:
        if len(request.POST['StopDate']) > 0:
            request.session['IssuesStopFilter']=request.POST['StopDate']

    #   #   #   #   #   #   #   
    # Check to see if the tick box to show all has been marked and store
    # IssuesShowAll in the Session Data
    #   #   #   #   #   #   #   
    
    if 'ShowAll' in request.POST:
        if request.POST['ShowAll']:
            request.session['IssuesShowAll']=True
    else:
        request.session['IssuesShowall']=False
    return HttpResponseRedirect('/WillowTree/%s/%s/ViewPupilIssues/%s' % (school,AcYear,PupilNo))
    
def FilterStaffIssues(request,school,AcYear,StaffNo):    
    ''' This is called by the View Notes page. It reads the filter form entries and
    puts them in to the session data before calling the Notes display pages. These 
    pages then use this filter data to filter the Notes to be displayed'''
    
    # # # #
    # Find any old filter data in the session and delete them
    if 'IssuesKWordFilter' in request.session:
        del request.session['IssuesKWordFilter']
    if 'IssuesStartFilter' in request.session:
        del request.session['IssuesStartFilter']
    if 'IssuesStopFilter' in request.session:
        del request.session['IssuesStopFilter']
    if 'IssuesShowAll' in request.session:
        del request.session['IssuesShowAll']

    FilterWords=[]
    #  # # ## # # # # 
    # Store Keywords in the session area
    # # # # 
    if 'KWSelect' in request.POST:
        for words in request.POST.getlist('KWSelect'):
            FilterWords.append(words)
        request.session['IssuesKWordFilter']=FilterWords
    #   #   #   #   #   #   #   #
    #  STore possible start and stop date filter data in session
    #   #   #   #   #   #   #   #   #   
    if 'StartDate' in request.POST:
        if len(request.POST['StartDate']) > 0:
            request.session['IssuesStartFilter']=request.POST['StartDate']
    if 'StopDate' in request.POST:
        if len(request.POST['StopDate']) > 0:
            request.session['IssuesStopFilter']=request.POST['StopDate']

    #   #   #   #   #   #   #   
    # Check to see if the tick box to show all has been marked and store
    # IssuesShowAll in the Session Data
    #   #   #   #   #   #   #   
    
    if 'ShowAll' in request.POST:
        if request.POST['ShowAll']:
            request.session['IssuesShowAll']=True
    else:
        request.session['IssuesShowall']=False
    return HttpResponseRedirect('/WillowTree/%s/%s/ViewStaffIssues/%s' % (school,AcYear,StaffNo))
    
def UpdatePupilIssue(request,school,AcYear,PupilNo, IssueId):
    ''' used to add extra data to an Issue record or change the issue's status '''
    IssueRecord=IssueDetails.objects.get(id=int(IssueId))
    Messages=[]
    ReturnLink={'Reference':'pupil/%s' % PupilNo,'Text':'Return to Pupil Record'}
    if 'AddedText' in request.POST:
        if len(request.POST['AddedText']) > 30:
            IssueRecord.IssueText+="<br>   ********* %s : %s %s ************* <br>" % (datetime.datetime.today().isoformat(),request.session['StaffId'].User.first_name,request.session['StaffId'].User.last_name)
            IssueRecord.IssueText+=request.POST['AddedText']
            IssueRecord.save()
            Messages.append('Text Added to the Issue')
    if 'IssueStatus' in request.POST:
        if not request.POST['IssueStatus']== IssueRecord.IssueStatus:
            IssueRecord.IssueStatus=request.POST['IssueStatus']
            IssueRecord.save()
            PupilRecord(PupilNo,AcYear).NotifyStaffOfIssue(IssueRecord)
            Messages.append('Issue Status changed and requred notifications sent')
    c={ 'school':school, 'AcYear':AcYear, 'return_msg':Messages,'ReturnLink':ReturnLink }
    return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    
def ModifyPupilIssue(request,school,AcYear,PupilNo,IssueId):
    ''' This view builds the Modify Issue page '''
    
    c={ 'school':school, 'AcYear':AcYear,}
    c.update({'IssueDetails':IssueDetails.objects.get(id=int(IssueId))})
    c.update({'PupilDetails':Pupil.objects.get(id=int(PupilNo))})
    return render_to_response('UpdatePupilIssues.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
 
def ModifyStaffIssue(request,school,AcYear,StaffNo,IssueId):
    ''' This view builds the Modify Issue page '''
    
    c={ 'school':school, 'AcYear':AcYear,}
    c.update({'IssueDetails':IssueDetails.objects.get(id=int(IssueId))})
    c.update({'StaffDetails':Staff.objects.get(id=int(StaffNo))})
    return render_to_response('UpdateStaffIssues.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
 
def UpdateStaffIssue(request,school,AcYear,StaffNo, IssueId):
    ''' used to add extra data to an Issue record or change the issue's status '''
    IssueRecord=IssueDetails.objects.get(id=int(IssueId))
    Messages=[]
    if 'AddedText' in request.POST:
        if len(request.POST['AddedText']) > 30:
            IssueRecord.IssueText+="<br>   ********* %s : %s %s ************* <br>" % (datetime.datetime.today().isoformat(),request.session['StaffId'].UserId.User.first_name,request.session['StaffId'].UserId.User.last_name)
            IssueRecord.IssueText+=request.POST['AddedText']
            IssueRecord.save()
            Messages.append('Text Added to the Issue')
    if 'IssueStatus' in request.POST:
        if not request.POST['IssueStatus']== IssueRecord.IssueStatus:
            IssueRecord.IssueStatus=request.POST['IssueStatus']
            IssueRecord.save()
            StaffIssueFunctions(request,StaffNo).NotifyStaffOfIssue(IssueRecord)
            Messages.append('Issue Status changed and requred notifications sent')
    c={ 'school':school, 'AcYear':AcYear, 'return_msg':Messages }
    return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    
def CreateAStaffIssue(request, school, AcYear, StaffNo):
    ''' This View is activated from the pupil page and brings up the
    create an issue page '''
    StaffDetails=Staff.objects.get(id=int(StaffNo))
    Keywords=IssueKeywords.objects.filter(Active=True)
    KWPrimary=[]
    KWSecondary=[]
    for words in Keywords:
        if words.KeywordType=='Primary':
            KWPrimary.append(words.Keyword)
        else:
            KWSecondary.append(words.Keyword)
    c={'school':school,'AcYear':AcYear,'KWList':Keywords,'KWPrimary':KWPrimary,
    'KWSecondary':KWSecondary,'StaffDetails':StaffDetails}
    return render_to_response('CreateAStaffIssue.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    
def SaveAStaffIssue(request,school,AcYear,StaffNo):
    ''' called by the CreateAnIssue page to save the created Note '''
    # save the Note and return the saved record
    IssueObject=StaffIssueFunctions(request,StaffNo)
            
    c={'school':school,'AcYear':AcYear}
    c.update({'StaffDetails':Staff.objects.get(id=int(StaffNo))})
    c.update({'IssueRecord':IssueObject.AddAnIssue()})
    # Send data to see if any staff should be notified
    IssueObject.NotifyStaffOfIssue(c)
    return render_to_response('IssueStaffSaved.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
          