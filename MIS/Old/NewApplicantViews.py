# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q

# WillowTree system imports

from ViewExtras import SetMenu, SetLogo
from MIS.forms import *
from ModelFunctions import *


# # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  *******  New applicant pages  ********************
#
# # # # # # # # # # # # # # # # # # # # # # # # # # #


def NewApplicantSelectFamily(request,school,AcYear,GroupName,IndexLetter='a'):
    request.session.set_expiry(900)
    request.session['NewApplicant']={}
    request.session['NewApplicant']['TimeStamp']=datetime.datetime.today()
    request.session['NewApplicant']['GroupName']=GroupName

    FamilyList=[]
    Families=Family.objects.filter(FamilyName__istartswith=IndexLetter,Active=True)
    for FamilyDetails in Families:
        FamilyDic={}
        FamilyDic['FamilyName']=FamilyDetails.FamilyName
        FamilyDic['FamilyId']=FamilyDetails.id
        FamilyDic['Addresses']=[]
        for Addresses in FamilyDetails.Address.all():
            FamilyDic['Addresses'].append('%s %s,%s' % (Addresses.StreetNo,Addresses.StreetName,Addresses.PostCode))
        FamilyList.append(FamilyDic)
    c={'school':school,'GroupName':GroupName,'AcYear':AcYear,
    'BannerTitle':'New Applicant for %s : Select Family' % GroupName,
    'FamilyDetails':FamilyList,'TriggerAction':'NewApplicantAddress'}
    c.update(csrf(request))
    return render_to_response('NewAppSelectFamily.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def NewApplicantSelectAddress(request,school,AcYear,FamilyId,IndexLetter='a'):
    if FamilyId.find('NewFamily') < 0:
        request.session['NewApplicant']['NewFamily']=False
        request.session['NewApplicant']['FamilyId']=FamilyId
        return HttpResponseRedirect('/%s/%s/NewApplicantForm/OldFamily' % (school,AcYear))

    request.session['NewApplicant']['NewFamily']=True
    request.session.set_expiry(900)

    AddressDetails=Address.objects.filter(Active=True,StreetName__istartswith=IndexLetter).order_by('StreetName')

    c={'school':school,'AcYear':AcYear,'Addresses':AddressDetails,
    'BannerTitle':'New Applicant : Choose Postal Address','TriggerAction':'NewApplicantForm'}
    c.update(csrf(request))

    return render_to_response('NewAppSelectAddress.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def NewApplicantForm(request,school,AcYear,AddressId):
    ReadOnlyFields=['PostalTitle','StreetNo','HomeSal','StreetName','City','County','Country','PostCode','Phone1','Phone2','Phone3','Email']

    if AddressId.find('NewAddress') > -1:
        form=NewApplicant()
        request.session['NewApplicant']['NewAddress']=True
    elif request.session['NewApplicant']['NewFamily']:
        request.session['NewApplicant']['NewAddress']=False
        AddressDetails=Address.objects.get(id=int(AddressId))
        request.session['NewApplicant']['AddressId']=AddressDetails.id
        form=NewApplicant(initial={
        'PostalTitle':AddressDetails.PostalTitle,
        'HomeSal':AddressDetails.HomeSalutation,
        'StreetNo':AddressDetails.StreetNo,
        'StreetName':AddressDetails.StreetName,
        'City':AddressDetails.City,
        'County':AddressDetails.County,
        'Country':AddressDetails.Country,
        'PostCode':AddressDetails.PostCode,
        'Phone1':AddressDetails.Phone1,
        'Phone2':AddressDetails.Phone2,
        'Phone3':AddressDetails.Phone3,
        'Email':AddressDetails.EmailAddress,
        'Notes':AddressDetails.Note})
        for fields in form.fields:
            if fields in ReadOnlyFields:
                form[fields].field.widget.attrs['readonly']='readonly'
                form[fields].field.widget.attrs['class']='WillowTableReadOnly'
            else:
                form[fields].field.widget.attrs['class']='WillowTableEdit'
    else:
        request.session['NewApplicant']['NewAddress']=False
        FamilyDetails=Family.objects.get(id=int(request.session['NewApplicant']['FamilyId']))
        AddressDetails=FamilyDetails.Address.all()[0]
        request.session['NewApplicant']['AddressId']=AddressDetails.id
        form=NewApplicant(initial={'Family':FamilyDetails.FamilyName,
        'PostalTitle':AddressDetails.PostalTitle,
        'HomeSal':AddressDetails.HomeSalutation,
        'StreetNo':AddressDetails.StreetNo,
        'StreetName':AddressDetails.StreetName,
        'City':AddressDetails.City,
        'County':AddressDetails.County,
        'Country':AddressDetails.Country,
        'PostCode':AddressDetails.PostCode,
        'Phone1':AddressDetails.Phone1,
        'Phone2':AddressDetails.Phone2,
        'Phone3':AddressDetails.Phone3,
        'Email':AddressDetails.EmailAddress,
        'Notes':AddressDetails.Note})
        ReadOnlyFields.append('Family')
        for fields in form.fields:
            if fields in ReadOnlyFields:
                form[fields].field.widget.attrs['readonly']='readonly'
                form[fields].field.widget.attrs['class']='WillowTableReadOnly'
            else:
                form[fields].field.widget.attrs['class']='WillowTableEdit'

    request.session.set_expiry(900)
    c={'school':school,'AcYear':AcYear,'ApplicantsForm':form,
    'BannerTitle':'Add Applicant','TriggerAction':'NewApplicantProccess'}
    c.update(csrf(request))
    return render_to_response('NewApp1.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def NewApplicantProccess(request,school,AcYear):
    Details=NewApplicantNF(request.POST)
    if Details.is_valid():
        CleanDetails=Details.cleaned_data
        if 'NewApplicant' in request.session:
            # # # # # #
            # if a new  Address add here
            # # # # # #
            if request.session['NewApplicant']['NewAddress']:
                NewAddress=Address(HomeSalutation=CleanDetails['HomeSal'],
                PostalTitle=CleanDetails['PostalTitle'],
                StreetNo=CleanDetails['StreetNo'],
                StreetName=CleanDetails['StreetName'],
                City=CleanDetails['City'],
                County=CleanDetails['County'],
                Country=CleanDetails['Country'],
                PostCode=CleanDetails['PostCode'],
                Phone1=CleanDetails['Phone1'],
                Phone2=CleanDetails['Phone2'],
                Phone3=CleanDetails['Phone3'],
                EmailAddress=CleanDetails['Email'],
                Note=CleanDetails['Notes'])
                NewAddress.save()
            else:
                NewAddress=Address.objects.get(id=request.session['NewApplicant']['AddressId'])
            if request.session['NewApplicant']['NewFamily']:
                # # # # # # #
                # if  new family add here
                # # # # # # #
                NewFamily=Family(FamilyName=CleanDetails['Family'])
                NewFamily.save()
                NewFamily.Address.add(NewAddress)
                NewEmergency=FamilyEmergencyContact(FamilyId=NewFamily,Address=NewAddress,Priority=1)
                NewEmergency.save()
            else:
                NewFamily=Family.objects.get(id=request.session['NewApplicant']['FamilyId'])

             # # # # # # # # #
             # Add child Details here ff
             # # # # # # # # #

            NewApplicant=Pupil(Forename=CleanDetails['Forename'],
            Surname=CleanDetails['Surname'],
            OtherNames=CleanDetails['OtherNames'],
            Title=CleanDetails['Title'],
            DateOfBirth=CleanDetails['DOB'],
            Gender=CleanDetails['Gender'])
            NewApplicant.save()

            # # # # # #
            # create relationship tp family
            # # # # # #
            NewRelationship=FamilyChildren(FamilyId=NewFamily,
            Pupil=NewApplicant,
            Relationship=FamilyRelationship.objects.get(Type='Child'),
            UsePostalAddress=True)
            NewRelationship.save()

            #  # # # # # # #
            # Add child to the relevent group
            # # # # # # # # #
            NewGroup=PupilGroup(AcademicYear='Applicants',
            Group=Group.objects.get(Name=request.session['NewApplicant']['GroupName']),
            Pupil=NewApplicant)
            NewGroup.save()
        # # # # # # #
        # delete form data in the session cache
        # # # # # # #
        del request.session['NewApplicant']
        return HttpResponseRedirect('/%s/%s/pupil/%d' % (school,AcYear,NewApplicant.id))
    else:
        form=Details
        request.session.set_expiry(900)
        c={'school':school,'AcYear':AcYear,'ApplicantsForm':form,
        'BannerTitle':'New Applicant:  Enter Applicant Details' ,'TriggerAction':'NewApplicantProccess'}
        c.update(csrf(request))
        return render_to_response('NewApp1.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

