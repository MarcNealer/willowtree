# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q

# WillowTree system imports

from ViewExtras import SetMenu, SetLogo
from MIS.forms import *
from ModelFunctions import *

def PageNotFound(request):
    ''' PageNotFound : Used to render the page not found '''
    c={'errmsg': 'Page Not found : %s' % request.META['PATH_INFO']}
    return render_to_response('Welcome.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


# # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  *******  Attendance System ********************
#
# # # # # # # # # # # # # # # # # # # # # # # # # # #

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Creates the primary Attendance page when Attendance is chosen from the menu
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def AttendanceForm(request,school,AcYear,GroupName,Am_Pm=None):
    ''' This view creates the Attendance input form for teachers'''
    request.session.set_expiry(900)
    c={'school':school,'GroupName':GroupName,'AcYear':AcYear}
    c.update(csrf(request))
    TimeNow=datetime.datetime.now()

    if TimeNow.hour > 8 and TimeNow.hour < 13 and TimeNow.hour > 13 and school.find('Admin') < 0:
            return HttpResponseRedirect('/WillowTree/%s/%s/AttendanceClosed/' % (school,AcYear))
    else:
        PupilList=GroupFunctions.Get_PupilList(AcYear,GroupName=GroupName) 
        if Am_Pm==None:
            c['AmPm']=datetime.datetime.today().strftime('%p')
            Am_Pm=c['AmPm']
        else:
            c['AmPm']=Am_Pm
        AttendanceData=[]
        for pupils in PupilList:
            AlertItems=Alerts(request,pupils.id)
            AttendanceRecord=[pupils.id,pupils.Surname,pupils.Forename,pupils.Picture,'/',True,AlertItems.ActivePupilAlerts(),False]
            if PupilAttendance.objects.filter(Pupil=pupils.id,AttendanceDate=datetime.datetime.today(),Active=True,AmPm=Am_Pm).exists():
                NewCode=PupilAttendance.objects.get(Pupil=pupils,AttendanceDate=datetime.datetime.today(),Active=True,AmPm=Am_Pm)
                AttendanceRecord[4]=str(NewCode.Code.AttendanceCode)
                AttendanceRecord[7]=True
                AdminPerm=False
                for Perms in NewCode.Teacher.User.get_all_permissions():
                    if Perms.find('Admin') > -1:
                        AdminPerm=True
                if not AdminPerm and school.find('Admin') < 0:
                    AttendanceRecord[5]=False

            AttendanceData.append(AttendanceRecord)
        c.update({'PupilList':sorted(AttendanceData,key=itemgetter(1,0))})
        if school.find('Admin') > -1:
            c.update({'AttendanceCodes':AttendanceCode.objects.all()})
        else:
            c.update({'AttendanceCodes':AttendanceCode.objects.filter(AttendanceAccess='Teacher')})
        return render_to_response('DailyAttendanceForm.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def AttendanceP(request,school,AcYear,GroupName,Am_Pm):
    AttendanceReport=[]
    for items in request.POST:
        if items.find('Code_')> -1:
            Pupil_Id=items.split('_')[1]
            PupilObject=Pupil.objects.get(id=int(Pupil_Id))
            if request.POST[items]=='/':
                if PupilAttendance.objects.filter(Pupil=PupilObject,AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm,Active=True).exists():
                    AttendanceRecord=PupilAttendance.objects.get(Pupil=Pupil_Id,AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm)
                    AttendanceRecord.Active=False
                    AttendanceRecord.save()
                    AttendanceReport.append('%s is has been Updated at present' % PupilObject.FullName())
            else:
                if PupilAttendance.objects.filter(Pupil=PupilObject,AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm,Active=True).exists():
                    AttendanceRecord=PupilAttendance.objects.get(Pupil=Pupil_Id,AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm)
                    if not AttendanceRecord.Code==AttendanceCode.objects.get(AttendanceCode=request.POST[items]):
                        AttendanceRecord.Active=False
                        AttendanceRecord.save()
                        # test
                        NewRecord=PupilAttendance(Pupil=PupilObject,
                        AttendanceDate=datetime.datetime.today(),AmPm=Am_Pm,
                        AttendanceType='D',Group=Group.objects.get(Name=GroupName),
                        Teacher=request.session['StaffId'],
                        Code=AttendanceCode.objects.get(AttendanceCode=request.POST[items]),
                        RecordedDate=datetime.datetime.today(),Active=True)
                        NewRecord.save()
                        AttendanceReport.append('%s has been updated as %s' % (PupilObject.FullName(), request.POST[items]))
                else:
                        NewRecord=PupilAttendance(Pupil=PupilObject,
                        AttendanceDate=datetime.datetime.today(),
                        AmPm=Am_Pm,AttendanceType='D',
                        Group=Group.objects.get(Name=GroupName),
                        Teacher=request.session['StaffId'],
                        Code=AttendanceCode.objects.get(AttendanceCode=request.POST[items]),
                        RecordedDate=datetime.datetime.today(),
                        Active=True)
                        NewRecord.save()
                        AttendanceReport.append('%s has been marked as %s' % (PupilObject.FullName(), request.POST[items]))
                        

    if not AttendanceTaken.objects.filter(AttendanceDate=datetime.datetime.today(),Group__Name=GroupName,AmPm=Am_Pm).exists():
        AttTakenRecord=AttendanceTaken(AttendanceDate=datetime.datetime.today(),
        AttendanceType='D',Group=Group.objects.get(Name=GroupName),AmPm=Am_Pm,
        RecordedDate=datetime.datetime.now(),Teacher=request.session['StaffId'])
        AttTakenRecord.save()

    request.session.set_expiry(900)
    c={'school':school,'AcYear':AcYear,'BannerTitle':'Attendance records saved for %s' % GroupName,'return_msg':AttendanceReport}
    c.update(csrf(request))
    return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def DailyAttendanceReport(request,school,AcYear,ReportDate=None):
    if ReportDate==None:
        AttendanceDate=datetime.date.today()
    else:
        AttendanceDate=request.POST['AttendanceDate']
    ClassList=DailyAttendance.Get_ClassList(school)
    if type(AttendanceDate)=='date':
        DisplayDate=AttendanceDate.strftime("%Y-%m-%d")
    else:
        DisplayDate=AttendanceDate
    AttReport=[]
    for Classes in ClassList:
        AttReport.append(DailyAttendance(AttendanceDate,Classes))
    request.session.set_expiry(1800)
    c={'school':school,'AcYear':AcYear,'BannerTitle':'Daily Attendance Report for %s' % DisplayDate}
    c.update(csrf(request))
    c.update({'AttendanceReport':AttReport})
    return render_to_response('DailyAttendanceReport.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
        
        