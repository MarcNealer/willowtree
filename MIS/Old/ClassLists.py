from django.http import HttpResponse, Http404
from django.shortcuts import render_to_response
from MIS.models import *
from ModelFunctions import *
from django.template import loader, RequestContext
from MIS.forms import *
from MIS.views import SetMenu



def PupilList(request,school,AcYear,GroupName,ListType):
    PupilList=GroupFunctions.Get_PupilList(AcYear,GroupName)
    ClassList=[]
    if GroupName.find('Applicant') > -1:
        AcYear='Applicants'
    for pupil in PupilList:
        ClassList.append(PupilRecord(pupil.id,AcYear))
    HTMLfile = '%sList.html' %ListType
    return render_to_response(HTMLfile,{'school':school,'AcYear':AcYear,'ClassList':sorted(ClassList, key=lambda pupil: pupil.Pupil.Surname),'GroupName':GroupName,'School':school,'AcYear':AcYear},context_instance=RequestContext(request,processors=[SetMenu]))
    
def StaffList(request,school,AcYear,GroupName,ListType):
    StaffList=GroupFunctions.Get_StaffList(AcYear,GroupName)
    ClassList=[]
    for Staff in StaffList:
        ClassList.append(StaffRecord(request,Staff.id,AcYear))
    HTMLfile = '%sList.html' % ListType
    return render_to_response(HTMLfile,{'school':school,'AcYear':AcYear,'StaffList':StaffList,'ClassList':sorted(ClassList, key=lambda Staff: Staff.Staff.Surname),'GroupName':GroupName,'School':school,'AcYear':AcYear},context_instance=RequestContext(request,processors=[SetMenu]))
