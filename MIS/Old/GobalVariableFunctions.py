from MIS.models import *
from django import template
import datetime, copy
from datetime import timedelta, date
from django.core.cache import cache


class GlobalVariables():
    def __init__(self,xVarType):
        self.VariableList=SystemVariable.objects.filter(VariableType=xVarType)
    def GetVariableList(self,xVarName):
        return SystemVarData.objects.filter(Variable__in=self.VariableList.filter(Name__icontains=xVarName))
    @staticmethod
    def Get_SystemVar(VariableName,AcYear=None):
        VarDetails=SystemVariable.objects.get(Name=VariableName)
        if 'Yearly' in VarDetails.VariableType:
            if not AcYear:
                AcYear=WillowFunc.Get_SystemVar(CurrentYear)
        else:
            if not AcYear:
                AcYear='System'
                
        if SystemVarData.objects.filter(Variable=VarDetails,AcademicYear=AcYear).exists():
            Sysdata=SystemVarData.objects.get(Variable=VarDetails,AcademicYear=AcYear)
            if VarDetails.DataType=='Numerical':
                return float(Sysdata.Data)
            elif VarDetails.DataType=='Date':
                return datetime.datetime.strptime(Sysdata.Data,'%Y-%m-%d')
            elif VarDetails.DataType=='Boolean':
                return eval(Sysdata.Data)
            else:
                return Sysdata.Data
        else:
            return False
        
    @staticmethod
    def Write_SystemVar(VariableName,VariableData,AcYear=None):
        VarDetails=SystemVariable.objects.get(Name=VariableName)
        if 'Yearly' in VarDetails.VariableType:
            if not AcYear:
                AcYear=WillowFunc.Get_SystemVar(CurrentYear)
        else:
            AcYear='System'
        if SystemVarData.objects.filter(Variable=VarDetails,AcademicYear=AcYear).exists():
            Sysdata=SystemVarData.objects.get(Variable=VarDetails,AcademicYear=AcYear)
            if VarDetails.DataType=='Date':
                Sysdata.DataVariableData.strftime('%Y-%m-%d')
            else:
                Sysdata.Data=str(VariableData)
        else:
            if VarDetails.DataType=='Date':
                Sysdata=SystemVarData(Variable=VarDetails,AcademicYear=AcYear,Data=VariableData.strftime('%Y-%m-%d'))
            else:
                Sysdata=SystemVarData(Variable=VarDetails,AcademicYear=AcYear,Data=str(VariableData))   
        Sysdata.save()
        return
    
