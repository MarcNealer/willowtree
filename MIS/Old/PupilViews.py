from ModelFunctions import UpdatePupilRecord
from django.http import *

def EditPupilDetails(request,school,AcYear,PupilNo):
    newPupilRecord = UpdatePupilRecord(request,AcYear,school,PupilNo)
    newPupilRecord.Base()
    newPupilRecord.addToGroup('house',request.POST['AcHouse'])    
    newPupilRecord.addToGroup('form',request.POST['Form'])  
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,AcYear,PupilNo))
    
def EditPupilPermissions(request,school,AcYear,PupilNo):
    try:    
        internetPermissions = request.POST['internet_permissions_checkbox']
    except:
        internetPermissions = False
    try:
        mediaPermissions = request.POST['media_permissions_checkbox']
    except:
        mediaPermissions = False
    UpdatePupilPermissions = UpdatePupilRecord(request,AcYear,school,PupilNo)
    UpdatePupilPermissions.Permissions(internetPermissions=internetPermissions,mediaPermissions=mediaPermissions)
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,AcYear,PupilNo))
    
def EditPupilOtherInformation(request,school,AcYear,PupilNo):
    UpdatePupilPermissions = UpdatePupilRecord(request,AcYear,school,PupilNo)
    UpdatePupilPermissions.UpdatePupilOtherInformation()    
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school,AcYear,PupilNo))