# Base python libray imports
import time
from operator import itemgetter

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.db.models import Q

# WillowTree system imports

from ViewExtras import SetMenu, SetLogo
from MIS.forms import *
from ModelFunctions import *



 #  #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
 #                                                                                      #
 #              Notes views                                                             #
 #  #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #

def CreateAStaffNote(request, school, AcYear):
    ''' This view sends out the Create a Note page. ikts trigged by either the 
    NoteForAPupil, or NoteForAGroup View '''
    StaffId=request.session['NoteStaffId']
    Keywords=NoteKeywords.objects.filter(Active=True)
    KWPrimary=[]
    KWSecondary=[]
    for words in Keywords:
        if words.KeywordType=='Primary':
            KWPrimary.append(words.Keyword)
        else:
            KWSecondary.append(words.Keyword)
    c={'school':school,'AcYear':AcYear,'KWList':Keywords,'KWPrimary':KWPrimary,'KWSecondary':KWSecondary,'StaffId':StaffId}
    return render_to_response('CreateAStaffNote.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def NoteForAPupil(request,school,AcYear,PupilNo):
    ''' Triggered from the pupil Page to add a note for the selected pupil. It
    does this by storing the Pupil ID in NotePupilList in the session data and
    then calling Create a Note via a redirect'''
    c={'school':school,'AcYear':AcYear,'KWList':KeywordList('Note'),'PupilList':[Pupil.objects.get(id=int(PupilNo))]}
    return render_to_response('CreateANote.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def NoteForAGroup(request,school,AcYear,GroupName):
    ''' triggered from the menu. It gets a list of pupils for the selcted group
    and puts them into the session data before calling the CreateANote via the URL'''
    PupilList=[]
    for pupils in request.POST['PupilList']:
        PupilList.append(Pupil.objects.get(id=int(Pupils)))
    c={'school':school,'AcYear':AcYear,'KWList':KeywordList('Note'),'PupilList':PupilList}
    return render_to_response('CreateANote.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    
def NoteForStaff(request,school,AcYear,StaffNo):
    ''' Triggered from the pupil Page to add a note for the selected pupil. It
    does this by storing the Pupil ID in NotePupilList in the session data and
    then calling Create a Note via a redirect'''
    request.session['NoteStaffId']=Staff.objects.get(id=int(StaffNo))
    return HttpResponseRedirect('/WillowTree/%s/%s/CreateAStaffNote/' % (school,AcYear))    

def SaveANote(request,school,AcYear):
    ''' called by the CreateANote page to save the created Note '''
    PupilList=[]
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        PupilList.append(Pupil.objects.get(id=int(Pupil_Id)))
    if len(PupilList) > 0:
        # save the Note and return the saved record
        NoteRecord=NoteFunctions.AddPupilNotes(request,PupilList)
            
        c={'school':school,'AcYear':AcYear,'PupilList':PupilList,'NoteRecord':NoteRecord}
        # Send data to see if any staff should be notified
        NoteFunctions.NotifyStaffOfNote(c)
        return render_to_response('NoteForPupilSaved.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    else:
        ErrorMsg='No pupils selected thus no Note has been saved'
        c={'school':school,'AcYear':AcYear,'errmsg':ErrorMsg}
        return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def SaveANoteManager(request,school,AcYear,GroupName):
    ''' called by the CreateANote page to save the created Note '''
    PupilList=[]
    for Pupil_Id in request.POST.getlist('PupilSelect'):
        PupilList.append(Pupil.objects.get(id=int(Pupil_Id)))
    if len(PupilList) > 0:
        # save the Note and return the saved record
        NoteRecord=NoteFunctions.AddPupilNotes(request,PupilList)
            
        if len(PupilList)==1:
            request.session['Messages']=['Note saved for %s' % PupilList[0].FullName()]
        else:
            request.session['Messages']=['Note saved for %d Pupils' % len(PupilList)]

    else:
        request.session['Messages']=['No pupils selected thus no Note has been saved']
    return HttpResponseRedirect('/WillowTree/%s/%s/Manage/%s/' % (school, AcYear, GroupName))
    
def SaveANoteManagerSinglePupil(request,school,AcYear,PupilNo):
    ''' called by the CreateANote page to save the created Note for a single pupil'''
    PupilList=[]
    PupilList.append(Pupil.objects.get(id=int(PupilNo)))
    NoteRecord=NoteFunctions.AddPupilNotes(request,PupilList)
    return HttpResponseRedirect('/WillowTree/%s/%s/pupil/%s/' % (school, AcYear, PupilNo))
        
def SaveAStaffNote(request,school,AcYear):
    ''' called by the CreateANote page to save the created Note '''
    # save the Note and return the saved record
    Staff_Id=request.session['NoteStaffId']
    del request.session['NoteStaffId']
    NoteRecord=NoteFunctions.AddNote(request,BaseId=Staff_Id,Type='Staff')
      
    c={'school':school,'AcYear':AcYear,'BaseId':Staff_Id,'NoteRecord':NoteRecord,'BaseType':'Staff'}
    # Send data to see if any staff should be notified
    NoteFunctions.NotifyStaffOfNote(c)
    return render_to_response('NoteSaved.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
   

def ViewPupilNotes(request,school,AcYear,PupilNo):
    ''' Displays all Notes that meet the filter criteria. This is called from the
    Pupil Record Page'''
    PupilDetails=PupilRecord(PupilNo,AcYear)
    keywords = KeywordList('Notes')
    c={'school':school,'AcYear':AcYear,'PupilDetails':PupilDetails,'KWList':keywords}
    c.update(NoteFunctions.GetPupilNotes(request,Pupil_Id=PupilNo)) 
    try:
        c.update({"NotesKWordFilter":request.session['NotesKWordFilter']})
    except:
        pass
    try:
        c.update({"NotesStartFilter":request.session['NotesStartFilter']})
    except:
        pass
    try:
        c.update({"NotesStopFilter":request.session['NotesStopFilter']})
    except:
        pass
    return render_to_response('ViewPupilNotes.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

    
def ViewPupilIssues(request,school,AcYear,PupilNo):
    ''' Displays all Notes that meet the filter criteria. This is called from the
    Pupil Record Page'''
    PupilDetails=PupilRecord(PupilNo,AcYear)
    c={'school':school,'AcYear':AcYear,'PupilDetails':PupilDetails,'KWList':KeywordList('Notes')}
    c.update(NoteFunctions.GetPupilNotes(request,Pupil_Id=PupilNo))
    return render_to_response('ViewPupilIssues.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
     
def ViewStaffNotes(request,school,AcYear,StaffNo):
    ''' Displays all Notes that meet the filter criteria. This is called from the
    Pupil Record Page'''
    c={'school':school,'AcYear':AcYear,'StaffDetails':Staff.objects.get(id=int(StaffNo)),'KWList':NoteKeywords.objects.filter(Active=True)}
    c.update(NoteFunctions.GetNotes(request,BaseId=int(StaffNo),Type='Staff'))
    return render_to_response('ViewStaffNotes.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def FilterNotes(request,school,AcYear,DisplayType):
    ''' This is called by the View Notes page. It reads the filter form entries and
    puts them in to the session data before calling the Notes display pages. These 
    pages then use this filter data to filter the Notes to be displayed'''
    if 'NotesKWordFilter' in request.session:
        del request.session['NotesKWordFilter']
    if 'NotesStartFilter' in request.session:
        del request.session['NotesStartFilter']
    if 'NotesStopFilter' in request.session:
        del request.session['NotesStopFilter']

    FilterWords=[]
    if 'KWSelect' in request.POST:
        for words in request.POST.getlist('KWSelect'):
            FilterWords.append(words)
        request.session['NotesKWordFilter']=FilterWords
    if 'StartDate' in request.POST:
        if len(request.POST['StartDate']) > 0:
            request.session['NotesStartFilter']=request.POST['StartDate']
    if 'StopDate' in request.POST:
        if len(request.POST['StopDate']) > 0:
            request.session['NotesStopFilter']=request.POST['StopDate']
    if DisplayType=='Pupil':
        return HttpResponseRedirect('/WillowTree/%s/%s/ViewPupilNotes/%s' % (school,AcYear,request.POST['PupilNo']))
    elif DisplayType=='Group':
        return HttpResponseRedirect('/WillowTree/%s/%s/ViewGroupNotes/%s' % (school,AcYear,request.POST['GroupName']))
    elif DisplayType=='Staff':
        return HttpResponseRedirect('/WillowTree/%s/%s/ViewStaffNotes/%s' % (school,AcYear,request.POST['StaffId']))
  
        
def ListGroupNotes(request,school,AcYear,GroupName):
    ''' To display Notes for a group, we show a filter page first as there could be a large number of 
    Notes for the selected group. The user then sets filter options before going to the
    ViewGroupNotes page'''
    Keywords=NoteKeywords.objects.filter(Active=True)
    c={'school':school,'AcYear':AcYear,'GroupName':GroupName,'KWList':Keywords}
    return render_to_response('FilterGroupNotes.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def ViewGroupNotes(request,school,AcYear):
    ''' Displayes the Notes for a selected group that match the filter criteria'''
    Pupils=[]
    for pupils in request.POST['PupilList']:
        Pupils.append(Pupil.objects.get(id=int(Pupils)).id)
    c={'school':school,'AcYear':AcYear,'PupilList':Pupils,'KWList':NoteKeywords.objects.filter(Active=True)}
    c.update(NoteFunctions.GetPupilNotes(request,Pupil_List=Pupils))
    return render_to_response('ViewGroupNotes.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def ModifyPupilNote(request,school,AcYear,PupilNo, NoteId):
    ''' Used to either inactivate or modify a Note. If remove is selected, it removes the Note and
    sends a message to say the Note has been removed.
    If its a modify, it gets the Notes details and sends the data to the ModifyANote template'''
    if 'Remove' in request.POST:
        if NoteFunctions.PermissionToRemove(request):
            if NoteFunctions.RemoveSingleNote(Note_Id=NoteId,BaseId=int(PupilNo),NoteType='Pupil'):
                PupilDetails=PupilRecord(PupilNo,AcYear)
                Message='Note Removed for <a href="/WillowTree/%s/%s/pupil/%s">%s %s</a>' % (school, AcYear, PupilDetails.Pupil.id,PupilDetails.Pupil.Forename,PupilDetails.Pupil.Surname)
            else:
                Message='No corresponding Note found. No action has been taken NoteId:%s PupilId:%s' % (NoteId, PupilNo)
        else:
            Message='You do not have permission to close this Note. Please contact your dept head'
        c={'school':school,'AcYear':AcYear,'return_msg':[Message]}
        return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    else:
        NoteRecord=NoteFunctions.GetSingleNote(Note_Id=NoteId)
        
        if NoteRecord and NoteFunctions.PermissionToUpdate(request,NoteId):
            Keywords=NoteKeywords.objects.filter(Active=True)
            KWPrimary=[]
            KWSecondary=[]
            for words in Keywords:
                if words.KeywordType=='Primary':
                    KWPrimary.append(words.Keyword)
                else:
                    KWSecondary.append(words.Keyword)
            c={'school':school,'AcYear':AcYear,'NoteRecord':NoteRecord,'KWList':KeywordList('Note'),'PupilList':[Pupil.objects.get(id=int(PupilNo))]}
            return render_to_response('ModifyANote_New.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
        else:
            c={'school':school,'AcYear':AcYear,'return_msg':["Sorry, but you don't have access to Modify this record"]}
            return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def RemovePupilNote(request,school,AcYear,PupilNo, NoteId):
    ''' Used to either inactivate or modify a Note. If remove is selected, it removes the Note and
    sends a message to say the Note has been removed.
    If its a modify, it gets the Notes details and sends the data to the ModifyANote template'''
    if NoteFunctions.PermissionToRemove(request):
        if NoteFunctions.RemoveSingleNote(Note_Id=NoteId,BaseId=int(PupilNo),NoteType='Pupil'):
            PupilDetails=PupilRecord(PupilNo,AcYear)
            Message='Note Removed for <a href="/WillowTree/%s/%s/pupil/%s">%s %s</a>' % (school, AcYear, PupilDetails.Pupil.id,PupilDetails.Pupil.Forename,PupilDetails.Pupil.Surname)
        else:
            Message='No corresponding Note found. No action has been taken NoteId:%s PupilId:%s' % (NoteId, PupilNo)
    else:
        Message='You do not have permission to close this Note. Please contact your dept head'
    Message = Message+"<br /><br /><a href='/WillowTree/%s/%s/ViewPupilNotes/%s/'>Back to Notes?</a>" % (school, AcYear, PupilNo)
    c={'school':school,'AcYear':AcYear,'return_msg':[Message]}
    return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def RemovePupilNote2(request,school,AcYear,PupilNo, NoteId):
    '''ONLY Removes a pupil note (no modify function) and returns the user to the view pupil notes 
    page without going to a note deleted confirmation page as with the orginal function'''
    NoteFunctions.RemoveSingleNote(Note_Id=NoteId,BaseId=int(PupilNo),NoteType='Pupil')
    return HttpResponseRedirect('/WillowTree/%s/%s/ViewPupilNotes/%s/' % (school,AcYear,PupilNo))

def ModifyPupilNotePopUp(request,school,AcYear,PupilNo, NoteId):
    ''' Used to either inactivate or modify a Note. If remove is selected, it removes the Note and
    sends a message to say the Note has been removed.
    If its a modify, it gets the Notes details and sends the data to the ModifyANote template'''
    if 'Remove' in request.POST:
        if NoteFunctions.PermissionToRemove(request):
            if NoteFunctions.RemoveSingleNote(Note_Id=NoteId,BaseId=int(PupilNo),NoteType='Pupil'):
                PupilDetails=PupilRecord(PupilNo,AcYear)
                Message='Note Removed for <a href="/WillowTree/%s/%s/pupil/%s">%s %s</a>' % (school, AcYear, PupilDetails.Pupil.id,PupilDetails.Pupil.Forename,PupilDetails.Pupil.Surname)
            else:
                Message='No corresponding Note found. No action has been taken NoteId:%s PupilId:%s' % (NoteId, PupilNo)
        else:
            Message='You do not have permission to close this Note. Please contact your dept head'
        c={'school':school,'AcYear':AcYear,'return_msg':[Message]}
        return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    else:
        NoteRecord=NoteFunctions.GetSingleNote(Note_Id=NoteId)
        
        if NoteRecord and NoteFunctions.PermissionToUpdate(request,NoteId):
            Keywords=NoteKeywords.objects.filter(Active=True)
            KWPrimary=[]
            KWSecondary=[]
            for words in Keywords:
                if words.KeywordType=='Primary':
                    KWPrimary.append(words.Keyword)
                else:
                    KWSecondary.append(words.Keyword)
            c={'school':school,'AcYear':AcYear,'NoteRecord':NoteRecord,'KWList':KeywordList('Note'),'PupilList':[Pupil.objects.get(id=int(PupilNo))]}
            return render_to_response('ModifyANote_New.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
        else:
            c={'school':school,'AcYear':AcYear,'return_msg':["Sorry, but you don't have access to Modify this record"]}
            return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
            
def ModifyStaffNote(request,school,AcYear,StaffNo, NoteId):
    ''' Used to either inactivate or modify a Note. If remove is selected, it removes the Note and
    sends a message to say the Note has been removed.
    If its a modify, it gets the Notes details and sends the data to the ModifyANote template'''
    if 'Remove' in request.POST:
        if NoteFunctions.PermissionToRemove(request):
            if NoteFunctions.RemoveSingleNote(Note_Id=NoteId,BaseId=int(StaffNo),NoteType='Staff'):
                Message='Note %s has been marked as Inactive and will not appear on the Staff %s record' % (NoteId, StaffNo)
            else:
                Message='No corresponding Note found. No action has been taken NoteId:%s StaffId:%s' % (NoteId, StaffNo)
        else:
            Message='You do not have permission to close this Note. Please contact your dept head'
        c={'school':school,'AcYear':AcYear,'return_msg':[Message]}
        return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

    else:
        NoteRecord=NoteFunctions.GetSingleNote(Note_Id=NoteId)
        
        
        if NoteRecord and NoteFunctions.PermissionToUpdate(request,NoteId):
            Keywords=NoteKeywords.objects.filter(Active=True)
            KWPrimary=[]
            KWSecondary=[]
            for words in Keywords:
                if words.KeywordType=='Primary':
                    KWPrimary.append(words.Keyword)
                else:
                    KWSecondary.append(words.Keyword)
            c={'school':school,'AcYear':AcYear,'NoteRecord':NoteRecord,'KWList':Keywords,'KWPrimary':KWPrimary,'KWSecondary':KWSecondary}
            return render_to_response('ModifyANote.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
        else:
            c={'school':school,'AcYear':AcYear,'return_msg':["Sorry, but you don't have access to Modify this record"]}
            return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
                        
def UpdateANote(request,school,AcYear):
    ''' Tiggered by the ModifyANote page. It triigers the UpdateNoteFromRequest fucntion
    which first off inactivates the old Note before adding a new one'''
    PupilList=NoteFunctions.RemoveNote(request.POST['NoteId'],Type='Pupil')
    NoteFunctions.AddPupilNotes(request,PupilList)          
    c={'school':school,'AcYear':AcYear,'PupilList':PupilList}
    return render_to_response('UpdatedMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    
def UpdateANote2(request,school,AcYear,PupilNo,NoteId):
    ''' Tiggered by the ModifyANote page. It triigers the UpdateNoteFromRequest fucntion
    which first off inactivates the old Note before adding a new one'''
    PupilList=NoteFunctions.RemoveNote(NoteId,Type='Pupil')
    NoteFunctions.AddPupilNotes(request,PupilList)          
    c={'school':school,'AcYear':AcYear,'PupilList':PupilList}
    return render_to_response('UpdatedMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
    
def UpdateAStaffNote(request,school,AcYear):
    ''' Tiggered by the ModifyANote page. It triigers the UpdateNoteFromRequest fucntion
    which first off inactivates the old Note before adding a new one'''
    if NoteFunctions.UpdateNoteFromRequest(request,Type='Staff'):
        Messages=['Note successfully Updated']
    else:
        Messages['Note Failed to Update successfully']
    c={'school':school,'AcYear':AcYear,'return_msg':Messages}
    return render_to_response('ShowMsg.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

def UpdateIssue(request,school,AcYear,IssueNo,PupilNo):
    NoteRecord=NoteDetails.objects.get(id=int(IssueNo))
    NoteRecord.NoteText+="<br>   ********* %s : %s %s ************* <br>" % (datetime.datetime.today().strftime('%d/%m/%y'),request.session['StaffId'].User.first_name,request.session['StaffId'].User.last_name)
    NoteRecord.NoteText+=request.POST['NoteTextArea']
    NoteRecord.IssueStatus=request.POST['IssueStatus%s' % IssueNo]
    NoteRecord.save()
    request.session['Messages']=['Text Added to Issue %d' % NoteRecord.id]    
    return HttpResponseRedirect('/WillowTree/%s/%s/ViewPupilIssues/%s/' % (school, AcYear, PupilNo))
                        