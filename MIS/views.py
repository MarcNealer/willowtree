# Base python libray imports
import time
from operator import itemgetter
import datetime

# Django middleware imports
from django.shortcuts import render_to_response
from django.template import loader, RequestContext
from django.contrib.auth import authenticate, login, logout
from django.core.context_processors import csrf
from django.http import *
from django.conf import settings
from django.core.servers.basehttp import FileWrapper
from django.views.decorators.csrf import csrf_exempt
import os
# WillowTree system imports

from MIS.ViewExtras import SetMenu, SetLogo
from MIS.modules.PupilRecs import *
from MIS.modules.ExtendedRecords import *
from MIS.modules.FamilyRecord import *
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules import GeneralFunctions
from MIS.modules.notes import NoteFunctions
from MIS.modules.Favourites import FavouritesFunctions
from MIS.modules.attendance import AttendanceFunctions
from MIS.modules.gradeInput import GradeFunctions
from MIS.modules import StaffRecs
from django.contrib.auth.models import User
from MIS.models import PupilAlert
from MIS.models import SystemVarData
from boto.s3.connection import S3Connection

def PageNotFound(request):
    ''' PageNotFound : Used to render the page not found '''
    c={'errmsg': 'Page Not found : %s' % request.META['PATH_INFO']}
    return render_to_response('Welcome.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


def send_file(request,FileName):
    """
    Send a file through Django without loading the whole file into
    memory at once. The FileWrapper will turn the file object into an
    iterator for chunks of 8KB.
    """
    filename ="%s%s" % (settings.MEDIA_ROOT,FileName)
    wrapper = FileWrapper(file(filename))
    if '.html' in FileName:
        response=HttpResponse(wrapper, content_type='text/html')
    else:
        response = HttpResponse(wrapper, content_type='text/plain')
    response['Content-Length'] = os.path.getsize(filename)
    return response

# # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#   Pupil Record Display
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # #
def pupilView(request,school,PupilNo,AcYear):
    ''' Creates the pupil record page.

    Simple View. Just obtains the Base Pupil Record and then obtains the Pupil
    Note for that record. The Template then decides where to place the data'''
    try:
        PupilDetails=PupilRecord(int(PupilNo),AcYear)
    except ValueError:
        raise Http404()
    if 'B' in PupilDetails.Form():
        schoolChildIsIn = 'BatterseaAdmin'
    elif 'C' in PupilDetails.Form():
        schoolChildIsIn = 'ClaphamAdmin'
    elif 'F' in PupilDetails.Form():
        schoolChildIsIn = 'FulhamAdmin'
    elif 'K' in PupilDetails.Form():
        schoolChildIsIn = 'KensingtonAdmin'
    else:
        schoolChildIsIn = school
    houses = GeneralFunctions.GetAcHousesInSchool(schoolChildIsIn)
    forms = GeneralFunctions.GetFormsInSchool(schoolChildIsIn)
    # getting a current group the pupil is in,
    # in turn getting current grade season...
    pupilGroups = PupilGroup.objects.filter(Pupil__id=PupilNo,
                                            Group__Name__icontains="Form",
                                            AcademicYear=AcYear,
                                            Active=True)
    if len(pupilGroups) > 0:
        GroupName = pupilGroups[0].Group
    else:
        GroupName =''
    gradeObj = GradeFunctions.SelectGradeReports(str(GroupName))
    yearRange = []
    for i in range(datetime.datetime.today().year-5,
                   datetime.datetime.today().year+20):
        if '3000' in str(i):
            yearRange.append('Error - see admin')
        else:
            yearRange.append(str(i))
    c = {'school': school, 'AcYear': AcYear, 'PupilDetails': PupilDetails,
         'houses': houses,
         'forms': forms,
         'KWList': NoteFunctions.KeywordList('Notes'),
         'gradeObj': gradeObj,
         'GroupName': GroupName,
         'currentGradeRecord': str(gradeObj.GetCurrentSeason()),
         'userEmailAddress': GeneralFunctions.getUserEmailAddress(request),
         'AttendanceCodes': AttendanceCode.objects.filter(),
         'yearRangeForFutureSchoolsProgress': yearRange,
         'groups': Group.objects.filter(Name__icontains=MenuTypes.objects.get(Name=school).SchoolId.Name).exclude(Name__icontains="Staff.").exclude(Name__icontains="Applicants."),
         'old_pupil_groups': PupilGroup.objects.filter(Pupil__id=PupilNo).exclude(AcademicYear=SystemVarData.objects.get(Variable__Name='CurrentYear').Data)}
    # test for more abled:
    if PupilAlert.objects.filter(PupilId__id=int(PupilNo),
                                 AlertType__AlertGroup__Name='More Abled',
                                 Active=True).exists():
        c.update({'moreAbled': 'True'})
    # religon list
    religionList = []
    for i in PupilDetails.Extended()['Religion'][2]:
        religionList.append(i[1])
    c.update({'religionList': religionList})
    # to avoid an error if a pupil was added to a school then the school was
    # deleted.
    try:
        pastSchool = PupilsNextSchool.objects.get(PupilId=int(PupilNo))
        c.update({'pastSchool': pastSchool})
    except:
        pass
    c.update({'pastSchools': SchoolList.objects.all().order_by('Name')})
    c.update(NoteFunctions.GetPupilNotes(request,Pupil_Id=PupilNo))
    c.update(csrf(request))
    c.update({'TypeofSchool': ['KinderGarten', 'Prep', 'Secondary']})
    groups = PupilGroup.objects.filter(Pupil__id=PupilNo,Group__Name='Applicants.%s.Main.Declined' % MenuTypes.objects.get(Name=school).SchoolId.Name,Active=True)
    if len(groups) > 0:
        c.update({'pupilDeclined':True,'pupilDeclinedText':ExtendedPupilRecordData['declinedReason'][0]})
    groups = PupilGroup.objects.filter(Pupil__id=PupilNo,Group__Name='Applicants.%s.Withdrawn' % MenuTypes.objects.get(Name=school).SchoolId.Name,Active=True)
    if len(groups) > 0:
        c.update({'pupilWithdrawn':True,'pupilWithdrawnText':ExtendedPupilRecordData['withdrawnReason'][0]})
    groups = PupilGroup.objects.filter(Pupil__id=PupilNo,Group__Name='Applicants.%s.Main.Declined.Place' % MenuTypes.objects.get(Name=school).SchoolId.Name,Active=True)
    if len(groups) > 0:
        c.update({'pupilDeclinedPlace':True,'pupilDeclinedPlaceText':ExtendedPupilRecordData['declinedPlaceReason'][0]})
    groups = PupilGroup.objects.filter(Pupil__id=PupilNo,Group__Name='Applicants.%s.Main.Declined.Interview' % MenuTypes.objects.get(Name=school).SchoolId.Name,Active=True)
    if len(groups) > 0:
        c.update({'pupilDeclinedInterview':True,'pupilDeclinedInterviewText':ExtendedPupilRecordData['declinedInterviewReason'][0]})
    groups = PupilGroup.objects.filter(Pupil__id=PupilNo,Group__Name='Applicants.%s.Main.Declined.AfterDeposit' % MenuTypes.objects.get(Name=school).SchoolId.Name,Active=True)
    if len(groups) > 0:
        c.update({'pupilDeclinedAfterDeposit':True,'pupilDeclinedAfterDepositText':ExtendedPupilRecordData['declinedAfterDepositReason'][0]})
    return render_to_response('PupilRecord.html',c,context_instance=RequestContext(request,processors=[SetMenu]))
# # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#   Staff Record Display
#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # #
def staffView(request, school, StaffNo, AcYear):
    ''' Creates the Staff record page.

    Simple View. Just obtains the Base Staff Record and then obtains the Staff
    Note for that record. The Template then decides where to place the data'''
    try:
        StaffDetails = StaffRecs.StaffRecord(int(StaffNo), request, AcYear)
    except ValueError:
        raise Http404()
    c = {'school': school, 'AcYear': AcYear, 'StaffDetails': StaffDetails}
    c.update(csrf(request))
    # groups staff can be added to from staff page - this will change as
    # per the school menu the admin user is currently in.
    schoolsList1 = ['Battersea', 'Clapham', 'Fulham', 'Kensington']
    thisSchool = MenuTypes.objects.get(Name=school).SchoolId.Name
    schoolsList = [thisSchool]
    for school in schoolsList1:
        if thisSchool != school:
            schoolsList.append(school)
    finalGroups = []
    for school in schoolsList:
        groups = Group.objects.filter(Name__icontains="Staff")
        groups = groups.filter(Name__icontains=school)
        staffGrps = []
        for i in StaffGroup.objects.filter(Staff__id=int(StaffNo)):
            staffGrps.append(i.Group.Name)
        tempGroups = []
        for i in groups:
            inGroup = False
            temp = i.Name
            temp2 = temp.split('.')
            if len(temp2) > 3 or '_Staff_Register' in temp:
                if i.Name in staffGrps:
                    inGroup = True
                tempGroups.append([i.Name, inGroup])
        finalGroups.append(tempGroups)
    c.update({'groups': finalGroups})
    # end
    try:
        if User.objects.get(staff__id=int(StaffNo)):
            c.update({'staffIsUser': 'True'})
    except:
        pass
    return render_to_response('StaffRecord.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


# # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  Family Record
#
# # # # # # # # # # # # # # # # # # # # # # # # # # #

def familyView(request,school,PupilNo,AcYear):
    FamilyRec=FamilyRecord(request,AcYear,PupilId=PupilNo)
    return HttpResponseRedirect('/WillowTree/%s/%s/EditFamily/%d/' % (school,AcYear,FamilyRec.FamilyId.id))


# # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Simple View to show a list of groups a child is
# Currently in
# # # # # # # # # # # # # # # # # # # # # # # # # # #

def ListGroups(request,school,AcYear,PupilNo):
    c={'ChangeList':GroupList}
    c.update(csrf(request))

    return render_to_response('SetUpdated.html',c,context_instance=RequestContext(request,processors=[SetMenu]))

# # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#   Login/Logout pages#
#
# # # # # # # # # # # # # # # # # # # # # # # # # # #

# Builds the login page
@csrf_exempt
def Login(request):
    ''' login page '''
    if  request.user.is_authenticated():
        AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
        StaffDetails=Staff.objects.get(User=request.user)
        school=StaffDetails.DefaultMenu.Name
        c={'school':school}
        return HttpResponseRedirect('/WillowTree/%s/%s/Welcome/' % (school,AcYear))
    else:
        c={'school':None}
        c.update(csrf(request))
        return render_to_response('Login.html',c,context_instance=RequestContext(request,processors=[SetLogo]))

#  # # # # # # # # # # # # # # # # #  # # # # #
#  Proccesses the  results frm the Login page
# # # # # # # # # # # # # # # # # # # # # # # #
@csrf_exempt
def Authenticate(request, school):
    ''' This view logs you onto the system '''
    c={'school': None}
    c.update(csrf(request))
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    try:
        StaffDetails = Staff.objects.get(User__username=username)
        if StaffDetails.Active == False:
            c.update({'errmsg': 'SORRY YOUR ACCOUNT IS LOCKED... PLEASE CONTACT SUPPORT'})
            c.update({'Locked': 'True'})
        else:
            if user is not None:
                if StaffDetails.Active:
                    login(request, user)
                    AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
                    StaffDetails=Staff.objects.get(User__username=username)
                    StaffDetails.FailedLoginAttempts = 0
                    StaffDetails.save()
                    request.session['StaffId']=StaffDetails
                    school=StaffDetails.DefaultMenu.Name
                    StaffGroups=StaffGroup.objects.filter(Staff=request.session['StaffId'],AcademicYear=AcYear)
                    StaffRoles=set()
                    for elements in StaffGroups:
                        StaffRoles.add(elements.Group)
                    request.session['StaffGroups']=StaffRoles
                    c['school']=school
                    return HttpResponseRedirect('/WillowTree/%s/%s/Welcome/' % (school,AcYear))
                else:
                    c.update({'errmsg':'UserId is disabled. Please contact Support'})
            else:
                c.update({'errmsg':'Invalid UserId or Password. Please Try again'})
                if Staff.objects.get(User__username=username):
                    StaffDetails=Staff.objects.get(User__username=username)
                    StaffDetails.FailedLoginAttempts += 1
                    StaffDetails.save()
                    if StaffDetails.FailedLoginAttempts > 4:
                        StaffDetails.Active = False
                        StaffDetails.save()
                        c.update({'errmsg':'5 Failed attempts to Login. Your UserId is now suspended. Contact Support'})
    except:
        c.update({'errmsg': 'INCORRECT USERNAME. PLEASE TRY AGAIN'})
    return render_to_response('Login.html',c,context_instance=RequestContext(request,processors=[SetLogo]))

@csrf_exempt
def Authenticate1(request,school):
    ''' This view logs you onto the system '''
    c={'school':None}
    c.update(csrf(request))
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(username=username, password=password)
    StaffDetails = Staff.objects.get(User__username=username)
    if StaffDetails.Active == False:
        c.update({'errmsg': 'SORRY YOUR ACCOUNT IS LOCKED... PLEASE CONTACT SUPPORT'})
        c.update({'Locked': 'True'})
    else:
        if user is not None:
            if StaffDetails.Active:
                login(request, user)
                AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
                StaffDetails=Staff.objects.get(User__username=username)
                StaffDetails.FailedLoginAttempts = 0
                StaffDetails.save()
                request.session['StaffId']=StaffDetails
                school=StaffDetails.DefaultMenu.Name
                StaffGroups=StaffGroup.objects.filter(Staff=request.session['StaffId'],AcademicYear=AcYear)
                StaffRoles=set()
                for elements in StaffGroups:
                    StaffRoles.add(elements.Group)
                request.session['StaffGroups']=StaffRoles
                c['school']=school
                return HttpResponseRedirect(request.META.get('HTTP_REFERER','/'))
            else:
                c.update({'errmsg':'UserId is disabled. Please contact Support'})
        else:
            c.update({'errmsg':'Invalid UserId or Password. Please Try again'})
            if Staff.objects.get(User__username=username):
                StaffDetails=Staff.objects.get(User__username=username)
                StaffDetails.FailedLoginAttempts += 1
                StaffDetails.save()
                if StaffDetails.FailedLoginAttempts > 4:
                    StaffDetails.Active = False
                    StaffDetails.save()
                    c.update({'errmsg':'5 Failed attempts to Login. Your UserId is now suspended. Contact Support'})
    return render_to_response('Login.html',c,context_instance=RequestContext(request,processors=[SetLogo]))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Proccesses a Logout request
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def Logout(request,school):
    logout(request)
    c={'school':school}
    c.update(csrf(request))
    c.update({'errmsg':'You Are Now Logged Out'})
    return render_to_response('Login.html',c,context_instance=RequestContext(request,processors=[SetLogo]))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Welcome page to display once authenticed and logged in
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def Welcome(request,school,AcYear):
    request.session.set_expiry(3600)
    attendanceClass = AttendanceFunctions.DailyAttendance.Get_ClassList(school)
    attendanceClass2 = []
    for i in attendanceClass:
        attendanceClass2.append(str(i.Name))
    c = {'school': school, 'AcYear': AcYear,
         'Favourites': FavouritesFunctions.StaffFavs(request)}
    c.update(csrf(request))
    c.update({'staffIdNumber': request.session['StaffId'].id})
    c.update({'attendanceClass': attendanceClass2})
    return render_to_response('Logged_In.html',
                              c,
                              context_instance=RequestContext(request,
                                                              processors=[SetMenu]))

# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
# Register a new User
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

def RegisterUser(request,school):
    c={'school':school}
    c.update(csrf(request))
    try:
        StaffDetails=Staff.objects.get(id=int(request.POST['NewStaffId']),Forename__iexact=request.POST['Forename'],Surname__iexact=request.POST['Surname'])
    except:
        c.update({'errmsg':'Sorry! You have entered an incorrect Staff details Please Try again'})
        return render_to_response('NewStaff.html',c,context_instance=RequestContext(request,processors=[SetLogo]))
    if not StaffDetails.User==None:
        c.update({'errmsg':'Sorry! This Staff Id has already been assigned a username and password. Please Contact System Admin for Help'})
        return render_to_response('Login.html',c,context_instance=RequestContext(request,processors=[SetLogo]))

    c.update({'StaffDetails':StaffDetails})
    request.session['StaffId']=StaffDetails
    return render_to_response('NewUser.html',c,context_instance=RequestContext(request,processors=[SetLogo]))

def CreateNewUser(request,school):
    c={'school':school}
    c.update(csrf(request))
    # test userid and password

    if not 'username' in request.POST:
        c.update({'errmsg':'No User Name specified'})
        return render_to_response('NewUser.html',c,context_instance=RequestContext(request,processors=[SetLogo]))
    if not 'password' in request.POST:
        c.update({'errmsg':'No passphrase Name specified'})
        return render_to_response('NewUser.html',c,context_instance=RequestContext(request,processors=[SetLogo]))
    if not 'password2' in request.POST:
        c.update({'errmsg':'No Repeat passphrase  specified'})
        return render_to_response('NewUser.html',c,context_instance=RequestContext(request,processors=[SetLogo]))

    if len(request.POST['username']) < 5:
        c.update({'errmsg':'Sorry Your username is too short'})
        return render_to_response('NewUser.html',c,context_instance=RequestContext(request,processors=[SetLogo]))
    if User.objects.filter(username=request.POST['username']).exists():
        c.update({'errmsg':'Sorry Your username has already been assgined to another Staff Memeber. Please Try another one'})
        return render_to_response('NewUser.html',c,context_instance=RequestContext(request,processors=[SetLogo]))

    if len(request.POST['password']) < 12:
        c.update({'errmsg':'Sorry Your Passphrase is too short'})
        return render_to_response('NewUser.html',c,context_instance=RequestContext(request,processors=[SetLogo]))
    if len(request.POST['password'].split(' ')) < 3:
        c.update({'errmsg':'Sorry, Not enough Words in your Passphrase'})
        return render_to_response('NewUser.html',c,context_instance=RequestContext(request,processors=[SetLogo]))
    PasswordError=False
    for words in request.POST['password'].split(' '):
        if len(words) < 3:
            PasswordError=True
    if PasswordError:
        c.update({'errmsg':'Sorry, Your Passphrase, words have to be at least 3 characters long'})
        return render_to_response('NewUser.html',c,context_instance=RequestContext(request,processors=[SetLogo]))
    if not request.POST['password']== request.POST['password2']:
        c.update({'errmsg':"Sorry, Your pass phrases don't match"})
        return render_to_response('NewUser.html',c,context_instance=RequestContext(request,processors=[SetLogo]))

    # Create Username and password

    NewUser=User.objects.create_user(request.POST['username'],request.session['StaffId'].EmailAddress,request.POST['password'])
    NewUser.save()
    NewUser.user_permissions.add(Permission.objects.get(codename=request.session['StaffId'].DefaultMenu.Name))
    NewUser.save()
    c.update({'errmsg':'New user successfully created. You can now Login'})
    request.session['StaffId'].User=NewUser
    request.session['StaffId'].save()
    return render_to_response('Login.html',c,context_instance=RequestContext(request,processors=[SetLogo]))




# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  PUPIL Search
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def PupilSearch(request, school, AcYear, transfer=False):
    # check to see if admin. If so this function will return all pupils from all schools otherwise only from menu school
    if 'Teacher' in school:
        school_name = school.split('Teacher')[0]
        part_email_address = "@%s." % school_name[0:3]

    c={'school':school,'AcYear':AcYear}
    err_msg=''
    if 'SearchText' in request.POST:
        searchText = request.POST['SearchText']
        if len(searchText) > 0:
            PupilList=Pupil.objects.filter(Active=True)
            for SearchWords in searchText.split():
                PupilList = PupilList.filter(Q(Forename__icontains=SearchWords) |
                                             Q(Surname__icontains=SearchWords) |
                                             Q(NickName__icontains=SearchWords) |
                                             Q(OtherNames__icontains=SearchWords) |
                                             Q(id__icontains=SearchWords))
            if "Teacher" in school:
                PupilList = PupilList.filter(EmailAddress__icontains=part_email_address)
            SearchResults=[]
            for Pupils in PupilList:
                SearchResults.append(PupilRecord(Pupils.id,AcYear))
            c.update({'SearchResults':SearchResults,'BannerTitle':'Pupil Search'})
            c.update(csrf(request))
            if transfer:
                c.update({'transfer': 'true'})
    else:
        err_msg='No String Found'
    c.update({'err_msg':err_msg})
    return render_to_response('PupilSearchResults.html',c,context_instance=RequestContext(request,processors=[SetMenu]))


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#  reset password
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
def resetStaffPassword(request, school, AcYear, StaffNo,
                       fromStaffRecord=False):
    ''' Resets a staff password to "password"'''
    if fromStaffRecord:
        staffRecord = Staff.objects.get(id=int(StaffNo))
        staffRecord.ResetPassword = 1
        staffRecord.FailedLoginAttempts = 0
        staffRecord.Active = True
        staffRecord.save()
        userObj = User.objects.get(staff__id=int(StaffNo))
        userObj.set_password('password')
        userObj.save()
        httpAddress = '/WillowTree/%s/%s/staff/%d/'
        tupleData = (school, AcYear, int(StaffNo))
        return HttpResponseRedirect(httpAddress % tupleData)

def getMediaFile(request):
    file_name=request.META['PATH_INFO'].replace('/getmedia/','/media/')
    s3 = S3Connection(settings.AWS_ACCESS_KEY_ID,
                      settings.AWS_SECRET_ACCESS_KEY,
                      is_secure=True)
    # Create a URL valid for 60 seconds.
    bucket=s3.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
    file_key=bucket.get_key(file_name)
    url=file_key.generate_url(120,query_auth=True)
    if url:
        return HttpResponseRedirect(url,content_type=file_key.content_type)
    else:
        return HttpResponseNotFound('Failed to get')