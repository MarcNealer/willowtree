from django.db import models
from django.contrib.auth.models import User, Permission
import datetime
from picklefield.fields import PickledObjectField
from django.db.models import Q
from django import template

#These are the choice tuples for the models. Place all choice tuples here, not in
# the model classes themselves

Gender_Choices = (
(u'M', u'Male'),
(u'F', u'Female')
)

Field_Choices = (
('Date', 'Date'),
('Time', 'Time'),
('Numerical', 'Numerical'),
('String', 'String'),
('Boolean', 'Boolean'),
('TextBox', 'Large Text Box or Comment')
)

Term_Choices = (
('Michaelmas', 'Michaelmas'),
('Lent', 'Lent'),
('Summer', 'Summer')
)

ShowInMenu_Choices = (
('T', 'Teacher'),
('A', 'Admin'),
)
AmPm_Choices = (
('AM', 'AM'),
('PM', 'PM')
)
AttendanceNotTaken_Choices = (
('AM', 'AM'),
('PM', 'PM'),
('AllDay', 'AllDay')
)
AttendanceCodeTypes_Choices = (
('Unauthorised', 'Unauthorised'),
('Authorised', 'Authorised'),
('Late', 'Late'),
('NotCounted', 'NotCounted'),
('Present', 'Present')
)
AttendanceAccess_Choices = (
('Teacher', 'Teacher'),
('Admin', 'Admin')
)

Attendance_Choice = (
('L', 'Lesson'),
('D', 'Daily'),
)

Base_Type_Choice = (
('Pupil', 'Pupil'),
('Staff', 'Staff'),
('Family', 'Family'),
('Contact', 'Contact'),
('Address', 'Address')
)
IssueObjects = (
('Pupil', 'Pupil'),
('Staff', 'Staff'),
('Family', 'Family'),
('Contact', 'Contact'),
('Address', 'Address'),
('Hardware', 'Hardware'),
('Software', 'Software')
)

IssueStatus_choice = (
('Open', 'Open'),
('Closed', 'Closed'),
('PendingClosure', 'PendingClosure'),
('WorkInProgress', 'WorkInProgress'),
('NeedInfo', 'NeedInfo')
)

RelType_Choices = (
('C', 'C'),
('A', 'A')
)
GroupType_Choices = (
('Grade', 'Grade'),
('Markbook', 'MarkBook'),
('External', 'External')
)
ClassReport_Type = (
('Analysis', 'Analysis'),
('Classlist', 'Classlist'),
('Letter', 'Letter'),
('Note', 'Note')
)
DaysOfTheWeek = (
('Monday', 'Monday'),
('Tuesday', 'Tuesday'),
('Wednesday', 'Wednesday'),
('Thursday', 'Thursday'),
('Friday', 'Friday'),
('Saturday', 'Saturday'),
('Sunday', 'Sunday')
)

User_Choices = (
('SysAdmin', 'SysAdmin'),
('Staff', 'Staff'),
('Family', 'Family'),
('Pupil', 'Pupil')
)

MenuItem_Choices = (
('Managed', 'managed'),
('Attendance', 'Attendance'),
('NameContains', 'NameContains')
)

KeywordType_choices = (
('Primary', 'Primary'),
('Secondary', 'Secondary'),
('Tertiary', 'Tertiary'),
('Subject', 'Subject')
)

ReportObject_Choices = (
('Pupil', 'Pupil'),
('Family', 'Family'),
('Staff', 'Staff'),
('PastSchool','PastSchool'),
('FutureSchool','FutureSchool'),
('School','School')
)

SchoolTypes_Choices = (
('KinderGarten', 'KinderGarten'),
('Prep', 'Prep'),
('Secondary', 'Secondary')
)

TargetType_Choices = (
('EAL', 'EAL'),
('SEN', 'SEN'),
('G/T', 'G/T'),
('Pastoral', 'Pastoral'),
('Other', 'Other')
)

SENType_Choices = (
('Class', 'Class'),
('Tuition', 'Tuition')
)

SystemVar_Choices = (
('System', 'System'),
('Yearly', 'Yearly'),
)

SystemVarData_Choices = (
('Date', 'Date'),
('Numerical', 'Numerical'),
('String', 'String'),
('Boolean', 'Boolean')
)

Process_of_admissions = (
('Parents interested', 'Parents interested'),
('Registered', 'Registered'),
('Unconditional place offered', 'Unconditional place offered'),
('Conditional place offered', 'Conditional place offered'),
('Reserve List', 'Reserve List'),
('Waiting List 1', 'Waiting List 1'),
('Waiting List 2', 'Waiting List 2'),
('Waiting List 3', 'Waiting List 3'),
('Withdrawn', 'Withdrawn'),
('Turned down', 'Turned down')
)

# Base record Tables


class School(models.Model):
    ''' These are school objects. They contain data on the school,
    the schools custom CSS file school banner, address and Head teacher'''
    Name = models.CharField(max_length=100)
    HeadTeacher = models.CharField(max_length=200)
    TemplateCSS = models.CharField(default='LetterCSS/LetterBasic.css',
                                   max_length=200)
    TemplateBanner = models.CharField(default='LetterCSS/LetterBanner.jpg',
                                      max_length=200)
    Address = models.ForeignKey('Address')
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.Name


class Family(models.Model):
    ''' This is the Family Object table .
    Contacts link to this table, but this table has a many to many relationship
    with the addresses '''
    FamilyName = models.CharField(max_length=100, unique=True)
    Address = models.ManyToManyField('Address')
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.FamilyName

    class Meta:
        ordering = ['FamilyName']


class BaseRecord(models.Model):
    ''' Abstract Class for the base people object records '''
    Forename = models.CharField(max_length=50)
    OtherNames = models.CharField(max_length=100, blank=True)
    Surname = models.CharField(max_length=100)
    NickName = models.CharField(max_length=50, blank=True)
    EmailAddress = models.EmailField(blank=True)
    Title = models.CharField(max_length=30, blank=True)
    DateOfBirth = models.DateField(default=datetime.datetime(1972, 1, 1))
    Gender = models.CharField(max_length=1, choices=Gender_Choices)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s %s' % (self.Forename, self.Surname)

    class Meta:
        ordering = ['Surname', 'Forename']
        abstract = True


class PupilManager(models.Manager):
    def get_by_natural_key(self, forename, surname):
        return self.get(forename=forename,
                        surname=surname)


class Pupil(BaseRecord):
    ''' This is the base Pupil Records table.
    Pupils are connected to families via the realtionship table and to groups
    by the groups table
    '''
    objects = PupilManager()
    Picture = models.ImageField(upload_to='media/images/StudentPhotos',
                                default='media/images/StudentPhotos/Student.png')
    Old_Id = models.CharField(max_length=40, blank=True)
    User = models.ForeignKey(User, unique=True, blank=True, null=True)
    RegistrationDate = models.DateField(default=datetime.date.today())

    def natural_key(self):
        return "%s %s" % (self.Forename, self.Surname)

    def FirstName(self):
        ''' FirstName will be either the childs Nickname, or if there is no
        nickname, the childs first name'''
        if len(self.NickName) > 0:
            return self.NickName
        else:
            return self.Forename

    def FullName(self):
        ''' returns a string of the childs full name, but will choose the
        childs nickname if present or the childs first name if not,
        plus their surname'''
        if len(self.NickName) > 0:
            FirstName = self.NickName
        else:
            FirstName = self.Forename
        return "%s %s" % (FirstName, self.Surname)

    def hisher(self):
        ''' Based upon gender, returns his or her.
        This is used for letter generation'''
        if self.Gender == 'M':
            return 'his'
        else:
            return 'her'

    def HisHer(self):
        ''' Based upon gender, returns His or Her.
        This is used for letter generation'''
        if self.Gender == 'M':
            return 'His'
        else:
            return 'Her'

    def heshe(self):
        ''' Based upon gender, returns he or she.
        This is used for letter generation'''
        if self.Gender == 'M':
            return 'he'
        else:
            return 'she'

    def HeShe(self):
        ''' Based upon gender, returns He or She.
        This is used for letter generation'''
        if self.Gender == 'M':
            return 'He'
        else:
            return 'She'

    def himher(self):
        ''' Based upon gender, returns He or She.
        This is used for letter generation'''
        if self.Gender == 'M':
            return 'him'
        else:
            return 'her'

    def DOB(self):
        ''' returns date of birth formated for Javascript to read'''
        return self.DateOfBirth.strftime('%d/%m/%Y')


class Contact(BaseRecord):
    '''This table holds the base details for all family contacts including
    parents, friends, doctors and other possible realtionships
    Primary address and work address are many to many relationships with
    the address table
    Sort order defaults to surname,forename'''
    Address = models.ForeignKey('Address', blank=True, null=True)
    FamilyId = models.ManyToManyField('Family', blank=True, null=True)
    Pupils = models.ManyToManyField('Pupil', blank=True, null=True)
    WorkAddress = models.ForeignKey('Address', related_name='WorkAddress',
                                    blank=True, null=True)
    Old_Id = models.CharField(max_length=40, blank=True)

    def FullName(self):
        if len(self.Title) > 0:
            FullName = "%s %s %s" % (self.Title, self.Forename, self.Surname)
        else:
            FullName =  "%s %s" % (self.Forename, self.Surname)
        return FullName


class StaffManager(models.Manager):
    def get_by_natural_key(self, fullname, staff_id):
        return self.get(fullname=fullname, staff_id=staff_id)


class Staff(BaseRecord):
    ''' These are the staff Records'''
    objects = StaffManager()
    Address = models.ForeignKey('Address', blank=True, null=True)
    Picture = models.ImageField(upload_to='images/StaffPhotos',
                                blank=True, null=True)
    Old_Id = models.CharField(max_length=40, blank=True)
    User = models.OneToOneField(User, blank=True, null=True)
    DefaultMenu = models.ForeignKey('MenuTypes', blank=True, null=True)
    FailedLoginAttempts = models.SmallIntegerField(default=0)
    ResetPassword = models.SmallIntegerField(default=0)

    def natural_key(self):
        return {'staff full name': self.FullName(),
                'staff id': self.id}

    class Meta:
        permissions = (
        ("BatterseaTeacher", "Access Battersea"),
        ("ClaphamTeacher", "Access Clapham"),
        ("KensingtonTeacher", "Access Kensington"),
        ("FulhamTeacher", "Access Fulham"),
        ("KindergartenTeacher", "Access Kindergarten"),
        ("BatterseaAdmin", "Access Admin Battersea"),
        ("ClaphamAdmin", "Access Admin Clapham"),
        ("KensingtonAdmin", "Access Admin Kensington"),
        ("FulhamAdmin", "Access Admin Fulham"),
        ("BatterseaApplicants", "Access Applicants Battersea"),
        ("ClaphamApplicants", "Access Applicants Clapham"),
        ("KensingtonApplicants", "Access Applicants Kensington"),
        ("FulhamApplicants", "Access Applicants Fulham"),
        ("KindergartenApplicants", "Access Applicants Kindergarten"),
        ("KindergartenAdmin", "Access Admin Kindergarten"),
        ('Family', 'Family'),
        ('Pupil', 'Pupil')
        )
        ordering = ['Surname']

    def FullName(self):
        return "%s %s" % (self.Forename, self.Surname)

    def PrintName(self):
        if len(self.Title) > 0:
            return "%s %s %s" % (self.Title,self.Forename[0],self.Surname)
        else:
            return "%s %s" % (self.Forename[0],self.Surname)

    def HeShe(self):
        if self.Gender=='M':
            return 'He'
        else:
            return 'She'

    def heshe(self):
        if self.Gender=='M':
            return 'he'
        else:
            return 'she'

    def HisHer(self):
        if self.Gender=='M':
            return 'His'
        else:
            return 'Her'

    def hisher(self):
        if self.Gender=='M':
            return 'his'
        else:
            return 'her'

    def GenerateLetterBody(self,LetterDetails,VarList,OtherItems,LetterType='Staff'):
        t=template.Template(LetterDetails.BodyText.replace('&quot;','"'))
        c=template.Context({'Staff':self,'Var':VarList})
        self.LetterBody=t.render(c)
        return

class StaffEmergencyContact(models.Model):
    '''Objects linking a staff member with an address record defining their
    emegency contacts. '''
    StaffId = models.ForeignKey('Staff')
    AddressId = models.ForeignKey('Address')
    Priority = models.SmallIntegerField()

    def __unicode__(self):
        return '%s %s (%d)' % (self.StaffId.Forename,
                                self.StaffId.Surname, self.Priority)


class Address(models.Model):
    ''' This table hold all the addresses for families, contacts and staff.

    All contact detail should be here, phones, addresses, postal titles etc
    are stored in these records
     '''
    HomeSalutation = models.CharField(max_length=100)
    PostalTitle = models.CharField(max_length=100)
    AddressLine1 = models.CharField(max_length=100)
    AddressLine2 = models.CharField(max_length=200)
    AddressLine3 = models.CharField(max_length=100, blank=True)
    AddressLine4 = models.CharField(max_length=100, blank=True)
    PostCode = models.CharField(max_length=100)
    Country = models.CharField(max_length=100, null=True, blank=True)
    Phone1 = models.CharField(max_length=150, blank=True)
    Phone2 = models.CharField(max_length=150, null=True, blank=True)
    Phone3 = models.CharField(max_length=150, null=True, blank=True)
    EmailAddress = models.EmailField(blank=True)
    Note = models.TextField(blank=True)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return u'%s , %s' % (self.PostalTitle, self.PostCode)

    def ListAddress(self):
        returnAddress=''
        if self.AddressLine1:
            returnAddress+=self.AddressLine1
        if self.AddressLine2:
            returnAddress+=', %s' % (self.AddressLine2)
        if self.AddressLine3:
            returnAddress+=', %s' % (self.AddressLine3)
        if self.AddressLine4:
            returnAddress+=', %s' % (self.AddressLine4)
        if self.PostCode:
            returnAddress+=', %s' % (self.PostCode)
        return returnAddress

 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
 # Linking tables for linking people to families                 #
 # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #


class FamilyContact(models.Model):
    ''' This is the Family Members or family relationships table'''
    FamilyId = models.ForeignKey('Family')
    Contact = models.ForeignKey('Contact')
    Relationship = models.ForeignKey('FamilyRelationship',
                                     limit_choices_to={'RelType': 'A'})
    Priority = models.SmallIntegerField(blank=True, null=True)
    RelationshipDesc = models.TextField(max_length=500, blank=True)
    SMS = models.BooleanField(default=False)
    EMAIL = models.BooleanField(default=False)

    def __unicode__(self):
        return '%s %s' % (self.FamilyId, self.Relationship)

    class Meta:
        ordering = ['FamilyId']


class PupilContact(models.Model):
    ''' This Table Holds records of where a child has different
    contacts from the rest of the family. The contacts attached
    to the pupil Must be a subset of the contacts assigned to the Family'''
    PupilId = models.ForeignKey('Pupil')
    FamilyContactId = models.ForeignKey('FamilyContact')

    def __unicode__(self):
        return '%s %s' % (self.PupilId, self.FamilyContactId.Relationship.Type)


class FamilyChildren(models.Model):
    ''' This is the Family Members or family relationships table'''
    FamilyId = models.ForeignKey('Family')
    Pupil = models.ForeignKey('Pupil')
    RelationshipDesc = models.TextField(max_length=500, blank=True)
    FamilyType = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return '%s %s' % (self.FamilyId, self.Pupil)

    class Meta:
        ordering = ['FamilyId']


class FamilyRelationship(models.Model):
    ''' This is a list of Relationship types to a Family Object
    For example Mother, Father, Son, daughter, aunt, Uncle, even
    no related members such a friend, neigbour, doctor etc'''
    Type = models.CharField(max_length=50)
    RelType = models.CharField(max_length=1, choices=RelType_Choices)

    def __unicode__(self):
        return self.Type


class SchoolManager(models.Manager):
    def get_by_natural_key(self, name):
        return self.get(name=name)


class SchoolList(models.Model):
    ''' This is a list of schools to be used for mailing letters
    or for connecting pupils to their previous or possible future schools'''
    objects = SchoolManager()
    Name = models.CharField(max_length=300)
    Type = models.CharField(max_length=50, choices=SchoolTypes_Choices)
    HeadTeacher = models.CharField(max_length=200)
    Address = models.ForeignKey('Address')
    Desc = models.TextField(blank=True)

    def natural_key(self):
        return self.Name


class PupilFutureSchoolProgress(models.Model):
    ''' This show pupils progress in their future/next school '''
    Pupil = models.ForeignKey('Pupil')
    School = models.ForeignKey('SchoolList')
    LastUpdatedBy = models.ForeignKey('Staff')
    Progress = models.CharField(max_length=30, choices=Process_of_admissions)
    EntryYear = models.CharField(max_length=10)
    Interview = models.DateTimeField(blank=True, null=True)
    Details = models.TextField(blank=True)
    Active = models.BooleanField(default=True)


class PupilsPastSchool(models.Model):
    ''' This links pupils to their previous school if there is one '''
    PupilId = models.ForeignKey('Pupil')
    SchoolId = models.ForeignKey('SchoolList')


class PupilsNextSchool(models.Model):
    ''' This links Pupils to the school the attended after leaving Thomas's '''
    PupilId = models.ForeignKey('Pupil')
    SchoolId = models.ForeignKey('SchoolList')


class Group(models.Model):
    ''' This is a list of all groups in the system. Each
    Group is linked to its parent group. The name of the group should
    reflect the group and its parents i.e current.Year1.1HE. The Menu
    name is the text to appear in the menu system.
    '''
    Name = models.CharField(max_length=500)
    MenuName = models.CharField(max_length=100)

    def __unicode__(self):
        return self.Name

    class Meta:
        ordering = ['Name']


class PupilGroup(models.Model):
    ''' PupilGroups is one of the most important tables in the system
    It lists which pupils are assgined to which groups.'''
    AcademicYear = models.CharField(max_length=50)
    Pupil = models.ForeignKey('Pupil')
    Group = models.ForeignKey('Group')
    DateAdded = models.DateField(default=datetime.date.today())
    DateInactivated = models.DateField(default=datetime.date.today())
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return '%s-%s' % (self.Pupil, self.Group)


class StaffGroup(models.Model):
    ''' PupilGroups is one of the most important tables in the system
    It lists which pupils are assgined to which groups.'''
    AcademicYear = models.CharField(max_length=50)
    Staff = models.ForeignKey('Staff')
    Group = models.ForeignKey('Group')

    def __unicode__(self):
        return '%s-%s' % (self.Staff, self.Group)

    class Meta:
        unique_together = ('AcademicYear', 'Staff', 'Group')


class StaffGroupFavourite(models.Model):
    ''' This class holds markers to Groups marked as their favourites by
    members of staff'''
    Staff = models.ForeignKey('Staff')
    Group = models.ForeignKey('Group')
    PoolName = models.CharField(max_length=50)


class RollOverRule(models.Model):
    ''' Stores the code riles for moving pupils from the
    current dataset into the new one'''
    NewGroup = models.ForeignKey('Group')
    OldGroup = models.ForeignKey('Group',
                                 related_name='RollOver_OldGroup',
                                 blank=True, null=True)
    ApplicantGroup = models.ForeignKey('Group',
                                       related_name='RollOver_AppGroup',
                                       blank=True, null=True)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.NewGroup.Name


class SubjectManager(models.Manager):
    def get_by_natural_key(self, id, Name, Desc):
        return self.get(id=id,
                        name=Name,
                        decription=Desc)


class Subject(models.Model):
    """
    Stores subject objects
    """
    objects = SubjectManager()
    Name = models.CharField(max_length=30)
    Desc = models.CharField(max_length=300)

    def __unicode__(self):
        return self.Name

    def natural_key(self):
        return (self.id,
                self.Name,
                self.Desc)

    class Meta:
        ordering = ['Name']


class ParentSubjects(models.Model):
    ''' This links subjects with parent subjects. for example Reading
    is a subject for english, Biology is a subsubject for Science etc'''
    Subject = models.ForeignKey('Subject')
    ParentSubject = models.ForeignKey('Subject', related_name='ParentSubject')

    def __unicode__(self):
        return '%s (%s)' % (self.ParentSubject.Name, self.Subject.Name)


class EventQueue(models.Model):
    ''' When selected actions occur in WillowTree, they may also issue
    an alert to the Event system. This adds a record to this table.
    Using Cron, this table is searched on a regular basis and if any
    Events that match items from the EventTrigger are found, then an email
    alert is issued. The record is removed once proccessed even id it
    doesn't trigger and alert. '''
    Priority = models.PositiveSmallIntegerField()
    Group = models.ForeignKey('Group')
    Type = models.CharField(max_length=10)
    Name = models.CharField(max_length=100)
    Details = models.TextField()
    TimeStamp = models.DateTimeField()


class EventTrigger(models.Model):
    '''Twined with the Event Queue. These are lists of event types
    that will trigger an action. The actions could be of many kinds
    from issuing an email, sending messages to the Portal, or
    performing other record changes such as adding a child to, or
    removing them from SEN and EAL lists'''
    Priority = models.PositiveIntegerField()
    Group = models.ForeignKey('Group')
    Type = models.CharField(max_length=10)
    Staff = models.ForeignKey('Group', related_name='EventStaffGroup')
    Action = models.CharField(max_length=200)

# Here are the BRE tables


class ExtentionData(models.Model):
    ''' Table to hold data for the Extention record system V2'''
    BaseType = models.CharField(max_length=10)
    BaseId = models.PositiveIntegerField()
    ExtentionRecord = models.ForeignKey('ExtentionRecords')
    Subject = models.ForeignKey('Subject')
    Data = PickledObjectField()
    UpdatedOn = models.DateTimeField(default=datetime.datetime.now())
    UpdatedBy = models.ForeignKey('Staff',
                                  related_name='ExtentionsUpdatedBy',
                                  null=True, blank=True)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s(%d)- %s(%s)" % (self.BaseType,self.BaseId,self.ExtentionRecord.Name,self.Subject.Name)


class ExtentionFieldsManager(models.Manager):
    def get_by_natural_key(self, id_number, name):
        return self.get(id=id_number,
                        name=name)


class ExtentionFields(models.Model):
    ''' Fields to be made available to the Extention Record in
    the the BRE system'''
    objects = ExtentionFieldsManager()
    Name = models.CharField(max_length=100)
    Type = models.CharField(max_length=30, choices=Field_Choices)
    PickList = models.ForeignKey('ExtentionPickList')

    def natural_key(self):
        return (self.id,
                self.Name)

    def __unicode__(self):
        return self.Name


class ExtentionRecords(models.Model):
    ''' This holds the Extention Record names and links to the
    fields used in each extention record'''
    Name = models.CharField(max_length=50)
    FullName = models.CharField(max_length=200)
    Fields = models.ManyToManyField('ExtentionFields')

    def __unicode__(self):
        return self.Name

    def FieldExists(self,FieldName):
        # return True or False
        return self.Fields.filter(Name=FieldName).exists()

    def GetPickListValue(self,FieldName,DataItem):
        PickList=eval(self.Fields.get(Name=FieldName).PickList.Data)
        AltData=False
        for items in PickList:
            if items[0]==DataItem:
                return items[1]
        return AltData

    def GetPickList(self,FieldName):
        return eval(self.Fields.get(Name=FieldName).PickList.Data)

    class Meta:
        ordering = ['Name']


class ExtentionPickList(models.Model):
    ''' This holds the Picklist data for the BRE fields. The data is held
    in a JSON format and will need to be converted for use'''
    Name = models.CharField(max_length=100)
    Data = models.TextField()

    def __unicode__(self):
        return self.Name

    def Evaluated(self):
        return eval(self.data)


class ExtentionForPupil(models.Model):
    ''' This is the relationship table between a pupil and the Extention
    Record. A pupil may only have one extention record of any given type, thus
    Pupilid-ExtentionRecordid is the primary key here'''
    PupilId = models.ForeignKey('Pupil')
    ExtentionRecordId = models.ForeignKey('ExtentionRecords')
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex',
                                         related_name='ExtentionIndexPupil')
    ExtentionSubject = models.CharField(max_length=100, blank=True, null=True)
    ExtentionUpdatedOn = models.DateTimeField(default=datetime.datetime.now())
    ExtentionUpdatedBy = models.ForeignKey('Staff',
                                related_name='ExtentionsPupilUpdatedBy',
                                null=True, blank=True)

    def __unicode__(self):
        return "%s %s" % (self.PupilId.Surname, self.ExtentionRecordId.Name)

    class Meta:
        unique_together = ('PupilId', 'ExtentionRecordId', 'ExtentionSubject')


class ExtentionForStaff(models.Model):
    ''' This is the relationship table between a Staff and the Extention
    Record. A pupil may only have one extention record of any given type, thus
    Staffid-ExtentionRecordid is the primary key here'''
    StaffId = models.ForeignKey('Staff')
    ExtentionRecordId = models.ForeignKey('ExtentionRecords')
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex',
                                         related_name='ExtentionIndexStaff',
                                         null=True, blank=True)
    ExtentionSubject = models.CharField(max_length=100, blank=True, null=True)
    ExtentionUpdatedOn = models.DateTimeField(default=datetime.datetime.now())
    ExtentionUpdatedBy = models.ForeignKey('Staff',
                                related_name='ExtentionStaffUpdatedBy',
                                null=True, blank=True)

    def __unicode__(self):
        return "%s %s" % (self.StaffId.Surname, self.ExtentionRecordId.Name)

    class Meta:
        unique_together = ('StaffId', 'ExtentionRecordId', 'ExtentionSubject')


class ExtentionForFamily(models.Model):
    ''' This is the relationship table between a Family and the Extention
    Record. A Family may only have one extention record of any given type, thus
    FamilyId-ExtentionRecordid is the primary key here'''
    FamilyId = models.ForeignKey('Family')
    ExtentionRecordId = models.ForeignKey('ExtentionRecords')
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex',
                                related_name='ExtentionIndexFamily')
    ExtentionSubject = models.CharField(max_length=100,
                                        blank=True, null=True)
    ExtentionUpdatedOn = models.DateTimeField(default=datetime.datetime.now())
    ExtentionUpdatedBy = models.ForeignKey('Staff',
                                related_name='ExtentionsFamilyUpdatedBy',
                                null=True, blank=True)

    def __unicode__(self):
        return "%s %s" % (self.Family.Name, self.ExtentionRecordId.Name)

    class Meta:
        unique_together = ('FamilyId', 'ExtentionRecordId', 'ExtentionSubject')


class ExtentionForContact(models.Model):
    ''' This is the relationship table between a Contact and the Extention
    Record. A Contact may only have one extention record of any given type,
    thus Contact-ExtentionRecordid is the primary key here'''
    ContactId = models.ForeignKey('Contact')
    ExtentionRecordId = models.ForeignKey('ExtentionRecords')
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex',
                            related_name='ExtentionIndexContact')
    ExtentionSubject = models.CharField(max_length=100,
                                        blank=True, null=True)
    ExtentionUpdatedOn = models.DateTimeField(default=datetime.datetime.now())
    ExtentionUpdatedBy = models.ForeignKey('Staff',
                                related_name='ExtentionsContactsUpdatedBy',
                                null=True, blank=True)

    def __unicode__(self):
        return " %s %s %s" % (self.ContactId.Forename,self.ContactId.Surname, self.ExtentionRecordId.Name)

    class Meta:
        unique_together = (('ContactId', 'ExtentionRecordId','ExtentionSubject'),)


class ExtentionForSchool(models.Model):
    ''' This is the relationship table between a Contact and the Extention
    Record. A Contact may only have one extention record of any given type,
    thus Contact-ExtentionRecordid is the primary key here'''
    SchoolId = models.ForeignKey('School', related_name='school_extention')
    ExtentionRecordId = models.ForeignKey('ExtentionRecords')
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex',
                            related_name='ExtentionIndexSchool')
    ExtentionSubject = models.CharField(max_length=100,
                                        blank=True, null=True)
    ExtentionUpdatedOn = models.DateTimeField(default=datetime.datetime.now())
    ExtentionUpdatedBy = models.ForeignKey('Staff',
                                related_name='ExtentionsSchoolsUpdatedBy',
                                null=True, blank=True)

    def __unicode__(self):
        return "%s %s" % (self.SchoolId.Surname, self.ExtentionRecordId.Name)

    class Meta:
        unique_together = (('SchoolId', 'ExtentionRecordId', 'ExtentionSubject'),)


class ExtentionsIndex(models.Model):
    ''' This table is used as a sort of buffer between the base record
    tables and the data tables. Thus pupils, contacts and families can
    link one way to this table and the data tables can link the other.
    If this is not here, then the data tables could only link to one of
    the base type tables'''
    ExtentionRecordId = models.ForeignKey('ExtentionRecords')

#  Extention records data tables


class ExtentionDate(models.Model):
    ''' This table holds date and time stamp data for the extention records'''
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex')
    FieldId = models.ForeignKey('ExtentionFields')
    Data = models.DateField()
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s %s" % (self.ExtentionIndexId.ExtentionRecordId.Name,
                          self.FieldId.Name)


class ExtentionTime(models.Model):
    ''' This table holds date and time stamp data for the extention records'''
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex')
    FieldId = models.ForeignKey('ExtentionFields')
    Data = models.TimeField()
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s %s" % (self.ExtentionIndexId.ExtentionRecordId.Name,
                          self.FieldId.Name)


class ExtentionNumerical(models.Model):
    ''' This table holds date and time stamp data for the extention records'''
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex')
    FieldId = models.ForeignKey('ExtentionFields')
    Data = models.FloatField()
    AlternativeValue = models.CharField(max_length=300)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s %s" % (self.ExtentionIndexId.ExtentionRecordId.Name,
                          self.FieldId.Name)


class ExtentionString(models.Model):
    ''' This table holds date and time stamp data for the extention records'''
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex')
    FieldId = models.ForeignKey('ExtentionFields')
    Data = models.CharField(max_length=300)
    AlternativeValue = models.CharField(max_length=300)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s %s" % (self.ExtentionIndexId.ExtentionRecordId.Name,
                          self.FieldId.Name)


class ExtentionTextBox(models.Model):
    ''' This table holds date and time stamp data for the extention records'''
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex')
    FieldId = models.ForeignKey('ExtentionFields')
    Data = models.TextField()
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s %s" % (self.ExtentionIndexId.ExtentionRecordId.Name,
                          self.FieldId.Name)


class ExtentionBoolean(models.Model):
    ''' This table holds date and time stamp data for the extention records'''
    ExtentionIndexId = models.ForeignKey('ExtentionsIndex')
    FieldId = models.ForeignKey('ExtentionFields')
    Data = models.BooleanField(default=True)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s %s" % (self.ExtentionIndexId.ExtentionRecordId.Name,
                          self.FieldId.Name)

# # # # # # # # # # # # # # # # # # # # # # # #
#
#Document Storage
#
# # # # # # # # # # # # # # # # # # # # # # # #


class Document(models.Model):
    ''' This class is used to link objects stored on the server to a pupil
    Record'''
    DocumentKeywords = models.CharField(max_length=300)
    DocumentName = models.CharField(max_length=300)
    Filename = models.FileField(upload_to='DocumentStorage',
                                max_length=300)
    UploadDate = models.DateField(default=datetime.date.today())
    UploadStaff = models.ForeignKey('Staff')

    def __unicode__(self):
        return self.DocumentName


class PupilDocument(models.Model):
    ''' This class links pupils with their attached documents in the document
    storage model'''
    PupilId = models.ForeignKey('Pupil')
    DocumentId = models.ForeignKey('Document')

    def __unicode__(self):
            return "%s - %s" % (self.PupilId.Surname,
                                self.DocumentId.DocumentName)


class FamilyDocument(models.Model):
    ''' This class links Families with their attached documents in the document
    storage model'''
    FamilyId = models.ForeignKey('Family')
    DocumentId = models.ForeignKey('Document')

    def __unicode__(self):
            return "%s - %s" % (self.FamilyId.Name,
                                self.DocumentId.DocumentName)


class StaffDocument(models.Model):
    ''' This class links Staff with their attached documents in the document
    storage model'''
    StaffId = models.ForeignKey('Staff')
    DocumentId = models.ForeignKey('Document')

    def __unicode__(self):
            return "%s - %s" % (self.StaffId.Name,
                                self.DocumentId.DocumentName)


class SchoolDocument(models.Model):
    ''' This class links Schools with their attached documents in the document
    storage model'''
    SchoolId = models.ForeignKey('School', related_name='school-docs')
    DocumentId = models.ForeignKey('Document')

    def __unicode__(self):
            return "%s - %s" % (self.SchoolId.Name,
                                self.DocumentId.DocumentName)

# # # # # # # # # # # # # # # # # # # # # # # # # #
#
#       Attendance
#
# # # # # # # # # # # # # # # # # # # # # # # # # #


class PupilAttendance(models.Model):
    '''Stores records of when a pupil has been marked absent from school'''
    Pupil = models.ForeignKey('Pupil')
    AttendanceDate = models.DateField()
    AttendanceType = models.CharField(max_length=1,
                                      choices=Attendance_Choice)
    Group = models.ForeignKey('Group')
    AmPm = models.CharField(max_length=2,
                             choices=AmPm_Choices, blank=True)
    Teacher = models.ForeignKey('Staff')
    RecordedDate = models.DateTimeField(default=datetime.datetime.now())
    Code = models.ForeignKey('AttendanceCode')
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return '%s(%s) %s' % (self.Pupil, self.AttendanceDate, self.Code)

    class Meta:
        ordering = ['AttendanceDate', 'Pupil']


class AttendanceCode(models.Model):
    ''' A list of codes to be used for pupil absences. Attendance access
    defines codes that the teacher can use, and ones reserved for Admin.'''
    AttendanceCode = models.CharField(max_length=1)
    AttendanceDesc = models.CharField(max_length=200)
    AttendanceAccess = models.CharField(max_length=15,
                                      choices=AttendanceAccess_Choices)
    AttendanceCodeType = models.CharField(max_length=20,
                                        choices=AttendanceCodeTypes_Choices)
    AttendanceStatistical = models.BooleanField(default=True)

    def __unicode__(self):
        return self.AttendanceDesc

    class Meta:
        ordering = ['AttendanceCode']


class AttendanceTaken(models.Model):
    '''Keeps records at a group levels for when a class attendance was taken
    and by whom'''
    AttendanceDate = models.DateField()
    AttendanceType = models.CharField(max_length=1, choices=Attendance_Choice)
    Group = models.ForeignKey('Group')
    AmPm = models.CharField(max_length=2, choices=AmPm_Choices, blank=True)
    Teacher = models.ForeignKey('Staff')
    RecordedDate = models.DateTimeField(default=datetime.datetime.now())


class AttendanceNotTakenCode(models.Model):
    ''' A list of codes to be used for records recording why attendance was not
    taken in selected periods'''
    Code = models.CharField(max_length=1, unique=True)
    Desc = models.TextField()

    def __unicode__(self):
        return self.Code


class AttendanceNotTaken(models.Model):
    ''' Records giving reasons why attendance was not taken during a selected
    AP/PM period '''
    Code = models.ForeignKey('AttendanceNotTakenCode')
    Date = models.DateField()
    Type = models.CharField(max_length=7, choices=AttendanceNotTaken_Choices)
    Details = models.TextField()
    Group = models.ForeignKey('Group', null=True, blank=True)
    AcademicYear = models.CharField(max_length=50)

    def __unicode__(self):
        return '%s (%s)' % (str(self.Date),self.Code.Code)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#     **  MENUS **
#  Menus are a very important part of WillowTree.
# The following tables are used to
# define what items go into the menus in what order,
# plus what fuctions are available
# against each of the group items in the menus
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

class MenuTypes(models.Model):
    ''' Used to Store the different menu types, such as teacher, admin etc
    Name : Name of MenuType
    Desc: text field descriptor
    MenuLevel: Sets access level for this menu.
                It will display any Menu Items with a higher number.
    Example if MenuLevel is 5, then it will show any items
                            that have a level of 5 or higher
    Logo: Image field for the page logo'''
    Name = models.CharField(max_length=50)
    Desc = models.TextField(blank=True)
    SchoolId = models.ForeignKey('School', related_name='schoolMenu')
    MenuLevel = models.PositiveSmallIntegerField()
    Logo = models.CharField(max_length=200)

    def __unicode__(self):
        return self.Name

    class Meta:
        ordering = ['Name']


class Menus(models.Model):
    ''' This tables holds details of what items are in each menu
    MenuType allows for multiple menus to be stored
    Order states the order of the items in that menu
    Group defines the Group assigned to this menu item
    Attendance states if attendance is requires, not req, or not allowed
    Analysis, Grade state if these actions are to be given for this item
    ClassList states is the list of pupils in the group is to be displayed in
    the menu Manage states if the option to allow management of subgroups is
    allowed MenagedGroups states to subgroups to be managed '''

    MenuType = models.ForeignKey('MenuTypes')
    Order = models.CharField(max_length=50)
    Group = models.ForeignKey('Group')
    MenuActions = models.ManyToManyField('MenuActions', blank=True, null=True)
    SubMenus = models.ManyToManyField('MenuSubMenu', blank=True, null=True)
    Manage = models.BooleanField(default=False)
    ManagedGroups = models.ManyToManyField('Group',
                                           related_name='SubGroups',
                                           blank=True, null=True)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s Item(%s) Group(%s)" % (self.MenuType, self.Order,
                                            self.Group)

    class Meta:
        ordering = ['MenuType', 'Order']
        unique_together = ('MenuType', 'Order')


class MenuSubMenu(models.Model):
    ''' This class contains all the submenu Groups such as Class lists
    reports, pupil reports, analysis reports etc '''
    Name = models.CharField(max_length=20, unique=True)
    MenuName = models.CharField(max_length=40)
    MenuOrder = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return self.Name


class MenuActions(models.Model):
    ''' MenuActions contain a list of items to be added
    to a selected Menu record'''
    Name = models.CharField(max_length=50)
    MenuName = models.CharField(max_length=50)
    MenuOrder = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return self.Name

    class meta:
        ordering = ['MenuOrder', 'MenuName']


class MenuItems(models.Model):
    '''This class holds details on various report objects that will be
    available in the menu. For example it holds a list of all the Different
    class lists avaiable The items here simply hold a menu name for the item,
    plus the django URL that activates the item'''
    Name = models.CharField(max_length=100)
    ItemURL = models.CharField(max_length=300)

    def __unicode__(self):
        return self.Name


class GroupMenuItems(models.Model):
    ''' This class says what MenuItem objects are available to what group in
    the menus. Any item assigned to a group is also assigned to its subgroups
    as well'''
    Type = models.ForeignKey('MenuSubMenu')
    Group = models.ForeignKey('Group')
    MenuItem = models.ForeignKey('MenuItems')
    GroupRequirements = models.CharField(max_length=12,
                                         choices=MenuItem_Choices,
                                         blank=True)
    ReqParms = models.CharField(max_length=100, blank=True)
    MenuLevel = models.PositiveSmallIntegerField()

    def __unicode__(self):
        return '%s-%s-%s' % (self.Type, self.Group, self.MenuItem)


class GradeInputElement(models.Model):
    ''' This class is used to store which HTML input form Snipits
    are used to enter grades for a set subject and group.
    The group column here is for a partial group name. A child will
    only be in one of these groups, for example Year611+, Year5 etc'''
    GradeSubject = models.ForeignKey('Subject')
    GradeRecord = models.ForeignKey('ExtentionRecords')
    PrevGradeRecord = models.ForeignKey('ExtentionRecords',
                                        null=True,
                                        blank=True,
                                        related_name='PrevGradeRecord')
    InputForm = models.CharField(max_length=200)
    DisplayOrder = models.SmallIntegerField(default=1)

    def __unicode__(self):
        return '%s (%s)' % (self.GradeRecord.Name, self.GradeSubject.Name)

    class Meta:
        ordering = ['GradeRecord', 'DisplayOrder']


class GradeInputFields(models.Model):
    '''
    This class is used to store the list of input fields required for key pairs
    of subject and Grade Record
    Example Form for Yr2_Eot_M has Form comment, and four targets
    FieldList is a pickled field that will contain a list of dictionaries
    in the form
    [{'FieldName':'subject:field','FieldType':'fieldtype'},{'FieldName':'subject:field','FieldType':'fieldtype'}]
    '''
    GradeSubject = models.CharField(max_length=100, default="default")
    GradeRecord = models.ForeignKey('ExtentionRecords')
    School = models.CharField(max_length=20, default="default")
    FieldList = PickledObjectField()

    def __unicode__(self):
        return '%s (%s)' % (self.GradeRecord.Name, self.GradeSubject)


class PupilSnapshot(models.Model):
    ''' This class will store a snapshot of all non constant elements
    of a pupils records that may be required for reports etc in the
    future, or for reproducing reports '''
    PupilRecord = models.ForeignKey('Pupil')
    GradeRecord = models.ForeignKey('ExtentionRecords')
    Snapshot = PickledObjectField()
    TimeStamp = models.DateTimeField(default=datetime.datetime.now())

    def __unicode__(self):
        return '%s %s (%s)' % (self.PupilRecord.Forname,
                                self.PupilRecord.Surnmae,
                                self.GradeRecord.Name)


class ReportingSeason(models.Model):
    ''' This class holds details on when various reports are due and for
    what Groups for example Yr5_Eot_M is due for all Year 5 Pupils and
    active between October and December.
    GradeStartDay and GradeStopDay is based upon Days this year
    '''
    GradeRecord = models.ForeignKey('ExtentionRecords')
    GradeGroup = models.ForeignKey('Group')
    GradeStartDay = models.IntegerField()
    GradeStopDay = models.IntegerField()

    def __unicode__(self):
        return '%s (%d-%d)' % (self.GradeRecord.Name,
                                self.GradeStartDay,
                                self.GradeStopDay)


# # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
#  Pupil Alert system
#
# # # # # # # # # # # # # # # # # #


class PupilAlert(models.Model):
    ''' This is part of the Alert system.
    Records on what flags are set for each pupil are kept in this table
    The types of Alert are stored in the AlertType table

    Fields:
        PupilId  -- Pupil Id from Pupil
        Type    --  Flag type from FlagType
        StartDate   -- When to start showing the Alert. Set to Past for
        now (optional)
        StopDate -- Whent to stop showing the Alert(optional)
        Active  -- Active Y/N
        TextDisplay -- Text to display when the flag is moused over '''
    PupilId = models.ForeignKey('Pupil')

    StartDate = models.DateField()
    StopDate = models.DateField(null=True, blank=True)
    AlertType = models.ForeignKey('AlertType')
    AlertDetails = models.TextField()
    AlertClosedDetails = models.TextField(blank=True)
    Active = models.BooleanField(default=True)
    RaisedBy = models.ForeignKey('Staff')
    ClosedBy = models.ForeignKey('Staff',
                                 related_name='AlertCloseStaffId',
                                 blank=True, null=True)
    UpdateDate = models.DateTimeField(default=datetime.datetime.now())

    def __unicode__(self):
        return '%s-%s' % (self.PupilId, self.AlertType)


class AlertType(models.Model):
    ''' These are the types of Alerts available to be set for a pupil.
    Fields:
        Name        - Name of the Alert (must be unique)
        Desc        - A quick description of what the Alert is for
        Icon        - the icon image to be used
        Priority    - This sets a priority on a flag. All Alerts with
                    priorites are show before the ones with none
                        '''
    AlertGroup = models.ForeignKey('AlertGroup')
    Name = models.CharField(max_length=100, unique=True)
    Desc = models.TextField()
    Icon = models.ImageField(upload_to='images/AlertIcons', blank=True)

    def __unicode__(self):
        return self.Name


class AlertGroup(models.Model):
    ''' This is a name for a group of related alerts. Only one alert in any
    group may be active for any give child'''
    Name = models.CharField(max_length=100, unique=True)
    DisplayOrder = models.IntegerField(max_length=2, blank=True)
    PermissionGroups = models.ManyToManyField('Group',
                        limit_choices_to={'Name__contains': 'Staff'},
                        null=True)

    def __unicode__(self):
        return self.Name


# # # # # # # # # # # # # # # # # # # # #
#                                       #
#       Notes system                    #
#                                       #
# # # # # # # # # # # # # # # # # # # # #
# These tables hold details on Notes raised against selected base records
# These will include items such as meeting notes, Acheivements etc
class NoteDetailsManager(models.Manager):
    def get_by_natural_key(self,
                           keywords,
                           note_text,
                           is_an_issue,
                           raised_by,
                           raised_date,
                           modified_by,
                           modified_date):
        return self.get(keywords=keywords,
                        note_text=note_text,
                        is_an_issue=is_an_issue,
                        raised_by=raised_by,
                        raised_date=raised_date,
                        modified_by=modified_by,
                        modified_date=modified_date)


class NoteDetails(models.Model):
    ''' This table holds details of the actual Note. If a note is modified
    the old version is saved as an inactive note '''
    objects = NoteDetailsManager()
    Keywords = models.CharField(max_length=300)
    NoteText = models.TextField()
    HousePoints = models.SmallIntegerField(null=True, blank=True)
    IsAnIssue = models.BooleanField(default=False)
    IssueStatus = models.CharField(max_length=20, choices=IssueStatus_choice)
    HousePoints = models.SmallIntegerField(default=0)
    HousePointsType = models.CharField(max_length=2,
                                       default='NA',
                                       choices=(('O', 'O'), ('S', 'S'),
                                                ('W', 'W'), ('NA', 'NA')))
    RaisedBy = models.ForeignKey('Staff', related_name='NoteRaisedBy')
    RaisedDate = models.DateTimeField()
    ModifiedBy = models.ForeignKey('Staff', related_name='NoteModifiedBy')
    ModifiedDate = models.DateTimeField()
    Active = models.BooleanField(default=True)

    def natural_key(self):
        return {'keywords': self.Keywords,
                'note_text': self.NoteText,
                'is_an_issue': self.IsAnIssue,
                'raised_by_staff_member': self.RaisedBy.FullName(),
                'raised_by_date': self.RaisedDate.date().isoformat(),
                'modified_by_staff_member': self.ModifiedBy.FullName(),
                'modified_by_date': self.ModifiedDate.date().isoformat()}

    def __unicode__(self):
        return self.Keywords

    def Keywords_as_list(self):
        return self.Keywords.split('\n')[0].split(':')


class NoteNotification(models.Model):
    ''' This table holds details of who if anybody is to be notified when
    a selected Note is raised. Its based upon matching against the keywords'''
    Keywords = models.CharField(max_length=200)
    Pupilgroup = models.ForeignKey('Group',
                                   related_name='PupilGroupNotification',
                        limit_choices_to={'Name__icontains': 'Current'})
    NotificationGroup = models.ForeignKey('Group',
                            limit_choices_to={'Name__icontains': 'Staff'})


class NoteToIssue(models.Model):
    ''' This table holds details of who if anybody is to be notified when
    a selected Note is raised. Its based upon matching against the keywords'''
    Keywords = models.CharField(max_length=200)
    Pupilgroup = models.ForeignKey('Group',
                        limit_choices_to={'Name__icontains': 'Current'})
    NotificationGroup = models.ForeignKey('Group',
                            related_name='IssueStaffLink',
                            limit_choices_to={'Name__icontains': 'Staff'})


class NoteKeywords(models.Model):
    ''' This class holds details of the keywords used and assigned to a note.
    Keywords come in two forms Primary and secondary. Each note must contain
    at least one Primary and one secondary Keyword. Each primary keyword has
    a set of seconday keywords where one of them must be selected to go'''
    Keyword = models.CharField(max_length=100)
    KeywordType = models.CharField(max_length=30, choices=KeywordType_choices)
    KeywordPool = models.CharField(max_length=30)
    HousePointsTrigger = models.BooleanField(default=False)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.Keyword


class PupilNotes(models.Model):
    ''' Pupil-Note relationship table'''
    PupilId = models.ForeignKey('Pupil')
    NoteId = models.ForeignKey('NoteDetails')
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s %s" % (self.PupilId.Forename, self.PupilId.Surname)


class StaffNotes(models.Model):
    ''' Staff Notes relationship table '''
    StaffId = models.ForeignKey('Staff')
    NoteId = models.ForeignKey('NoteDetails')
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.StaffId


class FamilyNotes(models.Model):
    ''' Family- Note relationship table'''
    FamilyId = models.ForeignKey('Family')
    NoteId = models.ForeignKey('NoteDetails')
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.FamilyId


class ContactNotes(models.Model):
    ''' Notes connected to a contact'''
    ContactId = models.ForeignKey('Contact')
    NoteId = models.ForeignKey('NoteDetails')
    Active = models.BooleanField(default=True)


# # # # # # # # # # # # # # # # # # #
#                                   #
#   Letters and Labels              #
#                                   #
# # # # # # # # # # # # # # # # # # #

class LetterBody(models.Model):
    '''' WillowTree Letters body text sections '''
    AttachedTo = models.ForeignKey('Group', blank=True, null=True)
    Name = models.CharField(max_length=200)
    Description = models.TextField(default=''''Please enter
    text to help users use this letter''')
    BodyText = models.TextField()
    SignatureName = models.CharField(max_length=150)
    SignatureTitle = models.CharField(max_length=150, blank=True, null=True)
    BodyVariables = models.TextField(blank=True)
    LetterTemplate = models.ForeignKey('LetterTemplate')
    ObjectBaseType = models.CharField(max_length=50,
                                      choices=ReportObject_Choices)
    ExtraStaffAccess = models.ForeignKey('Group',
                    limit_choices_to={'Name__icontains': 'Staff'},
                    blank=True, null=True, related_name='LetterStaffAccess')
    CreatedBy = models.ForeignKey('Staff')
    CreatedOn = models.DateTimeField(default=datetime.datetime.now())
    ModifiedBY = models.ForeignKey('Staff', related_name='letter_Modified_by')
    ModifiedOn = models.DateTimeField(default=datetime.datetime.now())
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.Name


class LetterTemplate(models.Model):
    ''' These are the base templates that will be used along with
    the body text to produce letters'''
    Name = models.CharField(max_length=100)
    Filename = models.CharField(max_length=200)
    LetterBaseType = models.CharField(max_length=50,
                                      choices=ReportObject_Choices)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return self.Name


class PupilTarget(models.Model):
    '''these are target objects for pupils. Each target holds details on the
    type of target, details of the target, stratergies to achieve the target
    and reviews of the target and dates of review'''
    Pupil = models.ForeignKey('Pupil')
    Type = models.CharField(max_length=20, choices=TargetType_Choices)
    Target = models.TextField()
    Review = models.TextField()
    Progress = models.CharField(max_length=20)
    ReviewDate = models.DateField(default=datetime.datetime.now)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s %s" % (self.Pupil.FullName, self.Type)


class PupilProvision(models.Model):
    '''These objects hold details of any special provisions made for a pupil.
    The data held here include details of the provision, they type, start and
    stop dates plus links to the staff assgined to the provision'''
    Pupil = models.ForeignKey('Pupil')
    Type = models.CharField(max_length=20, choices=TargetType_Choices)
    SENType = models.CharField(max_length=20,
                               blank=True,
                               null=True,
                               choices=SENType_Choices)
    Details = models.TextField()
    StartDate = models.DateField(default=datetime.datetime.now)
    StopDate = models.DateField(default=datetime.datetime.now)
    Active = models.BooleanField(default=True)

    def __unicode__(self):
        return "%s %s" % (self.Pupil.FullName, self.Type)


class PupilGiftedTalented(models.Model):
    ''' These records mark a child as gifted and Talented in selected
    subjects '''
    Pupil = models.ForeignKey('Pupil')
    Subject = models.ForeignKey('Subject')
    Details = models.TextField()
    Mentor = models.TextField(null=True, blank=True)
    Level = models.CharField(max_length=1)
    DateIdentified = models.DateField(null=True, blank=True)
    DateNotGiftedAndTalented = models.DateField(null=True, blank=True)
    DateAdded = models.DateField(default=datetime.datetime.now)
    DateClosed = models.DateField(default=datetime.datetime.now)
    ReasonsForRemovalGiftedAndTalented = models.TextField(null=True, blank=True)
    Active = models.BooleanField(default=True)


class SystemVariable(models.Model):
    '''these are system variable objects. They define system based variables.
    There can be two main types, system and Yearly. system can only exist
    once in the system. yearly variables exists for every given
    academic year'''
    Name = models.CharField(max_length=200)
    VariableType = models.CharField(max_length=30, choices=SystemVar_Choices)
    DataType = models.CharField(max_length=30, choices=SystemVarData_Choices)

    def __unicode__(self):
        return self.Name


class SystemVarData(models.Model):
    ''' these objects hold the system vaiable data. While system variables can
    exist in a number of formats, they are stored here in string format and are
    converted by its controling class when read by views'''
    Variable = models.ForeignKey('SystemVariable')
    AcademicYear = models.CharField(max_length=10,
                                    blank=True, null=True)
    Data = models.TextField()

    def __unicode__(self):
        return "%s(%s)" % (self.Variable.Name, self.Data)

    class Meta:
        unique_together = ('Variable', 'AcademicYear')


class StaffCpdRecords(models.Model):
    ''' These records hold CPD data for Staff '''
    Staff = models.ForeignKey('Staff')
    Name = models.CharField(max_length=200)
    TimeTaken = models.CharField(max_length=20, blank=True)
    DateTaken = models.DateField(default=datetime.date.today())
    Duration = models.CharField(max_length=20, blank=True)
    Details = models.TextField(blank=True)
    DateExpires = models.DateField(null=True, blank=True)
    CertIssued = models.BooleanField(default=False)
    Provider = models.CharField(max_length=200)
    InternalExternal = models.CharField(max_length=8,
                                       choices=(('Internal', 'Internal'),
                                                ('External', 'External')))
    Active = models.BooleanField(default=True)

class AnalysisStore(models.Model):
    '''
    This model is used to record when an analysis report is generated and held on the database. It allows staff to
    look through the list of generated files and download them to read them
    '''
    ReportName = models.FileField(upload_to='DocumentStorage',max_length=300)
    ReportGroup =  models.ForeignKey('Group')
    ReportType = models.CharField(max_length=50)
    ReportSubject = models.CharField(max_length=100)
    ReportTerm = models.CharField(max_length=20, default='Summer')
    AcademicYear = models.CharField(max_length=10,blank=True, null=True)
    GenDate = models.DateField(default=datetime.datetime.now())
    Status = models.CharField(max_length=20,default='ok')

    def __unicode__(self):
        return "%s -%s (%s)" % (self.ReportType, self.ReportSubject, self.ReportGroup.Name)

class TeacherMarkBook(models.Model):
    '''
    Holds JSON string markbooks
    '''
    BookName =  models.CharField(max_length=300)
    BookGroup = models.ForeignKey('Group')
    BookData = models.TextField()
    AcademicYear = models.CharField(max_length=10)
    UpdatedOn = models.DateTimeField(default=datetime.datetime.now())
    UpdatedBy = models.ForeignKey('Staff',null=True,blank=True)
    Active = models.BooleanField(default=True)


class ElectronicSignature(models.Model):
    """
    Electronic signatures to be used in letters.
    """
    Name = models.CharField(max_length=300)
    School = models.ForeignKey('School')
    SignatureImage = models.ImageField(upload_to='media/images/ElectronicSignatures')
    Active = models.BooleanField(default=True)