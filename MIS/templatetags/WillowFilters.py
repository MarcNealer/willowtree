# -*- coding: utf-8 -*-
"""
Created on Fri Sep 14 09:12:38 2012

@author: dbmgr
"""

from django import template
import datetime
import math
from django.utils.html import strip_tags
from django.utils.safestring import *
from MIS.modules.globalVariables.GlobalVariablesFunctions import *
import string
import settings
from boto.s3.connection import S3Connection

register = template.Library()


def  Age(birthdate, todaydate=None):
    if todaydate is None:
        todaydate = datetime.date.today()
    year = (todaydate.year - birthdate.year) - int((todaydate.month,
                            todaydate.day) < (birthdate.month, birthdate.day))
    month = todaydate.month - birthdate.month
    if month < 1:
        month = 12 + month
    month = month - int(todaydate.day < birthdate.day)
    return "%dy %dm" % (year, month)


def ChangeMenu(school, Type):
    if 'Admin' in school:
        return '%s%s' % (school.split('Admin')[0], Type)
    elif 'Applicants' in school:
        return '%s%s' % (school.split('Applicants')[0], Type)
    else:
        return '%s%s' % (school.split('Teacher')[0], Type)


def FindString(InputString, MatchString):
    if InputString.find(MatchString) > -1:
        return True
    else:
        return False


def SplitKW(KW):
    returnedString = ', '.join(KW.split(':'))
    return returnedString


def getHouseFromGroup(fullGroupName):
    newHouse = fullGroupName.split(".")
    if len(newHouse) > 4:
        newHouse2 = newHouse[4]
        return newHouse2
    else:
        return ''


def getFormFromGroup(fullGroupName):
    newForm = fullGroupName.split(".")
    if len(newForm) > 5:
        newForm2 = newForm[5]
        return newForm2
    else:
        return ''


def englishDateFormat(date):
    """returns a date in the format of YYYY-MM-DD into the UK date format
       of DD-MM-YYYY"""
    date = date.split('-')
    returnedDate = [date[2], date[1], date[0]]
    returnedDate = "-".join(returnedDate)
    return returnedDate


def AcYearPlus(AcYear, NoYears):
    """ takes the current AcYear and works out AcYear+1
    Usage {{AcYear|AcYearPlus1}}    """
    if AcYear.find('-') > 0:
        Year1, Year2 = AcYear.split('-')
        return "%d-%d" % (int(Year1) + NoYears, int(Year2) + NoYears)
    else:
        return AcYear


def JSDictMenu(Dictionary, DictName):
    """ Takes a dictionary and converts it into Java script.
    This means creating two javascript Arrays and a function"""
    if not isinstance(Dictionary, dict):
        return
    jscode = 'var %sKeys=("%s");\n' % (DictName, '","'.join(Dictionary.keys()))
    jscode += "var %sValues=  new Array ();\n" % (DictName)
    for vals in Dictionary.values():
        jscode += '%sValues.push(["%s"]);\n' % (DictName, '","'.join(vals))
    jscode += "function %s(DictValue){ return %sValues[%sKeys.indexOf(DictValue)]; }" % (DictName,DictName,DictName)
    return jscode


def removeSchoolName(school):
    ''' This filter removes the school name from the school variable found
    in the URL. This is only used in base.html for showing the menu type
    a user is currently in - roy'''
    if school:
        if school.find('Battersea') > -1:
            school = school.replace('Battersea', '')
        elif school.find('Kensington') > -1:
            school = school.replace('Kensington', '')
        elif school.find('Fulham') > -1:
            school = school.replace('Fulham', '')
        elif school.find('Clapham') > -1:
            school = school.replace('Clapham', '')
        elif school.find('Kindergarten') > -1:
            school = school.replace('Kindergarten', '')
    else:
        school = ''
    return school


def splitSalutation(Salutation):
    if len(Salutation) > 35:
        if Salutation.find('&') > 0:
            return Salutation.replace('&', '&<br>')
        elif Salutation.find('and') > 0:
            return Salutation.replace('and', 'and<br>')
        else:
            return Salutation
    else:
        return Salutation

def removeAdminStringFromSchoolInUrl(school):
    return school.replace('Admin', '')


def intToStr(number):
    return str(number)


def splitStringToListThenReturnLength(string):
    return len(string.split('.'))


@register.filter()
def Last500(textItem):
    if len(textItem) > 600:
        return strip_tags(textItem[-500:])
    else:
        return strip_tags(textItem)
Last500.is_safe = True


@register.filter()
def Last1000(textItem):
    if len(textItem) > 1100:
        return strip_tags(textItem[-1000:])
    else:
        return strip_tags(textItem)
Last1000.is_safe = True


@register.filter()
def StripSafe(textitem):
    return mark_safe(strip_tags(textitem))
StripSafe.is_safe=True


@register.filter()
def first_paragraph_italic_and_gray(text_item):
    returned_string = string.replace(text_item,
                                     '<p>',
                                     '<p><span style="color: #6E6E6E; font-style: italic;">',
                                     1)
    returned_string = string.replace(returned_string,
                                     '</p>',
                                     '</p></span>',
                                     1)
    return mark_safe(returned_string)
first_paragraph_italic_and_gray.is_safe = True


@register.filter()
def last_paragraph_italic_and_gray(text_item):
    text_item = text_item.replace("<p></p>", '')
    text_item = text_item.replace("<p>", '{{split}}<p>')
    text_item_split = text_item.split('{{split}}')
    text_to_change = text_item_split[-1]
    text_to_change = string.replace(text_to_change,
                                    '<p>',
                                    '<p><span style="color: #6E6E6E; font-style: italic;">')
    text_to_change = string.replace(text_to_change,
                                    '</p>',
                                    '</p></span>')
    text_item_split[-1] = text_to_change
    return mark_safe("".join(text_item_split))



@register.filter()
def electronic_signature(another_object, name_of_electronic_signature):
    """
    Display electronic signature in letter.
    """
    electronic_signature_object = ElectronicSignature.objects.get(Name=name_of_electronic_signature, Active=True)
    returned_html = '<img src="%s" alt="error" />' % electronic_signature_object.SignatureImage.url
    return mark_safe(returned_html)
electronic_signature.is_safe = True


@register.filter()
def StripGroup(group):
    return ('.'.join(group.split('.')[3:]).replace('Year','Yr').replace('Form.',''))


@register.filter()
def StripGroupApplicants(group):
    return ('.'.join(group.split('.')[2:]))

@register.filter()
def StripGroupTest(group):
    return len('.'.join(group.split('.')[3:]).replace('Year','Yr').replace('Form.','')) > 4

@register.simple_tag
def SchoolVar(VarName):
    returnvar=GlobalVariables.Get_SystemVar(VarName)
    if isinstance(returnvar,float):
        return "%0.0f" % returnvar
    else:
        return returnvar

@register.simple_tag
def AmChecked():
    timenow=datetime.datetime.now()
    if timenow.hour < 12:
        return "checked"
    else:
        return ''

@register.simple_tag
def PmChecked():
    timenow=datetime.datetime.now()
    if timenow.hour >= 12:
        return "checked"
    else:
        return ''

@register.simple_tag(takes_context=True)
def WillowMessages(context, MsgName):
    if MsgName in context['request'].session:
        Msg=context['request'].session[MsgName]
        del context['request'].session[MsgName]
        return Msg
    else:
        return ''

@register.simple_tag(takes_context=True)
def WorkOutAverage(context, exam1, exam2):
    ''' returns the average between two exams '''
    if exam1 and exam2:
        return int(math.ceil((float(exam1) + float(exam2))/2))
    else:
        return 'N/A'

@register.simple_tag(takes_context=True)
def ReadingWritingAverage(context, reading, writing):
    ''' returns the average level for reading and writing '''
    levelList = [['ELG', 0],
                ['W', 3],
                ['1c', 7],
                ['1b', 9],
                ['1a', 11],
                ['2c', 13],
                ['2b', 15],
                ['2a', 17],
                ['3c', 19],
                ['3b', 21],
                ['3a', 23],
                ['4c', 25],
                ['4b', 27],
                ['4a', 29],
                ['5c', 31],
                ['5b', 33],
                ['5a', 35],
                ['6c', 37],
                ['6b', 39],
                ['6a', 41],
                ['7c', 43],
                ['7b', 45],
                ['7a', 47],
                ['8c', 49],
                ['8b', 51],
                ['8a', 53]]
    result = ''
    try:
        r = int(reading[1])
    except:
        r = ''
    try:
        w = int(writing[1])
    except:
        w = ''
    if r != '' and w != '':
        level = (r+w)/2
        level = int(level)
        test = False
        for r in range(5):
            if not test:
                level = level - 1
                for i in levelList:
                    if level == i[1]:
                        result = i[0]
                        test = True
        return result
    else:
        return result

def replaceSpace(string2, stringReplaced):
    return string2.replace(stringReplaced, ' ')

@register.inclusion_tag('tinyMCEForAdhoc.js')
def includeTinyMCE(AdhocSelector,PupilId,SubjectName=None):
    if not SubjectName:
        return {'AdhocSelector':"%s%s" % (AdhocSelector,PupilId)}
    else:
        return {'AdhocSelector':"%s%s%s" % (AdhocSelector,PupilId,SubjectName)}

def removeTitle(TeacherName):
    ''' removes the title from teacher names in reports '''
    return ' '.join(TeacherName.split(' ')[1:])

def getFormOnlyFromGroupName(fullGroupName):
    ''' Get form name only from group name '''
    return fullGroupName.split('.')[5]

def keyvalue(dict, key):
    return dict[key]

def workOutDayFromWeekday(weekDay):
    ''' the datetime obj shows weeks days as numbers - this filter changes this
    number to a day. eg 0 == Monday, 1 == 'Tuesday' ...etc '''
    if weekDay == 0:
        return 'Monday'
    if weekDay == 1:
        return 'Tuesday'
    if weekDay == 2:
        return 'Wednesday'
    if weekDay == 3:
        return 'Thursday'
    if weekDay == 4:
        return 'Friday'
    if weekDay == 5:
        return 'Saturday'
    if weekDay == 6:
        return 'Sunday'

def workOutDayFromWeekdayShort(weekDay):
    ''' the datetime obj shows weeks days as numbers - this filter changes this
    number to a day. eg 0 == Mon, 1 == 'Tue' ...etc '''
    if weekDay == 0:
        return 'Mon'
    if weekDay == 1:
        return 'Tue'
    if weekDay == 2:
        return 'Wed'
    if weekDay == 3:
        return 'Thu'
    if weekDay == 4:
        return 'Fri'
    if weekDay == 5:
        return 'Sat'
    if weekDay == 6:
        return 'Sun'

@register.simple_tag
def pagebreak():
    ''' inserts a page break that works in javascript '''
    return "<div style='page-break-before:always;float:clear;'></div>"

register.filter('returnLength', splitStringToListThenReturnLength)
register.filter('removeAdminString', removeAdminStringFromSchoolInUrl)
register.filter('splitSalutation', splitSalutation)
register.filter('getFormFromGroup', getFormFromGroup)
register.filter('getHouseFromGroup', getHouseFromGroup)
register.filter('Age', Age)
register.filter('ChangeMenu', ChangeMenu)
register.filter('FindString', FindString)
register.filter('SplitKW', SplitKW)
register.filter('englishDateFormat', englishDateFormat)
register.filter('AcYearPlus', AcYearPlus)
register.filter('JSDictMenu', JSDictMenu)
register.filter('removeSchoolName', removeSchoolName)
register.filter('intToStr', intToStr)
register.filter('replaceSpace', replaceSpace)
register.filter('removeTitle', removeTitle)
register.filter('getFormOnlyFromGroupName', getFormOnlyFromGroupName)
register.filter('keyvalue', keyvalue)
register.filter('workOutDayFromWeekday', workOutDayFromWeekday)
register.filter('workOutDayFromWeekdayShort', workOutDayFromWeekdayShort)
