# imports
import kronos

# import log
import logging
log = logging.getLogger(__name__)

# WillowTree imports
from MIS.modules.AnalysisReports.AnalysisReportsNew import *
from MIS.modules.globalVariables import GlobalVariablesFunctions
from MIS.modules import GeneralFunctions
from MIS.modules.AnalysisReports.ReportFunctions import ExamDataExtract
from MIS.modules.documentServer.DocumentServerFunctions import EOTReportAttachManager
from MIS.modules.notes.BatterseaBehaviourReport import BatterseaBehaviourReport
from MIS.UK_Extra import UK_Extra
from MIS.modules.ThirdParty import ClarionCall

#######################################################################
#  Clarion  Call Updates
#############################################

@kronos.register('20 11 * * *')
def ClarionUpdateFulham():
    AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    ClarionCall(AcYear,'Current.Fulham',
                '07797151759','0122C162573B',
                "Thomas's Fulham",
                'dirsync-ICClub_337ACA3A570A540C00000122C1625754.xml')

########################################################################################################################
###############################                      Anaylsis Reports                    ###############################
########################################################################################################################


@kronos.register('15 00 * 6,7,8 *')
def SummerReportGen1():
    AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    test=AnalysisStoreCreator(AcYear, 'Summer')
    test.createTermReports()

@kronos.register('15 01 * 6,7,8 *')
def SummerReportGen2():
    AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    test=AnalysisStoreCreator(AcYear, 'Summer')
    test.createYearlyReports()


@kronos.register('15 02 * 6,7,8 *')
def SummerReportGen3():
    AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    test=AnalysisStoreCreator(AcYear, 'Summer')
    test.createCohortAnalysisReports()
    test.createSchoolReviewReports()
    test.createAPSProgressWholeSchool()

@kronos.register('30 02 * 6,7,8 *')
def SummerExams1():
    AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    test=ExamDataExtract(AcYear,'Battersea','Summer',2358)
    test.start()

@kronos.register('30 02 * 6,7,8 *')
def SummerExams2():
    AcYear=GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    test=ExamDataExtract(AcYear,'Clapham','Summer',2358)
    test.start()

########################################################################################################################
###############################               Attach EOT reports to pupils               ###############################
########################################################################################################################


def attach_eot_reports(term):
    ac_year = GlobalVariablesFunctions.GlobalVariables.Get_SystemVar('CurrentYear')
    attach_report_object = EOTReportAttachManager(term, ac_year)
    try:
        attach_report_object.AttachBattersea()
    except Exception as error:
        log.warn('ERROR: failed to attach EOT reports to Battersea: %s' % error)
    try:
        attach_report_object.AttachClapham()
    except Exception as error:
        log.warn('ERROR: failed to attach EOT reports to Clapham: %s' % error)
    try:
        attach_report_object.AttachFulham()
    except Exception as error:
        log.warn('ERROR: failed to attach EOT reports to Fulham: %s' % error)
    try:
        attach_report_object.AttachKensington()
    except Exception as error:
        log.warn('ERROR: failed to attach EOT reports to Kensington: %s' % error)


@kronos.register('30 3 4 4 *')
def attach_eot_lent():
    attach_eot_reports('L')


@kronos.register('30 3 25 12 *')
def attach_eot_mich():
    attach_eot_reports('M')


@kronos.register('30 3 20 7 *')
def attach_eot_summer():
    attach_eot_reports('S')


########################################################################################################################
###############################                 Battersea Behaviour Report               ###############################
########################################################################################################################

@kronos.register('30 23 * * *')
def run_battersea_behaviour_report():
    BatterseaBehaviourReport({'behaviour_keywords': ['Conduct', 'Detention'],
                             'groups': (('Staff.Battersea.Behavior_Report.MS_PASTORAL', [3, 4, 5]),
                                       ('Staff.Battersea.Behavior_Report.MS_HOMS_PASTORAL', [6, 7, 8]),
                                       ('Staff.Battersea.Behavior_Report.MS_LT_PASTORAL', [9]),
                                       ('Staff.Battersea.Behavior_Report.US_PASTORAL', [3, 4, 5]),
                                       ('Staff.Battersea.Behavior_Report.US_HOMS_PASTORAL', [6, 7, 8]),
                                       ('Staff.Battersea.Behavior_Report.US_LT_PASTORAL', [9]))}).main_logic()


########################################################################################################################
###############################                     Assign UPN Numbers                   ###############################
########################################################################################################################

@kronos.register('30 18 1 9 *')
def assign_upn_numbers():
    for school in ['Battersea', 'Clapham', 'Fulham', 'Kensington']:
        UK_Extra.Add_UPN(school)

########################################################################################################################
