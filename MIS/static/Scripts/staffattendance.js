var thisApp = angular.module( 'staffattendance' , [ 'ngAnimate' , 'ngSanitize' , 'ngRoute' ] );
var csrftoken = getCookie('csrftoken');
thisApp.config(function($routeProvider){
  $routeProvider
    .when('/', { templateUrl: '/WillowTree/loadhtml/staff_attendance/staff-groups.html', controller: 'staffGroupController' })
	.when('/take_attendance', { templateUrl: '/WillowTree/loadhtml/staff_attendance/take-attendance.html', controller: 'takeAttendanceController' })
	.when('/past_range', { templateUrl: '/WillowTree/loadhtml/staff_attendance/date-range.html', controller: 'dateRangeController' })
	.when('/edit_attendance', { templateUrl: '/WillowTree/loadhtml/staff_attendance/edit-attendance.html', controller: 'editAttendanceController' })
	.when('/rules', { templateUrl: '/WillowTree/loadhtml/staff_attendance/rules.html', controller: 'rulesController' })
	.when('/new_rule', { templateUrl: '/WillowTree/loadhtml/staff_attendance/new-rule.html', controller: 'newRuleController' })
	.when('/edit_rule/:ruleID', { templateUrl: '/WillowTree/loadhtml/staff_attendance/edit-rule.html', controller: 'editRuleController' })
    .otherwise({ redirectTo:'/' });
})
thisApp.controller( 'pageController' , function( $scope , $location ){
	$scope.data = {}; $scope.data.fetchingData = true; $scope.data.currentHost = window.location.protocol + '//' + window.location.host; pathArray = window.location.pathname.split( '/' );
	$scope.data.currentSchool = pathArray[2]; $scope.data.currentACYear = pathArray[3];
	$scope.data.go = function ( path ) { $scope.data.fetchingData=true; $location.path( path ); };
});
thisApp.controller( 'staffGroupController' , function( $scope , $http ){
	dataURL = $scope.data.currentHost + '/WillowTree/' + $scope.data.currentSchool + '/' + $scope.data.currentACYear + '/staff_attendance/home/';
	if( $scope.data.staffGroup == undefined ){
		$http.get( dataURL )
		.success( function( data ){ $scope.data.staffGroups = data; logoutTimerReset(); $scope.data.fetchingData=false; $scope.data.staffGroup = $scope.data.staffGroups[0]; });
	}else{ $scope.data.fetchingData=false;  }
});
thisApp.controller( 'takeAttendanceController' , function( $scope , $http , $filter ){
	if( !$scope.data.staffGroup ){ $scope.data.go( '/' ); return false; }
	$scope.data.serverMessages = "Server Messages.";
	dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/'+$scope.data.staffGroup+'/staff_attendance/take_attendance_page/';
	$http.get( dataURL )
	.success( function( data ){
		
		$scope.data.attendanceData = data;
		logoutTimerReset();
		$scope.data.fetchingData=false;
		$scope.data.ampm = 'AM';
		$scope.data.newAttendanceData = [];
		
		$.each( data , function( i , item ){
			if( item.am_rule !== false ){
				$scope.data.newAttendanceData.push( {'staff_id':item.staff_id , 'new_code':item.am_rule} );
			}else{
				$scope.data.newAttendanceData.push( {'staff_id':item.staff_id , 'new_code':'/'} );
			}
		});
		$scope.attendanceFormChange = function ( staffid ){ 
			newcode = this.selectedVal;
			$.each( $scope.data.newAttendanceData , function( i , item ){
				if( item.staff_id == staffid ){ $scope.data.newAttendanceData[i].new_code = newcode; }
			});
		};
		$scope.selectedVal = -1;
	});
	dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/staff_attendance_codes/';
	$http.get( dataURL )
	.success( function( data ){ $scope.data.attendanceCodes = data; logoutTimerReset(); });
	
	$scope.setAllCodes = function(){
		newcode = this.selectedVal;
		$( '.attendance-form-select' ).val( newcode );
		$.each( $scope.data.newAttendanceData , function( i , item ){
			angular.forEach( $scope.data.attendanceData, function( ad,a ){
				if( ad.staff_id == item.staff_id){
					if( $scope.data.ampm == 'AM' ){
						if( ad.am_rule === false ){ $scope.data.newAttendanceData[i].new_code = newcode; }
					}else{
						if( ad.pm_rule === false ){ $scope.data.newAttendanceData[i].new_code = newcode; }
					}
				}
			});
			
		});
	};
	
	$scope.submitAttendance = function(){
		ampm = $scope.data.ampm; count = 0; postData = 'csrfmiddlewaretoken=' + csrftoken;
		$.each( $scope.data.newAttendanceData , function( i , item ){
			if( item.new_code != '-1' ){
				postData += '&am_pm' + count + '=' + ampm + '&new_code' + count + '=' + item.new_code + '&staff_id' + count + '=' + item.staff_id;
				count ++;
			}
		});
		
		$scope.data.fetchingData = true;
		dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/process_attendance/';
		$scope.data.serverMessages = 'Processing...';
		$http({ method:'POST', url: dataURL, data: postData, headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'} })
		.success( function( data ){
			$scope.data.serverMessages = '';
			$.each( data ,function( i , item ){
				$scope.data.serverMessages += item + '<br>'; });
				logoutTimerReset();
				$scope.data.fetchingData = false;
				$("html, body").animate( { scrollTop: 0 }, "slow" );
				$.each( $('.attendance-form-select') , function( i , item ){
				if( $( item ).val() != -1 ){
					$.each( $scope.data.attendanceData , function( x , y ){
						if( $( item ).attr( 'data-staff-id' ) == y.staff_id ){
							if( $scope.data.ampm == 'AM' ){
								$scope.data.attendanceData[ x ].am_attendance_taken = true;
							}else if( $scope.data.ampm == 'PM' ){
								$scope.data.attendanceData[ x ].pm_attendance_taken = true;
							}
						}
					});
				}
			});
		})
		.error( function( data, status, headers, config ) { $scope.data.serverMessages = 'Error: ' + status; $scope.data.fetchingData = false; });
	};
	
	$scope.ampmChange = function(){
		tempAttendanceData = [];
		
		$.each( $scope.data.attendanceData , function( i , item ){
			if( $scope.data.ampm == 'AM' ){
				if( item.am_rule !== false ){
					tempAttendanceData.push( {'staff_id':item.staff_id , 'new_code':item.am_rule} );
				}else{
					$.each( $scope.data.newAttendanceData, function( ti,titem ){
						if( titem.staff_id == item.staff_id	){
							tempAttendanceData.push( {'staff_id':item.staff_id , 'new_code':titem.new_code} );
						}
					});
				}
			}else if( $scope.data.ampm == 'PM' ){
				if( item.pm_rule !== false ){
					tempAttendanceData.push( {'staff_id':item.staff_id , 'new_code':item.pm_rule} );
				}else{
					$.each( $scope.data.newAttendanceData, function( ti,titem ){
						if( titem.staff_id == item.staff_id	){
							tempAttendanceData.push( {'staff_id':item.staff_id , 'new_code':titem.new_code} );
						}
					});
				}
			}
		});
		$scope.data.newAttendanceData = tempAttendanceData;
		//console.log( $scope.data.newAttendanceData );
	}
});

thisApp.controller( 'dateRangeController' , function( $scope , $timeout , $http ){
	if( !$scope.data.staffGroup ){ $scope.data.go( '/' ); return false; }
	$scope.data.startDate = new Date().toDateInputValue(); $scope.data.endDate = new Date().toDateInputValue();
	$scope.validateDateRange = function(){
		x = $scope.data.startDate; y = $scope.data.endDate;
		today = new Date().toDateInputValue();
		if( x > y ){ $scope.data.formMessages = 'Start Date newer then End Date!!'; $timeout( clearFormMessage , 5000 );
		}else if( x>today || y>today ){ $scope.data.formMessages = 'Start or End Date is in the future!!'; $timeout( clearFormMessage , 5000 );
		}else{ $scope.data.go( '/edit_attendance' ); }
	}
	logoutTimerReset(); $scope.data.fetchingData = false; var clearFormMessage = function(){ $scope.data.formMessages = ""; }
});
thisApp.controller( 'editAttendanceController' , function( $scope , $timeout , $http ){
	if( !$scope.data.staffGroup ){ $scope.data.go( '/' ); return false; }
	dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/staff_attendance_codes/';
	$http.get( dataURL )
	.success( function( data ){
		$scope.data.attendanceCodes = [];
		newData = {};
		newData.attendance_code = ' ';
		newData.attendance_description = ' ';
		$scope.data.attendanceCodes.push( newData );
		
		$.each( data , function( i , item ){
			newData = {};
			newData.attendance_code = item.fields.attendance_code;
			newData.attendance_description = item.fields.attendance_description;
			$scope.data.attendanceCodes.push( newData );
		});
		logoutTimerReset();
	});
	$scope.data.serverMessages = 'Server Messaages.';
	fetchAttendanceData();
	function fetchAttendanceData(){
		dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/get_attendance_data/'+'/'+$scope.data.staffGroup+'/'+$scope.data.startDate+'/'+$scope.data.endDate+'/';
		$scope.data.fetchingData = true;
		$http.get( dataURL )
		.success( function( data ){
			var y = new Date( $scope.data.endDate );
			$scope.data.ampmData = [];
			$scope.data.dateData = [];
			$scope.data.dateCompareData = [];
			dayNames = ['Sun','Mon','Tue','Wed','Thur','Fri','Sat'];
			for(  x = new Date( $scope.data.startDate ) ; x <= y ; x.setDate(x.getDate() + 1) ){
				d = new Date( x );
				if( dayNames[ d.getDay() ] != 'Sat' && dayNames[ d.getDay() ] != 'Sun' ){
					$scope.data.ampmData.push( 'AM','PM' );
					$scope.data.dateData.push( dayNames[ d.getDay() ] + ' ' + d.getDate() + '/' + ( d.getMonth() + 1 ) );
					$scope.data.dateCompareData.push( new Date( x ).toDateInputValue() );
				}
			}
			newData = {}
			$.each( data , function( i , item ){
				$.each( $scope.data.dateCompareData, function( x , y ){
					isHere = 0; amHere = 0; pmHere = 0;
					$.each( item.attendance , function( a , b ){
						if( y == b.date_attendance_taken ) { isHere = 1; }
						if( y == b.date_attendance_taken && b.am_pm == 'AM' ) { amHere = 1; }
						if( y == b.date_attendance_taken && b.am_pm == 'PM' ) { pmHere = 1; }
					});
					newData = {};
					if( isHere == '0' ){
						newData.date_attendance_taken = y;
						newData.am_pm = 'AM';
						newData.attendance_code = ' ';
						data[ i ].attendance.push( newData );
						newData = {};
						newData.attendance_code = ' ';
						newData.date_attendance_taken = y;
						newData.am_pm = 'PM';
						data[ i ].attendance.push( newData );
					}else if( amHere == '0' ){
						newData.date_attendance_taken = y;
						newData.am_pm = 'AM';
						newData.attendance_code = ' ';
						data[ i ].attendance.push( newData );
					}else if( pmHere == '0' ){
						newData.date_attendance_taken = y;
						newData.am_pm = 'PM';
						newData.attendance_code = ' ';
						data[ i ].attendance.push( newData );
					}
				});
				item.attendance.sort( dynamicSortMultiple( 'date_attendance_taken' , 'am_pm' ) );
			});
			$scope.data.editAttendanceData = data;
			$scope.data.u = [];
			$.each( data , function( i , item ){
				$scope.data.u[i] = [];
				$.each( item.attendance , function( x , y ){
					$scope.data.u[i][x] = { 'attendance_code': y.attendance_code, 'attendance_description':' ' };
				});
			});
			logoutTimerReset();
			$scope.data.fetchingData = false;			
		});
	}
	$scope.saveAttendanceChange = function( pindex , aindex ){
		postData = 'csrfmiddlewaretoken=' + csrftoken;
		postData += '&date1=' + $scope.data.editAttendanceData[pindex].attendance[aindex].date_attendance_taken;
		postData += '&am_pm1=' +  $scope.data.editAttendanceData[pindex].attendance[aindex].am_pm;
		postData += '&staff_id1=' + $scope.data.editAttendanceData[pindex].staff.staff_id;
		postData += '&new_code1=' + $scope.data.u[pindex][aindex].attendance_code;
		$scope.data.fetchingData = true;
		dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/process_attendance/past_future/';
		$scope.data.serverMessages = 'Processing...';
		$http({
			method:'POST',
			url: dataURL,
			data: postData,
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
		})
		.success( function( data ){
			$scope.data.serverMessages = '';
			$.each( data ,function( i , item ){
				$scope.data.serverMessages += item + '<br>';
			});
			logoutTimerReset();
			$scope.data.fetchingData = false;
		})
		.error( function( data, status, headers, config ) {
			$scope.data.serverMessages = '<font color=red>WARNING!! Server responded with Error: ' + status + '</font>';
			$scope.data.fetchingData = false;
		});
	}
	$scope.popMaternity = function( item ){
		$scope.data.staff_full_name = $scope.data.editAttendanceData[ item ].staff.staff_full_name;
		$scope.data.staff_id = $scope.data.editAttendanceData[ item ].staff.staff_id;
		$scope.data.maternityPop = true;
		$scope.data.startDateMaternity = new Date( $scope.data.startDate ).toDateInputValue();
		$scope.data.endDateMaternity = new Date( $scope.data.endDate ).toDateInputValue();
	}
	$scope.processMaternity = function(){
		postData = 'csrfmiddlewaretoken=' + csrftoken;
		count = 0;
		dayNames = ['Sun','Mon','Tue','Wed','Thur','Fri','Sat'];
		var y = new Date( $scope.data.endDateMaternity );
		for(  x = new Date( $scope.data.startDateMaternity ) ; x <= y ; x.setDate(x.getDate() + 1) ){
			d = new Date( x );
			if( dayNames[ d.getDay() ] != 'Sat' && dayNames[ d.getDay() ] != 'Sun' ){
				newDate = d.getFullYear() + '-' + ( d.getMonth() +1 ) + '-' + d.getDate();
				postData += '&am_pm' + count + '=AM&new_code' + count + '=M&staff_id' + count + '=' + $scope.data.staff_id + '&date' + count + '=' + newDate;
				count++;
				postData += '&am_pm' + count + '=PM&new_code' + count + '=M&staff_id' + count + '=' + $scope.data.staff_id + '&date' + count + '=' + newDate;
				count++;
			}
		}
		dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/process_attendance/past_future/';
		$scope.data.maternityPop = false; $scope.data.fetchingData = true;
		$http({
			method:'POST',
			url: dataURL,
			data: postData,
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
		})
		.success( function( data ){
			$scope.data.serverMessages = '';
			$.each( data ,function( i , item ){
				$scope.data.serverMessages += item + '<br>';
			});
			logoutTimerReset();
			fetchAttendanceData();
			$("html, body").animate( { scrollTop: 0 }, "slow" );
		})
		.error( function( data, status, headers, config ) {
			$scope.data.serverMessages = 'Error: ' + status;
			$scope.data.fetchingData = false;
		});
	}
});
thisApp.controller( 'rulesController' , function( $scope , $timeout , $http ){
	//if( !$scope.data.staffGroup ){ $scope.data.go( '/' ); return false; }
	dataURL = '/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/view_active_staff_attendance_rules/';
	postData = 'csrfmiddlewaretoken=' + getCookie('csrftoken');
	
	$http({ method:'POST', data: postData, url: dataURL, headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'} })
	.success( function( data ){
		angular.forEach( data, function(item,i){
			data[i].fields.days = item.fields.days.replace( /,/g , ' ' );
			data[i].fields.user_names = item.fields.user_names.replace( /,/g , ' ' );
		});
		$scope.data.rulesData = data;
		logoutTimerReset();
		$scope.data.fetchingData = false;
	})
	.error( function( data, status, headers, config ) {
		$scope.data.serverMessages = 'Error: ' + status;
		$scope.data.fetchingData = false;
	});
	$scope.deleteRule = function( ruleId ){
		$scope.data.fetchingData = true;
		$timeout( function(){
			if( confirm( 'Are you sure you want to delete this rule' ) ){
				dataURL = '/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/remove_staff_attendance_rule/'+ruleId+'/';
				postData = 'csrfmiddlewaretoken=' + getCookie('csrftoken');
				$http.get( dataURL )
				.success( function( data ){
					$scope.data.serverMessages = data;
					logoutTimerReset();
					angular.forEach( $scope.data.rulesData, function( item, i ){ if( item.pk == ruleId ) $scope.data.rulesData.splice( i,1 ); });
					$scope.data.fetchingData = false;
				})
				.error( function( data, status, headers, config ) {
					$scope.data.serverMessages = 'Error: ' + status;
					$scope.data.fetchingData = false;
				});
			}else{ $scope.data.fetchingData = false; }
		}, 300);
	}
	$scope.editRule = function( ruleId ){ $scope.data.go( '/edit_rule/'+ruleId ); }
});
thisApp.controller( 'newRuleController' , function( $scope , $timeout , $http ){
	if( !$scope.data.staffGroup ){ fetchStaffGroups(); }else{ fetchStaffList(); }
	if( !$scope.data.startDate ){ $scope.data.startDate = new Date().toDateInputValue() }
	if( !$scope.data.endDate ){ $scope.data.endDate = new Date().toDateInputValue() }
	$scope.daysOfTheWeek = [{'day': 'Monday','checked':false},{'day': 'Tuesday','checked':false},{'day': 'Wednesday','checked':false},{'day': 'Thursday','checked':false},{'day': 'Friday','checked':false}];
	fetchAttendanceCodes();
	
	function fetchStaffGroups(){
		dataURL = $scope.data.currentHost + '/WillowTree/' + $scope.data.currentSchool + '/' + $scope.data.currentACYear + '/staff_attendance/home/';	
		$http.get( dataURL )
		.success( function( data ){
			$scope.data.staffGroups = data;
			$scope.data.staffGroup = $scope.data.staffGroups[0];
			fetchStaffList();
		});
	}
	function fetchStaffList(){
		$scope.data.fetchingData=true;
		dataURL = '/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/get_staff_user_names/'+$scope.data.staffGroup;
		$http.get( dataURL )
		.success( function( data ){
			angular.forEach( data, function( item, i){
				item.checked = false;
			});
			$scope.staffData = data;
			$scope.data.fetchingData=false;
		});
	}
	function fetchAttendanceCodes(){
		dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/staff_attendance_codes/';
		$http.get( dataURL )
		.success( function( data ){
			$scope.data.attendanceCodes = data;
			logoutTimerReset();
			$scope.selectedCode = -1;
		});
	}
	$scope.fetchStaffList = function(){ fetchStaffList(); }
	
	$scope.ruleSubmit = function(){
		dow = [];
		ampm = [];
		users = [];
		angular.forEach( $scope.daysOfTheWeek, function( item, i ){
			if( item.checked ){ dow.push( item.day ); }
		});
		if( $scope.timeAM ){ ampm.push( 'AM' ); }
		if( $scope.timePM ){ ampm.push( 'PM' ); }	
		angular.forEach( $scope.staffData, function( item, i ){
			if( item.checked ){ users.push( item.fields.User[0] ); }
		});
		if( !dow.length ){ $scope.data.formMessages = 'Please select at least one day!'; $timeout( clearFormMessage , 5000 ); return false; }
		if( !ampm.length ){ $scope.data.formMessages = 'Please select AM / PM!'; $timeout( clearFormMessage , 5000 ); return false; }
		if ( $scope.selectedCode == '-1' ){ $scope.data.formMessages = 'Please select an Attendance Code!'; $timeout( clearFormMessage , 5000 ); return false; }
		if( !users.length ){ $scope.data.formMessages = 'Please select at least one staff member!'; $timeout( clearFormMessage , 5000 ); return false; }
		postData = 'csrfmiddlewaretoken=' + getCookie('csrftoken');
		postData +=  '&user_names=' + users;
		postData +=  '&start_date=' + $scope.data.startDate;
		postData +=  '&stop_date=' + $scope.data.endDate;
		postData +=  '&days=' + dow;
		postData +=  '&am_pm=' + ampm;
		postData +=  '&attendance_code=' + $scope.selectedCode;
		dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/create_staff_attendance_rule/';
		$scope.data.fetchingData = true;
		$http({ method:'POST', data: postData, url: dataURL, headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'} })
		.success( function( data ){
			$scope.data.go( '/rules' );
		})
		.error( function( data, status, headers, config ) {
			alert( 'Error saving rule!\n\n Please try again or report the error.' );
			$scope.data.fetchingData = false;
		});
	}
	var clearFormMessage = function(){
		$scope.data.formMessages = "";
	}
});
thisApp.controller( 'editRuleController' , function( $scope , $timeout , $http , $routeParams ){
	//if( !$scope.data.staffGroup ){ $scope.data.go( '/' ); return false; }
	$scope.daysOfTheWeek = [{'day': 'Monday','checked':false},{'day': 'Tuesday','checked':false},{'day': 'Wednesday','checked':false},{'day': 'Thursday','checked':false},{'day': 'Friday','checked':false}];
	ruleID = $routeParams.ruleID;
	$scope.data.formMessages='';
	dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/staff_attendance_codes/';
	$http.get( dataURL )
	.success( function( data ){
		$scope.data.attendanceCodes = data;
		logoutTimerReset();
		$scope.data.fetchingData = false;
	});
	angular.forEach( $scope.data.rulesData, function( item,i ){
		if( item.pk == ruleID ){
			$scope.data.startDate = new Date( item.fields.start_date ).toDateInputValue();
			$scope.data.endDate = new Date( item.fields.stop_date ).toDateInputValue();
			$scope.selectedCode = item.fields.attendance_code;	
			dow = item.fields.days.split( ' ' );
			angular.forEach( $scope.daysOfTheWeek, function( dw,dwi ){
				angular.forEach( dow, function( day, d ){
					if( dw.day == day )$scope.daysOfTheWeek[dwi].checked = true;
				});
			});
			ampm = item.fields.am_pm.split( ',' );
			if( ampm.length == 2 ){ $scope.timeAM = true; $scope.timePM = true; }
			else if( ampm == 'AM' ){ $scope.timeAM = true; }
			else{ $scope.timePM = true; }
		}
	});
});
function dynamicSort(property) { 
    return function (obj1,obj2) {
        return obj1[property] > obj2[property] ? 1
            : obj1[property] < obj2[property] ? -1 : 0;
    }
}
function dynamicSortMultiple() {
    var props = arguments;
    return function (obj1, obj2) {
        var i = 0, result = 0, numberOfProperties = props.length;
        while(result === 0 && i < numberOfProperties) {
            result = dynamicSort(props[i])(obj1, obj2);
            i++;
        }
        return result;
    }
}
Date.prototype.toDateInputValue = (function() { var local = new Date(this); local.setMinutes(this.getMinutes() - this.getTimezoneOffset()); return local.toJSON().slice(0,10); });
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}