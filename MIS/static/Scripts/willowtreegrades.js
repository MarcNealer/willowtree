$.ajaxSetup({ 
	error: function(jqXHR, exception) {
		if (jqXHR.status === 0) {
			errortxt = 'Sorry! The record for ' + errorData + ' NOT saved!<br><br>Not connected.\n Verify Network.';
		} else if (jqXHR.status == 400) {
			errortxt = 'Sorry! The record for ' + errorData + ' NOT saved!<br><br>[400]';
		} else if (jqXHR.status == 404) {
			errortxt = 'Sorry! The record for ' + errorData + ' NOT saved!<br><br>[404]';
		} else if (jqXHR.status == 500) {
			errortxt = 'Sorry! The record for ' + errorData + ' NOT saved!<br><br>[500].';
		} else if (exception === 'parsererror') {
			errortxt = 'Sorry! The record for ' + errorData + ' NOT saved!<br><br>parse failed.';
		} else if (exception === 'timeout') {
			errortxt = 'Sorry! The record for ' + errorData + ' NOT saved!<br><br>Time out.';
		} else if (exception === 'abort') {
			errortxt = 'Sorry! The record for ' + errorData + ' NOT saved!<br><br>Ajax request aborted.';
		} else if (jqXHR.status != 403){
			errortxt = 'Sorry! The record for ' + errorData + ' NOT saved!<br><br>Uncaught Error.\n' + jqXHR.responseText;
		} else { 
			errortxt = 'Sorry! The record for ' + errorData + ' NOT saved!<br><br>Please click page refresh and try again';
		}
		showErrorPop( errortxt );
		return false;
	},
	datatype: 'json',
	type: "POST"
});

var errorData = '';

$(document).ready(function() {

	$( 'body' ).append( '<div style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background: #333; opacity: .7; z-index: 1000; display: none;" id="jerrormask"></div>' );

	jerror = '<div style="position: fixed; top: 200px; left: 0; width: 500px; height: 100px; background: #fff; z-index: 1001; padding: 30px 0; border-radius: 5px; border: 4px solid #ff4444; display: none;" id="jerror">';
	jerror += '<div id="jerrortxt" style="color: red; font-size: 20px; text-align: center;"></div>';
	jerror += '<div style="position: absolute; top: -5px; right: -5px; width: 12px; height: 13px; background: #ff4444; font-size: 18px; font-weight: bold; padding: 3px 3px; line-height: 10px; cursor: pointer; border-radius: 3px;" ';
	jerror += 'onclick="closeErrorPop();">X</div>';
	jerror += '</div>';
	$( 'body' ).append( jerror );
	
	
	//$( 'body' ).append( '<div style="position: fixed; top: 0; left: 0; width: 10px; height: 10px; background: #fff;" onclick="showErrorPop( \'Sorry, the record for snotty was NOT saved<br><br>some sort of error 404\' );">' );
	positionErrorPop();
	$( window ).resize(function() { positionErrorPop(); });
	
	$( '.RecordFormDialog').submit(function ( e ) {
		e.preventDefault();
		tinyMCE.triggerSave();	
		var formAction = $( this ).attr( 'action' );
		var formData = $( this ).serialize();
		var allInputs = $( ':input' , this );
		errorData = $( this ).attr( 'PupilName' );
		
		$.ajax({ url: formAction, data: formData, statusCode: { 200: function() {
			console.log('200 returned');
		} } });
		allInputs.each(function(){
			if( $(this).attr( 'type' ) != 'button' && $(this).attr( 'type' ) != 'submit' && $(this).attr( 'type' ) != 'hidden' ){
				if ($(this).val()){
					dElement = 'D' + $(this).attr('id').substr( 1 );
					if( this.type == 'textarea' ){
						charCount = $(this).val().length;
						wordCount = $(this).val().split( ' ' );
						wordCount = wordCount.length;
						ccElement = 'L' + $(this).attr('id').substr( 1 );
						wcElement = 'C' + $(this).attr('id').substr( 1 );
						$( '[id="' + wcElement + '"]' ).html( '<center>' + wordCount + '</center>' );
						$( '[id="' + ccElement + '"]' ).html( '<center>' + charCount + '</center>' );
						$( '[id="' + dElement + '"]' ).html( $(this).val() );
					}else{
						$( '[id="' + dElement + '"]' ).html( '<center>' + $(this).val() + '</center>' );
					}
				}
			}else{
				if( $(this).attr( 'type' ) == 'submit' ){
					showSubmitButton2( $( this ).attr( 'id' ) );
					$('#mask , .login-popup').fadeOut(300 , function() {
						$('#mask').remove();
					}); 
				}
			}
		});
    });
});

function showErrorPop( errortxt ){
	$( '#jerrortxt' ).html( errortxt );
	$( '#jerror' ).fadeIn();
	$( '#jerrormask' ).fadeIn();

}
function closeErrorPop(){
	$( '#jerror' ).fadeOut();
	$( '#jerrormask' ).fadeOut();
}
function positionErrorPop(){
	var w = ( $(window).width() / 2 ) - ( $( '#jerror' ).width() / 2 );
	$( '#jerror' ).css( 'left' , w + 'px' );
}