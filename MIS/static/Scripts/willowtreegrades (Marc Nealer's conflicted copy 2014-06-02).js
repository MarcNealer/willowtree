var errorData = '';
$.ajaxSetup({ 
	error: function(jqXHR, exception) {
		if (jqXHR.status === 0) {
			alert('Sorry! The record for ' + errorData + ' NOT saved! \n\n Not connected.\n Verify Network.');
		} else if (jqXHR.status == 400) {
			alert('Sorry! The record for ' + errorData + ' NOT saved! \n\n [400]');
		} else if (jqXHR.status == 404) {
			alert('Sorry! The record for ' + errorData + ' NOT saved! \n\n [404]');
		} else if (jqXHR.status == 500) {
			alert('Sorry! The record for ' + errorData + ' NOT saved! \n\n [500].');
		} else if (exception === 'parsererror') {
			alert('Sorry! The record for ' + errorData + ' NOT saved! \n\n parse failed.');
		} else if (exception === 'timeout') {
			alert('Sorry! The record for ' + errorData + ' NOT saved! \n\n Time out.');
		} else if (exception === 'abort') {
			alert('Sorry! The record for ' + errorData + ' NOT saved! \n\n Ajax request aborted.');
		} else if (jqXHR.status != 403){
			alert('Sorry! The record for ' + errorData + ' NOT saved! \n\n Uncaught Error.\n' + jqXHR.responseText);
		}
		return false;
	}, datatype: 'json', type: "POST"
});
$(document).ready(function(){
	$( '.RecordFormDialog' ).submit(function (e) {
		e.preventDefault();
		tinyMCE.triggerSave();	
		var formAction = $( this ).attr( 'action' );
		var formData = $( this ).serialize();
		var allInputs = $( ':input' , this );
		errorData = $( this ).attr( 'PupilName' );
		$.ajax({ url: formAction, data: formData, statusCode: { 200: function() { } } });
		allInputs.each(function(){
			if( $( this ).attr( 'type' ) != 'button' && $( this ).attr( 'type' ) != 'submit' && $( this ).attr( 'type' ) != 'hidden' ){
				if( $( this ).val() && $( this ).val() != 'None' ){
					dElement = 'D' + $( this ).attr('id').substr( 1 );
					$( '[id="' + dElement + '"]' ).html( '<center>' + $( this ).val() + '</center>' );
					if( this.type == 'textarea' ){
						charCount = $( this ).val().length;
						wordCount = $( this ).val().split( ' ' );
						wordCount = wordCount.length;
						ccElement = dElement.replace( 'Comment' , 'CommentLength' );
						wcElement = dElement.replace( 'Comment' , 'WordCount' );
						$( '[id="' + wcElement + '"]' ).html( '<center>' + wordCount + '</center>' );
						$( '[id="' + ccElement + '"]' ).html( '<center>' + charCount + '</center>' );
					}
				}
			}else{
				if( $( this ).attr( 'type' ) == 'submit' ){
					showSubmitButton2( $( this ).attr( 'id' ) );
					$('#mask , .login-popup').fadeOut(300 , function() {
						$('#mask').remove();
					}); 
				}
			}
		});
    });
});