var logoutTimerText = "Auto logout";
var logoutTimerMinutesUnits = ' Mins';
var logoutTimerSecondsUnits = ' Secs';
var logoutTimerTargetDiv = '.container';
var logoutTimerLastActionDiv = '.profileValueLogoutTimer';


var logoutTimerServerURL = window.location.origin;
var logoutTimerUrlPath = window.location.pathname;
var logoutTimerLogoutURL = logoutTimerServerURL + '/WillowTree/' + logoutTimerSchool + '/Logout/'

var logoutTimerSeconds = 60;
var logoutTimeout = '';
var logoutTimerFlashing = '';

if( logoutTimerUrlPath.indexOf( 'Admin/' ) != -1  || logoutTimerUrlPath.indexOf("Applicants/") != -1 ){
	logoutTimerMaster = 60;
}else{
	logoutTimerMaster = 30;
};
var logoutTimer = logoutTimerMaster;

var startTimer = new Date().getTime(),  
    timeTimer = 0,  
    elapsedTimer = '0.0';
	

$(document).ready(function() {
	//############## Actions after all AJAX calls ##############//
	$(document).bind("ajaxComplete", function(){
		logoutTimerReset();
	});
	
	$( logoutTimerTargetDiv ).addClass( 'logout-timer-relative' );
	$( logoutTimerTargetDiv ).append( '<div id="logout-timer"></div>' );
	$( '#logout-timer' ).append( '<div id="logout-timer-text">' + logoutTimerText + '</div>' );
	$( '#logout-timer' ).append( '<div id="logout-timer-count">' + logoutTimer + logoutTimerMinutesUnits + '</div>' );
	$( '#logout-timer' ).append( '<div style="clear: both;"></div>' );

	setTimeout( 'logoutCountdown();' , 60000 );
	setTimeout( "$( '#logout-timer' ).slideDown( 'slow' );" , 5000 );
});

function logoutCountdown(){
	logoutTimer --;

	if( logoutTimer <= 5 ){
		clearTimeout( logoutTimerFlashing );
		logoutTimerFlash();
	}
	if( logoutTimer <= 1 ){
		setTimeout( 'logoutCountdownSeconds();' , 1000 );
	}else{	
		$( '#logout-timer' ).slideUp( 'slow' , function(){
			$( '#logout-timer-count' ).html( logoutTimer + logoutTimerMinutesUnits );
			$( '#logout-timer' ).slideDown( 'slow' );
		});
		timeTimer += 60000;  
		elapsedTimer = Math.floor(timeTimer / 60000) / 10;  
		if(Math.round(elapsedTimer) == elapsedTimer) { elapsedTimer += '.0'; } 
		var diff = (new Date().getTime() - startTimer) - timeTimer;    
		logoutTimeout = setTimeout( 'logoutCountdown();' , (60000 - diff) );
	}
}

function logoutCountdownSeconds(){
	logoutTimerSeconds --;
	
	if( logoutTimerSeconds <= 0 ){
		window.location.href = logoutTimerLogoutURL;
	}else{
		$( '#logout-timer-count' ).html( logoutTimerSeconds + logoutTimerSecondsUnits );
		timeTimer += 1000;  
		elapsedTimer = Math.floor(timeTimer / 1000) / 10;  
		if(Math.round(elapsedTimer) == elapsedTimer) { elapsedTimer += '.0'; } 
		var diff = (new Date().getTime() - startTimer) - timeTimer;    
		logoutTimeout = setTimeout( 'logoutCountdownSeconds();' , (1000 - diff) );
	}
}

function logoutTimerFlash(){
	if( $( '#logout-timer-count' ).hasClass( 'logout-timer-warning' ) ){
		$( '#logout-timer-count' ).removeClass( 'logout-timer-warning' );
	}else{
		$( '#logout-timer-count' ).addClass( 'logout-timer-warning' );
	}
	logoutTimerFlashing = setTimeout( 'logoutTimerFlash();' , 500 );
}

function logoutTimerReset(){
	$( 'a' ).off( 'click');
	
	$( 'a' ).click( function(e){
		logoutTimerReset();
	});
	clearTimeout( logoutTimeout );
	clearTimeout( logoutTimerFlashing );
	logoutTimer = logoutTimerMaster + 1;
	logoutTimerSeconds = 60;
	startTimer = new Date().getTime();
    timeTimer = 0;
    elapsedTimer = '0.0';
	logoutCountdown();
	if( logoutTimerLastActionDiv ){
		dt = new Date();
		time = dt.getHours() + ":" + ( dt.getMinutes()<10?'0':'' ) + dt.getMinutes();
		$( logoutTimerLastActionDiv ).html( time );
	}
}