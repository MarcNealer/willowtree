var thisApp = angular.module( 'markbook' , [ 'ngAnimate' , 'ngSanitize' , 'ngCsv' ] );
var csrftoken = getCookie('csrftoken');

thisApp.controller( 'pageController' , function( $scope , $http , $timeout ){
	$scope.data = {};
	$scope.data.fetchingData = true;
	$scope.data.currentHost = window.location.protocol + '//' + window.location.host;
	pathArray = window.location.pathname.split( '/' );
	$scope.data.currentSchool = pathArray[2];
	$scope.data.currentACYear = pathArray[3];
	$scope.data.newBookName = '';
	$scope.groupName = groupName;
	$scope.data.saving = 'Ready.';
	
	fetchMarkBooks();
	
	$scope.newBookProcess = function(){
		if( $scope.data.newBookName.length <=4 ){
			$scope.data.newBookMessages = "Name must be 5 chars or more.";
			$timeout( clearMessage, 4000 );
			return false;
		}
		dataURL =  '/WillowTree/markbook/create/'
		postData = 'csrfmiddlewaretoken=' + csrftoken;
		postData += '&GroupName=' + $scope.groupName;
		postData += '&AcYear=' + $scope.data.currentACYear;
		postData += '&BookName=' + $scope.data.newBookName;
		$scope.data.fetchingData = true;
		$scope.data.newBookPop = false;
		closeBook();
		
		$http({
			method:'POST',
			url: dataURL,
			data: postData,
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
		})
		.success( function( data ){
			$scope.data.newBookName = '';
			fetchMarkBooks();
		})
		.error( function( data, status, headers, config ) {
			$scope.data.newBookMessages = "SERVER ERROR !! Please Report.";
			$scope.data.fetchingData = false;
			$scope.data.newBookPop = true;
		});
	}
	clearMessage = function(){
		$scope.data.newBookMessages = '';
	}
	function fetchMarkBooks(){
		dataURL = '/WillowTree/markbook/list/'+groupName+'/'+$scope.data.currentACYear+'/';
		$http.get( dataURL )
		.success( function( data ){
			$scope.data.markbookList = data;
			logoutTimerReset();
			$scope.data.fetchingData=false;
		});
	}
	$scope.deleteMarkBook = function(){
		if( angular.isUndefined( $scope.selected ) ){ return false; }
		
		if( confirm( 'Are you sure you want to delete MarkBook: ' + $scope.selected.fields.BookName ) ){
			dataURL = '/WillowTree/markbook/remove/';
			postData = 'csrfmiddlewaretoken=' + csrftoken;
			postData += '&bookid=' + $scope.selected.pk;
			$scope.data.fetchingData = true;
			closeBook();

			$http({
				method:'POST',
				url: dataURL,
				data: postData,
				headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
			})
			.success( function( data ){
				fetchMarkBooks();
			})
			.error( function( data, status, headers, config ) {
				alert( "SERVER ERROR !! MarkBook NOT deleted\n\nPlease try again or report.\n\n"+ status );
				$scope.data.fetchingData=false;
			});
		}
	}
	$scope.setSelected = function( book ){
		if( $scope.selected === book ){
			$scope.selected = undefined;
		}else{
			$scope.selected = book;
		}
	}
	$scope.isSelected = function(book) {
		return $scope.selected === book;
	}
	$scope.isSelectedDefined = function(){
		return angular.isUndefined( $scope.selected );
	}
	$scope.openMarkBook = function(){
		if( angular.isUndefined( $scope.selected ) ){ return false; }
		closeBook();
		$scope.data.fetchingData=true;
		readMarkBook();
	}
	$scope.openMarkBookDbl = function( book ){
		closeBook();
		$scope.data.fetchingData=true;
		$scope.selected = book;
		readMarkBook();
	}
	function readMarkBook(){
		dataURL = '/WillowTree/markbook/read/' + $scope.selected.pk;
		$http.get( dataURL )
		.success( function( data ){
			//console.log( JSON.parse( data[0].fields.BookData ) );
			$scope.data.handsonData = JSON.parse( data[0].fields.BookData );
			$scope.data.openBook = data[0];
			loadHandson();
			
			logoutTimerReset();
			$scope.data.fetchingData=false;
		})
		.error( function( data, status, headers, config ) {
				alert( "SERVER ERROR !! Could not load MarkBook\n\nPlease try again or report.\n\n"+ status );
				$scope.data.fetchingData=false;
		});
	}
	function loadHandson(){	
		$("#mb-handson").handsontable({
			//rowHeaders: true,
			//colHeaders: true,
			contextMenu: true,
			data: $scope.data.handsonData,
			afterChange: function( change , source ){
				if( change !== null ){
					dataURL = '/WillowTree/markbook/write/';
					postData = 'csrfmiddlewaretoken=' + csrftoken;
					postData += '&bookid=' + $scope.data.openBook.pk;
					postData += '&Data=' + angular.toJson( $scope.data.handsonData );
					$scope.data.saving = 'Auto Saving ..';
					
					$http({
						method:'POST',
						url: dataURL,
						data: postData,
						headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
					})
					.success( function( data ){
						$scope.data.saving = 'Ready.';
						logoutTimerReset();
					})
					.error( function( data, status, headers, config ) {
						alert( "SERVER ERROR !! MarkBook data NOT saved.\n\nPlease try again or report.\n\n"+ status );
						$scope.data.saving = 'Auto Save ERROR!!!';
						$scope.data.fetchingData=false;
					});
				}
			},
			cells: function (row, col, prop) {
				if (row === 0) {
					return { renderer: firstRowRenderer , readOnly: false };
				}
			}
		});
		
		function firstRowRenderer(instance, td, row, col, prop, value, cellProperties) {
			Handsontable.renderers.TextRenderer.apply(this, arguments);
			td.style.fontWeight = 'bold';
			td.style.color = '#333';
			td.style.background = '#eee';
		}
	}
	
	function closeBook (){
		$scope.data.handsonData = [];
		$scope.data.openBook =[];
		loadHandson();
	}
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}