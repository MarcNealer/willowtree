var thisApp = angular.module( 'staffattendance' , [ 'ngAnimate' , 'ngSanitize' , 'ngRoute' ] );
var csrftoken = getCookie('csrftoken');

thisApp.config(function($routeProvider) {

  $routeProvider
    .when('/', {
      templateUrl: '/WillowTree/loadhtml/staff_attendance/staff-groups.html',
	  controller: 'staffGroupController'
    })
	.when('/take_attendance', {
      templateUrl: '/WillowTree/loadhtml/staff_attendance/take-attendance.html',
	  controller: 'takeAttendanceController'
    })
	.when('/past_future_range', {
      templateUrl: '/WillowTree/loadhtml/staff_attendance/date-range.html',
	  controller: 'dateRangeController'
    })
	.when('/edit_attendance', {
      templateUrl: '/WillowTree/loadhtml/staff_attendance/edit-attendance.html',
	  controller: 'editAttendanceController'
    })
    .otherwise({
      redirectTo:'/'
    });
})

thisApp.controller( 'pageController' , function( $scope , $location ){
	$scope.data = {};
	$scope.data.fetchingData = true;
	$scope.data.currentHost = window.location.protocol + '//' + window.location.host;
	pathArray = window.location.pathname.split( '/' );
	$scope.data.currentSchool = pathArray[2];
	$scope.data.currentACYear = pathArray[3];
	
	$scope.data.go = function ( path ) {
		$scope.data.fetchingData=true;
		$location.path( path );
	};
});

thisApp.controller( 'staffGroupController' , function( $scope , $http ){
	dataURL = $scope.data.currentHost + '/WillowTree/' + $scope.data.currentSchool + '/' + $scope.data.currentACYear + '/staff_attendance/home/';
	
	$http.get( dataURL )
	.success( function( data ){
		$scope.data.staffGroups = data;
		logoutTimerReset();
		$scope.data.fetchingData=false;
		$scope.data.staffGroup = $scope.data.staffGroups[0];
	});
});

thisApp.controller( 'takeAttendanceController' , function( $scope , $http , $filter ){
	if( !$scope.data.staffGroup ){ $scope.data.go( '/' ); return false; }
	$scope.data.serverMessages = "Server Messages.";
	
	dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/'+$scope.data.staffGroup+'/staff_attendance/take_attendance_page/';
	
	$http.get( dataURL )
	.success( function( data ){
		//console.log(data);
		$scope.data.attendanceData = data;
		logoutTimerReset();
		$scope.data.fetchingData=false;
		$scope.data.ampm = 'AM';
		$scope.data.newAttendanceData = [];
		$.each( data , function( i , item ){
			$scope.data.newAttendanceData.push( {'staff_id':item.staff_id , 'new_code':-1} );
		});
		
		$scope.attendanceFormChange = function ( staffid ){
			newcode = this.selectedVal;
			$.each( $scope.data.newAttendanceData , function( i , item ){
				if( item.staff_id == staffid ){
					$scope.data.newAttendanceData[i].new_code = newcode;
				}
			});
		};
		$scope.selectedVal = -1;
	});
	
	dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/staff_attendance_codes/';
	
	$http.get( dataURL )
	.success( function( data ){
		$scope.data.attendanceCodes = data;
		logoutTimerReset();
	});
	
	$scope.setAllCodes = function(){
		newcode = this.selectedVal;
		$( '.attendance-form-select' ).val( newcode );
		
		$.each( $scope.data.newAttendanceData , function( i , item ){
			$scope.data.newAttendanceData[i].new_code = newcode;
		});
	};
	
	$scope.submitAttendance = function(){
		ampm = $scope.data.ampm;
		count = 0;
		postData = 'csrfmiddlewaretoken=' + csrftoken;
		
		$.each( $scope.data.newAttendanceData , function( i , item ){
			if( item.new_code != '-1' ){
				postData += '&am_pm' + count + '=' + ampm + '&new_code' + count + '=' + item.new_code + '&staff_id' + count + '=' + item.staff_id;
				count ++;
			}
		});
		
		$scope.data.fetchingData = true;
		dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/process_attendance/';
		$scope.data.serverMessages = 'Processing...';
		
		$http({
			method:'POST',
			url: dataURL,
			data: postData,
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
		})
		.success( function( data ){
			console.log( data.length );
			$scope.data.serverMessages = '';
			$.each( data ,function( i , item ){
				$scope.data.serverMessages += item + '<br>';
			});
			logoutTimerReset();
			$scope.data.fetchingData = false;
			$("html, body").animate( { scrollTop: 0 }, "slow" );

			$.each( $('.attendance-form-select') , function( i , item ){
				if( $( item ).val() != -1 ){
					$.each( $scope.data.attendanceData , function( x , y ){
						if( $( item ).attr( 'data-staff-id' ) == y.staff_id ){
							if( $scope.data.ampm == 'AM' ){
								$scope.data.attendanceData[ x ].am_attendance_taken = true;
							}else if( $scope.data.ampm == 'PM' ){
								$scope.data.attendanceData[ x ].pm_attendance_taken = true;
							}
						}
					});
				}
			});
		})
		.error( function( data, status, headers, config ) {
			$scope.data.serverMessages = '<font color=red>Error: ' + status } '</font>';
			$scope.data.fetchingData = false;
		});
	};
});

thisApp.controller( 'dateRangeController' , function( $scope , $timeout , $http ){
	if( !$scope.data.staffGroup ){ $scope.data.go( '/' ); return false; }
	$scope.data.startDate = new Date().toDateInputValue();
	$scope.data.endDate = new Date().toDateInputValue();
	
	$scope.validateDateRange = function(){
		var x = new Date( $scope.data.startDate );
		var y = new Date( $scope.data.endDate );
		if( x > y ){
			$scope.data.formMessages = 'Start Date newer then End Date!!';
			$timeout( clearFormMessage , 5000 );
		}else{
			$scope.data.go( '/edit_attendance' );
		}
	}
	logoutTimerReset();
	$scope.data.fetchingData = false;	
	var clearFormMessage = function(){
		$scope.data.formMessages = "";
	}
});

thisApp.controller( 'editAttendanceController' , function( $scope , $timeout , $http ){
	if( !$scope.data.staffGroup ){ $scope.data.go( '/' ); return false; }
	
	dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/staff_attendance_codes/';
	
	$http.get( dataURL )
	.success( function( data ){
		$scope.data.attendanceCodes = [];
		newData = {};
		newData.attendance_code = ' ';
		newData.attendance_description = ' ';
		$scope.data.attendanceCodes.push( newData );
		
		$.each( data , function( i , item ){
			newData = {};
			newData.attendance_code = item.fields.attendance_code;
			newData.attendance_description = item.fields.attendance_description;
			$scope.data.attendanceCodes.push( newData );
		});

		logoutTimerReset();
	});
	
	dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/get_attendance_data/'+'/'+$scope.data.staffGroup+'/'+$scope.data.startDate+'/'+$scope.data.endDate+'/';
	$scope.data.fetchingData = true;
	$scope.data.serverMessages = 'Server Messaages.';
	
	$http.get( dataURL )
	.success( function( data ){
		var y = new Date( $scope.data.endDate );
		$scope.data.ampmData = [];
		$scope.data.dateData = [];
		$scope.data.dateCompareData = [];
		
		dayNames = ['Sun','Mon','Tue','Wed','Thur','Fri','Sat'];
		
		for(  x = new Date( $scope.data.startDate ) ; x <= y ; x.setDate(x.getDate() + 1) ){
			d = new Date( x );
			if( dayNames[ d.getDay() ] != 'Sat' && dayNames[ d.getDay() ] != 'Sun' ){
				$scope.data.ampmData.push( 'AM','PM' );
				$scope.data.dateData.push( dayNames[ d.getDay() ] + ' ' + d.getDate() + '/' + ( d.getMonth() + 1 ) );
				$scope.data.dateCompareData.push( new Date( x ).toDateInputValue() );
			}
		}
		
		newData = {}
		
		$.each( data , function( i , item ){
			$.each( $scope.data.dateCompareData, function( x , y ){
				isHere = 0;
				amHere = 0;
				pmHere = 0;
				
				$.each( item.attendance , function( a , b ){
					if( y == b.date_attendance_taken ) { isHere = 1; }
					if( y == b.date_attendance_taken && b.am_pm == 'AM' ) { amHere = 1; }
					if( y == b.date_attendance_taken && b.am_pm == 'PM' ) { pmHere = 1; }
				});
				
				newData = {};
				if( isHere == '0' ){
					newData.date_attendance_taken = y;
					newData.am_pm = 'AM';
					newData.attendance_code = ' ';
					data[ i ].attendance.push( newData );
					newData = {};
					newData.attendance_code = ' ';
					newData.date_attendance_taken = y;
					newData.am_pm = 'PM';
					data[ i ].attendance.push( newData );
				}else if( amHere == '0' ){
					newData.date_attendance_taken = y;
					newData.am_pm = 'AM';
					newData.attendance_code = ' ';
					data[ i ].attendance.push( newData );
				}else if( pmHere == '0' ){
					newData.date_attendance_taken = y;
					newData.am_pm = 'PM';
					newData.attendance_code = ' ';
					data[ i ].attendance.push( newData );
				}
			});
			item.attendance.sort( dynamicSortMultiple( 'date_attendance_taken' , 'am_pm' ) );
		});
		$scope.data.editAttendanceData = data;
		
		$scope.data.u = [];
		
		$.each( data , function( i , item ){
			$scope.data.u[i] = [];
			$.each( item.attendance , function( x , y ){
				$scope.data.u[i][x] = { 'attendance_code': y.attendance_code, 'attendance_description':' ' };
			});
		});
		
		//console.log( $scope.data.editAttendanceData );
		logoutTimerReset();
		$scope.data.fetchingData = false;
	});
	
	$scope.saveAttendanceChange = function( pindex , aindex ){
		postData = 'csrfmiddlewaretoken=' + csrftoken;
		postData += '&date1=' + $scope.data.editAttendanceData[pindex].attendance[aindex].date_attendance_taken;
		postData += '&am_pm1=' +  $scope.data.editAttendanceData[pindex].attendance[aindex].am_pm;
		postData += '&staff_id1=' + $scope.data.editAttendanceData[pindex].staff.staff_id;
		postData += '&new_code1=' + $scope.data.u[pindex][aindex].attendance_code;
		
		$scope.data.fetchingData = true;
		dataURL = $scope.data.currentHost+'/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/staff_attendance/process_attendance/past_future/';
		$scope.data.serverMessages = 'Processing...';
		
		$http({
			method:'POST',
			url: dataURL,
			data: postData,
			headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'}
		})
		.success( function( data ){
			console.log( data.length );
			$scope.data.serverMessages = '';
			$.each( data ,function( i , item ){
				$scope.data.serverMessages += item + '<br>';
			});
			logoutTimerReset();
			$scope.data.fetchingData = false;
			
		})
		.error( function( data, status, headers, config ) {
			$scope.data.serverMessages = '<font color=red>WARNING!! Server responded with Error: ' + status + '</font>';
			$scope.data.fetchingData = false;
		});
	}
});

function dynamicSort(property) { 
    return function (obj1,obj2) {
        return obj1[property] > obj2[property] ? 1
            : obj1[property] < obj2[property] ? -1 : 0;
    }
}

function dynamicSortMultiple() {
    var props = arguments;
    return function (obj1, obj2) {
        var i = 0, result = 0, numberOfProperties = props.length;
        while(result === 0 && i < numberOfProperties) {
            result = dynamicSort(props[i])(obj1, obj2);
            i++;
        }
        return result;
    }
}

Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}