var thisApp = angular.module( 'notifications' , [ 'ngAnimate' , 'ngSanitize' , 'ngRoute' ] );
var csrftoken = getCookie('csrftoken');

thisApp.config(function($routeProvider){
  $routeProvider
    .when('/', {
      templateUrl: '/WillowTree/loadhtml/notes-notifications/home.html',
	  controller: 'homeController'
    })
    .otherwise({
      redirectTo:'/'
    });
})

thisApp.controller( 'pageController' , function( $scope , $location ){
	$scope.data = {};
	$scope.data.fetchingData = true;
	$scope.data.currentHost = window.location.protocol + '//' + window.location.host;
	pathArray = window.location.pathname.split( '/' );
	$scope.data.currentSchool = pathArray[2];
	$scope.data.currentACYear = pathArray[3];
	
	$scope.data.go = function ( path ) {
		$scope.data.fetchingData=true;
		$location.path( path );
	};
});

thisApp.controller( 'homeController' , function( $scope , $http ){
	dataURL = $scope.data.currentHost + '/WillowTree/' + $scope.data.currentSchool + '/' + $scope.data.currentACYear + '/staff_attendance/home/';
	$scope.data.fetchingData=false;
	return false;
	$http.get( dataURL )
	.success( function( data ){
		$scope.data.staffGroups = data;
		logoutTimerReset();
		$scope.data.fetchingData=false;
		$scope.data.staffGroup = $scope.data.staffGroups[0];
	});
});





function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}