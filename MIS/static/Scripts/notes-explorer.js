var thisApp = angular.module( 'notes-explorer' , [ 'ngAnimate' , 'ngSanitize' , 'ngRoute', 'multi-select', 'ngCsv' ] );
var csrftoken = getCookie('csrftoken');

thisApp.config(function($routeProvider){
  $routeProvider
    .when('/', {
      templateUrl: '/WillowTree/loadhtml/notes-explorer/home.html',
	  controller: 'homeController'
    })
    .otherwise({
      redirectTo:'/'
    });
})
thisApp.controller( 'pageController' , function( $scope , $location ){
	$scope.data = {};
	$scope.data.fetchingData = true;
	$scope.data.currentHost = window.location.protocol + '//' + window.location.host;
	pathArray = window.location.pathname.split( '/' );
	$scope.data.currentSchool = pathArray[2];
	$scope.data.currentACYear = pathArray[3];
	
	$scope.data.go = function ( path ) {
		$scope.data.fetchingData=true;
		$location.path( path );
	};
});
thisApp.controller( 'homeController' , function( $scope , $location, $http ){
	$scope.data.fetchingData = true;
	$scope.startDate = new Date().toDateInputValue();
	$scope.endDate = new Date().toDateInputValue();
	searchReset();
	dataURL = '/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/notes_explorer/get_all_current_pupil_groups/';
	$http.get( dataURL )
	.success( function( data ){ $scope.pupilGroups = data; $scope.selectedGroup = $scope.pupilGroups[0]; $scope.data.fetchingData = false; })
	.error( function( data ){ alert( 'ERROR fetching data.\n\nPlease retry or report the problem.' ); });

	dataURL = '/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/notes_explorer/get_all_keywords/';
	$http.get( dataURL )
	.success( function( data ){
		$scope.keywords1 = [];
		$scope.keywords2 = [];
		$scope.keywords3 = [];
		$scope.keywords4 = [];
		angular.forEach( data, function( item,i ){
			$scope.keywords1.push({ 'name': item , 'keyword': item , 'selected': false });
			$scope.keywords2.push({ 'name': item , 'keyword': item , 'selected': false });
			$scope.keywords3.push({ 'name': item , 'keyword': item , 'selected': false });
			$scope.keywords4.push({ 'name': item , 'keyword': item , 'selected': false });
		});
		$scope.data.fetchingData = false;
	})
	.error( function( data ){ alert( 'ERROR fetching data.\n\nPlease retry or report the problem.' ); });
	$scope.search = function(){
		searchReset();
		isValid = 0;
		postData = 'csrfmiddlewaretoken=' + getCookie('csrftoken');
		if( $scope.search1.length ){ varData = ''; isValid = 1; angular.forEach( $scope.search1, function( item,i ){ varData += item.keyword; if( i < $scope.search1.length-1 ){ varData += ':'; } }); postData += '&keyword_1=' + varData; $scope.searchView1 = varData.split( ':' ).join( ', ' ); }
		if( $scope.search2.length ){ varData = ''; isValid = 1; angular.forEach( $scope.search2, function( item,i ){ varData += item.keyword; if( i < $scope.search2.length-1 ){ varData += ':'; } }); postData += '&keyword_2=' + varData; $scope.searchView2 = varData.split( ':' ).join( ', ' ); }
		if( $scope.search3.length ){ varData = ''; isValid = 1; angular.forEach( $scope.search3, function( item,i ){ varData += item.keyword; if( i < $scope.search3.length-1 ){ varData += ':'; } }); postData += '&keyword_3=' + varData; $scope.searchView3 = varData.split( ':' ).join( ', ' ); }
		if( $scope.search4.length ){ varData = ''; isValid = 1; angular.forEach( $scope.search4, function( item,i ){ varData += item.keyword; if( i < $scope.search4.length-1 ){ varData += ':'; } }); postData += '&keyword_4=' + varData; $scope.searchView4 = varData.split( ':' ).join( ', ' ); }
		if( !isValid ){ return false; }
		$scope.data.fetchingData = true;
		postData += '&start_date=' + $scope.startDate;
		postData += '&end_date=' + $scope.endDate;
		postData += '&group_name=' + $scope.selectedGroup;
		dataURL = '/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/notes_explorer/get_pupil_notes_count/';
		$http({ method:'POST', url: dataURL, data: postData, headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'} })
		.success( function( data ){
			angular.forEach( data ,function( item,i ){ data[i].name = item[1]; });
			$scope.pupilData = data;
			logoutTimerReset();
			$scope.data.fetchingData = false;
		})
		.error( function( data, status, headers, config ){ alert( 'Server responded with Error: ' + status ); $scope.data.fetchingData = false; });	
	}
	function searchReset(){
		$scope.searchView1 = '';
		$scope.searchView2 = '';
		$scope.searchView3 = '';
		$scope.searchView4 = '';
		$scope.pupilData = [];
	}
	$scope.showNotes = function( col , id , count ){
		if( ! count ){ return false; }
		dataURL = '/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/notes_explorer/get_pupil_notes_individual/';
		postData = 'csrfmiddlewaretoken=' + getCookie('csrftoken');
		postData += '&pupil_id=' + id;
		postData += '&start_date=' + $scope.startDate;
		postData += '&end_date=' + $scope.endDate;
		if( col == 1 ){ varData = $scope.search1; } if( col == 2 ){ varData = $scope.search2; } if( col == 3 ){ varData = $scope.search3; } if( col == 4 ){ varData = $scope.search4; }
		postData += '&keywords=';
		angular.forEach( varData, function ( item,i ){ postData += item.keyword; if( i < varData.length-1 ){  postData += ':'; } });
		$scope.data.fetchingData = true;
		$http({ method:'POST', url: dataURL, data: postData, headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'} })
		.success( function( data ){
			angular.forEach( data, function( item,i ){
				data[i].fields.NoteId.keywords = data[i].fields.NoteId.keywords.split(':').join(', ');
			});
			$scope.noteData = data;
			logoutTimerReset();
			$scope.data.fetchingData = false;
			$scope.notePop = true;
			$('html,body').animate({ scrollTop: 0 }, 500);
			$scope.csvFileName = $scope.selectedGroup.split('.').join('_') + '_' + new Date().toDateInputValue();
		})
		.error( function( data, status, headers, config ) {
			alert( 'Server responded with Error: ' + status );
			$scope.data.fetchingData = false;
		});	
	}
	$scope.exportCVS = function(){
		notesArray = [];
		angular.forEach( $scope.noteData, function( item,i ){
			notesArray.push( [ item.fields.PupilId , item.fields.NoteId.keywords , item.fields.NoteId.raised_by_staff_member , item.fields.NoteId.raised_by_date , item.fields.NoteId.note_text ] );
		});
		return notesArray;
	}
	$scope.resetForm = function(){
		angular.forEach( $scope.keywords1, function( item,i ){ item.selected = false; });
		angular.forEach( $scope.keywords2, function( item,i ){ item.selected = false; });
		angular.forEach( $scope.keywords3, function( item,i ){ item.selected = false; });
		angular.forEach( $scope.keywords4, function( item,i ){ item.selected = false; });
		$scope.selectedGroup = $scope.pupilGroups[0];
		$scope.startDate = new Date().toDateInputValue();
		$scope.endDate = new Date().toDateInputValue();
		$scope.pupilData = [];
	};
});
Date.prototype.toDateInputValue = (function() {
    var local = new Date(this);
    local.setMinutes(this.getMinutes() - this.getTimezoneOffset());
    return local.toJSON().slice(0,10);
});
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}