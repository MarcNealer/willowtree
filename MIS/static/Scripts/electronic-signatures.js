var thisApp = angular.module( 'electronic-signatures' , [ 'ngAnimate' , 'ngSanitize' , 'ngRoute', 'angularFileUpload' ] );
var csrftoken = getCookie('csrftoken');

thisApp.config(function($routeProvider){
  $routeProvider
    .when('/', {
      templateUrl: '/WillowTree/loadhtml/electronic-signatures/home.html',
	  controller: 'homeController'
    })
    .otherwise({
      redirectTo:'/'
    });
})

thisApp.controller( 'pageController' , function( $scope , $location ){
	$scope.data = {};
	$scope.data.fetchingData = true;
	$scope.data.currentHost = window.location.protocol + '//' + window.location.host;
	pathArray = window.location.pathname.split( '/' );
	$scope.data.currentSchool = pathArray[2];
	$scope.data.currentACYear = pathArray[3];
	
	$scope.data.go = function ( path ) {
		$scope.data.fetchingData=true;
		$location.path( path );
	};
});

thisApp.controller( 'homeController' , function( $scope , $http, $upload, $timeout ){
	
	function getSignatures(){
		$scope.data.fetchingData = true;
		dataURL = '/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/view_electronic_signatures/';
		$http.get( dataURL )
		.success( function( data ){ $scope.signatures = data; console.log(data); $scope.data.fetchingData = false; logoutTimerReset(); })
		.error( function( data ){ alert( 'ERROR fetching data.\n\nPlease retry or report the problem.' ); });
	}
	
	getSignatures();
	
	$scope.uploadImage = function( $files, $event, $rejected ){
		if( $files[0] == undefined ){ return false; }
		if( $scope.sigName == '' || $scope.sigName == undefined ){ angular.element( '#file-input' ).val(null); angular.element( '#sig-name' ).focus(); return false; }
		
		isRejected = 0;
		if( $rejected.length ){ isRejected = 1; }
		if( $rejected == -1 ){
			fileType = $files[0].type;
			if( fileType != 'image/png' && fileType != 'image/jpg' && fileType != 'image/gif' && fileType != 'image/jpeg' ){
				isRejected = 1;
			}
		}
		
		if( !isRejected ){
			dataURL = '/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/add_electronic_signature/';
			//$scope.data.fetchingData = true;
			$scope.uploading = true;
			$scope.uploadProgress = 0;
			
			$upload.upload({
				url: dataURL,
				method: 'POST',
				data: { csrfmiddlewaretoken: getCookie('csrftoken'), image_for_signature: $files[0], name_of_signature: $scope.sigName  },
			  })
			  .progress(function(evt) {
				$scope.uploadProgress = parseInt(100.0 * evt.loaded / evt.total);
			  })
			  .success(function(data, status, headers, config) {
				$scope.uploading = false;
				angular.element( '#file-input' ).val(null);
				getSignatures();
			  })
			  .error(function(){
				alert( 'Upload ERROR! Please try again or report.' );
				$scope.uploading = false;
			  });
		}
	}
	
	$scope.deleteSignature = function( pk ){
		$scope.data.fetchingData = true;
		$timeout( function(){
			if( confirm( 'Are you sure you want to delete this signature?' ) ){
				dataURL = '/WillowTree/'+$scope.data.currentSchool+'/'+$scope.data.currentACYear+'/remove_electronic_signature/' + pk + '/';
				console.log( dataURL );
				
				$http.get( dataURL )
				.success( function( data ){
					getSignatures();
				})
				.error( function( data, status, headers, config ){ alert( 'Delete ERROR!! Please try again or report.' ); $scope.data.fetchingData = false;  });
			}else{ $scope.data.fetchingData = false; }
		}, 300);
	}
});

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}