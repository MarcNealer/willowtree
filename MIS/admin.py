#-------------------------------------------------------------------------------

# Name:        module1
# Purpose:
#
# Author:      DBMgr
#
# Created:     15/01/2012
# Copyright:   (c) DBMgr 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
#!/usr/bin/env python

from django.contrib import admin
from MIS.models import *

class MenusAdmin(admin.ModelAdmin):
    list_display=('MenuType','Order','Group')
    ordering=['MenuType','Order','Group']
    search_fields=['Group__Name']

class PupilAdmin(admin.ModelAdmin):
    search_fields = ['Forename', 'Surname']

class AlertTypeInLine(admin.StackedInline):
    model=AlertType

class AlertGroupInLine(admin.ModelAdmin):
    inlines=[AlertTypeInLine,]
    entries=5

class StaffAdmin(admin.ModelAdmin):
    search_fields = ['Forename', 'Surname']

admin.site.register(Family)
admin.site.register(Pupil, PupilAdmin)
admin.site.register(Contact)
admin.site.register(Staff,StaffAdmin)
admin.site.register(StaffEmergencyContact)
admin.site.register(School)
admin.site.register(SchoolList)
admin.site.register(Address)
admin.site.register(FamilyContact)
admin.site.register(FamilyChildren)
admin.site.register(FamilyRelationship)
admin.site.register(Group)
admin.site.register(PupilGroup)
admin.site.register(RollOverRule)
admin.site.register(Subject)
admin.site.register(ParentSubjects)
admin.site.register(Document)
admin.site.register(PupilDocument)
admin.site.register(StaffDocument)
admin.site.register(FamilyDocument)
admin.site.register(SchoolDocument)
admin.site.register(MenuTypes)
admin.site.register(Menus,MenusAdmin)
admin.site.register(MenuItems)
admin.site.register(MenuActions)
admin.site.register(MenuSubMenu)
admin.site.register(GroupMenuItems)
admin.site.register(AttendanceCode)
admin.site.register(PupilAttendance)
admin.site.register(AttendanceTaken)
admin.site.register(AttendanceNotTaken)
admin.site.register(AttendanceNotTakenCode)
admin.site.register(PupilAlert)
admin.site.register(AlertType)
admin.site.register(AlertGroup,AlertGroupInLine)
admin.site.register(StaffGroup)
admin.site.register(NoteKeywords)
admin.site.register(NoteDetails)
admin.site.register(NoteToIssue)
admin.site.register(PupilNotes)
admin.site.register(StaffNotes)
admin.site.register(FamilyNotes)
admin.site.register(ContactNotes)
admin.site.register(ExtentionRecords)
admin.site.register(ExtentionData)
admin.site.register(ExtentionFields)
admin.site.register(ExtentionPickList)
admin.site.register(ExtentionForPupil)
admin.site.register(ExtentionForStaff)
admin.site.register(ExtentionForContact)
admin.site.register(ExtentionForSchool)
admin.site.register(ExtentionForFamily)
admin.site.register(ExtentionsIndex)
admin.site.register(ExtentionString)
admin.site.register(ExtentionNumerical)
admin.site.register(ExtentionBoolean)
admin.site.register(ExtentionDate)
admin.site.register(ExtentionTime)
admin.site.register(ExtentionTextBox)
admin.site.register(LetterTemplate)
admin.site.register(LetterBody)
admin.site.register(PupilsPastSchool)
admin.site.register(PupilsNextSchool)
admin.site.register(PupilTarget)
admin.site.register(PupilProvision)
admin.site.register(SystemVariable)
admin.site.register(SystemVarData)
admin.site.register(GradeInputElement)
admin.site.register(ReportingSeason)
admin.site.register(PupilGiftedTalented)
admin.site.register(NoteNotification)
admin.site.register(PupilFutureSchoolProgress)
admin.site.register(AnalysisStore)
admin.site.register(TeacherMarkBook)
